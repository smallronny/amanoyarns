/*
 * Custom code goes here.
 * A template should always ship with an empty custom.js
 */
 
/* Code hover Section nuestros Hilados */
function show (sec_nuestros_hilados, type){
            if (type == 'in'){
                $('.sec_nuestros_hilados').css('background-image','url(' + sec_nuestros_hilados+ ')');
            }else {
                $('.sec_nuestros_hilados').css('background-image','url(' + sec_nuestros_hilados+ ')');
            }
            
        }
        $(".hilados_luxury").on({
            "mouseover" : function() {
                console.log('1');
                show("/img/cms/01-luxury-patrones-home-amano-yarns.jpg", 'in');
            },
            "mouseout" : function() {
                show("/img/cms/02-origin-nuestros-hilados-home-amano-yarns.jpg");
            }
        });

        $(".hilados_origin").on({
            "mouseover" : function() {
                console.log('1');
                show("/img/cms/02-origin-nuestros-hilados-home-amano-yarns.jpg", 'in');
            },
            "mouseout" : function() {
                show("/img/cms/02-origin-nuestros-hilados-home-amano-yarns.jpg");
            }
        });

        $(".hilados_ourwool").on({
            "mouseover" : function() {
                console.log('1');
                show("/img/cms/03-Our-wool-Nuestros-hilados-amano-yarns.jpeg", 'in');
                
            },
            "mouseout" : function() {
                show("/img/cms/02-origin-nuestros-hilados-home-amano-yarns.jpg");
            }
        });

        $(".hilados_special_blends").on({
            "mouseover" : function() {
                console.log('1');
                show("/img/cms/04-special-blends-patrones-home-amano-yarns.jpg", 'in');
                
            },
            "mouseout" : function() {
                show("/img/cms/02-origin-nuestros-hilados-home-amano-yarns.jpg");
            }
        });

         $(".hilados_spring_summer").on({
            "mouseover" : function() {
                console.log('1');
                show("/img/cms/05-Spring_Summer-nuestros-hilados-home-amano-yarns.jpeg", 'in');
                
            },
            "mouseout" : function() {
                show("/img/cms/02-origin-nuestros-hilados-home-amano-yarns.jpg");
            }
        });
 
  
/* Menu Mobile close */
 $('.menu-close').click(function() {
      $('#_mobile_iqitmegamenu-mobile').collapse('hide');
    });


/**** HERO BLOG *****/
$( document ).ready(function() {
    var html = `
  <div class="row align-items-center justify-content-center ">
   <div class="col-md-3 d-none">
      <div class="text-enlaces">
         <div class="">
            <h2 class="blue">Nuestras<br>Tiendas</h2>
         </div>
      </div>
   </div>
   <div class="col-md-12 p-0">
      <img class="d-none d-sm-none d-md-block w-100" src="/img/cms/blog-top-Amano-yarns.jpg" alt="Blog">  
      <img class="d-block d-sm-block d-md-none w-100" src="/img/cms/blog-top-movil-Amano-yarns.jpg" alt="Blog"> 
      <h2 class="text-hero-blog">Blog</h2> 
   </div>
</div>
`;
$( "#module-prestablog-blog nav.breadcrumb" ).html(html);
});
 


