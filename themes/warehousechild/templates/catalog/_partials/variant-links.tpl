<div class="variant-links">
  {foreach from=$variants item=variant}
  {assign var="value_at" value=$variant.name|strpos:"|"}
    <a href="{$variant.url}"
       class="{$variant.type}"
       title="{if $variant.name|substr:0:$value_at != ""}{$variant.name|substr:0:$value_at}{else}{$variant.name}{/if}"
      {if $variant.html_color_code} style="background-color: {$variant.html_color_code}" {/if}
      {if $variant.texture} style="background-image: url({$variant.texture})" {/if}
    ><span class="sr-only">{$variant.name}</span></a>
  {/foreach}
  <span class="js-count count"></span>
</div>
