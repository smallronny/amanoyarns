{if $logged}
{if $customer instanceof customer}
{$customer->firstname|truncate:15:'...'}
{else}
{$customer.firstname|truncate:15:'...'}
{/if}
{else}
  {l s='Sign in' d='Shop.Theme.Actions'}
{/if}
