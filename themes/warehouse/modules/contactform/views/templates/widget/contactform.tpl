<div class="row justify-content-center">
   <section class="col-md-6 contact-form">
      <form action="{$urls.pages.contact}" method="post"
      {if $contact.allow_file_upload}enctype="multipart/form-data"{/if}>
      {if $notifications}
      <div class="col-xs-12 alert {if $notifications.nw_error}alert-danger{else}alert-success{/if}">
         <ul>
            {foreach $notifications.messages as $notif}
            <li>{$notif}</li>
            {/foreach}
         </ul>
      </div>
      {/if}
      <section class="form-fields">
         <div class="form-group row">
            <div class="col-md-8 d-none">
               <h4>{l s='Contact us' d='Shop.Theme.Global'}</h4>
            </div>
         </div>

         <div class="row form-group justify-content-center ">
            <div class="col-md-8 form-yarns ">
               <label class="form-control-label mb-0  required">{l s='Full Name' d='Shop.Forms.Labels'}</label> 
               <input
                  class="form-control"
                  name="fullname"
                  type="text"
                  value="{$contact.fullname}"
                  placeholder="{l s='Enter your full name' d='Shop.Forms.Help'}"
                  {if $contact.fullname}{$contact.fullname}{/if}>
            </div>
         </div>
         

         <div class="row form-group justify-content-center d-none">
            <div class="col-md-8 form-yarns">
               <label class=" form-control-label d-none">{l s='Subject' d='Shop.Forms.Labels'}</label>
               <select name="id_contact" class="form-control form-control-select">
                  {foreach from=$contact.contacts item=contact_elt}
                  <option value="{$contact_elt.id_contact}">{$contact_elt.name}</option>
                  {/foreach}
               </select>
            </div>
         </div>


         

         <div class="row form-group justify-content-center ">
            <div class="col-md-8 form-yarns">
               <label class=" form-control-label mb-0 required">{l s='Email address' d='Shop.Forms.Labels'}</label>
               <input
                  class="form-control required"
                  name="from"
                  type="email"
                  value="{$contact.email}"
                  placeholder="{l s='Enter your e-mail' d='Shop.Forms.Help'}"
                  >
            </div>
         </div>

         <div class="row form-group justify-content-center ">
         	<div class="col-md-8 form-yarns ">
         	 <label class="form-control-label mb-0  required">{l s='Select Country' d='Shop.Forms.Labels'}</label> 
          {hook h='AdditionalCustomerFormFields'}
            </div>
         </div>

        

         {if $contact.allow_file_upload}

         <div class="row form-group justify-content-center ">
            <div class="col-md-8 form-yarns">
               <label class=" form-control-label d-none">{l s='Attachment' d='Shop.Forms.Labels'}</label>
               <input type="file" name="fileUpload" class="filestyle" data-buttonText="{l s='Choose file' d='Shop.Theme.Actions'}">
            </div>
            <span class="col-md-3 form-control-comment">
            {l s='optional' d='Shop.Forms.Help'}
            </span>
         </div>

         {/if}

         <div class="row form-group justify-content-center ">
            <div class="col-md-8 form-yarns">
               <label class="form-control-label required">{l s='Message' d='Shop.Forms.Labels'}</label>
               <textarea
                  class="form-control"
                  style="border: 2px solid #f3f3f3;"
                  name="message"
                  placeholder="{l s='' d='Shop.Forms.Help'}"
                  rows="5"
                  >{if $contact.message}{$contact.message}{/if}</textarea>
            </div>
         </div>

         {if isset($id_module)}
         <div class="form-group row">
            <div class="offset-md-3 col-md-9">
               {hook h='displayGDPRConsent' id_module=$id_module}
            </div>
         </div>
         {/if}

      </section>
      <footer class="row justify-content-center form-footer p-0">
      <div class="col-md-8">
         <style>
            input[name=url] {
            display: none !important;
            }
         </style>
         <input type="text" name="url" value=""/>
         <input type="hidden" name="token" value="{$token}" />
         <input class="btn  btn_contact" type="submit" name="submitMessage"
            value="{l s='Send' d='Shop.Theme.Actions'}">
            </div>
      </footer>
      </form>
   </section>
</div>