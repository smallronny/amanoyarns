{*

* 2007-2017 PrestaShop

*

* NOTICE OF LICENSE

*

* This source file is subject to the Academic Free License (AFL 3.0)

* that is bundled with this package in the file LICENSE.txt.

* It is also available through the world-wide-web at this URL:

* http://opensource.org/licenses/afl-3.0.php

* If you did not receive a copy of the license and are unable to

* obtain it through the world-wide-web, please send an email

* to license@prestashop.com so we can send you a copy immediately.

*

* DISCLAIMER

*

* Do not edit or add to this file if you wish to upgrade PrestaShop to newer

* versions in the future. If you wish to customize PrestaShop for your

* needs please refer to http://www.prestashop.com for more information.

*

*  @author PrestaShop SA <contact@prestashop.com>

*  @copyright  2007-2017 PrestaShop SA

*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)

*  International Registered Trademark & Property of PrestaShop SA

*}




<div class="col col-md pb-5 ps-emailsubscription-block">
 
     <h5 class="block-title"><span>{l s='Suscríbete' d='Shop.Forms.Labels'}</span></h5>
    <label class="col-auto "><span>{l s='Subscribe to our newsletter 
        for updates and special offers' d='Shop.Forms.Labels'}</span>
    </label>
    


    <form action="{url entity=index params=['fc' => 'module', 'module' => 'iqitemailsubscriptionconf', 'controller' => 'subscription']}" method="post">

               <div class="input-group newsletter-input-group inpt_newsletter_amano">

                    <input

                        name="email"

                        type="email"

                        value="{$value}"

                        class="form-control input-subscription"

                        placeholder="{l s='E-mail *' d='Shop.Forms.Labels'}"

                        aria-label="{l s='Your email address' d='Shop.Forms.Labels'}"

                    >

                </div>
                <div class="pt-4">
                    <button class="btn btn-primary btn_newsletter text-white" name="submitNewsletter" type="submit" aria-label="{l s='Subscribe' d='Shop.Theme.Actions'}">{l s='SUBSCRIBE' d='Shop.Forms.Labels'} </button>
                </div>

        {if isset($id_module)}

            <div class="mt-2 text-muted"> {hook h='displayGDPRConsent' id_module=$id_module}</div>

        {/if}

                <input type="hidden" name="action" value="0">

    </form>

</div>









