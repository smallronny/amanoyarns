<?php

class FrontController extends FrontControllerCore
{
    /*
    * module: ets_cfultimate
    * date: 2020-09-22 23:03:33
    * version: 1.0.8
    */
    public function smartyOutputContent($content)
    {
        if (version_compare(_PS_VERSION_, '1.7.0', '<')) {
            ob_start();
            parent::smartyOutputContent($content);
            $html = ob_get_contents();
            ob_clean();
            Hook::exec('actionOutputHTMLBefore', array('html' => &$html));
            echo $html;
        } else
            return parent::smartyOutputContent($content);
    }
    
    /*
    * module: kburlcleaner
    * date: 2021-03-01 21:21:53
    * version: 1.0.2
    */
    public function init()
    {
        
        parent::init();
        $knowbandUrlConfiguration = Tools::jsonDecode(Configuration::get('KB_URLCLEANER_SETTINGS'), true);
        if ($knowbandUrlConfiguration['enable'] == 1) {
            $uri_var = $_SERVER['REQUEST_URI'];
            $isRedirectExist = Db::getInstance()->getRow("SELECT * FROM " . _DB_PREFIX_ . "kb_seo_redirect_url where active = 1 and old_url = '" . pSQL($uri_var) . "'");
            if ($isRedirectExist !== false) {
                if ($uri_var == $isRedirectExist['old_url']) {
                    if ($isRedirectExist['redirect_type'] == 301) {
                        header("HTTP/1.1 301 Moved Permanently");
                        header('Cache-Control: no-cache');
                    }
                    if ($isRedirectExist['redirect_type'] == 302) {
                        header("HTTP/1.1 302 Moved Temporarily");
                        header('Cache-Control: no-cache');
                    }
                    if ($isRedirectExist['redirect_type'] == 303) {
                        header("HTTP/1.1 303 See Other");
                        header('Cache-Control: no-cache');
                    }
                    header('Cache-Control: no-cache');
                    Tools::redirect($isRedirectExist['new_url']);
                    exit;
                }
            }
            if (($knowbandUrlConfiguration['kb_url_iso'] == 1) && (Configuration::get('PS_LANG_DEFAULT') != $this->context->language->id)) {
                $useSSL = true;
                $protocol_content = (isset($useSSL) && $useSSL && Configuration::get('PS_SSL_ENABLED')) ? 'https://' : 'http://';
                $protocol_link = (Configuration::get('PS_SSL_ENABLED') || Tools::usingSecureMode()) ? 'https://' : 'http://';
                $iso = Language::getIsoById($this->context->language->id) . '/';
                $this->context->smarty->assign(array(
                    'base_dir' => _PS_BASE_URL_ . __PS_BASE_URI__ . $iso,
                    'base_dir_ssl' => $protocol_link . Tools::getShopDomainSsl() . __PS_BASE_URI__ . $iso,
                    'content_dir' => $protocol_content . Tools::getHttpHost() . __PS_BASE_URI__ . $iso,
                    'base_uri' => $protocol_content . Tools::getHttpHost() . __PS_BASE_URI__ . $iso . (!Configuration::get('PS_REWRITING_SETTINGS') ? 'index.php' : ''),
                ));
            }
        }
    }
}
