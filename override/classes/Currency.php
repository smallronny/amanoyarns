<?php
use PrestaShop\PrestaShop\Core\Localization\CLDR\LocaleRepository;
class Currency extends CurrencyCore
{
    /*
    * module: progeo
    * date: 2020-10-09 16:30:42
    * version: 1.3.0
    */
    public static function getCurrencies($object = false, $active = true, $groupBy = false)
    {
        $currencies_to_return = array();
        if ((version_compare(_PS_VERSION_, '1.7.6') >= 0)) {
            $currencies = static::addCldrDatasToCurrency(
                static::findAll($active, $groupBy),
                $object
            );
            $rule = false;
            $currencies_to_return = array();
            $geo_currencies = array();
            $progeo_exists = false;
            if ((!defined('_PS_ADMIN_DIR_') || Context::getContext()->controller->controller_type == 'front')) {
                $progeo = @Module::getInstanceByName('progeo');
                $geolocation = $progeo::returnUserCountry();
                if ($geolocation != false) {
                    $rule = georule::getByCountry(strtoupper($geolocation));
                    if ($rule != false && $progeo->active == 1) {
                        $geo_currencies = $rule['currencies'];
                        $progeo_exists = true;
                    }
                }
            }
            if (count($geo_currencies) > 0 && $progeo_exists == true) {
                foreach ($currencies AS $kc => $vc) {
                    if (in_array($vc->id, $geo_currencies)) {
                        $currencies_to_return[] = $vc;
                    }
                }
            }
            return (count($currencies_to_return) > 0 ? $currencies_to_return : $currencies);
        } else {
            $currencies = Db::getInstance()->executeS('
    		SELECT *
    		FROM `' . _DB_PREFIX_ . 'currency` c
    		' . Shop::addSqlAssociation('currency', 'c') .
                ' WHERE `deleted` = 0' .
                ($active ? ' AND c.`active` = 1' : '') .
                ($groupBy ? ' GROUP BY c.`id_currency`' : '') .
                ' ORDER BY `iso_code` ASC');
            $rule = false;
            $currencies_to_return = array();
            $geo_currencies = array();
            $progeo_exists = false;
            if ((!defined('_PS_ADMIN_DIR_') || Context::getContext()->controller->controller_type == 'front')) {
                $progeo = @Module::getInstanceByName('progeo');
                $geolocation = $progeo::returnUserCountry();
                if ($geolocation != false) {
                    $rule = georule::getByCountry(strtoupper($geolocation));
                    if ($rule != false && $progeo->active == 1) {
                        $geo_currencies = $rule['currencies'];
                        $progeo_exists = true;
                    }
                }
            }
            if (count($geo_currencies) > 0 && $progeo_exists == true) {
                foreach ($currencies AS $kc => $vc) {
                    if ($object) {
                        if (in_array($vc->id_currency, $geo_currencies)) {
                            $currencies_to_return[] = $vc;
                        }
                    } else {
                        if (in_array($vc['id_currency'], $geo_currencies)) {
                            $currencies_to_return[] = $vc;
                        }
                    }
                }
            }
            return self::addCldrDatasToCurrency((count($currencies_to_return) > 0 ? $currencies_to_return : $currencies), $object);
        }
    }
}
?>