<?php
use PrestaShop\PrestaShop\Core\Addon\Theme\ThemeManagerBuilder;
use PrestaShop\PrestaShop\Core\Cldr\Repository as cldrRepository;
use PrestaShop\PrestaShop\Core\Localization\RTL\Processor as RtlStylesheetProcessor;
use PrestaShopBundle\Translation\Translator;
use Symfony\Component\Filesystem\Filesystem;
class Language extends LanguageCore
{
    /*
    * module: progeo
    * date: 2020-10-09 16:30:43
    * version: 1.3.0
    */
    public static function getLanguageCodeByIso($iso_code)
    {
        if ((!defined('_PS_ADMIN_DIR_') || Context::getContext()->controller->controller_type == 'front') && $iso_code == false) {
            $progeo = @Module::getInstanceByName('progeo');
            if ($progeo->active == 1) {
                $geolocation = $progeo::returnUserCountry();
                $rule = georule::getByCountry(strtoupper($geolocation));
                if ($rule != false) {
                    $default_language = new Language($rule['id_language']);
                    $iso_code = $default_language->iso_code;
                }
            }
        }
        return parent::getLanguageCodeByIso($iso_code);
    }
    /*
    * module: progeo
    * date: 2020-10-09 16:30:43
    * version: 1.3.0
    */
    public static function getLanguages($active = true, $id_shop = false, $ids_only = false)
    {
        if (!self::$_LANGUAGES) {
            Language::loadLanguages();
        }
        $rule = false;
        $geo_languages = array();
        $geo_default_language = false;
        $progeo_exists = false;
        if ((!defined('_PS_ADMIN_DIR_') || Context::getContext()->controller->controller_type == 'front')) {
            $progeo = @Module::getInstanceByName('progeo');
            $geolocation = $progeo::returnUserCountry();
            if ($geolocation != false) {
                $rule = georule::getByCountry(strtoupper($geolocation));
                if ($rule != false) {
                    $geo_languages = $rule['languages'];
                    $geo_default_language = $rule['id_language'];
                    $progeo_exists = true;
                }
            }
        }
        $languages = array();
        foreach (self::$_LANGUAGES as $language) {
            if (count($geo_languages) > 0 && $progeo_exists == true) {
                if (!in_array($language['id_lang'], $geo_languages)) {
                    if (isset(self::$_LANGUAGES[$language['id_lang']])) {
                        unset(self::$_LANGUAGES[$language['id_lang']]);
                    }
                    continue;
                }
            }
            if ($active && !$language['active'] || ($id_shop && !isset($language['shops'][(int)$id_shop])) || self::$locale_crowdin_lang === $language['locale']) {
                continue;
            }
            $languages[] = $ids_only ? $language['id_lang'] : $language;
        }
        return $languages;
    }
}
?>
