<?php
/**
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 * We offer the best and most useful modules PrestaShop and modifications for your online store.
 *
 * @author    knowband.com <support@knowband.com>
 * @copyright 2015 knowband
 * @license   see file: LICENSE.txt
 * @category  PrestaShop Module
 */
class Link extends LinkCore
{
    /**
     * Create a link to a category
     *
     * @param mixed $category Category object (can be an ID category, but deprecated)
     * @param string $alias
     * @param int $id_lang
     * @param string $selected_filters Url parameter to autocheck filters of the module blocklayered
     * @return string
     */
    /*
    * module: kburlcleaner
    * date: 2021-03-01 21:21:53
    * version: 1.0.2
    */
    public function getCategoryLink($category, $alias = null, $id_lang = null, $selected_filters = null, $id_shop = null, $relative_protocol = false)
    {
        $knowbandUrlConfiguration = Tools::jsonDecode(Configuration::get('KB_URLCLEANER_SETTINGS'), true);
        if (($knowbandUrlConfiguration['enable'] == 1) && ($knowbandUrlConfiguration['kb_url_category'] == 1)) {
            if (!$id_lang) {
                $id_lang = Context::getContext()->language->id;
            }
            $url = $this->getBaseLink($id_shop, null, $relative_protocol) . $this->getLangLink($id_lang, null, $id_shop);
            if (!is_object($category)) {
                $category = new Category($category, $id_lang);
            }
            $params = array();
            $params['id'] = $category->id;
            $params['rewrite'] = (!$alias) ? $category->link_rewrite : $alias;
            $params['meta_keywords'] = Tools::str2url($category->getFieldByLang('meta_keywords'));
            $params['meta_title'] = Tools::str2url($category->getFieldByLang('meta_title'));
            $parentCategoryList = array();
            foreach ($category->getParentsCategories($id_lang) as $cat) {
                if (!in_array($cat['id_category'], array(
                        Configuration::get('PS_HOME_CATEGORY'),
                        Configuration::get('PS_ROOT_CATEGORY'),
                        $category->id))) {
                    $parentCategoryList[] = $cat['link_rewrite'];
                }
            }
            $params['parents'] = implode('/', array_reverse($parentCategoryList));
            $selected_filters = is_null($selected_filters) ? '' : $selected_filters;
            if (empty($selected_filters)) {
                $rule = 'category_rule';
            } else {
                $rule = 'layered_rule';
                $params['selected_filters'] = $selected_filters;
            }
            return $url . Dispatcher::getInstance()->createUrl($rule, $id_lang, $params, $this->allow, '', $id_shop);
        } else {
            return parent::getCategoryLink($category, $alias, $id_lang, $selected_filters, $id_shop, $relative_protocol);
        }
    }
    /*
    * module: kburlcleaner
    * date: 2021-03-01 21:21:53
    * version: 1.0.2
    */
    protected function getLangLink($id_lang = null, Context $context = null, $id_shop = null)
    {
        $knowbandUrlConfiguration = Tools::jsonDecode(Configuration::get('KB_URLCLEANER_SETTINGS'), true);
        if (($knowbandUrlConfiguration['enable'] == 1) && ($knowbandUrlConfiguration['kb_url_iso'] == 1) && (Configuration::get('PS_LANG_DEFAULT') == $id_lang)) {
            return;
        } else {
            return parent::getLangLink($id_lang, $context, $id_shop);
        }
    }
    
    /*
    * module: kburlcleaner
    * date: 2021-03-01 21:21:53
    * version: 1.0.2
    */
    public function getProductLink(
        $product,
        $alias = null,
        $category = null,
        $ean13 = null,
        $idLang = null,
        $idShop = null,
        $ipa = null,
        $force_routes = false,
        $relativeProtocol = false,
        $addAnchor = false,
        $extraParams = array()
    ) {
        $knowbandUrlConfiguration = Tools::jsonDecode(Configuration::get('KB_URLCLEANER_SETTINGS'), true);
        if ($knowbandUrlConfiguration['enable'] == 1 && $knowbandUrlConfiguration['kb_url_product'] == 1) {
            $dispatcher = Dispatcher::getInstance();
            if (!$idLang) {
                $idLang = Context::getContext()->language->id;
            }
            $url = $this->getBaseLink($idShop, null, $relativeProtocol) . $this->getLangLink($idLang, null, $idShop);
            $params = array();
            if (!is_object($product)) {
                if (is_array($product) && isset($product['id_product'])) {
                    $params['id'] = $product['id_product'];
                } elseif ((int) $product) {
                    $params['id'] = $product;
                } else {
                    throw new PrestaShopException('Invalid product vars');
                }
            } else {
                $params['id'] = $product->id;
            }
            if (empty($ipa)) {
                $ipa = null;
            }
            $ipa = null;
            $params['id_product_attribute'] = $ipa;
            if (!$alias) {
                $product = $this->getProductObject($product, $idLang, $idShop);
            }
            $params['rewrite'] = (!$alias) ? $product->getFieldByLang('link_rewrite') : $alias;
            if (!$ean13) {
                $product = $this->getProductObject($product, $idLang, $idShop);
            }
            $params['ean13'] = (!$ean13) ? $product->ean13 : $ean13;
            if ($dispatcher->hasKeyword('product_rule', $idLang, 'meta_keywords', $idShop)) {
                $product = $this->getProductObject($product, $idLang, $idShop);
                $params['meta_keywords'] = Tools::str2url($product->getFieldByLang('meta_keywords'));
            }
            if ($dispatcher->hasKeyword('product_rule', $idLang, 'meta_title', $idShop)) {
                $product = $this->getProductObject($product, $idLang, $idShop);
                $params['meta_title'] = Tools::str2url($product->getFieldByLang('meta_title'));
            }
            if ($dispatcher->hasKeyword('product_rule', $idLang, 'manufacturer', $idShop)) {
                $product = $this->getProductObject($product, $idLang, $idShop);
                $params['manufacturer'] = Tools::str2url($product->isFullyLoaded ? $product->manufacturer_name : Manufacturer::getNameById($product->id_manufacturer));
            }
            if ($dispatcher->hasKeyword('product_rule', $idLang, 'supplier', $idShop)) {
                $product = $this->getProductObject($product, $idLang, $idShop);
                $params['supplier'] = Tools::str2url($product->isFullyLoaded ? $product->supplier_name : Supplier::getNameById($product->id_supplier));
            }
            if ($dispatcher->hasKeyword('product_rule', $idLang, 'price', $idShop)) {
                $product = $this->getProductObject($product, $idLang, $idShop);
                $params['price'] = $product->isFullyLoaded ? $product->price : Product::getPriceStatic($product->id, false, null, 6, null, false, true, 1, false, null, null, null, $product->specificPrice);
            }
            if ($dispatcher->hasKeyword('product_rule', $idLang, 'tags', $idShop)) {
                $product = $this->getProductObject($product, $idLang, $idShop);
                $params['tags'] = Tools::str2url($product->getTags($idLang));
            }
            if ($dispatcher->hasKeyword('product_rule', $idLang, 'category', $idShop)) {
                if (!$category) {
                    $product = $this->getProductObject($product, $idLang, $idShop);
                }
                $params['category'] = (!$category) ? $product->category : $category;
            }
            if ($dispatcher->hasKeyword('product_rule', $idLang, 'reference', $idShop)) {
                $product = $this->getProductObject($product, $idLang, $idShop);
                $params['reference'] = Tools::str2url($product->reference);
            }
            if ($dispatcher->hasKeyword('product_rule', $idLang, 'categories', $idShop)) {
                $product = $this->getProductObject($product, $idLang, $idShop);
                $params['category'] = (!$category) ? $product->category : $category;
                $cats = array();
                foreach ($product->getParentCategories($idLang) as $cat) {
                    if (!in_array($cat['id_category'], Link::$category_disable_rewrite)) {
                        $cats[] = $cat['link_rewrite'];
                    }
                }
                $params['categories'] = implode('/', $cats);
            }
            if ($ipa) {
                $product = $this->getProductObject($product, $idLang, $idShop);
            }
            $anchor = $ipa ? $product->getAnchor((int) $ipa, (bool) $addAnchor) : '';
            $ress = $url . $dispatcher->createUrl('product_rule', $idLang, array_merge($params, $extraParams), $force_routes, $anchor, $idShop);
            if (Tools::substr($ress, Tools::strlen($ress) - 1, 1) == "?") {
                return Tools::substr($ress, 0, Tools::strlen($ress) - 1);
            }
            return $ress;
        } else {
            return parent::getProductLink($product, $alias, $category, $ean13, $idLang, $idShop, $ipa, $force_routes, $relativeProtocol, $addAnchor, $extraParams);
        }
    }
}
