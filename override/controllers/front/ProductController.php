<?php
class ProductController extends ProductControllerCore
{
    
    /*
    * module: hideprice
    * date: 2020-10-28 12:25:06
    * version: 1.1.1
    */
    public function initContent()
    {
        if (Module::isEnabled('hideprice')) {
            include_once(_PS_MODULE_DIR_.'hideprice/classes/HidePriceConfiguration.php');
            $this->product = HidepriceConfiguration::changeProductProperties($this->product);
        }
        return parent::initContent();
    }
}
