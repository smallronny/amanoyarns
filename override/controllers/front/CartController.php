<?php
/**
* Hide prices and disallow purchase of products
*
* NOTICE OF LICENSE
*
* This product is licensed for one customer to use on one installation (test stores and multishop included).
* Site developer has the right to modify this module to suit their needs, but can not redistribute the module in
* whole or in part. Any other use of this module constitues a violation of the user agreement.
*
* DISCLAIMER
*
* NO WARRANTIES OF DATA SAFETY OR MODULE SECURITY
* ARE EXPRESSED OR IMPLIED. USE THIS MODULE IN ACCORDANCE
* WITH YOUR MERCHANT AGREEMENT, KNOWING THAT VIOLATIONS OF
* PCI COMPLIANCY OR A DATA BREACH CAN COST THOUSANDS OF DOLLARS
* IN FINES AND DAMAGE A STORES REPUTATION. USE AT YOUR OWN RISK.
*
*  @author    idnovate
*  @copyright 2019 idnovate
*  @license   See above
*/
class CartController extends CartControllerCore
{
    /*
    * module: hideprice
    * date: 2020-10-28 12:25:06
    * version: 1.1.1
    */
    protected function processChangeProductInCart()
    {
        if (Module::isEnabled('hideprice')) {
            include_once(_PS_MODULE_DIR_.'hideprice/hideprice.php');
            $mod = new HidepriceConfiguration();
            $errors = $mod->checkProductsAvailability($this->id_product);
            if (!empty($errors)) {
                foreach ($errors as $error) {
                    $this->errors[] = $error;
                }
            }
        }
        return parent::processChangeProductInCart();
    }
	/*
    * module: hideprice
    * date: 2020-10-28 12:25:06
    * version: 1.1.1
    */
    public function init()
    {
        parent::init();
        if (Module::isEnabled('hideprice')) {
            include_once(_PS_MODULE_DIR_.'hideprice/hideprice.php');
            $mod = new HidepriceConfiguration();
            if (!Tools::isSubmit('delete')) {
                $errors = $mod->checkProductsAvailability($this->context->cart->getProducts());
                if (!empty($errors)) {
                    foreach ($errors as $error) {
                        $this->errors[] = $error;
                    }
                }
            }
            if (!empty($this->errors)) {
                $params = array('action' => 'show');
                $this->canonicalRedirection($this->context->link->getPageLink('cart', true, (int)$this->context->language->id, $params));
            }
        }
    }
}
