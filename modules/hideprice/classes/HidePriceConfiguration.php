<?php
/**
* Hide prices by categories, groups and more
*
* NOTICE OF LICENSE
*
* This product is licensed for one customer to use on one installation (test stores and multishop included).
* Site developer has the right to modify this module to suit their needs, but can not redistribute the module in
* whole or in part. Any other use of this module constitues a violation of the user agreement.
*
* DISCLAIMER
*
* NO WARRANTIES OF DATA SAFETY OR MODULE SECURITY
* ARE EXPRESSED OR IMPLIED. USE THIS MODULE IN ACCORDANCE
* WITH YOUR MERCHANT AGREEMENT, KNOWING THAT VIOLATIONS OF
* PCI COMPLIANCY OR A DATA BREACH CAN COST THOUSANDS OF DOLLARS
* IN FINES AND DAMAGE A STORES REPUTATION. USE AT YOUR OWN RISK.
*
*  @author    idnovate
*  @copyright 2019 idnovate
*  @license   See above
*/

class HidePriceConfiguration extends ObjectModel
{
    public $id_hideprice_configuration;
    public $name;
    public $hide_price = true;
    public $hide_price_status;
    public $disallow_purchase = false;
    public $groups;
    public $customers;
    public $countries;
    public $zones;
    public $categories;
    public $products;
    public $manufacturers;
    public $suppliers;
    public $currencies;
    public $languages;
    public $active = true;
    public $priority;
    public $id_shop;
    public $date_add;
    public $date_upd;
    public $date_from;
    public $date_to;
    public $features;
    public $attributes;
    public $price_calculate;
    public $min_price;
    public $max_price;
    public $filter_prices;
    public $filter_stock;
    public $filter_store;
    public $show_text;
    public $alternative_text;
    public $min_stock;
    public $max_stock;
    public $filter_weight;
    public $min_weight;
    public $max_weight;
    public $schedule;
    public $products_excluded;
    public $customers_excluded;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'hideprice_configuration',
        'primary' => 'id_hideprice_configuration',
        'multilang' => true,
        'fields' => array(
            'name' =>               array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 100),
            'hide_price' =>         array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
            'hide_price_status' =>  array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
            'disallow_purchase' =>  array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
            'show_text' =>          array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
            'alternative_text' =>   array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isAnything'),
            'groups' =>             array('type' => self::TYPE_STRING),
            'countries' =>          array('type' => self::TYPE_STRING),
            'products' =>           array('type' => self::TYPE_STRING),
            'customers' =>          array('type' => self::TYPE_STRING),
            'zones' =>              array('type' => self::TYPE_STRING),
            'categories' =>         array('type' => self::TYPE_STRING),
            'manufacturers' =>      array('type' => self::TYPE_STRING),
            'currencies' =>         array('type' => self::TYPE_STRING),
            'languages' =>          array('type' => self::TYPE_STRING),
            'suppliers' =>          array('type' => self::TYPE_STRING),
            'features' =>           array('type' => self::TYPE_STRING),
            'attributes' =>         array('type' => self::TYPE_STRING),
            'active' =>             array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
            'priority' =>           array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'copy_post' => false),
            'id_shop' =>            array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'copy_post' => false),
            'date_add' =>           array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'copy_post' => false),
            'date_upd' =>           array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'copy_post' => false),
            'date_from' =>          array('type' => self::TYPE_DATE, 'copy_post' => false),
            'date_to' =>            array('type' => self::TYPE_DATE, 'copy_post' => false),
            'price_calculate' =>    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'copy_post' => false),
            'filter_prices' =>      array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'copy_post' => false),
            'filter_store' =>       array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'copy_post' => false),
            'min_price' =>          array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'max_price' =>          array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'filter_stock' =>       array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'copy_post' => false),
            'min_stock' =>          array('type' => self::TYPE_INT, 'copy_post' => false),
            'max_stock' =>          array('type' => self::TYPE_INT, 'copy_post' => false),
            'min_weight' =>         array('type' => self::TYPE_FLOAT, 'copy_post' => false),
            'max_weight' =>         array('type' => self::TYPE_FLOAT, 'copy_post' => false),
            'filter_weight' =>      array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'copy_post' => false),
            'schedule' =>           array('type' => self::TYPE_STRING),
            'products_excluded' =>  array('type' => self::TYPE_STRING),
            'customers_excluded' => array('type' => self::TYPE_STRING),
        ),
    );

    public function __construct($id = null, $id_lang = null)
    {
        parent::__construct($id, $id_lang);
    }

    public function add($autodate = true, $null_values = true)
    {
        $this->id_shop = ($this->id_shop) ? $this->id_shop : Context::getContext()->shop->id;
        $success = parent::add($autodate, $null_values);
        return $success;
    }

    public function toggleStatus()
    {
        parent::toggleStatus();
        return Db::getInstance()->execute('
        UPDATE `'._DB_PREFIX_.bqSQL($this->def['table']).'`
        SET `date_upd` = NOW()
        WHERE `'.bqSQL($this->def['primary']).'` = '.(int)$this->id);
    }

    public function delete()
    {
        if (parent::delete()) {
            return $this->deleteImage();
        }
    }

    public static function getConfigurations($id_product = 0, $id_product_attribute = 0)
    {
        $context = Context::getContext();

        $id_lang = $context->language->id;
        $id_shop = $context->shop->id;

        if (isset($context->currency) && !empty($context->currency)) {
            $id_currency = $context->currency->id;
        }

        $id_customer = 0;
        if (isset($context->customer) && !empty($context->customer)) {
            $id_customer = $context->customer->id;
        }

        $id_country = 0;
        $id_state = 0;

        if (isset($context->cart) && !empty($context->cart)) {
            $id_address_delivery = $context->cart->id_address_delivery;
            $address = new Address($id_address_delivery);
            $id_country = $address->id_country;

            if ($id_country == 0) {
                $id_country = $context->country->id;
            }

            $id_state = $address->id_state;
        }

        $query = '';
        $today = date("Y-m-d H:i:s");

        $query = '
                SELECT h.* FROM `'._DB_PREFIX_.'hideprice_configuration` h WHERE h.`id_shop` = '.(int)$id_shop.'
                    AND h.`active` = 1 AND (date_from <= "'.pSQL($today). '" OR date_from <= "1970-01-01 00:00:01") AND (date_to >= "'.pSQL($today).'" OR date_to <= "1970-01-01 00:00:01")';

        $configs = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($query);
        if ($configs === false) {
            return false;
        }

        $configs_priority = array();
        foreach ($configs as $key => $row) {
            $configs_priority[$key] = $row['priority'];
        }
        array_multisort($configs_priority, SORT_ASC, $configs);

        $customer = new Customer($id_customer);
        $customer_groups = $customer->getGroupsStatic($customer->id);

        $country = new Country($id_country);
        $zone = 0;
        if ($id_state > 0) {
            $zone = State::getIdZone($id_state);
        } else if ($id_country != null and $id_country > 0) {
            $zone = $country->getIdZone($id_country);
        }

        $array_confs_result = array();
        $product = new Product($id_product);
        $array_configurations_result = array();
        $categories = Product::getProductCategories($id_product);
        $product = new Product($id_product);
        $id_manufacturer = $product->id_manufacturer;
        $product_suppliers_array = ProductSupplier::getSupplierCollection($id_product);



        foreach ($configs as $conf) {
            if (!HidepriceConfiguration::checkExceptions($conf, $id_product, $id_customer)) {
                continue;
            }

            if (!HidepriceConfiguration::isShowableBySchedule($conf)) {
                continue;
            }

            if (!HidepriceConfiguration::checkStockPriceWeight($conf, $id_product)) {
                continue;
            }

            if ($conf['attributes'] == '' && $conf['features'] == '' && $conf['currencies'] == '' && $conf['languages'] == '' && $conf['groups'] == '' && $conf['products'] == '' && $conf['customers'] == '' && $conf['countries'] == ''
                && $conf['zones'] == '' && $conf['categories'] == '' && $conf['manufacturers'] == '' && $conf['suppliers'] == '') {
                return $conf;
            }

            $filter_features = false;
            $array_features_selected = Tools::jsonDecode($conf['features'], true);
            $product_features = Product::getFeaturesStatic((int)$id_product);

            $flag_features = 0;
            if (!empty($array_features_selected) && count($array_features_selected) > 0) {
                foreach ($product_features as $pf) {
                    if (isset($array_features_selected[$pf['id_feature']])) {
                        $array_f = explode(";", $array_features_selected[$pf['id_feature']]);
                        if (in_array($pf['id_feature_value'], $array_f)) {
                            $flag_features++;
                            continue;
                        }
                    }
                }
            } else {
                $filter_features = true;
            }

            if ($flag_features > 0) {
                $filter_features = true;
            }

            $filter_currencies = true;
            if ($conf['currencies'] !== '') {
                $currencies_array = explode(';', $conf['currencies']);
                if (!in_array($id_currency, $currencies_array)) {
                    $filter_currencies = false;
                }
            }
            $filter_languages = true;
            if ($conf['languages'] !== '') {
                $languages_array = explode(';', $conf['languages']);
                if (!in_array($id_lang, $languages_array)) {
                    $filter_languages = false;
                }
            }

            $filter_groups = true;
            $filter_customers = true;
            if ($conf['groups'] !== '' && $conf['customers'] == '') {
                $groups_array = explode(';', $conf['groups']);
                foreach ($customer_groups as $group) {
                    if (!in_array($group, $groups_array)) {
                        $filter_groups = false;
                    } else {
                        $filter_groups = true;
                        break;
                    }
                }
                if (!$filter_groups) {
                    $filter_customers = false;
                }
            } else if ($conf['groups'] == '' && $conf['customers'] !== '') {
                $customers_array = explode(';', $conf['customers']);
                if (!in_array($id_customer, $customers_array)) {
                    $filter_customers = false;
                }
            } else if ($conf['groups'] !== '' && $conf['customers'] !== '') {
                $groups_array = explode(';', $conf['groups']);
                foreach ($customer_groups as $group) {
                    if (!in_array($group, $groups_array)) {
                        $filter_groups = false;
                    } else {
                        $filter_groups = true;
                    }
                }
                if (!$filter_groups) {
                    $customers_array = explode(';', $conf['customers']);
                    if (!in_array($id_customer, $customers_array)) {
                        $filter_customers = false;
                    } else {
                        $filter_customers = true;
                    }
                } else {
                    $customers_array = explode(';', $conf['customers']);
                    if (!in_array($id_customer, $customers_array)) {
                        $filter_customers = false;
                    }
                }
            }
            $filter_countries = true;
            if ($conf['countries'] !== '') {
                $countries_array = explode(';', $conf['countries']);

                if (!in_array($country->id, $countries_array)) {
                    $filter_countries = false;
                }
            }

            $filter_zones = true;
            if ($conf['zones'] !== '') {
                $zones_array = explode(';', $conf['zones']);
                if (!in_array($zone, $zones_array)) {
                    $filter_zones = false;
                }
            }
            $filter_categories = true;
            $filter_products = true;

            if (@unserialize($conf['categories']) !== false) {
                $categories_array = unserialize($conf['categories']);
            } else {
                $categories_array = explode(';', $conf['categories']);
            }

            if ($conf['categories'] !== '' && $conf['products'] == '') {
                foreach ($categories as $category) {
                    if (in_array($category, $categories_array)) {
                        $filter_categories = true;
                        $filter_products = true;
                        break;
                    } else {
                        $filter_categories = false;
                    }
                }
                if (!$filter_categories) {
                    $filter_products = false;
                }
            } else if ($conf['categories'] == '' && $conf['products'] !== '') {

                $products_array = explode(';', $conf['products']);
                if (!in_array($id_product, $products_array)) {
                    $filter_products = false;
                    $filter_categories = true;
                }
            } else if ($conf['categories'] !== '' && $conf['products'] !== '') {
                foreach ($categories as $category) {
                    if (!in_array($category, $categories_array)) {
                        $filter_categories = false;
                    } else {
                        $filter_categories = true;
                        break;
                    }
                }
                if (!$filter_categories) {
                    $products_array = explode(';', $conf['products']);
                    if (!in_array($id_product, $products_array)) {
                        $filter_products = false;
                    } else {
                        $filter_products = true;
                    }
                } else {
                    $products_array = explode(';', $conf['products']);
                    if (!in_array($id_product, $products_array)) {
                        $filter_products = false;
                    }
                }
            }

            $filter_manufacturers = true;
            if ($conf['manufacturers'] !== '') {
                $manufacturers_array = explode(';', $conf['manufacturers']);
                if (!in_array($id_manufacturer, $manufacturers_array)) {
                    $filter_manufacturers = false;
                }
            }
            $filter_suppliers = true;
            if ($conf['suppliers'] !== '') {
                $filter_suppliers = false;
                $suppliers_array = explode(';', $conf['suppliers']);
                if (!empty($product_suppliers_array)) {
                    foreach ($product_suppliers_array as $ps) {
                        if (in_array($ps->id_supplier, $suppliers_array)) {
                            $filter_suppliers = true;
                            break;
                        }
                    }
                }
            }
/*
$logger->logDebug("filter_groups: ".print_r($filter_groups, true));
$logger->logDebug("filter_customers: ".print_r($filter_customers, true));
$logger->logDebug("filter_countries: ".print_r($filter_countries, true));
$logger->logDebug("filter_zones: ".print_r($filter_zones, true));
$logger->logDebug("filter_categories: ".print_r($filter_categories, true));
$logger->logDebug("filter_products: ".print_r($filter_products, true));
$logger->logDebug("filter_manufacturers: ".print_r($filter_manufacturers, true));
$logger->logDebug("filter_suppliers: ".print_r($filter_suppliers, true));
$logger->logDebug("filter_attributes: ".print_r($filter_attributes, true));
$logger->logDebug("filter_features: ".print_r($filter_features, true));
$logger->logDebug("filter_currencies: ".print_r($filter_currencies, true));
$logger->logDebug("filter_languages: ".print_r($filter_languages, true));
*/
            if ($filter_currencies && $filter_languages && $filter_features && $filter_groups && $filter_customers && $filter_countries
                && $filter_zones && $filter_categories && $filter_products && $filter_manufacturers && $filter_suppliers) {
                    return $conf;
            }
        }

        return false;
    }

    public static function isShowableBySchedule($conf)
    {
        $schedule = Tools::jsonDecode($conf['schedule']);
        $dayOfWeek = date('w') - 1;
        if ($dayOfWeek < 0) {
            $dayOfWeek = 6;
        }
        if (is_array($schedule)) {
            if (is_object($schedule[$dayOfWeek]) && $schedule[$dayOfWeek]->isActive === true) {
                if ($schedule[$dayOfWeek]->timeFrom <= date('H:i') && $schedule[$dayOfWeek]->timeTill > date('H:i')) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    public static function checkStockPriceWeight($conf, $id_product = 0, $id_product_attribute = 0)
    {
        $product = new Product($id_product);
        if ($conf['filter_stock']) {
            if (!$product->hasAttributes()) {
                $stock = Product::getQuantity($id_product);
            } else if ($conf['attributes'] != '') {
                $stock = StockAvailable::getQuantityAvailableByProduct($id_product, $id_product_attribute);
            } else {
                $stock = Product::getQuantity($id_product);
            }

            if (($conf['max_stock'] > 0 && $conf['min_stock'] > 0) || ($conf['max_stock'] > 0 && $conf['min_stock'] <= 0)) {
                if ((int)$stock < $conf['min_stock'] || (int)$stock > $conf['max_stock']) {
                    return false;
                }
            } else if ($conf['max_stock'] <= 0 && $conf['min_stock'] <= 0) {
                if ((int)$stock > 0) {
                    return false;
                } else if ($conf['hide_price_status']) {
                    $out_of_stock = Product::isAvailableWhenOutOfStock((int)StockAvailable::outOfStock($id_product));
                    if ($out_of_stock) {
                        return false;
                    }
                }
            }
        }

        if ($conf['filter_weight']) {
            $weight = $product->weight;
            if ($product->hasAttributes()) {
                $combination = new Combination($id_product_attribute);
                $weight += $combination->weight;
            }

            if ($weight < $conf['min_weight'] || ($conf['max_weight'] > 0 && $weight > $conf['max_weight'])) {
                return false;
            }
        }

        if ($conf['filter_prices']) {
            $price = Product::getPriceStatic((int)$id_product, false, 0, 6, null, false, false, 1, false, Context::getContext()->customer->id);
            $price_withtax = Product::getPriceStatic((int)$id_product, true, 0, 6, null, false, false, 1, false, Context::getContext()->customer->id);

            if ($conf['price_calculate'] == 0) {
                $price_to_compare = $product->wholesale_price;
            } else if ($conf['price_calculate'] == 1) {
                $price_to_compare = $price;
            } else if ($conf['price_calculate'] == 2) {
                $price_to_compare = $product->wholesale_price;
            } else if ($conf['price_calculate'] == 3) {
                $price_to_compare = $price_withtax;
            }

            if ((float)$conf['max_price'] == 0 && (float)$conf['min_price'] == 0 && (float)$price_to_compare == 0) {
                return true;
            } else if ((float)$conf['max_price'] == 0 && (float)$conf['min_price'] == 0 && (float)$price_to_compare != 0) {
                return false;
            } else if ((float)$conf['max_price'] != 0 && (float)$conf['min_price'] == 0 && (float)$price_to_compare > $conf['max_price']) {
                return false;
            } else if ((float)$conf['max_price'] == 0 && (float)$conf['max_price'] != 0 && (float)$price_to_compare < $conf['min_price']) {
                return false;
            }
        }
        return true;
    }

    protected static function checkExceptions($conf = false, $id_product = false, $id_customer = false)
    {
        if ($conf) {
            if ($conf['customers_excluded']) {
                $customers_excl_array = explode(';', $conf['customers_excluded']);
                if (in_array($id_customer, $customers_excl_array)) {
                    return false;
                }
            }

            if ($conf['products_excluded']) {
                $products_excl_array = explode(';', $conf['products_excluded']);
                if (in_array($id_product, $products_excl_array)) {
                    return false;
                }
            }
        }
        return true;
    }

    public static function getProductProperties($properties = null)
    {
        if ($properties != null) {
            $id_product_attribute = isset($properties['id_product_attribute']) ? $properties['id_product_attribute'] : 0;
            $conf = HidePriceConfiguration::getConfigurations($properties['id_product'], $id_product_attribute);

            if (!empty($conf)) {
                if ($conf['hide_price']) {
                    $properties['show_price'] = 0;
                    $properties['available_for_order'] = 0;
                }
                if ($conf['disallow_purchase']) {
                    $properties['available_for_order'] = 0;
                }
            }
        }
        return $properties;
    }

    public static function checkProductsAvailability($products = null)
    {
        $errors = array();
        if (is_array($products)) {
            foreach ($products as $p) {
                $configs = HidePriceConfiguration::getConfigurations($p['id_product'], $p['id_product_attribute']);
                if (!empty($configs) && $configs['disallow_purchase']) {
                    $mod = new Hideprice();
                    $errors[] = $mod->getMessageAvailable($p['name']);
                    break;
                }
            }
        } else if (is_int($products)) {
            $configs = HidePriceConfiguration::getConfigurations($products);
            if (!empty($configs) && $configs['disallow_purchase']) {
                $mod = new Hideprice();
                $errors[] = $mod->getMessageAvailable($p['name']);
            }
        }
        return $errors;
    }

    public static function changeProductProperties($product = null)
    {
        $hideprice = new HidepriceConfiguration();
        $id_product_attribute = (isset($product->id_product_attribute)) ? $product->id_product_attribute : 0;

        $configs = $hideprice->getConfigurations($product->id, $id_product_attribute);
        if (!empty($configs)) {
            if ($configs['hide_price']) {
                $product->show_price = 0;
                $product->available_for_order = 0;
            }
            if ($configs['disallow_purchase']) {
                $product->available_for_order = 0;
            }
        }
        return $product;
    }
}