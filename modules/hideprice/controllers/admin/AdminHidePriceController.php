<?php
/**
* Hide prices by categories, groups and more
*
* NOTICE OF LICENSE
*
* This product is licensed for one customer to use on one installation (test stores and multishop included).
* Site developer has the right to modify this module to suit their needs, but can not redistribute the module in
* whole or in part. Any other use of this module constitues a violation of the user agreement.
*
* DISCLAIMER
*
* NO WARRANTIES OF DATA SAFETY OR MODULE SECURITY
* ARE EXPRESSED OR IMPLIED. USE THIS MODULE IN ACCORDANCE
* WITH YOUR MERCHANT AGREEMENT, KNOWING THAT VIOLATIONS OF
* PCI COMPLIANCY OR A DATA BREACH CAN COST THOUSANDS OF DOLLARS
* IN FINES AND DAMAGE A STORES REPUTATION. USE AT YOUR OWN RISK.
*
*  @author    idnovate
*  @copyright 2019 idnovate
*  @license   See above
*/

class AdminHidePriceController extends ModuleAdminController
{
    protected $delete_mode;

    protected $_defaultOrderBy = 'date_add';
    protected $_defaultOrderWay = 'DESC';
    protected $can_add_conf = true;
    protected $top_elements_in_list = 4;
    protected $orderBy = 'id_product';
    protected $orderWay = 'ASC';

    protected static $meaning_status = array();

    public function __construct()
    {
        $this->bootstrap = true;
        $this->table = 'hideprice_configuration';
        $this->className = 'HidePriceConfiguration';
        $this->tabClassName = 'AdminHidePrice';
        $this->lang = false;
        $this->addRowAction('edit');
        $this->addRowAction('delete');
        $this->_orderWay = $this->_defaultOrderWay;
        $this->taxes_included = (Configuration::get('PS_TAX') == '0' ? false : true);
        $this->module_name = 'hideprice';

        parent::__construct();

        $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->l('Delete selected'),
                'confirm' => $this->l('Delete selected items?'),
                'icon' => 'icon-trash'
            )
        );

        $this->context = Context::getContext();

        $this->default_form_language = $this->context->language->id;

        $this->fields_list = array(
            'id_hideprice_configuration' => array(
                'title' => $this->l('ID'),
                'align' => 'text-center',
                'class' => 'fixed-width-xs'
            ),
            'name' => array(
                'title' => $this->l('Name'),
                'filter_key' => 'a!name'
            ),
            'hide_price' => array(
                'title' => $this->l('Hide Price'),
                'align' => 'text-center',
                'type' => 'select',
                'list' => array(0 => $this->l('No'), 1 => $this->l('Yes')),
                'callback' => 'printHidePriceIcon',
                'filter_key' => 'a!hide_price'
            ),
            'disallow_purchase' => array(
                'title' => $this->l('Disallow Purchase'),
                'align' => 'text-center',
                'type' => 'select',
                'list' => array(0 => $this->l('No'), 1 => $this->l('Yes')),
                'callback' => 'printDisallowPurchaseIcon',
                'filter_key' => 'a!disallow_purchase'
            ),
            'categories' => array(
                'title' => $this->l('Category(s)'),
                'callback' => 'getCategories',
                'align' => 'text-center'
            ),
            'products' => array(
                'title' => $this->l('Products(s)'),
                'callback' => 'getProducts',
                'align' => 'text-center'
            ),
            'groups' => array(
                'title' => $this->l('Group(s)'),
                'callback' => 'getCustomerGroups',
                'align' => 'text-center'
            ),
            'customers' => array(
                'title' => $this->l('Customers(s)'),
                'callback' => 'getCustomers',
                'align' => 'text-center'
            ),
            'countries' => array(
                'title' => $this->l('Country(s)'),
                'callback' => 'getCountries',
                'align' => 'text-center'
            ),
            'priority' => array(
                'title' => $this->l('Priority'),
                'align' => 'text-center'
            ),
            'date_to' => array(
                'title' => $this->l('Valid'),
                'align' => 'text-center',
                'callback' => 'printValidIcon',
            ),
            'active' => array(
                'title' => $this->l('Enabled'),
                'align' => 'text-center',
                'active' => 'status',
                'type' => 'bool',
                'callback' => 'printActiveIcon'
            ),
        );

        if (Shop::isFeatureActive() && (Shop::getContext() == Shop::CONTEXT_ALL || Shop::getContext() == Shop::CONTEXT_GROUP)) {
            $this->can_add_conf = false;
        }

        if (!Shop::isFeatureActive()) {
            $this->shopLinkType = '';
        } else {
            $this->shopLinkType = 'shop';
        }
    }

    public function setMedia($isNewTheme = false)
    {
        parent::setMedia($isNewTheme);
        $this->addJqueryPlugin(array('typewatch', 'fancybox', 'autocomplete'));

        $this->addJqueryUI('ui.button');
        $this->addJqueryUI('ui.sortable');
        $this->addJqueryUI('ui.droppable');
        $_path = _MODULE_DIR_.$this->module->name;

        $this->context->controller->addCSS($_path.'/views/css/back.css');
        if (version_compare(_PS_VERSION_, '1.6', '<')) {
            $this->context->controller->addJS($_path.'/views/js/back.js');
        } else {
            $this->context->controller->addJS($_path.'/views/js/back.js');
        }

        if (version_compare(_PS_VERSION_, '1.6', '>=')) {
            if ($this->display) {
                $this->context->controller->addJS($_path.'/views/js/tabs.js');
            }
        }
    }

    public function postProcess()
    {
        if (Tools::isSubmit('submitAdd'.$this->table)) {
            $this->errors = $this->_formValidations();
        }

        return parent::postProcess();
    }

    public function initContent()
    {
        if ($this->action == 'select_delete') {
            $this->context->smarty->assign(array(
                'delete_form' => true,
                'url_delete' => htmlentities($_SERVER['REQUEST_URI']),
                'boxes' => $this->boxes,
            ));
        }

        if (!$this->can_add_conf && !$this->display) {
            $this->informations[] = $this->l('You have to select a shop if you want to create a new configuration.');
        }

        $module = new Hideprice();
        if ($this->action != 'new' && !Tools::isSubmit('update'.$this->table)) {
            if (Tools::isSubmit('submitHidepriceModuleGlobalConfig')) {
                $form_values = $this->getGlobalConfigFormValues();
                foreach (array_keys($form_values) as $key) {
                    if ((version_compare(_PS_VERSION_, '1.6', '>=') ? Tools::strpos($key, '[]') > 0 : strpos($key, '[]') > 0)) {
                        $key = Tools::str_replace_once('[]', '', $key);
                        Configuration::updateValue($key, implode(';', Tools::getValue($key)));
                    } else {
                        Configuration::updateValue($key, Tools::getValue($key));
                    }
                }
                $this->content .= $module->displayConfirmation($this->l('Configuration saved successfully.'));
            }
        }

        $this->content .= $this->_createTemplate('admin_translations.tpl')->fetch();

        if (!$this->display) {
            $this->content .= $this->renderGlobalConfigForm();
        }

        if ((Tools::getValue('id_'.$this->table)) && Tools::getIsset('duplicate'.$this->table)) {
            $this->processDuplicate();
        }

        parent::initContent();

        if (!$this->display) {
            if (version_compare(_PS_VERSION_, '1.6', '>=')) {
                $this->context->smarty->assign(array(
                    'this_path'                 => $this->module->getPathUri(),
                    'support_id'                => $module->addons_id_product,
                ));

                $available_lang_codes = array('en', 'es', 'fr', 'it', 'de');
                $default_lang_code = 'en';
                $template_iso_suffix = in_array(strtok($this->context->language->language_code, '-'), $available_lang_codes) ? strtok($this->context->language->language_code, '-') : $default_lang_code;
                $this->content .= $this->context->smarty->fetch($this->module->getLocalPath().'/views/templates/admin/company/information_'.$template_iso_suffix.'.tpl');
            }
            $this->context->smarty->assign(array(
                'content' => $this->content,
                'token' => $this->token,
            ));
        }
    }

    public function initToolbar()
    {
        parent::initToolbar();

        if (!$this->can_add_conf) {
            unset($this->toolbar_btn['new']);
        }
    }

    public function getList($id_lang, $orderBy = null, $orderWay = null, $start = 0, $limit = null, $id_lang_shop = null)
    {
        parent::getList($id_lang, $orderBy, $orderWay, $start, $limit, $id_lang_shop);
    }

    public function initModal()
    {
        parent::initModal();

        $languages = Language::getLanguages(false);
        $translateLinks = array();

        if (version_compare(_PS_VERSION_, '1.7.2.1', '>=')) {
            $module = Module::getInstanceByName($this->module->name);
            $isNewTranslateSystem = $module->isUsingNewTranslationSystem();
            $link = Context::getContext()->link;
            foreach ($languages as $lang) {
                if ($isNewTranslateSystem) {
                    $translateLinks[$lang['iso_code']] = $link->getAdminLink('AdminTranslationSf', true, array(
                        'lang' => $lang['iso_code'],
                        'type' => 'modules',
                        'selected' => $module->name,
                        'locale' => $lang['locale'],
                    ));
                } else {
                    $translateLinks[$lang['iso_code']] = $link->getAdminLink('AdminTranslations', true, array(), array(
                        'type' => 'modules',
                        'module' => $module->name,
                        'lang' => $lang['iso_code'],
                    ));
                }
            }
        }

        $this->context->smarty->assign(array(
            'trad_link' => 'index.php?tab=AdminTranslations&token='.Tools::getAdminTokenLite('AdminTranslations').'&type=modules&module='.$this->module->name.'&lang=',
            'module_languages' => $languages,
            'module_name' => $this->module->name,
            'translateLinks' => $translateLinks,
        ));

        $modal_content = $this->context->smarty->fetch('controllers/modules/modal_translation.tpl');

        $this->modals[] = array(
            'modal_id' => 'moduleTradLangSelect',
            'modal_class' => 'modal-sm',
            'modal_title' => $this->l('Translate this module'),
            'modal_content' => $modal_content
        );
    }

    public function initToolbarTitle()
    {
        parent::initToolbarTitle();

        switch ($this->display) {
            case '':
            case 'list':
                array_pop($this->toolbar_title);
                $this->toolbar_title[] = $this->l('Manage Hide Prices Configuration');
                break;
            case 'view':
                if (($conf = $this->loadObject(true)) && Validate::isLoadedObject($conf)) {
                    array_pop($this->toolbar_title);
                    $this->toolbar_title[] = sprintf($this->l('Configuration: %s'), $conf->name);
                }
                break;
            case 'add':
            case 'edit':
                array_pop($this->toolbar_title);
                if (($conf = $this->loadObject(true)) && Validate::isLoadedObject($conf)) {
                    $this->toolbar_title[] = sprintf($this->l('Editing Configuration: %s'), $conf->name);
                } else {
                    $this->toolbar_title[] = $this->l('Creating a new hide price and disallow purchase configuration:');
                }
                break;
        }
    }

    protected function renderGlobalConfigForm()
    {
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->module = new Hideprice();
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);
        $helper->identifier = $this->identifier;
        $helper->currentIndex = self::$currentIndex;
        $helper->submit_action = 'submitHidepriceModuleGlobalConfig';
        $helper->token = Tools::getAdminTokenLite($this->tabClassName);
        $helper->tpl_vars = array(
            'fields_value' => $this->getGlobalConfigFormValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getGlobalConfigForm()));
    }

    protected function getGlobalConfigForm()
    {
        $form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Global settings'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'col' => 5,
                        'type' => (version_compare(_PS_VERSION_, '1.6', '>=')) ? 'switch' : 'radio',
                        'name' => 'HIDEPRICE_USE_PRODUCTS',
                        'label' => $this->l('Use products filter'),
                        'class' => 't',
                        'default_value' => true,
                        'desc' => (version_compare(_PS_VERSION_, '1.6', '<')) ? $this->l('Enable if you want to use the features. Disable if you will not need to create rules by products') : '',
                        'hint' => (version_compare(_PS_VERSION_, '1.6', '>=')) ? $this->l('Enable if you want to use the features. Disable if you will not need to create rules by products') : '',
                        'values' => array(
                            array(
                                'id' => 'HIDEPRICE_USE_PRODUCTS_on',
                                'value' => true,
                                'label' => $this->l('Yes')
                            ),
                            array(
                                'id' => 'HIDEPRICE_USE_PRODUCTS_off',
                                'value' => false,
                                'label' => $this->l('No')
                            )
                        ),
                    ),
                    array(
                        'col' => 5,
                        'type' => (version_compare(_PS_VERSION_, '1.6', '>=')) ? 'switch' : 'radio',
                        'name' => 'HIDEPRICE_USE_CUSTOMERS',
                        'label' => $this->l('Use customers filter'),
                        'class' => 't',
                        'default_value' => true,
                        'desc' => (version_compare(_PS_VERSION_, '1.6', '<')) ? $this->l('Enable if you want to use the attributes. Disable if you will not need to create rules by customers') : '',
                        'hint' => (version_compare(_PS_VERSION_, '1.6', '>=')) ? $this->l('Enable if you want to use the attributes. Disable if you will not need to create rules by customers') : '',
                        'values' => array(
                            array(
                                'id' => 'HIDEPRICE_USE_CUSTOMERS_on',
                                'value' => true,
                                'label' => $this->l('Yes')
                            ),
                            array(
                                'id' => 'HIDEPRICE_USE_CUSTOMERS_off',
                                'value' => false,
                                'label' => $this->l('No')
                            )
                        ),
                    ),
                    array(
                        'col' => 5,
                        'type' => (version_compare(_PS_VERSION_, '1.6', '>=')) ? 'switch' : 'radio',
                        'name' => 'HIDEPRICE_USE_FEATURES',
                        'label' => $this->l('Use features filter'),
                        'class' => 't',
                        'default_value' => true,
                        'desc' => (version_compare(_PS_VERSION_, '1.6', '<')) ? $this->l('Enable if you want to use the features. Disable if you will not need to create rules by features') : '',
                        'hint' => (version_compare(_PS_VERSION_, '1.6', '>=')) ? $this->l('Enable if you want to use the features. Disable if you will not need to create rules by features') : '',
                        'values' => array(
                            array(
                                'id' => 'HIDEPRICE_USE_FEATURES_on',
                                'value' => true,
                                'label' => $this->l('Yes')
                            ),
                            array(
                                'id' => 'HIDEPRICE_USE_FEATURES_off',
                                'value' => false,
                                'label' => $this->l('No')
                            )
                        ),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                    'type' => 'submit',
                    'class' => (version_compare(_PS_VERSION_, '1.6', '>=')) ? 'btn btn-default pull-right' : 'button big',
                    'name' => 'submitHidepriceModuleGlobalConfig',
                ),
            ),
        );
        return $form;
    }

    protected function getGlobalConfigFormValues()
    {
        return array(
            'HIDEPRICE_USE_FEATURES' => Configuration::get('HIDEPRICE_USE_FEATURES'),
            'HIDEPRICE_USE_PRODUCTS' => Configuration::get('HIDEPRICE_USE_PRODUCTS'),
            'HIDEPRICE_USE_CUSTOMERS' => Configuration::get('HIDEPRICE_USE_CUSTOMERS'),
        );
    }

    public function initPageHeaderToolbar()
    {
        parent::initPageHeaderToolbar();

        if (empty($this->display)) {
            $this->page_header_toolbar_btn['desc-module-back'] = array(
                'href' => 'index.php?controller=AdminModules&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back'),
                'icon' => 'process-icon-back'
            );
            $this->page_header_toolbar_btn['desc-module-new'] = array(
                'href' => 'index.php?controller='.$this->tabClassName.'&add'.$this->table.'&token='.Tools::getAdminTokenLite($this->tabClassName),
                'desc' => $this->l('New'),
                'icon' => 'process-icon-new'
            );
            $this->page_header_toolbar_btn['desc-module-reload'] = array(
                'href' => 'index.php?controller='.$this->tabClassName.'&token='.Tools::getAdminTokenLite($this->tabClassName).'&reload=1',
                'desc' => $this->l('Reload'),
                'icon' => 'process-icon-refresh'
            );
            $this->page_header_toolbar_btn['desc-module-translate'] = array(
                'href' => '#',
                'desc' => $this->l('Translate'),
                'modal_target' => '#moduleTradLangSelect',
                'icon' => 'process-icon-flag'
            );
            $this->page_header_toolbar_btn['desc-module-hook'] = array(
                'href' => 'index.php?tab=AdminModulesPositions&token='.Tools::getAdminTokenLite('AdminModulesPositions').'&show_modules='.Module::getModuleIdByName('hideprice'),
                'desc' => $this->l('Manage hooks'),
                'icon' => 'process-icon-anchor'
            );
        }

        if (!$this->can_add_conf) {
            unset($this->page_header_toolbar_btn['desc-module-new']);
        }
    }

    public function processDuplicate()
    {
        $id_conf = Tools::getValue($this->identifier);
        $conf = new HidepriceConfiguration($id_conf);
        unset($conf->id_hideprice_configuration);

        if (!$conf->add()) {
            $this->errors[] = Tools::displayError('An error occurred while duplicating the report #'.$id_conf);
        } else {
            $this->confirmations[] = sprintf($this->l('Rule #%s - %s successfully duplicated.'), $id_conf, $conf->name);
            $this->afterUpdate($conf, $conf->id);
            if (version_compare(_PS_VERSION_, '1.6', '<')) {
                return Tools::redirectAdmin('index.php?tab='.$this->tabClassName.'&token='.Tools::getAdminTokenLite($this->tabClassName));
            } else {
                return Tools::redirectAdmin('index.php?controller='.$this->tabClassName.'&token='.Tools::getAdminTokenLite($this->tabClassName));
            }
        }
    }

    public function initProcess()
    {
        parent::initProcess();

        if (Tools::isSubmit('changeDisallowPurchaseVal') && $this->id_object) {
            if ($this->tabAccess['edit'] === '1') {
                $this->action = 'change_disallowpurchase_val';
            } else {
                $this->errors[] = Tools::displayError('You do not have permission to edit this.');
            }
        }

        if (Tools::isSubmit('changeHidePriceVal') && $this->id_object) {
            if ($this->tabAccess['edit'] === '1') {
                $this->action = 'change_hideprice_val';
            } else {
                $this->errors[] = Tools::displayError('You do not have permission to edit this.');
            }
        }
    }

    public function renderList()
    {
        if ((Tools::isSubmit('submitBulkdelete'.$this->table) || Tools::isSubmit('delete'.$this->table)) && $this->tabAccess['delete'] === '1') {
            $this->tpl_list_vars = array(
                'delete_hidepriceconf' => true,
                'REQUEST_URI' => $_SERVER['REQUEST_URI'],
                'POST' => $_POST
            );
        }
        return parent::renderList();
    }

    public function renderForm()
    {
        if (!($conf = $this->loadObject(true))) {
            return;
        }

        $id_lang = 0;
        $id_shop = 0;
        $categories = array();

        $price_options = $this->getPriceOptions();

        if (version_compare(_PS_VERSION_, '1.5', '<')) {
            $id_lang = (int)$this->context->cookie->id_lang;
            $id_shop = (int)$this->context->cookie->id_shop;
            $currencies = Currency::getCurrencies(false, true);
        } else {
            $id_lang = (int)$this->context->language->id;
            $id_shop = (int)$this->context->shop->id;
            if (Shop::isFeatureActive()) {
                $currencies = Currency::getCurrenciesByIdShop($this->context->shop->id);
            } else {
                $currencies = Currency::getCurrencies(false, true);
            }
        }

        if (version_compare(_PS_VERSION_, '1.6', '<')) {
            $categories = array_merge($categories, Category::getCategories((int)($this->context->cookie->id_lang), false, false, '', 'ORDER BY cl.`name` ASC'));
        }

        $groups = Group::getGroups($id_lang, true);
        $customers = array();
        $products = array();
        $countries = Country::getCountries($id_lang);
        $zones = Zone::getZones();
        $manufacturers = Manufacturer::getManufacturers(false, $id_lang, false);
        $suppliers = Supplier::getSuppliers(false, $id_lang, false);
        $languages = Language::getLanguages(false, $id_shop);
        /*$features = Feature::getFeatures((int)$id_lang);
        $attributeGroups = AttributeGroup::getAttributesGroups((int)$id_lang);

        $array_features = array();

        foreach ($features as $key => $feature) {
            if ($feature['name']) {
                $feature_values = FeatureValue::getFeatureValuesWithLang((int)$id_lang, $feature['id_feature']);
                if (!empty($feature_values)) {
                    $array_features[] = array(
                        'type' => 'select',
                        'label' => $this->l('Select').' '.$feature['name'],
                        'name' => 'feature_'.$feature['id_feature'].'[]',
                        'multiple' => true,
                        'required' => false,
                        'class' => 'multiple_select',
                        'col' => '3',
                        'options' => array(
                            'query' => $feature_values,
                            'id' => 'id_feature_value',
                            'name' => 'value'
                        ),
                        'desc' => (version_compare(_PS_VERSION_, '1.6', '>=')) ? $this->l('Select the ').' '.$feature['name'].' '.$this->l('to apply this configuration') : '',
                    );
                }
            }
        }

        $array_attributes = array();
        foreach ($attributeGroups as $key => $attributeGroup) {
            if ($attributeGroup['name']) {
                if (!empty($attributeGroups)) {
                    $array_attributes[] = array(
                        'type' => 'select',
                        'label' => $this->l('Select').' '.$attributeGroup['name'],
                        'name' => 'attribute_'.$attributeGroup['id_attribute_group'].'[]',
                        'multiple' => true,
                        'required' => false,
                        'class' => 'multiple_select',
                        'col' => '3',
                        'options' => array(
                            'query' => AttributeGroup::getAttributes((int)$id_lang, $attributeGroup['id_attribute_group']),
                            'id' => 'id_attribute',
                            'name' => 'name'
                        ),
                        'desc' => (version_compare(_PS_VERSION_, '1.6', '>=')) ? $this->l('Select the ').' '.$attributeGroup['name'].' '.$this->l('to apply this configuration') : '',
                    );
                }
            }
        }*/

        $this->multiple_fieldsets = true;
        $this->default_form_language = $this->context->language->id;

        $this->fields_form[]['form'] = array(
            'legend' => array(
                'title' => $this->l('Hide Prices Configuration'),
                'icon' => 'icon-key'
            ),
            'input' => array(
                array(
                    'type' => (version_compare(_PS_VERSION_, '1.6', '>=')) ? 'switch' : 'radio',
                    'label' => $this->l('Active'),
                    'name' => 'active',
                    'class' => 't',
                    'col' => '5',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                    'desc' => $this->l('Enable or Disable this configuration'),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Name'),
                    'name' => 'name',
                    'required' => true,
                    'col' => '5',
                    'desc' => $this->l('Invalid characters:').' !&lt;&gt;,;?=+()@#"°{}_$%:',
                ),
                array(
                    'type' => (version_compare(_PS_VERSION_, '1.6', '>=')) ? 'switch' : 'radio',
                    'label' => $this->l('Hide Price'),
                    'name' => 'hide_price',
                    'class' => 't',
                    'col' => '5',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'hide_price_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'hide_price_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                    'desc' => $this->l('Enable it to hide the price of the products in the store'),
                ),
                array(
                    'type' => (version_compare(_PS_VERSION_, '1.6', '>=')) ? 'switch' : 'radio',
                    'label' => $this->l('Disallow purchase'),
                    'name' => 'disallow_purchase',
                    'class' => 't',
                    'col' => '5',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'disallow_purchase_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'disallow_purchase_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                    'desc' => $this->l('Enable it to disallow the purchase of the product in the store (hide the add to cart button)'),
                ),
                array(
                    'type' => (version_compare(_PS_VERSION_, '1.6', '>=')) ? 'switch' : 'radio',
                    'label' => $this->l('Show alternative text'),
                    'name' => 'show_text',
                    'class' => 't',
                    'col' => '5',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'show_text_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'show_text_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                    'desc' => $this->l('Show alternative text in the place of the product price'),
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Alternative text'),
                    'name' => 'alternative_text',
                    'id' => 'html_content',
                    'required' => false,
                    'lang' => true,
                    'cols' => 8,
                    'rows' => 4,
                    'class' => 'toggle_show_text',
                    'autoload_rte' => 'rte',
                    'desc' => $this->l('Alternative text to show in the place of the product price'),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Priority'),
                    'name' => 'priority',
                    'default' => '1',
                    'col' => '3',
                    'desc' => $this->l('Set priority when 2 or more configurations overlaps. Configuration with less number here will have more priority'),
                ),
                array(
                    'type' =>  (version_compare(_PS_VERSION_, '1.6', '>=')) ? 'datetime' : 'date',
                    'label' => $this->l('Date From'),
                    'name' => 'date_from',
                    'col' => '4',
                    'size' => 20,
                    'width' => 150,
                    'desc' => $this->l('Date from which the rule is valid. You can use hours, minutes and secons. Example: 2016-10-27 is considered 2016-10-27 00:00:00 and it means that the rule is valid from 2016-10-27 00:00:00'),
                ),
                array(
                    'type' =>  (version_compare(_PS_VERSION_, '1.6', '>=')) ? 'datetime' : 'date',
                    'label' => $this->l('Date To'),
                    'name' => 'date_to',
                    'col' => '4',
                    'size' => 20,
                    'width' => 150,
                    'desc' => $this->l('Date to which the rule is valid. You can use hours, minutes and secons. Example: 2016-10-27 is considered 2016-10-27 00:00:00 and it means that the rule is valid until 2016-10-26 23:59:59'),
                ),
                array(
                    'type' => 'free',
                    'label' => $this->l('Schedule'),
                    'name' => 'schedule',
                    'hint' => $this->l('Select days of week and hours to show apply the rule (Click on the box to enable or disable the day and define the time range)')
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'type' => 'submit',
            ),
        );

        $this->fields_form[1]['form'] = array(
            'legend' => array(
                'title' => $this->l('Product filters (what product prices will be hidden)'),
                'icon' => 'icon-edit'
            ),
            'input' => array(
                array(
                    'type' => (version_compare(_PS_VERSION_, '1.6', '>=')) ? 'switch' : 'radio',
                    'label' => $this->l('Filter by prices'),
                    'name' => 'filter_prices',
                    'class' => 't',
                    'col' => '5',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'filter_prices_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'filter_prices_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                    'desc' => $this->l('Enable or Disable the filter by prices range. For example: Appy the rule to the products with Retail price with taxes between 10€ (minimum) and 50€ (maximum)'),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Price from calculate the minimum and maximum'),
                    'name' => 'price_calculate',
                    'class' => 'toggle_filter_prices',
                    'col' => '5',
                    'options' => array(
                        'query' => $price_options,
                        'id' => 'id',
                        'name' => 'name',
                        'default' => array(
                            'value' => '',
                            'label' => $this->l('-- Choose --')
                        )
                    ),
                    'desc' => $this->l('Select the price which the threshold will be calculated. For example: Appy the rule to the products that have a Retail price with taxes prices between 10€ (minimum) and 50€ (maximum)'),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Threshold minimum price'),
                    'name' => 'min_price',
                    'class' => 'toggle_filter_prices',
                    'col' => '2',
                    'default' => '0',
                    'desc' => sprintf($this->l('Minimum price to set a less Threshold price (%s) to apply the rule. If this value is 0 there\'s no minimum limit'), ($this->taxes_included) ? $this->l('taxes included') : $this->l('without taxes')),
                    'suffix' => $this->context->currency->sign
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Threshold maximum price'),
                    'name' => 'max_price',
                    'class' => 'toggle_filter_prices',
                    'col' => '2',
                    'default' => '0',
                    'desc' => sprintf($this->l('Maximum price to set a top Threshold price (%s) to apply the rule. If this value is 0 there\'s no maximum limit'), ($this->taxes_included) ? $this->l('taxes included') : $this->l('without taxes')),
                    'suffix' => $this->context->currency->sign
                ),
                array(
                    'type' => (version_compare(_PS_VERSION_, '1.6', '>=')) ? 'switch' : 'radio',
                    'label' => $this->l('Filter by stock'),
                    'name' => 'filter_stock',
                    'class' => 't',
                    'col' => '5',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'filter_stock_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'filter_stock_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                    'desc' => $this->l('Enable or Disable the filter by stock range. For example: Appy the rule to the products with stock quantity between 50 and 100'),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Products with stock from'),
                    'name' => 'min_stock',
                    'class' => 'toggle_filter_stock',
                    'col' => '3',
                    'default' => '0',
                    'desc' => $this->l('Enable rule to products with stock from'),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Products with stock until'),
                    'name' => 'max_stock',
                    'class' => 'toggle_filter_stock',
                    'col' => '3',
                    'default' => '0',
                    'desc' => $this->l('Enable rule to products with stock until'),
                ),
                array(
                    'type' => (version_compare(_PS_VERSION_, '1.6', '>=')) ? 'switch' : 'radio',
                    'label' => $this->l('Apply only if deny orders without stock'),
                    'name' => 'hide_price_status',
                    'class' => 't toggle_filter_stock',
                    'col' => '5',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'hide_price_status_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'hide_price_status_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                    'desc' => $this->l('Enable it to hide the price of the products in the store only if the status of the product is "Deny"'),
                ),
                array(
                    'type' => (version_compare(_PS_VERSION_, '1.6', '>=')) ? 'switch' : 'radio',
                    'label' => $this->l('Filter by weight'),
                    'name' => 'filter_weight',
                    'class' => 't',
                    'col' => '5',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'filter_weight_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'filter_weight_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                    'desc' => $this->l('Enable or Disable the filter by weight range. For example: Appy the rule to the products with weight between 0 and 6'),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Products with weight from'),
                    'name' => 'min_weight',
                    'class' => 'toggle_filter_weight',
                    'col' => '2',
                    'default' => '0',
                    'desc' => $this->l('Enable rule to products with weight from'),
                    'suffix' => Configuration::get('PS_WEIGHT_UNIT')
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Products with weight until'),
                    'name' => 'max_weight',
                    'class' => 'toggle_filter_weight',
                    'col' => '2',
                    'default' => '0',
                    'desc' => $this->l('Enable rule to products with weight until'),
                    'suffix' => Configuration::get('PS_WEIGHT_UNIT')
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'type' => 'submit',
            ),
        );

        if (version_compare(_PS_VERSION_, '1.6', '>=')) {
            $selected_categories = array();
            if ($conf->categories != '') {
                if (@unserialize($conf->categories) !== false) {
                    $selected_categories = unserialize($conf->categories);
                } else {
                    $selected_categories = explode(';', $conf->categories);
                }
            }

            $categories_form_array = array(
               'type'  => 'categories',
               'label' => $this->l('Select Category(s)'),
               'multiple' => true,
               'name'  => 'categories',
               'col' => '7',
               'tree'  => array(
                    'id' => 'id_category',
                    'use_checkbox' => true,
                    'selected_categories' => $selected_categories,
                    ),
                'desc' => $this->l('Select the Category(es) where the rule will be applied. If you don\'t select any value, the rule will be applied to all Categories'),
            );
        } else {
            $categories_form_array = array(
                'type' => 'select',
                'label' => $this->l('Select Category(s)'),
                'class' => 'multiple_select',
                'name'  => 'categories[]',
                'multiple' => true,
                'required' => false,
                'col' => '2',
                'options' => array(
                    'query' => $categories,
                    'id' => 'id_category',
                    'name' => 'name'
                ),
                'desc' => $this->l('Select the Category(es) where the rule will be applied. If you don\'t select any value, the rule will be applied to all Categories'),
            );
        }

        array_push($this->fields_form[1]['form']['input'], $categories_form_array);

        $array_product_filters = array();

        if (Configuration::get('HIDEPRICE_USE_PRODUCTS')) {
            $array_product_filters[] =
                array(
                    'type' => 'select',
                    'label' => $this->l('Select Product(s)'),
                    'name' => 'products[]',
                    'class' => 'multiple_select',
                    'multiple' => true,
                    'required' => false,
                    'col' => '7',
                    'options' => array(
                        'query' => $products,
                        'id' => 'id_product',
                        'name' => 'name'
                    ),
                    'desc' => $this->l('Select the Product(s) where the rule will be applied. If you don\'t select any value, the rule will be applied to all Products'),
                );

            $array_product_filters[] =
                array(
                    'type' => 'select',
                    'label' => $this->l('Select Excluded Product(s)'),
                    'name' => 'products_excluded[]',
                    'class' => 'multiple_select',
                    'multiple' => true,
                    'required' => false,
                    'col' => '7',
                    'options' => array(
                        'query' => $products,
                        'id' => 'id_product',
                        'name' => 'name'
                    ),
                    'desc' => $this->l('Select the Product(s) where the rule will not be applied. Only the products selected will be excluded'),
                );

        }


        $array_product_filters[] =
            array(
                'type' => 'select',
                'label' => $this->l('Select Manufacturer(s)'),
                'name' => 'manufacturers[]',
                'class' => 'multiple_select',
                'multiple' => true,
                'required' => false,
                'col' => '7',
                'options' => array(
                    'query' => $manufacturers,
                    'id' => 'id_manufacturer',
                    'name' => 'name'
                ),
                'desc' => $this->l('Select the Manufacturer(s) where the rule will be applied. If you don\'t select any value, the rule will be applied to all Manufacturers'),
            );
        $array_product_filters[] =
            array(
                'type' => 'select',
                'label' => $this->l('Select Supplier(s)'),
                'name' => 'suppliers[]',
                'class' => 'multiple_select',
                'multiple' => true,
                'required' => false,
                'col' => '7',
                'options' => array(
                    'query' => $suppliers,
                    'id' => 'id_supplier',
                    'name' => 'name'
                ),
                'desc' => $this->l('Select the Supplier(s) where the rule will be applied. If you don\'t select any value, the rule will be applied to all Suppliers'),
            );


        if (Configuration::get('HIDEPRICE_USE_FEATURES')) {
            $features = Feature::getFeatures((int)$id_lang);
            $array_features = array();

            foreach ($features as $key => $feature) {
                if ($feature['name']) {
                    $feature_values = FeatureValue::getFeatureValuesWithLang((int)$id_lang, $feature['id_feature']);
                    if (!empty($feature_values)) {
                        $array_features[] = array(
                            'type' => 'select',
                            'label' => $this->l('Select').' '.$feature['name'],
                            'name' => 'feature_'.$feature['id_feature'].'[]',
                            'multiple' => true,
                            'required' => false,
                            'class' => 'multiple_select',
                            'col' => '7',
                            'options' => array(
                                'query' => $feature_values,
                                'id' => 'id_feature_value',
                                'name' => 'value'
                            ),
                            'desc' => (version_compare(_PS_VERSION_, '1.6', '>=')) ? $this->l('Select the ').' '.$feature['name'].' '.$this->l('to apply this configuration') : '',
                        );
                    }
                }
            }

            foreach ($array_features as $f) {
                array_push($array_product_filters, $f);
            }
        }

        $attributeGroups = AttributeGroup::getAttributesGroups((int)$id_lang);

        /*$array_attributes = array();
        foreach ($attributeGroups as $key => $attributeGroup) {
            if ($attributeGroup['name']) {
                if (!empty($attributeGroups)) {
                    $array_attributes[] = array(
                        'type' => 'select',
                        'label' => $this->l('Select').' '.$attributeGroup['name'],
                        'name' => 'attribute_'.$attributeGroup['id_attribute_group'].'[]',
                        'multiple' => true,
                        'required' => false,
                        'class' => 'multiple_select',
                        'col' => '7',
                        'options' => array(
                            'query' => AttributeGroup::getAttributes((int)$id_lang, $attributeGroup['id_attribute_group']),
                            'id' => 'id_attribute',
                            'name' => 'name'
                        ),
                        'desc' => (version_compare(_PS_VERSION_, '1.6', '>=')) ? $this->l('Select the ').' '.$attributeGroup['name'].' '.$this->l('to apply this configuration') : '',
                    );
                }
            }
        }

        foreach ($array_attributes as $a) {
            array_push($array_product_filters, $a);
        }*/

        // add the final part of the form
        foreach ($array_product_filters as $f) {
            array_push($this->fields_form[1]['form']['input'], $f);
        }



        $this->fields_form[]['form'] = array(
            'legend' => array(
                'title' => $this->l('Target filters (who will not see the prices)'),
                'icon' => 'icon-globe'
            ),
            'input' => array(
                array(
                    'type' => 'select',
                    'label' => $this->l('Select Customer group(s)'),
                    'name' => 'groups[]',
                    'class' => 'multiple_select',
                    'multiple' => true,
                    'required' => false,
                    'col' => '7',
                    'options' => array(
                        'query' => $groups,
                        'id' => 'id_group',
                        'name' => 'name'
                    ),
                    'desc' => $this->l('Select the Customer Group(s) where the rule will be applied. If you don\'t select any value, the rule will be applied to all Groups'),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Select Currency(es)'),
                    'name' => 'currencies[]',
                    'class' => 'multiple_select',
                    'multiple' => true,
                    'required' => false,
                    'col' => '7',
                    'options' => array(
                        'query' => $currencies,
                        'id' => 'id_currency',
                        'name' => 'name'
                    ),
                    'desc' => $this->l('Currency(es) to apply this configuration'),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Select Language(s)'),
                    'name' => 'languages[]',
                    'class' => 'multiple_select',
                    'multiple' => true,
                    'required' => false,
                    'col' => '7',
                    'options' => array(
                        'query' => $languages,
                        'id' => 'id_lang',
                        'name' => 'name'
                    ),
                    'desc' => $this->l('Select the Language(s) where the rule will be applied. If you don\'t select any value, the rule will be applied to all Languages'),
                ),
                    array(
                    'type' => 'select',
                    'label' => $this->l('Select Zone(s)'),
                    'name' => 'zones[]',
                    'class' => 'multiple_select',
                    'multiple' => true,
                    'required' => false,
                    'col' => '7',
                    'options' => array(
                        'query' => $zones,
                        'id' => 'id_zone',
                        'name' => 'name'
                    ),
                    'desc' => $this->l('Select the Zone(s) where the rule will be applied. If you don\'t select any value, the rule will be applied to all Zones'),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Select Country(s)'),
                    'name' => 'countries[]',
                    'class' => 'multiple_select',
                    'multiple' => true,
                    'required' => false,
                    'col' => '7',
                    'options' => array(
                        'query' => $countries,
                        'id' => 'id_country',
                        'name' => 'name'
                    ),
                    'desc' => $this->l('Select the Country(s) where the rule will be applied. If you don\'t select any value, the rule will be applied to all Countries'),
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'type' => 'submit',
            ),
        );

        $array_form_dest = array();

        if (Configuration::get('HIDEPRICE_USE_CUSTOMERS')) {
            $array_form_dest[] =
                array(
                    'type' => 'select',
                    'label' => $this->l('Select Excluded Customer(s)'),
                    'name' => 'customers_excluded[]',
                    'class' => 'multiple_select',
                    'multiple' => true,
                    'required' => false,
                    'col' => '7',
                    'options' => array(
                        'query' => $customers,
                        'id' => 'id_customer',
                        'name' => 'email'
                    ),
                    'desc' => $this->l('Select the Customer(s) where the rule will not be applied. Only the customers selected will be excluded.'),
                );

            $array_form_dest[] =
                array(
                    'type' => 'select',
                    'label' => $this->l('Select Customer(s)'),
                    'name' => 'customers[]',
                    'class' => 'multiple_select',
                    'multiple' => true,
                    'required' => false,
                    'col' => '7',
                    'options' => array(
                        'query' => $customers,
                        'id' => 'id_customer',
                        'name' => 'email'
                    ),
                    'desc' => $this->l('Select the Customer(s) where the rule will be applied. If you don\'t select any value, the rule will be applied to all Customers'),
                );
        }

        foreach ($array_form_dest as $f) {
            array_unshift($this->fields_form[2]['form']['input'], $f);
        }
        if ($conf->id) {
            if ($conf->date_from == "1970-01-01 00:00:01") {
                $conf->date_from = "";
            }
            if ($conf->date_to == "1970-01-01 00:00:01") {
                $conf->date_to = "";
            }
            $groups_db = explode(';', $conf->groups);
            $zones_db = explode(';', $conf->zones);
            $countries_db = explode(';', $conf->countries);
            $manufacturers_db = explode(';', $conf->manufacturers);
            $suppliers_db = explode(';', $conf->suppliers);
            $currencies_db = explode(';', $conf->currencies);
            $languages_db = explode(';', $conf->languages);

            $features_decoded_array = Tools::jsonDecode($conf->features);
            $attributes_decoded_array = Tools::jsonDecode($conf->attributes);

            $this->fields_value = array(
                'groups[]' => $groups_db,
                'zones[]' => $zones_db,
                'countries[]' => $countries_db,
                'manufacturers[]' => $manufacturers_db,
                'suppliers[]' => $suppliers_db,
                'currencies[]' => $currencies_db,
                'languages[]' => $languages_db,
            );

            $i = 0;
            if (!empty($features_decoded_array)) {
                foreach ($features_decoded_array as $key => $feature) {
                    $feature_values['feature_'.$key.'[]'] = explode(';', $feature);
                    $i++;
                }

                foreach ($feature_values as $key => $f) {
                    $this->fields_value[$key] = $f;
                }
            }

            $i = 0;
            $attribute_values = array();
            if (!empty($attributes_decoded_array)) {
                foreach ($attributes_decoded_array as $key => $attribute) {
                    $attribute_values['attribute_'.$key.'[]'] = explode(';', $attribute);
                    $i++;
                }

                foreach ($attribute_values as $key => $f) {
                    $this->fields_value[$key] = $f;
                }
            }

            if (version_compare(_PS_VERSION_, '1.6', '<')) {
                $this->fields_value['categories[]'] = explode(';', $conf->categories);
            }
        }

        if (Configuration::get('HIDEPRICE_USE_PRODUCTS')) {
            $products = $this->getProductsLite($id_lang, true, true);

            $this->context->smarty->assign(array(
                'products_selected' => $conf->products,
                'products_available' => $products,
                'products_excl_selected' => $conf->products_excluded,
                'products_excl_available' => $products,
            ));
        }

        if (Configuration::get('HIDEPRICE_USE_CUSTOMERS')) {
            $customers = Customer::getCustomers(true);
            $this->context->smarty->assign(array(
                'customers_selected' => $conf->customers,
                'customers_available' => $customers,
                'customers_excl_selected' => $conf->customers_excluded,
                'customers_excl_available' => $customers,
            ));
        }

        if ($conf->id) {
            $this->context->smarty->assign(array(
                'schedule' => $conf->schedule,
            ));
        } else {
            $this->context->smarty->assign(array(
                'schedule' => '',
            ));
        }

        $protocol = $this->getProtocolUrl();
        $url_admin = $protocol.$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']);

        $this->fields_value['schedule'] =  $this->context->smarty->fetch($this->module->getLocalPath().'views/templates/admin/schedule.tpl');

        $this->content .= parent::renderForm();
        $this->content .= $this->_createTemplate('admin_content.tpl')->fetch();
        return;
    }

    public function renderView()
    {
        return parent::renderView();
    }

    public function processDelete()
    {
        parent::processDelete();
    }

    public function processSave()
    {
        $_POST['groups'] = (!Tools::getValue('groups')) ? '' : implode(';', Tools::getValue('groups'));
        $_POST['countries'] = (!Tools::getValue('countries')) ? '' : implode(';', Tools::getValue('countries'));
        $_POST['zones'] = (!Tools::getValue('zones')) ? '' : implode(';', Tools::getValue('zones'));
        $_POST['manufacturers'] = (!Tools::getValue('manufacturers')) ? '' : implode(';', Tools::getValue('manufacturers'));
        $_POST['suppliers'] = (!Tools::getValue('suppliers')) ? '' : implode(';', Tools::getValue('suppliers'));
        $_POST['customers'] = (!Tools::getValue('customers')) ? '' : implode(';', Tools::getValue('customers'));
        $_POST['customers_excluded'] = (!Tools::getValue('customers_excluded')) ? '' : implode(';', Tools::getValue('customers_excluded'));
        $_POST['products'] = (!Tools::getValue('products')) ? '' : implode(';', Tools::getValue('products'));
        $_POST['products_excluded'] = (!Tools::getValue('products_excluded')) ? '' : implode(';', Tools::getValue('products_excluded'));
        $_POST['currencies'] = (!Tools::getValue('currencies')) ? '' : implode(';', Tools::getValue('currencies'));
        $_POST['languages'] = (!Tools::getValue('languages')) ? '' : implode(';', Tools::getValue('languages'));
        $_POST['attributes'] = (!Tools::getValue('attributes')) ? '' : implode(';', Tools::getValue('attributes'));
        $_POST['features'] = (!Tools::getValue('features')) ? '' : implode(';', Tools::getValue('features'));

        $_POST['date_from'] = ((Tools::getValue('date_from') == "") ? "1970-01-01 00:00:01": Tools::getValue('date_from'));
        $_POST['date_to'] = ((Tools::getValue('date_to') == "") ? "1970-01-01 00:00:01": Tools::getValue('date_to'));

        if (version_compare(_PS_VERSION_, '1.6', '>=')) {
            if (Tools::isSubmit('categories')) {
                $cats = Tools::getValue('categories');
                $_POST['categories'] = serialize($cats);
            } else {
                $_POST['categories'] = '';
            }
        } else {
            $_POST['categories'] = (!Tools::isSubmit('categories')) ? '' : implode(';', Tools::getValue('categories'));
        }

        if (version_compare(_PS_VERSION_, '1.6', '>=')) {
            $this->cleanCache();
        }

        return parent::processSave();
    }

    protected function afterAdd($object)
    {
        $id = Tools::getValue('id_'.$this->table);
        $this->afterUpdate($object, $id);
        return true;
    }

    protected function afterUpdate($object, $id = false)
    {
        if ($id) {
            $conf = new HidepriceConfiguration((int)$id);
        } else {
            $conf = new HidepriceConfiguration((int)$object->id);
        }
        if (Validate::isLoadedObject($conf)) {
            $features = Feature::getFeatures((int)$this->context->cookie->id_lang);
            $attributeGroups = AttributeGroup::getAttributesGroups((int)$this->context->cookie->id_lang);

            $array_features_result = array();
            $array_attributes_result = array();

            foreach ($features as $f) {
                if (Tools::getValue('feature_'.$f['id_feature'])) {
                    $array_features_result[$f['id_feature']] = implode(';', Tools::getValue('feature_'.$f['id_feature']));
                }
            }

            /*foreach ($attributeGroups as $a) {
                if (Tools::getValue('attribute_'.$a['id_attribute_group'])) {
                    $array_attributes_result[$a['id_attribute_group']] = implode(';', Tools::getValue('attribute_'.$a['id_attribute_group']));
                }
            }*/
            if (!empty($array_features_result)) {
                $conf->features = Tools::jsonEncode($array_features_result);
            } else {
                $conf->features = '';
            }

            /*if (!empty($array_attributes_result)) {
                $conf->attributes = Tools::jsonEncode($array_attributes_result);
            } else {
                $conf->attributes = '';
            }*/
            $conf->save();
        }
        return true;
    }


    public function processChangeDisallowPurchaseVal()
    {
        $conf = new HidepriceConfiguration($this->id_object);
        if (!Validate::isLoadedObject($conf)) {
            $this->errors[] = Tools::displayError('An error occurred while updating Virtual POS information.');
        }
        $conf->disallow_purchase = $conf->disallow_purchase ? 0 : 1;
        if (!$conf->update()) {
            $this->errors[] = Tools::displayError('An error occurred while updating customer information.');
        }
        Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token);
    }

    public function processChangeHidePriceVal()
    {
        $conf = new HidepriceConfiguration($this->id_object);
        if (!Validate::isLoadedObject($conf)) {
            $this->errors[] = Tools::displayError('An error occurred while updating Virtual POS information.');
        }
        $conf->hide_price = $conf->hide_price ? 0 : 1;
        if (!$conf->update()) {
            $this->errors[] = Tools::displayError('An error occurred while updating customer information.');
        }
        Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token);
    }

    public function printDisallowPurchaseIcon($value, $conf)
    {
        if (version_compare(_PS_VERSION_, '1.6', '>=')) {
            return '<a class="list-action-enable '.($value ? 'action-enabled' : 'action-disabled').'" href="index.php?'.htmlspecialchars('tab=AdminHidePrice&id_hideprice_configuration='.(int)$conf['id_hideprice_configuration'].'&changeDisallowPurchaseVal&token='.Tools::getAdminTokenLite('AdminHidePrice')).'">
                '.($value ? '<i class="icon-check"></i>' : '<i class="icon-remove"></i>').
                '</a>';
        } else {
            return '<a class="list-action-enable '.($value ? 'action-enabled' : 'action-disabled').'" href="index.php?'.htmlspecialchars('tab=AdminHidePrice&id_hideprice_configuration='.(int)$conf['id_hideprice_configuration'].'&changeDisallowPurchaseVal&token='.Tools::getAdminTokenLite('AdminHidePrice')).'">
                '.($value ? '<img src="../img/admin/enabled.gif">' : '<img src="../img/admin/disabled.gif">').
                '</a>';
        }
    }

    public function printHidePriceIcon($value, $conf)
    {
        if (version_compare(_PS_VERSION_, '1.6', '>=')) {
            return '<a class="list-action-enable '.($value ? 'action-enabled' : 'action-disabled').'" href="index.php?'.htmlspecialchars('tab=AdminHidePrice&id_hideprice_configuration='.(int)$conf['id_hideprice_configuration'].'&changeHidePriceVal&token='.Tools::getAdminTokenLite('AdminHidePrice')).'">
                    '.($value ? '<i class="icon-check"></i>' : '<i class="icon-remove"></i>').
                '</a>';
        } else {
            return '<a class="list-action-enable '.($value ? 'action-enabled' : 'action-disabled').'" href="index.php?'.htmlspecialchars('tab=AdminHidePrice&id_hideprice_configuration='.(int)$conf['id_hideprice_configuration'].'&changeHidePriceVal&token='.Tools::getAdminTokenLite('AdminHidePrice')).'">
                '.($value ? '<img src="../img/admin/enabled.gif">' : '<img src="../img/admin/disabled.gif">').
                '</a>';
        }
    }

    /**
     * @param string $token
     * @param int $id
     * @param string $name
     * @return mixed
     */
    public function displayDeleteLink($token = null, $id, $name = null)
    {
        $tpl = $this->createTemplate('helpers/list/list_action_delete.tpl');

        $tpl->assign(array(
            'href' => self::$currentIndex.'&'.$this->identifier.'='.$id.'&delete'.$this->table.'&token='.($token != null ? $token : $this->token),
            'confirm' => $this->l('Delete the selected item? ').$name,
            'action' => $this->l('Delete'),
            'id' => $id,
        ));

        return $tpl->fetch();
    }

    public function getPriceOptions()
    {
        $price_options = array($this->l('Wholesale Price without Taxes'), $this->l('Retail Price Without Taxes'), $this->l('Wholesale Price with Taxes'), $this->l('Retail Price with Taxes'));

        $list_price_options = array();
        foreach ($price_options as $key => $mode) {
            $list_price_options[$key]['id'] = $key;
            $list_price_options[$key]['value'] = $key;
            $list_price_options[$key]['name'] = $mode;
        }
        return $list_price_options;
    }

    public function getCustomerGroups($ids_customer_groups)
    {
        if ($ids_customer_groups === '' || $ids_customer_groups === 'all') {
            return $this->l('All');
        }
        $groups = array();
        $groups_array = explode(';', $ids_customer_groups);
        foreach ($groups_array as $key => $group) {
            if ($key == $this->top_elements_in_list) {
                $groups[] = $this->l('...and more');
                break;
            }
            $group = new Group($group, $this->context->language->id);
            $groups[] = $group->name;
        }
        return implode('<br />', $groups);
    }

    public function getCountries($ids_countries)
    {
        if ($ids_countries === '' || $ids_countries === 'all') {
            return $this->l('All');
        }
        $countries = array();
        $countries_array = explode(';', $ids_countries);
        foreach ($countries_array as $key => $country) {
            if ($key == $this->top_elements_in_list) {
                $countries[] = $this->l('...and more');
                break;
            }
            $country = new Country($country, $this->context->language->id);
            $countries[] = $country->name;
        }
        return implode('<br />', $countries);
    }

    public function getZones($ids_zones)
    {
        if ($ids_zones === '' || $ids_zones === 'all') {
            return $this->l('All');
        }
        $zones = array();
        $zones_array = explode(';', $ids_zones);
        foreach ($zones_array as $key => $zone) {
            if ($key == $this->top_elements_in_list) {
                $zones[] = $this->l('...and more');
                break;
            }
            $zone = new Zone($zone, $this->context->language->id);
            $zones[] = $zone->name;
        }
        return implode('<br />', $zones);
    }

    public function getSuppliers($ids_suppliers)
    {
        if ($ids_suppliers === '' || $ids_suppliers === 'all') {
            return $this->l('All');
        }

        $suppliers = array();
        $suppliers_array = explode(';', $ids_suppliers);
        foreach ($suppliers_array as $key => $supplier) {
            if ($key == $this->top_elements_in_list) {
                $suppliers[] = $this->l('...and more');
                break;
            }
            $supplier = new Supplier($supplier);
            $suppliers[] = $supplier->name;
        }
        return implode('<br />', $suppliers);
    }


    public function getCategories($ids_categories)
    {
        if ($ids_categories === '' || $ids_categories === 'all') {
            return $this->l('All');
        }

        $categories = array();

        if (@unserialize($ids_categories) !== false) {
            $categories_array = unserialize($ids_categories);
        } else {
            $categories_array = explode(';', $ids_categories);
        }

        foreach ($categories_array as $key => $category) {
            if ($key == $this->top_elements_in_list) {
                $categories[] = $this->l('...and more');
                break;
            }
            $category = new Category($category, $this->context->language->id);
            $categories[] = $category->name;
        }
        return implode('<br />', $categories);
    }

    public function getCurrencies($ids_currencies)
    {
        if ($ids_currencies === '' || $ids_currencies === 'all') {
            return $this->l('All');
        }
        $currencies = array();
        $currencies_array = explode(';', $ids_currencies);
        foreach ($currencies_array as $key => $currency) {
            if ($key == $this->top_elements_in_list) {
                $currencies[] = $this->l('...and more');
                break;
            }
            $currency = new Currency($currency);
            $currencies[] = $currency->name;
        }
        return implode('<br />', $currencies);
    }

    public function getProducts($ids_products)
    {
        if ($ids_products === '' || $ids_products === 'all') {
            return $this->l('All');
        }
        $products = array();
        $products_array = explode(';', $ids_products);
        foreach ($products_array as $key => $product) {
            if ($key == $this->top_elements_in_list) {
                $products[] = $this->l('...and more');
                break;
            }
            $product = new Product($product, $this->context->language->id);
            $products[] = '['.$product->id.'] - '.$product->name[$this->context->language->id];
        }
        return implode('<br />', $products);
    }

    public function getCustomers($ids_customers)
    {
        if ($ids_customers === '' || $ids_customers === 'all') {
            return $this->l('All');
        }
        $customers = array();
        $customers_array = explode(';', $ids_customers);
        foreach ($customers_array as $key => $customer) {
            if ($key == $this->top_elements_in_list) {
                $customers[] = $this->l('...and more');
                break;
            }
            $customer = new Customer($customer, $this->context->language->id);
            $customers[] = $customer->firstname.' '.$customer->lastname;
        }
        return implode('<br />', $customers);
    }

    private function _createTemplate($tpl_name)
    {
        if ($this->override_folder) {
            if ($this->context->controller instanceof ModuleAdminController) {
                $override_tpl_path = $this->context->controller->getTemplatePath().$tpl_name;
            } elseif ($this->module) {
                $override_tpl_path = _PS_MODULE_DIR_.$this->module_name.'/views/templates/admin/'.$tpl_name;
            } else {
                if (file_exists($this->context->smarty->getTemplateDir(1).DIRECTORY_SEPARATOR.$this->override_folder.$this->base_folder.$tpl_name)) {
                    $override_tpl_path = $this->context->smarty->getTemplateDir(1).DIRECTORY_SEPARATOR.$this->override_folder.$this->base_folder.$tpl_name;
                } elseif (file_exists($this->context->smarty->getTemplateDir(0).DIRECTORY_SEPARATOR.'controllers'.DIRECTORY_SEPARATOR.$this->override_folder.$this->base_folder.$tpl_name)) {
                    $override_tpl_path = $this->context->smarty->getTemplateDir(0).'controllers'.DIRECTORY_SEPARATOR.$this->override_folder.$this->base_folder.$tpl_name;
                }
            }
        } else if ($this->module) {
            $override_tpl_path = _PS_MODULE_DIR_.$this->module_name.'/views/templates/admin/'.$tpl_name;
        }
        if (isset($override_tpl_path) && file_exists($override_tpl_path)) {
            return $this->context->smarty->createTemplate($override_tpl_path, $this->context->smarty);
        } else {
            return $this->context->smarty->createTemplate($tpl_name, $this->context->smarty);
        }
    }

    private function _formValidations()
    {
        if (trim(Tools::getValue('name')) == '') {
            $this->validateRules();
            $this->errors[] = Tools::displayError($this->l('Field Name can not be empty.'));
            $this->display = 'edit';
        }

        if (Tools::getValue('date_from') > 0) {
            if (!Validate::isDate(Tools::getValue('date_from'))) {
                $this->errors[] = $this->l('Invalid "Date From" format');
                $this->display = 'edit';
            }
        }

        if (Tools::getValue('date_to') > 0) {
            if (!Validate::isDate(Tools::getValue('date_to'))) {
                $this->errors[] = $this->l('Invalid "Date To" format');
                $this->display = 'edit';
            }
        }

        if (count($this->errors)) {
            return $this->errors;
        }
        return array();
    }

    public function getProtocolUrl()
    {
        if (isset($_SERVER['HTTPS'])) {
            $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
        } else {
            $protocol = 'http';
        }

        return $protocol . "://";
    }

    public function printValidIcon($value, $conf)
    {
        $today = date("Y-m-d H:i:s");
        $date_title = '';

        if ($conf['date_from'] > $today) {
            $date_title = $this->l("Future rule");
            if ($conf['date_from'] > "1970-01-01 00:00:01") {
                $date_title = $date_title.'. '.$this->l("Begins in:").' '.$conf['date_from'];
            }
            if (version_compare(_PS_VERSION_, '1.6', '<')) {
                return '<span class="time-column future-date" title="'.$date_title.'"></span>';
            } else {
                return '<span class="time-column future-date-icon" title="'.$date_title.'"><i class="icon-time"></i></span>';
            }
        }

        if ($conf['date_to'] <= "1970-01-01 00:00:01" || $today < $conf['date_to']) {
            $date_title = $this->l("Valid rule");
            if ($conf['date_from'] != "1970-01-01 00:00:01" && $conf['date_to'] > "1970-01-01 00:00:01") {
                $date_title = $date_title.'. '.$this->l("From:").' '.$conf['date_from'].'. '.$this->l("Until:").' '.$conf['date_to'];
            } else if ($conf['date_from'] > "1970-01-01 00:00:01" && $conf['date_to'] <= "1970-01-01 00:00:01") {
                $date_title = $date_title.'. '.$this->l("From:").' '.$conf['date_from'].' ('.$this->l("no expires").')';
            } else if ($conf['date_from'] <= "1970-01-01 00:00:01" && $conf['date_to'] > "1970-01-01 00:00:01") {
                $date_title = $date_title.'. '.$this->l("Until:").' '.$conf['date_to'];
            } else {
                $date_title = $date_title.' ('.$this->l("no expires").')';
            }

            if (version_compare(_PS_VERSION_, '1.6', '<')) {
                return '<span class="time-column valid-date" title="'.$date_title.'"></span>';
            } else {
                return '<span class="time-column valid-date-icon" title="'.$date_title.'"><i class="icon-time"></i></span>';
            }
        } else {
            $date_title = $this->l("Expired rule");
            if ($conf['date_from'] > "1970-01-01 00:00:01" && $conf['date_to'] > "1970-01-01 00:00:01") {
                $date_title = $date_title.'. '.$this->l("Between:").' '.$conf['date_from'].' '.$this->l("and:").' '.$conf['date_to'];
            } else {
                $date_title = $date_title.'. '.$this->l("From:").' '.$conf['date_to'];
            }
            if (version_compare(_PS_VERSION_, '1.6', '<')) {
                return '<span class="time-column expired-date" title="'.$date_title.'"></span>';
            } else {
                return '<span class="time-column expired-date-icon" title="'.$date_title.'"><i class="icon-time"></i></span>';
            }
        }
    }

    protected function getProductsLite($id_lang, $only_active = false, $front = false, Context $context = null)
    {
        if (!$context)
            $context = Context::getContext();

        $sql = 'SELECT p.`id_product`, CONCAT(p.`reference`, " - ", pl.`name`) as name FROM `'._DB_PREFIX_.'product` p
                '.Shop::addSqlAssociation('product', 'p').'
                LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` '.Shop::addSqlRestrictionOnLang('pl').')
                WHERE pl.`id_lang` = '.(int)$id_lang.
                    ($front ? ' AND product_shop.`visibility` IN ("both", "catalog")' : '').
                    ($only_active ? ' AND product_shop.`active` = 1' : '');

        $rq = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);

        return ($rq);
    }

    protected function getNumProducts($id_lang, $only_active = false, $front = false, Context $context = null)
    {
        if (!$context)
            $context = Context::getContext();

        $sql = 'SELECT count(p.`id_product`) as num_products FROM `'._DB_PREFIX_.'product` p
                '.Shop::addSqlAssociation('product', 'p').'
                LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` '.Shop::addSqlRestrictionOnLang('pl').')
                WHERE pl.`id_lang` = '.(int)$id_lang.
                    ($front ? ' AND product_shop.`visibility` IN ("both", "catalog")' : '').
                    ($only_active ? ' AND product_shop.`active` = 1' : '');

        $rq = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql);
        return ($rq);
    }

    protected function cleanCache() {
        /* delete smarty cache to refresh the static blocks */
        Tools::clearSmartyCache();
        Tools::clearXMLCache();
        Media::clearCache();
        if (version_compare(_PS_VERSION_, '1.7', '<') && version_compare(_PS_VERSION_, '1.4', '>')) {
            PrestaShopAutoload::getInstance()->generateIndex();
        } else if (version_compare(_PS_VERSION_, '1.7', '>=')) {
            Tools::generateIndex();
        }
    }
}
