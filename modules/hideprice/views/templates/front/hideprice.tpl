{*
 * Hide prices by categories, groups and more
 *
 * This product is licensed for one customer to use on one installation (test stores and multishop included).
 * Site developer has the right to modify this module to suit their needs, but can not redistribute the module in
 * whole or in part. Any other use of this module constitues a violation of the user agreement.
 *
 * NO WARRANTIES OF DATA SAFETY OR MODULE SECURITY
 * ARE EXPRESSED OR IMPLIED. USE THIS MODULE IN ACCORDANCE
 * WITH YOUR MERCHANT AGREEMENT, KNOWING THAT VIOLATIONS OF
 * PCI COMPLIANCY OR A DATA BREACH CAN COST THOUSANDS OF DOLLARS
 * IN FINES AND DAMAGE A STORES REPUTATION. USE AT YOUR OWN RISK.
 *
 *  @author    idnovate.com <info@idnovate.com>
 *  @copyright 2019 idnovate
 *  @license   See above
*}

<script type="text/javascript">
    if (document.addEventListener) {
        window.addEventListener('load', moveText, false);
    }
</script>

<div class="hidePriceText_{$id_product}">{$text|escape:'quotes':'UTF-8' nofilter}</div>

<script type="text/javascript">
	id_product = "{$id_product}";

	function moveText()
	{
		if ($('#buy_block').length > 0) {
			$('.hidePriceText_'+id_product).prependTo("#buy_block");
			if ($('.hidePriceText_'+id_product).length > 0) {
				$('.hidePriceText_'+id_product).prependTo("#buy_block");
			}
		}
	}
</script>