{*
 * Hide prices by categories, groups and more
 *
 * This product is licensed for one customer to use on one installation (test stores and multishop included).
 * Site developer has the right to modify this module to suit their needs, but can not redistribute the module in
 * whole or in part. Any other use of this module constitues a violation of the user agreement.
 *
 * NO WARRANTIES OF DATA SAFETY OR MODULE SECURITY
 * ARE EXPRESSED OR IMPLIED. USE THIS MODULE IN ACCORDANCE
 * WITH YOUR MERCHANT AGREEMENT, KNOWING THAT VIOLATIONS OF
 * PCI COMPLIANCY OR A DATA BREACH CAN COST THOUSANDS OF DOLLARS
 * IN FINES AND DAMAGE A STORES REPUTATION. USE AT YOUR OWN RISK.
 *
 *  @author    idnovate.com <info@idnovate.com>
 *  @copyright 2019 idnovate
 *  @license   See above
*}


{if isset($text)}
<div class="hidePriceText">{$text|escape:'quotes':'UTF-8' nofilter}</div>
{/if}

<script type="text/javascript">
{if ($remove_button)}
	remove_button = "{$remove_button|escape:'quotes':'UTF-8'}";
{/if}

var waitForJQuery = setInterval(function () {
    if (typeof $ != 'undefined') {
		if ($('.product-information').length > 0) {
			if ($('.product-information .hidePriceText').length == 1) {
				$('.product-additional-info .hidePriceText').prependTo(".product-information");
			} else if ($('.product-information .hidePriceText').length > 1) {
				$('.product-additional-info .hidePriceText').hide();
			}
		}
		if (remove_button == 1) {
			$('.product-actions .product-add-to-cart').html('');
		}

        clearInterval(waitForJQuery);
    }
}, 10);


</script>