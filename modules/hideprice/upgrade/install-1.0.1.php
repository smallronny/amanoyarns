<?php
/**
* Hide prices by categories, groups and more
*
* NOTICE OF LICENSE
*
* This product is licensed for one customer to use on one installation (test stores and multishop included).
* Site developer has the right to modify this module to suit their needs, but can not redistribute the module in
* whole or in part. Any other use of this module constitues a violation of the user agreement.
*
* DISCLAIMER
*
* NO WARRANTIES OF DATA SAFETY OR MODULE SECURITY
* ARE EXPRESSED OR IMPLIED. USE THIS MODULE IN ACCORDANCE
* WITH YOUR MERCHANT AGREEMENT, KNOWING THAT VIOLATIONS OF
* PCI COMPLIANCY OR A DATA BREACH CAN COST THOUSANDS OF DOLLARS
* IN FINES AND DAMAGE A STORES REPUTATION. USE AT YOUR OWN RISK.
*
*  @author    idnovate.com <info@idnovate.com>
*  @copyright 2016 idnovate.com
*  @license   See above
*/

function upgrade_module_1_0_1($module)
{
    Db::getInstance()->execute(
        'ALTER TABLE `'._DB_PREFIX_.'hideprice_configuration`
        ADD `filter_store` tinyint(1) unsigned,
        ADD `filter_stock` tinyint(1) unsigned,
        ADD `min_stock` int(10) NULL,
        ADD `max_stock` int(10) NULL,
        ADD `filter_prices` tinyint(1) unsigned,
        ADD `price_calculate` tinyint(1) unsigned,
		ADD `min_price` decimal(10,3) NULL DEFAULT "0.000",
        ADD `max_price` decimal(10,3) NULL DEFAULT "0.000";'
    );

    $module->uninstallOverrides();
	$module->installOverrides();

    return $module;
}
