<?php
/**
* Hide prices by categories, groups and more
*
* NOTICE OF LICENSE
*
* This product is licensed for one customer to use on one installation (test stores and multishop included).
* Site developer has the right to modify this module to suit their needs, but can not redistribute the module in
* whole or in part. Any other use of this module constitues a violation of the user agreement.
*
* DISCLAIMER
*
* NO WARRANTIES OF DATA SAFETY OR MODULE SECURITY
* ARE EXPRESSED OR IMPLIED. USE THIS MODULE IN ACCORDANCE
* WITH YOUR MERCHANT AGREEMENT, KNOWING THAT VIOLATIONS OF
* PCI COMPLIANCY OR A DATA BREACH CAN COST THOUSANDS OF DOLLARS
* IN FINES AND DAMAGE A STORES REPUTATION. USE AT YOUR OWN RISK.
*
*  @author    idnovate.com <info@idnovate.com>
*  @copyright 2016 idnovate.com
*  @license   See above
*/

function upgrade_module_1_0_2($module)
{
    Db::getInstance()->execute(
        'ALTER TABLE `'._DB_PREFIX_.'hideprice_configuration`
        ADD `show_text` tinyint(1) unsigned;'
    );

	Db::getInstance()->Execute('
        CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'hideprice_configuration_lang` (
            `id_hideprice_configuration` int unsigned NOT NULL,
            `id_lang` int unsigned NOT NULL,
            `alternative_text` TEXT NULL,
        PRIMARY KEY (`id_hideprice_configuration`, `id_lang`),
        KEY `id_hideprice_configuration` (`id_hideprice_configuration`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');

    $module->uninstallOverrides();
	$module->installOverrides();

    return $module;
}
