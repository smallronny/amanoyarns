<?php
/**
* Hide prices by categories, groups and more
*
* NOTICE OF LICENSE
*
* This product is licensed for one customer to use on one installation (test stores and multishop included).
* Site developer has the right to modify this module to suit their needs, but can not redistribute the module in
* whole or in part. Any other use of this module constitues a violation of the user agreement.
*
* DISCLAIMER
*
* NO WARRANTIES OF DATA SAFETY OR MODULE SECURITY
* ARE EXPRESSED OR IMPLIED. USE THIS MODULE IN ACCORDANCE
* WITH YOUR MERCHANT AGREEMENT, KNOWING THAT VIOLATIONS OF
* PCI COMPLIANCY OR A DATA BREACH CAN COST THOUSANDS OF DOLLARS
* IN FINES AND DAMAGE A STORES REPUTATION. USE AT YOUR OWN RISK.
*
*  @author    idnovate
*  @copyright 2019 idnovate
*  @license   See above
*/

if (!defined('_PS_VERSION_'))
    exit;
if (!defined('_CAN_LOAD_FILES_')) {
    exit;
}

include_once(_PS_MODULE_DIR_.'hideprice/classes/HidePriceConfiguration.php');

class Hideprice extends Module
{
    private $errors = array();
    private $success;

    public function __construct()
    {
        $this->name = 'hideprice';
        $this->tab = 'front_office_features';
        $this->version = '1.1.1';
        $this->author = 'idnovate';
        $this->module_key = '0a2012c3afc1c70e6d2d53db6a0429c1';
        $this->addons_id_product = '26993';

        parent::__construct();

        $this->displayName = $this->l('Hide the product prices and disallow purchases by categories, groups, and more');
        $this->description = $this->l('Hide the product prices and disallow purchases by categories, groups, and more');

        $this->tabs[] = array(
            'class_name' => 'AdminHidePrice',
            'name' => $this->l('Admin HidePrice'),
            'visible' => false
        );

        /* Backward compatibility */
        if (version_compare(_PS_VERSION_, '1.5', '<'))
            require(_PS_MODULE_DIR_.$this->name.'/backward_compatibility/backward.php');
    }


    public function copyOverrideFolder()
    {
        $version_override_folder = _PS_MODULE_DIR_.$this->name.'/override_'.Tools::substr(str_replace('.', '', _PS_VERSION_), 0, 2);
        $override_folder = _PS_MODULE_DIR_.$this->name.'/override';

        if (file_exists($override_folder) && is_dir($override_folder)) {
            $this->recursiveRmdir($override_folder);
        }

        if (is_dir($version_override_folder)) {
            $this->copyDir($version_override_folder, $override_folder);
        }

        return true;
    }

    protected function copyDir($src, $dst)
    {
        if (is_dir($src)) {
            $dir = opendir($src);
            @mkdir($dst);
            while (false !== ($file = readdir($dir))) {
                if (($file != '.') && ($file != '..')) {
                    if (is_dir($src.'/'.$file)) {
                        $this->copyDir($src.'/'.$file,$dst.'/'.$file);
                    } else {
                        copy($src.'/'.$file,$dst.'/'.$file);
                    }
                }
            }
            closedir($dir);
        }
    }

    protected function recursiveRmdir($dir)
    {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dir."/".$object) == "dir"){
                        $this->recursiveRmdir($dir."/".$object);
                    }else{
                        unlink($dir."/".$object);
                    }
                }
            }
            reset($objects);
            rmdir($dir);
        }
    }

    public function reset()
    {
        if (!$this->uninstall(false))
            return false;
        if (!$this->install(false))
            return false;

        return true;
    }


    public function install()
    {
        $this->copyOverrideFolder();

        if (!parent::install()
            || !$this->initSQL()
            || !$this->installTabs()
            || !$this->registerHook('header')
            || (version_compare(_PS_VERSION_, '1.7', '<') && version_compare(_PS_VERSION_, '1.5', '>') && !$this->registerHook('displayProductPriceBlock'))
            || (version_compare(_PS_VERSION_, '1.6', '<') && !$this->registerHook('displayProductButtons'))
            || (version_compare(_PS_VERSION_, '1.7', '>=') && !$this->registerHook('displayProductListReviews'))
            || (version_compare(_PS_VERSION_, '1.7', '>=') && !$this->registerHook('displayProductAdditionalInfo'))) {
            return false;
        }

        return true;
    }

    public function uninstall()
    {
        return parent::uninstall()
            && $this->uninstallTabs()
            && $this->uninstallSQL();
    }

    public function hookDisplayHeader()
    {
        if ($this->checkRulesExist()) {
            $this->context->controller->addCSS($this->_path.'views/css/front.css');
        }
    }

    public function hookdisplayProductListReviews($params)
    {
        if ($params) {
            $id_product = $this->getIdProduct($params);

            $conf = $this->getConfigByProduct($id_product);
            if (!empty($conf)) {
                $context = Context::getContext();
                $id_lang = $context->cart->id_lang;
                if ($conf->show_text && $conf->alternative_text[$id_lang] != '') {
                    $this->context->smarty->assign(array(
                        'text' => $conf->alternative_text[$id_lang],
                        'id_product' => $id_product,
                    ));

                    return $this->display(__FILE__, 'views/templates/front/hideprice_miniatures.tpl');
                }
            }
        }
    }

    public function hookdisplayProductAdditionalInfo($params)
    {
        if ($params) {
            $id_product = $this->getIdProduct($params);

            $conf = $this->getConfigByProduct($id_product);
            if ($conf) {
                $context = Context::getContext();
                $id_lang = $context->cart->id_lang;
                if ($conf->show_text && $conf->alternative_text[$id_lang] != '') {
                    $this->context->smarty->assign(array(
                        'text' => $conf->alternative_text[$id_lang],
                        'id_product' => $id_product,
                    ));
                }
                $this->context->smarty->assign(array(
                    'remove_button' => $conf->disallow_purchase
                ));

                return $this->display(__FILE__, 'views/templates/front/hideprice17.tpl');
            }
        }
    }

    public function hookDisplayProductPriceBlock($params)
    {
        if ($params['type'] == 'weight') {
            return $this->hookDisplayProductButtons($params);
        }
    }

    public function hookDisplayProductButtons($params)
    {
        if ($params) {
            $id_product = $this->getIdProduct($params);

            $conf = $this->getConfigByProduct($id_product);
            if ($conf) {
                $context = Context::getContext();
                $id_lang = $context->cart->id_lang;
                if ($conf->show_text && $conf->alternative_text[$id_lang] != '') {
                    $this->context->smarty->assign(array(
                        'text' => $conf->alternative_text[$id_lang],
                        'id_product' => $id_product,
                    ));
                    return $this->display(__FILE__, 'views/templates/front/hideprice.tpl');
                }
            }
        }
    }

    public function getIdProduct($params)
    {
        if (version_compare(_PS_VERSION_, '1.7', '<')) {
            $id_product = isset($params['id_product']) ? $params['id_product'] : isset($params['product']->id) ? $params['product']->id : 0;
        } else {
            $id_product = isset($params['id_product']) ? $params['id_product'] : isset($params['product']['id_product']) ? $params['product']['id_product'] : 0;
        }

        if ((int)$id_product == 0) {
            if (isset($params['product'])) {
                if (is_array($params['product'])) {
                    $id_product = $params['product']['id_product'];
                } else {
                    $id_product = $params['product']->id;
                }
            }
        }

        return $id_product;
    }

    public function getConfigByProduct($id_product)
    {
        include_once(_PS_MODULE_DIR_.'hideprice/classes/HidePriceConfiguration.php');
        $hideprice = new HidepriceConfiguration();

        $configs = array();
        if ($id_product) {
            $configs = $hideprice->getConfigurations($id_product, 0);
        }

        if (!empty($configs)) {
            $conf = new HidePriceConfiguration($configs['id_hideprice_configuration']);
            return $conf;
        }

        return false;
    }

    public function getContent()
    {
        $this->initSQL();
        // check if the tab was not created in the installation
        foreach ($this->tabs as $myTab) {
            $id_tab = Tab::getIdFromClassName($myTab['class_name']);
            if (!$id_tab) {
                $this->addTab($myTab);
            }
        }

        $warnings_to_show = '';
        if (version_compare(_PS_VERSION_, '1.6', '>=')) {
            if (Configuration::get('PS_DISABLE_NON_NATIVE_MODULE')) {
                $warnings_to_show = $warnings_to_show . $this->displayError($this->l('You have to disable the option Disable non native modules at ADVANCED PARAMETERS - PERFORMANCE'));
            }

            if (Configuration::get('PS_DISABLE_OVERRIDES')) {
                $warnings_to_show = $warnings_to_show . $this->displayError($this->l('You have to disable the option Disable all overrides at ADVANCED PARAMETERS - PERFORMANCE'));
            }
        }

        if (!empty($warnings_to_show)) {
            $this->context->smarty->assign(array(
                'performance_link' => $this->context->link->getAdminLink('AdminPerformance'),
            ));
            return $warnings_to_show . $this->display(__FILE__, 'views/templates/admin/admin_warnings.tpl');
        }



        return Tools::redirectAdmin('index.php?controller=' . $this->tabs[0]['class_name'] . '&token=' . Tools::getAdminTokenLite($this->tabs[0]['class_name']));
    }

    public function installTabs()
    {
        if (version_compare(_PS_VERSION_, '1.7.1', '>=')) {
            return true;
        }

        foreach ($this->tabs as $myTab) {
            $this->addTab($myTab);
        }
        return true;
    }

    public function addTab($myTab)
    {
        $id_tab = Tab::getIdFromClassName($myTab['class_name']);
        if (!$id_tab) {
            $tab = new Tab();
            $tab->class_name = $myTab['class_name'];
            $tab->module = $this->name;

            if (isset($myTab['parent_class_name'])) {
                $tab->id_parent = Tab::getIdFromClassName($myTab['parent_class_name']);
            } else {
                $tab->id_parent = -1;
            }

            $languages = Language::getLanguages(false);
            foreach ($languages as $lang) {
                $tab->name[$lang['id_lang']] = $myTab['name'];
            }

            $tab->add();
        }
    }

    public function uninstallTabs()
    {
        if (version_compare(_PS_VERSION_, '1.7.1', '>=')) {
            return true;
        }

        foreach ($this->tabs as $myTab) {
            $idTab = Tab::getIdFromClassName($myTab['class_name']);
            if ($idTab) {
                $tab = new Tab($idTab);
                $tab->delete();
            }
        }

        return true;
    }

    protected function initSQL()
    {
        Db::getInstance()->Execute('
            CREATE TABLE IF NOT EXISTS `'.pSQL(_DB_PREFIX_.$this->name).'_configuration` (
                `id_hideprice_configuration` int(10) unsigned NOT NULL auto_increment,
                `name` VARCHAR(100) NULL,
                `hide_price` tinyint(1) unsigned NOT NULL DEFAULT "0",
                `hide_price_status` tinyint(1) unsigned NOT NULL DEFAULT "0",
                `disallow_purchase` tinyint(1) unsigned NOT NULL DEFAULT "0",
                `groups` TEXT NULL,
                `customers` TEXT NULL,
                `products` TEXT NULL,
                `countries` TEXT NULL,
                `zones` TEXT NULL,
                `categories` TEXT NULL,
                `manufacturers` TEXT NULL,
                `suppliers` TEXT NULL,
                `languages` TEXT NULL,
                `currencies` TEXT NULL,
                `features` TEXT NULL,
                `attributes` TEXT NULL,
                `filter_prices` tinyint(1) unsigned,
                `price_calculate` tinyint(1) unsigned,
                `min_price` decimal(10,3) NULL DEFAULT "0.000",
                `max_price` decimal(10,3) NULL DEFAULT "0.000",
                `filter_store` tinyint(1) unsigned,
                `filter_stock` tinyint(1) unsigned,
                `min_stock` int(10) NULL,
                `max_stock` int(10) NULL,
                `active` tinyint(1) unsigned NOT NULL DEFAULT "0",
                `priority` int(1) unsigned DEFAULT "0",
                `id_shop` tinyint(1) unsigned NOT NULL DEFAULT "0",
                `show_text` tinyint(1) unsigned NOT NULL DEFAULT "0",
                `date_from` DATETIME NULL DEFAULT NULL,
                `date_to` DATETIME NULL DEFAULT NULL,
                `date_add` DATETIME NULL DEFAULT NULL,
                `date_upd` DATETIME NULL DEFAULT NULL,
                `filter_weight` tinyint(1) unsigned,
                `min_weight` decimal(10,3) NULL DEFAULT "0.000",
                `max_weight` decimal(10,3) NULL DEFAULT "0.000",
                `schedule` TEXT NULL,
                `products_excluded` TEXT NULL,
                `customers_excluded` TEXT NULL,
            PRIMARY KEY (`id_hideprice_configuration`),
            KEY `id_hideprice_configuration` (`id_hideprice_configuration`)
            ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;');

         Db::getInstance()->Execute('
            CREATE TABLE IF NOT EXISTS `'.pSQL(_DB_PREFIX_.$this->name).'_configuration_lang` (
                `id_hideprice_configuration` int unsigned NOT NULL,
                `id_lang` int unsigned NOT NULL,
                `alternative_text` TEXT NULL,
            PRIMARY KEY (`id_hideprice_configuration`, `id_lang`),
            KEY `id_hideprice_configuration` (`id_hideprice_configuration`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');

        return true;
    }

    protected function uninstallSQL()
    {
        Db::getInstance()->Execute('DROP TABLE IF EXISTS `'.pSQL(_DB_PREFIX_.$this->name).'_configuration`');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS `'.pSQL(_DB_PREFIX_.$this->name).'_configuration_lang`');
        return true;
    }

    protected function checkRulesExist()
    {
        $id_shop = Context::getContext()->shop->id;

        $query = '
                SELECT conf.* FROM `'._DB_PREFIX_.'hideprice_configuration` conf WHERE conf.`id_shop` = '.(int)$id_shop.'
                AND conf.`active` = 1';
        $rules = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($query);

        return ($rules == false) ? false : true;
    }

    public function getMessageAvailable($name)
    {
        $message = sprintf($this->l('An item (%1s) in your cart is not available to purchase. You cannot proceed with your order until you remove the product from cart.'), $name);
        if (Module::isInstalled('onepagecheckoutps')) {
            $onepagecheckoutps = Module::getInstanceByName('onepagecheckoutps');
            if ($onepagecheckoutps->active && $onepagecheckoutps->core->isVisible()) {
                return $message;
            }
        }

        return Tools::displayError($message);
    }
}