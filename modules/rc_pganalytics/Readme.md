# Premium Google Analytics Enhanced Ecommerce

## Description

This module allows you to track your site with Google Analytics Enhanced Ecommerce on Prestashop

## Features

- Google Analytics Enhanced Ecommerce
- Scroll Tracking
- Dynamic Remarketing
- AdWords Conversions

## Links
  
  - [online guide](https://docs.reactioncode.com/en/modules/prestashop/premium-google-analytics-enhanced-ecommerce/)
  - [support](https://addons.prestashop.com/contact-form.php?id_product=18623)