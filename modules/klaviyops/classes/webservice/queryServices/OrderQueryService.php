<?php
/**
 * Klaviyo
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Commercial License
 * you can't distribute, modify or sell this code
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file
 * If you need help please contact extensions@klaviyo.com
 *
 * @author    Klaviyo
 * @copyright Klaviyo
 * @license   commercial
 */

namespace KlaviyoPs\Classes\Webservice\QueryServices;

use Db;
use Order;

use KlaviyoPs\Classes\BusinessLogicServices\OrderPayloadService;

class OrderQueryService extends QueryServiceInterface
{
    /**
     * @inheritDoc
     */
    protected function getTableName()
    {
        return 'orders';
    }

    /**
     * Retrieve and build single order by its ID.
     *
     * @param $orderId
     * @return array
     * @throws \PrestaShopDatabaseException
     * @throws \PrestaShopException
     */
    public function getObjectById($orderId)
    {
        $order = new Order($orderId);
        return OrderPayloadService::buildPayload($order);
    }

    /**
     * Fetch order IDs for a given time range.
     *
     * @param $queryParams
     * @throws \WebserviceException
     */
    public function getObjectsByTimeRange()
    {
        $this->records = $this->getTimeRangeRecords('id_order');

        $cursorValue = $this->getCursorValue();
        $returnRecords = $this->buildTimeRangeReturnRecords(
            'Order',
            'id_order',
            'KlaviyoPs\Classes\BusinessLogicServices\OrderPayloadService'
        );

        return $this->buildTimeRangeResultsPayload($returnRecords, $cursorValue);
    }
}
