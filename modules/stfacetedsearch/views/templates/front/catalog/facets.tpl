{**
  * 2007-2019 PrestaShop.
  *
  * NOTICE OF LICENSE
  *
  * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
  * that is bundled with this package in the file LICENSE.txt.
  * It is also available through the world-wide-web at this URL:
  * https://opensource.org/licenses/AFL-3.0
  * If you did not receive a copy of the license and are unable to
  * obtain it through the world-wide-web, please send an email
  * to license@prestashop.com so we can send you a copy immediately.
  *
  * DISCLAIMER
  *
  * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
  * versions in the future. If you wish to customize PrestaShop for your
  * needs please refer to http://www.prestashop.com for more information.
  *
  * @author    PrestaShop SA <contact@prestashop.com>
  * @copyright 2007-2019 PrestaShop SA
  * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
  * International Registered Trademark & Property of PrestaShop SA
  *}
{if $displayedFacets|count}
{assign var="feds_showmore_watcher" value=0}
{assign var="feds_show_quanbu_watcher" value=0}
{assign var='loading_effect' value=Configuration::get('ST_FAC_SEARCH_LOADING_EFFECT')}
{assign var='show_on_mobile' value=Configuration::get('ST_FAC_SEARCH_SHOW_ON_MOBILE')}
  <div id="search_filters" class="feds_show_on_mobile_{$show_on_mobile}">
    {if $loading_effect==3}<div class="feds_overlay feds_overlay_center stfeds_flex_container stfeds_flex_center feds_overlay_hide feds_overlay_click"><i class="feds_overlay_loader feds-spin5 feds_animate-spin"></i></div>{/if}
    {block name='facets_title'}
      {if $template.title}<div class="feds_block_title">{$template.title}</div>{/if}
    {/block}
    {foreach $displayedFacets as $jon}
      {if !isset($jon.properties.facet_item.zhuangtai) || $jon.properties.facet_item.zhuangtai!=2}{continue}{/if}
      {foreach $activeFilters as $ny}
        {if $ny.type==$jon.type}
          {if $ny.type=='feature'}
            {if $ny.properties.id_key==$jon.properties.id_feature}{$feds_show_quanbu_watcher=1}{break}{/if}
          {elseif $ny.type=='attribute_group'}
            {if $ny.properties.id_key==$jon.properties.id_attribute_group}{$feds_show_quanbu_watcher=1}{break}{/if}
          {else}
            {$feds_show_quanbu_watcher=1}{break}
          {/if}
        {/if}
      {/foreach}
    {/foreach}
    <div class="feds_block_content feds_showmore_box {if $feds_show_quanbu_watcher} feds_show_quanbu {/if}">
    {block name='facets_clearall_button'}
      {if $activeFilters|count}
        {include file='module:stfacetedsearch/views/templates/front/catalog/active-filters.tpl'}
      {/if}
    {/block}

    {if $show_on==1}
    <div class="row feds_grid_view">
    {/if}
    {if $show_on==2 && $drop_down_position}
    <div class="feds_dropdown_even">
    {/if}
    {foreach $displayedFacets as $facet}
        {assign var=_expand_id value=10|mt_rand:100000}
        {assign var=_collapse value=true}
        {foreach from=$facet.filters item="filter"}
          {if $filter.active}{assign var=_collapse value=false}{/if}
        {/foreach}
      {if !$feds_showmore_watcher && isset($facet.properties.facet_item) && $facet.properties.facet_item.zhuangtai==2}{$feds_showmore_watcher=1}{/if}
      {if $feds_showmore_watcher && $show_on==2}{break}{/if}

      {assign var="feds_colexp_quanbu_watcher" value=0}
      {if $show_on!=2 && in_array($facet.widgetType, ['radio', 'checkbox','colorbox','link','button','image'])}
        {foreach $facet.filters as $li}
          {if $li.active && isset($li.properties.zhuangtai) && $li.properties.zhuangtai}{$feds_colexp_quanbu_watcher=1}{break}{/if}
        {/foreach}
      {/if}

      {if $show_on==1}
      <div class="{if $filters_per_xl}col-xl-{(12/$filters_per_xl)|replace:'.':'-'}{/if} col-lg-{(12/$filters_per_lg)|replace:'.':'-'} col-md-{(12/$filters_per_md)|replace:'.':'-'} {if $filters_per_xl && $facet@iteration%$filters_per_xl == 1} feds_first-item-of-desktop-line{/if}{if $facet@iteration%$filters_per_lg == 1} feds_first-item-of-line{/if}{if $facet@iteration%$filters_per_md == 1} feds_first-item-of-tablet-line{/if}">
      {/if}

      <section class="feds_facet feds_zhuangtai_{if $show_on==2 || !isset($facet.properties.facet_item)}1{else}{$facet.properties.facet_item.zhuangtai}{/if} facet_title_colexp_{if isset($collapse) && $show_on!=2}{$collapse}{else}0{/if} 
      {if $_collapse}
      {if isset($facet.properties.facet_item.collapsed) && $facet.properties.facet_item.collapsed} facet_coled_1 {/if} 
      {if isset($facet.properties.facet_item.collapsed_mobile) && $facet.properties.facet_item.collapsed_mobile} facet_coled {/if} 
      {/if}
      {if isset($facet.properties.facet_item.pips) && $facet.properties.facet_item.pips} facet_pips {/if} 
      clearfix facet_type_{$facet.widgetType} feds_facet_{$facet.properties.id_st_search_filter}">
        {if $show_on==2}
            <div class="feds_dropdown_wrap facet_feds_dropdown_item">
              <div class="feds_dropdown_tri feds_dropdown_tri_in stfeds_flex_container" aria-haspopup="true" aria-expanded="false">
                  <span class="stfeds_flex_child">{$facet.label}
                    {foreach from=$activeFilters item="filter"}
                      {if $filter.type==$facet.type}
                        {if $filter.type=='feature'}
                          {if $filter.properties.id_key==$facet.properties.id_feature}<span class="feds_dropdown_active_label">{l s=' - ' mod='stfacetedsearch'}{$filter.label}</span>{/if}
                        {elseif $filter.type=='attribute_group'}
                          {if $filter.properties.id_key==$facet.properties.id_attribute_group}<span class="feds_dropdown_active_label">{l s=' - ' mod='stfacetedsearch'}{$filter.label}</span>{/if}
                        {else}
                          <span class="feds_dropdown_active_label">{l s=' - ' mod='stfacetedsearch'}{$filter.label}</span>
                        {/if}
                      {/if}
                    {/foreach}
                  </span>
                  <i class="feds-angle-down feds_arrow_down feds_arrow"></i>
                  <i class="feds-angle-up feds_arrow_up feds_arrow"></i>
              </div>
              <div class="feds_dropdown_list {if !$_collapse} feds_dropdown_kai {/if}" aria-labelledby="{$facet.label}">
                  <div class="facet_title facet-title-mobile stfeds_flex_container">
                    <div class="facet_title_text stfeds_flex_child">{$facet.label}</div>
                    <span class="facet_colexp_icons">
                      <i class="feds-plus-2 facet_exped_kai"></i>
                      <i class="feds-minus facet_exped_guan"></i>
                    </span>
                  </div>
                  {if in_array($facet.widgetType, ['radio', 'checkbox','colorbox','link'])}
                    <div id="facet_{$_expand_id}" class="facet_colexp_block feds_filter_{if isset($facet.properties.facet_item)}{$facet.properties.facet_item.id_st_search_facet_item}{else}0{/if} facet_with_max_height feds_showmore_box {if $feds_colexp_quanbu_watcher} feds_show_quanbu {/if} {if $facet.widgetType=='colorbox'} stfeds_flex_box {/if}">
                      {include file='module:stfacetedsearch/views/templates/front/catalog/facets-input.tpl'}
                    </div>
                  {elseif $facet.widgetType == 'button' || $facet.widgetType == 'image'}
                    <div id="facet_{$_expand_id}" class="facet_colexp_block feds_filter_{if isset($facet.properties.facet_item)}{$facet.properties.facet_item.id_st_search_facet_item}{else}0{/if} facet_with_max_height feds_showmore_box {if $feds_colexp_quanbu_watcher} feds_show_quanbu {/if} {if $facet.properties.facet_item.per_row==0} stfeds_flex_box {/if}">
                      {include file='module:stfacetedsearch/views/templates/front/catalog/facets-button.tpl'}
                    </div>
                  {elseif $facet.widgetType == 'dropdown'}
                    <div id="facet_{$_expand_id}" class="facet_colexp_block feds_filter_{if isset($facet.properties.facet_item)}{$facet.properties.facet_item.id_st_search_facet_item}{else}0{/if} ">
                      {include file='module:stfacetedsearch/views/templates/front/catalog/facets-select.tpl'}
                    </div>
                  {elseif $facet.widgetType == 'slider'}
                    <div class="st-range-box facet_colexp_block feds_filter_{if isset($facet.properties.facet_item)}{$facet.properties.facet_item.id_st_search_facet_item}{else}0{/if}  st-noUi-style-{$range_style}" id="facet_{$_expand_id}">
                      {include file='module:stfacetedsearch/views/templates/front/catalog/facets-range-slider.tpl'}
                    </div>
                  {/if}
              </div>
            </div>
          {else}  
        <div class="facet_title stfeds_flex_container">
          <div class="facet_title_text stfeds_flex_child">{$facet.label}</div>
          <span class="facet_colexp_icons">
            <i class="feds-plus-2 facet_exped_kai"></i>
            <i class="feds-minus facet_exped_guan"></i>
          </span>
        </div>
        {if in_array($facet.widgetType, ['radio', 'checkbox', 'colorbox', 'link'])}
          {block name='facet_item_other'}
          <div id="facet_{$_expand_id}" class="facet_colexp_block feds_filter_{if isset($facet.properties.facet_item)}{$facet.properties.facet_item.id_st_search_facet_item}{else}0{/if} facet_with_max_height feds_showmore_box {if $feds_colexp_quanbu_watcher} feds_show_quanbu {/if} {if $facet.widgetType=='colorbox'} stfeds_flex_box {/if}">
            {include file='module:stfacetedsearch/views/templates/front/catalog/facets-input.tpl'}
          </div>
          {/block}
        {elseif $facet.widgetType == 'button' || $facet.widgetType == 'image'}
          <div id="facet_{$_expand_id}" class="facet_colexp_block feds_filter_{if isset($facet.properties.facet_item)}{$facet.properties.facet_item.id_st_search_facet_item}{else}0{/if} facet_with_max_height feds_showmore_box {if $feds_colexp_quanbu_watcher} feds_show_quanbu {/if} {if $facet.properties.facet_item.per_row==0} stfeds_flex_box {/if}">
            {include file='module:stfacetedsearch/views/templates/front/catalog/facets-button.tpl'}
          </div>
        {elseif $facet.widgetType == 'dropdown'}
          {block name='facet_item_dropdown'}
            <div id="facet_{$_expand_id}" class="facet_colexp_block feds_filter_{if isset($facet.properties.facet_item)}{$facet.properties.facet_item.id_st_search_facet_item}{else}0{/if}">
            {include file='module:stfacetedsearch/views/templates/front/catalog/facets-select.tpl'}
            </div>
          {/block}

        {elseif $facet.widgetType == 'slider'}
          {block name='facet_item_slider'}
              <div class="st-range-box facet_colexp_block feds_filter_{if isset($facet.properties.facet_item)}{$facet.properties.facet_item.id_st_search_facet_item}{else}0{/if} st-noUi-style-{$range_style}" id="facet_{$_expand_id}">
                {include file='module:stfacetedsearch/views/templates/front/catalog/facets-range-slider.tpl'}
              </div>
          {/block}
        {/if}
      {/if}
      </section>
      {if $show_on==1}
      </div>
      {/if}
    {/foreach}
    {if $feds_showmore_watcher && $show_on==2}
      <section class="feds_facet feds_zhuangtai_1 feds_facet_x">
        <div class="feds_dropdown_wrap facet_feds_dropdown_item">
          <div class="feds_dropdown_tri feds_dropdown_tri_in stfeds_flex_container" aria-haspopup="true" aria-expanded="false">
              <span class="stfeds_flex_child">{l s='Others' mod='stfacetedsearch'}</span>
              <i class="feds-angle-down feds_arrow_down feds_arrow"></i>
              <i class="feds-angle-up feds_arrow_up feds_arrow"></i>
          </div>
          <div class="feds_dropdown_list {if !$_collapse} feds_dropdown_kai {/if}" aria-labelledby="{l s='Others' mod='stfacetedsearch'}">
              {foreach $displayedFacets as $facet}
              {if !isset($facet.properties.facet_item) || $facet.properties.facet_item.zhuangtai!=2}{continue}{/if}
              <div class="{if isset($facet.properties.facet_item.pips) && $facet.properties.facet_item.pips} facet_pips {/if} 
              facet_type_{$facet.widgetType} feds_facet_{$facet.properties.id_st_search_filter} feds_facet_other_blocks facet_title_colexp_{if isset($collapse)}{$collapse}{else}0{/if} 
              {if $_collapse}
              {if isset($facet.properties.facet_item.collapsed) && $facet.properties.facet_item.collapsed} facet_coled_1 {/if} 
              {if isset($facet.properties.facet_item.collapsed_mobile) && $facet.properties.facet_item.collapsed_mobile} facet_coled {/if}
              {/if} ">
              <div class="facet_title stfeds_flex_container">
                <div class="facet_title_text stfeds_flex_child">{$facet.label}</div>
                <span class="facet_colexp_icons">
                  <i class="feds-plus-2 facet_exped_kai"></i>
                  <i class="feds-minus facet_exped_guan"></i>
                </span>
              </div>
              {if in_array($facet.widgetType, ['radio', 'checkbox','colorbox','link'])}
                <div id="facet_{$_expand_id}" class="facet_colexp_block feds_filter_{if isset($facet.properties.facet_item)}{$facet.properties.facet_item.id_st_search_facet_item}{else}0{/if} facet_with_max_height feds_showmore_box {if $feds_colexp_quanbu_watcher} feds_show_quanbu {/if} {if $facet.widgetType=='colorbox'} stfeds_flex_box {/if}">
                  {include file='module:stfacetedsearch/views/templates/front/catalog/facets-input.tpl'}
                </div>
              {elseif $facet.widgetType == 'button' || $facet.widgetType == 'image'}
                <div id="facet_{$_expand_id}" class="facet_colexp_block feds_filter_{if isset($facet.properties.facet_item)}{$facet.properties.facet_item.id_st_search_facet_item}{else}0{/if} facet_with_max_height feds_showmore_box {if $feds_colexp_quanbu_watcher} feds_show_quanbu {/if} {if $facet.properties.facet_item.per_row==0} stfeds_flex_box {/if}">
                  {include file='module:stfacetedsearch/views/templates/front/catalog/facets-button.tpl'}
                </div>
              {elseif $facet.widgetType == 'dropdown'}
                <div id="facet_{$_expand_id}" class="facet_colexp_block feds_filter_{if isset($facet.properties.facet_item)}{$facet.properties.facet_item.id_st_search_facet_item}{else}0{/if} ">
                  {include file='module:stfacetedsearch/views/templates/front/catalog/facets-select.tpl'}
                </div>
              {elseif $facet.widgetType == 'slider'}
                <div class="st-range-box facet_colexp_block feds_filter_{if isset($facet.properties.facet_item)}{$facet.properties.facet_item.id_st_search_facet_item}{else}0{/if}  st-noUi-style-{$range_style}" id="facet_{$_expand_id}">
                  {include file='module:stfacetedsearch/views/templates/front/catalog/facets-range-slider.tpl'}
                </div>
              {/if}
              </div>
              {/foreach}
          </div>
        </div>
      </section>
    {/if}
    {if $show_on==1}
    </div>
    {/if}
    {if $show_on==2 && $drop_down_position}
    </div>
    {/if}
    {if $feds_showmore_watcher && $show_on!=2}<div class="feds_showmore feds_facet_showmore"><div class="stfeds_flex_container stfeds_flex_center"><span class="feds_text_showmore">{l s='Show more' mod='stfacetedsearch'}</span><span class="feds_text_showless">{l s='Show less' mod='stfacetedsearch'}</span><i class="feds-angle-up feds_text_showless"></i><i class="feds-angle-down feds_text_showmore"></i></div></div>{/if}
  </div>
  <div class="feds_st_btn">
  <div class="stfeds_flex_container">
    <a href="{$clear_all_link}" title="{l s='Reset' mod='stfacetedsearch'}" class="btn btn-default feds_link stfeds_flex_child mar_r6">{l s='Reset' mod='stfacetedsearch'}</a>
    <a href="javascript:;" title="{l s='Done' mod='stfacetedsearch'}" class="btn btn-default close_right_side stfeds_flex_child">{l s='Done' mod='stfacetedsearch'}</a>
  </div>
  </div>
  </div>
{/if}
