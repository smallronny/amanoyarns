{assign var="per_row_index" value=0}
{assign var="feds_showmore_watcher" value=0}
{if isset($facet.properties.facet_item) && in_array($facet.widgetType, ['radio', 'checkbox','button','image','link']) && ($facet.properties.facet_item.per_row>0 || $facet.properties.facet_item.per_row_mobile>0)}<div class="feds_grid_view row">{/if}
              {foreach from=$facet.filters key=filter_key item="filter"}
                {if !$filter.displayed || $filter.properties.zhuangtai==2}
                  {continue}
                {/if}
                {if !$feds_showmore_watcher && $filter.properties.zhuangtai==1}{$feds_showmore_watcher=1}{/if}
                {if isset($facet.properties.facet_item) && in_array($facet.widgetType, ['radio', 'checkbox','button','image','link']) && ($facet.properties.facet_item.per_row>0 || $facet.properties.facet_item.per_row_mobile>0)}
                {$per_row_index = $per_row_index+1}
                <div class="{if $facet.properties.facet_item.per_row>0} col-md-{(12/$facet.properties.facet_item.per_row)|replace:'.':'-'} {if ($per_row_index)%$facet.properties.facet_item.per_row == 1} feds_first-item-of-descktop-line{/if} {/if} {if $facet.properties.facet_item.per_row_mobile>0} col-{(12/$facet.properties.facet_item.per_row_mobile)|replace:'.':'-'} col-xs-{(12/$facet.properties.facet_item.per_row_mobile)|replace:'.':'-'} {if ($per_row_index)%$facet.properties.facet_item.per_row_mobile == 1} feds_first-item-of-mobile-line{/if} {/if}">
                {/if}

                <div class="facet_filter_item_li feds_facet_item feds_item_{$filter.type}_{$filter.value}">
                  <div class="filter_zhuangtai feds_zhuangtai_{if $filter.properties.zhuangtai==1}2{else}1{/if}">
                    <div class="{if in_array($facet.widgetType, ['radio', 'checkbox'])} stfeds_flex_container {/if}">{strip}
                  <label class="facet-label checkbox-inline {if $filter.active} active {/if}" for="facet_input_{$_expand_id}_{$filter_key}">
                      <span class="feds_custom-input-box">
                        <input
                          id="facet_input_{$_expand_id}_{$filter_key}"
                          data-url="{$filter.nextEncodedFacetsURL}"
                          type="{if $facet.multipleSelectionAllowed}checkbox{else}radio{/if}"
                          title="{$filter.label} {if $filter.magnitude and $show_quantities}({$filter.magnitude}){/if}"
                          name="filter {$facet.label}"
                          class="feds_custom-input feds_input feds_custom-input-{if isset($filter.properties.color) || isset($filter.properties.texture)}yanse{else}default{/if}"
                          {if $filter.active } checked {/if}
                        >
                        {if isset($filter.properties.color)}
                          <span class="feds_custom-input-item feds_custom-input-color" style="background-color:{$filter.properties.color}"><i class="feds-ok-1 checkbox-checked"></i><i class="feds-spin5 feds_animate-spin"></i></span>
                          {elseif isset($filter.properties.texture)}
                            <span class="feds_custom-input-item feds_custom-input-color texture" style="background-image:url({$filter.properties.texture})"><i class="feds-ok-1 checkbox-checked"></i><i class="feds-spin5 feds_animate-spin"></i></span>
                          {else}
                        <span class="feds_custom-input-item feds_custom-input-{if $facet.multipleSelectionAllowed}checkbox{else}radio{/if} {if !$js_enabled} ps-shown-by-js {/if}"><i class="feds-ok-1 checkbox-checked"></i><i class="feds-spin5 feds_animate-spin"></i></span>
                        {/if}
                      </span>
                  </label>
                    <a
                      href="{$filter.nextEncodedFacetsURL}"
                      class="feds_link  {if $filter.active} active {/if}"
                      rel="nofollow"
                    >
                      {$filter.label}
                      {if $filter.magnitude and $show_quantities}
                        <span class="magnitude">({$filter.magnitude})</span>
                      {/if}
                    </a>
                  {/strip}</div>
                  </div>
                </div>
                {if isset($facet.properties.facet_item) && in_array($facet.widgetType, ['radio', 'checkbox','button','image','link']) && ($facet.properties.facet_item.per_row>0 || $facet.properties.facet_item.per_row_mobile>0)}</div>{/if}
              {/foreach}
              {if isset($facet.properties.facet_item) && in_array($facet.widgetType, ['radio', 'checkbox','button','image','link']) && ($facet.properties.facet_item.per_row>0 || $facet.properties.facet_item.per_row_mobile>0)}</div>{/if}
              {if $feds_showmore_watcher}<div class="feds_showmore"><div class="filter_zhuangtai"><a href="javascript:;" class="feds_showmore_button" title="{l s='+More' mod='stfacetedsearch'}"><span class="feds_text_showmore">{l s='+More' mod='stfacetedsearch'}</span><span class="feds_text_showless">{l s='-Less' mod='stfacetedsearch'}</span></a></div></div>{/if}