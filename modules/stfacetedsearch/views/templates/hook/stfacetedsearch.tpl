{if isset($listing.rendered_facets)}
	{assign var='show_on' value=Configuration::get('ST_FAC_SEARCH_SHOW_ON')}
	<div id="feds_search_filters" class="feds_show_on_{$show_on} {if $show_on!=2} feds_show_on_x {/if}">
  	{$listing.rendered_facets nofilter}
	</div>
{/if}
