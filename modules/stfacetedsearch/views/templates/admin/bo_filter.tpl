<li class="filter_list_item filter_list_{if isset($filter['filter_type'])}{$filter['filter_type']}{else}0{/if}" draggable="true">
          <input type="hidden" name="{$filterId}_leixin" value="{$filter['leixin']}" autocomplete="off"  />
          <input type="hidden" name="{$filterId}_item_k" value="{if isset($filter['item_k'])}{$filter['item_k']}{else}0{/if}" autocomplete="off"  />
          <input type="hidden" class="kaiguan" name="{$filterId}" id="{$filterId}" autocomplete="off" value="{if isset($filter['filter_type'])}on{/if}" />
          <input type="hidden" class="filter_zhuangtai" name="{$filterId}_filter_zhuangtai" autocomplete="off" value="{if isset($filter['zhuangtai'])}{$filter['zhuangtai']}{else}0{/if}" />
            <div class="st_flex_container">
          <div class="">
            <i class="icon-move main_move i_btn"></i>
            <i class="icon-arrow-right main_move i_btn"></i>
            <i class="icon-arrow-left main_move i_btn"></i>
          </div>

          <div class="st_flex_child filter_list_label">
              <a href="javascript:;" class="st_toggle_sub">{l s=$filter['label'] d='Modules.Facetedsearch.Admin'}<i class="icon-edit"></i></a>
          </div>

          <div class="hidden_item">
            {if !empty($filter['slider'])}
              <p class="form-control-static">{l s='Range slider' d='Modules.Facetedsearch.Admin'}</p>
            {else}
                <select autocomplete="off" class="bo_fiter_type" name="{$filterId}_filter_type">
                  <option value="0" {if isset($filter['filter_type']) && $filter['filter_type']==0} selected {/if}>{l s='Checkbox' d='Modules.Facetedsearch.Admin'}</option>
                  <option value="4" {if isset($filter['filter_type']) && $filter['filter_type']==4} selected {/if}>{l s='Button' d='Modules.Facetedsearch.Admin'}</option>
                  {if $filter['leixin']=='ag' || $filter['leixin']=='feat'}
                  <option value="3" {if isset($filter['filter_type']) && $filter['filter_type']==3} selected {/if}>{l s='Range slider' d='Modules.Facetedsearch.Admin'}</option>
                  <option value="5" {if isset($filter['filter_type']) && $filter['filter_type']==5} selected {/if}>{l s='Color box' d='Modules.Facetedsearch.Admin'}</option>
                  <option value="6" {if isset($filter['filter_type']) && $filter['filter_type']==6} selected {/if}>{l s='Image' d='Modules.Facetedsearch.Admin'}</option>
                  {/if}
                  <option value="1" {if isset($filter['filter_type']) && $filter['filter_type']==1} selected {/if}>{l s='Radio button' d='Modules.Facetedsearch.Admin'}</option>
                  <!-- <option value="7" {if isset($filter['filter_type']) && $filter['filter_type']==7} selected {/if}>{l s='Link' d='Modules.Facetedsearch.Admin'}</option> -->
                  <option value="2" {if isset($filter['filter_type']) && $filter['filter_type']==2} selected {/if}>{l s='Drop-down list' d='Modules.Facetedsearch.Admin'}</option>
                </select>
            {/if}
          </div>
          <div class="hidden_item">
          </div>
          </div>

          <div class="fliter_sub_content">

            <div class="row fliter_sub_settings">
              <div class="col-sm-4 ft_item ft_3 ft_4 ft_5 ft_6">
                {l s='Multiple choice' mod='stfactedsearch'}
                <label class="switch-light prestashop-switch fixed-width-lg">
                  <input name="{$filterId}_multi" type="checkbox" autocomplete="off" value="1" {if (isset($filter['multi']) && $filter['multi']) || !isset($filter['multi'])} checked="checked" {/if} />
                  <span>
                    <span>{l s='Yes' d='Admin.Global'}</span>
                    <span>{l s='No' d='Admin.Global'}</span>
                  </span>
                  <a class="slide-button btn"></a>
                </label>
              </div>
              <div class="col-sm-4 ft_item ft_0 ft_1 ft_7 ft_3 ft_4 ft_6">
                {l s='Collapsed on desktop' mod='stfactedsearch'}
                <label class="switch-light prestashop-switch fixed-width-lg">
                  <input name="{$filterId}_collapsed" type="checkbox" autocomplete="off" value="1" {if isset($filter['collapsed']) && $filter['collapsed']} checked="checked" {/if} />
                  <span>
                    <span>{l s='Yes' d='Admin.Global'}</span>
                    <span>{l s='No' d='Admin.Global'}</span>
                  </span>
                  <a class="slide-button btn"></a>
                </label>
              </div>
              <div class="col-sm-4  ft_item ft_0 ft_1 ft_7 ft_3 ft_4 ft_6">
                {l s='Collapsed on mobile' mod='stfactedsearch'}
                <label class="switch-light prestashop-switch fixed-width-lg">
                  <input name="{$filterId}_collapsed_mobile" type="checkbox" autocomplete="off" value="1" {if isset($filter['collapsed_mobile']) && $filter['collapsed_mobile']} checked="checked" {/if} />
                  <span>
                    <span>{l s='Yes' d='Admin.Global'}</span>
                    <span>{l s='No' d='Admin.Global'}</span>
                  </span>
                  <a class="slide-button btn"></a>
                </label>
              </div>
              <div class="col-sm-4">
                {l s='Filter block background' mod='stfactedsearch'}
                <div class="input-group">
                    <input type="color"
                    data-hex="true"
                    class="color"
                    name="{$filterId}_bg"
                    value="{if isset($filter['bg']) && $filter['bg']}{$filter['bg']}{/if}" />
                  </div>
              </div>

              <div class="col-sm-4 ft_item ft_3">
                {l s='Show pips when in range slider' mod='stfactedsearch'}
                  <label class="switch-light prestashop-switch fixed-width-lg">
                    <input name="{$filterId}_pips" type="checkbox" autocomplete="off" value="1" {if isset($filter['pips']) && $filter['pips']} checked="checked" {/if} />
                    <span>
                      <span>{l s='Yes' d='Admin.Global'}</span>
                      <span>{l s='No' d='Admin.Global'}</span>
                    </span>
                    <a class="slide-button btn"></a>
                  </label>
              </div>
              <div class="col-sm-4 ft_item ft_3">
                {l s='Show selected start value and end value for sliders' mod='stfactedsearch'}
                  <label class="switch-light prestashop-switch fixed-width-lg">
                    <input name="{$filterId}_snap" type="checkbox" autocomplete="off" value="1" {if (isset($filter['snap']) && $filter['snap']) || (!isset($filter['snap']) && !empty($filter['slider']))} checked="checked" {/if} />
                    <span>
                      <span>{l s='Yes' d='Admin.Global'}</span>
                      <span>{l s='No' d='Admin.Global'}</span>
                    </span>
                    <a class="slide-button btn"></a>
                  </label>
              </div>
              <div class="col-sm-4 ft_item ft_3">
                {l s='Show tooltips' mod='stfactedsearch'}
                  <label class="switch-light prestashop-switch fixed-width-lg">
                    <input name="{$filterId}_tooltips" type="checkbox" autocomplete="off" value="1" {if isset($filter['tooltips']) && $filter['tooltips']} checked="checked" {/if} />
                    <span>
                      <span>{l s='Yes' d='Admin.Global'}</span>
                      <span>{l s='No' d='Admin.Global'}</span>
                    </span>
                    <a class="slide-button btn"></a>
                  </label>
              </div>
              <div class="col-sm-4 ft_item">
                {l s='Numerical slider' mod='stfactedsearch'}
                  <label class="switch-light prestashop-switch fixed-width-lg">
                    <input name="{$filterId}_numerical" type="checkbox" autocomplete="off" value="1" {if isset($filter['numerical']) && $filter['numerical']} checked="checked" {/if} />
                    <span>
                      <span>{l s='Yes' d='Admin.Global'}</span>
                      <span>{l s='No' d='Admin.Global'}</span>
                    </span>
                    <a class="slide-button btn"></a>
                  </label>
              </div>
              <div class="col-sm-4 ft_item">
                {l s='Step for numerical slider' mod='stfactedsearch'}
                  <div class="input-group ">
                  <input type="text" name="{$filterId}_numerical_step" value="{if isset($filter['numerical_step'])}{(int)$filter['numerical_step']}{else}0{/if}">
                  </div>
              </div>
              <div class="col-sm-4 ft_item ft_3">
                {l s='Vertical slider' mod='stfactedsearch'}
                  <label class="switch-light prestashop-switch fixed-width-lg">
                    <input name="{$filterId}_vertical" type="checkbox" autocomplete="off" value="1" {if isset($filter['vertical']) && $filter['vertical']} checked="checked" {/if} />
                    <span>
                      <span>{l s='Yes' d='Admin.Global'}</span>
                      <span>{l s='No' d='Admin.Global'}</span>
                    </span>
                    <a class="slide-button btn"></a>
                  </label>
              </div>
              <div class="col-sm-4 ft_item ft_3">
                  {l s='Color for the bar between handles' mod='stfactedsearch'}
                  <div class="input-group">
                      <input type="color"
                      data-hex="true"
                      class="color"
                      name="{$filterId}_bar_bg_left"
                      value="" />
                    </div>
              </div>
              <div class="col-sm-4 ft_item ft_3">
                  {l s='Right side color for the bar between handles' mod='stfactedsearch'}
                  <div class="input-group">
                      <input type="color"
                      data-hex="true"
                      class="color"
                      name="{$filterId}_bar_bg_right"
                      value="" />
                    </div>
              </div>
              <div class="col-sm-4 ft_item ft_0 ft_1 ft_7 ft_4 ft_6">
                {l s='How many filter items per row on descktop' mod='stfactedsearch'}
                <select autocomplete="off" name="{$filterId}_per_row">
                  <option value="0" {if isset($filter['per_row']) && $filter['per_row']==0} selected {/if}>{l s='Auto' mod='stfactedsearch'}</option>
                  <option value="1" {if isset($filter['per_row']) && $filter['per_row']==1} selected {/if}>1</option>
                  <option value="2" {if isset($filter['per_row']) && $filter['per_row']==2} selected {/if}>2</option>
                  <option value="3" {if isset($filter['per_row']) && $filter['per_row']==3} selected {/if}>3</option>
                  <!-- <option value="4" {if isset($filter['per_row']) && $filter['per_row']==4} selected {/if}>4</option>
                  <option value="6" {if isset($filter['per_row']) && $filter['per_row']==6} selected {/if}>6</option> -->
                </select>
              </div>
              <div class="col-sm-4 ft_item ft_0 ft_1 ft_7 ft_4 ft_6">
                {l s='How many filter items per row on mobile' mod='stfactedsearch'}
                <select autocomplete="off" name="{$filterId}_per_row_mobile">
                  <option value="0" {if isset($filter['per_row_mobile']) && $filter['per_row_mobile']==0} selected {/if}>{l s='Auto' mod='stfactedsearch'}</option>
                  <option value="1" {if isset($filter['per_row_mobile']) && $filter['per_row_mobile']==1} selected {/if}>1</option>
                  <option value="2" {if isset($filter['per_row_mobile']) && $filter['per_row_mobile']==2} selected {/if}>2</option>
                  <option value="3" {if isset($filter['per_row_mobile']) && $filter['per_row_mobile']==3} selected {/if}>3</option>
                  <!-- <option value="4" {if isset($filter['per_row_mobile']) && $filter['per_row_mobile']==4} selected {/if}>4</option>
                  <option value="6" {if isset($filter['per_row_mobile']) && $filter['per_row_mobile']==6} selected {/if}>6</option> -->
                </select>
              </div>
              <div class="col-sm-4 ft_item ft_7 ft_4 ft_6">
                {l s='Filter item min width' mod='stfactedsearch'}
                <div class="input-group ">
                <span class="input-group-addon">px</span>
                <input type="text" name="{$filterId}_min_width" value="{if isset($filter['min_width'])}{(int)$filter['min_width']}{else}0{/if}">
                </div>
              </div>
              <div class="col-sm-4 ft_item ft_6">
                {l s='Image width' mod='stfactedsearch'}
                <div class="input-group ">
                <span class="input-group-addon">px</span>
                <input type="text" name="{$filterId}_img_width" value="{if isset($filter['img_width'])}{(int)$filter['img_width']}{else}0{/if}">
                </div>
              </div>
              <div class="col-sm-4 ft_item ft_6">
                {l s='How to show images' mod='stfactedsearch'}
                <select autocomplete="off" name="{$filterId}_show_img">
                  <option value="0" {if isset($filter['show_img']) && $filter['show_img']==0} selected {/if}>{l s='Left-right layout' mod='stfactedsearch'}</option>
                  <option value="1" {if isset($filter['show_img']) && $filter['show_img']==1} selected {/if}>{l s='Top-bottom layout' mod='stfactedsearch'}</option>
                  <option value="2" {if isset($filter['show_img']) && $filter['show_img']==2} selected {/if}>{l s='Show image only' mod='stfactedsearch'}</option>
                </select>
              </div>
              <div class="col-sm-4 ft_item ft_4 ft_6">
                {l s='Filter item text alignment' mod='stfactedsearch'}
                <select autocomplete="off" name="{$filterId}_text_align">
                  <option value="0" {if isset($filter['text_align']) && $filter['text_align']==0} selected {/if}>{l s='Center' mod='stfactedsearch'}</option>
                  <option value="1" {if isset($filter['text_align']) && $filter['text_align']==1} selected {/if}>{l s='Left' mod='stfactedsearch'}</option>
                </select>
              </div>
              <div class="col-sm-4 ft_item ft_0 ft_1 ft_2 ft_4 ft_5 ft_6">
                {l s='Filter item top & bottom margins' mod='stfactedsearch'}
                <div class="input-group ">
                <span class="input-group-addon">px</span>
                <input type="text" name="{$filterId}_tb_mar" value="{if isset($filter['tb_mar'])}{$filter['tb_mar']}{/if}">
                </div>
              </div>
              <div class="col-sm-4 ft_item ft_0 ft_1 ft_2 ft_4 ft_5 ft_6">
                {l s='Filter item left & right margins' mod='stfactedsearch'}
                <div class="input-group ">
                <span class="input-group-addon">px</span>
                <input type="text" name="{$filterId}_lr_mar" value="{if isset($filter['lr_mar'])}{$filter['lr_mar']}{/if}">
                </div>
              </div>
              <div class="col-sm-4 ft_item ft_0 ft_1 ft_4 ft_6">
                {l s='Filter item top & bottom paddings' mod='stfactedsearch'}
                <div class="input-group ">
                <span class="input-group-addon">px</span>
                <input type="text" name="{$filterId}_tb_pad" value="{if isset($filter['tb_pad'])}{$filter['tb_pad']}{else}2{/if}">
                </div>
              </div>
              <div class="col-sm-4 ft_item ft_0 ft_1 ft_4 ft_6">
                {l s='Filter item left & right paddings' mod='stfactedsearch'}
                <div class="input-group ">
                <span class="input-group-addon">px</span>
                <input type="text" name="{$filterId}_lr_pad" value="{if isset($filter['lr_pad'])}{$filter['lr_pad']}{/if}">
                </div>
              </div>
              <div class="col-sm-4 ft_item ft_0 ft_1 ft_2 ft_3 ft_4 ft_5 ft_6 ft_7">
                {l s='Filter block paddings' mod='stfactedsearch'}
                <div class="input-group ">
                <span class="input-group-addon">px</span>
                <input type="text" name="{$filterId}_pad" value="{if isset($filter['pad'])}{$filter['pad']}{/if}">
                </div>
              </div>

              <div class="col-sm-4 ft_item ft_4 ft_6">
                {l s='Filter item border size' mod='stfactedsearch'}
                <div class="input-group ">
                <span class="input-group-addon">px</span>
                <input type="text" name="{$filterId}_border" value="{if isset($filter['border'])}{$filter['border']}{/if}">
                </div>
              </div>
              {if $filter['leixin']=='ag' || $filter['leixin']=='feat' || $filter['leixin']=='subcategories' || $filter['leixin']=='manufacturer'}
              <div class="col-sm-4 ft_item ft_0 ft_1 ft_2 ft_3 ft_4 ft_5 ft_6 ft_7">
                {l s='Auto-sort sub items' mod='stfactedsearch'}
                  <label class="switch-light prestashop-switch fixed-width-lg">
                    <input name="{$filterId}_auto_sort" type="checkbox" autocomplete="off" value="1" {if (isset($filter['auto_sort']) && $filter['auto_sort']) || !isset($filter['auto_sort'])} checked="checked" {/if} />
                    <span>
                      <span>{l s='Yes' d='Admin.Global'}</span>
                      <span>{l s='No' d='Admin.Global'}</span>
                    </span>
                    <a class="slide-button btn"></a>
                  </label>
              </div>
              {/if}
              
            </div>

            {if !$id_st_search_filter}<div class="alert alert-info">{l s='To manage sub items, you need to save this filter group first.' mod='stfacetedsearch'}</div>{/if}
            {if isset($filter['childern'])}
            <div class="alert alert-info">{l s='If you want to sort sub items manually, then turn off the above Auto-sort option.' mod='stfacetedsearch'}</div>
            <ul class="list-unstyled sub_sortable">
              {foreach $filter['childern'] as $item}
              {if !isset($item['zhuangtai']) || $item['zhuangtai']==0}{include file='./bo_item.tpl' zhuangtai=0}{/if}
              {/foreach}
              <li class="filtered filtered_show_more">{l s='Items below will be hidden by default' mod='stfacetedsearch'}</li>
              {foreach $filter['childern'] as $item}
              {if isset($item['zhuangtai']) && $item['zhuangtai']==1}{include file='./bo_item.tpl' zhuangtai=1}{/if}
              {/foreach}
              <li class="filtered filtered_hidden">{l s='Items below will not shown out on the front end' mod='stfacetedsearch'}</li>
              {foreach $filter['childern'] as $item}
              {if isset($item['zhuangtai']) && $item['zhuangtai']==2}{include file='./bo_item.tpl' zhuangtai=2}{/if}
              {/foreach}
            </ul>
            {/if}
            </div>
        </li>