<section class="filter_panel">
  <header class="clearfix">
    <span class="badge pull-right">
      {l
        s='Total filter blocks: %s'
        sprintf=[$total_filters]
        d='Modules.Facetedsearch.Admin'
      }
    </span>
  </header>
  <section class="filter_list">
    <div class="row">
      <div class="col-xs-4">
        <h3>{l s='Available filter blocks' d='stfacetedsearch'}</h3>
        <ul class="list-unstyled sortable disabled_filters">
        {foreach from=$quanbu item=filter key=filterId}
          {if !isset($filter['filter_type'])}
            {include file='./bo_filter.tpl' filter=$filter}
          {/if}
        {/foreach} 
        </ul>   
      </div>
      <div class="col-xs-8">
        <h3>{l s='Enabled filter blocks' d='stfacetedsearch'}</h3>
        <ul class="list-unstyled sortable enabled_filters">
        {foreach from=$quanbu item=filter key=filterId}
          {if isset($filter['filter_type']) && $filter['zhuangtai']==1}
            {include file='./bo_filter.tpl' filter=$filter}
          {/if}
        {/foreach}     
        <li class="filtered filtered_show_more filter_list_item">{l s='Items below will be hidden by default' mod='stfacetedsearch'}</li>
        {foreach from=$quanbu item=filter key=filterId}
          {if isset($filter['filter_type']) && $filter['zhuangtai']==2}
            {include file='./bo_filter.tpl' filter=$filter}
          {/if}
        {/foreach}    
        </ul>   
      </div>
    </div>
  </section>
</section>

<script type="text/javascript">
  var translations = new Array();
  translations['no_selected_categories'] = "{l s='You must select at least one category' d='Modules.Facetedsearch.Admin'}";
  translations['no_selected_filters'] = "{l s='You must select at least one filter' d='Modules.Facetedsearch.Admin'}";
</script>