{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{extends file="helpers/form/form.tpl"}

{block name="field"}
	{if $input.type == 'dropdownlistgroup'}
		<div class="col-lg-{if isset($input.col)}{$input.col|intval}{else}9{/if} {if !isset($input.label)}col-lg-offset-3{/if} fontello_wrap">
			<div class="row">
				{foreach $input.values.medias AS $media}
					<div class="col-xs-4 col-sm-3">
						<label data-html="true" data-toggle="tooltip" class="label-tooltip" data-original-title="{if $media=='fw'}{l s='If this is set and this module is hooked to the displayFullWidthXXX hooks, then this module will be displayed in full screen.' mod='stfacetedsearch'}{elseif $media=='xxl'}{l s='Desktops (>1400px)' mod='stfacetedsearch'}{elseif $media=='xl'}{l s='Desktops (>1200px)' mod='stfacetedsearch'}{elseif $media=='lg'}{l s='Desktops (>992px)' mod='stfacetedsearch'}{elseif $media=='md'}{l s='Tablets (>768px)' mod='stfacetedsearch'}{elseif $media=='sm'}{l s='Phones (>544px)' mod='stfacetedsearch'}{elseif $media=='xs'}{l s='Phones (<544px)' mod='stfacetedsearch'}{/if}">{if $media=='fw'}{l s='Full screen' mod='stfacetedsearch'}{elseif $media=='xxl'}{l s='Extra large devices' mod='stfacetedsearch'}{elseif $media=='xl'}{l s='Large devices' mod='stfacetedsearch'}{elseif $media=='lg'}{l s='Medium devices' mod='stfacetedsearch'}{elseif $media=='md'}{l s='Small devices' mod='stfacetedsearch'}{elseif $media=='sm'}{l s='Extra small devices' mod='stfacetedsearch'}{elseif $media=='xs'}{l s='Extremely small devices' mod='stfacetedsearch'}{/if}</label>
						<select name="{$input.name}_{$media}" id="{$input.name}_{$media}" class="fixed-width-md">
						{if $media=='fw'}<option value="0" {if !$fields_value[$input['name']|cat:"_"|cat:$media]} selected="selected" {/if}></option>{/if}
            			{for $foo=1 to $input.values.maximum}
	                        <option value="{$foo}" {if $fields_value[$input['name']|cat:"_"|cat:$media] == $foo} selected="selected" {/if}>{$foo}</option>
	                    {/for}
            			</select>
					</div>
				{/foreach}
			</div>
			{if isset($input.desc) && !empty($input.desc)}
				<p class="help-block">
					{if is_array($input.desc)}
						{foreach $input.desc as $p}
							{if is_array($p)}
								<span id="{$p.id}">{$p.text}</span><br />
							{else}
								{$p}<br />
							{/if}
						{/foreach}
					{else}
						{$input.desc}
					{/if}
				</p>
			{/if}
		</div>
	{elseif $input.type == 'pc_images'}
		<div class="col-lg-9">
			<input name="{$input.name}" id="pc_images" type="hidden" value="{if isset($input.values.str)}{$input.values.str}{/if}" />
			<ul class="pc_images_list clearfix">
				{foreach $input.values.data as $img}
					<li>
						<img src="{$img.url}" alt="{$img.name}">
						<a href="javascript:;" class="pc_image_item" data-image="{$img.name}">{l s='Remove' mod='stfacetedsearch'}</a>
					</li>
				{/foreach}
			</ul>
		</div>
    {elseif $input.type == 'go_back_to_list'}
		<div class="col-lg-9">
			<a class="btn btn-default btn-block fixed-width-md" href="{$input.name}" title="{l s='Back to list' mod='stfacetedsearch'}"><i class="icon-arrow-left"></i> {l s='Back to list' mod='stfacetedsearch'}</a>
		</div>
	{elseif $input.type == 'show_bg_patterns'}
		<div class="col-lg-9">
			{for $foo=1 to $input.size}
				<div class="parttern_wrap" style="background:url({$input.name}{$foo}.png);"><span>{$foo}</span></div>
			{/for}
			<div>{l s='Pattern credits:' mod='stfacetedsearch'}<a href="http://subtlepatterns.com" target="_blank">subtlepatterns.com</a></div>
		</div>
	{else}
		{$smarty.block.parent}
	{/if}
{/block}