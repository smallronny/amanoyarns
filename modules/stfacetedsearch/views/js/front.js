jQuery(function($){
    stfs.init();
    prestashop.on('updateProductList', function(data) {
        if(typeof(stfacetdsearch.magic)!='undefined' && stfacetdsearch.magic){
            setTimeout(function(){
                stfs.overlay(0);
                stfs.init();
                stfs.facets_watcher=false;
            }, 200);
        }else{
            stfs.overlay(0);
            stfs.init();
            stfs.facets_watcher=false;
        }
    });
    prestashop.on('updateFacets', function(data) {
        stfs.overlay(1);
    });
    $(document).on('change', '.st_lower_input,.st_upper_input', function() {
        var val = $(this).val();
        var rang = $(this).closest('.st-range-box').find('.st-range');
        var jiazhong = rang.data('jiazhong');
        val = stfs.deFormatPrice(val,jiazhong);
        if($(this).hasClass('st_lower_input'))
            rang[0].noUiSlider.set([val, null]);
        else
            rang[0].noUiSlider.set([null, val]);
    });
    $(document).on('click', '.noUi-value', function() {
        var value = $(this).data('value');
        var rang = $(this).closest('.st-range-box').find('.st-range');
        if((rang.data('upper')-rang.data('lower'))/2>value)
            rang[0].noUiSlider.set([value, null]);
        else
            rang[0].noUiSlider.set([null, value]);
    });
    $(document).on('click', '.facet_title, .facet-title-mobile', function(){
        if($(this).closest('.feds_facet_other_blocks').length)
            var facet = $(this).closest('.feds_facet_other_blocks');
        else
            var facet = $(this).closest('.feds_facet');
        facet.toggleClass('facet_coled_1 facet_coled');
    });
    stfs.dropDown();
    $('body').on('change', '.feds_input', function (event) {
        if(stfs.guanli_watcher())
        {
          $(this).prop('checked', false);
          return false;
        }

        $(this).closest('.feds_facet_item').addClass('feds_loading');
        prestashop.emit('updateFacets', $(this).data('url'));
    });

    $('body').on('click', '.feds_link', function (event) {
        event.preventDefault();

        if(stfs.guanli_watcher())
          return false;

        $(this).closest('.feds_facet_item').addClass('feds_loading');
        prestashop.emit('updateFacets',$(this).attr('href'));
    });
    $('body').off('change', '#search_filters select');
    $('body').on('change', '.feds_select', function (event) {
        var url = $(this).val();
        if(url)
            prestashop.emit('updateFacets',url);
    });
    $(document).on('click', '.feds_offcanvas_tri,.side_feds_offcanvas_tri', function(){
        if(!$('#feds_offcanvas_search_filters #search_filters').length)
            $('#search_filters').appendTo($('#feds_offcanvas_search_filters'));
        $('body').addClass('feds_open');
    });
    $(document).on('click', '.feds_offcanvas_background, .feds_offcanvas_guan', function(){
        if($('#feds_search_filters').length)
            $('#search_filters').appendTo($('#feds_search_filters'));
        $('body').removeClass('feds_open');
    });
    $(document).on('click', '.feds_showmore', function(){
        $(this).closest('.feds_showmore_box').toggleClass('feds_show_quanbu');
    });
    $(document).on('click', '.feds_overlay_click', function(){
        stfs.overlay(0);
    });
});


var stfs = {
    'facets_watcher': false,
    'init': function(){
        $.each(this.jiazhong, function(k,v){
            if(!v.format.symbles.length)
                stfs.learnFormat(k);
        });
        this.run();
    },
    'jiazhong':{'price':{'url':'','format': {'prefix':'', 'suffix':'','symbles':[], 'sample':'', 'decimals':2}},'weight':{'url':'','format': {'prefix':'', 'suffix':'','symbles':[], 'sample':'', 'decimals':6}}},
    'run' : function(){
        this.init_slide();
    },
    'init_slide': function(){
        $.each(this.jiazhong, function(k,v){
            var slides = $('.st-range[data-jiazhong="'+k+'"]');
            if(slides.length)
            {
                $.each(slides, function(index,slide_dom){
                    stfs.chushihua(k, slide_dom);
                });
            }
        });
        var rangesliders = $('.st-range[data-jiazhong="rangeslider"]');
        if(rangesliders.length){
            $.each(rangesliders, function(index,slide_dom){
                stfs.chushihua('rangeslider', slide_dom);
            });
        }
    },
    'chushihua': function(k, slide_dom){
        var has_error = false;
        var slide = $(slide_dom);
        var has_lower = typeof slide.data('lower') !== 'undefined';
        var has_upper = typeof slide.data('upper') !== 'undefined';
        var snapValues = [];
        var inputValues = [];
        if(stfacetdsearch.with_inputs==1 && k!='rangeslider')
            inputValues = [
                slide.closest('.st-range-box').find('.st_lower_input'),
                slide.closest('.st-range-box').find('.st_upper_input')
            ];
        else{
            snapValues = [
                slide.closest('.st-range-box').find('.value-lower')                
            ];
            if(has_upper)
                snapValues.push(slide.closest('.st-range-box').find('.value-upper'));
        }

        var url = slide.data('url');
        var range_values = [];
        var range_values_orginal = [];
        var is_numerical = false;
        if(has_lower && has_upper)
            var params = {
                start: [parseFloat(slide.data('lower')) , parseFloat(slide.data('upper'))],
                connect: true
            };
        else
            var params = {
                start: parseFloat(slide.data('lower'))
            };
        var rangeslider_suffix = '';
        var rangeslider_prefix = '';
        if(slide.data('tooltips') && (k=='weight' || k=='price'))
            $.extend(params,{tooltips: [ stfs.fomater[k], stfs.fomater[k] ]});
        if(slide.data('vertical')==1)
            $.extend(params,{orientation: 'vertical'});
        if(k=='weight'){
            $.extend(params,{range: {'min': [ parseFloat(slide.data('min')) ],'max': [ parseFloat(slide.data('max')) ]}, format: { 'to': function( value ){return value !== undefined && value.toFixed(stfs.jiazhong[k].format.decimals);}, 'from': Number }});
            if(stfacetdsearch.weight_step>0)
                $.extend(params,{step: stfacetdsearch.weight_step});
            if(slide.data('pips')==1)
                $.extend(params,{'pips':{
                        mode: 'positions',
                        values: [0, 25, 50, 75, 100], 
                        density: 20,
                        format: { 'to': function( value ){return value !== undefined && value.toFixed(stfs.jiazhong[k].format.decimals);}, 'from': Number }
                    }});
        }
        else if(k=='price'){
            if(stfacetdsearch.price_step>0)
                $.extend(params,{step: stfacetdsearch.price_step});
            $.extend(params,{range: {'min': [ parseFloat(slide.data('min')) ],'max': [ parseFloat(slide.data('max')) ]}, format: { 'to': function( value ){return value !== undefined && value.toFixed(stfs.jiazhong[k].format.decimals);}, 'from': Number }});
            if(slide.data('pips')==1)
                $.extend(params,{'pips':{
                        mode: 'positions',
                        values: [0, 25, 50, 75, 100], 
                        density: 20,
                        format: { 'to': function( value ){return value !== undefined && value.toFixed(stfs.jiazhong[k].format.decimals);}, 'from': Number }
                    }});
        }else if(k=='rangeslider'){
            var range_values_string = slide.data('values')+'';
            var numerical_reg = /^([^\d]*)(.*?)([^\d]*)$/;
            rangeslider_suffix = slide.data('suffix');
            rangeslider_prefix = slide.data('prefix');
            if(range_values_string && range_values_string.indexOf('#')!=-1){
                range_values_orginal = slide.data('values').split('#');
                if(0 && slide.data('numerical') && range_values_orginal[0].match(numerical_reg) && range_values_orginal[range_values_orginal.length-1].match(numerical_reg)){
                    is_numerical = true;
                    var found = range_values_orginal[0].match(numerical_reg);
                    rangeslider_prefix=found[1];
                    range_values.push(parseFloat(found[2],10));
                    rangeslider_suffix=found[3];
                    found = range_values_orginal[range_values_orginal.length-1].match(numerical_reg);
                    range_values.push(parseFloat(found[2],10));
                    $.extend(params,{'start':[range_values[0],range_values[1]]});
                }
                else{
                    if(!has_upper){
                        range_values_orginal.unshift('');
                        $.extend(params,{'start':slide.data('nolower')==1  ? 0 : params.start+1});
                    }
                    $.each(range_values_orginal, function(rk,rv){
                        // range_values[rk] = parseFloat(rv, 10);
                        range_values[rk] = parseFloat(rk, 10);
                        // range_values[rk] = Number(rv);
                    });
                }
                // console.log(range_values);
                var range_json = {};
                var range_array = [];
                var range_length = range_values.length;
                var range_min = range_values[0];
                var range_max = range_values[range_length-1];
                $.each(range_values, function(rk,rv){
                    if(rk==0)
                        rk='min';
                    else if(rk==range_length-1)
                        rk='max';
                    else
                        rk = ((rv-range_min)/(range_max-range_min)*100).toFixed(2)+'%';
                    range_json[rk] = [rv];
                    range_array.push(rv);
                });
                $.extend(params,{snap: !is_numerical, range: range_json, format: {'to': function(value){
                    if(is_numerical)
                        return value;
                        // return rangeslider_prefix+value+rangeslider_suffix;
                    else
                        return range_values_orginal[value];
                }, 'from': Number}});
                if(slide.data('pips')==1)
                    $.extend(params,{'pips':{
                        mode: 'values',
                        values: range_array,
                        density: 100/range_array.length,
                        format: {'to': function(value){
                            if(is_numerical)
                                return rangeslider_prefix+value+rangeslider_suffix;
                            else
                                return range_values_orginal[value];
                        }, 'from': Number}
                    }});

                if(slide.data('tooltips')){
                    if(is_numerical)
                        var rangeslider_fomater = {'to': function( value ){return value !== undefined && rangeslider_prefix+value+rangeslider_suffix;}, 'from': Number};
                    else
                        var rangeslider_fomater = {'to': function( value ){return value !== undefined && range_values_orginal[value];}, 'from': Number};
                    var fomater_tooltips = [ rangeslider_fomater ];
                    if(has_upper)
                        fomater_tooltips.push(rangeslider_fomater);
                    $.extend(params,{tooltips: fomater_tooltips});
                }
            }else{
                has_error = true;
            }
        }
        if(prestashop.language.is_rtl=='1')
            $.extend(params,{direction: 'rtl'});

        if(has_error)
            return;
        noUiSlider.create(slide[0], params);
        slide[0].noUiSlider.on('update', function( values, handle ){
            var formated_value = '';
            if(k=='rangeslider'){
                $.each(range_values, function(rk,rv){
                    if(values[handle]==rv){
                        formated_value =  rv;
                        return false;
                    }
                });
                if(!formated_value)
                    formated_value = values[handle];
                formated_value = rangeslider_prefix+formated_value+rangeslider_suffix;
            }else{
                formated_value =  stfs.formatPrice(values[handle],k);
            }
            if(stfacetdsearch.with_inputs==1 && k!='rangeslider')
                inputValues[handle].val(formated_value);
            else
                snapValues[handle].html(formated_value);
        });
        var temp_min = '';
        var temp_max = '';
        slide[0].noUiSlider.on('start', function(values, handle){
            temp_min = values[0];
            if(has_upper)
                temp_max = values[1];
        });
        
        var urlsSplitted = url.split('?');
        var queryParams = [];

        if (urlsSplitted.length > 1) {
          queryParams = stfs.getQueryParameters(urlsSplitted[1]);
        }

        var found = false;
        queryParams.forEach(function(query){
          if (query.name === 'q') {
            found = true;
          }
        });

        if (!found) {
          queryParams.push({name: 'q', value: ''});
        }
        slide[0].noUiSlider.on('set', function(values, handle){
            var facet_url = '';
            var set_to_empty = false;
            if(values[0]!=temp_min || (has_upper && values[1]!=temp_max)){
                var values_k = [];
                $.each(range_values_orginal, function(rk,rv){
                    if(values[0]==rv)
                        values_k[0] = rk;
                    if(has_upper && values[1]==rv)
                        values_k[1] = rk;
                });
                if(k=='rangeslider'){
                    if(is_numerical){
                        facet_url += '-'+rangeslider_prefix+values[0]+rangeslider_suffix;
                        facet_url += '-'+rangeslider_prefix+values[1]+rangeslider_suffix;
                    }else{
                    $.each(range_values_orginal, function(rk,rv){
                        if(has_upper){
                            if(rk>=values_k[0] && rk<=values_k[1])
                                facet_url += '-'+rangeslider_prefix+rv+rangeslider_suffix;
                        }else{
                            if(rk==values_k[0]){
                                if(rk == 0)
                                    set_to_empty = true;
                                else
                                    facet_url += '-'+rangeslider_prefix+rv+rangeslider_suffix;
                                return false;
                            }
                        }
                    });
                    }
                }else{
                    facet_url += '-'+slide.data('slider-unit')+'-'+values[0]+'-'+values[1];
                }
            }
            if(facet_url || set_to_empty){
                queryParams.forEach(function(query) {
                  if (query.name === 'q') {
                    if(!set_to_empty){
                        query.value += [
                          query.value.length > 0 ? '/' : '',
                          slide.data('slider-label'),
                        ].join('');
                        query.value += facet_url;
                    }
                  }
                });

                var requestUrl = [
                  urlsSplitted[0],
                  '?',
                  $.param(queryParams),
                ].join('');

                if(requestUrl)
                    prestashop.emit('updateFacets', requestUrl);
            }
        });
    },
    getQueryParameters: function(params){
        return params.split('&').map(function(str){
          var str_arr = str.split('=');
          var key = str_arr[0], val = str_arr[1];
          return {
            name: key,
            value: decodeURIComponent(val).replace(/\+/g, ' '),
          };
        });
    },
    'fomater': {
        'weight' : {'to': function( value ){return value !== undefined && stfs.formatPrice(value.toFixed(stfs.jiazhong['weight'].format.decimals),'weight');}, 'from': Number},
        'price' : {'to': function( value ){return value !== undefined && stfs.formatPrice(value.toFixed(stfs.jiazhong['price'].format.decimals),'price');}, 'from': Number}
    },
    learnFormat : function(jiazhong){
        var reg = new RegExp("^([^\\d]*).*?([^\\d]*)$");
        var sample = stfacetdsearch.sample[jiazhong];
        var new_sample = stfacetdsearch.sample[jiazhong];
        var res = reg.exec(sample);
        if(res){
            if(res[1]){
                stfs.jiazhong[jiazhong].format.prefix = res[1];
                new_sample = new_sample.replace(res[1],'');
            }
            if(res[2]){
                stfs.jiazhong[jiazhong].format.suffix = res[2];
                new_sample = new_sample.replace(res[2],'');
            }
        }
        var reg = /([^\d])?(\d+)/g;
        var match;
        while (match = reg.exec(new_sample)) {
            stfs.jiazhong[jiazhong].format.symbles.unshift({'change':(''+match[2]).length,'symble':match[1]?match[1]:''})
        }
        if(jiazhong=='weight'){
            var reg = new RegExp("[123456]",'g');
            var d_arr = sample.match(reg);
            stfs.jiazhong[jiazhong].format.decimals = d_arr.length-6;
        }else{
            var reg = new RegExp("[123]",'g');
            var d_arr = sample.match(reg);
            stfs.jiazhong[jiazhong].format.decimals = d_arr.length-3;
        }
    },
    'formatPrice' : function(value,jiazhong){
        if(!stfs.jiazhong[jiazhong].format.symbles.length)
            return value;
        if(value==0)
            return stfs.jiazhong[jiazhong].format.prefix+'0'+stfs.jiazhong[jiazhong].format.suffix;
        if(jiazhong=='weight')
        {
            if(stfs.jiazhong.weight.format.decimals)
                value = parseFloat(value).toFixed(stfs.jiazhong.weight.format.decimals);
            else
                value = parseInt(value,10);
        }
        /*else
            value = value.toFixed(steco_payment.format.decimals);*/
        value = value.toString().replace(/([^\d]+)/g,'');
        var price_arr = value.split('').reverse();

        var price_new = '';
        var index = 0;
        $.each(stfs.jiazhong[jiazhong].format.symbles, function(k,v){
            for (var i = 0; i < v.change; i++) { 
                if(index<price_arr.length)
                    price_new = price_arr[index++]+price_new;
                else
                    break;
            }
            if(index<price_arr.length)
                price_new = v.symble+price_new;
        });
        if(index<price_arr.length)
            for (var j=index; j < price_arr.length; j++) {
                price_new = price_arr[j]+price_new;
            }
        return stfs.jiazhong[jiazhong].format.prefix+price_new+stfs.jiazhong[jiazhong].format.suffix;
    },
    'deFormatPrice' : function(value,jiazhong){
        value = value.toString().replace(stfs.jiazhong[jiazhong].format.prefix,'');
        value = value.replace(stfs.jiazhong[jiazhong].format.suffix,'');
        if(stfs.jiazhong[jiazhong].format.decimals)
        {
            var value_arr = value.split(stfs.jiazhong[jiazhong].format.symbles[0].symble);
            if(value_arr.length>1){
                var decimal = value_arr.pop();
                value = value_arr.join('').replace(/([^\d]+)/g,'')+'.'+decimal;
            }else{
                value = value.replace(/([^\d]+)/g,'');
            }
        }
        else
            value = value.replace(/([^\d]+)/g,'');
        return value;
    },
    'dropDown': function(){
        if(typeof(stfacetdsearch.drop_down)!=='undefined' && stfacetdsearch.drop_down){
            //click
            var click_event = stfacetdsearch.is_mobile_device ? 'touchstart' : 'click';
            $(document).on(click_event, 'html,body', function(e) {
                if($(e.target).parents('.feds_dropdown_wrap.feds_d_open').first().length==0){
                    $('.feds_dropdown_wrap.feds_d_open').removeClass('feds_d_open');
                }
            });
            $(document).on(click_event, '.feds_dropdown_tri', function(e) {
                e.stopPropagation();
                if(!$(this).siblings('.feds_dropdown_list').length)
                    return true;
                e.preventDefault();
                var p = $(this).parents('.feds_dropdown_wrap');
                if(!p.hasClass('feds_d_open') && $('.feds_dropdown_wrap.feds_d_open').length>0){
                    $('.feds_dropdown_wrap.feds_d_open').removeClass('feds_d_open');
                }
                p.toggleClass('feds_d_open');
            });
        }else{
            if(!stfacetdsearch.is_mobile_device)
            {
                $(document).on('click', 'html,body', function(e) {
                    if($(e.target).parents('.feds_dropdown_wrap.feds_d_open').first().length==0){
                        $('.feds_dropdown_wrap.feds_d_open').removeClass('feds_d_open');
                    }
                });
                $('body').on({
                    mouseenter: function () {
                        $(this).addClass('feds_d_open');
                    },
                    mouseleave: function () {
                        $(this).removeClass('feds_d_open');
                    }
                }, ".feds_dropdown_wrap");
            }else{
                $('body').on(
              'touchstart',
              '.feds_dropdown_tri',
              function(event){
                var dropdown_wrap = $(this).closest('.feds_dropdown_wrap');
                if(!dropdown_wrap.hasClass('feds_d_open'))
                {
                    event.preventDefault();
                    dropdown_wrap.addClass('feds_d_open');
                    return false;
                }
                else{
                    dropdown_wrap.removeClass('feds_d_open')
                }
                return false;
              });
            }
        }
    },
    'guanli_watcher': function(){
        if(stfs.facets_watcher)
          return stfs.facets_watcher;
        else
        {
          stfs.facets_watcher = true;
          return false;
        }
    },
    'overlay': function(xy){
        if(stfacetdsearch.loading_effect){
            var selector = '#feds_overlay';
            if(stfacetdsearch.loading_effect==3)
                selector = '#search_filters .feds_overlay';
            if($('body').hasClass('feds_open'))
                selector = '#feds_overlay';
            if(xy)
                $(selector).removeClass('feds_overlay_hide');
            else
                $(selector).addClass('feds_overlay_hide');
        }else{
            var product_list_selector = stfacetdsearch.product_list_selector ? stfacetdsearch.product_list_selector : '#js-product-list';
            $(product_list_selector).css('opacity', xy ? '0.5' : '1');
        }
    },
};