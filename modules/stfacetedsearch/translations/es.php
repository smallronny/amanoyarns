<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{stfacetedsearch}prestashop>stfacetedsearch_f3f43e30c8c7d78c6ac0173515e57a00'] = 'Filtros';
$_MODULE['<{stfacetedsearch}prestashop>center_column_f3f43e30c8c7d78c6ac0173515e57a00'] = 'Filtros';
$_MODULE['<{stfacetedsearch}prestashop>transit_3601146c4e948c32b6424d2c0a7f0118'] = 'Precio';
$_MODULE['<{stfacetedsearch}prestashop>searchprovider_ad24ffca4f9e9c0c7e80fe1512df6db9'] = 'Relevancia';
$_MODULE['<{stfacetedsearch}prestashop>searchprovider_05d61847840bcddb5d37374de3be0ccc'] = 'Nombre, de la A a la Z';
$_MODULE['<{stfacetedsearch}prestashop>searchprovider_e21f2ef58ff858167655c18a874f9706'] = 'Nombre, de la Z a la A';
$_MODULE['<{stfacetedsearch}prestashop>searchprovider_e2174d7435696694b8f4d984d99eef03'] = 'Precio, menor a mayor';
$_MODULE['<{stfacetedsearch}prestashop>searchprovider_0ee81913615b71aba7dc8bbf1f144672'] = 'Precio, mayor a menor';
$_MODULE['<{stfacetedsearch}prestashop>searchprovider_d2aed35af5b3b2a8a8de1914911a5ed9'] = 'Ventas, bajas a altas';
$_MODULE['<{stfacetedsearch}prestashop>searchprovider_9ff39273a271b8854e4c36ca0811f1b5'] = 'Ventas, altas a bajas';
