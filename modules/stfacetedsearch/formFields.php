<?php
/**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

function getFormFields($object)
{
    $name = substr(strtolower(basename(__FILE__)), 0, -4);
    $form_fields = array();
    $form_fields['setting'] = array(
        'show_on' => array(
            'type' => 'radio',
            'label' => $object->l('Show on', $name),
            'name' => 'show_on',
            'default_value' => 0,
            'values' => array(
                array(
                    'id' => 'show_on_0',
                    'value' => 0,
                    'label' => $object->l('Left column', $name)),
                array(
                    'id' => 'show_on_1',
                    'value' => 1,
                    'label' => $object->l('Center column', $name)),
                array(
                    'id' => 'show_on_2',
                    'value' => 2,
                    'label' => $object->l('As dropdown list on the center column', $name)),
                array(
                    'id' => 'show_on_3',
                    'value' => 3,
                    'label' => $object->l('Slide in sidebar', $name)),
            ),
            'validation' => 'isUnsignedInt',
        ),
        'show_on_mobile' => array(
            'type' => 'radio',
            'label' => $object->l('Show on mobile', $name),
            'name' => 'show_on_mobile',
            'default_value' => 1,
            'values' => array(
                array(
                    'id' => 'show_on_mobile_0',
                    'value' => 0,
                    'label' => $object->l('The same as desktop', $name)),
                array(
                    'id' => 'show_on_mobile_1',
                    'value' => 1,
                    'label' => $object->l('Slide in sidebar(Screen width < 768pxpx)', $name)),
                array(
                    'id' => 'show_on_mobile_2',
                    'value' => 2,
                    'label' => $object->l('Slide in sidebar(Screen width < 992px)', $name)),
            ),
            'validation' => 'isUnsignedInt',
        ),
        'active_filters' => array(
            'type' => 'switch',
            'label' => $object->l('Display active filters', $name),
            'name' => 'active_filters',
            'default_value' => 1,
            'values' => array(
                array(
                    'id' => 'active_filters_on',
                    'value' => 1,
                    'label' => $object->l('Yes', $name)),
                array(
                    'id' => 'active_filters_off',
                    'value' => 0,
                    'label' => $object->l('No', $name)),
            ),
            'validation' => 'isUnsignedInt',
        ),
        'show_qties' => array(
            'type' => 'switch',
            'label' => $object->l('Show the number of matching products', $name),
            'name' => 'show_qties',
            'is_bool' => true,
            'default_value' => 1,
            'values' => array(
                array(
                    'id' => 'show_qties_on',
                    'value' => 1,
                    'label' => $object->l('Yes', $name)),
                array(
                    'id' => 'show_qties_off',
                    'value' => 0,
                    'label' => $object->l('No', $name)),
            ),
            'validation' => 'isBool',
            'desc' => '<input type="hidden" name="id_tab_index" value="0" />'
        ),
        'full_tree' => array(
            'type' => 'switch',
            'label' => $object->l('Show products from subcategories', $name),
            'name' => 'full_tree',
            'is_bool' => true,
            'default_value' => 1,
            'values' => array(
                array(
                    'id' => 'full_tree_on',
                    'value' => 1,
                    'label' => $object->l('Yes', $name)),
                array(
                    'id' => 'full_tree_off',
                    'value' => 0,
                    'label' => $object->l('No', $name)),
            ),
            'validation' => 'isBool',
        ),
        'by_default_category' => array(
            'type' => 'switch',
            'label' => $object->l('Show products only from default category', $name),
            'name' => 'by_default_category',
            'is_bool' => true,
            'default_value' => 1,
            'values' => array(
                array(
                    'id' => 'by_default_category_on',
                    'value' => 1,
                    'label' => $object->l('Yes', $name)),
                array(
                    'id' => 'by_default_category_off',
                    'value' => 0,
                    'label' => $object->l('No', $name)),
            ),
            'validation' => 'isBool',
            'desc' => $object->l('Works only if "Show products from subcategories" is off.', $name)
        ),
        'show_attribute_image' => array(
            'type' => 'switch',
            'label' => $object->l('Use attribute image as cover image', $name),
            'name' => 'show_attribute_image',
            'is_bool' => true,
            'default_value' => 0,
            'values' => array(
                array(
                    'id' => 'show_attribute_image_on',
                    'value' => 1,
                    'label' => $object->l('Yes', $name)),
                array(
                    'id' => 'show_attribute_image_off',
                    'value' => 0,
                    'label' => $object->l('No', $name)),
            ),
            'validation' => 'isBool',
            'desc' => $object->l('Use the first image assigned to the as current filted atrribute to be cover image.', $name)
        ),
        'category_depth' => array(
            'type' => 'text',
            'label' => $object->l('Category filter depth (0 for no limits, 1 by default)', $name),
            'name' => 'category_depth',
            'default_value' => 6,
            'validation' => 'isUnsignedInt',
            'class' => 'fixed-width-sm'
        ),
        'price_usetax' => array(
            'type' => 'switch',
            'label' => $object->l('Use tax to filter price', $name),
            'name' => 'price_usetax',
            'is_bool' => true,
            'default_value' => 1,
            'values' => array(
                array(
                    'id' => 'price_usetax_on',
                    'value' => 1,
                    'label' => $object->l('Yes', $name)),
                array(
                    'id' => 'price_usetax_off',
                    'value' => 0,
                    'label' => $object->l('No', $name)),
            ),
            'validation' => 'isBool',
        ),
        'price_rounding' => array(
            'type' => 'switch',
            'label' => $object->l('Use rounding to filter price', $name),
            'name' => 'price_rounding',
            'is_bool' => true,
            'default_value' => 1,
            'values' => array(
                array(
                    'id' => 'price_rounding_on',
                    'value' => 1,
                    'label' => $object->l('Yes', $name)),
                array(
                    'id' => 'price_rounding_off',
                    'value' => 0,
                    'label' => $object->l('No', $name)),
            ),
            'validation' => 'isBool',
        ),
        'show_unavailable' => array(
            'type' => 'switch',
            'label' => $object->l('Show out of stock products and products with out of stock combinations', $name),
            'name' => 'show_unavailable',
            'is_bool' => true,
            'default_value' => 1,
            'values' => array(
                array(
                    'id' => 'out_of_stock_on',
                    'value' => 1,
                    'label' => $object->l('Yes', $name)),
                array(
                    'id' => 'out_of_stock_off',
                    'value' => 0,
                    'label' => $object->l('No', $name)),
            ),
            'validation' => 'isBool',
        ),
        'collapse' => array(
            'type' => 'switch',
            'label' => $object->l('Enable collapse feature also for desktop', $name),
            'name' => 'collapse',
            'default_value' => 1,
            'values' => array(
                array(
                    'id' => 'collapse_on',
                    'value' => 1,
                    'label' => $object->l('Yes', $name)),
                array(
                    'id' => 'collapse_off',
                    'value' => 0,
                    'label' => $object->l('No', $name)),
            ),
            'validation' => 'isUnsignedInt',
        ),
        'by_sales' => array(
            'type' => 'switch',
            'label' => $object->l('Add two sort options: "Sales high to low" and "Sales low to high"', $name),
            'name' => 'by_sales',
            'default_value' => 0,
            'values' => array(
                array(
                    'id' => 'by_sales_on',
                    'value' => 1,
                    'label' => $object->l('Yes', $name)),
                array(
                    'id' => 'by_sales_off',
                    'value' => 0,
                    'label' => $object->l('No', $name)),
            ),
            'validation' => 'isUnsignedInt',
        ),
        'attribute_in_url' => array(
            'type' => 'switch',
            'label' => $object->l('Show attribute in the URL', $name),
            'name' => 'attribute_in_url',
            'default_value' => 0,
            'values' => array(
                array(
                    'id' => 'attribute_in_url_on',
                    'value' => 1,
                    'label' => $object->l('Yes', $name)),
                array(
                    'id' => 'attribute_in_url_off',
                    'value' => 0,
                    'label' => $object->l('No', $name)),
            ),
            'validation' => 'isUnsignedInt',
            'desc' => $object->l('Show attribute URL on the list page. if click the link, current combination will be selected.', $name)
        ),
        'magic' => array(
            'type' => 'switch',
            'label' => $object->l('Magic option', $name),
            'name' => 'magic',
            'default_value' => 0,
            'values' => array(
                array(
                    'id' => 'magic_on',
                    'value' => 1,
                    'label' => $object->l('Yes', $name)),
                array(
                    'id' => 'magic_off',
                    'value' => 0,
                    'label' => $object->l('No', $name)),
            ),
            'validation' => 'isUnsignedInt',
            'desc' => $object->l('If you are not using Transformer theme and panda theme, if the filter doesn\'t work on your site, try enabling this option.', $name)
        ),
    );
    $form_fields['slider'] = array(
        
        'price_step' => array(
            'type' => 'text',
            'label' => $object->l('Step for price range slider:', $name),
            'name' => 'price_step',
            'class' => 'fixed-width-lg',
            'default_value' => 0,
            'validation' => 'isUnsignedFloat',
            'desc' => $object->l('The amount price slider changes on movement, default value is 0.01. For example if you set it to 10, price range slider will goes like 0-10-20, no vaules in between.', $name),
        ), 
        'weight_step' => array(
            'type' => 'text',
            'label' => $object->l('Step for weight range slider:', $name),
            'name' => 'weight_step',
            'class' => 'fixed-width-lg',
            'default_value' => 0,
            'validation' => 'isUnsignedFloat',
            'desc' => $object->l('The amount weight slider changes on movement, default value is 0.01. For example if you set it to 10, weight range slider will goes like 0-10-20, no vaules in between.', $name),
        ),
        'w_decimal' => array(
            'type' => 'select',
            'label' => $object->l('Number of decimals for weight:', $name),
            'name' => 'w_decimal',
            'default_value' => 2,
            'options' => array(
                'query' => array(
                    array('id' => 0, 'name' => 0), 
                    array('id' => 1, 'name' => 1), 
                    array('id' => 2, 'name' => 2), 
                    array('id' => 3, 'name' => 3), 
                    array('id' => 4, 'name' => 4), 
                    array('id' => 5, 'name' => 5), 
                    array('id' => 6, 'name' => 6), 
                ),
                'id' => 'id',
                'name' => 'name',
            ),
            'validation' => 'isUnsignedInt',
        ),
        'with_inputs' => array(
            'type' => 'switch',
            'label' => $object->l('With inputs to allow users to set values:', $name),
            'name' => 'with_inputs',
            'is_bool' => true,
            'default_value' => 0,
            'values' => array(
                array(
                    'id' => 'with_inputs_on',
                    'value' => 1,
                    'label' => $object->l('Yes', $name)),
                array(
                    'id' => 'with_inputs_off',
                    'value' => 0,
                    'label' => $object->l('No', $name)),
            ),
            'validation' => 'isBool',
            'desc' => $object->l('For price and weight sliders only', $name),
        ),  
        /*'tooltips' => array(
            'type' => 'switch',
            'label' => $object->l('Display tooltips:', $name),
            'name' => 'tooltips',
            'is_bool' => true,
            'default_value' => 0,
            'values' => array(
                array(
                    'id' => 'tooltips_on',
                    'value' => 1,
                    'label' => $object->l('Yes', $name)),
                array(
                    'id' => 'tooltips_off',
                    'value' => 0,
                    'label' => $object->l('No', $name)),
            ),
            'validation' => 'isBool',
        ),  
        'disable_range_text' => array(
            'type' => 'switch',
            'label' => $object->l('Disable range text:', $name),
            'name' => 'disable_range_text',
            'is_bool' => true,
            'default_value' => 0,
            'values' => array(
                array(
                    'id' => 'disable_range_text_on',
                    'value' => 1,
                    'label' => $object->l('Yes', $name)),
                array(
                    'id' => 'disable_range_text_off',
                    'value' => 0,
                    'label' => $object->l('No', $name)),
            ),
            'validation' => 'isBool',
            'desc' => $object->l('It is recommended to disable range text like $11.00 - $23.00 when tooltips are enabled.', $name),
        ),
        'vertical' => array(
            'type' => 'switch',
            'label' => $object->l('Vertical:', $name),
            'name' => 'vertical',
            'is_bool' => true,
            'default_value' => 0,
            'values' => array(
                array(
                    'id' => 'vertical_on',
                    'value' => 1,
                    'label' => $object->l('Yes', $name)),
                array(
                    'id' => 'vertical_off',
                    'value' => 0,
                    'label' => $object->l('No', $name)),
            ),
            'validation' => 'isBool',
        ), */
        'vertical_height' => array(
            'type' => 'text',
            'label' => $object->l('Vertical slider height:', $name),
            'name' => 'vertical_height',
            'prefix' => 'px',
            'class' => 'fixed-width-lg',
            'default_value' => 200,
            'validation' => 'isUnsignedInt',
            'desc' => $object->l('Default vaule is 200', $name),
        ),
        'range_style' => array(
            'type' => 'radio',
            'label' => $object->l('Range slider style:', $name),
            'name' => 'range_style',
            'default_value' => 0,
            'values' => array(
                array(
                    'id' => 'range_style_0',
                    'value' => 0,
                    'label' => $object->l('Square handles', $name)),
                array(
                    'id' => 'range_style_1',
                    'value' => 1,
                    'label' => $object->l('Round handles', $name)),
            ),
            'validation' => 'isUnsignedInt',
        ),  
    );
    $form_fields['slider_color'] = array(
        'bar_bg' => array(
            'type' => 'color',
            'label' => $object->l('Slider bar color:', $name),
            'name' => 'bar_bg',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
        'bar_height' => array(
            'type' => 'text',
            'label' => $object->l('Slider bar height:', $name),
            'name' => 'bar_height',
            'prefix' => 'px',
            'class' => 'fixed-width-lg',
            'default_value' => 0,
            'validation' => 'isUnsignedInt',
        ),
         'handle_bg' => array(
            'type' => 'color',
            'label' => $object->l('Slider handle color:', $name),
            'name' => 'handle_bg',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
         'border_color' => array(
            'type' => 'color',
            'label' => $object->l('Bar border color:', $name),
            'name' => 'border_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
        'handle_width' => array(
            'type' => 'text',
            'label' => $object->l('Handle width:', $name),
            'name' => 'handle_width',
            'prefix' => 'px',
            'class' => 'fixed-width-lg',
            'default_value' => 0,
            'validation' => 'isUnsignedInt',
        ),
        'handle_height' => array(
            'type' => 'text',
            'label' => $object->l('Handle height:', $name),
            'name' => 'handle_height',
            'prefix' => 'px',
            'class' => 'fixed-width-lg',
            'default_value' => 0,
            'validation' => 'isUnsignedInt',
        ),
         'handle_border_color' => array(
            'type' => 'color',
            'label' => $object->l('Handle border color:', $name),
            'name' => 'handle_border_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
         'connect_bg' => array(
            'type' => 'color',
            'label' => $object->l('Color for the bar between handles:', $name),
            'name' => 'connect_bg',
            'default_value' => '#3FB8AF',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
        'bar_bg_right' => array(
            'type' => 'color',
            'label' => $object->l('Right side color for the bar between handles:', $name),
            'name' => 'bar_bg_right',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
            'desc' => $object->l('If you fill in this filed, the bar will be in gradient color:', $name),
         ),
        'topbar_text_color' => array(
            'type' => 'color',
            'label' => $object->l('Text color for start value and end value:', $name),
            'name' => 'topbar_text_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
        'pips_color' => array(
            'type' => 'color',
            'label' => $object->l('Text color for pips:', $name),
            'name' => 'pips_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
         'tooltips_color' => array(
            'type' => 'color',
            'label' => $object->l('Text color in tooltips:', $name),
            'name' => 'tooltips_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
         'tooltips_border' => array(
            'type' => 'color',
            'label' => $object->l('Tooltips border color:', $name),
            'name' => 'tooltips_border',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
         'tooltips_bg' => array(
            'type' => 'color',
            'label' => $object->l('Tooltips background color:', $name),
            'name' => 'tooltips_bg',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
    );
    $form_fields['left_column'] = array(
    );
    $form_fields['center_column'] = array(
        'filters_per' => array(
            'type' => 'html',
            'id' => 'filters_per',
            'label'=> $object->l('How many filter blocks per row:', $name),
            'name' => '',
        ),
    );
    $form_fields['center_column_1'] = array(
        'drop_down_position' => array(
            'type' => 'radio',
            'label' => $object->l('How to display:', $name),
            'name' => 'drop_down_position',
            'values' => array(
                array(
                    'id' => 'drop_down_position_left',
                    'value' => 0,
                    'label' => $object->l('Align Left', $name)),
                array(
                    'id' => 'drop_down_position_full',
                    'value' => 1,
                    'label' => $object->l('Full width, dropdowns have equal width.', $name)),
            ),
            'validation' => 'isUnsignedInt',
        ), 
        'drop_down' => array(
            'type' => 'radio',
            'label' => $object->l('How to open drop down lists', $name),
            'name' => 'drop_down',
            'default_value' => 1,
            'values' => array(
                array(
                    'id' => 'drop_down_0',
                    'value' => 0,
                    'label' => $object->l('Hover', $name)),
                array(
                    'id' => 'drop_down_1',
                    'value' => 1,
                    'label' => $object->l('Click', $name)),
            ),
            'validation' => 'isUnsignedInt',
        ),
        'drop_down_width' => array(
            'type' => 'text',
            'label' => $object->l('Drop down width:', $name),
            'name' => 'drop_down_width',
            'prefix' => 'px',
            'class' => 'fixed-width-lg',
            'default_value' => 260,
            'validation' => 'isUnsignedInt',
        ),
        /* 'drop_title_color' => array(
            'type' => 'color',
            'label' => $object->l('Dropdown title color:', $name),
            'name' => 'drop_title_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
         'drop_title_bg' => array(
            'type' => 'color',
            'label' => $object->l('Dropdown title background:', $name),
            'name' => 'drop_title_bg',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),*/
         'drop_title_active_color' => array(
            'type' => 'color',
            'label' => $object->l('Dropdown title active color:', $name),
            'name' => 'drop_title_active_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
         'drop_title_active_bg' => array(
            'type' => 'color',
            'label' => $object->l('Dropdown title active background:', $name),
            'name' => 'drop_title_active_bg',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
         'drop_list_background' => array(
            'type' => 'color',
            'label' => $object->l('Dropdown block background:', $name),
            'name' => 'drop_list_background',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
    );
    $form_fields['sidebar'] = array(
        'sidebar_bg' => array(
            'type' => 'color',
            'label' => $object->l('Background:', $name),
            'name' => 'sidebar_bg',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
        'sidebar_overlay_bg' => array(
            'type' => 'color',
            'label' => $object->l('Overlay color:', $name),
            'name' => 'sidebar_overlay_bg',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
        'sidebar_close_color' => array(
            'type' => 'color',
            'label' => $object->l('Close button color:', $name),
            'name' => 'sidebar_close_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
        'sidebar_close_active_color' => array(
            'type' => 'color',
            'label' => $object->l('Close button active color:', $name),
            'name' => 'sidebar_close_active_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
        'sidebar_overlay_opacity' => array(
            'type' => 'text',
            'label' => $object->l('Overlay opacity:', $name),
            'name' => 'sidebar_overlay_opacity',
            'validation' => 'isFloat',
            'class' => 'fixed-width-lg',
            'desc' => $object->l('From 0.0 (fully transparent) to 1.0 (fully opaque).', $name),
        ),
        'sidebar_width' => array(
            'type' => 'text',
            'label' => $object->l('Width:', $name),
            'name' => 'sidebar_width',
            'prefix' => 'px',
            'class' => 'fixed-width-lg',
            'default_value' => 0,
            'validation' => 'isUnsignedInt',
        ),
        'sidebar_btn_color' => array(
            'type' => 'color',
            'label' => $object->l('Button color:', $name),
            'name' => 'sidebar_btn_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
            'desc' => $object->l('For the reset and the done button on the bottom of sidebar.', $name),
         ),
        'sidebar_btn_bg' => array(
            'type' => 'color',
            'label' => $object->l('Button background:', $name),
            'name' => 'sidebar_btn_bg',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
        'sidebar_btn_hover_color' => array(
            'type' => 'color',
            'label' => $object->l('Button hover color:', $name),
            'name' => 'sidebar_btn_hover_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
        'sidebar_btn_hover_bg' => array(
            'type' => 'color',
            'label' => $object->l('Button hover background:', $name),
            'name' => 'sidebar_btn_hover_bg',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
    );
    $form_fields['basic_color'] = array(
        array(
            'type' => 'html',
            'id' => '',
            'label' => $object->l('How eveything is named.', $name),
            'name' => '<img src="'._MODULE_DIR_.$object->name.'/views/img/layout.jpg" />',
        ), 
        'block_color' => array(
            'type' => 'color',
            'label' => $object->l('Text color:', $name),
            'name' => 'block_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
        'block_link_color' => array(
            'type' => 'color',
            'label' => $object->l('Link color:', $name),
            'name' => 'block_link_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
        'block_link_active_color' => array(
            'type' => 'color',
            'label' => $object->l('Link hover color:', $name),
            'name' => 'block_link_active_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
    );
    $form_fields['block'] = array(
        'block_bg' => array(
            'type' => 'color',
            'label' => $object->l('Filter group background:', $name),
            'name' => 'block_bg',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
        'block_paddings' => array(
            'type' => 'html',
            'id' => 'block_paddings',
            'label'=> $object->l('Filter group content paddings', $name),
            'name' => '',
        ),
        'block_border_color' => array(
            'type' => 'color',
            'label' => $object->l('Filter group border color:', $name),
            'name' => 'block_border_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
        'block_border_size' => array(
            'type' => 'text',
            'label' => $object->l('Filter group border size:', $name),
            'name' => 'block_border_size',
            'prefix' => 'px',
            'class' => 'fixed-width-lg',
            'default_value' => 0,
            'validation' => 'isUnsignedInt',
        ),
        'block_border_radius' => array(
            'type' => 'text',
            'label' => $object->l('Filter group border radius:', $name),
            'name' => 'block_border_radius',
            'prefix' => 'px',
            'class' => 'fixed-width-lg',
            'default_value' => 0,
            'validation' => 'isUnsignedInt',
        ),
        'block_bottom_margin' => array(
            'type' => 'text',
            'label' => $object->l('Filter group bottom spacing:', $name),
            'name' => 'block_bottom_margin',
            'prefix' => 'px',
            'class' => 'fixed-width-lg',
            'default_value' => 25,
            'validation' => 'isNullOrUnsignedId',
        ),
    );
    $form_fields['block_title'] = array(
        'block_title_color' => array(
            'type' => 'color',
            'label' => $object->l('Filter group title color:', $name),
            'name' => 'block_title_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
            'default_value' => '#444444',
            'desc' => $object->l('For exmaple, "Sort by", "Refine by", thye are filter group titles.', $name),
         ),
        'block_title_bg' => array(
            'type' => 'color',
            'label' => $object->l('Filter group title background:', $name),
            'name' => 'block_title_bg',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
            'default_value' => '#F6F6F6',
         ),
        'heading_paddings' => array(
            'type' => 'html',
            'id' => 'heading_paddings',
            'label'=> $object->l('Filter group title paddings', $name),
            'name' => '',
        ),
        'block_title_border_color' => array(
            'type' => 'color',
            'label' => $object->l('Filter group title border color:', $name),
            'name' => 'block_title_border_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
        'block_title_border_size' => array(
            'type' => 'text',
            'label' => $object->l('Filter group title border size:', $name),
            'name' => 'block_title_border_size',
            'prefix' => 'px',
            'class' => 'fixed-width-lg',
            'default_value' => 0,
            'validation' => 'isUnsignedInt',
        ),
        'block_title_border_radius' => array(
            'type' => 'text',
            'label' => $object->l('Filter group title border radius:', $name),
            'name' => 'block_title_border_radius',
            'prefix' => 'px',
            'class' => 'fixed-width-lg',
            'default_value' => 0,
            'validation' => 'isUnsignedInt',
        ),
        'font_text_list'=>array(
            'type' => 'select',
            'label' => $object->l('Font:', $name),
            'name' => 'font_text_list',
            'onchange' => 'handle_font_change(this);',
            'options' => array(
                'optiongroup' => array (
                    'query' => $object->fontOptions(),
                    'label' => 'name'
                ),
                'options' => array (
                    'query' => 'query',
                    'id' => 'id',
                    'name' => 'name'
                ),
                'default' => array(
                    'value' => 0,
                    'label' => $object->l('Use default', $name)
                ),
            ),
            'desc' => '<p id="font_text_list_example" class="fontshow">Filter</p>',
        ),
        'font_text'=>array(
            'type' => 'select',
            'label' => $object->l('Font weight:', $name),
            'onchange' => 'handle_font_style(this);',
            'class' => 'fontOptions',
            'name' => 'font_text',
            'options' => array(
                'query' => array(),
                'id' => 'id',
                'name' => 'name',
            ),
            'validation' => 'isAnything',
        ),
        'font_text_trans'=>array(
            'type' => 'select',
            'label' => $object->l('Filter group title transform:', $name),
            'name' => 'font_text_trans',
            'options' => array(
                'query' => $object::$textTransform,
                'id' => 'id',
                'name' => 'name',
            ),
            'validation' => 'isUnsignedInt',
        ),
        'font_text_size'=>array(
            'type' => 'text',
            'label' => $object->l('Filter group title size:', $name),
            'name' => 'font_text_size',
            'prefix' => 'px',
            'class' => 'fixed-width-lg',
            'default_value' => 0,
            'validation' => 'isUnsignedInt',
        ),
    );
    $form_fields['filter'] = array(
        'filter_title_color' => array(
            'type' => 'color',
            'label' => $object->l('Filter title color:', $name),
            'name' => 'filter_title_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
            'desc' => $object->l('For exmaple, "Size", "Color", "Price", thye are filter titles.', $name),
         ),
        'filter_title_bg' => array(
            'type' => 'color',
            'label' => $object->l('Filter title background:', $name),
            'name' => 'filter_title_bg',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
        'filter_title_collapsed_color' => array(
            'type' => 'color',
            'label' => $object->l('Collapsed filter title color:', $name),
            'name' => 'filter_title_collapsed_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
        'filter_title_collapsed_bg' => array(
            'type' => 'color',
            'label' => $object->l('Collapsed filter title background:', $name),
            'name' => 'filter_title_collapsed_bg',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
        'filter_title_paddings' => array(
            'type' => 'html',
            'id' => 'filter_title_paddings',
            'label'=> $object->l('Filter title paddings', $name),
            'name' => '',
        ),
        'filter_title_border_sizes' => array(
            'type' => 'html',
            'id' => 'filter_title_border_sizes',
            'label'=> $object->l('Filter title border size', $name),
            'name' => '',
        ),
        'filter_title_border_color' => array(
            'type' => 'color',
            'label' => $object->l('Filter title border color:', $name),
            'name' => 'filter_title_border_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
            'default_value' => '#E2E2E2',
         ),
        'filter_title_border_radius' => array(
            'type' => 'text',
            'label' => $object->l('Filter title border radius:', $name),
            'name' => 'filter_title_border_radius',
            'prefix' => 'px',
            'class' => 'fixed-width-lg',
            'default_value' => 0,
            'validation' => 'isUnsignedInt',
        ),
        'font_filter_title_list'=>array(
            'type' => 'select',
            'label' => $object->l('Filter title font:', $name),
            'name' => 'font_filter_title_list',
            'onchange' => 'handle_font_change(this);',
            'options' => array(
                'optiongroup' => array (
                    'query' => $object->fontOptions(),
                    'label' => 'name'
                ),
                'options' => array (
                    'query' => 'query',
                    'id' => 'id',
                    'name' => 'name'
                ),
                'default' => array(
                    'value' => 0,
                    'label' => $object->l('Use default', $name)
                ),
            ),
            'desc' => '<p id="font_filter_title_list_example" class="fontshow">Pirce Size</p>',
        ),
        'font_filter_title'=>array(
            'type' => 'select',
            'label' => $object->l('Filter title font weight:', $name),
            'onchange' => 'handle_font_style(this);',
            'class' => 'fontOptions',
            'name' => 'font_filter_title',
            'options' => array(
                'query' => array(),
                'id' => 'id',
                'name' => 'name',
            ),
            'validation' => 'isAnything',
        ),
        'font_filter_title_trans'=>array(
            'type' => 'select',
            'label' => $object->l('Filter title transform:', $name),
            'name' => 'font_filter_title_trans',
            'options' => array(
                'query' => $object::$textTransform,
                'id' => 'id',
                'name' => 'name',
            ),
            'validation' => 'isUnsignedInt',
        ),
        'font_filter_title_size'=>array(
            'type' => 'text',
            'label' => $object->l('Filter title size:', $name),
            'name' => 'font_filter_title_size',
            'prefix' => 'px',
            'class' => 'fixed-width-lg',
            'default_value' => 14,
            'validation' => 'isUnsignedInt',
        ),
        'filter_paddings' => array(
            'type' => 'html',
            'id' => 'filter_paddings',
            'label'=> $object->l('Filter block paddings', $name),
            'name' => '',
        ),
        'filter_border_color' => array(
            'type' => 'color',
            'label' => $object->l('Filter block border color:', $name),
            'name' => 'filter_border_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
        'filter_border_sizes' => array(
            'type' => 'html',
            'id' => 'filter_border_sizes',
            'label'=> $object->l('Filter block border size', $name),
            'name' => '',
        ),
        'filter_border_radius' => array(
            'type' => 'text',
            'label' => $object->l('Filter block border radius:', $name),
            'name' => 'filter_border_radius',
            'prefix' => 'px',
            'class' => 'fixed-width-lg',
            'default_value' => 0,
            'validation' => 'isUnsignedInt',
        ),
        'filter_bg' => array(
            'type' => 'color',
            'label' => $object->l('Filter block background:', $name),
            'name' => 'filter_bg',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
        'filter_bottom_margin' => array(
            'type' => 'text',
            'label' => $object->l('Filter block bottom spacing:', $name),
            'name' => 'filter_bottom_margin',
            'prefix' => 'px',
            'class' => 'fixed-width-lg',
            'default_value' => 0,
            'validation' => 'isUnsignedInt',
        ),
        'filter_title_size'=>array(
            'type' => 'text',
            'label' => $object->l('Filter block font size:', $name),
            'name' => 'filter_title_size',
            'prefix' => 'px',
            'class' => 'fixed-width-lg',
            'default_value' => 14,
            'validation' => 'isUnsignedInt',
        ),
        'filter_max_height' => array(
            'type' => 'text',
            'label' => $object->l('Max height:', $name),
            'name' => 'filter_max_height',
            'prefix' => 'px',
            'class' => 'fixed-width-lg',
            'default_value' => 320,
            'validation' => 'isUnsignedInt',
            'desc' => $object->l('Set to 0 to disalbe the feature.', $name),
        ),
        'toggle_icon_color' => array(
            'type' => 'color',
            'label' => $object->l('Toggle icon color:', $name),
            'name' => 'toggle_icon_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
            'desc' => $object->l('Including expand icons, collapse icons and arrow icons.', $name),
         ),
        'toggle_icon_active_color' => array(
            'type' => 'color',
            'label' => $object->l('Toggle icon active color:', $name),
            'name' => 'toggle_icon_active_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
    );
    $form_fields['active_filter'] = array(
        'af_color' => array(
            'type' => 'color',
            'label' => $object->l('Text color:', $name),
            'name' => 'af_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
        'af_active_color' => array(
            'type' => 'color',
            'label' => $object->l('Text hover color:', $name),
            'name' => 'af_active_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
            'default_value' => '#E54D26',
         ),
        'af_bg' => array(
            'type' => 'color',
            'label' => $object->l('Background color:', $name),
            'name' => 'af_bg',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
        'af_active_bg' => array(
            'type' => 'color',
            'label' => $object->l('Background hover color:', $name),
            'name' => 'af_active_bg',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
        'af_border_size' => array(
            'type' => 'text',
            'label' => $object->l('Border size:', $name),
            'name' => 'af_border_size',
            'prefix' => 'px',
            'class' => 'fixed-width-lg',
            'default_value' => 1,
            'validation' => 'isUnsignedInt',
        ),
        'af_border_color' => array(
            'type' => 'color',
            'label' => $object->l('Border color:', $name),
            'name' => 'af_border_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
            'default_value' => '#d9d9d9',
         ),
        'af_border_style' => array(
            'type' => 'radio',
            'label' => $object->l('Border style', $name),
            'name' => 'af_border_style',
            'default_value' => 1,
            'values' => array(
                array(
                    'id' => 'af_border_style_0',
                    'value' => 0,
                    'label' => $object->l('Solid', $name)),
                array(
                    'id' => 'af_border_style_1',
                    'value' => 1,
                    'label' => $object->l('Dashed', $name)),
                array(
                    'id' => 'af_border_style_2',
                    'value' => 2,
                    'label' => $object->l('Dotted', $name)),
            ),
            'validation' => 'isUnsignedInt',
        ),
        'active_paddings' => array(
            'type' => 'html',
            'id' => 'active_paddings',
            'label'=> $object->l('Paddings', $name),
            'name' => '',
        ),
    );
    $form_fields['triger_button'] = array(
        'trigger_color' => array(
            'type' => 'color',
            'label' => $object->l('Text color:', $name),
            'name' => 'trigger_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
        'trigger_active_color' => array(
            'type' => 'color',
            'label' => $object->l('Text hover color:', $name),
            'name' => 'trigger_active_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
        'trigger_bg' => array(
            'type' => 'color',
            'label' => $object->l('Background color:', $name),
            'name' => 'trigger_bg',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
        'trigger_active_bg' => array(
            'type' => 'color',
            'label' => $object->l('Background hover color:', $name),
            'name' => 'trigger_active_bg',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
        'trigger_border_size' => array(
            'type' => 'text',
            'label' => $object->l('Border size:', $name),
            'name' => 'trigger_border_size',
            'prefix' => 'px',
            'class' => 'fixed-width-lg',
            'default_value' => 0,
            'validation' => 'isUnsignedInt',
        ),
        'trigger_border_color' => array(
            'type' => 'color',
            'label' => $object->l('Border color:', $name),
            'name' => 'trigger_border_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
        'trigger_border_style' => array(
            'type' => 'radio',
            'label' => $object->l('Border style', $name),
            'name' => 'trigger_border_style',
            'default_value' => 0,
            'values' => array(
                array(
                    'id' => 'trigger_border_style_0',
                    'value' => 0,
                    'label' => $object->l('Solid', $name)),
                array(
                    'id' => 'trigger_border_style_1',
                    'value' => 1,
                    'label' => $object->l('Dashed', $name)),
                array(
                    'id' => 'trigger_border_style_2',
                    'value' => 2,
                    'label' => $object->l('Dotted', $name)),
            ),
            'validation' => 'isUnsignedInt',
        ),
        'trigger_size' => array(
            'type' => 'text',
            'label' => $object->l('Font size:', $name),
            'name' => 'trigger_size',
            'prefix' => 'px',
            'class' => 'fixed-width-lg',
            'default_value' => 16,
            'validation' => 'isUnsignedInt',
        ),
        'trigger_width' => array(
            'type' => 'text',
            'label' => $object->l('Width:', $name),
            'name' => 'trigger_width',
            'prefix' => 'px',
            'class' => 'fixed-width-lg',
            'default_value' => 0,
            'validation' => 'isUnsignedInt',
        ),
        'trigger_height' => array(
            'type' => 'text',
            'label' => $object->l('Height:', $name),
            'name' => 'trigger_height',
            'prefix' => 'px',
            'class' => 'fixed-width-lg',
            'default_value' => 0,
            'validation' => 'isUnsignedInt',
        ),
    );
    $form_fields['showmore_blocks'] = array(
        'showmore_blocks_color' => array(
            'type' => 'color',
            'label' => $object->l('Text color:', $name),
            'name' => 'showmore_blocks_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
        'showmore_blocks_bg' => array(
            'type' => 'color',
            'label' => $object->l('Background:', $name),
            'name' => 'showmore_blocks_bg',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
        'showmore_blocks_active_color' => array(
            'type' => 'color',
            'label' => $object->l('Text hover color:', $name),
            'name' => 'showmore_blocks_active_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
        'showmore_blocks_active_bg' => array(
            'type' => 'color',
            'label' => $object->l('Hover background:', $name),
            'name' => 'showmore_blocks_active_bg',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
    );
    $form_fields['showmore_items'] = array(
        'showmore_btn_color' => array(
            'type' => 'color',
            'label' => $object->l('Text color:', $name),
            'name' => 'showmore_btn_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
        'showmore_btn_active_color' => array(
            'type' => 'color',
            'label' => $object->l('Text hover color:', $name),
            'name' => 'showmore_btn_active_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
    );
    $form_fields['other'] = array(
        'custom_css' => array(
            'type' => 'textarea',
            'label' => $object->l('Custom CSS Code:', $name),
            'name' => 'custom_css',
            'cols' => 80,
            'rows' => 20,
            'validation' => 'isAnything',
        ),
    );
    $form_fields['loading'] = array(
        'loading_effect' => array(
            'type' => 'radio',
            'label' => $object->l('Loading effect', $name),
            'name' => 'loading_effect',
            'default_value' => 3,
            'values' => array(
                array(
                    'id' => 'loading_effect_2',
                    'value' => 2,
                    'label' => $object->l('Show a loading icon on the top right corner of browser window', $name)),
                array(
                    'id' => 'loading_effect_1',
                    'value' => 1,
                    'label' => $object->l('Add a full screen overlay', $name)),
                array(
                    'id' => 'loading_effect_0',
                    'value' => 0,
                    'label' => $object->l('Gray out product lists', $name)),
                array(
                    'id' => 'loading_effect_3',
                    'value' => 3,
                    'label' => $object->l('Add a overlay to the filter block', $name)),
            ),
            'validation' => 'isUnsignedInt',
        ),
        'product_list_selector' => array(
            'type' => 'text',
            'label' => $object->l('Product lists selector:', $name),
            'name' => 'product_list_selector',
            'class' => 'fixed-width-lg',
            'validation' => 'isAnything',
            'desc' => $object->l('Leave it empty to use the default #js-product-list', $name),
        ),
        'loading_overlay_bg' => array(
            'type' => 'color',
            'label' => $object->l('Overlay background:', $name),
            'name' => 'loading_overlay_bg',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
        'loading_overlay_opacity' => array(
            'type' => 'text',
            'label' => $object->l('Overlay opacity:', $name),
            'name' => 'loading_overlay_opacity',
            'validation' => 'isFloat',
            'class' => 'fixed-width-lg',
            'desc' => $object->l('From 0.0 (fully transparent) to 1.0 (fully opaque).', $name),
        ),
        'loading_icon_color' => array(
            'type' => 'color',
            'label' => $object->l('Loading icon color:', $name),
            'name' => 'loading_icon_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
        'loading_icon_size' => array(
            'type' => 'text',
            'label' => $object->l('Loading icon size:', $name),
            'name' => 'loading_icon_size',
            'prefix' => 'px',
            'class' => 'fixed-width-lg',
            'default_value' => 16,
            'validation' => 'isUnsignedInt',
            'desc' => $object->l('Leave it empty to use the default value', $name),
        ),
    );
    $form_fields['button'] = array(
        'button_color' => array(
            'type' => 'color',
            'label' => $object->l('Button text color:', $name),
            'name' => 'button_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
        'button_bg' => array(
            'type' => 'color',
            'label' => $object->l('Button background:', $name),
            'name' => 'button_bg',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
        ),
        'button_active_color' => array(
            'type' => 'color',
            'label' => $object->l('Button selected text color:', $name),
            'name' => 'button_active_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
        'button_active_bg' => array(
            'type' => 'color',
            'label' => $object->l('Button selected background:', $name),
            'name' => 'button_active_bg',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
        ),
        'button_border_color' => array(
            'type' => 'color',
            'label' => $object->l('Button border color:', $name),
            'name' => 'button_border_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
        'button_active_border_color' => array(
            'type' => 'color',
            'label' => $object->l('Button selected border color:', $name),
            'name' => 'button_active_border_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
        'button_border_size' => array(
            'type' => 'text',
            'label' => $object->l('Button border size:', $name),
            'name' => 'button_border_size',
            'prefix' => 'px',
            'class' => 'fixed-width-lg',
            'default_value' => 0,
            'validation' => 'isUnsignedInt',
        ),
        'button_border_radius' => array(
            'type' => 'text',
            'label' => $object->l('Button border radius:', $name),
            'name' => 'button_border_radius',
            'prefix' => 'px',
            'class' => 'fixed-width-lg',
            'default_value' => 0,
            'validation' => 'isUnsignedInt',
        ),
        'button_text_size' => array(
            'type' => 'text',
            'label' => $object->l('Button text size:', $name),
            'name' => 'button_text_size',
            'prefix' => 'px',
            'class' => 'fixed-width-lg',
            'default_value' => 0,
            'validation' => 'isUnsignedInt',
        ),
        /*'button_paddings' => array(
            'type' => 'html',
            'id' => 'button_paddings',
            'label'=> $object->l('Button paddings', $name),
            'name' => '',
        ),
        'button_min_width' => array(
            'type' => 'text',
            'label' => $object->l('Button min width:', $name),
            'name' => 'button_min_width',
            'prefix' => 'px',
            'class' => 'fixed-width-lg',
            'default_value' => 0,
            'validation' => 'isUnsignedInt',
        ),*/
    );
    $form_fields['checkbox'] = array(
        'checkbox_bg' => array(
            'type' => 'color',
            'label' => $object->l('Checkbox and radio background:', $name),
            'name' => 'checkbox_bg',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
        ),
        'checkbox_active_bg' => array(
            'type' => 'color',
            'label' => $object->l('Checkbox and radio selected background:', $name),
            'name' => 'checkbox_active_bg',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
        'checkbox_tick_color' => array(
            'type' => 'color',
            'label' => $object->l('Checkbox and radio tick color:', $name),
            'name' => 'checkbox_tick_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
        ),
        'checkbox_tick_size' => array(
            'type' => 'text',
            'label' => $object->l('Checkbox and radio tick size:', $name),
            'name' => 'checkbox_tick_size',
            'prefix' => 'px',
            'class' => 'fixed-width-lg',
            'default_value' => 0,
            'validation' => 'isUnsignedInt',
        ),
        'checkbox_border_color' => array(
            'type' => 'color',
            'label' => $object->l('Checkbox and radio border color:', $name),
            'name' => 'checkbox_border_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
        ),
        'checkbox_border_size' => array(
            'type' => 'text',
            'label' => $object->l('Checkbox and radio border size:', $name),
            'name' => 'checkbox_border_size',
            'prefix' => 'px',
            'class' => 'fixed-width-lg',
            'default_value' => 1,
            'validation' => 'isUnsignedInt',
        ),
        'checkbox_color' => array(
            'type' => 'color',
            'label' => $object->l('Label text color:', $name),
            'name' => 'checkbox_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
        'checkbox_active_color' => array(
            'type' => 'color',
            'label' => $object->l('Label text selected color:', $name),
            'name' => 'checkbox_active_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
         ),
    );
    $form_fields['color'] = array(
        'color_box_width' => array(
            'type' => 'text',
            'label' => $object->l('Color box width:', $name),
            'name' => 'color_box_width',
            'prefix' => 'px',
            'class' => 'fixed-width-lg',
            'default_value' => 0,
            'validation' => 'isUnsignedInt',
        ),
        'color_box_height' => array(
            'type' => 'text',
            'label' => $object->l('Color box height:', $name),
            'name' => 'color_box_height',
            'prefix' => 'px',
            'class' => 'fixed-width-lg',
            'default_value' => 0,
            'validation' => 'isUnsignedInt',
        ),
        'color_box_border_size' => array(
            'type' => 'text',
            'label' => $object->l('Color box border size:', $name),
            'name' => 'color_box_border_size',
            'prefix' => 'px',
            'class' => 'fixed-width-lg',
            'default_value' => 0,
            'validation' => 'isUnsignedInt',
        ),
        'color_box_border_radius' => array(
            'type' => 'text',
            'label' => $object->l('Color box border radius:', $name),
            'name' => 'color_box_border_radius',
            'prefix' => 'px',
            'class' => 'fixed-width-lg',
            'default_value' => 0,
            'validation' => 'isUnsignedInt',
        ),
        'color_box_border_color' => array(
            'type' => 'color',
            'label' => $object->l('Color box border color:', $name),
            'name' => 'color_box_border_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
        ),
        'color_box_border_hover_color' => array(
            'type' => 'color',
            'label' => $object->l('Color box border selected color:', $name),
            'name' => 'color_box_border_hover_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
        ),
    );
    $form_fields['img'] = array(
        'img_border_size' => array(
            'type' => 'text',
            'label' => $object->l('Border size:', $name),
            'name' => 'img_border_size',
            'prefix' => 'px',
            'class' => 'fixed-width-lg',
            'default_value' => 0,
            'validation' => 'isUnsignedInt',
        ),
        'img_border_radius' => array(
            'type' => 'text',
            'label' => $object->l('Border radius:', $name),
            'name' => 'img_border_radius',
            'prefix' => 'px',
            'class' => 'fixed-width-lg',
            'default_value' => 0,
            'validation' => 'isUnsignedInt',
        ),
        'img_border_color' => array(
            'type' => 'color',
            'label' => $object->l('Border color:', $name),
            'name' => 'img_border_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
        ),
        'img_border_hover_color' => array(
            'type' => 'color',
            'label' => $object->l('Border selected color:', $name),
            'name' => 'img_border_hover_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
        ),
    );
    $form_fields['select'] = array(
        'select_color' => array(
            'type' => 'color',
            'label' => $object->l('Text color:', $name),
            'name' => 'select_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
        ),
        'select_actve_color' => array(
            'type' => 'color',
            'label' => $object->l('Active text color:', $name),
            'name' => 'select_actve_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
        ),
        'select_bg' => array(
            'type' => 'color',
            'label' => $object->l('Background:', $name),
            'name' => 'select_bg',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
        ),
        'select_actve_bg' => array(
            'type' => 'color',
            'label' => $object->l('Active background:', $name),
            'name' => 'select_actve_bg',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
        ),
        'select_border_size' => array(
            'type' => 'text',
            'label' => $object->l('Border size:', $name),
            'name' => 'select_border_size',
            'prefix' => 'px',
            'class' => 'fixed-width-lg',
            'default_value' => 0,
            'validation' => 'isUnsignedInt',
        ),
        'select_border_color' => array(
            'type' => 'color',
            'label' => $object->l('Border color:', $name),
            'name' => 'select_border_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
        ),
        'select_active_border_color' => array(
            'type' => 'color',
            'label' => $object->l('Active border color:', $name),
            'name' => 'select_active_border_color',
            'class' => 'color',
            'size' => 20,
            'validation' => 'isColor',
        ),
    );
    return $form_fields;
}