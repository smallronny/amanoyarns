<?php
/**
 * 2007-2019 PrestaShop.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2019 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */

namespace PrestaShop\Module\StFacetedSearch\Product;

use Configuration;
use Context;
use PrestaShop\Module\StFacetedSearch\Filters;
use PrestaShop\Module\StFacetedSearch\URLSerializer;
use PrestaShop\PrestaShop\Core\Product\Search\Facet;
use PrestaShop\PrestaShop\Core\Product\Search\FacetCollection;
use PrestaShop\PrestaShop\Core\Product\Search\FacetsRendererInterface;
use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchContext;
use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchProviderInterface;
use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchQuery;
use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchResult;
use PrestaShop\PrestaShop\Core\Product\Search\SortOrder;
use PrestaShop\PrestaShop\Core\Product\Search\URLFragmentSerializer;
use StFacetedsearch;
use Tools;
use Module;

class SearchProvider implements FacetsRendererInterface, ProductSearchProviderInterface
{
    /**
     * @var StFacetedsearch
     */
    private $module;

    /**
     * @var Filters\Converter
     */
    private $filtersConverter;

    /**
     * @var URLSerializer
     */
    private $facetsSerializer;

    /**
     * @var id_st_search_filter
     */
    public $id_st_search_filter = 0;

    public function __construct(
        StFacetedsearch $module,
        Filters\Converter $converter,
        URLSerializer $serializer
    ) {
        $this->module = $module;
        $this->filtersConverter = $converter;
        $this->facetsSerializer = $serializer;
    }

    /**
     * @return array
     */
    private function getAvailableSortOrders()
    {
        $sortPosAsc = new SortOrder('product', 'position', 'asc');
        $sortNameAsc = new SortOrder('product', 'name', 'asc');
        $sortNameDesc = new SortOrder('product', 'name', 'desc');
        $sortPriceAsc = new SortOrder('product', 'price', 'asc');
        $sortPriceDesc = new SortOrder('product', 'price', 'desc');
        
        $sort_options = [
            $sortPosAsc->setLabel(
                $this->module->l('Relevance', 'searchprovider')
            ),
            $sortNameAsc->setLabel(
                $this->module->l('Name, A to Z', 'searchprovider')
            ),
            $sortNameDesc->setLabel(
                $this->module->l('Name, Z to A', 'searchprovider')
            ),
            $sortPriceAsc->setLabel(
                $this->module->l('Price, low to high', 'searchprovider')
            ),
            $sortPriceDesc->setLabel(
                $this->module->l('Price, high to low', 'searchprovider')
            ),
        ];
        if(Configuration::get($this->module->_prefix_st.'BY_SALES')){
            $sortSaleAsc = new SortOrder('product_shop', 'sale', 'asc');
            $sortSaleDesc = new SortOrder('product_shop', 'sale', 'desc');

            $sort_options = array_merge($sort_options,array(
                $sortSaleAsc->setLabel(
                    $this->module->l('Sales, low to high', 'searchprovider')
                ),
                $sortSaleDesc->setLabel(
                    $this->module->l('Sales, high to low', 'searchprovider')
                ),
            ));
        }
        return $sort_options;
    }

    /**
     * @param ProductSearchContext $context
     * @param ProductSearchQuery $query
     *
     * @return ProductSearchResult
     */
    public function runQuery(
        ProductSearchContext $context,
        ProductSearchQuery $query
    ) {
        $result = new ProductSearchResult();
        // extract the filter array from the Search query
        $facetedSearchFilters = $this->filtersConverter->createFacetedSearchFiltersFromQuery($query);
        // Add filters to show relevant image
        StFacetedsearch::setFacetedSearchFilters($facetedSearchFilters);
        $context = $this->module->getContext();
        $facetedSearch = new Search($context);
        // init the search with the initial population associated with the current filters
        $facetedSearch->initSearch($facetedSearchFilters);

        if (\Module::isEnabled('stproductsbyattrs') && !\Tools::getValue('order') && ($sort_order = \Configuration::get('ST_PRO_ATTR_SORT_ORDER'))) {
            list($orderBy, $orderWay) = explode('-', $sort_order);
            $query->setSortOrder(new SortOrder('product', $orderBy, $orderWay));
        }

        $orderBy = $query->getSortOrder()->toLegacyOrderBy(false);
        $orderWay = $query->getSortOrder()->toLegacyOrderWay();

        $filterProductSearch = new Filters\Products($facetedSearch);

        // get the product associated with the current filter
        $productsAndCount = $filterProductSearch->getProductByFilters(
            $query->getResultsPerPage(),
            $query->getPage(),
            $orderBy,
            $orderWay,
            $facetedSearchFilters
        );

        $result
            ->setProducts($productsAndCount['products'])
            ->setTotalProductsCount($productsAndCount['count'])
            ->setAvailableSortOrders($this->getAvailableSortOrders());

        // now get the filter blocks associated with the current search
        $filterBlockSearch = new Filters\Block(
            $facetedSearch->getSearchAdapter(),
            $context,
            $this->module->getDatabase()
        );

        $idShop = (int) $context->shop->id;
        $idLang = (int) $context->language->id;
        $idCurrency = (int) $context->currency->id;
        $idCountry = (int) $context->country->id;
        $idCategory = (int) $query->getIdCategory();
        $idManufacturer = (int) $query->getIdManufacturer();
        $idSupplier = (int) $query->getIdSupplier();
        $page = \Dispatcher::getInstance()->getController();

        $filterHash = md5(
            sprintf(
                '%d-%d-%d-%d-%d-%s-%s-%s-%s',
                $idShop,
                $idCurrency,
                $idLang,
                $idCategory,
                $idManufacturer,
                $idSupplier,
                $idCountry,
                serialize($facetedSearchFilters),
                $page
            )
        );
        $filterBlock = $filterBlockSearch->getFromCache($filterHash);
        // if (1) {
        if (empty($filterBlock)) {
            $filterBlock = $filterBlockSearch->getFilterBlock($productsAndCount['count'], $facetedSearchFilters);
            $filterBlockSearch->insertIntoCache($filterHash, $filterBlock);
        }
        $this->id_st_search_filter = $filterBlock['id_st_search_filter'];
        $facets = $this->filtersConverter->getFacetsFromFilterBlocks(
            $filterBlock
        );

        $this->labelRangeFilters($facets);
        $this->addEncodedFacetsToFilters($facets);
        $this->addPWEncodedFacets($facets);
        $this->hideUselessFacets($facets, (int) $result->getTotalProductsCount());

        $facetCollection = new FacetCollection();
        $nextMenu = $facetCollection->setFacets($facets);
        $result->setFacetCollection($nextMenu);
        $result->setEncodedFacets($this->facetsSerializer->serialize($facets));

        return $result;
    }

    /**
     * Renders an product search result.
     *
     * @param ProductSearchContext $context
     * @param ProductSearchResult $result
     *
     * @return string the HTML of the facets
     */
    public function renderFacets(ProductSearchContext $context, ProductSearchResult $result)
    {
        list($activeFilters, $displayedFacets, $facetsVar) = $this->prepareActiveFiltersForRender($context, $result);

        // No need to render without facets
        if (empty($facetsVar)) {
            return '';
        }
        $template = 'module:stfacetedsearch/views/templates/front/catalog/facets.tpl';
        /*if(Module::isInstalled('stthemeeditor') && Module::isEnabled('stthemeeditor')){
            $template = 'views/templates/front/catalog/facets_st.tpl';
        }*/
        $this->module->getContext()->smarty->assign(
            [
                'show_quantities' => Configuration::get('ST_FAC_SEARCH_SHOW_QTIES'),
                'facets' => $facetsVar,
                'js_enabled' => $this->module->isAjax(),
                'displayedFacets' => $displayedFacets,
                'activeFilters' => $activeFilters,
                'sort_order' => $result->getCurrentSortOrder()->toString(),
                'clear_all_link' => $this->updateQueryString(
                    [
                        'q' => null,
                        'page' => null,
                    ]
                ),
                'template' => $this->getTemplateParams(),
                'with_inputs' => Configuration::get($this->module->_prefix_st.'WITH_INPUTS'),
                'range_style' => (int)Configuration::get($this->module->_prefix_st.'RANGE_STYLE'),
                'vertical' => Configuration::get($this->module->_prefix_st.'VERTICAL'),
                'disable_range_text' => Configuration::get($this->module->_prefix_st.'DISABLE_RANGE_TEXT'),
                'show_on' => Configuration::get($this->module->_prefix_st.'SHOW_ON'),
                'show_on_mobile' => Configuration::get($this->module->_prefix_st.'SHOW_ON_MOBILE'),
                'drop_down_position' => Configuration::get($this->module->_prefix_st.'DROP_DOWN_POSITION'),
                'collapse' => Configuration::get($this->module->_prefix_st.'COLLAPSE'),
                'show_active_filters' => (int)Configuration::get($this->module->_prefix_st.'ACTIVE_FILTERS'),
                'filters_per_xl' => (int)Configuration::get($this->module->_prefix_st.'FILTERS_PER_XL'),
                'filters_per_lg' => (int)Configuration::get($this->module->_prefix_st.'FILTERS_PER_LG'),
                'filters_per_md' => (int)Configuration::get($this->module->_prefix_st.'FILTERS_PER_MD'),
                'color_view' => (int)Configuration::get($this->module->_prefix_st.'COLOR_VIEW'),
            ]
        );
        return $this->module->fetch(
            $template
        );
    }

    /**
     * Parse parameters from template settings.
     *
     * @return array
     */
    public function getTemplateParams()
    {
        $filters = $this->module->getDatabase()->getValue('SELECT `filters` FROM `'._DB_PREFIX_.'st_search_filter` WHERE `id_st_search_filter` = '.(int)$this->id_st_search_filter);
        $params = $filters ? unserialize($filters) : [];
        if (isset($params['parameters'])) {
            $params = $params['parameters'];
            // Multlang for title.
            $id_lang = Context::getContext()->language->id;
            $params['title'] = isset($params['title']) ? $params['title'][$id_lang] : $this->module->getDefaultTitle();
            $params['collapse'] = isset($params['collapse']) ? (int)$params['collapse'] : 0;
        }else{
            $params['title'] = $this->module->getDefaultTitle();
        }
        return $params;
    }

    /**
     * Renders an product search result of active filters.
     *
     * @param ProductSearchContext $context
     * @param ProductSearchResult $result
     *
     * @return string the HTML of the facets
     */
    public function renderActiveFilters(ProductSearchContext $context, ProductSearchResult $result)
    {
        list($activeFilters) = $this->prepareActiveFiltersForRender($context, $result);

        $this->module->getContext()->smarty->assign(
            [
                'activeFilters' => $activeFilters,
                'clear_all_link' => $this->updateQueryString(
                    [
                        'q' => null,
                        'page' => null,
                    ]
                ),
            ]
        );
        return $this->module->fetch(
            'module:stfacetedsearch/views/templates/front/catalog/active-filters.tpl'
        );
    }

    /**
     * Prepare active filters for renderer.
     *
     * @param ProductSearchContext $context
     * @param ProductSearchResult $result
     *
     * @return array|null
     */
    private function prepareActiveFiltersForRender(ProductSearchContext $context, ProductSearchResult $result)
    {
        $facetCollection = $result->getFacetCollection();
        // not all search providers generate menus
        if (empty($facetCollection)) {
            return null;
        }

        $facets = $facetCollection->getFacets();

        foreach ($facets as $facet) {
            if($facet->getType()=='price' || $facet->getType()=='weight')
            {
                foreach ($facet->getFilters() as $k => $filter) {
                    if ($filter->isActive()) {
                        $facet->setProperty('lower', $filter->getValue()[0]);
                        $facet->setProperty('upper', $filter->getValue()[1]);
                    }
                }
            }
            elseif($facet->getWidgetType()=='slider')
            {
                $lower = null;
                $upper = 0;
                foreach ($facet->getFilters() as $k => $filter) {
                    if ($filter->isActive()) {
                        if($lower===null){
                            $lower = $k;
                        }
                        $upper = $k;
                    }
                }
                if($lower===null){
                    $lower = 0;
                    $facet->setProperty('nolower', 1);
                }
                if($lower || $upper){
                    $facet->setProperty('lower', $lower);
                    $facet->setProperty('upper', $upper);
                }
            }
        }

        $facetsVar = array_map(
            [$this, 'prepareFacetForTemplate'],
            $facets
        );

        $displayedFacets = [];
        $activeFilters = [];
        foreach ($facetsVar as $idx => $facet) {
            // Remove undisplayed facets
            if (!empty($facet['displayed'])) {
                $displayedFacets[] = $facet;
            }

            if(isset($facet['properties']['values'])){
                $hebing = false;
                $all_active_values = array();
                foreach ($facet['filters'] as $filter) {
                    if($filter['active'])
                        $all_active_values[] = $filter['label'];
                    if ($filter['active'] && !$hebing) {
                        $hebing = $filter;
                    }
                }
                if($hebing){
                    if(count($all_active_values)>1){
                        //  sort($all_active_values);
                        $hebing['label'] = $all_active_values[0].'-'.array_pop($all_active_values);
                    }
                    $activeFilters[] = $hebing;
                }
            }else{
                // Check if a filter is active
                foreach ($facet['filters'] as $filter) {
                    if ($filter['active']) {
                        $activeFilters[] = $filter;
                    }
                }
            }
        }
        // print_r($activeFilters);
        // print_r($facetsVar);die;
        return [
            $activeFilters,
            $displayedFacets,
            $facetsVar,
        ];
    }

    /**
     * Converts a Facet to an array with all necessary
     * information for templating.
     *
     * @param Facet $facet
     *
     * @return array ready for templating
     */
    protected function prepareFacetForTemplate(Facet $facet)
    {
        $facetsArray = $facet->toArray();
        /*if(1){
        // if($facetsArray['label']=='Color'){
            print_r($facetsArray);die;
        }*/
        foreach ($facetsArray['filters'] as &$filter) {
            $filter['facetLabel'] = $facet->getLabel();
            if ($filter['nextEncodedFacets'] || $facet->getWidgetType() === 'slider') {//fix pagination
                $filter['nextEncodedFacetsURL'] = $this->updateQueryString([
                    'q' => $filter['nextEncodedFacets'],
                    'page' => null,
                ]);
            } else {
                $filter['nextEncodedFacetsURL'] = $this->updateQueryString([
                    'q' => null,
                ]);
            }
        }
        unset($filter);

        return $facetsArray;
    }

    /**
     * Add a label associated with the facets
     *
     * @param array $facets
     */
    private function labelRangeFilters(array $facets)
    {
        foreach ($facets as $facet) {
            if (!in_array($facet->getType(), Filters\Converter::RANGE_FILTERS)) {
                continue;
            }

            foreach ($facet->getFilters() as $filter) {
                $filterValue = $filter->getValue();
                $min = empty($filterValue[0]) ? $facet->getProperty('min') : $filterValue[0];
                $max = empty($filterValue[1]) ? $facet->getProperty('max') : $filterValue[1];
                if ($facet->getType() === 'weight') {
                    $unit = Configuration::get('PS_WEIGHT_UNIT');
                    $filter->setLabel(
                        sprintf(
                            '%1$s%2$s - %3$s%4$s',
                            Tools::displayNumber($min),
                            $unit,
                            Tools::displayNumber($max),
                            $unit
                        )
                    );
                } elseif ($facet->getType() === 'price') {
                    $filter->setLabel(
                        sprintf(
                            '%1$s - %2$s',
                            Tools::displayPrice($min),
                            Tools::displayPrice($max)
                        )
                    );
                }
            }
        }
    }

    /**
     * This method generates a URL stub for each filter inside the given facets
     * and assigns this stub to the filters.
     * The URL stub is called 'nextEncodedFacets' because it is used
     * to generate the URL of the search once a filter is activated.
     */
    private function addEncodedFacetsToFilters(array $facets)
    {
        // first get the currently active facetFilter in an array
        $originalFacetFilters = $this->facetsSerializer->getActiveFacetFiltersFromFacets($facets);
        $urlSerializer = new URLFragmentSerializer();

        foreach ($facets as $facet) {
            $activeFacetFilters = $originalFacetFilters;
            // If only one filter can be selected, we keep track of
            // the current active filter to disable it before generating the url stub
            // and not select two filters in a facet that can have only one active filter.
            if (!$facet->isMultipleSelectionAllowed()) {
                foreach ($facet->getFilters() as $filter) {
                    if ($filter->isActive()) {
                        // we have a currently active filter is the facet, remove it from the facetFilter array
                        $activeFacetFilters = $this->facetsSerializer->removeFilterFromFacetFilters(
                            $originalFacetFilters,
                            $filter,
                            $facet
                        );
                        break;
                    }
                }
            }
            $filters = $facet->getFilters();
            $active_filters = array();
            if($facet->getWidgetType()=='slider'){
                foreach ($filters as $filter) {
                    if ($filter->isActive())
                        $active_filters[] = $filter;
                }
            }
            foreach ($facet->getFilters() as $filter) {
                $facetFilters = $activeFacetFilters;
                // toggle the current filter
                if ($filter->isActive()) {
                    if(count($active_filters)>1){
                        foreach ($active_filters as $af) {
                            $facetFilters = $this->facetsSerializer->removeFilterFromFacetFilters($facetFilters, $af, $facet);
                        }             
                    }
                    else
                        $facetFilters = $this->facetsSerializer->removeFilterFromFacetFilters($facetFilters, $filter, $facet);
                } else {
                    // if($facet->getType()!='price' && $facet->getType()!='weight') //q=price-$-11.00-100.00-price-$-11.00-52.00
                    $facetFilters = $this->facetsSerializer->addFilterToFacetFilters(
                        $facetFilters,
                        $filter,
                        $facet
                    );
                }

                // We've toggled the filter, so the call to serialize
                // returns the "URL" for the search when user has toggled
                // the filter.
                $filter->setNextEncodedFacets(
                    $urlSerializer->serialize($facetFilters)
                );
            }
        }
    }
    private function addPWEncodedFacets(array $facets)
    {
        // first get the currently active facetFilter in an array
        $activeFacetFilters = $this->facetsSerializer->getActiveFacetFiltersFromFacets($facets);
        $urlSerializer = new URLFragmentSerializer();

        foreach ($facets as $facet) {
            if($facet->getType()=='price' || $facet->getType()=='weight' || $facet->getWidgetType()!='slider')
                continue;
            $tempActiveFacetFilters = $activeFacetFilters;
            // If only one filter can be selected, we keep track of
            // the current active filter to disable it before generating the url stub
            // and not select two filters in a facet that can have only one active filter.
            if (!$facet->isMultipleSelectionAllowed()) {
                foreach ($facet->getFilters() as $filter) {
                    if ($filter->isActive()) {
                        // we have a currently active filter is the facet, remove it from the facetFilter array
                        $tempActiveFacetFilters = $this->facetsSerializer->removeFilterFromFacetFilters($tempActiveFacetFilters, $filter, $facet);
                        break;
                    }
                }
            }

            foreach ($facet->getFilters() as $filter) {
                $facetFilters = $tempActiveFacetFilters;
                $facetFilters = $this->facetsSerializer->addFilterToFacetFilters($facetFilters, $filter, $facet);
                if($facet->getWidgetType()=='slider'){
                    unset($facetFilters[$facet->getLabel()]);
                }
                $facet->setProperty('url', $this->updateQueryString(array(
                    'q' => $urlSerializer->serialize($facetFilters),
                    'page' => null,
                )));
                break;
            }
        }
    }


    /**
     * Remove the facet when there's only 1 result.
     * Keep facet status when it's a slider
     *
     * @param array $facets
     * @param int $totalProducts
     */
    private function hideUselessFacets(array $facets, $totalProducts)
    {
        foreach ($facets as $facet) {
            if ($facet->getWidgetType() === 'slider') {
                $facet->setDisplayed(
                    $facet->getProperty('min') != $facet->getProperty('max')
                );
                continue;
            }
            /*if($facet->getType()=='price' || $facet->getType()=='weight') {
                 $facet->setDisplayed(
                    $facet->getProperty('min') != $facet->getProperty('max')
                );
                continue;
            } elseif ($facet->getWidgetType() === 'slider') {
                $facet->setDisplayed(count($facet->getFilters())>1 && $facet->getProperty('values'));
                continue;
            }*/
            $totalFacetProducts = 0;
            $usefulFiltersCount = 0;
            foreach ($facet->getFilters() as $filter) {
                if ($filter->getMagnitude() > 0 && $filter->isDisplayed()) {
                    $totalFacetProducts += $filter->getMagnitude();
                    ++$usefulFiltersCount;
                }
            }

            $facet->setDisplayed(
                // There are two filters displayed
                $usefulFiltersCount > 1
                ||
                /*
                 * There is only one fitler and the
                 * magnitude is different than the
                 * total products
                 */
                (
                    count($facet->getFilters()) === 1
                    && $totalFacetProducts < $totalProducts
                    && $usefulFiltersCount > 0
                )
            );
        }
    }

    /**
     * Generate a URL corresponding to the current page but
     * with the query string altered.
     *
     * Params from $extraParams that have a null value are stripped,
     * and other params are added. Params not in $extraParams are unchanged.
     */
    private function updateQueryString(array $extraParams = [])
    {
        $uriWithoutParams = explode('?', $_SERVER['REQUEST_URI'])[0];
        $url = Tools::getCurrentUrlProtocolPrefix() . $_SERVER['HTTP_HOST'] . $uriWithoutParams;
        $params = [];
        $paramsFromUri = '';
        if (strpos($_SERVER['REQUEST_URI'], '?') !== false) {
            $paramsFromUri = explode('?', $_SERVER['REQUEST_URI'])[1];
        }
        parse_str($paramsFromUri, $params);

        foreach ($extraParams as $key => $value) {
            if (null === $value) {
                // Force clear param if null value is passed
                unset($params[$key]);
            } else {
                $params[$key] = $value;
            }
        }

        foreach ($params as $key => $param) {
            if (null === $param || '' === $param) {
                unset($params[$key]);
            }
        }

        $queryString = str_replace('%2F', '/', http_build_query($params, '', '&'));

        return $url . ($queryString ? "?$queryString" : '');
    }
}
