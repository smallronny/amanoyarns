<?php
/**
 * 2007-2019 PrestaShop.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2019 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */

namespace PrestaShop\Module\StFacetedSearch\Hook;

class Design extends AbstractHook
{
    private $_prefix_st = 'ST_FAC_SEARCH_';
    const AVAILABLE_HOOKS = [
        'displayLeftColumn',
        'displayHeader',
        'displayOverrideTemplate',
        'displayAfterBodyOpeningTag',
    ];

    /**
     * Force this hook to be called here instance of using WidgetInterface
     * because Hook::isHookCallableOn before the instanceof function.
     * Which means is_callable always returns true with a __call usage.
     *
     * @param array $params
     */
    public function displayLeftColumn(array $params)
    {
        if(\Configuration::get($this->_prefix_st.'SHOW_ON'))
            return ;
        return $this->module->fetch('module:stfacetedsearch/views/templates/hook/stfacetedsearch.tpl');
    }
    public function displayOverrideTemplate(array $params)
    {
        if(
            ($params['controller']->php_self == 'category' && $params['template_file'] == 'catalog/listing/category') ||
            ($params['controller']->php_self == 'new-products' && $params['template_file'] == 'catalog/listing/new-products') ||
            ($params['controller']->php_self == 'best-sales' && $params['template_file'] == 'catalog/listing/best-sales') ||
            ($params['controller']->php_self == 'manufacturer' && $params['template_file'] == 'catalog/listing/manufacturer') ||
            ($params['controller']->php_self == 'prices-drop' && $params['template_file'] == 'catalog/listing/prices-drop') ||
            ($params['controller']->php_self == 'supplier' && $params['template_file'] == 'catalog/listing/supplier')
        )
            return 'module:stfacetedsearch/views/templates/listing/'.$params['controller']->php_self.'.tpl';
    }
    public function displayAfterBodyOpeningTag(array $params)
    {
        $this->context->smarty->assign([
            'clear_all_link' => $this->updateQueryString(
                    [
                        'q' => null,
                        'page' => null,
                    ]
                ),
        ]);
        return $this->module->fetch('module:stfacetedsearch/views/templates/hook/sidebar.tpl');
    }
    private function updateQueryString(array $extraParams = [])
    {
        $uriWithoutParams = explode('?', $_SERVER['REQUEST_URI'])[0];
        $url = \Tools::getCurrentUrlProtocolPrefix() . $_SERVER['HTTP_HOST'] . $uriWithoutParams;
        $params = [];
        $paramsFromUri = '';
        if (strpos($_SERVER['REQUEST_URI'], '?') !== false) {
            $paramsFromUri = explode('?', $_SERVER['REQUEST_URI'])[1];
        }
        parse_str($paramsFromUri, $params);

        foreach ($extraParams as $key => $value) {
            if (null === $value) {
                // Force clear param if null value is passed
                unset($params[$key]);
            } else {
                $params[$key] = $value;
            }
        }

        foreach ($params as $key => $param) {
            if (null === $param || '' === $param) {
                unset($params[$key]);
            }
        }

        $queryString = str_replace('%2F', '/', http_build_query($params, '', '&'));

        return $url . ($queryString ? "?$queryString" : '');
    }
}
