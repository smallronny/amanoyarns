###############################################################################
Change Log : Block Product V4.0.1 | COMPATIBLE:: PRESTASHOP V1.7 |
###############################################################################

New Feature:
    -Admin can hide the blocked product from the site.



###############################################################################
Change Log : Block Product V4.0.0 | COMPATIBLE:: PRESTASHOP V1.7 |
###############################################################################

Features:
    -Block products on the basis of customer shipping address or IP address.
    -Admin can enable/disable the display of product page.
    -Admin can configured the message which will be displayed on product page when the product is not available at the specific address.
    -Admin can select single or multiple countries to block the product.
    -Admin can select the single or multiple IP addresses to block the product.
    -Customer will be warned by the alert message on the product page.
    -Customer will be able to know the availability of the product in their country.