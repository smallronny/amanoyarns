<?php
/**
* 2010-2019 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2019 Webkul IN
*  @license   https://store.webkul.com/license.html
*/

class Product extends ProductCore
{
    public static function getProductProperties($id_lang, $row, Context $context = null)
    {
        if (Module::isEnabled('blockproduct')
        && Module::isInstalled('blockproduct')
        && Configuration::get('SHOW_PRODUCT_ON_SHOP')) {
            require_once _PS_MODULE_DIR_.'blockproduct/classes/BlockProductDetail.php';
            $obj_blockproductdetail = new BlockProductDetail();
            if ($obj_blockproductdetail->checkBlockProduct($row['id_product'])) {
                return false;
            } else {
                return parent::getProductProperties($id_lang, $row, $context);
            }
        } else {
            return parent::getProductProperties($id_lang, $row, $context);
        }
    }
}
