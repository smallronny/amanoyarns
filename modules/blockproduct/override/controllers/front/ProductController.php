<?php
/**
* 2010-2017 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2017 Webkul IN
*  @license   https://store.webkul.com/license.html
*/

class ProductController extends ProductControllerCore
{
    /**
     * Assign template vars related to page content.
     *
     * @see FrontController::initContent()
     */
    public function initContent()
    {
        if (Module::isEnabled('blockproduct')
        && Module::isInstalled('blockproduct')
        && Configuration::get('SHOW_PRODUCT_ON_SHOP')) {
            $id_product = $this->product->id;
            $obj_blockproductdetail = new BlockProductDetail();
            $allblockproduct = $obj_blockproductdetail->getAllBlockProductId();
            $allowed = false;
            if ($allblockproduct) {
                foreach ($allblockproduct as $blockproduct) {
                    if ($id_product == $blockproduct['id_product'] && $blockproduct['active']) {
                        // checking country wise
                        $user_ip = Tools::getRemoteAddr();
                        if (Configuration::get('WK_COUNTRY_BASED_ON') == 1) {
                            if ($this->context->cookie->logged) {
                                $iso_code = $this->context->country->iso_code;
                            } else {
                                $result = false;
                                $ip_data = Tools::jsonDecode(
                                    Tools::file_get_contents('http://www.geoplugin.net/json.gp?ip='.$user_ip)
                                );
                                if ($ip_data && $ip_data->geoplugin_countryName != null) {
                                    $result = $ip_data->geoplugin_countryCode;
                                }
                                $iso_code = $result;
                            }
                        } else {
                            $result = false;
                            $ip_data = Tools::jsonDecode(
                                Tools::file_get_contents('http://www.geoplugin.net/json.gp?ip='.$user_ip)
                            );
                            if ($ip_data && $ip_data->geoplugin_countryName != null) {
                                $result = $ip_data->geoplugin_countryCode;
                            }
                            $iso_code = $result;
                        }
                        $block_country = explode(';', $blockproduct['block_country']);
                        if (is_array($block_country) && count($block_country) && $iso_code) {
                            if (in_array(Tools::strtoupper($iso_code), $block_country)) {
                                $allowed = true;
                            }
                        }
                        // IP based
                        if (!$allowed) {
                            $allowed = false;
                            $user_ip = Tools::getRemoteAddr();
                            $block_ip = explode(';', $blockproduct['block_ip']);
                            $ips = array();
                            if (is_array($block_ip) && count($block_ip)) {
                                foreach ($block_ip as $ip) {
                                    $ips = array_merge($ips, explode("\n", $ip));
                                }
                            }
                            $ips = array_map('trim', $ips);
                            if (is_array($ips) && count($ips)) {
                                foreach ($ips as $ip) {
                                    if (!empty($ip) && preg_match('/^'.$ip.'.*/', $user_ip)) {
                                        $allowed = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if ($allowed) {
                header('HTTP/1.1 404 Not Found');
                header('Status: 404 Not Found');
                $this->errors[] = $this->trans('This product is no longer available.', array(), 'Shop.Notifications.Error');
                $this->setTemplate('errors/404');
            } else {
                parent::initContent();
            }
        } else {
            parent::initContent();
        }
        parent::initContent();
    }
}
