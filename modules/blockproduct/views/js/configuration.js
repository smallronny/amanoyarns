/**
* 2010-2019 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2019 Webkul IN
*  @license   https://store.webkul.com/license.html
*/

$(document).ready(function(){
	if ($('input[name="SHOW_PRODUCT_ON_SHOP"]:checked').val() == 1) {
		$(".default_message").closest(".form-group").hide();
		$("#SHOW_PRODUCT_DETAIL_PAGE_on").closest('.form-group').hide();
	}
	if ($('input[name="SHOW_PRODUCT_ON_SHOP"]:checked').val() == 0) {
		if ($('input[name="SHOW_PRODUCT_DETAIL_PAGE"]:checked').val() == 1) {
			$(".default_message").closest('.form-group').show();
		}
		$("#SHOW_PRODUCT_DETAIL_PAGE_on").closest('.form-group').show();
	}
	$('input[name=SHOW_PRODUCT_ON_SHOP]').on('change', function(){
		if ($(this).val() == 1) {
			$(".default_message").closest('.form-group').hide();
			$("#SHOW_PRODUCT_DETAIL_PAGE_on").closest('.form-group').hide();
		} else {
			if ($('input[name="SHOW_PRODUCT_DETAIL_PAGE"]:checked').val() == 1) {
				$(".default_message").closest('.form-group').show();
			}
			$("#SHOW_PRODUCT_DETAIL_PAGE_on").closest('.form-group').show();
		}
	});

	//hide and show text according to switch
	$('label[for="SHOW_PRODUCT_DETAIL_PAGE_on"]').on("click", function(){
		$(".default_message").closest('.form-group').show();
	});

	$('label[for="SHOW_PRODUCT_DETAIL_PAGE_off"]').on("click", function(){
		$(".default_message").closest('.form-group').hide();
	});

	$('.blockProduct-alert').each(function(){
		$(this).closest('.thumbnail-container').append($(this).detach());
	});
});
