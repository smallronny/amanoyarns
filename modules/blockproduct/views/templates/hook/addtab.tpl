{*
**
* 2010-2019 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
* @author    Webkul IN <support@webkul.com>
* @copyright 2010-2019 Webkul IN
* @license   https://store.webkul.com/license.html
*}


<div id="product-prices" class="product-tab">
	{if isset($id_product)}
		<div class="form-group row">
			<label class="col-lg-3">
				{l s='Enabled' mod='blockproduct'}
			</label>
			<div class="col-lg-9">
				<span class="switch prestashop-switch fixed-width-lg">
					<input onclick="showdetail(true)" type="radio" name="active_block" id="active_block_on" value="1" {if $active eq 1}checked="checked"{/if}>
					<label for="active_block_on" class="radioCheck">
						{l s='Yes' mod='blockproduct'}
					</label>
					<input onclick="showdetail(false)" type="radio" name="active_block" id="active_block_off" value="0" {if $active eq 0}checked="checked" {/if} />
					<label for="active_block_off" class="radioCheck">
						{l s='No' mod='blockproduct'}
					</label>
					<a class="slide-button btn"></a>
				</span>
			</div>
		</div>
		<div class="form-group row" id="country">
			<label class="col-lg-3"> {l s='Select the countries from which your product is not accessible' mod='blockproduct'} </label>
			<div style="height: 200px; overflow-y: auto;" class="well margin-form col-lg-5">
				<table style="border-spacing : 0; border-collapse : collapse;" class="table">
					<thead>
						<tr>
							<th><input type="checkbox" onclick="checkDelBoxes(this.form, 'countries[]', this.checked)" name="checkAll"></th>
							<th>{l s='Name' mod='blockproduct'}</th>
						</tr>
					</thead>
					<tbody>
						{if isset($country)}
							{foreach $country as $countrydetail}
								<tr>
									<td><input type="checkbox" value="{$countrydetail.iso_code|escape:'html':'UTF-8'}" name="countries[]" {if isset($block_country)}{foreach $block_country as $country}{if $country eq $countrydetail.iso_code}checked{/if}{/foreach}{/if} id=""></td>
									<td>{$countrydetail.name|escape:'html':'UTF-8'}</td>
								</tr>
							{/foreach}
						{/if}
					</tbody>
				</table>
			</div>
		</div>
		<div class="form-group row" id="ip">
			<label class="col-lg-3">{l s='Blacklist IP addresses' mod='blockproduct'}</label>
			<div class="col-lg-6">
				<textarea  name="blacklist_ip" class="form-control" number>{if isset($block_ip)}{$block_ip|escape:'html':'UTF-8'}{/if}</textarea>
			</div>
		</div>
	{else}
		<div class="alert alert-warning">
			<button class="close" data-dismiss="alert" type="button">×</button>
			{l s='There is 1 warning.' mod='blockproduct'}
			<ul style='display:block;' id='seeMore'>
				<li>{l s='You must save this product before adding ip address for blocking.'
				mod='blockproduct'}</li>
			</ul>
		</div>
	{/if}
</div>
<script type="text/javascript">
	$(document).ready(function() {
		//show on page load
		if ($('input[name="active_block"]:checked').val() == 1) {
			$("#country").show();
			$("#ip").show();
		} else {
			$("#country").hide();
			$("#ip").hide();
		}
	});

	//hide and show according to switch
	function showdetail(show)
	{
		if(show) {
			$("#country").show();
			$("#ip").show();
		} else {
			$("#country").hide();
			$("#ip").hide();
		}
	}
</script>