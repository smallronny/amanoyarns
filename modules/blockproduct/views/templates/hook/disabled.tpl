{*
**
* 2010-2019 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
* @author    Webkul IN <support@webkul.com>
* @copyright 2010-2019 Webkul IN
* @license   https://store.webkul.com/license.html
*}


<style>
	.blockProduct-alert{
		text-align: center;position:absolute;top:0;
	}
	.product-description .blockProduct-alert{
		display: none;
	}
</style>
{if isset($check) && $check > 0}
<div class="alert alert-danger blockProduct-alert" role="alert">
	{if $check eq 1}
		<i class="material-icons">error_outline</i><span>{$message}</span>
	{elseif $check eq 2}
		<i class="material-icons">error_outline</i><span>{$message}</span>
	{/if}
</div>
{/if}

