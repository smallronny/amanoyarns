{*
**
* 2010-2019 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
* @author    Webkul IN <support@webkul.com>
* @copyright 2010-2019 Webkul IN
* @license   https://store.webkul.com/license.html
*}


{if isset($check) && $check > 0}
<div class="alert alert-danger mt-2" role="alert">
	{if $check eq 1}
		<i class="material-icons">error_outline</i><span>{$message}</span>
	{elseif $check eq 2}
		<i class="material-icons">error_outline</i><span>{$message}</span>
	{/if}
</div>
{/if}
<script type="text/javascript">
{if isset($id_product)}
	function evaluateBlockProduct(){
		var id_product = {$id_product|escape:'html':'UTF-8'};
		if (document.getElementById('product_page_product_id')){
			var product = document.getElementById('product_page_product_id').value;

			if (product == id_product) {
				var cart = document.getElementsByClassName('add-to-cart');
					if(cart[0]!=undefined){
						cart[0].parentNode.removeChild(cart[0]);
					}


			}
		}
	}
	evaluateBlockProduct();
{/if}


</script>
