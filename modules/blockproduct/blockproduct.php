<?php
/**
* 2010-2019 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2019 Webkul IN
*  @license   https://store.webkul.com/license.html
*/

if (!defined('_PS_VERSION_')) {
    exit;
}
include_once 'classes/WkBlockProductDb.php';
include_once 'classes/BlockProductDetail.php';
class BlockProduct extends Module
{
    private $displayContent = '';
    public function __construct()
    {
        $this->name = 'blockproduct';
        $this->tab = 'administration';
        $this->version = '4.0.1';
        $this->author = 'Webkul';
        $this->need_instance = 0;
        $this->bootstrap = true;
        parent::__construct();
        $this->displayName = $this->l('Block Product');
        $this->description = $this->l('Block product based on IP address or country wise');
    }

    private function postValidation()
    {
        if (Tools::isSubmit('btnSubmit')) {
            if (Tools::getValue('SHOW_PRODUCT_DETAIL_PAGE')) {
                if (Tools::getValue('WK_PRODUCT_DEFAULT_MESSAGE') == '') {
                    $this->_postErrors[] = $this->l('Default message field is required');
                }
            }
        }
    }

    private function postProcess()
    {
        if (Tools::isSubmit('btnSubmit')) {
            Configuration::updateValue('SHOW_PRODUCT_ON_SHOP', Tools::getValue('SHOW_PRODUCT_ON_SHOP'));
            Configuration::updateValue('SHOW_PRODUCT_DETAIL_PAGE', Tools::getValue('SHOW_PRODUCT_DETAIL_PAGE'));
            Configuration::updateValue('WK_PRODUCT_DEFAULT_MESSAGE', Tools::getValue('WK_PRODUCT_DEFAULT_MESSAGE'));
            Configuration::updateValue('WK_COUNTRY_BASED_ON', Tools::getValue('WK_COUNTRY_BASED_ON'));

            $module_config = $this->context->link->getAdminLink('AdminModules');
            Tools::redirectAdmin($module_config.'&configure='.$this->name.'&module_name='.$this->name.'&conf=4');
        }
    }

    public function hookDisplayProductListReviews($params)
    {
        $id_product = $params['product']['id_product'];
        $allowed = false;
        $check = 1;
        $obj_blockproductdetail = new BlockProductDetail();
        $allblockproduct = $obj_blockproductdetail->getAllBlockProductId();

        if ($allblockproduct) {
            foreach ($allblockproduct as $blockproduct) {
                if ($id_product == $blockproduct['id_product'] && $blockproduct['active']) {
                    // checking country wise
                    $allowed = $this->isBlackListCountryForProduct($blockproduct);
                    // IP based
                    if (!$allowed) {
                        $allowed = $this->isBlackListIpForProduct($blockproduct);
                        $check = 2;
                    }
                }
                if ($allowed) {
                    $this->context->smarty->assign('id_product', $id_product);
                    $this->context->smarty->assign('check', $check);
                    if (Configuration::get('WK_PRODUCT_DEFAULT_MESSAGE')
                    && Configuration::get('SHOW_PRODUCT_DETAIL_PAGE')) {
                        $this->context->smarty->assign('message', Configuration::get('WK_PRODUCT_DEFAULT_MESSAGE'));
                    } else {
                        $this->context->smarty->assign('message', $this->l('This product is not available.'));
                    }


                    break;
                } else {
                    $this->context->smarty->assign('check', 0);
                }
            }
        }
        return $this->fetch('module:blockproduct/views/templates/hook/disabled.tpl');
    }

    // For configuration page.
    public function getContent()
    {
        $this->context->controller->addJs($this->_path.'views/js/configuration.js');

        if (Tools::isSubmit('btnSubmit')) {
            $this->postValidation();
            if (!count($this->_postErrors)) {
                $this->postProcess();
            } else {
                foreach ($this->_postErrors as $err) {
                    $this->displayContent .= $this->displayError($err);
                }
            }
        }
        $this->displayContent .= $this->renderForm();

        return $this->displayContent;
    }

    public function renderForm()
    {
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Block Product Configuration'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'is_bool' => true,
                        'label' => $this->l('Hide Product on Shop'),
                        'class' => 't',
                        'name' => 'SHOW_PRODUCT_ON_SHOP',
                        'values' => array(
                                        array(
                                            'id' => 'active_on',
                                            'value' => 1,
                                            'label' => $this->l('Enabled'),
                                        ),
                                        array(
                                            'id' => 'active_off',
                                            'value' => 0,
                                            'label' => $this->l('Disabled'),
                                        ),
                                    ),
                        'hint' => $this->l('If yes, customer is unable to see product on shop.'),
                    ),
                    array(
                        'type' => 'switch',
                        'is_bool' => true,
                        'label' => $this->l('Show product detail page'),
                        'class' => 't',
                        'name' => 'SHOW_PRODUCT_DETAIL_PAGE',
                        'values' => array(
                                        array(
                                            'id' => 'active_on',
                                            'value' => 1,
                                            'label' => $this->l('Enabled'),
                                        ),
                                        array(
                                            'id' => 'active_off',
                                            'value' => 0,
                                            'label' => $this->l('Disabled'),
                                        ),
                                    ),
                        'hint' => $this->l('If yes, customer can see product detail.'),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Default message'),
                        'col' => 3,
                        'class' => 'default_message',
                        'name' => 'WK_PRODUCT_DEFAULT_MESSAGE',
                        'hint' => $this->l('If product detail page is enable, than what message should be displayed on product detail page for customer.'),
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Get country'),
                        'name' => 'WK_COUNTRY_BASED_ON',
                        'options' => array(
                            'query' => array(
                                array('key' => '1', 'name' =>  $this->l('Customer Address')),
                                array('key' => '2', 'name' =>  $this->l('IP Address')),
                            ),
                            'id' => 'key',
                            'name' => 'name',
                        ),
                        'hint' => $this->l('Get country detail based on customer address or IP address wise.'),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $this->fields_form = array();
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'btnSubmit';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.
        $this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($fields_form));
    }

    public function getConfigFieldsValues()
    {
        return array(
            'SHOW_PRODUCT_ON_SHOP' => Tools::getValue(
                'SHOW_PRODUCT_ON_SHOP',
                Configuration::get('SHOW_PRODUCT_ON_SHOP')
            ),
            'SHOW_PRODUCT_DETAIL_PAGE' => Tools::getValue(
                'SHOW_PRODUCT_DETAIL_PAGE',
                Configuration::get('SHOW_PRODUCT_DETAIL_PAGE')
            ),
            'WK_PRODUCT_DEFAULT_MESSAGE' => Tools::getValue(
                'WK_PRODUCT_DEFAULT_MESSAGE',
                Configuration::get('WK_PRODUCT_DEFAULT_MESSAGE')
            ),
            'WK_COUNTRY_BASED_ON' => Tools::getValue('WK_COUNTRY_BASED_ON', Configuration::get('WK_COUNTRY_BASED_ON')),
        );
    }

    //Backend:add tab in product catelog.
    public function hookDisplayAdminProductsExtra($params)
    {
        $id_product = $params['id_product'];
        $country = Country::getCountries($this->context->language->id);
        if ($id_product) {
            $obj_blockproductdetail = new BlockProductDetail();
            $blockproduct = $obj_blockproductdetail->checkBlockProduct($id_product);
            if ($blockproduct['active']) {
                $block_country = explode(';', $blockproduct['block_country']);
                $block_ip = str_replace(';', "\n", $blockproduct['block_ip']);
                $this->context->smarty->assign(
                    array(
                        'active' => $blockproduct['active'],
                        'block_country' => $block_country,
                        'block_ip' => $block_ip,
                    )
                );
            } else {
                $this->context->smarty->assign('active', 0);
            }
            $this->context->smarty->assign(
                array(
                    'id_product' => $id_product,
                    'country' => $country,
                )
            );
        }

        return $this->display(__FILE__, 'addtab.tpl');
    }

    //Delete Product From Cart.
    protected function deleteCartProduct()
    {
        $cart_id = $this->context->cart->id;
        $obj_blockproductdetail = new BlockProductDetail();
        $obj_cart = new Cart($cart_id);
        if ($obj_cart && isset($obj_cart)) {
            $cartproduct = $obj_cart->getProducts();
            if ($cartproduct) {
                foreach ($cartproduct as $product) {
                    $checkproduct = $obj_blockproductdetail->checkBlockProduct($product['id_product']);
                    if ($checkproduct && $checkproduct['active']) {
                        // checking country wise
                        $allowed = $this->isBlackListCountryForProduct($checkproduct);
                        // IP based
                        if (!$allowed) {
                            $allowed = $this->isBlackListIpForProduct($checkproduct);
                        }

                        if ($allowed) {
                            $obj_cart->deleteProduct($checkproduct['id_product'], $product['id_product_attribute']);
                        }
                    }
                }
            }
        }
    }

    //For order page.
    public function hookDisplayHeader()
    {
        $controller = Tools::getValue('controller');
        if ($controller == 'order' || $controller == 'orderopc') {
            $this->deleteCartProduct();
        } elseif ($controller == 'cart') {
            $this->deleteCartProduct();
        }
    }
    //For Address change.
    public function hookDisplayCarrierList($params)
    {
        $this->deleteCartProduct();
    }

    //Get country iso code.
    protected function getIsoCode()
    {
        $user_ip = Tools::getRemoteAddr();
        if (Configuration::get('WK_COUNTRY_BASED_ON') == 1) {
            if ($this->context->cookie->logged) {
                $iso_code = $this->context->country->iso_code;
            } else {
                $iso_code = $this->getLocationInfoByIp($user_ip);
            }
        } else {
            $iso_code = $this->getLocationInfoByIp($user_ip);
        }

        return $iso_code;
    }

    //Check product for country wise.
    protected function isBlackListCountryForProduct($blockproduct)
    {
        $allowed = false;
        $iso_code = $this->getIsoCode();
        $block_country = explode(';', $blockproduct['block_country']);
        if (is_array($block_country) && count($block_country) && $iso_code) {
            if (in_array(Tools::strtoupper($iso_code), $block_country)) {
                $allowed = true;
            }
        }

        return $allowed;
    }

    //check product for ip address wise.
    protected function isBlackListIpForProduct($blockproduct)
    {
        $allowed = false;
        $user_ip = Tools::getRemoteAddr();
        $block_ip = explode(';', $blockproduct['block_ip']);
        $ips = array();

        if (is_array($block_ip) && count($block_ip)) {
            foreach ($block_ip as $ip) {
                $ips = array_merge($ips, explode("\n", $ip));
            }
        }

        $ips = array_map('trim', $ips);

        if (is_array($ips) && count($ips)) {
            foreach ($ips as $ip) {
                if (!empty($ip) && preg_match('/^'.$ip.'.*/', $user_ip)) {
                    $allowed = true;
                }
            }
        }

        return $allowed;
    }

    //get country iso code through ip address.
    protected function getLocationInfoByIp($ip)
    {
        $result = false;
        $ip_data = Tools::jsonDecode(Tools::file_get_contents('http://www.geoplugin.net/json.gp?ip='.$ip));

        if ($ip_data && $ip_data->geoplugin_countryName != null) {
            $result = $ip_data->geoplugin_countryCode;
        }

        return $result;
    }

    // In Front list of product.
    public function hookDisplayProductButtons($params)
    {
        $id_product = $params['product']['id_product'];
        $allowed = false;
        $check = 1;
        $obj_blockproductdetail = new BlockProductDetail();
        $allblockproduct = $obj_blockproductdetail->getAllBlockProductId();
        if ($allblockproduct) {
            foreach ($allblockproduct as $blockproduct) {
                if ($id_product == $blockproduct['id_product'] && $blockproduct['active']) {
                    // checking country wise
                    $allowed = $this->isBlackListCountryForProduct($blockproduct);
                    // IP based
                    if (!$allowed) {
                        $allowed = $this->isBlackListIpForProduct($blockproduct);
                        $check = 2;
                    }
                }

                if ($allowed) {
                    if (Configuration::get('SHOW_PRODUCT_DETAIL_PAGE')) {
                        $this->context->smarty->assign('id_product', $id_product);
                        $this->context->smarty->assign('check', $check);
                        $this->context->smarty->assign('message', Configuration::get('WK_PRODUCT_DEFAULT_MESSAGE'));
                    } else {
                        Tools::redirect($this->context->link->getPageLink('index', true));
                    }

                    break;
                } else {
                    $this->context->smarty->assign('check', 0);
                }
            }
            return $this->fetch('module:blockproduct/views/templates/hook/blockmessage.tpl');
        }
    }

    //For admin add product.
    public function hookActionProductAdd($params)
    {
        $id_product = $params['id_product'];

        if (isset($id_product) && $id_product) {
            $obj_blockproductdetail = new BlockProductDetail();
            $obj_blockproductdetail->id_product = $id_product;
            $obj_blockproductdetail->save();
        }
    }

    //For admin update product.
    public function hookActionProductUpdate($params)
    {
        $id_product = (int) Tools::getValue('id_product');
        if (isset($id_product) && $id_product) {
            $active = Tools::getValue('active_block');
            $country = implode(';', Tools::getValue('countries', array()));
            $blacklist_ip = str_replace("\n", ';', str_replace("\r", '', Tools::getValue('blacklist_ip')));

            $obj_blockproductdetail = new BlockProductDetail();
            $blockproduct = $obj_blockproductdetail->checkBlockProduct($id_product);

            if ($blockproduct) {
                $obj_blockproductdetail = new BlockProductDetail($id_product);
                $obj_blockproductdetail->active = $active;
                $obj_blockproductdetail->block_country = $country;
                $obj_blockproductdetail->block_ip = $blacklist_ip;
                $obj_blockproductdetail->save();
            } else {
                $obj_blockproductdetail->id_product = $id_product;
                $obj_blockproductdetail->active = $active;
                $obj_blockproductdetail->block_country = $country;
                $obj_blockproductdetail->block_ip = $blacklist_ip;
                $obj_blockproductdetail->save();
            }
        }
    }

    //Install mudule function.
    public function install()
    {
        $blockObj = new WkBlockProductDb();

        if (!parent::install()
            || !$this->registerHook('displayAdminProductsExtra')
            || !$this->registerHook('displayProductTab')
            || !$this->registerHook('displayProductButtons')
            || !$this->registerHook('displayCarrierList')
            || !$this->registerHook('actionProductUpdate')
            || !$blockObj->createTables()
            || !$this->registerHook('displayProductListReviews')
            || !$this->registerHook('displayHeader')
            || !$this->registerHook('filterProductSearch')
            || !$this->registerHook('actionFrontControllerSetMedia')
            || !$this->registerHook('actionProductAdd')) {
            return false;
        }
        Configuration::updateValue('SHOW_PRODUCT_ON_SHOP', 0);
        Configuration::updateValue('SHOW_PRODUCT_DETAIL_PAGE', 0);
        Configuration::updateValue('WK_PRODUCT_DEFAULT_MESSAGE', 'This product not available in this country');
        Configuration::updateValue('WK_COUNTRY_BASED_ON', 1);

        return true;
    }

    public function hookFilterProductSearch($params)
    {
        if (Configuration::get('SHOW_PRODUCT_ON_SHOP')) {
            $products = $params['searchVariables']['result']->getProducts();
            $obj_blockproductdetail = new BlockProductDetail();
            foreach ($products as $key => $pr) {
                if ($obj_blockproductdetail->checkBlockProduct($pr['id_product'])) {
                    unset($params["searchVariables"]["products"][$key]);
                }
            }
        }
    }

    public function hookActionFrontControllerSetMedia()
    {
        $this->registerHook('filterProductSearch');
        $this->context->controller->addJs($this->_path.'views/js/configuration.js');
    }

    //For uninstall module.
    public function uninstall()
    {
        $blockObj = new WkBlockProductDb();
        if (!parent::uninstall()
            || !$blockObj->deleteBlockTable()) {
            return false;
        }

        return true;
    }
}
