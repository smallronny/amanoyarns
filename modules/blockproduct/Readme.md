- Module V4.0.1 compatible with PrestaShop V1.7.x.x

- Module V1.0.1 compatible with PrestaShop V1.6.x.x

### User Guide Documentation:
https://webkul.com/blog/prestashop-block-product/

### Support:
https://store.webkul.com/support.html/

### Refund:
https://store.webkul.com/refund-policy.html/


