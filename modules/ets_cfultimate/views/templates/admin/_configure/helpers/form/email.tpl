{*
* 2007-2020 ETS-Soft
*
* NOTICE OF LICENSE
*
* This file is not open source! Each license that you purchased is only available for 1 wesite only.
* If you want to use this file on more websites (or projects), you need to purchase additional licenses.
* You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please, contact us for extra customization service at an affordable price
*
*  @author ETS-Soft <etssoft.jsc@gmail.com>
*  @copyright  2007-2020 ETS-Soft
*  @license    Valid for 1 website (or project) for each purchase of license
*  International Registered Trademark & Property of ETS-Soft
*}
<li class="ets_cfu_li">
    <div class="ets_cfu_{$input.name|lower|escape:'html':'utf-8'} col-lg-4">
        <input type="text" class="ets_cfu_name" data-type="text" name="{$key|escape:'html':'utf-8'}[name][]"  value="{if isset($element) && $element}{$element.name|escape:'html':'utf-8'}{/if}" placeholder="{l s='Name' mod='ets_cfultimate'}" />
    </div>
    <div class="ets_cfu_{$input.name|lower|escape:'html':'utf-8'} col-lg-6">
        <input type="text" class="ets_cfu_email" data-type="email" name="{$key|escape:'html':'utf-8'}[email][]"  value="{if isset($element) && $element}{$element.email|escape:'html':'utf-8'}{/if}" placeholder="{l s='Email' mod='ets_cfultimate'}" />
    </div>
    {if isset($input.show_btn_add) && $input.show_btn_add}
        <div class="ets_cfu_{$input.name|lower|escape:'html':'utf-8'} button col-lg-2">
            {if $ik > $end}<span class="ets_cfu_add btn btn-primary" title="{l s='Add' mod='ets_cfultimate'}"><i class="icon-plus-circle"></i></span>
            {else}<span class="ets_cfu_del btn btn-primary" title="{l s='Delete' mod='ets_cfultimate'}"><i class="icon-trash-o"></i></span>{/if}
        </div>
    {/if}
</li>