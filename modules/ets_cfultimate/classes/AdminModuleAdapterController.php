<?php
/**
 * 2007-2020 ETS-Soft
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 wesite only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please contact us for extra customization service at an affordable price
 *
 * @author ETS-Soft <etssoft.jsc@gmail.com>
 * @copyright  2007-2020 ETS-Soft
 * @license    Valid for 1 website (or project) for each purchase of license
 *  International Registered Trademark & Property of ETS-Soft
 */

if (!defined('_PS_VERSION_'))
    exit;

class AdminModuleAdapterController extends ModuleAdminController
{
    public $y_max_value = 0;
    public $charts;

    public function __construct()
    {
        $this->bootstrap = true;
        parent::__construct();
    }

    public function initContent()
    {
        parent::initContent();
    }

    public function getMaxY($top)
    {
        $top = (int)$top;
        if ($top < 10)
            return ($top <= 5) ? $top + 1 : $top + 2;
        elseif ($top < 100)
            return ($top % 10 < 5) ? (floor($top / 10) + 1) * 10 : (floor($top / 10) + 2) * 10;
        elseif ($top < 1000)
            return ($top % 100 < 5) ? (floor($top / 100) + 1) * 100 : (floor($top / 100) + 2) * 100;
        else
            return ($top % 1000 < 5) ? (floor($top / 1000) + 1) * 1000 : (floor($top / 1000) + 1) * 1000;
    }

    public function getColor($num)
    {
        $hash = md5('color' . $num);
        $rgb = array(
            hexdec(Tools::substr($hash, 0, 2)), // r
            hexdec(Tools::substr($hash, 2, 2)), // g
            hexdec(Tools::substr($hash, 4, 2))); //b
        return 'rgba(' . implode(',', $rgb) . ', %s)';
    }

    public function getCharts($ajax = false)
    {
        $this->y_max_value = 0;
        $is_dashboard = Tools::getValue('controller') != 'AdminContactFormUltimateStatistics';
        $labels = $messages = $views = $replies = $users = $years = array();
        $months = Tools::dateMonths();
        $max_year = date('Y');
        $min_year = Db::getInstance()->getValue('SELECT MIN(YEAR(date_add)) FROM ' . _DB_PREFIX_ . 'ets_cfu_contact WHERE 1 ' . ((int)Tools::getValue('id_contact') ? ' AND id_contact=' . (int)Tools::getValue('id_contact') : ''));
        $distance = ($max_year - $min_year);
        if ($distance <= 0) {
            $min_year = $max_year - 1;
        }
        if ($min_year) {
            for ($i = $min_year; $i <= $max_year; $i++) {
                $years[] = $i;
            }
        }
        $sl_year = Tools::getValue('ets_cfu_years', ($distance < 5 ? date('Y') : false));
        $sl_month = Tools::getValue('ets_cfu_months');
        $args = array(
            'year' => '',
            'month' => '',
            'day' => '',
            'id_contact' => Tools::getValue('id_contact', false)
        );
        $filter = Tools::getValue('filter', ($is_dashboard ? ($distance >= 5 ? 'all' : 'year') : ''));
        if (($filter == 'all' || (!$sl_year && !$is_dashboard)) && $years) {
            foreach ($years as $year) {
                $args['year'] = $labels[] = $year;
                $messages[] = $this->getCountMessage($args);
                $views[] = $this->getCountView($args);
                $replies[] = $this->getCountReplies($args);
                $users[] = $this->getCountUsers($args);
            }
        } elseif (($filter == 'year' || (!$sl_month && !$is_dashboard)) && $months) {
            $args['year'] = ($sl_year ? $sl_year : date('Y'));
            foreach ($months as $key => $month) {
                $args['month'] = $labels[] = $key;
                $messages[] = $this->getCountMessage($args);
                $views[] = $this->getCountView($args);
                $replies[] = $this->getCountReplies($args);
                $users[] = $this->getCountUsers($args);
            }
        } elseif ($filter == 'month' || ($sl_month && $sl_year && !$is_dashboard)) {
            $args['year'] = ($year = ($sl_year ? $sl_year : (int)date('Y')));
            $args['month'] = ($month = ($sl_month ? $sl_month : (int)date('m')));
            if (($days = function_exists('cal_days_in_month') ? cal_days_in_month(CAL_GREGORIAN, (int)$month, (int)$year) : (int)date('t', mktime(0, 0, 0, (int)$month, 1, (int)$year)))) {
                for ($day = 1; $day <= $days; $day++) {
                    $args['day'] = $labels[] = $day;
                    $messages[] = $this->getCountMessage($args);
                    $views[] = $this->getCountView($args);
                    $replies[] = $this->getCountReplies($args);
                    $users[] = $this->getCountUsers($args);
                }
            }
        }
        $this->charts['mes'] +=  array(
            'data' => $messages,
            'color' => 'rgba(243, 166, 180, %s)'
        );
        $this->charts['vie'] += array(
            'data' => $views,
            'color' => 'rgba(108, 206, 216, %s)'
        );
        $this->charts['rep'] += array(
            'data' => $replies,
            'color' => 'rgba(251, 227, 185, %s)'
        );
        $this->charts['use'] += array(
            'data' => $users,
            'color' => 'rgba(111, 208, 136, %s)'
        );

        foreach ($this->charts as &$item) {
            $item['backgroundColor'] = sprintf($item['color'], 0.3);
            $item['borderColor'] = sprintf($item['color'], 1);
            $item['borderWidth'] = 1;
            $item['pointRadius'] = 2;
            $item['fill'] = true;
        }
        $assigns = array(
            'ets_cfu_years' => $years,
            'ets_cfu_months' => $months,
            'ets_cfu_line_chart' => array_values($this->charts),
            'ets_cfu_lc_labels' => $labels,
            'y_max_value' => $this->getMaxY($this->y_max_value),
            'filter_active' => $filter,
            'sl_year' => $sl_year,
            'sl_month' => $sl_month,
            'sl_contact' => (int)Tools::getValue('id_contact'),
        );
        if ($ajax) {
            die(Tools::jsonEncode($assigns));
        }
        return $assigns;
    }

    public function getCountMessage($args = array())
    {
        $sql = '
            SELECT COUNT(*) FROM ' . _DB_PREFIX_ . 'ets_cfu_contact_message m 
            LEFT JOIN ' . _DB_PREFIX_ . 'ets_cfu_contact_shop cs ON (m.id_contact = cs.id_contact)
            WHERE cs.id_contact is NOT NULL AND  cs.id_shop = ' . (int)$this->context->shop->id
            . (isset($args['id_contact']) && $args['id_contact'] ? ' AND cs.id_contact=' . (int)$args['id_contact'] : '')
            . (isset($args['year']) && $args['year'] ? ' AND YEAR(m.date_add) ="' . pSQL($args['year']) . '"' : '')
            . (isset($args['month']) && $args['month'] ? ' AND MONTH(m.date_add) ="' . pSQL($args['month']) . '"' : '')
            . (isset($args['day']) && $args['day'] ? ' AND DAY(m.date_add) ="' . pSQL($args['day']) . '"' : '')
            . (isset($args['read']) ? ' AND m.readed ="' . pSQL($args['read']) . '"' : '');
        $result = (int)Db::getInstance()->getValue($sql);
        if ($result > $this->y_max_value)
            $this->y_max_value = $result;
        return $result;
    }

    public function getCountView($args = array())
    {
        $sql = '
            SELECT COUNT(*) FROM ' . _DB_PREFIX_ . 'ets_cfu_log l 
            LEFT JOIN ' . _DB_PREFIX_ . 'ets_cfu_contact_shop cs ON (l.id_contact = cs.id_contact)
            WHERE cs.id_contact is NOT NULL  AND cs.id_shop=' . (int)$this->context->shop->id
            . (isset($args['id_contact']) && $args['id_contact'] ? ' AND cs.id_contact=' . (int)$args['id_contact'] : '')
            . (isset($args['year']) && $args['year'] ? ' AND YEAR(l.datetime_added) ="' . pSQL($args['year']) . '"' : '')
            . (isset($args['month']) && $args['month'] ? ' AND MONTH(l.datetime_added) ="' . pSQL($args['month']) . '"' : '')
            . (isset($args['day']) && $args['day'] ? ' AND DAY(l.datetime_added) ="' . pSQL($args['day']) . '"' : '');
        $result = (int)Db::getInstance()->getValue($sql);
        if ($result > $this->y_max_value)
            $this->y_max_value = $result;
        return $result;
    }

    public function getCountReplies($args = array())
    {
        $sql = 'SELECT COUNT(r.id_ets_cfu_message_reply) FROM ' . _DB_PREFIX_ . 'ets_cfu_message_reply r
            LEFT JOIN ' . _DB_PREFIX_ . 'ets_cfu_contact_message m ON (r.id_contact_message = m.id_contact_message)
            LEFT JOIN ' . _DB_PREFIX_ . 'ets_cfu_contact_shop cs ON (m.id_contact = cs.id_contact)
            WHERE m.id_contact_message is NOT NULL AND cs.id_contact is NOT NULL AND cs.id_shop=' . (int)$this->context->shop->id . ''
            . (isset($args['id_contact']) && $args['id_contact'] ? ' AND cs.id_contact=' . (int)$args['id_contact'] : '')
            . (isset($args['year']) && $args['year'] ? ' AND YEAR(r.date_add) ="' . pSQL($args['year']) . '"' : '')
            . (isset($args['month']) && $args['month'] ? ' AND MONTH(r.date_add) ="' . pSQL($args['month']) . '"' : '')
            . (isset($args['day']) && $args['day'] ? ' AND DAY(r.date_add) ="' . pSQL($args['day']) . '"' : '');
        $result = (int)Db::getInstance()->getValue($sql);
        if ($result > $this->y_max_value)
            $this->y_max_value = $result;
        return $result;
    }

    public function getCountUsers($args = array())
    {
        $sql = '
            SELECT COUNT(DISTINCT l.id_customer) FROM ' . _DB_PREFIX_ . 'ets_cfu_log l 
            LEFT JOIN ' . _DB_PREFIX_ . 'ets_cfu_contact_shop cs ON (l.id_contact = cs.id_contact)
            WHERE cs.id_contact is NOT NULL AND l.id_customer != 0 AND cs.id_shop = ' . (int)$this->context->shop->id
            . (isset($args['id_contact']) && $args['id_contact'] ? ' AND cs.id_contact=' . (int)$args['id_contact'] : '')
            . (isset($args['year']) && $args['year'] ? ' AND YEAR(l.datetime_added) ="' . pSQL($args['year']) . '"' : '')
            . (isset($args['month']) && $args['month'] ? ' AND MONTH(l.datetime_added) ="' . pSQL($args['month']) . '"' : '')
            . (isset($args['day']) && $args['day'] ? ' AND DAY(l.datetime_added) ="' . pSQL($args['day']) . '"' : '');
        $result = (int)Db::getInstance()->getValue($sql);
        if ($result > $this->y_max_value)
            $this->y_max_value = $result;
        return $result;
    }
}