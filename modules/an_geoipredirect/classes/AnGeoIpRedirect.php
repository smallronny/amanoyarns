<?php
/**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 *         DISCLAIMER   *
 * Do not edit or add to this file if you wish to upgrade Prestashop to newer
 * versions in the future.
 * ****************************************************
 *
 *  @author     Anvanto (anvantoco@gmail.com)
 *  @copyright  anvanto.com
 *  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

class AnGeoIpRedirect extends ObjectModel
{
    public $id;

    public $id_an_geoipredirect;

    public $title;

    public $type = 'ip';

    public $status = 1;

    public $cycle = 0;

    public $position = 0;

    public $invert = 0;

    public $ip_list;

    public $countries;

    public $cities;

    public $to_page;

    const API_SERVICE = 'http://ip-api.com/json/';

    public static $definition = array(
        'table' => 'an_geoipredirect',
        'primary' => 'id_an_geoipredirect',
        'multilang' => false,
        'fields' => array(
            'title' =>      array('type' => self::TYPE_STRING, 'required' => true, 'size' => 255),
            'type' =>       array('type' => self::TYPE_STRING),
            'status' =>     array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'cycle' =>      array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'position' =>   array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'invert' =>   array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'ip_list' =>    array('type' => self::TYPE_STRING),
            'countries' =>  array('type' => self::TYPE_STRING),
            'cities' =>     array('type' => self::TYPE_STRING),
            'to_page' =>    array('type' => self::TYPE_STRING, 'required' => true),
        ),
    );

    public static function findRedirect($ip)
    {
        $locationraw = Tools::file_get_contents(self::API_SERVICE . $ip);
        $location = Tools::jsonDecode($locationraw);
        $location->ip = $ip;

        $q = '
			SELECT * FROM `' . _DB_PREFIX_ . 'an_geoipredirect` ag
			LEFT JOIN `' . _DB_PREFIX_ . 'an_geoipredirect_shop` ags ON ag.id_an_geoipredirect = ags.id_an_geoipredirect
			WHERE ag.`status` = 1
			';
        if(Shop::isFeatureActive()){
            $q .= 'AND ags.`id_shop` = ' . (int)Context::getContext()->shop->id;
        }
        $q .= '
        ORDER BY `position` ASC';

        $items = Db::getInstance()->ExecuteS($q);

        $isFound = false;
        foreach ($items as $item) {
            if ($item['type'] == 'ip') {
                $ip_list = explode(chr(10), $item['ip_list']);
                $ip_list = array_map('trim', $ip_list);
                foreach ($ip_list as $ip) {
                    if (($ip == $location->ip  && !$item['invert']) || ($ip != $location->ip && $item['invert'])) {
                        $isFound = $item;
                        break(2);
                    }

                    if (substr_count($ip, '-')) {
                        $ipRange = explode('-', $ip);
                        $ip2long = ip2long($location->ip);
                        if (($ip2long >= ip2long($ipRange[0]) && $ip2long <= ip2long($ipRange[1]) && !$item['invert'])
                            || ($ip2long < ip2long($ipRange[0]) && $ip2long > ip2long($ipRange[1]) && $item['invert'])) {
                            $isFound = $item;
                            break(2);
                        }
                    }
                }
            } elseif ($location->status == 'success') {
                $countries = explode(chr(','), $item['countries']);
                if(count(explode(',', $item['countries'])) > count($countries)){
                    $countries = explode(',', $item['countries']);
                }
                $countries = array_map('strtoupper', $countries);
                $countries = array_map('trim', $countries);

                $country_name = trim(Tools::strtoupper($location->country));
                $country_code = trim(Tools::strtoupper($location->countryCode));

                if (
                    count($countries) > 0
                    && $countries[0] != ''
                    && (!(bool)$item['invert'] && (in_array($country_name, $countries) || in_array($country_code, $countries)))
                    || ((bool)$item['invert'] && (!in_array($country_name, $countries) && !in_array($country_code, $countries)))
                ) {
                    $isFound = $item;
                    break;
                }

                $cities = explode(chr(','), $item['cities']);
                if(count(explode(',', $item['cities'])) > count($cities)){
                    $cities = explode(',', $item['cities']);
                }
                $cities = array_map('strtoupper', $cities);
                $cities = array_map('trim', $cities);

                $city = trim(Tools::strtoupper($location->city));
                if (
                    count($cities) > 0
                    && $cities[0] != ''
                    && ((in_array($city, $cities) && !(bool)$item['invert'])
                        || (!in_array($city, $cities) && (bool)$item['invert']))
                ) {
                    $isFound = $item;
                    break;
                }
            }
        }

        return $isFound;
    }

    public function processSave()
    {
        if (Tools::getIsset('billing_freq')) {
            $billing_freq = (int)Tools::getValue('billing_freq');
            if ($billing_freq < 1) {
                $this->errors[] = $this->l('Please enter the [Назва поля] field');
            }
        }
        if (!Tools::getIsset('allow_weekdays')) {
            $this->errors[] = $this->l('Please select at least one weekday');
        }

        if (Tools::getIsset('require_payment_before')) {
            $require_payment_before = (int)Tools::getValue('require_payment_before');
            if ($require_payment_before < 0) {
                $this->errors[] = $this->l('Please enter Payment prior to Delivery');
            }
        }
        if (!empty($this->errors)) {
            // if we have errors, we stay on the form instead of going back to the list
            $this->display = 'edit';
            return false;
        }

        parent::processSave();
    }
}
