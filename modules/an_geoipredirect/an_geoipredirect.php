<?php
/**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 *         DISCLAIMER   *
 * Do not edit or add to this file if you wish to upgrade Prestashop to newer
 * versions in the future.
 * ****************************************************
 *
 *  @author     Anvanto (anvantoco@gmail.com)
 *  @copyright  anvanto.com
 *  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

include_once(_PS_MODULE_DIR_ . 'an_geoipredirect/classes/AnGeoIpRedirect.php');

class an_geoipredirect extends Module
{
    protected $_hooks = array(
        'displayHeader',
    );

    public function __construct()
    {
        $this->name = 'an_geoipredirect';
        $this->tab = 'front_office_features';
        $this->version = '1.0.5';
        $this->author = 'Anvanto';
        $this->need_instance = 0;
        $this->module_key = '225f80faef8820f2ef7c0d22cfe6020c';

        parent::__construct();

        $this->displayName = $this->l('Geo Ip Redirect');
        $this->description = $this->l('Geo Ip Redirect');
    }

    public function install()
    {
        $sql = array();
        $sql[] =
            'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'an_geoipredirect` (
              `id_an_geoipredirect` int(10) unsigned NOT NULL auto_increment,
              `title` varchar(255) NOT NULL,
              `type` ENUM("ip", "location"),
              `status` int(1) NOT NULL default "1",
              `cycle` int(1) NOT NULL default "0",
              `position` INT(11) NOT NULL,
              `invert` int(1) NOT NULL default "0",
              `ip_list` varchar(255) NOT NULL COMMENT "Value for IP type",
              `countries` TEXT NOT NULL COMMENT "Value for LOCATION type",
              `cities` TEXT NOT NULL COMMENT "Value for LOCATION type",
              `to_page` TEXT NOT NULL,
              PRIMARY KEY  (`id_an_geoipredirect`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8';

        $sql[] = '
        CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'an_geoipredirect_shop` (
        `id_an_geoipredirect` int(10) unsigned NOT NULL,
        `id_shop` int(10) unsigned NOT NULL,
        PRIMARY KEY (`id_an_geoipredirect`, `id_shop`)
        ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8';

        foreach ($sql as $_sql) {
            Db::getInstance()->Execute($_sql);
        }
        $install = parent::install();



        $languages = Language::getLanguages();

        $_tab = new Tab();
        $_tab->class_name = 'AdminGeoIpRedirect';
        $_tab->id_parent = Tools::version_compare('1.7', _PS_VERSION_) ? Tab::getIdFromClassName('AdminParentModulesSf') : Tab::getCurrentParentId();
        if (empty($_tab->id_parent)) {
            $_tab->id_parent = 0;
        }
        $_tab->module = $this->name;
        foreach ($languages as $language) {
            $_tab->name[$language['id_lang']] = $this->l('Geo Ip Redirect');
        }
        $_tab->add();



        foreach ($this->_hooks as $hook) {
            $this->registerHook($hook);
        }

        return $install;
    }

    public function uninstall()
    {
        $sql = array();
        $sql[] = 'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'an_geoipredirect`';
        $sql[] = 'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'an_geoipredirect_shop`';

        foreach ($sql as $_sql) {
            Db::getInstance()->Execute($_sql);
        }

        $idTab = Tab::getIdFromClassName('AdminGeoIpRedirect');
        if ($idTab) {
            $tab = new Tab($idTab);
            $tab->delete();
        }

        foreach ($this->_hooks as $hook) {
            $this->unregisterHook($hook);
        }

        return parent::uninstall();
    }

    public function hookDisplayHeader()
    {


        if (preg_match(
            '/bot|crawl|curl|dataprovider|search|get|spider|find|java|majesticsEO|google|yahoo|teoma|contaxe|yandex|libwww-perl|facebookexternalhit/i',
                $_SERVER['HTTP_USER_AGENT'])
        ) {
            $redirect = false;
        } else {
            $ip = Tools::getRemoteAddr();
            $redirect = AnGeoIpRedirect::findRedirect($ip);
        }

        if ($redirect !== false) {
            if ($redirect['cycle']) {
                Tools::redirectAdmin($redirect['to_page']);
            }

            $cookie = Context::getContext()->cookie;
            if (!$cookie->hasGeoRedirect) {
                $cookie->hasGeoRedirect = true;
                Tools::redirectAdmin($redirect['to_page']);
            }
        }
    }

    public function getContent()
    {
        $redirect = $this->context->link->getAdminLink('AdminGeoIpRedirect');
        Tools::redirectAdmin($redirect);
    }
}
