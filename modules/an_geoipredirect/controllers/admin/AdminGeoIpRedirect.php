<?php
/**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 *         DISCLAIMER   *
 * Do not edit or add to this file if you wish to upgrade Prestashop to newer
 * versions in the future.
 * ****************************************************
 *
 *  @author     Anvanto (anvantoco@gmail.com)
 *  @copyright  anvanto.com
 *  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

include_once(_PS_MODULE_DIR_ . 'an_geoipredirect/an_geoipredirect.php');

class AdminGeoIpRedirectController extends ModuleAdminController
{

    protected $position_identifier = 'id_an_geoipredirect';

    public function __construct()
    {
        $this->bootstrap = true;
        $this->table = 'an_geoipredirect';
        $this->identifier = 'id_an_geoipredirect';
        $this->className = 'AnGeoIpRedirect';
        $this->addRowAction('edit');
        $this->addRowAction('delete');

        $this->fields_list = array(
            'id_an_geoipredirect' => array(
                'title' => 'ID',
                'align' => 'center',
                'width' => 30
            ),
            'title' => array(
                'title' => 'Title',
                'width' => 300
            ),
            'type' => array(
                'title' => 'Type',
                'width' => 40
            ),
            'position' => array(
                'title' => 'Position',
                'width' => 40
            ),
            'status' => array(
                'title' => 'Status',
                'width' => 40,
                'active' => 'update',
                'align' => 'center',
                'type' => 'bool',
                'orderby' => false
            ),
        );

        if (
            Shop::isFeatureActive()
            && Shop::getContext() != Shop::CONTEXT_ALL
            && version_compare(_PS_VERSION_, '1.6.9.9', '>')
        ) {
            $this->_where .= ' AND a.' . pSQL($this->identifier) . ' IN (
                SELECT sa.' . pSQL($this->identifier) . '
                FROM `' . _DB_PREFIX_ . pSQL($this->table) . '_shop` sa
                WHERE sa.id_shop IN (' . pSQL(implode(', ', Shop::
                getContextListShopID())) . ')
            )';
        }
        parent::__construct();
    }

    public function renderForm()
    {
        $this->display = 'edit';
        $this->initToolbar();
        if (!$this->loadObject(true)) {
            return;
        }

        $this->fields_form = array(
            'tinymce' => false,
            'legend' => array(
                'title' => $this->module->l('Geo IP Redirect')
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->module->l('Redirect Title'),
                    'name' => 'title',
                    'id' => 'title',
                    'required' => true,
                    'size' => 50,
                    'maxlength' => 50,
                ),
               array(
                    'type' => 'radio',
                    'label' => $this->module->l('Redirect Type'),
                    'name' => 'type',
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(array(
                        'id' => 'type_ip',
                        'value' => 'ip',
                        'label' => $this->module->l('by IP')), array(
                        'id' => 'type_location',
                        'value' => 'location',
                        'label' => $this->module->l('by Location'))
                    )
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->module->l('Enable Redirect'),
                    'name' => 'status',
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(array(
                        'id' => 'is_enabled_on',
                        'value' => 1), array(
                        'id' => 'is_enabled_off',
                        'value' => 0)
                    )
                ),

                array(
                    'type' => 'switch',
                    'label' => $this->module->l('Cyclic redirect'),
                    'name' => 'cycle',
                    'class' => 't',
                    'is_bool' => true,
                    'desc' => array($this->module->l('
                    If you choose "Yes"
                    then customers will be redirected every time when they will try to open the site. 
                    If "No" then customers will be redirected only one time.'),

                    $this->module->l('Be careful! If the option is enabled a redirection page should be
                    different from the site to avoid infinite redirection loop.')),
                    'values' => array(array(
                        'id' => 'cycle_on',
                        'value' => 1), array(
                        'id' => 'cycle_off',
                        'value' => 0)
                    )
                ),
                array(
                    'type' => 'text',
                    'label' => $this->module->l('Redirect Order'),
                    'name' => 'position',
                    'id' => 'position',
                    'size' => 50,
                    'maxlength' => 10,
                    'required' => true,
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->module->l('Invert GeoIP condition'),
                    'name' => 'invert',
                    'class' => 't',
                    'is_bool' => true,
                    'desc' => $this->module->l('If the option is enabled,
                     then all users will be redirected except for users from IP or location list.'),
                    'values' => array(array(
                        'id' => 'invert_on',
                        'value' => 1), array(
                        'id' => 'invert_off',
                        'value' => 0)
                    )
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->module->l('IP List'),
                    'name' => 'ip_list',
                    'id' => 'ip_list',
                    'desc' => $this->module->l('Single IPs and IP ranges are availbable. For example:"192.87.20.10" - single IP, "192.164.0.1-192.164.99.99" - IP range.'),
                    'required' => true,
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->module->l('From Countries'),
                    'name' => 'countries',
                    'id' => 'countries',
                    'desc' => $this->module->l('Country name and Country code are available. Comma is a delimeter.'),
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->module->l('From Cities'),
                    'name' => 'cities',
                    'id' => 'cities',
                    'desc' => $this->module->l('Comma is a delimeter'),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->module->l('To Page'),
                    'name' => 'to_page',
                    'id' => 'to_page',
                    'required' => true,
                    'desc' => $this->module->l('Customers will be redirected to this URL'),
                ),
            ),
            'submit' => array(
                'title' => $this->module->l('   Save   '),
            )
        );

        $display = $this->context->smarty->fetch(str_replace('/AdminGeoIpRedirect.php', '' ,__FILE__) . '/../../views/templates/admin/js.tpl');
        if (Shop::isFeatureActive()) {
            $this->fields_form['input'][] = array(
                'required' => true,
                'type' => 'shop',
                'label' => $this->l('Shop association'),
                'name' => 'checkBoxShopAsso',
            );
        }
        return parent::renderForm() . $display;
    }

    public function postProcess()
    {
        $this->module->enable(true);
        if (Tools::getValue('type') == 'ip' && (!Tools::getIsset('ip_list') || Tools::getValue('ip_list') == '')) {
            $this->errors[] = $this->module->l('Please enter the IP List field.');
        }
        if (Tools::getValue('type') == 'location'
            && ((!Tools::getIsset('countries')
            && !Tools::getIsset('cities'))
            || (Tools::getValue('countries') == ''
            && Tools::getValue('cities') == ''))
        ) {
            $this->errors[] = $this->module->l('Please enter at least one value of fields From Countries or From Cities.');
        }
        if (Tools::getIsset('title') && Tools::getValue('title') == '') {
            $this->errors[] = $this->module->l('Please enter the Redirect Title field.');
        }
        if (Tools::getIsset('to_page') && Tools::getValue('to_page') == '') {
            $this->errors[] = $this->module->l('Please enter the To Page field.');
        }
        if (Tools::getIsset('position') && Tools::getValue('position') == '') {
            $this->errors[] = $this->module->l('Please enter the Redirect Order field.');
        }

        if (Tools::getIsset('title') && !Validate::isString(Tools::getValue('title'))) {
            $this->errors[] = $this->module->l('Redirect Title field value is required to be string.');
        }
        if (Tools::getIsset('countries') && !Validate::isString(Tools::getValue('countries'))) {
            $this->errors[] = $this->module->l('From Countries field value is required to be string.');
        }
        if (Tools::getIsset('cities') && !Validate::isString(Tools::getValue('cities'))) {
            $this->errors[] = $this->module->l('From Cities field value is required to be string.');
        }
        if (Tools::getIsset('ip_list') && !Validate::isString(Tools::getValue('ip_list'))) {
            $this->errors[] = $this->module->l('IP List field value is required to be string.');
        }
        if (Tools::getIsset('position') && Tools::getValue('position') != '' && !Validate::isInt(Tools::getValue('position'))) {
            $this->errors[] = $this->module->l('Redirect Order field value is required to be numeric.');
        }
        if (Tools::getIsset('to_page') && Tools::getValue('to_page') != '' && !Validate::isUrl(Tools::getValue('to_page'))) {
            $this->errors[] = $this->module->l('To Page field value is required to be URL.');
        }
        if (
            Shop::isFeatureActive()
            && Tools::getIsset('submitAddan_geoipredirect') &&
            !Tools::getIsset('checkBoxShopAsso_an_geoipredirect')
        ) {
            $this->errors[] = $this->module->l('Please, select at least one shop you want to associate the redirect rule with');
        }

        if (count($this->errors)>0) {
            $this->display = 'edit';
            return false;
        }

        return parent::postProcess();
    }

    /**
     * @param int $id_object
     * @return bool|void
     * @throws PrestaShopDatabaseException
     */
    protected function updateAssoShop($id_object)
    {
        if (!Shop::isFeatureActive()) {
            return;
        }

        $assos_data = $this->getSelectedAssoShop($this->table, $id_object);

        $exclude_ids = $assos_data;
        foreach (Db::getInstance()->executeS('SELECT id_shop FROM ' . _DB_PREFIX_ .
            'shop') as $row) {
            if (!$this->context->employee->hasAuthOnShop($row['id_shop'])) {
                $exclude_ids[] = $row['id_shop'];
            }
        }

        Db::getInstance()->delete($this->table . '_shop', '`' . $this->identifier .
            '` = ' . (int)$id_object . ($exclude_ids ? ' AND id_shop NOT IN (' . implode(
                    ', ',
                    $exclude_ids
                ) . ')' : ''));

        $insert = array();
        foreach ($assos_data as $id_shop) {
            $insert[] = array(
                $this->identifier => $id_object,
                'id_shop' => (int)$id_shop,
            );
        }

        return Db::getInstance()->insert(
            $this->table . '_shop',
            $insert,
            false,
            true,
            Db::INSERT_IGNORE
        );
    }

    /**
     * @param string $table
     * @return array
     */
    protected function getSelectedAssoShop($table)
    {
        if (!Shop::isFeatureActive()) {
            return array();
        }

        $shops = Shop::getShops(true, null, true);
        if (count($shops) == 1 && isset($shops[0])) {
            return array($shops[0], 'shop');
        }

        $assos = array();
        if (Tools::isSubmit('checkBoxShopAsso_' . $table)) {
            foreach (Tools::getValue('checkBoxShopAsso_' . $table) as $id_shop => $value) {
                $assos[] = (int)$id_shop;
            }
        } else {
            if (Shop::getTotalShops(false) == 1) {
                // if we do not have the checkBox multishop, we can have an admin with only one shop and being in multishop
                $assos[] = (int)Shop::getContextShopID();
            }
        }

        return $assos;
    }
}
