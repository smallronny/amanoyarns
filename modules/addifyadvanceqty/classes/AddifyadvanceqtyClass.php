<?php
/**
 * 2020 Addify
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    Addify
 *  @copyright 2020 Addify
 *  @license   http://opensource.org/licenses/afl-3.0.php
 */

class AddifyadvanceqtyClass extends ObjectModel
{
    public $id;

    public $active;

    public $title;

    public $categories;

    public $groups;

    public $product_type;

    public $quantity_type;

    public $min_qty;

    public $max_qty;

    public $interval;

    public $fix_qty;

    public $product;

    public $id_shop;

    public $date_upd;

    public $date_add;

    public $priority;


    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'     => 'addifyadvanceqty',
        'primary'   => 'id_addifyadvanceqty',
        'multilang' => false,
        'fields'    => array(
            'active'  => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'priority'  => array('type' => self::TYPE_BOOL),
            'date_upd' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
            'date_add'   => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
            'categories' => array('type' => self::TYPE_HTML),
            'groups' => array('type' => self::TYPE_HTML),
            'title' => array('type' => self::TYPE_HTML),
            'product_type'  => array('type' => self::TYPE_BOOL),
            'quantity_type'  => array('type' => self::TYPE_BOOL),
            'min_qty'  => array('type' => self::TYPE_BOOL),
            'max_qty'  => array('type' => self::TYPE_BOOL),
            'interval'  => array('type' => self::TYPE_BOOL),
            'fix_qty'  => array('type' => self::TYPE_BOOL),
            'product' => array('type' => self::TYPE_HTML),
            'id_shop' => array('type' => self::TYPE_HTML),
        )
    );
    public static function getListContent()
    {
        return Db::getInstance()->executeS('
            SELECT cr.*
            FROM `'._DB_PREFIX_.'addifyadvanceqty` cr
            ');
    }
    public static function searchHidePriceProduct()
    {
        $query = Tools::getValue('q', false);
        if (!$query or $query == '' or Tools::strlen($query) < 1) {
            die();
        }

        /*
         * In the SQL request the "q" param is used entirely to match result in database.
         * In this way if string:"(ref : #ref_pattern#)" is displayed on the return list,
         * they are no return values just because string:"(ref : #ref_pattern#)"
         * is not write in the name field of the product.
         * So the ref pattern will be cut for the search request.
         */
        if ($pos = strpos($query, ' (ref:')) {
            $query = Tools::substr($query, 0, $pos);
        }

        $excludeIds = Tools::getValue('excludeIds', false);
        if ($excludeIds && $excludeIds != 'NaN') {
            $excludeIds = implode(',', array_map('intval', explode(',', $excludeIds)));
        } else {
            $excludeIds = '';
            $excludePackItself= Tools::getValue('packItself', false);
        }

        // Excluding downloadable products from packs because download from pack is not supported
        $excludeVirtuals = (bool)Tools::getValue('excludeVirtuals', true);
        $exclude_packs = (bool)Tools::getValue('exclude_packs', true);

        $context = Context::getContext();

        $sql = 'SELECT p.`id_product`, pl.`link_rewrite`, p.`reference`, pl.`name`, image_shop.`id_image` id_image, il.`legend`, p.`cache_default_attribute`
                FROM `'._DB_PREFIX_.'product` p
                '.Shop::addSqlAssociation('product', 'p').'
                LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (pl.id_product = p.id_product AND pl.id_lang = '.(int)$context->language->id.Shop::addSqlRestrictionOnLang('pl').')
                LEFT JOIN `'._DB_PREFIX_.'image_shop` image_shop
                    ON (image_shop.`id_product` = p.`id_product` AND image_shop.cover=1 AND image_shop.id_shop='.(int)$context->shop->id.')
                LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (image_shop.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)$context->language->id.')
                WHERE (pl.name LIKE \'%'.pSQL($query).'%\' OR p.reference LIKE \'%'.pSQL($query).'%\')'.
            (!empty($excludeIds) ? ' AND p.id_product NOT IN ('.$excludeIds.') ' : ' ').
            (!empty($excludePackItself) ? ' AND p.id_product <> '.$excludePackItself.' ' : ' ').
            ($excludeVirtuals ? 'AND NOT EXISTS (SELECT 1 FROM `'._DB_PREFIX_.'product_download` pd WHERE (pd.id_product = p.id_product))' : '').
            ($exclude_packs ? 'AND (p.cache_is_pack IS NULL OR p.cache_is_pack = 0)' : '').
            ' GROUP BY p.id_product';

        $items = Db::getInstance()->executeS($sql);

        if ($items) {
            foreach ($items as $item) {
                $item['name'] = str_replace('|', '&#124;', $item['name']);
                echo trim($item['name']).(!empty($item['reference']) ? ' (ref: '.$item['reference'].')' : '').'|'.(int)($item['id_product'])."\n";
            }
        }
        die();
    }
    public static function setStatus($status, $id_addifyextendedhideprice)
    {
        if (!$id_addifyextendedhideprice || empty($status)) {
            return false;
        }
        return (bool)Db::getInstance()->execute('UPDATE '._DB_PREFIX_.self::$definition['table'].'
            SET `'.pSQL($status).'` = !'.pSQL($status).'
            WHERE id_addifyadvanceqty = '.(int)$id_addifyextendedhideprice);
    }
    public static function getProductById($id_product)
    {
        return Db::getInstance()->getRow('SELECT `id_product`, `name`
            FROM `'._DB_PREFIX_.'product_lang` WHERE `id_product` = '.(int)$id_product);
    }
    
    public static function getRuleByProduct($productId, $categories, $groups)
    {
        $productCategories = array();
        foreach ($categories as $key => $value) {
            $productCategories[]=$value['id_category'];
        }

         $rules = Db::getInstance()->executeS('SELECT *
            FROM `'._DB_PREFIX_.'addifyadvanceqty` WHERE `active` = 1 AND  `product_type` = 1 ORDER BY priority ASC');

        if ($rules) {
            foreach ($rules as $rule) {
                if (!$rule['groups'] && !$rule['categories']) {
                    return $rule;
                } else if ($rule['groups'] && !$rule['categories']) {
                    $ruleGroups =  explode(',', $rule['groups']);
                    foreach ($groups as $key => $value) {
                        if (in_array($value, $ruleGroups)) {
                            return $rule;
                        }
                    }
                } else if ($rule['groups'] && $rule['categories']) {
                    $ruleGroups =  explode(',', $rule['groups']);
                    $groupCheck = false;
                    foreach ($groups as $key => $value) {
                        if (in_array($value, $ruleGroups)) {
                            $groupCheck = true;
                        }
                    }
                    $ruleCategories =  explode(',', $rule['categories']);

                    foreach ($productCategories as $key => $value) {
                        if (in_array($value, $ruleCategories)) {
                            if ($groupCheck) {
                                return $rule;
                            }
                        }
                    }
                }
            }
        }
        $rules = Db::getInstance()->executeS('SELECT *
            FROM `'._DB_PREFIX_.'addifyadvanceqty` WHERE `active` = 1 AND  `product_type` = 2 AND FIND_IN_SET('.$productId.',product) ORDER BY priority ASC');

        if ($rules) {
            foreach ($rules as $rule) {
                if (!$rule['groups'] && !$rule['categories']) {
                    return $rule;
                } else if ($rule['groups'] && !$rule['categories']) {
                    $ruleGroups =  explode(',', $rule['groups']);
                    foreach ($groups as $key => $value) {
                        if (in_array($value, $ruleGroups)) {
                            return $rule;
                        }
                    }
                } else if ($rule['groups'] && $rule['categories']) {
                    $ruleGroups =  explode(',', $rule['groups']);
                    $groupCheck = false;
                    foreach ($groups as $key => $value) {
                        if (in_array($value, $ruleGroups)) {
                            $groupCheck = true;
                        }
                    }
                    $ruleCategories =  explode(',', $rule['categories']);

                    foreach ($productCategories as $key => $value) {
                        if (in_array($value, $ruleCategories)) {
                            if ($groupCheck) {
                                return $rule;
                            }
                        }
                    }
                }
            }
        }
    }
}
