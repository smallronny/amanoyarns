<?php
/**
 * 2020 Addify
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    Addify
 *  @copyright 2020 Addify
 *  @license   http://opensource.org/licenses/afl-3.0.php
 */

if (!defined('_PS_VERSION_')) {
    exit;
}
include_once(_PS_MODULE_DIR_).'addifyadvanceqty/classes/AddifyadvanceqtyClass.php';
class Addifyadvanceqty extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'addifyadvanceqty';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'Addify';
        $this->need_instance = 0;
        $this->module_key = 'fa29b5a37bec4fce17434062a9d9d6c5';

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Addify Advance Quantity');
        $this->description = $this->l('Advance Quantity');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        include(dirname(__FILE__).'/sql/install.php');


        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('displayWrapperTop') && $this->registerHook('displayProductPriceBlock') && $this->registerHook('actionCartUpdateQuantityBefore') && $this->registerHook("actionBeforeCartUpdateQty") && $this->installTab();
    }

    public function uninstall()
    {
        //include(dirname(__FILE__).'/sql/uninstall.php');
        return parent::uninstall()   &&  $this->unregisterHook('header') &&  $this->unregisterHook('displayWrapperTop') && $this->unregisterHook('displayProductPriceBlock') && $this->unregisterHook('actionCartUpdateQuantityBefore') && $this->unregisterHook("actionBeforeCartUpdateQty") && $this->uninstallTab();
    }
    public function installTab()
    {
        $tab = new Tab();
        $tab->active = 1;
        if (true === Tools::version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
            $tab->id_parent = 0;
        } else {
            $tab->id_parent = 0;
        }
        $tab->class_name = 'AdminParentAddifyadvanceqty';
        $tab->name = array();
        foreach (Language::getLanguages(true) as $lang) {
            $tab->name[$lang['id_lang']] = $this->l('Addify Advance Quantity');
        }
        $tab->add();

        $tab = new Tab();
        $tab->active = 1;
        $tab->id_parent = (int)Tab::getIdFromClassName('AdminParentAddifyadvanceqty');
        ;
        $tab->class_name = 'AdminAddifyadvanceqty';
        $tab->name = array();
        foreach (Language::getLanguages(true) as $lang) {
            $tab->name[$lang['id_lang']] = $this->l('Manage Advance Quantity');
        }
        $tab->module = $this->name;
        return $tab->add();
    }

    public function uninstallTab()
    {
        $id_tab = (int)Tab::getIdFromClassName('AdminAddifyadvanceqty');
        if ($id_tab) {
            $tab = new Tab($id_tab);
            $tab->delete();
        }
        $id_tab = (int)Tab::getIdFromClassName('AdminParentAddifyadvanceqty');
        if ($id_tab) {
            $tab = new Tab($id_tab);
            return $tab->delete();
        }
        return false;
    }

    public function getContent()
    {
        if (!$this->active) {
            $this->context->controller->errors[] = $this->l('Access Denied...Module is disabled.');
        } else {
            /**
             * If values have been submitted in the form, process.
             */
            $this->postProcess();

            if (Tools::isSubmit('add'.$this->name) || Tools::isSubmit('update'.$this->name)) {
                return $this->renderForm();
            }
            return $this->renderList();
        }
    }
    protected function postProcess()
    {
        if (Tools::isSubmit('submitAddifyadvanceqty')) {
            $this->processSave();
        } elseif (Tools::isSubmit('status'.$this->name)) {
            if (!AddifyadvanceqtyClass::setStatus('active', (int)Tools::getValue('id_addifyadvanceqty'))) {
                $this->context->controller->errors[] = $this->l('Status update failed.');
            } else {
                $this->context->controller->confirmations[] = $this->l('Status updated successfully.');
            }
        } elseif (Tools::isSubmit('delete'.$this->name)) {
            if (!Validate::isLoadedObject($hideprice = new AddifyadvanceqtyClass((int)Tools::getValue('id_addifyadvanceqty')))) {
                $this->context->controller->errors[] = $this->l('Object not found.');
            } else {
                if (!$hideprice->delete()) {
                    $this->context->controller->errors[] = $this->l('Rule cannot be deleted.');
                } else {
                    $this->context->controller->confirmations[] = $this->l('Rule deleted successfully.');
                }
            }
        } elseif (Tools::isSubmit('submitBulkdelete'.$this->name)) {
            $addifyextendedhidepriceBox = Tools::getValue('addifyadvanceqtyBox');
            if (isset($addifyextendedhidepriceBox) && $addifyextendedhidepriceBox) {
                $deleted = 0;
                foreach ($addifyextendedhidepriceBox as $id_addifyextendedhideprice) {
                    if (!Validate::isLoadedObject($hideprice = new AddifyadvanceqtyClass((int)$id_addifyextendedhideprice))) {
                        $this->context->controller->errors[] = $this->l('Object not found.');
                        break;
                    } else {
                        if ($hideprice->delete()) {
                            $deleted++;
                        }
                    }
                }
                if (count($this->context->controller->errors)) {
                    $this->context->controller->errors;
                } else {
                    $this->context->controller->confirmations[] = sprintf($this->l('%s records deleted successfully.'), $deleted);
                }
            }
        }
    }

    protected function renderList()
    {
        $fields_list = array(
            'id_addifyadvanceqty' => array(
                'title' => $this->l('ID'),
                'width' => 120,
                'type' => 'text',
                'search' => false,
                'orderby' => false
            ),
            'title' => array(
                'title' => $this->l('Title'),
                'width' => 140,
                'type' => 'text',
                'search' => false,
                'orderby' => false
            ),
            'priority' => array(
                'title' => $this->l('Priority'),
                'width' => 140,
                'type' => 'text',
                'search' => false,
                'orderby' => false
            ),
            'active' => array(
                'title' => $this->l('Status'),
                'width' => 140,
                'active' => 'status',
                'type' => 'bool',
                'search' => false,
                'orderby' => false
            ),
            'date_add' => array(
                'title' => $this->l('Added Date'),
                'width' => 140,
                'type' => 'datetime',
                'search' => false,
                'orderby' => false
            ),
            'date_upd' => array(
                'title' => $this->l('Update Date'),
                'width' => 140,
                'type' => 'datetime',
                'search' => false,
                'orderby' => false
            ),
        );

        // multishop feature
        if (Shop::isFeatureActive()) {
            $this->fields_list['id_shop'] = array(
                'title' => $this->l('ID Shop'),
                'align' => 'center',
                'width' => 25,
                'type' => 'int'
            );
        }

        $helper = new HelperList();
        $helper->shopLinkType = '';
        $helper->simple_header = false;
        $helper->identifier = 'id_addifyadvanceqty';
        $helper->actions = array('edit', 'delete');
        $helper->bulk_actions = array(
            'delete' => array(
                'text' => $this->l('Delete selected'),
                'icon' => 'icon-trash',
                'confirm' => $this->l('Delete selected items?')
            )
        );
        $helper->listTotal = count(AddifyadvanceqtyClass::getListContent());
        $helper->show_toolbar = true;
        $helper->toolbar_btn['new'] =  array(
            'href' => AdminController::$currentIndex.'&configure='.$this->name.'&add'.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
            'desc' => $this->l('Add new')
        );

        $helper->title = $this->displayName;
        $helper->table = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        return $helper->generateList(AddifyadvanceqtyClass::getListContent(), $fields_list);
    }


    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->show_cancel_button = false;
        $helper->table = 'addifyadvanceqtyClass';
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = 'id_'.$this->table;
        $helper->submit_action = 'submitAddifyadvanceqty';
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->back_url = $helper->currentIndex.'&token='.$helper->token;
        $products = array();

        if (Tools::getValue('id_addifyadvanceqty')) {
            $hidepriceBlock = new AddifyadvanceqtyClass((int)Tools::getValue('id_addifyadvanceqty'));
            $post_accessories_tab = explode(',', $hidepriceBlock->product);
            foreach ($post_accessories_tab as $id_product) {
                if (!$this->haveThisProduct($id_product, $products) &&
                    $product = AddifyadvanceqtyClass::getProductById($id_product)) {
                    $products[] = $product;
                }
            }
        } else {
            $products = array();
        }
        if ($post_accessories = Tools::getValue('inputRestrictedProducts')) {
            $post_accessories_tab = explode('-', $post_accessories);
            foreach ($post_accessories_tab as $id_product) {
                if (!$this->haveThisProduct($id_product, $products) &&
                    $product = AddifyadvanceqtyClass::getProductById($id_product)) {
                    $products[] = $product;
                }
            }
        }

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
            'products' => $products,
            'context_link' => Context::getContext()->link,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }
    public function haveThisProduct($id_product, $products)
    {
        foreach ($products as $product) {
            if ((int)$product['id_product'] == (int)$id_product) {
                return true;
            }
        }
        return false;
    }
    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        $access_type = array(
            array('id' => '1', 'name' => $this->l('All Product')),
            array('id' => '2', 'name' => $this->l('Selected Product')),
        );
        $qty_type = array(
            array('id' => '1', 'name' => $this->l('Fixed')),
            array('id' => '2', 'name' => $this->l('Dropdown')),
            array('id' => '3', 'name' => $this->l('Default')),

        );

        if (Tools::getValue('id_addifyadvanceqty')) {
            $hidepriceBlock = new AddifyadvanceqtyClass((int)Tools::getValue('id_addifyadvanceqty'));
            $selected_categories = explode(',', $hidepriceBlock->categories);
        } else {
            $selected_categories = array();
        }


        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'hidden',
                        'name' => 'id_addifyadvanceqty'
                    ),array(
                        'type'          => 'text',
                        'label'         => $this->l('Title'),
                        'name'          => 'title',
                    ),array(
                        'type'          => 'text',
                        'label'         => $this->l('Priority'),
                        'name'          => 'priority',
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Active'),
                        'name' => 'active',
                        'is_bool' => true,
                        'desc' => $this->l('Enable/Disable this rule.'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    

                    array(
                        'type' => 'group',
                        'label' => $this->l('Group Disclaimer'),
                        'name' => 'groupBox',
                        'values' => Group::getGroups(Context::getContext()->language->id),
                        'info_introduction' => $this->l('You now have three default customer groups.'),
                        'desc' => $this->l('Mark any/all of the customer group(s) which you would like to show disclaimer.'),
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Quantity type'),
                        'name' => 'qty_type',
                        'options' => array(
                            'query' => $qty_type,
                            'id' => 'id',
                            'name' => 'name'
                        ),
                        'col' => 2,
                    ),
                    array(
                        'type'          => 'text',
                        'label'         => $this->l('Minimum Quantity'),
                        'name'          => 'min_qty',
                    ),
                    array(
                        'type'          => 'text',
                        'label'         => $this->l('Maximum Quantity'),
                        'name'          => 'max_qty',
                    ),
                    array(
                        'type'          => 'text',
                        'label'         => $this->l('Interval'),
                        'name'          => 'interval',
                    ),
                    array(
                        'type'          => 'text',
                        'label'         => $this->l('Fixed Quantity'),
                        'name'          => 'fix_qty',
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Product type'),
                        'name' => 'type',
                        'options' => array(
                            'query' => $access_type,
                            'id' => 'id',
                            'name' => 'name'
                        ),
                        'col' => 2,
                    ),
                    array(
                        'type' => 'addifyadvanceqty_product',
                        'label' => $this->l('Search Product'),
                        'name' => 'product',
                        'col' => 6,
                        'prefix' => '<i class="icon-search"></i>',
                        'hint' => $this->l('Product(s) Advance Quantity will be shown for selected product.')
                    ),
                    array(
                        'type'  => 'categories',
                        'label' => $this->l('Categories'),
                        'name'  => 'category',
                        'col' => 6,
                        'tree'  => array(
                            'id' => 'type_category',
                            'use_checkbox' => true,
                            'disabled_categories' => null,
                            'selected_categories' => $selected_categories,
                            'root_category' => Context::getContext()->shop->getCategory()
                        )
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
                'buttons' => array(
                    'cancel' => array(
                        'title' => $this->l('Cancel'),
                        'class' => 'btn btn-default',
                        'icon' => 'process-icon-cancel',
                        'href' => AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
                    ),
                ),
            ),
        );

        if (Shop::isFeatureActive()) {
            $this->fields_form['input'][] = array(
                'type' => 'shop',
                'label' => $this->l('Shop association'),
                'name' => 'checkBoxShopAsso',
            );
        }
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        $fields_value = array();
        $accessed_groups_ids = array();
        $fields_value = array(
            'message' => Tools::getValue('message'),
        );
        $accessed_groups_ids = array();


        if (Tools::getValue('id_addifyadvanceqty') && (Tools::isSubmit('submitAddifyadvanceqty') === false)) {
            $addifyadvanceqty = new AddifyadvanceqtyClass((int)Tools::getValue('id_addifyadvanceqty'));
            $fields_value['id_addifyadvanceqty'] = $addifyadvanceqty->id;
            $fields_value['title'] = $addifyadvanceqty->title;
            $fields_value['min_qty'] = $addifyadvanceqty->min_qty;
            $fields_value['max_qty'] = $addifyadvanceqty->max_qty;
            $fields_value['fix_qty'] = $addifyadvanceqty->fix_qty;
            $fields_value['interval'] = $addifyadvanceqty->interval;
            $fields_value['qty_type'] = $addifyadvanceqty->quantity_type;
            $fields_value['active'] = $addifyadvanceqty->active;
            $fields_value['groupBox'] = explode(',', $addifyadvanceqty->groups);
            $fields_value['type'] = $addifyadvanceqty->product_type;
            $fields_value['product'] = explode(',', $addifyadvanceqty->product);
            $fields_value['priority'] = $addifyadvanceqty->priority;
            $fields_value['category'] = explode(',', $addifyadvanceqty->categories);
            $accessed_groups_ids= $fields_value['groupBox'];
        } else {
            $fields_value['id_addifyadvanceqty'] = Tools::getValue('id_addifyadvanceqty');
            $fields_value['active'] = (int)Tools::getValue('active');
            $fields_value['min_qty'] = Tools::getValue('min_qty');
            $fields_value['max_qty'] = Tools::getValue('max_qty');
            $fields_value['fix_qty'] = Tools::getValue('fix_qty');
            $fields_value['interval'] = Tools::getValue('interval');
            $fields_value['qty_type'] = (int)Tools::getValue('qty_type');
            $fields_value['priority'] = Tools::getValue('priority');
            $fields_value['title'] = Tools::getValue('title');
            if (!Tools::getValue('groupBox')) {
                $fields_value['groupBox'] =  Tools::getValue('groupBox');
            } else {
                $fields_value['groupBox'] = implode(',', Tools::getValue('groupBox'));
            }
            $fields_value['type'] = Tools::getValue('type');
            if ($fields_value['type']==1) {
                $fields_value['product'] = '';
            } else {
                if (!Tools::getValue('product')) {
                    $fields_value['product'] = Tools::getValue('product');
                } else {
                    $fields_value['product'] = implode(',', Tools::getValue('product'));
                }
            }
            if (Tools::getValue('category')) {
                $fields_value['category'] = implode(',', Tools::getValue('category'));
            } else {
                $fields_value['category'] ='' ;
            }
            $accessed_groups_ids=  Tools::getValue('groupBox');
        }

        if (!is_array($accessed_groups_ids)) {
            $accessed_groups_ids = array();
        }
        // Added values of object Group
        $groups = Group::getGroups($this->context->language->id);
        if (empty($accessed_groups_ids)) {
            $preselected = array(Configuration::get('PS_UNIDENTIFIED_GROUP'), Configuration::get('PS_GUEST_GROUP'), Configuration::get('PS_CUSTOMER_GROUP'));
            $accessed_groups_ids = array_merge($accessed_groups_ids, $preselected);
        }
        foreach ($groups as $group) {
            $fields_value['groupBox_'.$group['id_group']] = (isset($accessed_groups_ids) && $accessed_groups_ids)? Tools::getValue('groupBox_'.$group['id_group'], (in_array($group['id_group'], $accessed_groups_ids))) : true;
        }

        return $fields_value;
    }
    /**
     * Save form data.
     */
    protected function processSave()
    {

        $call_back = 'add';
        $form_values = $this->getConfigFormValues();

        if (!isset($form_values) && !$form_values) {
            return $this->context->controller->errors[] = $this->l('Empty post values.');
        }
        if (!is_numeric($form_values['priority'])) {
            $this->context->controller->errors[] = $this->l('Priority value is not valid .');
        }
        if ($form_values['qty_type']==2) {
            if (!is_numeric($form_values['min_qty'])) {
                $this->context->controller->errors[] = $this->l('Minimum Quantity value is not valid .');
            }
            if (!is_numeric($form_values['max_qty'])) {
                $this->context->controller->errors[] = $this->l('Maximum Quantity value is not valid .');
            }
            if (!is_numeric($form_values['interval'])) {
                $this->context->controller->errors[] = $this->l('Interval value is not valid .');
            }
            if ($form_values['max_qty'] < $form_values['min_qty']) {
                $this->context->controller->errors[] = $this->l('Maximum Quantity value must be greater than Minimum Quantity .');
            }
        }
        if ($form_values['qty_type']==1) {
            if (!is_numeric($form_values['fix_qty'])) {
                $this->context->controller->errors[] = $this->l('Fixed Quantity value is not valid .');
            }
        }


        if (count($this->context->controller->errors)) {
            return $this->context->controller->errors;
        }

        if (($id_addifyadvanceqty = Tools::getValue('id_addifyadvanceqty'))) {
            $call_back = 'update';
            $addifyadvanceqty = new AddifyadvanceqtyClass((int)$id_addifyadvanceqty);
        } else {
            $addifyadvanceqty = new AddifyadvanceqtyClass();
        }
        $addifyadvanceqty->title = $form_values['title'];
        $addifyadvanceqty->active = $form_values['active'];
        $addifyadvanceqty->min_qty = $form_values['min_qty'];
        $addifyadvanceqty->max_qty = $form_values['max_qty'];
        $addifyadvanceqty->interval = $form_values['interval'];
        $addifyadvanceqty->fix_qty = $form_values['fix_qty'];
        $addifyadvanceqty->product_type = $form_values['type'];
        $addifyadvanceqty->quantity_type = $form_values['qty_type'];
        $addifyadvanceqty->groups = $form_values['groupBox'];
        $addifyadvanceqty->product = $form_values['product'];
        $addifyadvanceqty->categories = $form_values['category'];
        $addifyadvanceqty->displaytext = $form_values['message'];
        $addifyadvanceqty->priority = $form_values['priority'];

        if (!call_user_func(array($addifyadvanceqty, $call_back))) {
            $this->context->controller->errors = sprintf($this->l('Something went wrong while performing operation %s'), $call_back);
        } else {
            // clear cache after add/update
            $this->flushCache();
            $this->context->controller->confirmations[] = sprintf($this->l('Rule %s successfully.'), $call_back);
        }
    }
    public function flushCache()
    {
        Tools::clearSmartyCache();
        Tools::clearXMLCache();
        Media::clearCache();
        Tools::generateIndex();
        if (true === Tools::version_compare(_PS_VERSION_, '1.7.0.0', '>=') &&
            is_callable(array('Tools', 'clearAllCache'))) {
            Tools::clearAllCache();
        }
    }
    public function hookDisplayProductPriceBlock($param)
    {

        if (!$this->active) {
            return;
        }
        $type = Dispatcher::getInstance()->getController();
        if ($type != 'product') {
            return;
        }

        $product = $param['product'];

        if (is_object($product)) {
            $catas = Product::getProductCategoriesFull($product->id);
            $productId = $product->id;
            $productAttrib= $product->id_product_attribute;
        } else {
            $catas = Product::getProductCategoriesFull($product['id_product']);
            $productId = $product['id_product'];
            $productAttrib = $product['id_product_attribute'];
        }

        $context = $this->context;

        if ($context->cookie->addifycuradvpr == $productId) {
            return;
        }

        $context->cookie->__set('addifycuradvpr', $productId);
        $customerGroups = array(Group::getCurrent()->id);


        $ruleData = AddifyadvanceqtyClass::getRuleByProduct($productId, $catas, $customerGroups);
         
        if ($ruleData) {
            $text = '';
            $changeqtybox = 'yes';
            $qtyarray = [];
            $qtyfix = '';
            if ($ruleData['quantity_type']==2) {
                foreach (range($ruleData['min_qty'], $ruleData['max_qty'], $ruleData['interval']) as $number) {
                    $qtyarray[] =$number;
                }
            } else {
                $qtyfix = $ruleData['fix_qty'];
            }
            $interval =$ruleData['interval'];
            if ($ruleData['interval']=='') {
                $inteval = 1;
            }
            $this->context->smarty->assign(array(
                'ps_version' => _PS_VERSION_,
                'changeqtybox' => $changeqtybox,
                'quantity_type' => $ruleData['quantity_type'],
                'dropdowns' => $qtyarray,
                'fixqty' => $qtyfix,
                'min' => $ruleData['min_qty'],
                'max' => $ruleData['max_qty'],
                'interval' => $ruleData['interval'],
                'product_id'=> $productId,
            ));
            return $this->display(__FILE__, 'views/templates/hook/advanceqty.tpl');
        } else {
            $this->context->smarty->assign(array(
                'ps_version' => _PS_VERSION_,
                'changeqtybox' => 'no',
                'quantity_type' => '',
                'dropdowns' => '',
                'fixqty' => '',
                'min' => '',
                'max' => '',
                'interval' => '',
                'product_id'=> $product->id,
            ));
            return $this->display(__FILE__, 'views/templates/hook/advanceqty.tpl');
        }
    }
    public function hookHeader()
    {
        if (!$this->active) {
            return;
        }
        $this->context->cookie->__unset('addifycuradvpr');
        $type = Dispatcher::getInstance()->getController();
        $this->context->controller->addJS($this->_path . 'views/js/addifyadvanceqty17.js');
        $this->context->controller->addCSS($this->_path . 'views/css/addifyadvanceqty17.css');
    }
    public function hookDisplayWrapperTop()
    {
        if (!$this->active) {
            return;
        }

        
     
        $type = Dispatcher::getInstance()->getController();
        if ($type=='cart') {
            $cartProducts = $this->context->cart->getProducts();
            $customerGroups = array(Group::getCurrent()->id);
            $products = [];
                $i=1;
            foreach ($cartProducts as $cartProduct) {
                $catas = Product::getProductCategoriesFull($cartProduct['id_product']);
                $ruleData = AddifyadvanceqtyClass::getRuleByProduct($cartProduct['id_product'], $catas, $customerGroups);
                if ($ruleData) {
                    $changeqtybox = 'yes';
                    $qtyarray = [];
                    $qtyfix = '';
                    if ($ruleData['quantity_type']==2) {
                        foreach (range($ruleData['min_qty'], $ruleData['max_qty'], $ruleData['interval']) as $number) {
                            $qtyarray[] =$number;
                        }
                    } else {
                        $qtyfix = $ruleData['fix_qty'];
                    }
                      $interval =$ruleData['interval'];
                    if ($ruleData['interval']=='') {
                          $inteval = 1;
                    }
                      $i=$i+1;
                     $products[] =  array('id_product' => $cartProduct['id_product'],
                                    'quantity_type' => $ruleData['quantity_type'],
                                    'dropdowns' => $qtyarray,
                                    'changeqtybox' => $changeqtybox,
                                    'cart_quantity'=>$cartProduct['cart_quantity'],
                                    'fixqty' => $qtyfix,
                                    'min' => $ruleData['min_qty'],
                                    'max' => $ruleData['max_qty'],
                                    'interval' => $ruleData['interval'],
                                    'numbers' => $i,
                                );
                }
            }

            $this->context->smarty->assign(array(
                'ps_version' => _PS_VERSION_,
                'products' => $products,
            
            ));
            return $this->display(__FILE__, 'views/templates/hook/advanceqtycart.tpl');
        }
    }
    public function hookActionCartUpdateQuantityBefore($data)
    {

        $customerGroups = array(Group::getCurrent()->id);
        $productId = (int)Tools::getValue('id_product');
        $productAttrib= $data['id_product_attribute'];
        $id_customization= $data['id_customization'];
        $catas = Product::getProductCategoriesFull($productId);
        
        $ruleData = AddifyadvanceqtyClass::getRuleByProduct($productId, $catas, $customerGroups);
         
        if ($ruleData) {
                   $this->context->cart->deleteProduct($productId, $productAttrib, (int)$id_customization);
             return;
        }

        return;
    }
    public function hookActionBeforeCartUpdateQty($data)
    {

       
        $customerGroups = array(Group::getCurrent()->id);
        $productId = (int)Tools::getValue('id_product');
        $productAttrib= $data['id_product_attribute'];
        $id_customization= $data['id_customization'];
        $catas = Product::getProductCategoriesFull($productId);
        
        $ruleData = AddifyadvanceqtyClass::getRuleByProduct($productId, $catas, $customerGroups);
         
        if ($ruleData) {
                   $this->context->cart->deleteProduct($productId, $productAttrib, (int)$id_customization);
             return;
        }

        return;
    }
}
