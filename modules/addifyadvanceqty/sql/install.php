<?php
/**
 * 2020 Addify
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    Addify
 *  @copyright 2020 Addify
 *  @license   http://opensource.org/licenses/afl-3.0.php
 */

$sql = array();

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'addifyadvanceqty` (
    `id_addifyadvanceqty` int(11) NOT NULL AUTO_INCREMENT,
    `active`                       TINYINT(2),
    `priority`                     int(11),
    `title`                        TEXT,
    `categories`                   TEXT,
    `groups`                       TEXT,
    `quantity_type`                 TINYINT(2),
    `product_type`                 TINYINT(2),
    `max_qty`                     int(11),
    `min_qty`                     int(11),
    `interval`                     int(11),
    `fix_qty`                     int(11),
    `product`                      TEXT,
    `id_shop`                      TEXT,
    `date_upd`                     DATE,
    `date_add`                     DATE,

    PRIMARY KEY  (`id_addifyadvanceqty`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}
