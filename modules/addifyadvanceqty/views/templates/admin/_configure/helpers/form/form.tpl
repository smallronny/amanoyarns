{*
* 2020 Addify
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    Addify
*  @copyright 2020 Addify
*  @license   http://opensource.org/licenses/afl-3.0.php
*}

{extends file="helpers/form/form.tpl"}

{block name="input"}

    {if $input.type == "addifyadvanceqty_product"}
      <div id="type_product" class="col-lg-9">
        <input type="hidden" name="inputRestrictedProducts" id="inputRestrictedProducts" value="{if isset($products) AND $products}{foreach from=$products item=product}{$product.id_product|escape:'htmlall':'UTF-8'}-{/foreach}{/if}" />
        <input type="hidden" name="nameRestrictedProducts" id="nameRestrictedProducts" value="{if isset($products) AND $products}{foreach from=$products item=product}{$product.name|escape:'html':'UTF-8'}¤{/foreach}{/if}" />
        <div id="ajax_choose_product">
          <div class="input-group">
            <input type="text" id="restricted_product_input" name="restricted_product_input" />
            <span class="input-group-addon"><i class="icon-search"></i></span>
          </div>
        </div>
        <p class="help-block">{l s='Search and add product(s) to hide list.' mod='addifyadvanceqty'}</p>
        <br>

        <div id="divRestrictedProducts">
          {if isset($products) AND $products}
            {foreach from=$products item=product}
              <div class="form-control-static panel">
                <button type="button" class="btn btn-danger delRestrictedProducts" name="{$product.id_product|escape:'htmlall':'UTF-8'}">
                  <i class="icon-trash text-danger"></i>
                </button>
                <input type="hidden" name="product[]" value="{$product.id_product|escape:'htmlall':'UTF-8'}">
                {$product.name|escape:'html':'UTF-8'}{if !empty($product.reference)}&nbsp;{l s='(ref: %s)' sprintf=$product.reference mod='addifyadvanceqty'}{/if}
              </div>
            {/foreach}
          {/if}
        </div>
      </div>
      <div class="clearfix"></div>
    {else}
      {$smarty.block.parent}
    {/if}
{/block}

{block name="script"}
  
  var active_type = "{if isset($fields_value) AND isset($fields_value.type)}{$fields_value.type|escape:'htmlall':'UTF-8'}{/if}";
  if (typeof active_type === 'undefined' || active_type === '') {
    active_type = $('#type option:selected').val();
  }

  init_types(active_type);

  $(document).on('change', '#type', function(){
    init_types($(this).val());
  });

   var qty_type = "{if isset($fields_value) AND isset($fields_value.qty_type)}{$fields_value.qty_type|escape:'htmlall':'UTF-8'}{/if}";
  if (typeof qty_type === 'undefined' || qty_type === '') {
    qty_type = $('#qty_type option:selected').val();
  }

  qty_types(qty_type);

  $(document).on('change', '#qty_type', function(){
    qty_types($(this).val());
  });


  $('#divRestrictedProducts').delegate('.delRestrictedProducts', 'click', function(){
    self.delRestrictedProducts($(this).attr('name'));
  });
  
  var options = {
    minChars: 1,
    autoFill: true,
    max:20,
    matchContains: true,
    mustMatch:false,
    scroll:false,
    cacheLength:0,
    formatItem: function(item) {
      var itemStringToReturn = item[item.length - 1];
      for(var istr = 0; istr < item.length - 1;istr++) {
        itemStringToReturn += " " + item[istr];
      }
      return itemStringToReturn;
    },
    
  };

  $('#restricted_product_input')
  .autocomplete("{$context_link->getAdminLink('AdminAddifyadvanceqty') nofilter}&exclude_packs=0&excludeVirtuals=0",options)
  .result(addProduct);
  $('#restricted_product_input').setOptions({
    extraParams: {
      ajax: true,
      action: 'getProducts',
      excludeIds: getRestrictedProduct()
    }
  });

  function addProduct(event, data, formatted)
  {
    if (data !== null) {
      var productId = data[1];
      var productName = data[0];

      var $divRestrictedProducts = $('#divRestrictedProducts');
      var $inputRestrictedProducts = $('#inputRestrictedProducts');
      var $nameRestrictedProducts = $('#nameRestrictedProducts');

      /* delete product from select + add product line to the div, input_name, input_ids elements */
      $divRestrictedProducts.html($divRestrictedProducts.html() + '<div class="form-control-static panel"><button type="button" class="delRestrictedProducts btn btn-danger" name="' + productId + '"><i class="icon-trash text-danger"></i></button>&nbsp;'+ productName +'</div><input type="hidden" name="product[]" value="' + productId + '">');
      $nameRestrictedProducts.val($nameRestrictedProducts.val() + productName + '¤');
      ($inputRestrictedProducts.val() !== 'undefined')?$inputRestrictedProducts.val($inputRestrictedProducts.val() + productId + '-'):$inputRestrictedProducts.val(productId + '-');
      $('#restricted_product_input').val('');
      $('#restricted_product_input').setOptions({
        extraParams: {
          ajax: true,
          action: 'getProducts',
          excludeIds : getRestrictedProduct()
        }
      });
    }
  }

  function getRestrictedProduct() {
    if ($('#inputRestrictedProducts').val() !== undefined) {
      return $('#inputRestrictedProducts').val().replace(/\-/g,',');
    }
  }

  function delRestrictedProducts(id)
  {
    var div = getE('divRestrictedProducts');
    var input = getE('inputRestrictedProducts');
    var name = getE('nameRestrictedProducts');

    // Cut hidden fields in array
    var inputCut = input.value.split('-');
    var nameCut = name.value.split('¤');

    if (inputCut.length != nameCut.length) {
      return jAlert('Bad size');
    }

    // Reset all hidden fields
    input.value = '';
    name.value = '';
    div.innerHTML = '';
    for (i in inputCut)
    {
      // If empty, error, next
      if (!inputCut[i] || !nameCut[i]) {
        continue ;
      }

      // Add to hidden fields no selected products OR add to select field selected product
      if (inputCut[i] != id) {
        input.value += inputCut[i] + '-';
        name.value += nameCut[i] + '¤';
        div.innerHTML += '<div class="form-control-static panel"><button type="button" class="delRestrictedProducts btn btn-danger" name="' + inputCut[i] +'"><i class="icon-trash text-danger"></i></button>&nbsp;' + nameCut[i] + '</div><input type="hidden" name="product[]" value="' + inputCut[i] +'">';
      } else {
        $('#selectRestrictedProducts').append('<option selected="selected" value="' + inputCut[i] + '-' + nameCut[i] + '">' + inputCut[i] + ' - ' + nameCut[i] + '</option>');
      }
    }

    $('#restricted_product_input').setOptions({
      extraParams: {
        ajax: true,
        action: 'getProducts',
        excludeIds : getRestrictedProduct()
      }
    });
  };

  function init_types(active_type) {
    if (active_type==2) {
      $('#type_product').closest('.form-group').show();

    }else {
  $('#type_product').closest('.form-group').hide();
    }
  }
  function qty_types(active_type) {
    if (active_type==2) {
      $('#min_qty').closest('.form-group').show();
      $('#max_qty').closest('.form-group').show();
      $('#interval').closest('.form-group').show();
      $('#fix_qty').closest('.form-group').hide();


    } else if (active_type==3) {
      $('#min_qty').closest('.form-group').hide();
      $('#max_qty').closest('.form-group').hide();
      $('#interval').closest('.form-group').hide();
      $('#fix_qty').closest('.form-group').hide();


    }else {
      $('#min_qty').closest('.form-group').hide();
      $('#max_qty').closest('.form-group').hide();
      $('#interval').closest('.form-group').hide();
      $('#fix_qty').closest('.form-group').show();
    }
  }



{/block}

