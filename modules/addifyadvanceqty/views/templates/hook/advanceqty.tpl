{* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2020 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="addifyadvanceqty" id="addifyadvanceqty-{$product_id|escape:'htmlall':'UTF-8'}"  changeqtybox="{$changeqtybox|escape:'htmlall':'UTF-8'}" qtytype="{$quantity_type|escape:'htmlall':'UTF-8'}" 
     currentproid="{$product_id|escape:'htmlall':'UTF-8'}" 
      >
<div class="addifyadvancereplacebox"style="display: none !important;" >
    {if isset($quantity_type) && $quantity_type==1}
    <input type="number" name="qty" readonly="" id="quantity_wanted_addify" value="{$fixqty|escape:'htmlall':'UTF-8'}" class="input-group form-control" minlength="5" maxlength="5" aria-label="Quantity" style="display: block !important;">
    
   {elseif isset($quantity_type) && $quantity_type==3}
    <div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input  readonly="" type="number" name="qty" id="quantity_wanted_addify" value="{$min|escape:'htmlall':'UTF-8'}" class="input-group form-control" min="{$min|escape:'htmlall':'UTF-8'}" max="{$max|escape:'htmlall':'UTF-8'}" interval="{$interval|escape:'htmlall':'UTF-8'}" aria-label="Quantity" style="display: block !important;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"><button class="btn btn-touchspin addify_button_up bootstrap-touchspin-up" onclick="increase()" type="button"><i class="material-icons touchspin-up"></i></button><button class="btn btn-touchspin addify_button_down  bootstrap-touchspin-down" onclick="decrease()" type="button"><i class="material-icons  touchspin-down"></i></button></span></div>
    {else}
    <select type="number" name="qty" readonly="" class="form-control form-control-select" id="quantity_wanted_addify" style="display: block !important;">
    {foreach from =$dropdowns item=dropdown}
        <option value="{$dropdown|escape:'htmlall':'UTF-8'}">{$dropdown|escape:'htmlall':'UTF-8'}</option>
    {/foreach}
    </select>
    {/if}
</div>
    <ul class="ps-alert-error  addifyadvanceqty_error_msg_max" style="display: none;"><li class="item">
        <i>
            <svg viewBox="0 0 24 24">
                <path fill="#fff" d="M11,15H13V17H11V15M11,7H13V13H11V7M12,2C6.47,2 2,6.5 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2M12,20A8,8 0 0,1 4,12A8,8 0 0,1 12,4A8,8 0 0,1 20,12A8,8 0 0,1 12,20Z"></path>
            </svg>
        </i>
        {l s='You reached Maximun quantity' mod='addifyadvanceqty'}
    </li></ul>
     <ul class="ps-alert-error  addifyadvanceqty_error_msg_min" style="display: none;"><li class="item">
        <i>
            <svg viewBox="0 0 24 24">
                <path fill="#fff" d="M11,15H13V17H11V15M11,7H13V13H11V7M12,2C6.47,2 2,6.5 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2M12,20A8,8 0 0,1 4,12A8,8 0 0,1 12,4A8,8 0 0,1 20,12A8,8 0 0,1 12,20Z"></path>
            </svg>
        </i>
        {l s='You reached Minimum quantity' mod='addifyadvanceqty'}
    </li></ul>
</div>


