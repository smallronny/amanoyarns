{* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2020 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{foreach from =$products item=product}

<div class="addifyadvancereplaceboxcart " id="addifyadvancereplacebox-{$product.id_product|escape:'htmlall':'UTF-8'}" numbers="{$product.numbers|escape:'htmlall':'UTF-8'}" changeqtybox="{$product.changeqtybox|escape:'htmlall':'UTF-8'}" style="display: none; !important;" >
<div class="addifyadvancereplacebox">
    {if isset($product.quantity_type)}
     <div type="number" name="qty" readonly="" id="quantity_wanted_addify" value="{$product.cart_quantity|escape:'htmlall':'UTF-8'}" class="input-group form-control js-cart-line-product-quantity-addify" minlength="5" maxlength="5" aria-label="Quantity"   style="display: block !important;">{$product.cart_quantity|escape:'htmlall':'UTF-8'}</div>
    
    {/if}
</div>
</div>
{/foreach}
  
    


