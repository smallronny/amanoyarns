/**
 * 2020 Addify
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    Addify
 *  @copyright 2020 Addify
 *  @license   http://opensource.org/licenses/afl-3.0.php
 */
document.addEventListener("DOMContentLoaded", function (event) {


    var hiddenall = document.getElementsByClassName("addifyadvanceqty");
    
    for (var i = 0; i < hiddenall.length; i++) {
        if (i==0) {
            var changeqtybox = hiddenall[i].getAttribute('changeqtybox');
            var currentproid = hiddenall[i].getAttribute('currentproid');
            var quantity_type = hiddenall[i].getAttribute('qtytype');
            if (changeqtybox=="yes") {
                parentele = hiddenall[i].parentElement;
                attribute = parentele.getAttribute('class');
                var addifyreplacebox = parentele.getElementsByClassName('addifyadvancereplacebox');
                var qtybox = document.getElementById('quantity_wanted');

             
                html = addifyreplacebox[0].innerHTML;
                html = html.replace("quantity_wanted_addify", "quantity_wanted");
                qtybox.parentNode.parentNode.innerHTML= html;
                var qtybox = document.getElementById('quantity_wanted');
                if (quantity_type==3) {
                    qtybox.parentNode.parentNode.style.cssText='display:block !important';
                } else {
                    qtybox.parentNode.style.cssText='display:block !important';
                }
            } else {
                var qtybox = document.getElementById('quantity_wanted');
                if (qtybox) {
                    qtybox.parentNode.parentNode.style.cssText='display:block !important';
                }
            }
        }
    }



});



$(document).ajaxComplete(function (event, xhr, settings) {


        var hiddenall = document.getElementsByClassName("addifyadvanceqty");

    for (var i = 0; i < hiddenall.length; i++) {
        var changeqtybox = hiddenall[i].getAttribute('changeqtybox');
        var qtybox = document.getElementById('quantity_wanted_addify').value;
        var quantity_type = hiddenall[i].getAttribute('qtytype');


        if (changeqtybox=="yes") {
            if (settings.url.includes("/cart")==true) {
                 curval = document.getElementById('quantity_wanted').value;
                if (qtybox!=curval) {
                    document.getElementById('quantity_wanted').value=qtybox;
                }
            } else {
                parentele = hiddenall[i].parentElement;
                attribute = parentele.getAttribute('class');
                var addifyreplacebox = parentele.getElementsByClassName('addifyadvancereplacebox');
                var qtybox = document.getElementById('quantity_wanted');

         
                html = addifyreplacebox[0].innerHTML;
                html = html.replace("quantity_wanted_addify", "quantity_wanted");
                attribute = qtybox.parentNode.parentNode.getAttribute('class');
                if (attribute=="qty") {
                    qtybox.parentNode.parentNode.innerHTML= html;
                } else {
                    qtybox.innerHTML= html;
                }
                    var qtybox = document.getElementById('quantity_wanted');
                if (quantity_type==3) {
                    qtybox.parentNode.parentNode.style.cssText='display:block !important';
                } else {
                    qtybox.parentNode.style.cssText='display:block !important';
                }
            }
        } else {
            var qtybox = document.getElementById('quantity_wanted');
            if (qtybox) {
                qtybox.parentNode.parentNode.style.cssText='display:block !important';
            }
        }
    }
    if (settings.url.includes("module/ps_shoppingcart/ajax") ==true) {
        var hiddenall = document.getElementsByClassName("js-cart-line-product-quantity");
        
        for (var i = 0; i < hiddenall.length; i++) {
            var productid = hiddenall[i].getAttribute('data-product-id');
            var update = hiddenall[i].getAttribute('data-update-url');
            advqtybox = document.getElementById("addifyadvancereplacebox-"+productid);
            var number = advqtybox.getAttribute('numbers');
            if (advqtybox) {
                html = advqtybox.innerHTML;
                html = html.replace("data-update-url-upd", update);
                html = html.replace("js-cart-line-product-quantity-addify", 'js-cart-line-product-quantity');
                html = html.replace("adqty", 'qty');


                hiddenall[i].parentNode.parentNode.innerHTML= html;
                var qtyinput =document.getElementById('adqty-'+productid+'-'+number).value;

                var qtyinput =document.getElementById('qty-'+productid+'-'+number).value=qtyinput;
            }
        }
    }
    if (settings.url.includes("controller=product") ==true) {
            var url = new URL(settings.url);
            var qty = url.searchParams.get("qty");
           
        if (qty) {
            document.getElementById('quantity_wanted').value=qty;
        }
    }


});
// document.addEventListener("DOMContentLoaded", function (event) {

// $( ".addify_button_up" ).click(function() {
    
//     var qtyinput =document.getElementById('quantity_wanted');
//     var curval = qtyinput.value;
//     var min = qtyinput.getAttribute('min');
//     var max = qtyinput.getAttribute('max');
//     var interval = qtyinput.getAttribute('interval');
//     if(curval == max){
//         $( ".addifyadvanceqty_error_msg_max" ).show();
//         $( ".addifyadvanceqty_error_msg_min" ).hide();

//     }else {
//         qtyinput.value = parseInt(curval)+parseInt(interval);
//     }
// });

// $( ".addify_button_down").click(function() {
//   var qtyinput =document.getElementById('quantity_wanted');
//     var curval = qtyinput.value;
//     var min = qtyinput.getAttribute('min');
//     var max = qtyinput.getAttribute('max');
//     var interval = qtyinput.getAttribute('interval');
//     if(curval == min){
//         $( ".addifyadvanceqty_error_msg_min" ).show();
//                 $( ".addifyadvanceqty_error_msg_max" ).hide();


//     }else {
//         qtyinput.value = parseInt(curval)-parseInt(interval);
//     }
// });
// });

document.addEventListener("DOMContentLoaded", function (event) {


    var hiddenall = document.getElementsByClassName("js-cart-line-product-quantity");
    for (var i = 0; i < hiddenall.length; i++) {
        var productid = hiddenall[i].getAttribute('data-product-id');
        var update = hiddenall[i].getAttribute('data-update-url');
        advqtybox = document.getElementById("addifyadvancereplacebox-"+productid);
        if (advqtybox) {
            html = advqtybox.innerHTML;
            html = html.replace("js-cart-line-product-quantity-addify", 'js-cart-line-product-quantity');
            hiddenall[i].parentNode.parentNode.innerHTML= html;
        }
    }
   



});
$(document).ajaxComplete(function (event, xhr, settings) {


        var hiddenall = document.getElementsByClassName("js-cart-line-product-quantity");
    for (var i = 0; i < hiddenall.length; i++) {
        var productid = hiddenall[i].getAttribute('data-product-id');
        var update = hiddenall[i].getAttribute('data-update-url');
        advqtybox = document.getElementById("addifyadvancereplacebox-"+productid);
        if (advqtybox) {
            html = advqtybox.innerHTML;
            html = html.replace("js-cart-line-product-quantity-addify", 'js-cart-line-product-quantity');
            hiddenall[i].parentNode.parentNode.innerHTML= html;
        }
    }

});

function increase()
{
    var qtyinput =document.getElementById('quantity_wanted');
    var curval = qtyinput.value;
    var min = qtyinput.getAttribute('min');
    var max = qtyinput.getAttribute('max');
    var interval = qtyinput.getAttribute('interval');
    if (curval == max) {
        $( ".addifyadvanceqty_error_msg_max" ).show();
        $( ".addifyadvanceqty_error_msg_min" ).hide();
    } else {
        qtyinput.value = parseInt(curval)+parseInt(interval);
    }
}

function decrease()
{
    var qtyinput =document.getElementById('quantity_wanted');
    var curval = qtyinput.value;
    var min = qtyinput.getAttribute('min');
    var max = qtyinput.getAttribute('max');
    var interval = qtyinput.getAttribute('interval');
    if (curval == min) {
        $( ".addifyadvanceqty_error_msg_min" ).show();
                $( ".addifyadvanceqty_error_msg_max" ).hide();
    } else {
        qtyinput.value = parseInt(curval)-parseInt(interval);
    }
}

