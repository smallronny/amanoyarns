<?php
/**
 * PrestaShop module created by VEKIA, a guy from official PrestaShop community ;-)
 *
 * @author    VEKIA https://www.prestashop.com/forums/user/132608-vekia/
 * @copyright 2010-2020 VEKIA
 * @license   This program is not free software and you can't resell and redistribute it
 *
 * CONTACT WITH DEVELOPER http://mypresta.eu
 * support@mypresta.eu
 */

class Configuration extends ConfigurationCore
{
    public static function get($key, $idLang = null, $idShopGroup = null, $idShop = null, $default = false)
    {
        if (isset(Context::getContext()->controller->controller_type)) {
            $controller_type = Context::getContext()->controller->controller_type;
        } else {
            $controller_type = false;
        }
        if ((strtoupper($key) == 'PS_LANG_DEFAULT' || strtoupper($key) == 'PS_CURRENCY_DEFAULT') && (defined('_PS_ADMIN_DIR_') == false || $controller_type == 'front'))
        {
            $progeo = @Module::getInstanceByName('progeo');
            if ($progeo != false) {
                $geolocation = $progeo::returnUserCountry();
                $rule = georule::getByCountry(strtoupper($geolocation));
                if (strtoupper($key) == 'PS_LANG_DEFAULT' && Configuration::get('PROGEO_DIS_LANG') != true) {
                    if (isset($rule['id_language'])) {
                        if ($rule['id_language'] != 0) {
                            return $rule['id_language'];
                        }
                    }
                }
                if (strtoupper($key) == 'PS_CURRENCY_DEFAULT' && Configuration::get('PROGEO_DIS_CURR') != true) {
                    if (isset($rule['id_currency'])) {
                        if ($rule['id_currency'] != 0) {
                            return $rule['id_currency'];
                        }
                    }
                }
            }
        }
        return parent::get($key, $idLang, $idShopGroup, $idShop, $default);
    }
}