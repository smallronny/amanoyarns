<div class="alert alert-danger text-center">
    {l s='You run module geolocation in simulate mode. This mode allows to test the module for various IP addresses.' mod='progeo'}<br />
    {l s='Your simulated IP address is:' mod='progeo'} <strong>{Configuration::get('PROGEO_SIMULATE_IP')}</strong>. {l s='Geolocation identify this visit as a visit from country:' mod='progeo'}<strong> {progeo::returnUserCountry()}</strong><br/>
    {l s='This message will disappear once you will disable the simulate mode on module configuration page.' mod='progeo'}<br />
</div>