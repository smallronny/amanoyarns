<table class="table table-bordered table-striped">
    <tr>
        <th>{l s='Currency' mod='progeo'}</th>
        <th>{l s='Enable' mod='progeo'}</th>
        <th>{l s='Default currency' mod='progeo'}</th>
    </tr>
    {foreach $currencies AS $currency}
        <tr>
            <td width="300">{$currency['iso_code']} ({$currency.sign})</td>
            <td><input type="checkbox" class="selected_currency" id="selected_currency_{$currency.id_currency}"
                       name="currencies[]" value="{$currency.id_currency}"
                       {if isset($selected_currencies->currencies)}{if in_array($currency.id_currency, $selected_currencies->currencies)}checked{/if}{/if}/>
            </td>
            <td><input type="radio" class="default_currency" id="default_currency_{$currency.id_currency}" name="id_currency"
                       value="{$currency.id_currency}"
                       {if isset($selected_currencies->id_currency)}
                       {if $selected_currencies->id_currency == $currency.id_currency}checked{/if}/></td>
                       {/if}
        </tr>
    {/foreach}
</table>

<script>
    function reloadCurrencyForm() {
        checked_default = false;
        last_checked = false;
        $('input.selected_currency:checked').each(function (i, d) {
            last_checked = d.value;
            if ($('#default_currency_' + d.value).is(':checked')) {
                checked_default = true
            }
        });
        if (checked_default == false && last_checked != false) {
            $('#default_currency_' + last_checked).click();
        }
    }

    $(document).ready(function () {
        $('.default_currency').change(function(){
            if ($('#selected_currency_'+$('.default_currency:checked').val()).is(':checked')){

            } else {
                $('#selected_currency_'+$('.default_currency:checked').val()).click();
            }
        });
        $('.selected_currency').change(function(){
            reloadCurrencyForm();
        });
        reloadCurrencyForm();
    });
</script>