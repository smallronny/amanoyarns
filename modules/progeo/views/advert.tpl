{*
* PrestaShop module created by VEKIA, a guy from official PrestaShop community ;-)
*
* @author    VEKIA https://www.prestashop.com/forums/user/132608-vekia/
* @copyright 2010-2020 VEKIA
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER http://mypresta.eu
* support@mypresta.eu
*}

<div class="panel">
    <div class="panel-heading">
        <i class="icon-cogs"></i>&nbsp;{l s='Information' mod='progeo'}
        <a style="top:3px; margin-right:3px;" class="pull-right btn button label label-info" target="_blank" href="https://www.facebook.com/mypresta"><i class="icon-facebook"></i> {l s='Facebook' mod='progeo'}</span></a>
        <span class="pull-right">{l s='Like us on ' mod='progeo'}&nbsp;</span>
    </div>
    <div class="alert alert-info">
        {l s='Geolocation language & currency rules you can configure here' mod='progeo'}: <strong><a href="{Context::getContext()->link->getAdminLink('AdminGeorules', true)}">{l s='Shop Parameters > Geolocation rules' mod='progeo'}</a></strong>
    </div>
</div>