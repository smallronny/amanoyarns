<table class="table table-bordered table-striped">
    <tr>
        <th>{l s='Language' mod='progeo'}</th>
        <th>{l s='Enable' mod='progeo'}</th>
        <th>{l s='Default language' mod='progeo'}</th>
    </tr>
    {foreach $languages AS $language}
        <tr>
            <td width="300">{$language['name']} ({$language.iso_code})</td>
            <td><input type="checkbox" class="selected_language" id="selected_language_{$language.id_lang}"
                       name="languages[]" value="{$language.id_lang}"
                       {if isset($selected_languages->languages)}{if in_array($language.id_lang, $selected_languages->languages)}checked{/if}{/if}/>
            </td>
            <td><input type="radio" class="default_language" id="default_language_{$language.id_lang}" name="id_language"
                       value="{$language.id_lang}"
                       {if isset($selected_languages->id_language)}
                       {if $selected_languages->id_language == $language.id_lang}checked{/if}
                       {/if}
                       /></td>
        </tr>
    {/foreach}
</table>

<script>
    function reloadlanguageForm() {
        checked_default_lang = false;
        last_checked = false;
        $('input.selected_language:checked').each(function (i, d) {
            last_checked = d.value;
            if ($('#default_language_' + d.value).is(':checked')) {
                checked_default_lang = true
            }
        });
        if (checked_default_lang == false && last_checked != false) {
            $('#default_language_' + last_checked).click();
        }
    }

    $(document).ready(function () {
        $('.default_language').change(function () {
            if ($('#selected_language_' + $('.default_language:checked').val()).is(':checked')) {

            } else {
                $('#selected_language_' + $('.default_language:checked').val()).click();
            }
        });
        $('.selected_language').change(function () {
            reloadlanguageForm();
        });
        reloadlanguageForm();
    });
</script>