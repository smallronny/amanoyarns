<?php

class georule extends ObjectModel
{
    public $id_georule;
    public $id_shop;
    public $active;
    public $id_country;
    public $id_currency;
    public $id_language;
    public $currencies;
    public $languages;

    public static $definition = array(
        'table' => 'georule',
        'primary' => 'id_georule',
        'multilang' => false,
        'fields' => array(
            'id_georule' => array('type' => ObjectModel :: TYPE_INT),
            'id_shop' => array('type' => ObjectModel :: TYPE_INT),
            'active' => array('type' => ObjectModel :: TYPE_BOOL),
            'id_country' => array('type' => ObjectModel :: TYPE_INT),
            'id_currency' => array('type' => ObjectModel :: TYPE_INT),
            'id_language' => array('type' => ObjectModel :: TYPE_INT),
            'currencies' => array('type' => ObjectModel :: TYPE_STRING),
            'languages' => array('type' => ObjectModel :: TYPE_STRING),
        ),
    );

    public static function getByCountry($country)
    {
        $return = Db::getInstance(_PS_USE_SQL_SLAVE_)->Executes('
            SELECT g.* FROM `' . _DB_PREFIX_ . 'georule` g 
            INNER JOIN `' . _DB_PREFIX_ . 'country` c ON (g.`id_country` = c.`id_country`)
            WHERE g.active = 1 AND c.iso_code = "' . $country . '" AND g.id_shop = '.(isset(Context::getContext()->shop->id) ? (int)Context::getContext()->shop->id:1).' LIMIT 1');
        if ($return == false) {
            return false;
        } else {
            foreach ($return AS $k => $i) {
                $return[$k]['currencies'] = explode(',', $i['currencies']);
                $return[$k]['languages'] = explode(',', $i['languages']);
            }
        }
        return (isset($return[0]) ? $return[0]:false);
    }
}

?>