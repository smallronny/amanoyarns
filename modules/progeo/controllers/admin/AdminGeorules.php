<?php

/**
 * PrestaShop module created by VEKIA, a guy from official PrestaShop community ;-)
 *
 * @author    VEKIA https://www.prestashop.com/forums/user/132608-vekia/
 * @copyright 2010-2020 VEKIA
 * @license   This program is not free software and you can't resell and redistribute it
 *
 * CONTACT WITH DEVELOPER http://mypresta.eu
 * support@mypresta.eu
 */
include_once('../modules/progeo/model/georule.php');
include_once('../modules/progeo/progeo.php');

class AdminGeorulesController extends ModuleAdminController
{
    public function __construct()
    {
        $this->progeo = new Progeo();
        $this->table = 'georule';
        $this->className = 'georule';
        $this->lang = false;
        $this->addRowAction('edit');
        $this->addRowAction('add');
        $this->addRowAction('delete');
        parent::__construct();

        $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->l('Delete selected'),
                'confirm' => $this->l('Delete selected items?')
            )
        );
        $this->bootstrap = true;
        $this->_orderBy = 'id_georule';
        $this->fields_list = array(
            'id_georule' => array(
                'title' => $this->l('ID'),
                'align' => 'left',
                'orderby' => false,
                'search' => false,
                'width' => 40
            ),
            'id_country' => array(
                'title' => $this->l('Country'),
                'align' => 'left',
                'orderby' => false,
                'search' => false,
                'width' => 250,
                'callback' => 'getCountryName',
            ),
            'languages' => array(
                'title' => $this->l('Languages'),
                'width' => 50,
                'orderby' => false,
                'search' => false,
                'align' => 'left',
                'callback' => 'getListLanguages'
            ),
            'id_language' => array(
                'title' => $this->l('Default language'),
                'width' => 50,
                'orderby' => false,
                'search' => false,
                'align' => 'left',
                'callback' => 'getDefaultLanguage'
            ),
            'currencies' => array(
                'title' => $this->l('Currencies'),
                'width' => 50,
                'orderby' => false,
                'search' => false,
                'align' => 'left',
                'callback' => 'getListCurrencies'
            ),
            'id_currency' => array(
                'title' => $this->l('Default currency'),
                'width' => 50,
                'orderby' => false,
                'search' => false,
                'align' => 'left',
                'callback' => 'getDefaultCurrency'
            ),
            'active' => array(
                'title' => $this->l('Active'),
                'width' => 50,
                'orderby' => true,
                'type' => 'bool',
                'active' => 'status',
                'align' => 'center'
            ),
        );
    }

    public function init()
    {
        if (Shop::getContext() == Shop::CONTEXT_SHOP && Shop::isFeatureActive()) {
            $this->_where = Shop::addSqlRestriction(false);
        }
        $this->_group = 'GROUP BY a.`id_georule`';
        parent::init();
    }

    public function renderForm()
    {
        if (!$this->loadObject(true)) {
            return;
        }
        $obj = $this->loadObject(true);

        if (isset($obj->id)) {
            $this->display = 'edit';
            $title = $this->l('Edit geolocation rule');
        } else {
            $this->display = 'add';
            $title = $this->l('Add new geolocation rule');
            $obj = false;
        }

        $this->fields_form = array(
            'legend' => array(
                'title' => $title,
            ),
            'input' => array(
                array(
                    'type' => 'select',
                    'label' => $this->l('Select country'),
                    'name' => 'id_country',
                    'required' => true,
                    'options' => array(
                        'query' => Country::getCountries(Context::getContext()->language->id, false, false),
                        'id' => 'id_country',
                        'name' => 'name'
                    ),
                ),
                array(
                    'type' => 'html',
                    'label' => $this->l('Currencies'),
                    'name' => 'currencies',
                    'required' => true,
                    'html_content' => $this->renderCurrenciesForm($obj),
                    'lang' => false,
                ),
                array(
                    'type' => 'html',
                    'label' => $this->l('Languages'),
                    'name' => 'languages',
                    'required' => true,
                    'html_content' => $this->renderLanguagesForm($obj),
                    'lang' => false,
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Active:'),
                    'name' => 'active',
                    'required' => true,
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('On')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Off')
                        )
                    ),
                    'lang' => false,
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save')
            )
        );
        return parent::renderForm();
    }

    public function renderCurrenciesForm($object = false)
    {
        if ($object != false) {
            $object->currencies = explode(',', $object->currencies);
        }

        $this->context->smarty->assign('selected_currencies', $object);
        $this->context->smarty->assign('currencies', Currency::getCurrencies(false, true, 'id_currency'));
        return $this->context->smarty->fetch(_PS_MODULE_DIR_ . $this->progeo->name . '/views/currenciesForm.tpl');
    }

    public function renderLanguagesForm($object = false)
    {
        if ($object != false) {
            $object->languages = explode(',', $object->languages);
        }

        $this->context->smarty->assign('selected_languages', $object);
        $this->context->smarty->assign('languages', Language::getLanguages());
        return $this->context->smarty->fetch(_PS_MODULE_DIR_ . $this->progeo->name . '/views/languagesForm.tpl');
    }

    public function processUpdate()
    {
        $this->preProcess();
        $object = parent::processUpdate();
        return true;
    }

    public function preProcess()
    {
        $_POST['currencies'] = implode(',', Tools::getValue('currencies'));
        $_POST['languages'] = implode(',', Tools::getValue('languages'));
    }

    public function processAdd()
    {
        $this->preProcess();
        $object = parent::processAdd();
        return true;
    }

    public function getDefaultCurrency($val, $row)
    {
        $currency = new Currency($val);
        if (isset($currency->name) && isset($currency->iso_code)) {
            if ($currency->name != NULL && $currency->iso_code != NULL) {
                return $currency->name . ' (' . $currency->iso_code . ')';
            }
        }
        return $val;
    }

    public function getListCurrencies($val, $row)
    {
        $return = '';
        $currencies = explode(',', $val);
        foreach ($currencies AS $currency) {
            $return .= $this->getDefaultCurrency($currency, $row) . '<br/>';
        }
        return $return;
    }

    public function getDefaultLanguage($val, $row)
    {
        $language = new Language($val);
        if (isset($language->name) && isset($language->iso_code)) {
            if ($language->name != NULL && $language->iso_code != NULL) {
                return $language->name . ' (' . $language->iso_code . ')';
            }
        }
        return $val;
    }

    public function beforeAdd($object)
    {
        $object->id_shop = Context::getContext()->shop->id;
        return true;
    }

    public function getListLanguages($val, $row)
    {
        $return = '';
        $languages = explode(',', $val);
        foreach ($languages AS $language) {
            $return .= $this->getDefaultLanguage($language, $row) . '<br/>';
        }
        return $return;
    }

    public function getCountryName($val, $row)
    {
        $country = new Country($val, $this->context->language->id);
        return $this->l('Visitors from:'). ' <strong>' . $country->name . '</strong>';
        return $val;
    }
}
