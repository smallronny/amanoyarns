<?php
/**
 * PrestaShop module created by VEKIA, a guy from official PrestaShop community ;-)
 *
 *
 *
 * TEMPORARILY DISABLED OVERRIDE
 *
 *
 *
 * @author    VEKIA https://www.prestashop.com/forums/user/132608-vekia/
 * @copyright 2010-2020 VEKIA
 * @license   This program is not free software and you can't resell and redistribute it
 *
 * CONTACT WITH DEVELOPER http://mypresta.eu
 * support@mypresta.eu
 */

require_once _PS_MODULE_DIR_ . 'progeo/model/georule.php';

class progeo extends Module
{
    public function __construct()
    {
        $this->name = 'progeo';
        $this->tab = 'pricing_promotion';
        $this->author = 'MyPresta.eu';
        $this->mypresta_link = 'https://mypresta.eu/modules/front-office-features/geolocation-pro-currency-and-language-by-country.html';
        $this->version = '1.3.0';
        parent::__construct();
        $this->secure_key = Tools::encrypt($this->name);
        $this->bootstrap = true;
        $this->displayName = $this->l('Geolocation - currency and language by location - PRO VERSION');
        $this->description = $this->l('Module sets the currency + language of shop depending on visitor origin (by country)');
        $this->checkforupdates();
    }

    public function checkforupdates($display_msg = 0, $form = 0)
    {
        // ---------- //
        // ---------- //
        // VERSION 16 //
        // ---------- //
        // ---------- //
        $this->mkey = "nlc";
        if (@file_exists('../modules/' . $this->name . '/key.php')) {
            @require_once('../modules/' . $this->name . '/key.php');
        } else {
            if (@file_exists(dirname(__FILE__) . $this->name . '/key.php')) {
                @require_once(dirname(__FILE__) . $this->name . '/key.php');
            } else {
                if (@file_exists('modules/' . $this->name . '/key.php')) {
                    @require_once('modules/' . $this->name . '/key.php');
                }
            }
        }
        if ($form == 1) {
            return '
            <div class="panel" id="fieldset_myprestaupdates" style="margin-top:20px;">
            ' . ($this->psversion() == 6 || $this->psversion() == 7 ? '<div class="panel-heading"><i class="icon-wrench"></i> ' . $this->l('MyPresta updates') . '</div>' : '') . '
			<div class="form-wrapper" style="padding:0px!important;">
            <div id="module_block_settings">
                    <fieldset id="fieldset_module_block_settings">
                         ' . ($this->psversion() == 5 ? '<legend style="">' . $this->l('MyPresta updates') . '</legend>' : '') . '
                        <form action="' . $_SERVER['REQUEST_URI'] . '" method="post">
                            <label>' . $this->l('Check updates') . '</label>
                            <div class="margin-form">' . (Tools::isSubmit('submit_settings_updates_now') ? ($this->inconsistency(0) ? '' : '') . $this->checkforupdates(1) : '') . '
                                <button style="margin: 0px; top: -3px; position: relative;" type="submit" name="submit_settings_updates_now" class="button btn btn-default" />
                                <i class="process-icon-update"></i>
                                ' . $this->l('Check now') . '
                                </button>
                            </div>
                            <label>' . $this->l('Updates notifications') . '</label>
                            <div class="margin-form">
                                <select name="mypresta_updates">
                                    <option value="-">' . $this->l('-- select --') . '</option>
                                    <option value="1" ' . ((int)(Configuration::get('mypresta_updates') == 1) ? 'selected="selected"' : '') . '>' . $this->l('Enable') . '</option>
                                    <option value="0" ' . ((int)(Configuration::get('mypresta_updates') == 0) ? 'selected="selected"' : '') . '>' . $this->l('Disable') . '</option>
                                </select>
                                <p class="clear">' . $this->l('Turn this option on if you want to check MyPresta.eu for module updates automatically. This option will display notification about new versions of this addon.') . '</p>
                            </div>
                            <label>' . $this->l('Module page') . '</label>
                            <div class="margin-form">
                                <a style="font-size:14px;" href="' . $this->mypresta_link . '" target="_blank">' . $this->displayName . '</a>
                                <p class="clear">' . $this->l('This is direct link to official addon page, where you can read about changes in the module (changelog)') . '</p>
                            </div>
                            <div class="panel-footer">
                                <button type="submit" name="submit_settings_updates"class="button btn btn-default pull-right" />
                                <i class="process-icon-save"></i>
                                ' . $this->l('Save') . '
                                </button>
                            </div>
                        </form>
                    </fieldset>
                    <style>
                    #fieldset_myprestaupdates {
                        display:block;clear:both;
                        float:inherit!important;
                    }
                    </style>
                </div>
            </div>
            </div>';
        } else {
            if (defined('_PS_ADMIN_DIR_')) {
                if (Tools::isSubmit('submit_settings_updates')) {
                    Configuration::updateValue('mypresta_updates', Tools::getValue('mypresta_updates'));
                }
                if (Configuration::get('mypresta_updates') != 0 || (bool)Configuration::get('mypresta_updates') != false) {
                    if (Configuration::get('update_' . $this->name) < (date("U") - 259200)) {
                        $actual_version = progeoUpdate::verify($this->name, (isset($this->mkey) ? $this->mkey : 'nokey'), $this->version);
                    }
                    if (progeoUpdate::version($this->version) < progeoUpdate::version(Configuration::get('updatev_' . $this->name)) && Tools::getValue('ajax', 'false') == 'false') {
                        $this->context->controller->warnings[] = '<strong>' . $this->displayName . '</strong>: ' . $this->l('New version available, check http://MyPresta.eu for more informations') . ' <a href="' . $this->mypresta_link . '">' . $this->l('More details in changelog') . '</a>';
                        $this->warning = $this->context->controller->warnings[0];
                    }
                } else {
                    if (Configuration::get('update_' . $this->name) < (date("U") - 259200)) {
                        $actual_version = progeoUpdate::verify($this->name, (isset($this->mkey) ? $this->mkey : 'nokey'), $this->version);
                    }
                }
                if ($display_msg == 1) {
                    if (progeoUpdate::version($this->version) < progeoUpdate::version(progeoUpdate::verify($this->name, (isset($this->mkey) ? $this->mkey : 'nokey'), $this->version))) {
                        return "<span style='color:red; font-weight:bold; font-size:16px; margin-right:10px;'>" . $this->l('New version available!') . "</span>";
                    } else {
                        return "<span style='color:green; font-weight:bold; font-size:16px; margin-right:10px;'>" . $this->l('Module is up to date!') . "</span>";
                    }
                }
            }
        }
    }

    public function inconsistency($ret)
    {
        $this->maybeUpdateDatabase('georule', 'id_shop', "INT(4)", 1, "NOT NULL");
        return true;
    }

    public function install()
    {
        if (parent::install() == false ||
            $this->inDelMenu('install', 'AdminGeorules', $this->l('Geolocation rules'), 'ShopParameters') == false ||
            $this->registerHook('actionAdminControllerSetMedia') == false ||
            $this->registerHook('actionFrontControllerAfterInit') == false ||
            $this->registerHook('actionDispatcherBefore') == false ||
            $this->registerHook('displayHeader') == false ||
            Configuration::updateValue('PS_DETECT_LANG', 0) == false ||
            Configuration::updateValue('PS_DETECT_COUNTRY', 0) == false ||
            $this->installdb() == false
        ) {
            return false;
        }
        return true;
    }

    private function installdb()
    {
        $prefix = _DB_PREFIX_;
        $statements = array();

        $statements[] = "CREATE TABLE IF NOT EXISTS `${prefix}georule` (
          `id_georule` int(10) unsigned NOT NULL AUTO_INCREMENT,
          `active` int(1) NOT NULL DEFAULT 0,
          `id_country` int(4) NULL DEFAULT 0,
          `id_language` int(4) NULL DEFAULT 0,
          `id_currency` int(4) NULL DEFAULT 0,
          `currencies` VARCHAR(254) NULL DEFAULT NULL,
          `languages` VARCHAR(254) NULL DEFAULT NULL,
          PRIMARY KEY(`id_georule`)
        ) DEFAULT CHARSET = utf8;";

        foreach ($statements as $statement) {
            if (!Db::getInstance()->Execute($statement)) {
                return false;
            }
        }

        $this->inconsistency(0);

        return true;
    }

    public function uninstall()
    {
        parent::uninstall();
        $this->inDelMenu('uninstall', 'AdminGeorules');
        return true;
    }

    public function hookactionAdminControllerSetMedia()
    {
        //HOOK FOR UPDATE NOTIFICATIONS PURPOSES
    }

    public function hookactionDispatcherBefore($params)
    {
        return $this->hookactionFrontControllerAfterInit($params);
    }

    public function hookactionFrontControllerAfterInit($params)
    {
        if ((defined('_PS_ADMIN_DIR_') == false || Context::getContext()->controller->controller_type == 'front')) {
            $geolocation = $this->returnUserCountry();
            if ($geolocation != false) {
                $rule = georule::getByCountry(strtoupper($geolocation));
                $date_cookie = (isset($this->context->cookie->date_add) ? date("U", strtotime($this->context->cookie->date_add)) : date("U"));
                $date_now = date("U");

                if ($rule != false) {
                    if (!in_array($this->context->cookie->id_lang, $rule['languages']) || ($date_now - $date_cookie) < 5) {
                        $default_language = new Language($rule['id_language']);
                        $this->context->language = $default_language;
                        $this->context->cookie->id_lang = $rule['id_language'];
                        $params['cookie']->id_lang = $rule['id_language'];
                        $params['cookie']->write();
                        $this->context->cookie->write();
                    }

                    if (!in_array($this->context->cookie->id_currency, $rule['currencies']) || ($date_now - $date_cookie) < 5) {
                        $default_currency = new Currency($rule['id_currency'], $this->context->language->id);
                        $this->context->cookie->id_currency = $rule['id_currency'];
                        $this->context->currency = $default_currency;
                        $params['cookie']->id_currency = $rule['id_currency'];
                        $params['cookie']->write();
                        $this->context->cookie->write();
                    }

                    if (Context::getContext()->customer->isLogged()) {
                        $customer = new Customer($this->context->customer->id);
                        if (!in_array($customer->id_lang, $rule['languages'])) {
                            $customer->id_lang = $this->context->cookie->id_lang;
                            $customer->save();
                        }
                    }
                }
            }
        }
    }

    private function maybeUpdateDatabase($table, $column, $type = "int(8)", $default = "1", $null = "NULL", $onUpdate = '')
    {
        $sql = 'DESCRIBE ' . _DB_PREFIX_ . $table;
        $columns = Db::getInstance()->executeS($sql);
        $found = false;
        foreach ($columns as $col) {
            if ($col['Field'] == $column) {
                $found = true;
                break;
            }
        }
        if (!$found) {
            if (!Db::getInstance()->execute('ALTER TABLE `' . _DB_PREFIX_ . $table . '` ADD `' . $column . '` ' . $type . ' DEFAULT ' . $default . ' ' . $null . ' ' . $onUpdate)) {
                return false;
            }
        }
        return true;
    }

    public function hookdisplayHeader()
    {
        if (Configuration::get('PROGEO_SIMULATE_ON') == true) {
            return $this->context->smarty->fetch(_PS_MODULE_DIR_ . $this->name . '/views/displayHeader.tpl');
        }

    }

    private function _postProcess()
    {
        if (Tools::isSubmit('btnSubmit')) {
            Configuration::updateValue('PROGEO_SIMULATE_IP', Tools::getValue('PROGEO_SIMULATE_IP'));
            Configuration::updateValue('PROGEO_SIMULATE_ON', Tools::getValue('PROGEO_SIMULATE_ON'));
            Configuration::updateValue('PROGEO_DIS_CURR', Tools::getValue('PROGEO_DIS_CURR'));
            Configuration::updateValue('PROGEO_DIS_LANG', Tools::getValue('PROGEO_DIS_LANG'));
            return $this->displayConfirmation($this->l('Settings updated'));
        }
    }

    public static function checkFreeGeoCountry()
    {
        $geolocation = self::returnUserCountry();
        if ($geolocation != false) {
            $visitor_country = Country::getByIso($geolocation);
            if ($visitor_country != false) {
                return $visitor_country;
            }
        }
        return false;
    }

    public static function returnUserCountry()
    {
        $record = false;
        if (!in_array($_SERVER['SERVER_NAME'], array('localhost', '127.0.0.1')) || Configuration::get('PROGEO_SIMULATE_ON') == true) {
            /* Check if Maxmind Database exists */
            if (@filemtime(_PS_GEOIP_DIR_ . _PS_GEOIP_CITY_FILE_)) {
                $reader = new GeoIp2\Database\Reader(_PS_GEOIP_DIR_ . _PS_GEOIP_CITY_FILE_);
                try {
                    $ip = Configuration::get('PROGEO_SIMULATE_IP');
                    $record = $reader->city((Configuration::get('PROGEO_SIMULATE_ON') ? (filter_var($ip, FILTER_VALIDATE_IP) ? $ip : Tools::getRemoteAddr()) : Tools::getRemoteAddr()));
                } catch (\GeoIp2\Exception\AddressNotFoundException $e) {
                    $record = null;
                }

                if (isset($record->country->isoCode)) {
                    return $record->country->isoCode;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function displayForm()
    {
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-wrench'
                ),
                'input' => array(
                    array(
                        'type' => (version_compare(_PS_VERSION_, '1.6') < 0) ? 'radio' : 'switch',
                        'class' => 't',
                        'label' => $this->l('Simulate visit'),
                        'name' => 'PROGEO_SIMULATE_ON',
                        'values' => array(
                            array(
                                'id' => 'FPROGEO_SIMULATE_ON_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'PROGEO_SIMULATE_ON_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Simulate IP'),
                        'name' => 'PROGEO_SIMULATE_IP',
                        'desc' => $this->l('If you enabled option to simulate visit - insert here the example of IP address that module will use for geolocation purposes'),
                    ),
                    array(
                        'type' => (version_compare(_PS_VERSION_, '1.6') < 0) ? 'radio' : 'switch',
                        'class' => 't',
                        'desc' => $this->l('Turn this option on if this geolocation module affected prestashop\'s currency conversion calculation'),
                        'label' => $this->l('Disable override of PS_CURRENCY_DEFAULT'),
                        'name' => 'PROGEO_DIS_CURR',
                        'values' => array(
                            array(
                                'id' => 'PROGEO_DIS_CURR_ON',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'PROGEO_DIS_CURR_OFF',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'type' => (version_compare(_PS_VERSION_, '1.6') < 0) ? 'radio' : 'switch',
                        'class' => 't',
                        'desc' => $this->l('Turn this option on if this geolocation module affected prestashop\'s front-office translations'),
                        'label' => $this->l('Disable override of PS_LANG_DEFAULT'),
                        'name' => 'PROGEO_DIS_LANG',
                        'values' => array(
                            array(
                                'id' => 'PROGEO_DIS_LANG',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'PROGEO_DIS_LANG',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                )
            ),
        );

        $helper = new HelperForm();
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();
        $helper->id = 'progeoID';
        $helper->identifier = 'progeo';
        $helper->submit_action = 'btnSubmit';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form));

    }

    public function getConfigFieldsValues()
    {
        return array(
            'PROGEO_SIMULATE_IP' => Tools::getValue('PROGEO_SIMULATE_IP', Configuration::get('PROGEO_SIMULATE_IP')),
            'PROGEO_SIMULATE_ON' => Tools::getValue('PROGEO_SIMULATE_ON', Configuration::get('PROGEO_SIMULATE_ON')),
            'PROGEO_DIS_CURR' => Tools::getValue('PROGEO_DIS_CURR', Configuration::get('PROGEO_DIS_CURR')),
            'PROGEO_DIS_LANG' => Tools::getValue('PROGEO_DIS_LANG', Configuration::get('PROGEO_DIS_LANG')),
        );
    }

    public function advert()
    {
        return $this->context->smarty->fetch(_PS_MODULE_DIR_ . $this->name . '/views/advert.tpl');
    }

    public function getContent()
    {
        if (in_array($_SERVER['SERVER_NAME'], array('localhost', '127.0.0.1'))) {
            $this->context->controller->errors[] = $this->l('You are on localhost, geolocation identifies your country only if your website is on-line.');
        }

        $this->_postProcess();
        if (@filemtime(_PS_GEOIP_DIR_ . _PS_GEOIP_CITY_FILE_) == false) {
            $this->context->controller->errors[] = $this->l('Module to identify customer country uses geolocation.') . ' ' . $this->l('In order to use Geolocation, please download') . ' ' . '<a href="https://mypresta.eu/prestashop-17/geolite2-city-geolocation-download.html">' . $this->l('this file') . '</a> ' . $this->l('and extract it (using Winrar or Gzip) into the /app/Resources/geoip/ directory.');
        }
        return $this->advert() . $this->displayForm() . $this->checkforupdates(0, true);
    }

    private function InDelMenu($what, $controller, $name = null, $parent = null)
    {
        if ($what == 'install') {
            if ($parent == null) {
                return;
            }
            $tab = new Tab();
            $tab->class_name = $controller;
            $tab->id_parent = Tab::getIdFromClassName($parent);
            $tab->module = $this->name;
            $languages = Language::getLanguages(false);
            foreach ($languages as $lang) {
                $tab->name[$lang['id_lang']] = $name;
            }
            if ($tab->save()) {
                return true;
            }
        } elseif ($what == 'uninstall') {
            $tab = new Tab(Tab::getIdFromClassName($controller));
            if ($tab->delete()) {
                return true;
            }
        }
        return false;
    }

    public function psversion($part = 1)
    {
        $version = _PS_VERSION_;
        $exp = $explode = explode(".", $version);
        if ($part == 1) {
            return $exp[1];
        }
        if ($part == 2) {
            return $exp[2];
        }
        if ($part == 3) {
            return $exp[3];
        }
    }
}

class progeoUpdate extends progeo
{
    public static function version($version)
    {
        $version = (int)str_replace(".", "", $version);
        if (strlen($version) == 3) {
            $version = (int)$version . "0";
        }
        if (strlen($version) == 2) {
            $version = (int)$version . "00";
        }
        if (strlen($version) == 1) {
            $version = (int)$version . "000";
        }
        if (strlen($version) == 0) {
            $version = (int)$version . "0000";
        }
        return (int)$version;
    }

    public static function encrypt($string)
    {
        return base64_encode($string);
    }

    public static function verify($module, $key, $version)
    {
        if (ini_get("allow_url_fopen")) {
            if (function_exists("file_get_contents")) {
                $actual_version = @file_get_contents('http://dev.mypresta.eu/update/get.php?module=' . $module . "&version=" . self::encrypt($version) . "&lic=$key&u=" . self::encrypt(_PS_BASE_URL_ . __PS_BASE_URI__));
            }
        }
        Configuration::updateValue("update_" . $module, date("U"));
        Configuration::updateValue("updatev_" . $module, $actual_version);
        return $actual_version;
    }
}

?>