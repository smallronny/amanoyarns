{*

 * 2008 - 2020 (c) Prestablog

 *

 * MODULE PrestaBlog

 *

 * @author    Prestablog

 * @copyright Copyright (c) permanent, Prestablog

 * @license   Commercial

 *}



<!-- Module Presta Blog -->

<section id="grid-all-blog" class="clearfix prestablog">
    
	{hook h='displayPrestaBlogList' id='3' mod='prestablog'}

</section>

<!-- /Module Presta Blog -->

