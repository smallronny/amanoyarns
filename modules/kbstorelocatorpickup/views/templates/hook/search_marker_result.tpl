{if !empty($available_store)}
    <div class="velo-popup">
        
        
                            
                            <table style="
    width: 100%;
">
                                <tr>
                                    <td style="padding-left:0px;">
                                        <div class="velo_add_name"> <span class="velo_store_icon" style="display:none;"><img class="velo-phone-icon" src="{$store_icon|escape:'htmlall':'UTF-8'}"/> </span>{$available_store['name']|escape:'htmlall':'UTF-8'}</div>
                            <div class="velo-add-address"> {$available_store['address1']|escape:'htmlall':'UTF-8'}
                            {if !empty($available_store['address2'])} 
                                <br/>{$available_store['address2']|escape:'htmlall':'UTF-8'}
                            {/if}
                            {if !empty($available_store['state'])} 
                                <br/>{$available_store['state']|escape:'htmlall':'UTF-8'}
                            {/if}
                            </div>
                                    </td>
                                    {if $is_enabled_store_image} 
                                    <td>
                                        <div id="kb-store-image">
                                        {if !empty($available_store['image'])}{$available_store['image'] nofilter}{*escape not required*}{/if}
                        </div>
                                        <div></div>
                                    </td>
                                    {/if}
                                </tr>
                            </table>

                            <div style="display: none" class="velo-w3-address-country">{$available_store['country']|escape:'htmlall':'UTF-8'}</div>
                            <div class="velo-show-more"><strong><a href="javascript:void(0);" id="button-show-more-{$available_store['id_store']}" class="button-show-more" >{l s='Show More' mod='kbstorelocatorpickup'}</a></strong></div>
                            <div class="extra_content" style="display:none;">
                        {if !empty($available_store['email'])}
                                <span class="velo_add_number"><i class="icon-envelope"></i> {$available_store['email']|escape:'htmlall':'UTF-8'}</span>
                        {/if}
                        {if $display_phone && !empty($available_store['phone'])}
                            <span class="velo_add_number"><i class="icon-phone"></i>{$available_store['phone']|escape:'htmlall':'UTF-8'}</span>
                            {/if}
                            {* changes by rishabh jain *}
                            {if $is_enabled_website_link}
                                <div class="kb-store-url">
                                    <span class="velo_add_number"><img class="velo-web-icon" src="{$web_icon|escape:'htmlall':'UTF-8'}"/> <a href='{$index_page_link}'>
                                        {$index_page_link}
                                    </a></span>

                                </div>
                            {/if}
                            </div>
                        {* changes over *}
                        <div class="velo_add_distance"></div>

                        
                    
 {*       <div class="gm_name">
            {$available_store['name']|escape:'htmlall':'UTF-8'}
        </div>
        <div class="gm_address">
            {$available_store['address1']|escape:'htmlall':'UTF-8'}<br/>
            {$available_store['address2']|escape:'htmlall':'UTF-8'}
        </div>
        <div class="gm_location">
            {if !empty($available_store['state'])}{$available_store['state']|escape:'htmlall':'UTF-8'},{/if}
            {$available_store['country']|escape:'htmlall':'UTF-8'}
        </div>
        {if $display_phone && !empty($available_store['phone'])}
            <div class="gm_phone">
                <img class="velo-phone-icon" src="{$phone_icon|escape:'quotes':'UTF-8'}"> {$available_store['phone']|escape:'htmlall':'UTF-8'}
            </div>
        {/if}
    {if !empty($available_store['image'])}{$available_store['image']|escape:'htmlall':'UTF-8'}{/if}
     {if !empty($available_store['hours'])}
        <div style="" class="kb-store-hours">
            {foreach $available_store['hours'] as $key => $time}
                {if isset($time['hours'])}
                    <span><strong>{$time['day']|escape:'htmlall':'UTF-8'}</strong>: {$time['hours']|escape:'htmlall':'UTF-8'}</span><br/>
                {/if}
            {/foreach}
        </div>
    {/if}*}
</div>
{/if}

{*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer tohttp://www.prestashop.com for more information.
* We offer the best and most useful modules PrestaShop and modifications for your online store.
*
* @category  PrestaShop Module
* @author    knowband.com <support@knowband.com>
* @copyright 2018 Knowband
* @license   see file: LICENSE.txt
*
* Description
*
* Admin tpl file
*}
