<div id="header_kb-header-link">
    <div class="kb-header-link">
        <a href="{$fc_link|escape:'quotes':'UTF-8'}" title="{$menu_text|escape:'htmlall':'UTF-8'}">
            <i class="material-icons">location_on</i> {$menu_text|escape:'htmlall':'UTF-8'}</a>

    </div>
</div>
<style>
    .kb-header-link {
        margin-left: 0.5rem;
        margin-top: .9375rem;
        text-align: right;
        white-space: nowrap;
        float: left;
    }
</style>
{*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer tohttp://www.prestashop.com for more information.
* We offer the best and most useful modules PrestaShop and modifications for your online store.
*
* @category  PrestaShop Module
* @author    knowband.com <support@knowband.com>
* @copyright 2017 knowband
* @license   see file: LICENSE.txt
*}