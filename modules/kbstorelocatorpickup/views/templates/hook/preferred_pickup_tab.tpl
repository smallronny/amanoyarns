<li>
    <a href = "#preferred_pickup" style="padding: 13px;">
        <i class = "icon-truck"></i>
        {l s = 'Pickup At Store' mod='kbstorelocatorpickup'} <span class = "badge"></span>
    </a>
</li>

{*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer tohttp://www.prestashop.com for more information.
* We offer the best and most useful modules PrestaShop and modifications for your online store.
*
* @category  PrestaShop Module
* @author    knowband.com <support@knowband.com>
* @copyright 2018 Knowband
* @license   see file: LICENSE.txt
*
* Description
*
* Admin tpl file
*}