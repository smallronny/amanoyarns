<!-- Content starts here -->

<table width="640" class="mlContentTable" cellspacing="0" cellpadding="0" border="0" align="center">
    <tbody><tr>
            <td height="30"></td>
        </tr>
    </tbody></table>
<table align="center" width="640" class="mlContentTable" cellpadding="0" cellspacing="0" border="0" style="min-width: 640px; width: 640px;">
    <tbody><tr>
            <td class="mlContentTable">

            </td>
        </tr>
    </tbody></table>

<table align="center" border="0" bgcolor="#FFFFFF" class="mlContentTable" cellspacing="0" cellpadding="0" style="background: #FFFFFF; min-width: 640px; width: 640px;" width="640" id="ml-block-55155243">
    <tbody>
        <tr>
            <td>
                <table width="640" class="mlContentTable" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0" align="center" style="background: #FFFFFF; width: 640px;">
                    <tbody>
                        <tr>
                            <td align="center" class="mlContentContainer mlContentImage mlContentHeight" style="padding: 15px 50px 15px 50px;"> 
                                <a title="[shop_name]" href="[shop_url]"><img border="0" src="{$shop_logo}{*escape not required as contains URL*}" alt='[shop_name]' width="175"  class="mlContentImage" style="display: block;max-width: 175px;"/></a>
                            </td>   
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table> 

<table align="center" border="0" bgcolor="#000000" class="mlContentTable" cellspacing="0" cellpadding="0" style="background: #000000; min-width: 640px; width: 640px;" width="640" id="ml-block-55155075">     <tbody><tr>         <td>  
                <table width="640" class="mlContentTable" bgcolor="#000000" cellspacing="0" cellpadding="0" border="0" align="center" style="background: #000000; width: 640px;"> 
                    <tbody>
                        <tr>
                            <td align="left" class="mlContentContainer" style="padding: 15px 50px 5px 50px; font-family: Helvetica; font-size: 14px; color: #7F8C8D; line-height: 23px;">    
                                <p style="margin: 0px 0px 10px 0px;        line-height: 23px;text-align: center;">
                                    <a href="[shop_url]" title="home" style="color: #FFFFFF;        text-decoration: underline;">
                                        Home
                                    </a> 
                                    | 
                                    <a href="[new_arrival_link]" style="color: #FFFFFF;        text-decoration: underline;">
                                        New Arrivals
                                    </a> | 
                                    <a href="[price_drop_link]" style="color: #FFFFFF;        text-decoration: underline;">
                                        Price Drop
                                    </a>
                                    | 
                                    <a href="[best_sale_link]" style="color: #FFFFFF;        text-decoration: underline;">
                                        Best Sales
                                    </a>
                                </p>          </td>
                        </tr>  </tbody></table>                             </td>     </tr> </tbody></table> 




<table align="center" border="0" bgcolor="#e2e2e2" class="mlContentTable" cellspacing="0" cellpadding="0" style="background: #e2e2e2; min-width: 640px; width: 640px;" width="640" id="ml-block-55155079">
    <tbody><tr>
            <td>
                <table width="640" class="mlContentTable" bgcolor="#e2e2e2" cellspacing="0" cellpadding="0" border="0" align="center" style="background: #e2e2e2; width: 640px;">

                    <tbody><tr>

                            <td align="center" class="mlContentContainer" style="padding: 25px 50px 5px 50px;">

                                <h1 style="margin: 0px; font-family: Helvetica; font-weight: bold; font-size: 27px; text-decoration: none; line-height: 40px; color: #404040;">

                                    Hi [first_name],


                                </h1>

                            </td>

                        </tr>

                    </tbody></table>

            </td>
        </tr>
    </tbody></table>





{*<table align="center" border="0" bgcolor="#e2e2e2" class="mlContentTable" cellspacing="0" cellpadding="0" style="background: #e2e2e2; min-width: 640px; width: 640px;" width="640" id="ml-block-55155719">  
<tbody><tr>         <td>  
<table width="640" class="mlContentTable" bgcolor="#e2e2e2" cellspacing="0" cellpadding="0" border="0" align="center" style="background: #e2e2e2; width: 640px;"> 
<tbody><tr>    
<td align="left" class="mlContentContainer" style="padding: 0px 50px 0px 50px; font-family: Helvetica; font-size: 14px; color: #404040; line-height: 23px;">       
<p style="margin: 0px 0px 10px 0px;        line-height: 23px;text-align: center;">
THANK YOU FOR SHOPPING WITH [shop_name]</p>     
</td>      </tr>  </tbody></table>             
</td>     </tr> </tbody></table> *}




<table align="center" border="0" bgcolor="#e2e2e2" class="mlContentTable" cellspacing="0" cellpadding="0" style="background: #e2e2e2; min-width: 640px; width: 640px;" width="640" id="ml-block-55155585">     <tbody><tr>         <td>               <table width="640" class="mlContentTable" bgcolor="#e2e2e2" cellspacing="0" cellpadding="0" border="0" align="center" style="background: #e2e2e2; width: 640px;">         <tbody><tr>     <td align="center" class="mlContentContainer mlContentImage mlContentHeight" style="padding: 15px 50px 15px 50px;">              
                                <img border="0" src="{$delivery_icon}{*escape not required as contains URL*}" width="120" height="120" class="mlContentImage" style="display: block;max-width: 120px;">    
                            </td>   </tr> </tbody></table>          </td>     </tr> </tbody></table> 




<table align="center" border="0" bgcolor="#e2e2e2" class="mlContentTable" cellspacing="0" cellpadding="0" style="background: #e2e2e2; min-width: 640px; width: 640px;" width="640" id="ml-block-55155679">     <tbody><tr>         <td>               <table width="640" class="mlContentTable" bgcolor="#e2e2e2" cellspacing="0" cellpadding="0" border="0" align="center" style="background: #e2e2e2; width: 640px;">      <tbody><tr>          <td align="left" class="mlContentContainer" style="padding: 15px 50px 8px 50px; font-family: Helvetica; font-size: 14px; color: #404040; line-height: 23px;">                           <div style="border: 1px solid #7F8C8D; border-radius: 10px; padding-left: 10px; padding-top: 5px; background: #fff;"> <p style="margin: 0px 0px 10px 0px;        line-height: 23px;"><strong>Pickup At Store</strong></p> <hr> 
                                    <p style="margin: 0px 0px 10px 0px;        line-height: 23px;">
                                        <strong>A customer has just placed an Order and selected Pickup as Store to pick up the package.
                                        </strong></p> <table> <tbody> <tr> <td>Preferred Pickup Date</td> 
                                                <td style="padding-left: 10px;">[preferred_date]</td> 
                                                {*                                                    <td style="padding-left: 10px;">[preferred_time]</td>*}
                                            </tr> </tbody> </table>
                                    <p style="margin: 0px 0px 10px 0px;        line-height: 23px;">
                                        &nbsp;
                                    </p> </div>          
                            </td>      </tr>  </tbody></table>                             </td>     </tr> </tbody></table> 




<table align="center" border="0" bgcolor="#e2e2e2" class="mlContentTable" cellspacing="0" cellpadding="0" style="background: #e2e2e2; min-width: 640px; width: 640px;" width="640" id="ml-block-55156813">     <tbody><tr>         <td>               
                <table width="640" class="mlContentTable" bgcolor="#e2e2e2" cellspacing="0" cellpadding="0" border="0" align="center" style="background: #e2e2e2; width: 640px;">      <tbody><tr>
                            <td align="left" class="mlContentContainer" style="padding: 30px 50px 5px 50px; font-family: Helvetica; font-size: 14px; color: #404040; line-height: 23px;">    
                                <div style="border: 1px solid #7F8C8D; border-radius: 10px; padding-left: 10px; padding-top: 5px; background: #fff;">
                                    <p style="margin: 0px 0px 10px 0px;        line-height: 23px;">
                                        <strong>ORDER DETAILS</strong></p> <hr>
                                    <p style="margin: 0px 0px 10px 0px;        line-height: 23px;">
                                        <strong>ORDER: </strong>[order_id] Placed on [order_time]</p> 
                                    <p style="margin: 0px 0px 10px 0px;        line-height: 23px;">
                                        <strong>Payment: </strong>[payment_method]</p> 
                                    <div>&nbsp;</div> </div>          </td> 
                        </tr>  </tbody></table>                             </td>
        </tr> </tbody></table> 




<table align="center" border="0" bgcolor="#e2e2e2" class="mlContentTable" cellspacing="0" cellpadding="0" style="background: #e2e2e2; min-width: 640px; width: 640px;" width="640" id="ml-block-55155073">     <tbody><tr>         <td>               <table width="640" class="mlContentTable" bgcolor="#e2e2e2" cellspacing="0" cellpadding="0" border="0" align="center" style="background: #e2e2e2; width: 640px;">      <tbody><tr>          <td align="left" class="mlContentContainer" style="padding: 15px 50px 5px 50px; font-family: Helvetica; font-size: 14px; color: #404040; line-height: 23px;">                  
                                <p style="margin: 0px 0px 10px 0px;        line-height: 23px;text-align: center;">
                                    <a href='[shop_url]' title="[shop_name]">[shop_name]</a>
                                </p>       
                                <p style="margin: 0px 0px 10px 0px;        line-height: 23px;text-align: center;">
                                    If you have questions, comments or concerns, please contact our expert customer support team.
                                </p>
                            </td>      </tr>  </tbody></table>                             </td>     </tr> </tbody>
</table> 
<table width="640" class="mlContentTable" cellspacing="0" cellpadding="0" border="0" align="center" style="min-width: 640px; width: 640px;">
    <tbody><tr>
            <td height="30"></td>
        </tr>
    </tbody></table>
{*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
* We offer the best and most useful modules PrestaShop and modifications for your online store.
*
* @category  PrestaShop Module
* @author    knowband.com <support@knowband.com>
* @copyright 2018 Velocity Software Solutions Pvt Ltd
* @license   see file: LICENSE.txt
*
* Description
*
* Product Update Block Page
*}