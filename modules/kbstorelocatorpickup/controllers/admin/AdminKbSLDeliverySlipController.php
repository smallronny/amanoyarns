<?php
/**
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future.If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 * We offer the best and most useful modules PrestaShop and modifications for your online store.
 *
 * @author    knowband.com <support@knowband.com>
 * @copyright 2018 knowband
 * @license   see file: LICENSE.txt
 * @category  PrestaShop Module
 */

//include_once(_PS_MODULE_DIR_.'kbproductfield/classes/KbProductCustomFields.php');

class AdminKbSLDeliverySlipController extends ModuleAdminController
{
    
    public function __construct()
    {
        $this->context = Context::getContext();
        $this->bootstrap = true;
//        $this->className = 'Configuration';
        $this->table = 'delivery';

        parent::__construct();
        $this->toolbar_title = $this->module->l('Download Delivery Slip', 'AdminKbSLDeliverySlipController');
    }
    
    public function postProcess()
    {
        if (Tools::isSubmit('submitAdd'.$this->table)) {
            $date_from = trim(Tools::getValue('date_from'));
            $date_to = trim(Tools::getValue('date_to'));
            $delivery_slip_carrier = Tools::getValue('delivery_slip_carrier');
            $delivery_slip_store = Tools::getValue('delivery_slip_store');
            $pickup_carrier_id = Tools::getValue('pickup_carrier_id');
            if (!Validate::isDate($date_from)) {
                $this->errors[] = $this->module->l('Invalid \'from\' date');
            }
            if (!Validate::isDate($date_to)) {
                $this->errors[] = $this->module->l('Invalid \'to\' date');
            }
            if (!count($this->errors)) {
                $slip_data = $this->getKbDeliverySlipData($date_from, $date_to, $delivery_slip_carrier, $delivery_slip_store, $pickup_carrier_id);
                if (count($slip_data)) {
                    $this->generateKbDeliverySlip($slip_data);
                    Tools::redirectAdmin($this->context->link->getAdminLink('AdminKbSLDeliverySlip', true));
                } else {
                    $this->context->cookie->__set('kb_redirect_error', $this->module->l('No delivery slip was found for this period.', 'AdminKbSLDeliverySlipController'));
                    Tools::redirectAdmin($this->context->link->getAdminLink('AdminKbSLDeliverySlip', true));
                }
            }
        } else {
            parent::postProcess();
        }
    }
    
    /*
     * function to render the PDF file
     */
    public function generateKbDeliverySlip($slip_data)
    {
        if (!count($slip_data)) {
            die($this->module->l('No invoice was found.', 'AdminKbSLDeliverySlipController'));
        }
        $pdf = new PDF($slip_data, PDF::TEMPLATE_DELIVERY_SLIP, Context::getContext()->smarty);
        $pdf->render();
    }
    
    /*
     * function to get the delivery slip data
     */
    public function getKbDeliverySlipData($date_from, $date_to, $carrier = null, $store = null, $pickup_carrier = null)
    {
        $str = '';
        if (!empty($carrier)) {
            $str .= ' AND o.id_carrier='.(int)$carrier;
            if (!empty($store) && ($carrier == $pickup_carrier) && !empty($pickup_carrier)) {
                $id_address_delivery = Db::getInstance()->getValue('SELECT id_address FROM '._DB_PREFIX_.'kb_pickup_store_address_mapping Where id_store='.(int)$store);
                $str .= ' AND o.id_address_delivery ='. (int)$id_address_delivery;
            }
        }
        
        $order_invoice_list = Db::getInstance()->executeS('
                SELECT oi.*
                FROM `' . _DB_PREFIX_ . 'order_invoice` oi
                LEFT JOIN `' . _DB_PREFIX_ . 'orders` o ON (o.`id_order` = oi.`id_order`)
                WHERE DATE_ADD(oi.delivery_date, INTERVAL -1 DAY) <= \'' . pSQL($date_to) . '\'
                AND oi.delivery_date >= \'' . pSQL($date_from) . '\' 
                ' . Shop::addSqlRestriction(Shop::SHARE_ORDER, 'o') . $str.'
                ORDER BY oi.delivery_date ASC
        ');
        
        return ObjectModel::hydrateCollection('OrderInvoice', $order_invoice_list);
    }
    
    /**
     * Prestashop Default Function in AdminController.
     * Assign smarty variables for all default views, list and form, then call other init functions
     */
    public function initContent()
    {
        if (isset($this->context->cookie->kb_redirect_error)) {
            $this->errors[] = $this->context->cookie->kb_redirect_error;
            unset($this->context->cookie->kb_redirect_error);
        }

        if (isset($this->context->cookie->kb_redirect_success)) {
            $this->confirmations[] = $this->context->cookie->kb_redirect_success;
            unset($this->context->cookie->kb_redirect_success);
        }
        
        $this->context->smarty->assign('selected_nav', 'sldeliveryslip');
        $this->context->smarty->assign(
            'admin_sl_setting_controller',
            $this->context->link->getAdminLink('AdminModules', true).'&configure='.urlencode($this->module->name).'&tab_module='.$this->module->tab.'&module_name='.urlencode($this->module->name)
        );
        $this->context->smarty->assign(
            'admin_sl_delivery_slip_controller',
            $this->context->link->getAdminLink('AdminKbSLDeliverySlip', true)
        );
        $this->context->smarty->assign(
            'admin_sl_email_controller',
            $this->context->link->getAdminLink('AdminKbSLEmailNotification', true)
        );
        $this->context->smarty->assign(
            'admin_sl_pickup_time_controller',
            $this->context->link->getAdminLink('AdminKbSLTimeSetting', true)
        );
        $this->context->smarty->assign(
            'admin_sl_importer_controller',
            $this->context->link->getAdminLink('AdminKbSLImporter', true)
        );
        $kb_tabs = $this->context->smarty->fetch(
            _PS_MODULE_DIR_.$this->module->name.'/views/templates/admin/kb_tabs.tpl'
        );
        $this->content .= $kb_tabs;
        
        $this->content .= $this->kbSLDeliverySettingForm();
        
        
        parent::initContent();
    }
    
    /*
     * function to create the delivery slip form
     */
    protected function kbSLDeliverySettingForm()
    {
        $carrier = array();
        $store = array();
        $carrier = Carrier::getCarriers($this->context->language->id, true);
        array_unshift($carrier, array('id_carrier' => null, 'name' => $this->module->l('All Carrier', 'AdminKbSLDeliverySlipController')));
        if (version_compare(_PS_VERSION_, '1.7.4.0', '>')) {
//            changes by tarun
            $store = Store::getStores($this->context->language->id);
        } else {
            $store = Store::getStores($this->context->language->id);
        }
        array_unshift($store, array('id_store' => null, 'name' => $this->module->l('All Store', 'AdminKbSLDeliverySlipController')));
        
        $formFields = array(
            array(
                'type' => 'text',
                'label' => $this->module->l('From', 'AdminKbSLDeliverySlipController'),
                'required' => true,
                'name' => 'date_from',
                'hint' => $this->module->l('Enter the from date(inclusive).', 'AdminKbSLDeliverySlipController'),
                'col' => 3,
                'suffix' => '<i class="icon-calendar-empty"></i>'
            ),
            array(
                'type' => 'text',
                'label' => $this->module->l('To'),
                'required' => true,
                'name' => 'date_to',
                'hint' => $this->module->l('Enter the To date(inclusive).', 'AdminKbSLDeliverySlipController'),
                'col' => 3,
                'suffix' => '<i class="icon-calendar-empty"></i>'
            ),
            array(
                'type' => 'select',
                'label' => $this->module->l('Carrier', 'AdminKbSLDeliverySlipController'),
                'name' => 'delivery_slip_carrier',
                'required' => true,
                'hint' => $this->module->l('Select the carrier to filter delivery slip', 'AdminKbSLDeliverySlipController'),
                'options' => array(
                    'query' => $carrier,
                    'id' => 'id_carrier',
                    'name' => 'name',
                ),
            ),
            array(
                'type' => 'select',
                'label' => $this->module->l('Store', 'AdminKbSLDeliverySlipController'),
                'name' => 'delivery_slip_store',
                'required' => true,
                'hint' => $this->module->l('Select the store to filter delivery slip', 'AdminKbSLDeliverySlipController'),
                'options' => array(
                    'query' => $store,
                    'id' => 'id_store',
                    'name' => 'name',
                ),
            ),
            array(
                'type' => 'hidden',
                'name' => 'pickup_carrier_id',
            )
        );

        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->module->l('Download Delivery Slip', 'AdminKbSLDeliverySlipController'),
                    'icon' => 'icon-download'
                ),
                'input' => $formFields,
                'submit' => array(
                    'class' => 'btn btn-default pull-right',
                    'title' => $this->module->l('Generate PDF file', 'AdminKbSLDeliverySlipController'),
                    'icon' => 'process-icon-download-alt'
                )
            )
        );

        $helper = new HelperForm();
         $helper->submit_action = 'submitAdd' . $this->table;
        $helper->table = $this->table;
        $helper->show_toolbar = false;
        $helper->default_form_language = $this->context->language->id;
        $helper->fields_value['date_from'] = date('Y-m-d');
        $helper->fields_value['date_to'] = date('Y-m-d');
        $helper->fields_value['pickup_carrier_id'] =  Configuration::get('KB_PICKUP_AT_STORE_SHIPPING');
        $helper->fields_value['delivery_slip_carrier'] = '';
        $helper->fields_value['delivery_slip_store'] = '';

        return $helper->generateForm(array($fields_form));
    }
    
    
    /**
     * Prestashop Default Function in AdminController.
     * Init context and dependencies, handles POST and GET
     */
    public function init()
    {
       
        parent::init();
    }
    
    /**
     * assign default action in toolbar_btn smarty var, if they are not set.
     * uses override to specifically add, modify or remove items
     *
     */
    public function initToolbar()
    {
        parent::initToolbar();
    }
    
    /*
     * Function for returning the absolute path of the module directory
     */
    protected function getKbModuleDir()
    {
        return _PS_MODULE_DIR_.$this->module->name.'/';
    }
    
    /*
     * Default function, used here to include JS/CSS files for the module.
     */
    public function setMedia($isNewTheme = false)
    {
        parent::setMedia($isNewTheme);
        $this->context->controller->addCSS($this->getKbModuleDir() . 'views/css/admin/kb_admin.css');
        $this->context->controller->addJS($this->getKbModuleDir() . 'views/js/admin/kbcustomfield_admin.js');
        $this->context->controller->addJS($this->getKbModuleDir() . 'views/js/velovalidation.js');
        $this->context->controller->addJS($this->getKbModuleDir() . 'views/js/admin/validation_admin.js');
        $this->context->controller->addJS($this->getKbModuleDir() . 'views/js/admin/admin.js');
    }
    
    /**
     * Function used display toolbar in page header
     */
    public function initPageHeaderToolbar()
    {
        parent::initPageHeaderToolbar();
    }
    
    /*
     * Function for returning the URL of PrestaShop Root Modules Directory
     */
    protected function getModuleDirUrl()
    {
        $module_dir = '';
        if ($this->checkSecureUrl()) {
            $module_dir = _PS_BASE_URL_SSL_ . __PS_BASE_URI__ . str_replace(_PS_ROOT_DIR_ . '/', '', _PS_MODULE_DIR_);
        } else {
            $module_dir = _PS_BASE_URL_ . __PS_BASE_URI__ . str_replace(_PS_ROOT_DIR_ . '/', '', _PS_MODULE_DIR_);
        }
        return $module_dir;
    }
    
    /*
     * Function for checking SSL
     */
    private function checkSecureUrl()
    {
        $custom_ssl_var = 0;
        if (isset($_SERVER['HTTPS'])) {
            if ($_SERVER['HTTPS'] == 'on') {
                $custom_ssl_var = 1;
            }
        } else if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
            $custom_ssl_var = 1;
        }
        if ((bool) Configuration::get('PS_SSL_ENABLED') && $custom_ssl_var == 1) {
            return true;
        } else {
            return false;
        }
    }
    
    /*
     * Function for returning all the languages in the system
     */
    public function getAllLanguages()
    {
        return Language::getLanguages(false);
    }
}
