<?php
/**
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future.If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 * We offer the best and most useful modules PrestaShop and modifications for your online store.
 *
 * @author    knowband.com <support@knowband.com>
 * @copyright 2018 knowband
 * @license   see file: LICENSE.txt
 * @category  PrestaShop Module
 */

//include_once(_PS_MODULE_DIR_.'kbproductfield/classes/KbProductCustomFields.php');

class AdminKbSLTimeSettingController extends ModuleAdminController
{
    
    public function __construct()
    {
        $this->context = Context::getContext();
        $this->bootstrap = true;
        $this->className = 'Configuration';
        $this->table = 'configuration';

        parent::__construct();
        $this->toolbar_title = $this->module->l('Pickup Time Setting', 'AdminKbSLTimeSettingController');
    }
    
    /*
     * Function for returning the URL of PrestaShop Root Modules Directory
     */
    protected function getModuleDirUrl()
    {
        $module_dir = '';
        if ($this->checkSecureUrl()) {
            $module_dir = _PS_BASE_URL_SSL_ . __PS_BASE_URI__ . str_replace(_PS_ROOT_DIR_ . '/', '', _PS_MODULE_DIR_);
        } else {
            $module_dir = _PS_BASE_URL_ . __PS_BASE_URI__ . str_replace(_PS_ROOT_DIR_ . '/', '', _PS_MODULE_DIR_);
        }
        return $module_dir;
    }
    
    /*
     * Function for checking SSL
     */
    private function checkSecureUrl()
    {
        $custom_ssl_var = 0;
        if (isset($_SERVER['HTTPS'])) {
            if ($_SERVER['HTTPS'] == 'on') {
                $custom_ssl_var = 1;
            }
        } else if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
            $custom_ssl_var = 1;
        }
        if ((bool) Configuration::get('PS_SSL_ENABLED') && $custom_ssl_var == 1) {
            return true;
        } else {
            return false;
        }
    }
    
    /*
     * Function for returning all the languages in the system
     */
    public function getAllLanguages()
    {
        return Language::getLanguages(false);
    }
    
    /**
     * Prestashop Default Function in AdminController.
     * Assign smarty variables for all default views, list and form, then call other init functions
     */
    public function initContent()
    {
        if (isset($this->context->cookie->kb_redirect_error)) {
            $this->errors[] = $this->context->cookie->kb_redirect_error;
            unset($this->context->cookie->kb_redirect_error);
        }

        if (isset($this->context->cookie->kb_redirect_success)) {
            $this->confirmations[] = $this->context->cookie->kb_redirect_success;
            unset($this->context->cookie->kb_redirect_success);
        }
        
        $this->context->smarty->assign('selected_nav', 'slpickupsetting');
        $this->context->smarty->assign(
            'admin_sl_setting_controller',
            $this->context->link->getAdminLink('AdminModules', true)
            .'&configure='.urlencode($this->module->name)
            .'&tab_module='.$this->module->tab
            .'&module_name='.urlencode($this->module->name)
        );
        $this->context->smarty->assign(
            'admin_sl_delivery_slip_controller',
            $this->context->link->getAdminLink('AdminKbSLDeliverySlip', true)
        );
        $this->context->smarty->assign(
            'admin_sl_email_controller',
            $this->context->link->getAdminLink('AdminKbSLEmailNotification', true)
        );
        $this->context->smarty->assign(
            'admin_sl_pickup_time_controller',
            $this->context->link->getAdminLink('AdminKbSLTimeSetting', true)
        );
        $this->context->smarty->assign(
            'admin_sl_importer_controller',
            $this->context->link->getAdminLink('AdminKbSLImporter', true)
        );
        
        $kb_tabs = $this->context->smarty->fetch(
            _PS_MODULE_DIR_.$this->module->name.'/views/templates/admin/kb_tabs.tpl'
        );
        $this->content .= $kb_tabs;
        
        $this->content .= $this->kbPickupSettingForm();
        
        
        parent::initContent();
    }
    
    /*
     * function to create pickup setting form
     */
    protected function kbPickupSettingForm()
    {
        $formFields = array(
            
        //code added to display the date selection button
            
            array(
                'type' => 'switch',
                'label' => $this->module->l('Enable pickup date selection', 'AdminKbSLTimeSettingController'),
                'name' => 'kbslpickuptime[enable_date_selection]',
                'hint' => $this->module->l('Enable to display Pickup date calender to pickup the package', 'AdminKbSLTimeSettingController'),
                'values' => array(
                    array(
                        'value' => 1,
                    ),
                    array(
                        'value' => 0,
                    ),
                ),
            ),
            // changes over
            array(
                'type' => 'text',
                'label' => $this->module->l('Pickup Days Gap', 'AdminKbSLTimeSettingController'),
                'name' => 'kbslpickuptime[days_gap]',
                'required' => true,
                'col' => 3,
                'hint' => $this->module->l('Minimum days after placing the order to wait before pickup the package', 'AdminKbSLTimeSettingController'),
            ),
            array(
                'type' => 'text',
                'label' => $this->module->l('Maximum Pickup days', 'AdminKbSLTimeSettingController'),
                'name' => 'kbslpickuptime[maximum_days]',
                'required' => true,
                'col' => 3,
                'hint' => $this->module->l('Maximum Pickup days allowed to pickup the package', 'AdminKbSLTimeSettingController'),
            ),
            array(
                'type' => 'text',
                'label' => $this->module->l('Date/Time format', 'AdminKbSLTimeSettingController'),
                'name' => 'kbslpickuptime[format]',
//                'lang' => true,
                'required' => true,
                'col' => 3,
                'desc' => $this->module->l('Default:', 'AdminKbSLTimeSettingController').' %y-%m-%d %t24'
                .'<br>'.$this->module->l('For Example:', 'AdminKbSLTimeSettingController').' 2018-03-18 15:30'.'<br><br>'
                .'%y - '.$this->module->l('Year', 'AdminKbSLTimeSettingController').'<br>'
                .'%m -'.$this->module->l('Month', 'AdminKbSLTimeSettingController').'<br>'
                .'%d - '.$this->module->l('Day', 'AdminKbSLTimeSettingController').'<br>'
                .'%t24 -'. $this->module->l('Time, 24 hour format', 'AdminKbSLTimeSettingController').'<br>'
                .'%t12 -'.$this->module->l('Time, 12 hour format', 'AdminKbSLTimeSettingController').'<br>'.$this->module->l('Do not use other charaters and symbols.', 'AdminKbSLTimeSettingController'),
                'hint' => $this->module->l('Enter the date/time format to display in order', 'AdminKbSLTimeSettingController'),
            ),
            array(
                'type' => 'switch',
                'label' => $this->module->l('Allow Pickup Time Slot', 'AdminKbSLTimeSettingController'),
                'name' => 'kbslpickuptime[time_slot]',
                'hint' => $this->module->l('Enable to display Pickup Time slot to pickup the package', 'AdminKbSLTimeSettingController'),
                'values' => array(
                    array(
                        'value' => 1,
                    ),
                    array(
                        'value' => 0,
                    ),
                ),
            ),
            array(
                'type' => 'text',
                'label' => $this->module->l('Pickup Hours Gap', 'AdminKbSLTimeSettingController'),
                'name' => 'kbslpickuptime[hours_gap]',
                'col' => 3,
                'suffix' => $this->module->l('hours', 'AdminKbSLTimeSettingController'),
                'required' => true,
                'hint' => $this->module->l('Minimum hours customer need to wait befor pickup the package after placing an order', 'AdminKbSLTimeSettingController'),
            ),
        );

        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->module->l('Pickup Time Setting', 'AdminKbSLTimeSettingController'),
                    'icon' => 'icon-clock'
                ),
                'input' => $formFields,
                'submit' => array(
                    'class' => 'btn btn-default pull-right',
                    'title' => $this->module->l('Save', 'AdminKbSLTimeSettingController')
                )
            )
        );
        
        $pickupSetting = Tools::jsonDecode(Configuration::get('KB_PICKUP_TIME_SETTINGS'), true);
        /*
         * loop to fetch all language with default language in an array
         */
        $languages = Language::getLanguages(false);
        foreach ($languages as $k => $language) {
            $languages[$k]['is_default'] = ((int) ($language['id_lang'] == $this->context->language->id));
        }
        
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->submit_action = 'submitpickuptimesetting';
        $helper->languages = $languages;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG')
            ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->default_form_language = $this->context->language->id;
//        $helper->fields_value['kbslpickuptime[enable]'] = (!empty($pickupSetting)) ? $pickupSetting['enable'] : '';
        $helper->fields_value['kbslpickuptime[days_gap]'] = (!empty($pickupSetting)) ? $pickupSetting['days_gap'] : 0;
        $helper->fields_value['kbslpickuptime[maximum_days]'] = (!empty($pickupSetting)) ? $pickupSetting['maximum_days'] : 15;
        $helper->fields_value['kbslpickuptime[format]'] = (!empty($pickupSetting)) ? $pickupSetting['format'] : '%y-%m-%d %t24';
        $helper->fields_value['kbslpickuptime[time_slot]'] = (!empty($pickupSetting)) ? $pickupSetting['time_slot'] : '';
        $helper->fields_value['kbslpickuptime[hours_gap]'] = (!empty($pickupSetting)) ? $pickupSetting['hours_gap'] : 0;
        // changes by rishabh jain
        if (isset($pickupSetting['enable_date_selection'])) {
            $helper->fields_value['kbslpickuptime[enable_date_selection]'] = (!empty($pickupSetting)) ? $pickupSetting['enable_date_selection'] : 0;
        } else {
            $helper->fields_value['kbslpickuptime[enable_date_selection]'] = 0;
        }

        return $helper->generateForm(array($fields_form));
    }
    
    /*
     * function to validate the date in given format
     */
    protected function validateDate($date, $format = 'Y-m-d H:i:s')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    public function postProcess()
    {
        if (Tools::isSubmit('validateAdminDateFormart')) {
            $format = trim(Tools::getValue('dateformat'));
            if ($format) {
                $date = date($format);
                if ($this->validateDate($date, $format)) {
                    echo '1';
                }
            }
            die;
        }
        
        if (Tools::isSubmit('submitpickuptimesetting')) {
            $kbslpickuptime = Tools::getValue('kbslpickuptime');
            $kbslpickuptime['days_gap'] = trim($kbslpickuptime['days_gap']);
            $kbslpickuptime['maximum_days'] = trim($kbslpickuptime['maximum_days']);
            $kbslpickuptime['hours_gap'] = trim($kbslpickuptime['hours_gap']);
            $kbslpickuptime['format'] = trim($kbslpickuptime['format']);
            Configuration::updateValue('KB_PICKUP_TIME_SETTINGS', Tools::jsonEncode($kbslpickuptime));
            $this->context->cookie->__set('kb_redirect_success', $this->l('Pickup Time Setting successfully updated.', 'AdminKbSLTimeSettingController'));
            Tools::redirectAdmin($this->context->link->getAdminLink('AdminKbSLTimeSetting'));
        }
        return parent::postProcess();
    }
    
    /**
     * Prestashop Default Function in AdminController.
     * Init context and dependencies, handles POST and GET
     */
    public function init()
    {
       
        parent::init();
    }
    
    /**
     * assign default action in toolbar_btn smarty var, if they are not set.
     * uses override to specifically add, modify or remove items
     *
     */
    public function initToolbar()
    {
        parent::initToolbar();
    }
    
    /*
     * Function for returning the absolute path of the module directory
     */
    protected function getKbModuleDir()
    {
        return _PS_MODULE_DIR_.$this->module->name.'/';
    }
    
    /*
     * Default function, used here to include JS/CSS files for the module.
     */
    public function setMedia($isNewTheme = false)
    {
        parent::setMedia($isNewTheme);
        $this->context->controller->addCSS($this->getKbModuleDir() . 'views/css/admin/kb_admin.css');
        $this->context->controller->addJS($this->getKbModuleDir() . 'views/js/admin/kbcustomfield_admin.js');
        $this->context->controller->addJS($this->getKbModuleDir() . 'views/js/velovalidation.js');
        $this->context->controller->addJS($this->getKbModuleDir() . 'views/js/admin/validation_admin.js');
        $this->context->controller->addJS($this->getKbModuleDir() . 'views/js/admin/admin.js');
    }
    
    /**
     * Function used display toolbar in page header
     */
    public function initPageHeaderToolbar()
    {
        parent::initPageHeaderToolbar();
    }
}
