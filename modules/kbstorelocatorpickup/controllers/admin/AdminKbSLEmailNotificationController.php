<?php
/**
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future.If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 * We offer the best and most useful modules PrestaShop and modifications for your online store.
 *
 * @author    knowband.com <support@knowband.com>
 * @copyright 2018 knowband
 * @license   see file: LICENSE.txt
 * @category  PrestaShop Module
 */

//include_once(_PS_MODULE_DIR_.'kbproductfield/classes/KbProductCustomFields.php');

class AdminKbSLEmailNotificationController extends ModuleAdminController
{
    public function __construct()
    {
        $this->context = Context::getContext();
        $this->bootstrap = true;
        $this->className = 'Configuration';
        $this->table = 'kb_pickup_at_store_email_template';

        parent::__construct();
        $this->lang  = true;
        $this->toolbar_title = $this->module->l('Email Notification', 'AdminKbSLEmailNotificationController');
    }
    
    public function postProcess()
    {
        parent::postProcess();
        if (Tools::isSubmit('submitAdd' . $this->table)) {
            $language = Language::getLanguages(false);
            foreach ($language as $lang) {
                $subject = trim(Tools::getValue('subject_' . $lang['id_lang']));
                $body = trim(Tools::getValue('email_template_' . $lang['id_lang']));
                Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . pSQL($this->table) . ' set subject="' . pSQL($subject) . '", body="' . pSQL(Tools::htmlentitiesUTF8($body)) . '" where id_lang=' . (int) $lang['id_lang']);
            }
            Configuration::updateValue('kb_pickup_email_enable', Tools::getValue('enable'));
            $this->context->cookie->__set(
                'kb_redirect_success',
                $this->module->l('Email Template setting successfully saved.', 'AdminKbSLEmailNotificationController')
            );
            $this->redirect_after = self::$currentIndex . '&token=' . $this->token;
        }
    }
    
    /*
     * Function for returning the URL of PrestaShop Root Modules Directory
     */
    protected function getModuleDirUrl()
    {
        $module_dir = '';
        if ($this->checkSecureUrl()) {
            $module_dir = _PS_BASE_URL_SSL_ . __PS_BASE_URI__ . str_replace(_PS_ROOT_DIR_ . '/', '', _PS_MODULE_DIR_);
        } else {
            $module_dir = _PS_BASE_URL_ . __PS_BASE_URI__ . str_replace(_PS_ROOT_DIR_ . '/', '', _PS_MODULE_DIR_);
        }
        return $module_dir;
    }
    
    /*
     * Function for checking SSL
     */
    private function checkSecureUrl()
    {
        $custom_ssl_var = 0;
        if (isset($_SERVER['HTTPS'])) {
            if ($_SERVER['HTTPS'] == 'on') {
                $custom_ssl_var = 1;
            }
        } else if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
            $custom_ssl_var = 1;
        }
        if ((bool) Configuration::get('PS_SSL_ENABLED') && $custom_ssl_var == 1) {
            return true;
        } else {
            return false;
        }
    }
    
    /*
     * Function for returning all the languages in the system
     */
    public function getAllLanguages()
    {
        return Language::getLanguages(false);
    }
    
    /**
     * Prestashop Default Function in AdminController.
     * Assign smarty variables for all default views, list and form, then call other init functions
     */
    public function initContent()
    {
        if (isset($this->context->cookie->kb_redirect_error)) {
            $this->errors[] = $this->context->cookie->kb_redirect_error;
            unset($this->context->cookie->kb_redirect_error);
        }

        if (isset($this->context->cookie->kb_redirect_success)) {
            $this->confirmations[] = $this->context->cookie->kb_redirect_success;
            unset($this->context->cookie->kb_redirect_success);
        }
        
        $this->context->smarty->assign('selected_nav', 'slemailnotification');
        $this->context->smarty->assign(
            'admin_sl_setting_controller',
            $this->context->link->getAdminLink('AdminModules', true)
            .'&configure='.urlencode($this->module->name)
            .'&tab_module='.$this->module->tab
            .'&module_name='.urlencode($this->module->name)
        );
        $this->context->smarty->assign(
            'admin_sl_delivery_slip_controller',
            $this->context->link->getAdminLink('AdminKbSLDeliverySlip', true)
        );
        $this->context->smarty->assign(
            'admin_sl_email_controller',
            $this->context->link->getAdminLink('AdminKbSLEmailNotification', true)
        );
        $this->context->smarty->assign(
            'admin_sl_pickup_time_controller',
            $this->context->link->getAdminLink('AdminKbSLTimeSetting', true)
        );
        $this->context->smarty->assign(
            'admin_sl_importer_controller',
            $this->context->link->getAdminLink('AdminKbSLImporter', true)
        );
        $kb_tabs = $this->context->smarty->fetch(
            _PS_MODULE_DIR_.$this->module->name.'/views/templates/admin/kb_tabs.tpl'
        );
        $this->content .= $kb_tabs;
        
        $this->content .= $this->kbPickupEmailSettingForm();
        
        
        parent::initContent();
    }
    
    /*
     * function to create the email setting form
     */
    protected function kbPickupEmailSettingForm()
    {
        $content_warning = $this->module->l('Keywords like {{sample}} will be replace by dynamic content at the time of execution. Please do not remove these type of words from template, otherwise proper information will not be send in email to seller as well you. You can only change the position of these keywords in the template.', 'AdminKbSLEmailNotificationController');
        $formFields = array(
            array(
                'type' => 'switch',
                'label' => $this->module->l('Enable Email Notification', 'AdminKbSLEmailNotificationController'),
                'required' => false,
                'name' => 'enable',
                'hint' => $this->module->l('Enable this setting to notify pickup store for the orders', 'AdminKbSLEmailNotificationController'),
                'values' => array(
                    array(
                        'value' => 1,
                    ),
                    array(
                        'value' => 0,
                    ),
                ),
            ),
            array(
                'type' => 'text',
                'label' => $this->module->l('Subject', 'AdminKbSLEmailNotificationController'),
                'name' => 'subject',
                'lang' => true,
                'required' => true,
                'hint' => $this->module->l('Enter the Subject for the Email Notification', 'AdminKbSLEmailNotificationController'),
            ),
            array(
                'type' => 'textarea',
                'label' => $this->module->l('Email Template', 'AdminKbSLEmailNotificationController'),
                'name' => 'email_template',
                'autoload_rte' => true,
                'required' => true,
                'desc' => $content_warning,
                'lang' => true,
            ),
        );

        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->module->l('Email Notification', 'AdminKbSLEmailNotificationController'),
                    'icon' => 'icon-envelope'
                ),
                'input' => $formFields,
                'submit' => array(
                    'class' => 'btn btn-default pull-right',
                    'title' => $this->module->l('Save', 'AdminKbSLEmailNotificationController')
                )
            )
        );
        $languages = Language::getLanguages();
        foreach ($languages as $k => $language) {
            $languages[$k]['is_default'] = ((int) ($language['id_lang'] == $this->context->language->id));
        }
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->languages = $languages;
        $helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ?
                Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $language = Language::getLanguages(false);

        $query = Db::getInstance()->executeS('SELECT * FROM ' . _DB_PREFIX_ . 'kb_pickup_at_store_email_template');
        $mail_content = array();
        if (!empty($query)) {
            foreach ($query as $content) {
                $mail_content[$content['id_lang']] = $content;
            }
        }

        $helper->fields_value['enable'] = Configuration::get('kb_pickup_email_enable', 0);
        foreach ($language as $lang) {
            $helper->fields_value['subject'][$lang['id_lang']] = (!empty($mail_content) && isset($mail_content[$lang['id_lang']])) ? $mail_content[$lang['id_lang']]['subject'] : '';
            $helper->fields_value['email_template'][$lang['id_lang']] = (!empty($mail_content) && isset($mail_content[$lang['id_lang']])) ? Tools::htmlentitiesDecodeUTF8($mail_content[$lang['id_lang']]['body']) : '';
        }

        return $helper->generateForm(array($fields_form));
    }
    
    /**
     * assign default action in toolbar_btn smarty var, if they are not set.
     * uses override to specifically add, modify or remove items
     *
     */
    public function initToolbar()
    {
        parent::initToolbar();
    }
    
    /*
     * Function for returning the absolute path of the module directory
     */
    protected function getKbModuleDir()
    {
        return _PS_MODULE_DIR_.$this->module->name.'/';
    }
    
    /*
     * Default function, used here to include JS/CSS files for the module.
     */
    public function setMedia($isNewTheme = false)
    {
        parent::setMedia($isNewTheme);
        $this->context->controller->addCSS($this->getKbModuleDir() . 'views/css/admin/kb_admin.css');
        $this->context->controller->addJS($this->getKbModuleDir() . 'views/js/admin/kbcustomfield_admin.js');
        $this->context->controller->addJS($this->getKbModuleDir() . 'views/js/velovalidation.js');
        $this->context->controller->addJS($this->getKbModuleDir() . 'views/js/admin/validation_admin.js');
        $this->context->controller->addJS($this->getKbModuleDir() . 'views/js/admin/admin.js');
    }
    
    /**
     * Function used display toolbar in page header
     */
    public function initPageHeaderToolbar()
    {
        parent::initPageHeaderToolbar();
    }
}
