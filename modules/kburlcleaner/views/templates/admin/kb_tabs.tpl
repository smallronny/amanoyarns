{*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer tohttp://www.prestashop.com for more information.
* We offer the best and most useful modules PrestaShop and modifications for your online store.
*
* @category  PrestaShop Module
* @author    knowband.com <support@knowband.com>
* @copyright 2017 Knowband
* @license   see file: LICENSE.txt
*
* Description
*
* Admin tpl file
*}
<div class="kb_custom_tabs kb_custom_panel">
    <span>
        <a class="kb_custom_tab {if $selected_nav == 'config'}kb_active{/if}" title="{l s='Module Configurations' mod='kburlcleaner'}" id="kbcf_config_link" href="{$admin_cf_configure_controller}">{*Variable contains URL content, escape not required*}
            <i class="icon-gear"></i>
            {l s='Module Configurations' mod='kburlcleaner'}
        </a>
    </span>
    <span>
        <a class="kb_custom_tab {if $selected_nav == 'kbDuplicateUrl'}kb_active{/if}" title="{l s='Duplicate URL Rewrite Products' mod='kburlcleaner'}" id="kbcf_duplicate_url_link" href="{$admin_duplicate_url_controller}">{*Variable contains URL content, escape not required*}
            <i class="icon-copy"></i>
            {l s='Duplicate URL Rewrite Products' mod='kburlcleaner'}
        </a>
    </span>
    <span>
        <a class="kb_custom_tab {if $selected_nav == 'kbRedirectURL'}kb_active{/if}" title="{l s='URL Redirection List' mod='kburlcleaner'}" id="kbcf_redirect_url_link" href="{$admin_redirect_url_controller}">{*Variable contains URL content, escape not required*}
            <i class="icon-pencil-square"></i>
            {l s='URL Redirection List' mod='kburlcleaner'}
        </a>
    </span>
</div>
{if isset($download_path)}
    <div id='kb_sample_link' class='download_sample' style="display:none;">
        <a href= '{$download_path|escape:'htmlall':'UTF-8'}'>{l s='Download Sample Document from here' mod='kburlcleaner'}</a>
    </div>
{/if}

<script>
    {if isset($kb_base_url)}
    var kb_base_url = "{$kb_base_url|escape:'htmlall':'UTF-8'}";
    {/if}
    {if isset($csv_selected)}
    var csv_selected = "{$csv_selected|escape:'htmlall':'UTF-8'}";
    {/if}
    var csv_file_only = "{l s='Only CSV file allowed.' mod='kburlcleaner'}";
    var select_file_message = "{l s='Please select CSV file.' mod='kburlcleaner'}";
    velovalidation.setErrorLanguage({
        empty_fname: "{l s='Please enter First name.' mod='kburlcleaner'}",
        empty_mname: "{l s='Please enter middle name.' mod='kburlcleaner'}",
        only_alphabet: "{l s='Only alphabets are allowed.' mod='kburlcleaner'}",
        empty_lname: "{l s='Please enter Last name.' mod='kburlcleaner'}",
        alphanumeric: "{l s='Field should be alphanumeric.' mod='kburlcleaner'}",
        empty_pass: "{l s='Please enter Password.' mod='kburlcleaner'}",
        specialchar_pass: "{l s='Password should contain atleast 1 special character.' mod='kburlcleaner'}",
        alphabets_pass: "{l s='Password should contain alphabets.' mod='kburlcleaner'}",
        capital_alphabets_pass: "{l s='Password should contain atleast 1 capital letter.' mod='kburlcleaner'}",
        small_alphabets_pass: "{l s='Password should contain atleast 1 small letter.' mod='kburlcleaner'}",
        digit_pass: "{l s='Password should contain atleast 1 digit.' mod='kburlcleaner'}",
        empty_field: "{l s='Field can not be empty.' mod='kburlcleaner'}",
        number_field: "{l s='You can enter only numbers.' mod='kburlcleaner'}",
        positive_number: "{l s='Number should be greater than 0.' mod='kburlcleaner'}",
        empty_email: "{l s='Please enter Email.' mod='kburlcleaner'}",
        validate_email: "{l s='Please enter a valid Email.' mod='kburlcleaner'}",
        empty_country: "{l s='Please enter country name.' mod='kburlcleaner'}",
        empty_city: "{l s='Please enter city name.' mod='kburlcleaner'}",
        empty_state: "{l s='Please enter state name.' mod='kburlcleaner'}",
        empty_proname: "{l s='Please enter product name.' mod='kburlcleaner'}",
        empty_catname: "{l s='Please enter category name.' mod='kburlcleaner'}",
        empty_zip: "{l s='Please enter zip code.' mod='kburlcleaner'}",
        empty_username: "{l s='Please enter Username.' mod='kburlcleaner'}",
        invalid_date: "{l s='Invalid date format.' mod='kburlcleaner'}",
        invalid_sku: "{l s='Invalid SKU format.' mod='kburlcleaner'}",
        empty_sku: "{l s='Please enter SKU.' mod='kburlcleaner'}",
        validate_range: "{l s='Number is not in the valid range.' mod='kburlcleaner'}",
        empty_address: "{l s='Please enter address.' mod='kburlcleaner'}",
        empty_company: "{l s='Please enter company name.' mod='kburlcleaner'}",
        invalid_phone: "{l s='Phone number is invalid.' mod='kburlcleaner'}",
        empty_phone: "{l s='Please enter phone number.' mod='kburlcleaner'}",
        empty_brand: "{l s='Please enter brand name.' mod='kburlcleaner'}",
        empty_shipment: "{l s='Please enter Shimpment.' mod='kburlcleaner'}",
        invalid_ip: "{l s='Invalid IP format.' mod='kburlcleaner'}",
        invalid_url: "{l s='Invalid URL format.' mod='kburlcleaner'}",
        empty_url: "{l s='Please enter URL.' mod='kburlcleaner'}",
        empty_amount: "{l s='Amount can not be empty.' mod='kburlcleaner'}",
        valid_amount: "{l s='Amount should be numeric.' mod='kburlcleaner'}",
        specialchar_zip: "{l s='Zip should not have special characters.' mod='kburlcleaner'}",
        specialchar_sku: "{l s='SKU should not have special characters.' mod='kburlcleaner'}",
        valid_percentage: "{l s='Percentage should be in number.' mod='kburlcleaner'}",
        between_percentage: "{l s='Percentage should be between 0 and 100.' mod='kburlcleaner'}",
        specialchar_size: "{l s='Size should not have special characters.' mod='kburlcleaner'}",
        specialchar_upc: "{l s='UPC should not have special characters.' mod='kburlcleaner'}",
        specialchar_ean: "{l s='EAN should not have special characters.' mod='kburlcleaner'}",
        specialchar_bar: "{l s='Barcode should not have special characters.' mod='kburlcleaner'}",
        positive_amount: "{l s='Amount should be positive.' mod='kburlcleaner'}",
        invalid_color: "{l s='Color is not valid.' mod='kburlcleaner'}",
        specialchar: "{l s='Special characters are not allowed.' mod='kburlcleaner'}",
        script: "{l s='Script tags are not allowed.' mod='kburlcleaner'}",
        style: "{l s='Style tags are not allowed.' mod='kburlcleaner'}",
        iframe: "{l s='Iframe tags are not allowed.' mod='kburlcleaner'}"
    });
</script>

