/**
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future.If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 * We offer the best and most useful modules PrestaShop and modifications for your online store.
 *
 * @author    knowband.com <support@knowband.com>
 * @copyright 2017 Knowband
 * @license   see file: LICENSE.txt
 * @category  PrestaShop Module
 **/



$(document).ready(function () {

    if ($("[name='kb_redirect[selected_file]']").parents('.form-group').find('.help-block').length == 1) {

        $("[name='kb_redirect[selected_file]']").parents('.form-group').find('.help-block').after($("#kb_sample_link").show());
        $("[name='kb_redirect[selected_file]']").parents('.form-group').find('.help-block').hide();
    }
    showHideFormDetails();
    if (csv_selected == '1') {
        $("input:radio:first").trigger('click');
        $("input[name='kb_redirect[selected_file]']").parents('.form-group ').parents('.form-group ').show();
        $("input[name='kb_redirect[old_url]']").parents('.form-group ').hide();
        $("input[name='kb_redirect[new_url]']").parents('.form-group ').hide();
        $("select[name='kb_redirect[redirect_type]']").parents('.form-group ').hide();
        $("input[name='kb_redirect[active]']").parents('.form-group ').hide();
    }
    $('input[name="kb_redirect[upload_csv]"]').on('change', function () {
        showHideFormDetails();
    });



    $("#kb_seo_redirect_url_form_submit_btn").on('click', function () {
        if (form_validation() == false) {
            return false;
        }
        /*Knowband button validation start*/
        $('#kb_seo_redirect_url_form_submit_btn').attr("disabled", "disabled");
        /*Knowband button validation end*/
        $('#kb_seo_redirect_url_form').submit();
    });
});


function showHideFormDetails() {
    $("input[name='kb_redirect[selected_file]']").parents('.form-group ').parents('.form-group ').hide();
    if ($('input[name="kb_redirect[upload_csv]"]:checked').val() == '1') {
        $("input[name='kb_redirect[selected_file]']").parents('.form-group ').parents('.form-group ').show();
        $("input[name='kb_redirect[old_url]']").parents('.form-group ').hide();
        $("input[name='kb_redirect[new_url]']").parents('.form-group ').hide();
        $("select[name='kb_redirect[redirect_type]']").parents('.form-group ').hide();
        $("input[name='kb_redirect[active]']").parents('.form-group ').hide();
    } else {
        $("input[name='kb_redirect[old_url]']").parents('.form-group ').show();
        $("input[name='kb_redirect[new_url]']").parents('.form-group ').show();
        $("select[name='kb_redirect[redirect_type]']").parents('.form-group ').show();
        $("input[name='kb_redirect[active]']").parents('.form-group ').show();

    }
}


function form_validation() {
    $('.error_message').remove();
    $('.error_field').removeClass('error_field');
    var error = false;

    if ($('input[name="kb_redirect[upload_csv]"]:checked').val() == '0') {
        var current_validation_element = $("input[name='kb_redirect[old_url]']");
        var enter_text_mand = velovalidation.checkMandatory(current_validation_element);
        var enter_text_url_check = velovalidation.checkUrl(current_validation_element, kb_base_url);
        if (enter_text_mand !== true) {
            error = true;
            current_validation_element.addClass('error_field');
            current_validation_element.parent().after($('<span class="error_message">' + enter_text_mand + '</spane>'));
        } else if (enter_text_url_check !== true) {
            error = true;
            current_validation_element.addClass('error_field');
            current_validation_element.parent().after($('<span class="error_message">' + enter_text_url_check + '</spane>'));
        }

        current_validation_element = $("input[name='kb_redirect[new_url]']");
        enter_text_mand = velovalidation.checkMandatory(current_validation_element);
        enter_text_url_check = velovalidation.checkUrl(current_validation_element);
        if (enter_text_mand !== true) {
            error = true;
            current_validation_element.addClass('error_field');
            current_validation_element.after($('<span class="error_message">' + enter_text_mand + '</spane>'));
        } else if (enter_text_url_check !== true) {
            error = true;
            current_validation_element.addClass('error_field');
            current_validation_element.after($('<span class="error_message">' + enter_text_url_check + '</spane>'));
        }
    } else {
        if ($('input[name="filename"]') == null || $('input[name="filename"]').val().trim() == '')
        {
            error = true;
            $('input[name="filename"]').addClass('error_field');
            $('input[name="filename"]').parent('div').after('<span class="error_message">' + select_file_message + '</span>');
        } else if ($('input[name="filename"]').val().split('.').pop() != 'csv') {
            error = true;
            $('input[name="filename"]').addClass('error_field');
            $('input[name="filename"]').parent('div').after('<span class="error_message">' + csv_file_only + '</span>');
        }
    }


    if (error === true) {
        return false;
    }


}


