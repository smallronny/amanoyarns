<?php
/**
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 * We offer the best and most useful modules PrestaShop and modifications for your online store.
 *
 * @author    knowband.com <support@knowband.com>
 * @copyright 2017 Knowband
 * @license   see file: LICENSE.txt
 * @category  PrestaShop Module
 *
 *
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

class KbUrlCleaner extends Module
{

    public function __construct()
    {
        $this->name = 'kburlcleaner';
        $this->tab = 'seo';
        $this->version = '1.0.2';
        $this->author = 'knowband';
        $this->need_instance = 1;
        $this->module_key = 'd72192f0fd2aa7a0476e4a2418fc866b';
        $this->bootstrap = true;

        parent::__construct();
        $this->displayName = $this->l('SEO Optimizer - Clean URLs and 301/302/303 redirection');
        $this->description = $this->l('Remove ID\'s to generate SEO Friendly URL and create unlimited number of 301, 302 and 303 URL redirects to optimize the SEO of your website and avoid the 404 errors');

        $this->ps_versions_compliancy = array(
            'min' => '1.6',
            'max' => _PS_VERSION_);
        $this->confirmUninstall = $this->l('Are you sure you want to uninstall URL Cleaner module?');
    }

    /*
     * Install function to install module
     */

    public function install()
    {
        /*
         * Register various hook functions
         */
        if (!parent::install() || !$this->registerHook('displayBackOfficeHeader')) {
            return false;
        }

        $kb_seo_404_url_table = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'kb_seo_404_url` (
                                 `url_id` int(11) NOT NULL AUTO_INCREMENT,
                                    `url` text NOT NULL,
                                    `occurrence` int(11) NOT NULL ,
                                    `date_upd` datetime NOT NULL,
                                    PRIMARY KEY (`url_id`)
                            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8';


        $kb_seo_redirect_url_table = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'kb_seo_redirect_url` (
                         `redirect_url_id` int(11) NOT NULL AUTO_INCREMENT,
                        `old_url` varchar(255) NOT NULL,
                        `new_url` varchar(255) NOT NULL,
                        `redirect_type` varchar(10) NOT NULL,
                        `active` int(1) NOT NULL,
                        `date_upd` datetime NOT NULL,
                        PRIMARY KEY (`redirect_url_id`),
                        UNIQUE KEY `old_url` (`old_url`)
                     ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8';

        Db::getInstance()->execute($kb_seo_404_url_table);
        Db::getInstance()->execute($kb_seo_redirect_url_table);

        $this->installKbModuleTabs();

        /*
         * Convert array of the default value of the configuration
         *  setting into jsonEncode and then save
         *  the value in the Configuration table
         */
        if (!Configuration::hasKey('KB_URLCLEANER_SETTINGS')) {
            $defaultsettings = Tools::jsonEncode($this->getDefaultSettings());
            Configuration::updateValue('KB_URLCLEANER_SETTINGS', $defaultsettings);
        }
        $this->clearCache();
        return true;
    }

    /*
     * Function to uninstall the module with
     * unregister various hook and
     * also delete the configuration setting
     */

    public function uninstall()
    {
        /*
         * Uninstall admin tabs
         */
        if (!parent::uninstall() || !$this->unregisterHook('displayBackOfficeHeader')) {
            return false;
        }
        $this->unistallDefaultSettings();
        $this->uninstallKbModuleTabs();
        $this->clearCache();
        return true;
    }

    /**
     * enable module
     * @param boolean $force_all
     * @return boolean
     */
    public function enable($force_all = false)
    {
        $this->unistallDefaultSettings();
        $this->clearCache();
        return parent::enable($force_all);
    }

    protected function clearCache()
    {
        $context = ContextCore::getContext();
        $context->smarty->clearAllCache();
    }

    /**
     * disable module
     * @param boolean $force_all
     * @return boolean
     */
    public function disable($force_all = false)
    {
        $this->unistallDefaultSettings();
        $this->clearCache();
        return parent::disable($force_all);
    }

    private function unistallDefaultSettings()
    {
        $settings = Tools::jsonDecode(Configuration::get('KB_URLCLEANER_SETTINGS'), true);
        $settings['enable'] = 0;
        Configuration::updateValue('KB_URLCLEANER_SETTINGS', Tools::jsonEncode($settings));
    }

    /*
     * Function to set the default configuration setting
     */

    private function getDefaultSettings()
    {
        $settings = array(
            'enable' => 0,
            'kb_url_product' => 0,
            'kb_url_category' => 0,
            'kb_url_cms' => 0,
            'kb_url_manufacturer' => 0,
            'kb_url_supplier' => 0,
            'kb_url_iso' => 0,
            'kb_url_html' => 0,
        );
        return $settings;
    }

    /*
     * Function to install admin tabs
     */

    protected function installKbModuleTabs()
    {
        $lang = Language::getLanguages();
        $tab = new Tab();
        $tab->class_name = 'AdminKbDuplicateUrls';
        $tab->module = 'kburlcleaner';
        $tab->active = 0;
        $tab->id_parent = 0;
        foreach ($lang as $l) {
            $tab->name[$l['id_lang']] = $this->l('Duplicate Product URLs');
        }
        $tab->save();
        $tab = new Tab();
        $tab->class_name = 'AdminKb404Urls';
        $tab->module = 'kburlcleaner';
        $tab->active = 0;
        $tab->id_parent = 0;
        foreach ($lang as $l) {
            $tab->name[$l['id_lang']] = $this->l('URLs Redirected on 404 page');
        }
        $tab->save();
        $tab = new Tab();
        $tab->class_name = 'AdminKbRedirectUrls';
        $tab->module = 'kburlcleaner';
        $tab->active = 0;
        $tab->id_parent = 0;
        foreach ($lang as $l) {
            $tab->name[$l['id_lang']] = $this->l('URL Redirection');
        }
        $tab->save();
        return true;
    }

    /*
     * Function to uninstall admin tabs
     */

    protected function uninstallKbModuleTabs()
    {
        $admin_menus = array(
            array(
                'class_name' => 'AdminKbDuplicateUrls',
                'active' => 1,
            ),
            array(
                'class_name' => 'AdminKb404Urls',
                'active' => 1,
            ),
            array(
                'class_name' => 'AdminKbRedirectUrls',
                'active' => 1,
            ),
        );
        foreach ($admin_menus as $menu) {
            $sql = 'SELECT id_tab FROM `' . _DB_PREFIX_ . 'tab` Where class_name = "' . pSQL($menu['class_name']) . '"
				AND module = "' . pSQL($this->name) . '"';
            $id_tab = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
            $tab = new Tab($id_tab);
            $tab->delete();
        }
        return true;
    }

    /*
     * Function to create module panel
     */

    public function getContent()
    {
        $errors = array();
        $knowbandUrlConfiguration = Tools::jsonDecode(Configuration::get('KB_URLCLEANER_SETTINGS'), true);
        
        /*
         * Function to submit the configuration setting values,
         * first by validating the form data and then save into the DB
         */
        if (Tools::isSubmit('submit' . $this->name)) {
            $formvalue = Tools::getvalue('kb_urlcleaner');
            Configuration::updateValue('KB_URLCLEANER_SETTINGS', Tools::jsonEncode($formvalue));
            $this->clearCache();
            $this->context->cookie->__set('kb_redirect_success', $this->l('Configuration successfully updated.'));
            Tools::redirectAdmin(AdminController::$currentIndex . '&configure=' . $this->name . '&token=' . Tools::getAdminTokenLite('AdminModules'));
        }
        $output = '';
        if (isset($this->context->cookie->kb_redirect_success)) {
            $output .= $this->displayConfirmation($this->context->cookie->kb_redirect_success);
            unset($this->context->cookie->kb_redirect_success);
        }

        /*
         * Persistence the configuration setting form data
         */
        $form_value = Tools::jsonDecode(Configuration::get('KB_URLCLEANER_SETTINGS'), true);
        $config_field_value = array(
            'kb_urlcleaner[enable]' => isset($form_value['enable']) ? $form_value['enable'] : '0',
            'kb_urlcleaner[kb_url_product]' => isset($form_value['kb_url_product']) ? $form_value['kb_url_product'] : '0',
            'kb_urlcleaner[kb_url_category]' => isset($form_value['kb_url_category']) ? $form_value['kb_url_category'] : '0',
            'kb_urlcleaner[kb_url_cms]' => isset($form_value['kb_url_cms']) ? $form_value['kb_url_cms'] : '0',
            'kb_urlcleaner[kb_url_manufacturer]' => isset($form_value['kb_url_manufacturer']) ? $form_value['kb_url_manufacturer'] : '0',
            'kb_urlcleaner[kb_url_supplier]' => isset($form_value['kb_url_supplier']) ? $form_value['kb_url_supplier'] : '0',
            'kb_urlcleaner[kb_url_iso]' => isset($form_value['kb_url_iso']) ? $form_value['kb_url_iso'] : '0',
            'kb_urlcleaner[kb_url_html]' => isset($form_value['kb_url_html']) ? $form_value['kb_url_html'] : '0',
        );

        /*
         * loop to fetch all language with default language in an array
         */
        $languages = Language::getLanguages(false);
        foreach ($languages as $k => $language) {
            $languages[$k]['is_default'] = ((int) ($language['id_lang'] == $this->context->language->id));
        }
        /* below line added by rishabh
         *  to show the warning message if friendly url is disabled
         */
        $error_msg = '';
        $friendy_url_active = Configuration::get('PS_REWRITING_SETTINGS');
        if ((int)$friendy_url_active == 0) {
            $error_msg = $this->l('Friendly url is disabled.Kindly enable the same from (Shop parameters->Traffic & SEO->Friendly URL),Otherwise this plugin functionality does not work.');
            $output .= $this->displayError($error_msg);
        }
        /* changes over*/
        /*
         * Create configuration setting form
         */
        $this->fields_form = $this->getConfigurationForm();

        /*
         * Create helper form for configuration setting form
         */
        $form = $this->getform($this->fields_form, $languages, $this->l('Configuration'), false, $config_field_value);
        $this->context->smarty->assign('form', $form);
        $this->context->smarty->assign('selected_nav', 'config');
        $this->context->smarty->assign('admin_cf_configure_controller', $this->context->link->getAdminLink('AdminModules', true) . '&configure=' . urlencode($this->name) . '&tab_module=' . $this->tab . '&module_name=' . urlencode($this->name));
        $this->context->smarty->assign('admin_duplicate_url_controller', $this->context->link->getAdminLink('AdminKbDuplicateUrls', true));
        $this->context->smarty->assign('admin_404_url_controller', $this->context->link->getAdminLink('AdminKb404Urls', true));
        $this->context->smarty->assign('admin_redirect_url_controller', $this->context->link->getAdminLink('AdminKbRedirectUrls', true));
        $this->context->smarty->assign('firstCall', false);
        $this->context->smarty->assign('kb_tabs', $this->context->smarty->fetch(_PS_MODULE_DIR_ . $this->name . '/views/templates/admin/kb_tabs.tpl'));

        //Loads JS and CSS
        $this->setKbMedia();

        /*
         * Generate form using Helper class
         */
        $helper = new Helper();
        $helper->module = $this;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->current = AdminController::$currentIndex . '&configure=' . $this->name;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->override_folder = 'helpers/';
        $helper->base_folder = 'form/';
        $tpl = 'Form_custom.tpl';
        $helper->setTpl($tpl);
        $tpl = $helper->generate();
        $output .= $tpl;
        return $output;
    }

    /*
     * Function to create configuration setting form
     */

    private function getConfigurationForm()
    {

        $form = array(
            'form' => array(
                'id_form' => 'general_configuration_form',
                'legend' => array(
                    'title' => $this->l('Configuration'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'label' => $this->l('Enable/Disable'),
                        'type' => 'switch',
                        'name' => 'kb_urlcleaner[enable]',
                        'values' => array(
                            array(
                                'value' => 1,
                            ),
                            array(
                                'value' => 0,
                            ),
                        ),
                        'hint' => $this->l('Enable/Disable the plugin'),
                        'desc' => $this->l('Enable/Disable the plugin')
                    ),
                    array(
                        'label' => $this->l('Remove Product ID from Product Link'),
                        'type' => 'switch',
                        'name' => 'kb_urlcleaner[kb_url_product]',
                        'values' => array(
                            array(
                                'value' => 1,
                            ),
                            array(
                                'value' => 0,
                            ),
                        ),
                        'hint' => $this->l('Enable to remove product Id from product link'),
                        'desc' => $this->l('Enable to remove product Id from product link'),
                    ),
                    array(
                        'label' => $this->l('Remove Category ID from Category Link'),
                        'type' => 'switch',
                        'name' => 'kb_urlcleaner[kb_url_category]',
                        'values' => array(
                            array(
                                'value' => 1,
                            ),
                            array(
                                'value' => 0,
                            ),
                        ),
                        'hint' => $this->l('Enable to remove category Id from category link'),
                        'desc' => $this->l('Enable to remove category Id from category link'),
                    ),
                    array(
                        'label' => $this->l('Remove ID from CMS Link'),
                        'type' => 'switch',
                        'name' => 'kb_urlcleaner[kb_url_cms]',
                        'values' => array(
                            array(
                                'value' => 1,
                            ),
                            array(
                                'value' => 0,
                            ),
                        ),
                        'hint' => $this->l('Enable to remove Id from cms link'),
                        'desc' => $this->l('Enable to remove Id from cms link'),
                    ),
                    array(
                        'label' => $this->l('Remove ID from Manufacturer Link'),
                        'type' => 'switch',
                        'name' => 'kb_urlcleaner[kb_url_manufacturer]',
                        'values' => array(
                            array(
                                'value' => 1,
                            ),
                            array(
                                'value' => 0,
                            ),
                        ),
                        'hint' => $this->l('Enable to remove Id from manufacturer link'),
                        'desc' => $this->l('Enable to remove Id from manufacturer link'),
                    ),
                    array(
                        'label' => $this->l('Remove ID from Supplier Link'),
                        'type' => 'switch',
                        'name' => 'kb_urlcleaner[kb_url_supplier]',
                        'values' => array(
                            array(
                                'value' => 1,
                            ),
                            array(
                                'value' => 0,
                            ),
                        ),
                        'hint' => $this->l('Enable to remove Id from supplier link'),
                        'desc' => $this->l('Enable to remove Id from supplier link'),
                    ),
                    array(
                        'label' => $this->l('Remove default ISO From Link'),
                        'type' => 'switch',
                        'name' => 'kb_urlcleaner[kb_url_iso]',
                        'values' => array(
                            array(
                                'value' => 1,
                            ),
                            array(
                                'value' => 0,
                            ),
                        ),
                        'hint' => $this->l('Enable to remove default ISO from link'),
                        'desc' => $this->l('Enable to remove default ISO from link'),
                    ),
                    array(
                        'label' => $this->l('Remove .html From Product Link'),
                        'type' => 'switch',
                        'name' => 'kb_urlcleaner[kb_url_html]',
                        'values' => array(
                            array(
                                'value' => 1,
                            ),
                            array(
                                'value' => 0,
                            ),
                        ),
                        'hint' => $this->l('Enable to remove .html from link'),
                        'desc' => $this->l('Enable to remove .html from link'),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                    'class' => 'btn btn-default pull-right form_general'
                ),
            ),
        );


        return $form;
    }

    /*
     * Load JS and CSS file
     */

    public function setKbMedia()
    {
        $this->context->controller->addCSS($this->_path . 'views/css/admin/kb_admin.css');
        $this->context->controller->addJS($this->_path . 'views/js/admin/velovalidation.js');
    }

    /*
     * Function to create Helper Form
     */

    public function getform($field_form, $languages, $title, $show_cancel_button, $field_value)
    {
        $helper = new HelperForm();
        $helper->module = $this;
        $helper->fields_value = $field_value;
        $helper->name_controller = $this->name;
        $helper->languages = $languages;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->default_form_language = $this->context->language->id;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name;
        $helper->title = $title;
        $helper->show_toolbar = false;
        $helper->table = 'configuration';
        $helper->firstCall = true;
        $helper->toolbar_scroll = true;
        $helper->show_cancel_button = $show_cancel_button;
        $helper->submit_action = 'submit' . $this->name;
        return $helper->generateForm(array(
                'form' => $field_form));
    }

    public function hookDisplayBackOfficeHeader()
    {
        $config_data = Tools::jsonDecode(Configuration::get('KB_URLCLEANER_SETTINGS'), true);
        if ($config_data['enable']) {
            if (($this->context->controller->controller_name == 'AdminProducts') && (Tools::getIsset('action')) && (Tools::getValue('action') == 'kbSeo')) {
                $this->context->controller->addJquery();
                $this->context->controller->addJS($this->_path . 'views/js/admin/kburlcleaner.js');
            }
        }
    }
}
