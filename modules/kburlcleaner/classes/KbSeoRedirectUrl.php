<?php
/**
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future.If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 * We offer the best and most useful modules PrestaShop and modifications for your online store.
 *
 * @author    knowband.com <support@knowband.com>
 * @copyright 2017 Knowband
 * @license   see file: LICENSE.txt
 * @category  PrestaShop Module
 */

class KbSeoRedirectUrl extends ObjectModel
{
    public $redirect_url_id;
    public $old_url;
    public $new_url;
    public $redirect_type;
    public $active;
    public $date_upd;
    public static $definition = array(
        'table' => 'kb_seo_redirect_url',
        'primary' => 'redirect_url_id',
        'multilang' => false,
        'fields' => array(
            'redirect_url_id' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isInt'
            ),
            'old_url' => array(
                'type' => self::TYPE_STRING,
                'validate' => 'isCleanHtml'
            ),
            'new_url' => array(
                'type' => self::TYPE_STRING,
                'validate' => 'isCleanHtml'
            ),
            'redirect_type' => array(
                'type' => self::TYPE_STRING,
                'validate' => 'isCleanHtml'
            ),
            'active' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isInt'
            ),
            'date_upd' => array(
                'type' => self::TYPE_DATE,
                'validate' => 'isDate',
                'copy_post' => false
            ),
        ),
    );

    public function __construct($redirect_url_id = null)
    {
        parent::__construct($redirect_url_id);
    }
    
    
    public function insertNewRecord()
    {
        //Check record exist with this URL then return false and doesn't update record
        $old_url = $this->old_url;
        $isRedirectExist = Db::getInstance()->getRow("SELECT * FROM " . _DB_PREFIX_ . "kb_seo_redirect_url where old_url = '" . pSQL($old_url) . "'");
        if ($isRedirectExist !== false) {
            return false;
        } else {
            return $this->add();
        }
    }
}
