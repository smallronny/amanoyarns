<?php
/**
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future.If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 * We offer the best and most useful modules PrestaShop and modifications for your online store.
 *
 * @author    knowband.com <support@knowband.com>
 * @copyright 2017 Knowband
 * @license   see file: LICENSE.txt
 * @category  PrestaShop Module
 */

class Kb404Url extends ObjectModel
{
    public $url_id;
    public $url;
    public $occurrence;
    public $date_upd;
    public static $definition = array(
        'table' => 'kb_seo_404_url',
        'primary' => 'url_id',
        'multilang' => false,
        'fields' => array(
            'url_id' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isInt'
            ),
            'url' => array(
                'type' => self::TYPE_STRING,
                'validate' => 'isCleanHtml'
            ),
            'occurrence' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isInt'
            ),
            'date_upd' => array(
                'type' => self::TYPE_DATE,
                'validate' => 'isDate',
                'copy_post' => false
            ),
        ),
    );

    public function __construct($url_id = null)
    {
        parent::__construct($url_id);
    }

//    public static function getAllHookSettingByHook($hookKey)
//    {
//        if (!Tools::isEmpty($hookKey)) {
//            $query = "Select * from " . _DB_PREFIX_ . "kb_wpblog_hook where hook_key = '" . pSQL($hookKey) . "' and active=1";
//            $data = Db::getInstance()->executeS($query);
//            return $data;
//        } else {
//            return;
//        }
//    }
//
//    /* Get all CMS blocks */
//
//    public static function getAllCMSStructure($id_shop = false)
//    {
//
//        $id_shop = ($id_shop != false) ? $id_shop : Context::getContext()->shop->id;
//
//        $categories = KbWpblogHook::getCMSCategories(false, 0, $id_shop);
//
//        foreach ($categories as $key => $value) {
//            $categories[$key]['cms_pages'] = KbWpblogHook::getCMSPages($value['id_cms_category'], $id_shop);
//        }
//
//        return $categories;
//    }
//
//    /* Get all CMS blocks */
//
//    public static function getAllBlogCategory()
//    {
//        $category_list = array();
//        $query = "Select distinct category_list from " . _DB_PREFIX_ . "kb_wpblog_post";
//        $categories = Db::getInstance()->executeS($query);
//        foreach ($categories as $key => $value) {
//            $category_list = array_unique(array_merge($category_list, explode(',', $value['category_list'])));
//        }
//        return $category_list;
//    }
//
//    public static function getCMSCategories($recursive = false, $parent = 0, $id_shop = false)
//    {
//        $id_shop = ($id_shop != false) ? $id_shop : Context::getContext()->shop->id;
//        $join_shop = '';
//        $where_shop = '';
//        $categories = array();
//
//        if (Tools::version_compare(_PS_VERSION_, '1.6.0.12', '>=') == true) {
//            $join_shop = ' INNER JOIN `' . _DB_PREFIX_ . 'cms_category_shop` cs
//                    ON (bcp.`id_cms_category` = cs.`id_cms_category`)';
//            $where_shop = ' AND cs.`id_shop` = ' . (int) $id_shop . ' AND cl.`id_shop` = ' . (int) $id_shop;
//        }
//
//        if ($recursive === false) {
//            $sql = 'SELECT bcp.`id_cms_category`, bcp.`id_parent`, bcp.`level_depth`, bcp.`active`, bcp.`position`, cl.`name`, cl.`link_rewrite`
//                                    FROM `' . _DB_PREFIX_ . 'cms_category` bcp' .
//                $join_shop . '
//                                    INNER JOIN `' . _DB_PREFIX_ . 'cms_category_lang` cl
//                                    ON (bcp.`id_cms_category` = cl.`id_cms_category`)
//                                    WHERE cl.`id_lang` = ' . (int) Context::getContext()->language->id .
//                $where_shop;
//            if ($parent) {
//                $sql .= ' AND bcp.`id_parent` = ' . (int) $parent;
//            }
//            return Db::getInstance()->executeS($sql);
//        } else {
//            $sql = 'SELECT bcp.`id_cms_category`, bcp.`id_parent`, bcp.`level_depth`, bcp.`active`, bcp.`position`, cl.`name`, cl.`link_rewrite`
//                                    FROM `' . _DB_PREFIX_ . 'cms_category` bcp' .
//                $join_shop . '
//                                    INNER JOIN `' . _DB_PREFIX_ . 'cms_category_lang` cl
//                                    ON (bcp.`id_cms_category` = cl.`id_cms_category`)
//                                    WHERE cl.`id_lang` = ' . (int) Context::getContext()->language->id .
//                $where_shop;
//            if ($parent) {
//                $sql .= ' AND bcp.`id_parent` = ' . (int) $parent;
//            }
//            $results = Db::getInstance()->executeS($sql);
//            foreach ($results as $result) {
//                $sub_categories = KbWpblogHook::getCMSCategories(true, $result['id_cms_category']);
//                if ($sub_categories && count($sub_categories) > 0) {
//                    $result['sub_categories'] = $sub_categories;
//                }
//                $categories[] = $result;
//            }
//
//
//            return isset($categories) ? $categories : false;
//        }
//    }
//
//    public static function getCMSPages($id_cms_category, $id_shop = false)
//    {
//        $id_shop = ($id_shop != false) ? $id_shop : Context::getContext()->shop->id;
//
//        $where_shop = '';
//        if (Tools::version_compare(_PS_VERSION_, '1.6.0.12', '>=') == true && $id_shop != false) {
//            $where_shop = ' AND cl.`id_shop` = ' . (int) $id_shop;
//        }
//
//        $sql = 'SELECT c.`id_cms`, cl.`meta_title`, cl.`link_rewrite`
//                    FROM `' . _DB_PREFIX_ . 'cms` c
//                    INNER JOIN `' . _DB_PREFIX_ . 'cms_shop` cs
//                    ON (c.`id_cms` = cs.`id_cms`)
//                    INNER JOIN `' . _DB_PREFIX_ . 'cms_lang` cl
//                    ON (c.`id_cms` = cl.`id_cms`)
//                    WHERE c.`id_cms_category` = ' . (int) $id_cms_category . '
//                    AND cs.`id_shop` = ' . (int) $id_shop . '
//                    AND cl.`id_lang` = ' . (int) Context::getContext()->language->id .
//            $where_shop . '
//                    AND c.`active` = 1
//                    ORDER BY `position`';
//        return Db::getInstance()->executeS($sql);
//    }

    /**
     * Get product (only names)
     *
     * @param int $id_lang Language id
     * @param int $product_ids Product ids
     * @return array Product
     */
//    public static function getProductDataLight($id_lang, $product_ids)
//    {
//        return Db::getInstance()->executeS('SELECT p.`id_product`, p.`reference`, pl.`name` FROM `' . _DB_PREFIX_ . 'product` p' . Shop::addSqlAssociation('product', 'p') . 'LEFT JOIN `' . _DB_PREFIX_ . 'product_lang` pl ON (p.`id_product` = pl.`id_product` AND pl.`id_lang` = ' . (int) $id_lang . Shop::addSqlRestrictionOnLang('pl') . ') WHERE p.`id_product` IN (' . pSQL($product_ids) . ')');
//    }
}
