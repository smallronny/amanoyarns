<?php
/**
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 * We offer the best and most useful modules PrestaShop and modifications for your online store.
 *
 * @author    knowband.com <support@knowband.com>
 * @copyright 2015 knowband
 * @license   see file: LICENSE.txt
 * @category  PrestaShop Module
 */

class Dispatcher extends DispatcherCore
{

    protected $module;

    protected function __construct()
    {
        $knowbandUrlConfiguration = Tools::jsonDecode(Configuration::get('KB_URLCLEANER_SETTINGS'), true);
        if ($knowbandUrlConfiguration['enable']) {
            if ($knowbandUrlConfiguration['kb_url_category']) {
                $route_id = 'category_rule';
                $this->default_routes['default' . $route_id] = $this->default_routes[$route_id];
                if ($custom_route = Configuration::get('PS_ROUTE' . $route_id, null, null)) {
                    $this->default_routes['default' . $route_id]['rule'] = $custom_route;
                }
                $this->default_routes[$route_id] = array(
                    'controller' => 'category',
                    'rule' => '{parents:/}{rewrite}',
                    'keywords' => array(
                        'id' => array(
                            'regexp' => '[0-9]+'),
                        'rewrite' => array(
                            'regexp' => '[_a-zA-Z0-9\pL\pS-]*'),
                        'meta_keywords' => array(
                            'regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'meta_title' => array(
                            'regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'rewrite_category' => array(
                            'regexp' => '[_a-zA-Z0-9\pL\pS-]*',
                            'param' => 'rewrite_category'),
                        'parents' => array(
                            'regexp' => '[/_a-zA-Z0-9-\pL]*'),
                    ),
                );
            }
            if ($knowbandUrlConfiguration['kb_url_product']) {
                $route_id = 'product_rule';
                $this->default_routes['default' . $route_id] = $this->default_routes[$route_id];
                if ($custom_route = Configuration::get('PS_ROUTE' . $route_id, null, null)) {
                    $this->default_routes['default' . $route_id]['rule'] = $custom_route;
                }
                $this->default_routes['default' . $route_id]['rule'] = str_replace('{-:id_product_attribute}', '', $this->default_routes['default' . $route_id]['rule']);
                $this->default_routes[$route_id] = array(
                    'controller' => 'product',
                    'rule' => '{category:/}{rewrite}.html',
                    'keywords' => array(
                        'id' => array(
                            'regexp' => '[0-9]+'),
                        'rewrite' => array(
                            'regexp' => '[_a-zA-Z0-9\pL\pS-]*'),
                        'rewrite_product' => array(
                            'regexp' => '[_a-zA-Z0-9\pL\pS-]*',
                            'param' => 'rewrite_product'),
                        'ean13' => array(
                            'regexp' => '[0-9\pL]*'),
                        'category' => array(
                            'regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'categories' => array(
                            'regexp' => '[/_a-zA-Z0-9-\pL]*'),
                        'reference' => array(
                            'regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'meta_keywords' => array(
                            'regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'meta_title' => array(
                            'regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'manufacturer' => array(
                            'regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'supplier' => array(
                            'regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'price' => array(
                            'regexp' => '[0-9\.,]*'),
                        'tags' => array(
                            'regexp' => '[a-zA-Z0-9-\pL]*'),
                    ),
                );
            }
            if ($knowbandUrlConfiguration['kb_url_supplier']) {
                $route_id = 'supplier_rule';
                $this->default_routes['default' . $route_id] = $this->default_routes[$route_id];
                if ($custom_route = Configuration::get('PS_ROUTE' . $route_id, null, null)) {
                    $this->default_routes['default' . $route_id]['rule'] = $custom_route;
                }
                $this->default_routes['supplier_rule'] = array(
                    'controller' => 'supplier',
                    'rule' => 'supplier/{rewrite}',
                    'keywords' => array(
                        'id' => array(
                            'regexp' => '[0-9]+'),
                        'rewrite' => array(
                            'regexp' => '[_a-zA-Z0-9\pL\pS-]*',
                            'param' => 'rewrite'),
                        'rewrite_supplier' => array(
                            'regexp' => '[_a-zA-Z0-9\pL\pS-]*',
                            'param' => 'rewrite_supplier'),
                        'meta_keywords' => array(
                            'regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'meta_title' => array(
                            'regexp' => '[_a-zA-Z0-9-\pL]*'),
                    ),
                );
            }
            if ($knowbandUrlConfiguration['kb_url_manufacturer']) {
                $route_id = 'manufacturer_rule';
                $this->default_routes['default' . $route_id] = $this->default_routes[$route_id];
                if ($custom_route = Configuration::get('PS_ROUTE' . $route_id, null, null)) {
                    $this->default_routes['default' . $route_id]['rule'] = $custom_route;
                }
                $this->default_routes['manufacturer_rule'] = array(
                    'controller' => 'manufacturer',
                    'rule' => 'manufacturer/{rewrite}',
                    'keywords' => array(
                        'id' => array(
                            'regexp' => '[0-9]+'),
                        'rewrite' => array(
                            'regexp' => '[_a-zA-Z0-9\pL\pS-]*'),
                        'rewrite_manufacturer' => array(
                            'regexp' => '[_a-zA-Z0-9\pL\pS-]*',
                            'param' => 'rewrite_manufacturer'),
                        'meta_keywords' => array(
                            'regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'meta_title' => array(
                            'regexp' => '[_a-zA-Z0-9-\pL]*'),
                    ),
                );
            }
            if ($knowbandUrlConfiguration['kb_url_cms']) {
                $route_id = 'cms_rule';
                $this->default_routes['default' . $route_id] = $this->default_routes[$route_id];
                if ($custom_route = Configuration::get('PS_ROUTE' . $route_id, null, null)) {
                    $this->default_routes['default' . $route_id]['rule'] = $custom_route;
                }
                $this->default_routes['cms_rule'] = array(
                    'controller' => 'cms',
                    'rule' => 'content/{rewrite}',
                    'keywords' => array(
                        'id' => array(
                            'regexp' => '[0-9]+'),
                        'rewrite' => array(
                            'regexp' => '[_a-zA-Z0-9\pL\pS-]*'),
                        'rewrite_cms' => array(
                            'regexp' => '[_a-zA-Z0-9\pL\pS-]*',
                            'param' => 'rewrite_cms'),
                        'meta_keywords' => array(
                            'regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'meta_title' => array(
                            'regexp' => '[_a-zA-Z0-9-\pL]*'),
                    ),
                );
            }
        }
        parent::__construct();
    }

    protected function setRequestUri()
    {
        $knowbandUrlConfiguration = Tools::jsonDecode(Configuration::get('KB_URLCLEANER_SETTINGS'), true);
        if (($knowbandUrlConfiguration['enable'] == 1) && ($knowbandUrlConfiguration['kb_url_iso'] == 1)) {
            if (isset($_SERVER['REQUEST_URI'])) {
                $this->request_uri = $_SERVER['REQUEST_URI'];
            } elseif (isset($_SERVER['HTTP_X_REWRITE_URL'])) {
                $this->request_uri = $_SERVER['HTTP_X_REWRITE_URL'];
            }
            $this->request_uri = rawurldecode($this->request_uri);
            if (isset(Context::getContext()->shop) && is_object(Context::getContext()->shop)) {
                $this->request_uri = preg_replace('#^' . preg_quote(Context::getContext()->shop->getBaseURI(), '#') . '#i', '/', $this->request_uri);
            }
            if ($this->use_routes && Language::isMultiLanguageActivated()) {
                if (preg_match('#^/([a-z]{2})(?:/.*)?$#', $this->request_uri, $m)) {
                    $_GET['isolang'] = $m[1];
                    $this->request_uri = Tools::substr($this->request_uri, 3);
                } else {
                    $default_language = new Language(Configuration::get('PS_LANG_DEFAULT'));
                    $_GET['isolang'] = $default_language->iso_code;
                }
            }
        } else {
            return parent::setRequestUri();
        }
    }

    protected function loadRoutes($id_shop = null)
    {
        parent::loadRoutes();
        $knowbandUrlConfiguration = Tools::jsonDecode(Configuration::get('KB_URLCLEANER_SETTINGS'), true);
        if ($knowbandUrlConfiguration['enable'] == 1) {
            $context = Context::getContext();
            $language_ids = Language::getIDs();
            if (isset($context->language) && !in_array($context->language->id, $language_ids)) {
                $language_ids[] = (int) $context->language->id;
            }
            if ($this->use_routes) {
                foreach ($this->default_routes as $route_id => $route_data) {
                    if (in_array($route_id, array(
                            'category_rule',
                            'supplier_rule',
                            'manufacturer_rule',
                            'cms_rule',
                            'product_rule'))) {
                        $exp = explode('_', $route_id);
                        $name = 'kb_url_' . Tools::strtolower($exp[0]);
                        $rewrite = '{rewrite}';
                        if ($exp[0] == 'product') {
                            if ($knowbandUrlConfiguration['kb_url_category']) {
                                $rewrite = '{category:/}{rewrite}';
                            }
                            if (!($knowbandUrlConfiguration['kb_url_html'])) {
                                $rewrite .= '.html';
                            }
                        }
                        if ($exp[0] == 'category') {
                            $rewrite = '{parents:/}{rewrite}';
                        }
                        if ($exp[0] == 'cms') {
                            $rewrite = 'content/{rewrite}';
                        }
                        if ($exp[0] == 'manufacturer') {
                            $rewrite = 'manufacturer/{rewrite}';
                        }
                        if ($exp[0] == 'supplier') {
                            $rewrite = 'supplier/{rewrite}';
                        }
                        if ($knowbandUrlConfiguration[$name]) {
                            foreach ($language_ids as $id_lang) {
                                $this->addRoute($route_id, $rewrite, $route_data['controller'], $id_lang, $route_data['keywords'], isset($route_data['params']) ? $route_data['params'] : array(), $id_shop);
                            }
                        }
                    }
                }
            }
        }
    }

    public function getController($id_shop = null)
    {
        $knowbandUrlConfiguration = Tools::jsonDecode(Configuration::get('KB_URLCLEANER_SETTINGS'), true);
        if ($knowbandUrlConfiguration['enable'] == 1) {
            if (defined('_PS_ADMIN_DIR_')) {
                $_GET['controllerUri'] = Tools::getvalue('controller');
            }
            if ($this->controller) {
                $_GET['controller'] = $this->controller;
                return $this->controller;
            }
            if (isset(Context::getContext()->shop) && $id_shop === null) {
                $id_shop = (int) Context::getContext()->shop->id;
            }
            $controller = Tools::getValue('controller');
            if (isset($controller) && is_string($controller) && preg_match('/^([0-9a-z_-]+)\?(.*)=(.*)$/Ui', $controller, $m)) {
                $controller = $m[1];
                if (Tools::getIsset('controller')) {
                    $_GET[$m[2]] = $m[3];
                } elseif (Tools::getIsset('controller')) {
                    $_POST[$m[2]] = $m[3];
                }
            }
            if (!Validate::isControllerName($controller)) {
                $controller = false;
            }
            if ($this->use_routes && !$controller && !defined('_PS_ADMIN_DIR_')) {
                if (!$this->request_uri) {
                    return Tools::strtolower($this->controller_not_found);
                }
                $test_request_uri = preg_replace('/(=http:\/\/)/', '=', $this->request_uri);
                if (!preg_match('/\.(gif|jpe?g|png|css|js|ico)$/i', parse_url($test_request_uri, PHP_URL_PATH))) {
                    list($uri) = explode('?', $this->request_uri);
                    $short_link = ltrim(parse_url($uri, PHP_URL_PATH), '/');
                    if (!empty($short_link)) {
                        $kb_unique_id = '';
                        $route = $this->routes[$id_shop][Context::getContext()->language->id]['product_rule'];
                        if (!$controller && self::isProductLink($short_link, $route, $kb_unique_id)) {
                            $_GET['id_product'] = $kb_unique_id;
                            $controller = 'product';
                        }
                        $route = $this->routes[$id_shop][Context::getContext()->language->id]['category_rule'];
                        if (self::isCategoryLink($short_link, $route, $kb_unique_id)) {
                            $controller = 'category';
                            $_GET['id_category'] = $kb_unique_id;
                        }
                        $route = $this->routes[$id_shop][Context::getContext()->language->id]['cms_rule'];
                        if (!$controller && self::isCMSLink($short_link, $route, $kb_unique_id)) {
                            $_GET['id_cms'] = $kb_unique_id;
                            $controller = 'cms';
                        }
                        $route = $this->routes[$id_shop][Context::getContext()->language->id]['manufacturer_rule'];
                        if (!$controller && self::isManufacturerLink($short_link, $route, $kb_unique_id)) {
                            $_GET['id_manufacturer'] = $kb_unique_id;
                            $controller = 'manufacturer';
                        }
                        $route = $this->routes[$id_shop][Context::getContext()->language->id]['supplier_rule'];
                        if (!$controller && self::isSupplierLink($short_link, $route, $kb_unique_id)) {
                            $_GET['id_supplier'] = $kb_unique_id;
                            $controller = 'supplier';
                        }
                    }
                }
                if (!$controller) {
                    $controller = $this->controller_not_found;
                    if (!preg_match('/\.(gif|jpe?g|png|css|js|ico)$/i', parse_url($test_request_uri, PHP_URL_PATH))) {
                        if ($this->empty_route) {
                            $this->addRoute($this->empty_route['routeID'], $this->empty_route['rule'], $this->empty_route['controller'], Context::getContext()->language->id, array(), array(), $id_shop);
                        }
                        list($uri) = explode('?', $this->request_uri);
                        if (isset($this->routes[$id_shop][Context::getContext()->language->id])) {
                            foreach ($this->routes[$id_shop][Context::getContext()->language->id] as $id_route => $route) {
                                if (in_array($id_route, array(
                                        'product_rule',
                                        'supplier_rule',
                                        'category_rule',
                                        'manufacturer_rule',
                                        'cms_rule'))) {
                                    $exp = explode('_', $id_route);
                                    $name = 'kb_url_' . Tools::strtolower($exp[0]);
                                    if ($knowbandUrlConfiguration[$name] == 1) {
                                        continue;
                                    }
                                }
                                if (preg_match($route['regexp'], $uri, $m)) {
                                    foreach ($m as $k => $v) {
                                        if (!is_numeric($k)) {
                                            $_GET[$k] = $v;
                                        }
                                    }
                                    $controller = $route['controller'] ? $route['controller'] : Tools::getValue('controller');
                                    if (!empty($route['params'])) {
                                        foreach ($route['params'] as $k => $v) {
                                            $_GET[$k] = $v;
                                        }
                                    }
                                    if (preg_match('#module-([a-z0-9_-]+)-([a-z0-9_]+)$#i', $controller, $m)) {
                                        $_GET['module'] = $m[1];
                                        $_GET['fc'] = 'module';
                                        $controller = $m[2];
                                    }
                                    if (Tools::getIsset('fc') &&  Tools::getValue('fc') == 'module') {
                                        $this->front_controller = self::FC_MODULE;
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
                if ($controller == 'index' || preg_match('/^\/index.php(?:\?.*)?$/', $this->request_uri)) {
                    $controller = $this->useDefaultController();
                }
            }
            $this->controller = str_replace('-', '', $controller);
            $_GET['controller'] = $this->controller;
            return $this->controller;
        } else {
            return parent::getController($id_shop);
        }
    }

    private static function isProductLink($short_link, $route, &$kb_unique_id)
    {
        $short_link = preg_replace('#\.html?$#', '', '/' . $short_link);
        $regexp = preg_replace('!\\\.html?\\$#!', '$#', $route['regexp']);
        preg_match($regexp, $short_link, $kw);

        if (empty($kw)) {
            return false;
        }
        $sql = 'SELECT `id_product`
            FROM `' . _DB_PREFIX_ . 'product_lang`
            WHERE `link_rewrite` = \'' . pSQL(end($kw)) . '\' AND `id_lang` = ' . (int) Context::getContext()->language->id;
        if (Shop::isFeatureActive() && Shop::getContext() == Shop::CONTEXT_SHOP) {
            $sql .= ' AND `id_shop` = ' . (int) Shop::getContextShopID();
        }
        $id_product = (int) Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
        $kb_unique_id = $id_product;
        return $id_product > 0;
    }

    private static function isCategoryLink($short_link, $route, &$kb_unique_id)
    {
        $short_link = preg_replace('#\.html?$#', '', '/' . $short_link);
        $regexp = preg_replace('!\\\.html?\\$#!', '$#', $route['regexp']);
        preg_match($regexp, $short_link, $kw);
        if (empty($kw)) {
            return false;
        }
        $sql = 'SELECT `id_category`
            FROM `' . _DB_PREFIX_ . 'category_lang`
            WHERE `link_rewrite` = \'' . pSQL(end($kw)) . '\' AND `id_lang` = ' . (int) Context::getContext()->language->id;
        if (Shop::isFeatureActive() && Shop::getContext() == Shop::CONTEXT_SHOP) {
            $sql .= ' AND `id_shop` = ' . (int) Shop::getContextShopID();
        }
        $id_category = (int) Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
        $kb_unique_id = $id_category;
        return $id_category > 0;
    }

    private static function isCmsLink($short_link, $route, &$kb_unique_id)
    {
        $short_link = preg_replace('#\.html?$#', '', '/' . $short_link);
        $regexp = preg_replace('!\\\.html?\\$#!', '$#', $route['regexp']);
        preg_match($regexp, $short_link, $kw);

        if (empty($kw)) {
            return false;
        }
        $sql = 'SELECT l.`id_cms`
            FROM `' . _DB_PREFIX_ . 'cms_lang` l
            LEFT JOIN `' . _DB_PREFIX_ . 'cms_shop` s ON (l.`id_cms` = s.`id_cms`)
            WHERE l.`link_rewrite` = \'' . pSQL(end($kw)) . '\'';
        if (Shop::isFeatureActive() && Shop::getContext() == Shop::CONTEXT_SHOP) {
            $sql .= ' AND s.`id_shop` = ' . (int) Shop::getContextShopID();
        }
        $id_cms = (int) Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
        $kb_unique_id = $id_cms;
        return $id_cms > 0;
    }

    private static function isManufacturerLink($short_link, $route, &$kb_unique_id)
    {
        $short_link = preg_replace('#\.html?$#', '', '/' . $short_link);
        $regexp = preg_replace('!\\\.html?\\$#!', '$#', $route['regexp']);
        preg_match($regexp, $short_link, $kw);
        if (empty($kw)) {
//            if (0 === strpos('/' . $route['rule'], $short_link)) {
//                return true;
//            }
            return false;
        }
        $manufacturer = str_replace('-', ' ', end($kw));
        $sql = 'SELECT m.`id_manufacturer`
            FROM `' . _DB_PREFIX_ . 'manufacturer` m
            LEFT JOIN `' . _DB_PREFIX_ . 'manufacturer_shop` s ON (m.`id_manufacturer` = s.`id_manufacturer`)
            WHERE LOWER(m.`name`) LIKE \'' . pSQL($manufacturer) . '\'';
        if (Shop::isFeatureActive() && Shop::getContext() == Shop::CONTEXT_SHOP) {
            $sql .= ' AND s.`id_shop` = ' . (int) Shop::getContextShopID();
        }
        $id_manufacturer = (int) Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
        $kb_unique_id = $id_manufacturer;
        return $id_manufacturer > 0;
    }

    private static function isSupplierLink($short_link, $route, &$kb_unique_id)
    {
        $short_link = preg_replace('#\.html?$#', '', '/' . $short_link);
        $regexp = preg_replace('!\\\.html?\\$#!', '$#', $route['regexp']);
        preg_match($regexp, $short_link, $kw);
        if (empty($kw)) {
//            if (0 === strpos('/' . $route['rule'], $short_link)) {
//                return true;
//            }
            return false;
        }
        $supplier = str_replace('-', ' ', end($kw));
        $sql = 'SELECT sp.`id_supplier`
            FROM `' . _DB_PREFIX_ . 'supplier` sp
            LEFT JOIN `' . _DB_PREFIX_ . 'supplier_shop` s ON (sp.`id_supplier` = s.`id_supplier`)
            WHERE LOWER(sp.`name`) LIKE \'' . pSQL($supplier) . '\'';
        if (Shop::isFeatureActive() && Shop::getContext() == Shop::CONTEXT_SHOP) {
            $sql .= ' AND s.`id_shop` = ' . (int) Shop::getContextShopID();
        }
        $id_supplier = (int) Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
        $kb_unique_id = $id_supplier;
        return $id_supplier > 0;
    }
}
