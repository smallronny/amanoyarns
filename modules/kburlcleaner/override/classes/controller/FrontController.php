<?php
/**
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 * We offer the best and most useful modules PrestaShop and modifications for your online store.
 *
 * @author    knowband.com <support@knowband.com>
 * @copyright 2015 knowband
 * @license   see file: LICENSE.txt
 * @category  PrestaShop Module
 */

class FrontController extends FrontControllerCore
{

    /**
     * Initializes front controller: sets smarty variables,
     * class properties, redirects depending on context, etc.
     *
     * @global bool     $useSSL           SSL connection flag
     * @global Cookie   $cookie           Visitor's cookie
     * @global Smarty   $smarty
     * @global Cart     $cart             Visitor's cart
     * @global string   $iso              Language ISO
     * @global Country  $defaultCountry   Visitor's country object
     * @global string   $protocol_link
     * @global string   $protocol_content
     * @global Link     $link
     * @global array    $css_files
     * @global array    $js_files
     * @global Currency $currency         Visitor's selected currency
     *
     * @throws PrestaShopException
     */
    public function init()
    {
        /**
        * This variable is PrestaShop default global variable. We are using it here to check store SSL status. So we can not remove it.
        */
        parent::init();
        $knowbandUrlConfiguration = Tools::jsonDecode(Configuration::get('KB_URLCLEANER_SETTINGS'), true);
        if ($knowbandUrlConfiguration['enable'] == 1) {
            $uri_var = $_SERVER['REQUEST_URI'];
            $isRedirectExist = Db::getInstance()->getRow("SELECT * FROM " . _DB_PREFIX_ . "kb_seo_redirect_url where active = 1 and old_url = '" . pSQL($uri_var) . "'");
            if ($isRedirectExist !== false) {
                if ($uri_var == $isRedirectExist['old_url']) {
                    if ($isRedirectExist['redirect_type'] == 301) {
                        header("HTTP/1.1 301 Moved Permanently");
                        header('Cache-Control: no-cache');
                    }
                    if ($isRedirectExist['redirect_type'] == 302) {
                        header("HTTP/1.1 302 Moved Temporarily");
                        header('Cache-Control: no-cache');
                    }
                    if ($isRedirectExist['redirect_type'] == 303) {
                        header("HTTP/1.1 303 See Other");
                        header('Cache-Control: no-cache');
                    }
                    header('Cache-Control: no-cache');
                    Tools::redirect($isRedirectExist['new_url']);
                    exit;
                }
            }
            if (($knowbandUrlConfiguration['kb_url_iso'] == 1) && (Configuration::get('PS_LANG_DEFAULT') != $this->context->language->id)) {
                $useSSL = true;
                $protocol_content = (isset($useSSL) && $useSSL && Configuration::get('PS_SSL_ENABLED')) ? 'https://' : 'http://';
//                $protocol_content = ($useSSL) ? 'https://' : 'http://';
                $protocol_link = (Configuration::get('PS_SSL_ENABLED') || Tools::usingSecureMode()) ? 'https://' : 'http://';
                $iso = Language::getIsoById($this->context->language->id) . '/';
                $this->context->smarty->assign(array(
                    'base_dir' => _PS_BASE_URL_ . __PS_BASE_URI__ . $iso,
                    'base_dir_ssl' => $protocol_link . Tools::getShopDomainSsl() . __PS_BASE_URI__ . $iso,
                    'content_dir' => $protocol_content . Tools::getHttpHost() . __PS_BASE_URI__ . $iso,
                    'base_uri' => $protocol_content . Tools::getHttpHost() . __PS_BASE_URI__ . $iso . (!Configuration::get('PS_REWRITING_SETTINGS') ? 'index.php' : ''),
                ));
            }
        }
    }
}
