<?php
/**
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future.If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 * We offer the best and most useful modules PrestaShop and modifications for your online store.
 *
 * @author    knowband.com <support@knowband.com>
 * @copyright 2017 knowband
 * @license   see file: LICENSE.txt
 * @category  PrestaShop Module
 */

include_once(_PS_MODULE_DIR_ . 'kburlcleaner/classes/KbSeoRedirectUrl.php');

class AdminKbRedirectUrlsController extends ModuleAdminController
{

    public $kb_smarty;
    public $all_languages = array();
    protected $kb_module_name = 'kburlcleaner';

    public function __construct()
    {
        $this->bootstrap = true;
        $this->allow_export = true;
        $this->context = Context::getContext();
        $this->list_no_link = true;
        $this->kb_smarty = new Smarty();
        /* changes by rishabh jain */
        $this->kb_smarty->setCompileDir(_PS_CACHE_DIR_ . 'smarty/compile');
        $this->kb_smarty->setCacheDir(_PS_CACHE_DIR_ . 'smarty/cache');
        $this->kb_smarty->use_sub_dirs = true;
        $this->kb_smarty->setConfigDir(_PS_SMARTY_DIR_ . 'configs');
        $this->kb_smarty->caching = false;
        $this->kb_smarty->registerPlugin('function', 'l', 'smartyTranslate');
        $this->kb_smarty->setTemplateDir(_PS_MODULE_DIR_ . $this->kb_module_name . '/views/templates/admin/');
        $this->all_languages = $this->getAllLanguages();
        $this->table = 'kb_seo_redirect_url';
        $this->className = 'KbSeoRedirectUrl';
        $this->identifier = 'redirect_url_id';
        $this->lang = false;
        $this->display = 'list';
        $this->csv_line_number = array();
        $this->csv_invalid = 0;
        parent::__construct();
        $this->toolbar_title = $this->module->l('URL Redirection', 'AdminKbRedirectUrlsController');
        $this->fields_list = array(
            'redirect_url_id' => array(
                'title' => $this->module->l('Redirect URL Id', 'AdminKbRedirectUrlsController'),
                'search' => true,
                'align' => 'text-center',
            ),
            'old_url' => array(
                'title' => $this->module->l('Old URL', 'AdminKbRedirectUrlsController'),
                'search' => true,
                'align' => 'left',
            ),
            'new_url' => array(
                'title' => $this->module->l('New URL', 'AdminKbRedirectUrlsController'),
                'search' => true,
                'align' => 'text-center',
            ),
            'redirect_type' => array(
                'title' => $this->module->l('Redirection Type', 'AdminKbRedirectUrlsController'),
                'search' => true,
                'align' => 'text-center',
            ),
            'active' => array(
                'title' => $this->module->l('Enabled', 'AdminKbRedirectUrlsController'),
                'align' => 'text-center',
                'active' => 'status',
                'type' => 'bool',
                'filter_key' => 'a!active'
            ),
            'date_upd' => array(
                'title' => $this->l('Modified date', 'AdminKbRedirectUrlsController'),
                'type' => 'datetime'
            )
        );
        $this->_use_found_rows = false;
        $this->addRowAction('delete');
        $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->l('Delete selected'),
                'icon' => 'icon-trash',
                'confirm' => $this->l('Delete selected items?')
            )
        );
    }

    /*
     * Function for returning the URL of PrestaShop Root Modules Directory
     */

    protected function getModuleDirUrl()
    {
        $module_dir = '';
        if ($this->checkSecureUrl()) {
            $module_dir = _PS_BASE_URL_SSL_ . __PS_BASE_URI__ . str_replace(_PS_ROOT_DIR_ . '/', '', _PS_MODULE_DIR_);
        } else {
            $module_dir = _PS_BASE_URL_ . __PS_BASE_URI__ . str_replace(_PS_ROOT_DIR_ . '/', '', _PS_MODULE_DIR_);
        }
        return $module_dir;
    }

    /*
     * Function for checking SSL
     */

    private function checkSecureUrl()
    {
        $custom_ssl_var = 0;
        if (isset($_SERVER['HTTPS'])) {
            if ($_SERVER['HTTPS'] == 'on') {
                $custom_ssl_var = 1;
            }
        } else if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
            $custom_ssl_var = 1;
        }
        if ((bool) Configuration::get('PS_SSL_ENABLED') && $custom_ssl_var == 1) {
            return true;
        } else {
            return false;
        }
    }

    /*
     * Function for returning all the languages in the system
     */

    public function getAllLanguages()
    {
        return Language::getLanguages(false);
    }

    /**
     * Prestashop Default Function in AdminController.
     * Assign smarty variables for all default views, list and form, then call other init functions
     */
    public function initContent()
    {
        /* below line added by rishabh
         *  to show the warning message if friendly url is disabled
         */
        $friendy_url_active = Configuration::get('PS_REWRITING_SETTINGS');
        if ((int)$friendy_url_active == 0) {
            $this->context->cookie->__set('kb_redirect_error', $this->l('Friendly url is disabled.Kindly enable the same from (Shop parameters->Traffic & SEO->Friendly URL),Otherwise this plugin functionality does not work.'));
        }
        /* changes over*/
        if (isset($this->context->cookie->kb_redirect_error)) {
            $this->errors[] = $this->context->cookie->kb_redirect_error;
            unset($this->context->cookie->kb_redirect_error);
        }

        if (isset($this->context->cookie->kb_redirect_success)) {
            $this->confirmations[] = $this->context->cookie->kb_redirect_success;
            unset($this->context->cookie->kb_redirect_success);
        }
        if (Tools::getIsset('samplecsv')) {
            $this->sampleCSV();
        }

        $this->context->smarty->assign('selected_nav', 'kbRedirectURL');
        $this->context->smarty->assign(
            array(
                'kb_current_token' => Tools::getAdminTokenLite('AdminKbDuplicateUrls')
            )
        );


        $views_path = 'kburlcleaner/views/';
        $file_name = 'file/document_old_new_url.csv';
        if (Tools::getShopProtocol() == 'https://') {
            $download_path = _PS_BASE_URL_SSL_ . _MODULE_DIR_ . $views_path . $file_name;
            $this->context->smarty->assign('kb_base_url', _PS_BASE_URL_SSL_);
        } else {
            $download_path = _PS_BASE_URL_ . _MODULE_DIR_ . $views_path . $file_name;
            $this->context->smarty->assign('kb_base_url', _PS_BASE_URL_);
        }
        if (Tools::getIsset('errorcsv')) {
            $this->context->smarty->assign('csv_selected', 1);
        } else {
            $this->context->smarty->assign('csv_selected', 0);
        }
        $download_path = $this->context->link->getAdminLink('AdminKbRedirectUrls', true).'&samplecsv';
        $this->context->smarty->assign('download_path', $download_path);

        $this->context->smarty->assign('admin_cf_configure_controller', $this->context->link->getAdminLink('AdminModules', true) . '&configure=' . urlencode($this->module->name) . '&tab_module=' . $this->module->tab . '&module_name=' . urlencode($this->module->name));
        $this->context->smarty->assign('admin_duplicate_url_controller', $this->context->link->getAdminLink('AdminKbDuplicateUrls', true));
        $this->context->smarty->assign('admin_404_url_controller', $this->context->link->getAdminLink('AdminKb404Urls', true));
        $this->context->smarty->assign('admin_redirect_url_controller', $this->context->link->getAdminLink('AdminKbRedirectUrls', true));
        $kb_tabs = $this->context->smarty->fetch(_PS_MODULE_DIR_ . $this->kb_module_name . '/views/templates/admin/kb_tabs.tpl');
        $this->content .= $kb_tabs;
        parent::initContent();
    }

    /**
     * Prestashop Default Function in AdminController.
     * Init context and dependencies, handles POST and GET
     */
    public function init()
    {

        parent::init();
    }

    /** Prestashop Default Function in AdminController
     * @TODO uses redirectAdmin only if !$this->ajax
     * @return bool
     */
    public function postProcess()
    {
        if ($this->action == 'bulkdelete') {
            $this->processBulkdelete($this->boxes);
        }
        parent::postProcess();
    }

    protected function processBulkdelete()
    {
        if (is_array($this->boxes) && !empty($this->boxes)) {
            foreach ($this->boxes as $url_id) {
                $kbSeoRedirectUrl = new KbSeoRedirectUrl($url_id);
                $kbSeoRedirectUrl->delete();
            }
            $this->context->cookie->kb_redirect_success = $this->module->l('Selected URLs has been deleted successfully.', 'AdminKbRedirectUrlsController');
            Tools::redirectAdmin($this->context->link->getAdminLink('AdminKbRedirectUrls', true));
        } else {
            $this->context->cookie->kb_redirect_error = $this->module->l('No box selected to delete record.', 'AdminKbRedirectUrlsController');
            Tools::redirectAdmin($this->context->link->getAdminLink('AdminKbRedirectUrls', true));
        }
    }

    /*
     * Function for returning the absolute path of the module directory
     */

    protected function getKbModuleDir()
    {
        return _PS_MODULE_DIR_ . $this->kb_module_name . '/';
    }

    /*
     * Default function, used here to include JS/CSS files for the module.
     */

    public function setMedia($isNewTheme = false)
    {
        parent::setMedia($isNewTheme);
        $this->addJquery();
        $this->context->controller->addCSS($this->getKbModuleDir() . 'views/css/admin/kb_admin.css');
        $this->context->controller->addJS($this->getKbModuleDir() . 'views/js/admin/kburlcleaner_admin.js');
        $this->context->controller->addJS($this->getKbModuleDir() . 'views/js/admin/velovalidation.js');
    }

    /**
     * Function used display toolbar in page header
     */
    public function initPageHeaderToolbar()
    {
        $this->page_header_toolbar_btn['back_url'] = array(
            'href' => 'javascript: window.history.back();',
            'desc' => $this->module->l('Back', 'AdminKbRedirectUrlsController'),
            'icon' => 'process-icon-back'
        );
        if (!Tools::getIsset('addkb_seo_redirect_url')) {
            $this->page_header_toolbar_btn['new'] = array(
                'href' => $this->context->link->getAdminLink('AdminKbRedirectUrls', true) . '&add' . $this->table,
                'desc' => $this->module->l('Add New URL Redirection', 'AdminKbRedirectUrlsController'),
                'icon' => 'process-icon-new'
            );
        }
        $this->page_header_toolbar_btn['export'] = array(
            'href' => $this->context->link->getAdminLink('AdminKbRedirectUrls', true) . '&exportkb_seo_redirect_url',
            'desc' => $this->module->l('Export Redirection URLs', 'AdminKbRedirectUrlsController'),
            'icon' => 'process-icon-export'
        );

        parent::initPageHeaderToolbar();
    }

    /**
     * assign default action in toolbar_btn smarty var, if they are not set.
     * uses override to specifically add, modify or remove items
     *
     */
    public function initToolbar()
    {
        parent::initToolbar();
//        unset($this->toolbar_btn['new']);
    }

    /**
     * Function used to render the form for this controller
     *
     * @return string
     * @throws Exception
     * @throws SmartyException
     */
    public function renderForm()
    {
        $redirect_type = array(
            array(
                'id' => '301',
                'name' => $this->module->l('301 - URL moved permanently', 'AdminKbRedirectUrlsController'),
            ),
            array(
                'id' => '302',
                'name' => $this->module->l('302 - URL moved temporarily', 'AdminKbRedirectUrlsController'),
            ),
            array(
                'id' => '303',
                'name' => $this->module->l('303 - GET method used to retreive information', 'AdminKbRedirectUrlsController'),
            )
        );
        $this->fields_form = array(
            'id_form' => 'kb_seo_redirect_url_form',
            'legend' => array(
                'title' => $this->module->l('Add URL Redirection', 'AdminKbRedirectUrlsController'),
            ),
            'input' => array(
                array(
                    'label' => $this->module->l('Upload CSV File', 'AdminKbRedirectUrlsController'),
                    'type' => 'switch',
                    'hint' => $this->module->l('Upload CSV File to add large number of URL Redirection.', 'AdminKbRedirectUrlsController'),
                    'name' => 'kb_redirect[upload_csv]',
                    'values' => array(
                        array(
                            'value' => 1,
                        ),
                        array(
                            'value' => 0,
                        ),
                    )
                ),
                array(
                    'type' => 'file',
                    'label' => $this->module->l('Choose CSV File', 'AdminKbRedirectUrlsController'),
                    'name' => 'kb_redirect[selected_file]',
                    'id' => 'uploadedfile',
                    'required' => true,
                    'display_image' => true,
                    'desc' => $this->module->l('Download Sample of CSV File', 'AdminKbRedirectUrlsController'),
                    'hint' => $this->module->l('Select the CSV File to be add zipcodes.', 'AdminKbRedirectUrlsController'),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->module->l('Old URL', 'AdminKbRedirectUrlsController'),
                    'hint' => $this->module->l('Old URL which you want to redirect', 'AdminKbRedirectUrlsController'),
                    'desc' => $this->module->l('You must write it WITHOUT http, www and domain name. Example: /en/apple/macbook.html', 'AdminKbRedirectUrlsController'),
                    'empty_message' => '/.../old-page',
                    'prefix' => _PS_BASE_URL_SSL_,
                    'required' => true,
                    'name' => 'kb_redirect[old_url]'
                ),
                array(
                    'type' => 'text',
                    'label' => $this->module->l('New URL', 'AdminKbRedirectUrlsController'),
                    'hint' => $this->module->l('New URL where you want to redirect', 'AdminKbRedirectUrlsController'),
                    'desc' => $this->module->l('You must write it WITH http, www and domain name. Example: http://www.domain.com/en/apple/macbook-air.html', 'AdminKbRedirectUrlsController'),
                    'empty_message' => 'https://www.domain.com/.../new-page',
                    'required' => true,
                    'name' => 'kb_redirect[new_url]'
                ),
                array(
                    'label' => $this->module->l('Redirect Type', 'AdminKbRedirectUrlsController'),
                    'type' => 'select',
                    'name' => 'kb_redirect[redirect_type]',
                    'hint' => $this->module->l('Select type of redirect', 'AdminKbRedirectUrlsController'),
                    'is_bool' => true,
                    'options' => array(
                        'query' => $redirect_type,
                        'id' => 'id',
                        'name' => 'name',
                    ),
                ),
                array(
                    'label' => $this->module->l('Active', 'AdminKbRedirectUrlsController'),
                    'type' => 'switch',
                    'name' => 'kb_redirect[active]',
                    'hint' => $this->module->l('Enable redirection for this URL', 'AdminKbRedirectUrlsController'),
                    'values' => array(
                        array(
                            'value' => 1,
                        ),
                        array(
                            'value' => 0,
                        ),
                    ),
                ),
            ),
            'submit' => array(
                'title' => $this->module->l('Save', 'AdminKbRedirectUrlsController'),
                'class' => 'btn btn-default pull-right form_kb_add_redirect_url'
            ),
        );
        return parent::renderForm();
    }

    /**
     * Function used to add Hooks
     */
    public function processAdd()
    {
        $error = false;
        if (Tools::isSubmit('submitAdd' . $this->table)) {
            $formvalue = array();
            $kbSeoRedirectData = Tools::getValue('kb_redirect');
            if (!$kbSeoRedirectData['upload_csv']) {
                $addSeoRedirect = new KbSeoRedirectUrl();
                $addSeoRedirect->old_url = pSQL($kbSeoRedirectData['old_url']);
                $addSeoRedirect->new_url = pSQL($kbSeoRedirectData['new_url']);
                $addSeoRedirect->redirect_type = pSQL($kbSeoRedirectData['redirect_type']);
                $addSeoRedirect->active = (int)$kbSeoRedirectData['active'];
                
                
                if ($addSeoRedirect->insertNewRecord()) {
                    $this->context->cookie->__set('kb_redirect_success', $this->module->l('New URL Redirection has been added successfully', 'AdminKbRedirectUrlsController'));
                    Tools::redirectAdmin($this->context->link->getAdminLink('AdminKbRedirectUrls', true));
                } else {
                    $this->context->cookie->__set('kb_redirect_error', $this->module->l('Old URL is already exist in redirection list. Please delete previous one to add new.', 'AdminKbRedirectUrlsController'));
                    Tools::redirectAdmin($this->context->link->getAdminLink('AdminKbRedirectUrls', true));
                }
            } else {
                $formvalue['selected_file'] = Tools::getvalue('filename');
                if ($formvalue['selected_file'] == null) {
                    $this->context->cookie->__set('kb_redirect_error', $this->module->l('Please upload the CSV file.', 'AdminKbRedirectUrlsController'));
                    $error = true;
                } else {
                    if ($_FILES['kb_redirect']['name']['selected_file'] != null) {
                        $file_name = $_FILES['kb_redirect']['name']['selected_file'];
                        $file_size = $_FILES['kb_redirect']['size']['selected_file'];
                        $file_tmp = $_FILES['kb_redirect']['tmp_name']['selected_file'];
                        $x = explode('.', $file_name);
                        $file_ext = Tools::strtolower(end($x));
                        $expensions = array(
                            "csv");
                        $formvalue['selected_file'] = "sample_old_new_url_mapping." . $file_ext;

                        if (in_array($file_ext, $expensions) === false) {
                            $this->context->cookie->__set('kb_redirect_error', $this->module->l('Please choose file with .csv extension.', 'AdminKbRedirectUrlsController'));
                            $error = true;
                        }
                        if ($file_size >= 4194304) {
                            $this->context->cookie->__set('kb_redirect_error', $this->module->l('File size must be less than 4 MB.', 'AdminKbRedirectUrlsController'));
                            $error = true;
                        }
                        if (!$error) {
                            if ($_FILES['kb_redirect']['name']['selected_file'] != null) {
                                $file_path1 = 'kburlcleaner/views/file/';
                                move_uploaded_file($file_tmp, _PS_MODULE_DIR_ . $file_path1 . "sample_old_new_url_mapping." . $file_ext);
                            }
                            $csv_file_array = $this->readCSVtoArray("sample_old_new_url_mapping." . $file_ext);
                            if ($this->validateCSVFile($csv_file_array) == false) {
                                if ($this->csv_invalid == 1) {
                                    $this->context->cookie->__set('kb_redirect_error', $this->module->l('Invalid content inside CSV file.Please check the sample document.', 'AdminKbRedirectUrlsController'));
                                    $error = true;
                                } else {
                                    $pv_part1 = $this->module->l('Please check ', 'AdminKbRedirectUrlsController');
                                    $pv_part2 = $this->module->l('th row of CSV File.Following are the validation rules-', 'AdminKbRedirectUrlsController');
                                    $pv_part3 = $this->module->l(' Old URL, New URL, Redirect Type and Active status must exist with valid value.', 'AdminKbRedirectUrlsController');
                                    $pv_middle = implode(",", array_unique($this->csv_line_number));
                                    $this->context->cookie->__set('kb_redirect_error', $pv_part1 . " " . $pv_middle . $pv_part2 . $pv_part3);
                                    $this->csv_line_number = null;
                                    $error = true;
                                }
                            }
                        }
                    }
                }
            }
            if (!$error) {
                $this->insertCsvData($csv_file_array);
                $this->context->cookie->__set('kb_redirect_success', $this->module->l('All the new URL Redirection has been added successfully.', 'AdminKbRedirectUrlsController'));
                Tools::redirectAdmin($this->context->link->getAdminLink('AdminKbRedirectUrls', true));
            } else {
                Tools::redirectAdmin($this->context->link->getAdminLink('AdminKbRedirectUrls', true) . '&addkb_seo_redirect_url&errorcsv');
            }
        }
    }

    public function insertCsvData($csv_file_array)
    {
        $i = 0;
        foreach ($csv_file_array as $values) {
            if (!($i == 0 || $i == count($csv_file_array) - 1)) {
                $addSeoRedirect = new KbSeoRedirectUrl();
                $addSeoRedirect->old_url = pSQL($values[0]);
                $addSeoRedirect->new_url = pSQL($values[1]);
                $addSeoRedirect->redirect_type = pSQL($values[2]);
                $addSeoRedirect->active = (int)$values[3];
                $addSeoRedirect->insertNewRecord();
            }
            $i++;
        }
    }

    public function validateCSVFile($csv_file_array)
    {
        //Function to validate data of csv file
        $avoid_first_row = 0;
        $false_flag = 0;
        foreach ($csv_file_array as $formvalue) {
            if (count($csv_file_array) - 1 > $avoid_first_row) {
                if ($avoid_first_row != 0) {
                    if (count($formvalue) <= 4) {
                        $formvalue = array_map('trim', $formvalue);
                        if (Tools::isEmpty($formvalue[0])) {
                            $this->csv_line_number[] .= $avoid_first_row + 1;
                            $false_flag = 1;
                        } else {
                            if (Validate::isAbsoluteUrl($formvalue[0])) {
                                $this->csv_line_number[] .= $avoid_first_row + 1;
                                $false_flag = 1;
                            } else if (!Validate::isUrl(_PS_BASE_URL_SSL_.''.$formvalue[0])) {
                                $this->csv_line_number[] .= $avoid_first_row + 1;
                                $false_flag = 1;
                            }
                        }
                        if (Tools::isEmpty($formvalue[1])) {
                            $this->csv_line_number[] .= $avoid_first_row + 1;
                            $false_flag = 1;
                        } else {
                            if (!Validate::isAbsoluteUrl($formvalue[1])) {
                                $this->csv_line_number[] .= $avoid_first_row + 1;
                                $false_flag = 1;
                            }
                        }
                        if (!in_array($formvalue[2], array(
                                '301',
                                '302',
                                '303'))) {
                            $this->csv_line_number[] .= $avoid_first_row + 1;
                            $false_flag = 1;
                        }
                        if (!in_array($formvalue[3], array(
                                '0',
                                '1'))) {
                            $this->csv_line_number[] .= $avoid_first_row + 1;
                            $false_flag = 1;
                        }
                    } else {
                        $this->csv_invalid = 1;
                        $false_flag = 1;
                    }
                }
            }
            $avoid_first_row++;
        }
        if ($false_flag == 1) {
            return false;
        } else {
            return true;
        }
    }

    public function readCSVtoArray($csv_file)
    {
        if (Tools::getShopProtocol() == 'https://') {
            $file_path = _PS_BASE_URL_SSL_ . _MODULE_DIR_ . 'kburlcleaner/views/file/';
        } else {
            $file_path = _PS_BASE_URL_ . _MODULE_DIR_ . 'kburlcleaner/views/file/';
        }

        $csvData = Tools::file_get_contents($file_path . $csv_file);
        $lines = explode("\n", $csvData);
        $array = array();
        foreach ($lines as $line) {
            $array[] = str_getcsv($line);
        }
        return $array;
    }
    
    public function processExport($text_delimiter = '"')
    {
        if (ob_get_level() && ob_get_length() > 0) {
            ob_clean();
        }
        $this->getList($this->context->language->id, null, null, 0, false);
        if (!count($this->_list)) {
            return;
        }
        $headers = array();
        foreach ($this->fields_list as $key => $datas) {
            if ('PDF' === $datas['title']) {
                unset($this->fields_list[$key]);
            } else {
                if ('ID' === $datas['title']) {
                    $headers[] = Tools::strtolower(Tools::htmlentitiesDecodeUTF8($datas['title']));
                } else {
                    $headers[] = Tools::htmlentitiesDecodeUTF8($datas['title']);
                }
            }
        }
        $content = array();
        foreach ($this->_list as $i => $row) {
            $content[$i] = array();
            $path_to_image = false;
            foreach ($this->fields_list as $key => $params) {
                $field_value = isset($row[$key]) ? Tools::htmlentitiesDecodeUTF8(Tools::nl2br($row[$key])) : '';
                if ($key == 'image') {
                    if ($params['image'] != 'p' || Configuration::get('PS_LEGACY_IMAGES')) {
                        $path_to_image = Tools::getShopDomain(true)._PS_IMG_.$params['image'].'/'.$row['id_'.$this->table].(isset($row['id_image']) ? '-'.(int)$row['id_image'] : '').'.'.$this->imageType;
                    } else {
                        $path_to_image = Tools::getShopDomain(true)._PS_IMG_.$params['image'].'/'.Image::getImgFolderStatic($row['id_image']).(int)$row['id_image'].'.'.$this->imageType;
                    }
                    if ($path_to_image) {
                        $field_value = $path_to_image;
                    }
                }
                if (isset($params['callback'])) {
                    $callback_obj = (isset($params['callback_object'])) ? $params['callback_object'] : $this->context->controller;
                    if (!preg_match('/<([a-z]+)([^<]+)*(?:>(.*)<\/\1>|\s+\/>)/ism', call_user_func_array(array($callback_obj, $params['callback']), array($field_value, $row)))) {
                        $field_value = call_user_func_array(array($callback_obj, $params['callback']), array($field_value, $row));
                    }
                }
                $content[$i][] = $field_value;
            }
        }
        
        $data = array();
        $data['label'] = $headers;
        $data['rec'] = $content;
        $type ='redirect_url';
        
        if (!empty($headers) && (count($headers) > 0)) {
            $filename =  $type. '_' . time() . '.csv';
            $file = fopen('php://output', 'w');
            header("Content-Transfer-Encoding: Binary");
            header('Content-Type: application/excel');
            header('Content-Disposition: attachment; filename=' . basename($filename));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            ob_clean();
            
            $this->kbCsvExport($data, $file);
            fclose($file);
            die;
        }
    }
    
    
    public function kbCsvExport($fields, $file)
    {
        $data = array();
        if (!empty($fields)) {
            foreach ($fields['label'] as $field) {
                $data[] = $field;
            }
            if (!empty($data)) {
                fputcsv($file, $data, ',');
            }
            if (!empty($fields['rec']) && isset($fields['rec'])) {
                foreach ($fields['rec'] as $field) {
                    fputcsv($file, $field, ',');
                }
            }
        }
        $line = array();
        fputcsv($file, $line);
        return $data;
    }
    
    public function sampleCSV()
    {
        $data = array();
        $headers = array(
            $this->module->l('Old URL', 'AdminKbRedirectUrlsController'),
            $this->module->l('New URL', 'AdminKbRedirectUrlsController'),
            $this->module->l('Redirect Type', 'AdminKbRedirectUrlsController'),
            $this->module->l('Active', 'AdminKbRedirectUrlsController'),
        );
        $content = array(
            array('/en/abc', 'https://atpreprod.on-web.fr/de/abc','301','1'),
            array('/en/def', 'https://atpreprod.on-web.fr/de/def','302','0'),
            array('/en/ghi', 'https://atpreprod.on-web.fr/de/ghi','303','0'),
            array('/en/jkl', 'https://atpreprod.on-web.fr/de/jkl','301','1'),
        );
        $data['label'] = $headers;
        $data['rec'] = $content;
        $type ='sample_redirect_url';
        $filename =  $type. '_' . time() . '.csv';
        $file = fopen('php://output', 'w');
        header("Content-Transfer-Encoding: Binary");
        header('Content-Type: application/excel');
        header('Content-Disposition: attachment; filename=' . basename($filename));
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        ob_clean();
        $this->kbCsvExport($data, $file);
        fclose($file);
        die;
    }
}
