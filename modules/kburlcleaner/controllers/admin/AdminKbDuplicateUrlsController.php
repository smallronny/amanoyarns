<?php
/**
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future.If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 * We offer the best and most useful modules PrestaShop and modifications for your online store.
 *
 * @author    knowband.com <support@knowband.com>
 * @copyright 2017 knowband
 * @license   see file: LICENSE.txt
 * @category  PrestaShop Module
 */

class AdminKbDuplicateUrlsController extends ModuleAdminController
{

    public $kb_smarty;
    public $all_languages = array();
    protected $kb_module_name = 'kburlcleaner';
    public function __construct()
    {

        $this->bootstrap = true;
        $this->allow_export = true;
        $this->context = Context::getContext();
        $this->list_no_link = true;
        $this->kb_smarty = new Smarty();
        /* changes by rishabh jain */
        $this->kb_smarty->setCompileDir(_PS_CACHE_DIR_ . 'smarty/compile');
        $this->kb_smarty->setCacheDir(_PS_CACHE_DIR_ . 'smarty/cache');
        $this->kb_smarty->use_sub_dirs = true;
        $this->kb_smarty->setConfigDir(_PS_SMARTY_DIR_ . 'configs');
        $this->kb_smarty->caching = false;
        $this->kb_smarty->registerPlugin('function', 'l', 'smartyTranslate');
        $this->kb_smarty->setTemplateDir(_PS_MODULE_DIR_ . $this->kb_module_name . '/views/templates/admin/');
        $this->all_languages = $this->getAllLanguages();
        $this->table = 'product_lang';
        $this->identifier = 'id_product';
        $this->lang = false;
        $this->display = 'list';
        parent::__construct();
        $this->toolbar_title = $this->module->l('Duplicate URL Rewrite Products', 'AdminKbDuplicateUrlsController');
        $this->fields_list = array(
            'id_product' => array(
                'title' => $this->module->l('Product Id', 'AdminKbDuplicateUrlsController'),
                'search' => true,
                'align' => 'text-center',
            ),
            'name' => array(
                'title' => $this->module->l('Product Name', 'AdminKbDuplicateUrlsController'),
                'search' => true,
                'align' => 'text-center',
            ),
            'id_product' => array(
                'title' => $this->module->l('Product Id', 'AdminKbDuplicateUrlsController'),
                'search' => true,
                'align' => 'text-center',
            ),
            'id_lang' => array(
                'title' => $this->module->l('Language', 'AdminKbDuplicateUrlsController'),
                'search' => true,
                'align' => 'text-center',
                'type' => 'select',
                'list' => $this->getLanguageArray(),
                'callback' => 'showISOCode',
                'filter_key' => 'id_lang'
            ),
            'link_rewrite' => array(
                'title' => $this->module->l('Rewrite', 'AdminKbDuplicateUrlsController'),
                'search' => true,
                'align' => 'text-center',
            ),
        );
        $active_languages = Language::getLanguages();
        $oneDimensionalArray = array_map('current', $active_languages);
        $language_string = implode(',', $oneDimensionalArray);
        $this->_where.=' AND `id_lang` IN ('.$language_string.') AND `link_rewrite` IN (SELECT `link_rewrite` FROM `' . _DB_PREFIX_ . 'product_lang`
                        GROUP BY `link_rewrite`, `id_lang` HAVING count(`link_rewrite`) > 1) ';
        if (Shop::isFeatureActive() && Shop::getContext() == Shop::CONTEXT_SHOP) {
            $this->_where.= ' AND `id_shop` = ' . (int) Shop::getContextShopID();
        }

        $this->_use_found_rows = false;
        $this->addRowAction('update');
    }

    public function showISOCode($id_row, $row_data)
    {
        return Language::getIsoById($row_data['id_lang']);
    }

    public function displayUpdateLink($token = null, $id = null, $name = null)
    {
        $this->context->smarty->assign(array(
            'href' => $this->context->link->getAdminLink(
                'AdminProducts',
                true,
                array(),
                array('id_product' => $id)
            ),
            'action' => $this->l('Edit Rewrite'),
            'icon' => 'edit'
        ));
        return $this->context->smarty->fetch(_PS_MODULE_DIR_ . 'kburlcleaner/views/templates/admin/list_action.tpl');
    }

    /*
     * Function for returning the URL of PrestaShop Root Modules Directory
     */

    protected function getModuleDirUrl()
    {
        $module_dir = '';
        if ($this->checkSecureUrl()) {
            $module_dir = _PS_BASE_URL_SSL_ . __PS_BASE_URI__ . str_replace(_PS_ROOT_DIR_ . '/', '', _PS_MODULE_DIR_);
        } else {
            $module_dir = _PS_BASE_URL_ . __PS_BASE_URI__ . str_replace(_PS_ROOT_DIR_ . '/', '', _PS_MODULE_DIR_);
        }
        return $module_dir;
    }

    /*
     * Function for checking SSL
     */

    private function checkSecureUrl()
    {
        $custom_ssl_var = 0;
        if (isset($_SERVER['HTTPS'])) {
            if ($_SERVER['HTTPS'] == 'on') {
                $custom_ssl_var = 1;
            }
        } else if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
            $custom_ssl_var = 1;
        }
        if ((bool) Configuration::get('PS_SSL_ENABLED') && $custom_ssl_var == 1) {
            return true;
        } else {
            return false;
        }
    }

    /*
     * Function for returning all the languages in the system
     */

    public function getAllLanguages()
    {
        return Language::getLanguages(false);
    }
    
    public function getLanguageArray()
    {
        $lang_array = array();
        $total_language =  Language::getLanguages(true);
        foreach ($total_language as $language) {
            $lang_array[$language['id_lang']] = $language['iso_code'];
        }
        return $lang_array;
    }

    /**
     * Prestashop Default Function in AdminController.
     * Assign smarty variables for all default views, list and form, then call other init functions
     */
    public function initContent()
    {
        /* below line added by rishabh
         *  to show the warning message if friendly url is disabled
         */
        $friendy_url_active = Configuration::get('PS_REWRITING_SETTINGS');
        if ((int)$friendy_url_active == 0) {
            $this->context->cookie->__set('kb_redirect_error', $this->l('Friendly url is disabled.Kindly enable the same from (Shop parameters->Traffic & SEO->Friendly URL),Otherwise this plugin functionality does not work.'));
        }
        /* changes over*/
        if (isset($this->context->cookie->kb_redirect_error)) {
            $this->errors[] = $this->context->cookie->kb_redirect_error;
            unset($this->context->cookie->kb_redirect_error);
        }

        if (isset($this->context->cookie->kb_redirect_success)) {
            $this->confirmations[] = $this->context->cookie->kb_redirect_success;
            unset($this->context->cookie->kb_redirect_success);
        }

        $this->context->smarty->assign('selected_nav', 'kbDuplicateUrl');
        $this->context->smarty->assign(
            array(
                'kb_current_token' => Tools::getAdminTokenLite('AdminKbDuplicateUrls')
            )
        );
        $this->context->smarty->assign('admin_cf_configure_controller', $this->context->link->getAdminLink('AdminModules', true) . '&configure=' . urlencode($this->module->name) . '&tab_module=' . $this->module->tab . '&module_name=' . urlencode($this->module->name));
        $this->context->smarty->assign('admin_duplicate_url_controller', $this->context->link->getAdminLink('AdminKbDuplicateUrls', true));
        $this->context->smarty->assign('admin_404_url_controller', $this->context->link->getAdminLink('AdminKb404Urls', true));
        $this->context->smarty->assign('admin_redirect_url_controller', $this->context->link->getAdminLink('AdminKbRedirectUrls', true));
        $kb_tabs = $this->context->smarty->fetch(_PS_MODULE_DIR_ . $this->kb_module_name . '/views/templates/admin/kb_tabs.tpl');
        $this->content .= $kb_tabs;
        parent::initContent();
    }

    /**
     * Prestashop Default Function in AdminController.
     * Init context and dependencies, handles POST and GET
     */
    public function init()
    {

        parent::init();
    }

    /** Prestashop Default Function in AdminController
     * @TODO uses redirectAdmin only if !$this->ajax
     * @return bool
     */
    public function postProcess()
    {
        parent::postProcess();
    }

    /*
     * Function for returning the absolute path of the module directory
     */

    protected function getKbModuleDir()
    {
        return _PS_MODULE_DIR_ . $this->kb_module_name . '/';
    }

    /*
     * Default function, used here to include JS/CSS files for the module.
     */

    public function setMedia($isNewTheme = false)
    {
        parent::setMedia($isNewTheme);
        $this->context->controller->addCSS($this->getKbModuleDir() . 'views/css/admin/kb_admin.css');
        $this->context->controller->addJS($this->getKbModuleDir() . 'views/js/admin/velovalidation.js');
    }

    /**
     * Function used display toolbar in page header
     */
    public function initPageHeaderToolbar()
    {
        $this->page_header_toolbar_btn['back_url'] = array(
            'href' => 'javascript: window.history.back();',
            'desc' => $this->module->l('Back', 'AdminKbDuplicateUrlsController'),
            'icon' => 'process-icon-back'
        );
        parent::initPageHeaderToolbar();
    }

    /**
     * assign default action in toolbar_btn smarty var, if they are not set.
     * uses override to specifically add, modify or remove items
     *
     */
    public function initToolbar()
    {
        parent::initToolbar();
        unset($this->toolbar_btn['new']);
    }
}
