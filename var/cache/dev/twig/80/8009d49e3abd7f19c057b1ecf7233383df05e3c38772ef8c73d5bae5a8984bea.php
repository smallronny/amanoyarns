<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__2ee3ed67ad4686faa32c7ea66857e0794580e461ef44d17c1c51009c564f611a */
class __TwigTemplate_9496c790d8d21941f5a828cb446fc586bb606e2fd8edfdd0d151c57df6ac2ba1 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'stylesheets' => [$this, 'block_stylesheets'],
            'extra_stylesheets' => [$this, 'block_extra_stylesheets'],
            'content_header' => [$this, 'block_content_header'],
            'content' => [$this, 'block_content'],
            'content_footer' => [$this, 'block_content_footer'],
            'sidebar_right' => [$this, 'block_sidebar_right'],
            'javascripts' => [$this, 'block_javascripts'],
            'extra_javascripts' => [$this, 'block_extra_javascripts'],
            'translate_javascripts' => [$this, 'block_translate_javascripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "__string_template__2ee3ed67ad4686faa32c7ea66857e0794580e461ef44d17c1c51009c564f611a"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "__string_template__2ee3ed67ad4686faa32c7ea66857e0794580e461ef44d17c1c51009c564f611a"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"es\">
<head>
  <meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
<meta name=\"apple-mobile-web-app-capable\" content=\"yes\">
<meta name=\"robots\" content=\"NOFOLLOW, NOINDEX\">

<link rel=\"icon\" type=\"image/x-icon\" href=\"/img/favicon.ico\" />
<link rel=\"apple-touch-icon\" href=\"/img/app_icon.png\" />

<title>Rendimiento • Amano Yarns</title>

  <script type=\"text/javascript\">
    var help_class_name = 'AdminPerformance';
    var iso_user = 'es';
    var lang_is_rtl = '0';
    var full_language_code = 'es';
    var full_cldr_language_code = 'es-ES';
    var country_iso_code = 'PE';
    var _PS_VERSION_ = '1.7.6.7';
    var roundMode = 2;
    var youEditFieldFor = '';
        var new_order_msg = 'Se ha recibido un nuevo pedido en tu tienda.';
    var order_number_msg = 'Número de pedido: ';
    var total_msg = 'Total: ';
    var from_msg = 'Desde: ';
    var see_order_msg = 'Ver este pedido';
    var new_customer_msg = 'Un nuevo cliente se ha registrado en tu tienda.';
    var customer_name_msg = 'Nombre del cliente: ';
    var new_msg = 'Un nuevo mensaje ha sido publicado en tu tienda.';
    var see_msg = 'Leer este mensaje';
    var token = 'c8bc945c236c4e69a99a1b206d3f43fc';
    var token_admin_orders = '337a752ec09bda44b82130d8be68f564';
    var token_admin_customers = '35478eae119ba8780bacfa853d357b43';
    var token_admin_customer_threads = '6970a63ff80a978d75df4fb59bb670ed';
    var currentIndex = 'index.php?controller=AdminPerformance';
    var employee_token = '78bd854d9d8d8d9008ab91be5d613ac1';
    var choose_language_translate = 'Selecciona el idioma';
    var default_language = '3';
    var admin_modules_link = '/multitienda/admin518ddw65d/index.php/improve/modules/catalog/recommended?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c';
    var admin_notification_get_link = '/multitienda/admin518ddw65d/index.php/common/notifications?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c';
    var admin_notification_push_link = '/multitienda/admin518ddw65d/index.php/common/notifications/ack?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c';
    var tab_modules_list = 'oneandonehosting,jmango360_api';
    var update_success_msg = 'Actualización correcta';
    var errorLogin = 'PrestaShop no pudo iniciar sesión en Addons. Por favor verifica tus datos de acceso y tu conexión de Internet.';
    var search_product_msg = 'Buscar un producto';
  </script>

      <link href=\"/modules/iqitelementor/views/css/backoffice.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/admin518ddw65d/themes/new-theme/public/theme.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/modules/prestablog/views/css/prestablog-back-office.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/modules/prestablog/views/css/admin.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/modules/ets_cfultimate/views/css/contact_form7_admin_all.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/modules/customfields/views/css/admin.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/js/jquery/plugins/chosen/jquery.chosen.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/admin518ddw65d/themes/default/css/vendor/nv.d3.css\" rel=\"stylesheet\" type=\"text/css\"/>
  
  <script type=\"text/javascript\">
var baseAdminDir = \"\\/admin518ddw65d\\/\";
var baseDir = \"\\/\";
var changeFormLanguageUrl = \"\\/multitienda\\/admin518ddw65d\\/index.php\\/configure\\/advanced\\/employees\\/change-form-language?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\";
var currency = {\"iso_code\":\"PEN\",\"sign\":\"PEN\",\"name\":\"Sol peruano\",\"format\":null};
var currency_specifications = {\"symbol\":[\",\",\".\",\";\",\"%\",\"-\",\"+\",\"E\",\"\\u00d7\",\"\\u2030\",\"\\u221e\",\"NaN\"],\"currencyCode\":\"PEN\",\"currencySymbol\":\"PEN\",\"positivePattern\":\"#,##0.00\\u00a0\\u00a4\",\"negativePattern\":\"-#,##0.00\\u00a0\\u00a4\",\"maxFractionDigits\":2,\"minFractionDigits\":2,\"groupingUsed\":true,\"primaryGroupSize\":3,\"secondaryGroupSize\":3};
var host_mode = false;
var iqitModulesAdditionalTabs = {\"ajaxUrl\":\"https:\\/\\/amanoyarns.com\\/admin518ddw65d\\/index.php?controller=AdminModules&token=c254d09bde7a3c43ba55640a47808738&ajax=1&configure=iqitadditionaltabs\"};
var iqitModulesSizeCharts = {\"ajaxUrl\":\"https:\\/\\/amanoyarns.com\\/admin518ddw65d\\/index.php?controller=AdminModules&token=c254d09bde7a3c43ba55640a47808738&ajax=1&configure=iqitsizecharts\"};
var number_specifications = {\"symbol\":[\",\",\".\",\";\",\"%\",\"-\",\"+\",\"E\",\"\\u00d7\",\"\\u2030\",\"\\u221e\",\"NaN\"],\"positivePattern\":\"#,##0.###\",\"negativePattern\":\"-#,##0.###\",\"maxFractionDigits\":3,\"minFractionDigits\":0,\"groupingUsed\":true,\"primaryGroupSize\":3,\"secondaryGroupSize\":3};
var show_new_customers = \"1\";
var show_new_messages = false;
var show_new_orders = \"1\";
</script>
<script type=\"text/javascript\" src=\"/modules/ps_faviconnotificationbo/views/js/favico.js\"></script>
<script type=\"text/javascript\" src=\"/modules/ps_faviconnotificationbo/views/js/ps_faviconnotificationbo.js\"></script>
<script type=\"text/javascript\" src=\"/admin518ddw65d/themes/new-theme/public/main.bundle.js\"></script>
<script type=\"text/javascript\" src=\"/js/jquery/plugins/jquery.chosen.js\"></script>
<script type=\"text/javascript\" src=\"/js/admin.js?v=1.7.6.7\"></script>
<script type=\"text/javascript\" src=\"/admin518ddw65d/themes/new-theme/public/cldr.bundle.js\"></script>
<script type=\"text/javascript\" src=\"/js/tools.js?v=1.7.6.7\"></script>
<script type=\"text/javascript\" src=\"/admin518ddw65d/public/bundle.js\"></script>
<script type=\"text/javascript\" src=\"/js/vendor/d3.v3.min.js\"></script>
<script type=\"text/javascript\" src=\"/admin518ddw65d/themes/default/js/vendor/nv.d3.min.js\"></script>

  <script>
  if (undefined !== ps_faviconnotificationbo) {
    ps_faviconnotificationbo.initialize({
      backgroundColor: '#DF0067',
      textColor: '#FFFFFF',
      notificationGetUrl: '/multitienda/admin518ddw65d/index.php/common/notifications?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c',
      CHECKBOX_ORDER: 1,
      CHECKBOX_CUSTOMER: 1,
      CHECKBOX_MESSAGE: 1,
      timer: 120000, // Refresh every 2 minutes
    });
  }
</script>


";
        // line 99
        $this->displayBlock('stylesheets', $context, $blocks);
        $this->displayBlock('extra_stylesheets', $context, $blocks);
        echo "</head>

<body class=\"lang-es adminperformance\">

  <header id=\"header\">

    <nav id=\"header_infos\" class=\"main-header\">
      <button class=\"btn btn-primary-reverse onclick btn-lg unbind ajax-spinner\"></button>

            <i class=\"material-icons js-mobile-menu\">menu</i>
      <a id=\"header_logo\" class=\"logo float-left\" href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminDashboard&amp;token=85fa14fe1cc9e34f50bf8fe2dbe383af\"></a>
      <span id=\"shop_version\">1.7.6.7</span>

      <div class=\"component\" id=\"quick-access-container\">
        <div class=\"dropdown quick-accesses\">
  <button class=\"btn btn-link btn-sm dropdown-toggle\" type=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" id=\"quick_select\">
    Acceso rápido
  </button>
  <div class=\"dropdown-menu\">
          <a class=\"dropdown-item\"
         href=\"https://amanoyarns.com/pe/admin518ddw65d/index.php?controller=AdminStats&amp;module=statscheckup&amp;token=253cda18de773939270c521081431117\"
                 data-item=\"Evaluación del catálogo\"
      >Evaluación del catálogo</a>
          <a class=\"dropdown-item\"
         href=\"https://amanoyarns.com/pe/admin518ddw65d/index.php/improve/modules/manage?token=8f7cbd628bc90d525d7e899bf7bfee1e\"
                 data-item=\"Módulos instalados\"
      >Módulos instalados</a>
          <a class=\"dropdown-item\"
         href=\"https://amanoyarns.com/pe/admin518ddw65d/index.php/sell/catalog/categories/new?token=8f7cbd628bc90d525d7e899bf7bfee1e\"
                 data-item=\"Nueva categoría\"
      >Nueva categoría</a>
          <a class=\"dropdown-item\"
         href=\"https://amanoyarns.com/pe/admin518ddw65d/index.php/sell/catalog/products/new?token=8f7cbd628bc90d525d7e899bf7bfee1e\"
                 data-item=\"Nuevo\"
      >Nuevo</a>
          <a class=\"dropdown-item\"
         href=\"https://amanoyarns.com/pe/admin518ddw65d/index.php?controller=AdminCartRules&amp;addcart_rule&amp;token=fb1ccab211a2c2db7969eeef885270b6\"
                 data-item=\"Nuevo cupón de descuento\"
      >Nuevo cupón de descuento</a>
          <a class=\"dropdown-item\"
         href=\"https://amanoyarns.com/pe/admin518ddw65d/index.php?controller=AdminOrders&amp;token=337a752ec09bda44b82130d8be68f564\"
                 data-item=\"Pedidos\"
      >Pedidos</a>
          <a class=\"dropdown-item\"
         href=\"https://amanoyarns.com/pe/admin518ddw65d/index.php?controller=AdminModules&amp;configure=prestablog&amp;module_name=prestablog&amp;token=c254d09bde7a3c43ba55640a47808738\"
                 data-item=\"PrestaBlog\"
      >PrestaBlog</a>
        <div class=\"dropdown-divider\"></div>
          <a
        class=\"dropdown-item js-quick-link\"
        href=\"#\"
        data-rand=\"190\"
        data-icon=\"icon-AdminAdvancedParameters\"
        data-method=\"add\"
        data-url=\"index.php/multitienda//configure/advanced/performance\"
        data-post-link=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminQuickAccesses&token=318d0b2b502e9217bcc4638d165f6395\"
        data-prompt-text=\"Por favor, renombre este acceso rápido:\"
        data-link=\"Rendimiento - Lista\"
      >
        <i class=\"material-icons\">add_circle</i>
        Añadir esta página a Acceso rápido
      </a>
        <a class=\"dropdown-item\" href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminQuickAccesses&token=318d0b2b502e9217bcc4638d165f6395\">
      <i class=\"material-icons\">settings</i>
      Administrar accesos rápidos
    </a>
  </div>
</div>
      </div>
      <div class=\"component\" id=\"header-search-container\">
        <form id=\"header_search\"
      class=\"bo_search_form dropdown-form js-dropdown-form collapsed\"
      method=\"post\"
      action=\"/admin518ddw65d/index.php?controller=AdminSearch&amp;token=5ac1c17ec53ded87186d84cc53ab63e0\"
      role=\"search\">
  <input type=\"hidden\" name=\"bo_search_type\" id=\"bo_search_type\" class=\"js-search-type\" />
    <div class=\"input-group\">
    <input type=\"text\" class=\"form-control js-form-search\" id=\"bo_query\" name=\"bo_query\" value=\"\" placeholder=\"Buscar (p. ej.: referencia de producto, nombre de cliente...)\">
    <div class=\"input-group-append\">
      <button type=\"button\" class=\"btn btn-outline-secondary dropdown-toggle js-dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
        toda la tienda
      </button>
      <div class=\"dropdown-menu js-items-list\">
        <a class=\"dropdown-item\" data-item=\"toda la tienda\" href=\"#\" data-value=\"0\" data-placeholder=\"¿Qué estás buscando?\" data-icon=\"icon-search\"><i class=\"material-icons\">search</i> toda la tienda</a>
        <div class=\"dropdown-divider\"></div>
        <a class=\"dropdown-item\" data-item=\"Catálogo\" href=\"#\" data-value=\"1\" data-placeholder=\"Nombre del producto, SKU, referencia...\" data-icon=\"icon-book\"><i class=\"material-icons\">store_mall_directory</i> Catálogo</a>
        <a class=\"dropdown-item\" data-item=\"Clientes por nombre\" href=\"#\" data-value=\"2\" data-placeholder=\"Email, nombre...\" data-icon=\"icon-group\"><i class=\"material-icons\">group</i> Clientes por nombre</a>
        <a class=\"dropdown-item\" data-item=\"Clientes por dirección IP\" href=\"#\" data-value=\"6\" data-placeholder=\"123.45.67.89\" data-icon=\"icon-desktop\"><i class=\"material-icons\">desktop_mac</i> Clientes por dirección IP</a>
        <a class=\"dropdown-item\" data-item=\"Pedidos\" href=\"#\" data-value=\"3\" data-placeholder=\"ID del pedido\" data-icon=\"icon-credit-card\"><i class=\"material-icons\">shopping_basket</i> Pedidos</a>
        <a class=\"dropdown-item\" data-item=\"Facturas\" href=\"#\" data-value=\"4\" data-placeholder=\"Número de factura\" data-icon=\"icon-book\"><i class=\"material-icons\">book</i> Facturas</a>
        <a class=\"dropdown-item\" data-item=\"Carritos\" href=\"#\" data-value=\"5\" data-placeholder=\"ID carrito\" data-icon=\"icon-shopping-cart\"><i class=\"material-icons\">shopping_cart</i> Carritos</a>
        <a class=\"dropdown-item\" data-item=\"Módulos\" href=\"#\" data-value=\"7\" data-placeholder=\"Nombre del módulo\" data-icon=\"icon-puzzle-piece\"><i class=\"material-icons\">extension</i> Módulos</a>
      </div>
      <button class=\"btn btn-primary\" type=\"submit\"><span class=\"d-none\">BÚSQUEDA</span><i class=\"material-icons\">search</i></button>
    </div>
  </div>
</form>

<script type=\"text/javascript\">
 \$(document).ready(function(){
    \$('#bo_query').one('click', function() {
    \$(this).closest('form').removeClass('collapsed');
  });
});
</script>
      </div>

              <div class=\"component hide-mobile-sm\" id=\"header-debug-mode-container\">
          <a class=\"link shop-state\"
             id=\"debug-mode\"
             data-toggle=\"pstooltip\"
             data-placement=\"bottom\"
             data-html=\"true\"
             title=\"<p class='text-left'><strong>Tu tienda está en modo debug.</strong></p><p class='text-left'>Se muestran todos los errores y mensajes PHP. Cuando ya no los necesites, <strong>desactiva</strong> este modo.</p>\"
             href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/performance/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\"
          >
            <i class=\"material-icons\">bug_report</i>
            <span>Modo depuración</span>
          </a>
        </div>
      
      
      <div class=\"component\" id=\"header-shop-list-container\">
          <div id=\"shop-list\" class=\"shop-list dropdown ps-dropdown stores\">
    <button class=\"btn btn-link\" type=\"button\" data-toggle=\"dropdown\">
      <span class=\"selected-item\">
        <i class=\"material-icons visibility\">visibility</i>
                  Amano Yarns Peru
                <i class=\"material-icons arrow-down\">arrow_drop_down</i>
      </span>
    </button>
    <div class=\"dropdown-menu dropdown-menu-right ps-dropdown-menu\">
      <ul class=\"items-list\"><li><a class=\"dropdown-item\" href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/performance/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c&amp;setShopContext=\">Todas las tiendas</a></li><li class=\"group\"><a class=\"dropdown-item\" href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/performance/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c&amp;setShopContext=g-1\">grupo Default</a></li><li class=\"shop\"><a class=\"dropdown-item \" href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/performance/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c&amp;setShopContext=s-1\">Amano Yarns</a><a class=\"link-shop\" href=\"https://amanoyarns.com/\" target=\"_blank\"><i class=\"material-icons\">&#xE8F4;</i></a></li><li class=\"shop active\"><a class=\"dropdown-item \" href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/performance/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c&amp;setShopContext=s-2\">Amano Yarns Peru</a><a class=\"link-shop\" href=\"https://amanoyarns.com/pe/\" target=\"_blank\"><i class=\"material-icons\">&#xE8F4;</i></a></li></ul>

    </div>
  </div>
      </div>

              <div class=\"component header-right-component\" id=\"header-notifications-container\">
          <div id=\"notif\" class=\"notification-center dropdown dropdown-clickable\">
  <button class=\"btn notification js-notification dropdown-toggle\" data-toggle=\"dropdown\">
    <i class=\"material-icons\">notifications_none</i>
    <span id=\"notifications-total\" class=\"count hide\">0</span>
  </button>
  <div class=\"dropdown-menu dropdown-menu-right js-notifs_dropdown\">
    <div class=\"notifications\">
      <ul class=\"nav nav-tabs\" role=\"tablist\">
                          <li class=\"nav-item\">
            <a
              class=\"nav-link active\"
              id=\"orders-tab\"
              data-toggle=\"tab\"
              data-type=\"order\"
              href=\"#orders-notifications\"
              role=\"tab\"
            >
              Pedidos<span id=\"_nb_new_orders_\"></span>
            </a>
          </li>
                                    <li class=\"nav-item\">
            <a
              class=\"nav-link \"
              id=\"customers-tab\"
              data-toggle=\"tab\"
              data-type=\"customer\"
              href=\"#customers-notifications\"
              role=\"tab\"
            >
              Clientes<span id=\"_nb_new_customers_\"></span>
            </a>
          </li>
                                    <li class=\"nav-item\">
            <a
              class=\"nav-link \"
              id=\"messages-tab\"
              data-toggle=\"tab\"
              data-type=\"customer_message\"
              href=\"#messages-notifications\"
              role=\"tab\"
            >
              Mensajes<span id=\"_nb_new_messages_\"></span>
            </a>
          </li>
                        </ul>

      <!-- Tab panes -->
      <div class=\"tab-content\">
                          <div class=\"tab-pane active empty\" id=\"orders-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              No hay pedidos nuevos por ahora :(<br>
              ¿Y si incluyes algunos descuentos de temporada?
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                                    <div class=\"tab-pane  empty\" id=\"customers-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              No hay clientes nuevos por ahora :(<br>
              ¿Ha enviado algún correo electrónico de adquisición últimamente?
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                                    <div class=\"tab-pane  empty\" id=\"messages-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              No hay mensajes nuevo por ahora.<br>
              Que no haya noticias, es de por sí una buena noticia, ¿verdad?
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                        </div>
    </div>
  </div>
</div>

  <script type=\"text/html\" id=\"order-notification-template\">
    <a class=\"notif\" href='order_url'>
      #_id_order_ -
      de <strong>_customer_name_</strong> (_iso_code_)_carrier_
      <strong class=\"float-sm-right\">_total_paid_</strong>
    </a>
  </script>

  <script type=\"text/html\" id=\"customer-notification-template\">
    <a class=\"notif\" href='customer_url'>
      #_id_customer_ - <strong>_customer_name_</strong>_company_ - registrado <strong>_date_add_</strong>
    </a>
  </script>

  <script type=\"text/html\" id=\"message-notification-template\">
    <a class=\"notif\" href='message_url'>
    <span class=\"message-notification-status _status_\">
      <i class=\"material-icons\">fiber_manual_record</i> _status_
    </span>
      - <strong>_customer_name_</strong> (_company_) - <i class=\"material-icons\">access_time</i> _date_add_
    </a>
  </script>
        </div>
      
      <div class=\"component\" id=\"header-employee-container\">
        <div class=\"dropdown employee-dropdown\">
  <div class=\"rounded-circle person\" data-toggle=\"dropdown\">
    <i class=\"material-icons\">account_circle</i>
  </div>
  <div class=\"dropdown-menu dropdown-menu-right\">
    <div class=\"employee-wrapper-avatar\">
      
      <span class=\"employee_avatar\"><img class=\"avatar rounded-circle\" src=\"https://profile.prestashop.com/ventasonline%40amanoyarns.com.jpg\" /></span>
      <span class=\"employee_profile\">Bienvenido de nuevo, Mia Bravo</span>
      <a class=\"dropdown-item employee-link profile-link\" href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/employees/3/edit?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\">
      <i class=\"material-icons\">settings</i>
      Tu perfil
    </a>
    </div>
    
    <p class=\"divider\"></p>
    <a class=\"dropdown-item\" href=\"https://www.prestashop.com/en/resources/documentations?utm_source=back-office&amp;utm_medium=profile&amp;utm_campaign=resources-en&amp;utm_content=download17\" target=\"_blank\"><i class=\"material-icons\">book</i> Recursos</a>
    <a class=\"dropdown-item\" href=\"https://www.prestashop.com/en/training?utm_source=back-office&amp;utm_medium=profile&amp;utm_campaign=training-en&amp;utm_content=download17\" target=\"_blank\"><i class=\"material-icons\">school</i> Formación</a>
    <a class=\"dropdown-item\" href=\"https://www.prestashop.com/en/experts?utm_source=back-office&amp;utm_medium=profile&amp;utm_campaign=expert-en&amp;utm_content=download17\" target=\"_blank\"><i class=\"material-icons\">person_pin_circle</i> Encontrar un Experto</a>
    <a class=\"dropdown-item\" href=\"https://addons.prestashop.com?utm_source=back-office&amp;utm_medium=profile&amp;utm_campaign=addons-en&amp;utm_content=download17\" target=\"_blank\"><i class=\"material-icons\">extension</i> Marketplace de PrestaShop</a>
    <a class=\"dropdown-item\" href=\"https://www.prestashop.com/en/contact?utm_source=back-office&amp;utm_medium=profile&amp;utm_campaign=help-center-en&amp;utm_content=download17\" target=\"_blank\"><i class=\"material-icons\">help</i> Centro de ayuda</a>
    <p class=\"divider\"></p>
    <a class=\"dropdown-item employee-link text-center\" id=\"header_logout\" href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminLogin&amp;logout=1&amp;token=74c6f35c05d4708e8f43c3bec6b521cc\">
      <i class=\"material-icons d-lg-none\">power_settings_new</i>
      <span>Cerrar sesión</span>
    </a>
  </div>
</div>
      </div>
    </nav>

      </header>

  <nav class=\"nav-bar d-none d-md-block\">
  <span class=\"menu-collapse\" data-toggle-url=\"/multitienda/admin518ddw65d/index.php/configure/advanced/employees/toggle-navigation?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\">
    <i class=\"material-icons\">chevron_left</i>
    <i class=\"material-icons\">chevron_left</i>
  </span>

  <ul class=\"main-menu\">

          
                
                
        
          <li class=\"link-levelone \" data-submenu=\"1\" id=\"tab-AdminDashboard\">
            <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminDashboard&amp;token=85fa14fe1cc9e34f50bf8fe2dbe383af\" class=\"link\" >
              <i class=\"material-icons\">trending_up</i> <span>Inicio</span>
            </a>
          </li>

        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"2\" id=\"tab-SELL\">
              <span class=\"title\">Vender</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"3\" id=\"subtab-AdminParentOrders\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminOrders&amp;token=337a752ec09bda44b82130d8be68f564\" class=\"link\">
                    <i class=\"material-icons mi-shopping_basket\">shopping_basket</i>
                    <span>
                    Pedidos
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-3\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"4\" id=\"subtab-AdminOrders\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminOrders&amp;token=337a752ec09bda44b82130d8be68f564\" class=\"link\"> Pedidos
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"5\" id=\"subtab-AdminInvoices\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/sell/orders/invoices/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Facturas
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"6\" id=\"subtab-AdminSlip\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminSlip&amp;token=32d2b953445e02b625a9414bf556b583\" class=\"link\"> Facturas por abono
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"7\" id=\"subtab-AdminDeliverySlip\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/sell/orders/delivery-slips/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Albaranes de entrega
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"8\" id=\"subtab-AdminCarts\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminCarts&amp;token=8d603d6b157b1dfdc05cec5c41df3fa1\" class=\"link\"> Carritos de compra
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"192\" id=\"subtab-deleteorderstab\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=deleteorderstab&amp;token=9236cf14ee4d38063d82fbf956537d25\" class=\"link\"> Delete Orders Free
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"9\" id=\"subtab-AdminCatalog\">
                  <a href=\"/multitienda/admin518ddw65d/index.php/sell/catalog/products?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\">
                    <i class=\"material-icons mi-store\">store</i>
                    <span>
                    Catálogo
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-9\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"10\" id=\"subtab-AdminProducts\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/sell/catalog/products?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Productos
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"11\" id=\"subtab-AdminCategories\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/sell/catalog/categories?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Categorías
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"12\" id=\"subtab-AdminTracking\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminTracking&amp;token=dc5f6ffdef00f749f9a7fde811d18969\" class=\"link\"> Monitoreo
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"13\" id=\"subtab-AdminParentAttributesGroups\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminAttributesGroups&amp;token=5e7d2cf12ed17474143674f72b829ce2\" class=\"link\"> Atributos y Características
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"16\" id=\"subtab-AdminParentManufacturers\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/sell/catalog/brands/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Marcas y Proveedores
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"19\" id=\"subtab-AdminAttachments\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminAttachments&amp;token=f12848fff910bbdb18358282e4532148\" class=\"link\"> Archivos
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"20\" id=\"subtab-AdminParentCartRules\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminCartRules&amp;token=fb1ccab211a2c2db7969eeef885270b6\" class=\"link\"> Descuentos
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"23\" id=\"subtab-AdminStockManagement\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/sell/stocks/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Stocks
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"24\" id=\"subtab-AdminParentCustomer\">
                  <a href=\"/multitienda/admin518ddw65d/index.php/sell/customers/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\">
                    <i class=\"material-icons mi-account_circle\">account_circle</i>
                    <span>
                    Clientes
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-24\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"25\" id=\"subtab-AdminCustomers\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/sell/customers/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Clientes
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"26\" id=\"subtab-AdminAddresses\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminAddresses&amp;token=3473c75244a1130ea0ce3e1b276eb578\" class=\"link\"> Direcciones
                              </a>
                            </li>

                                                                                                                          </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"28\" id=\"subtab-AdminParentCustomerThreads\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminCustomerThreads&amp;token=6970a63ff80a978d75df4fb59bb670ed\" class=\"link\">
                    <i class=\"material-icons mi-chat\">chat</i>
                    <span>
                    Servicio al Cliente
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-28\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"29\" id=\"subtab-AdminCustomerThreads\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminCustomerThreads&amp;token=6970a63ff80a978d75df4fb59bb670ed\" class=\"link\"> Servicio al Cliente
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"30\" id=\"subtab-AdminOrderMessage\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminOrderMessage&amp;token=4ea687869efb5b878d8a681627a2f8ea\" class=\"link\"> Mensajes de Pedidos
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"31\" id=\"subtab-AdminReturn\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminReturn&amp;token=24731f0726e68ee4238adac770908bca\" class=\"link\"> Devoluciones de mercancía
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"32\" id=\"subtab-AdminStats\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminStats&amp;token=253cda18de773939270c521081431117\" class=\"link\">
                    <i class=\"material-icons mi-assessment\">assessment</i>
                    <span>
                    Estadísticas
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"42\" id=\"tab-IMPROVE\">
              <span class=\"title\">Personalizar</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"43\" id=\"subtab-AdminParentModulesSf\">
                  <a href=\"/multitienda/admin518ddw65d/index.php/improve/modules/manage?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Módulos
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-43\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"44\" id=\"subtab-AdminModulesSf\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/improve/modules/manage?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Module Manager
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"48\" id=\"subtab-AdminParentModulesCatalog\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/improve/modules/catalog?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Catálogo de Módulos
                              </a>
                            </li>

                                                                                                                              
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"190\" id=\"subtab-AdminGeoIpRedirect\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminGeoIpRedirect&amp;token=b8bec064e350f0aa40ff2819b62a15f6\" class=\"link\"> Geo Ip Redirect
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"52\" id=\"subtab-AdminParentThemes\">
                  <a href=\"/multitienda/admin518ddw65d/index.php/improve/design/themes/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\">
                    <i class=\"material-icons mi-desktop_mac\">desktop_mac</i>
                    <span>
                    Diseño
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-52\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"126\" id=\"subtab-AdminThemesParent\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/improve/design/themes/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Tema y logotipo
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"54\" id=\"subtab-AdminThemesCatalog\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/improve/design/themes-catalog/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Catálogo de Temas
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"55\" id=\"subtab-AdminParentMailTheme\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/improve/design/mail_theme/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Tema Email
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"57\" id=\"subtab-AdminCmsContent\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/improve/design/cms-pages/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Páginas
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"58\" id=\"subtab-AdminModulesPositions\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/improve/design/modules/positions/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Posiciones
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"59\" id=\"subtab-AdminImages\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminImages&amp;token=d22032a75512a8bd57c66994fb1d7479\" class=\"link\"> Ajustes de imágenes
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"125\" id=\"subtab-AdminLinkWidget\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/modules/link-widget/list?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Link Widget
                              </a>
                            </li>

                                                                                                                              
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"139\" id=\"subtab-AdminIqitElementor\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminIqitElementor&amp;token=b14024e4f265a6366c791baeb751b9ef\" class=\"link\"> Iqit Elementor - Page builder
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"142\" id=\"subtab-IqitFrontThemeEditor\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=IqitFrontThemeEditor&amp;token=ff0f3a0545fe5244425c56689a4b9a5e\" class=\"link\"> IqitThemeEditor - Live
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"143\" id=\"subtab-AdminIqitThemeEditor\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminIqitThemeEditor&amp;token=17a7d2a70ea17a3e4d7325aa8f53ec35\" class=\"link\"> IqitThemeEditor - Backoffice
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"60\" id=\"subtab-AdminParentShipping\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminCarriers&amp;token=58052dbee7ce39b63b35875b9fa22e33\" class=\"link\">
                    <i class=\"material-icons mi-local_shipping\">local_shipping</i>
                    <span>
                    Transporte
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-60\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"61\" id=\"subtab-AdminCarriers\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminCarriers&amp;token=58052dbee7ce39b63b35875b9fa22e33\" class=\"link\"> Transportistas
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"62\" id=\"subtab-AdminShipping\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/improve/shipping/preferences?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Preferencias
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"63\" id=\"subtab-AdminParentPayment\">
                  <a href=\"/multitienda/admin518ddw65d/index.php/improve/payment/payment_methods?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\">
                    <i class=\"material-icons mi-payment\">payment</i>
                    <span>
                    Pago
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-63\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"64\" id=\"subtab-AdminPayment\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/improve/payment/payment_methods?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Métodos de pago
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"65\" id=\"subtab-AdminPaymentPreferences\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/improve/payment/preferences?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Preferencias
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"66\" id=\"subtab-AdminInternational\">
                  <a href=\"/multitienda/admin518ddw65d/index.php/improve/international/localization/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\">
                    <i class=\"material-icons mi-language\">language</i>
                    <span>
                    Internacional
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-66\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"67\" id=\"subtab-AdminParentLocalization\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/improve/international/localization/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Localización
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"72\" id=\"subtab-AdminParentCountries\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminZones&amp;token=9ab264a858ec3464bc7fc491f009b121\" class=\"link\"> Ubicaciones Geográficas
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"76\" id=\"subtab-AdminParentTaxes\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/improve/international/taxes/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Impuestos
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"79\" id=\"subtab-AdminTranslations\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/improve/international/translations/settings?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Traducciones
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title -active\" data-submenu=\"80\" id=\"tab-CONFIGURE\">
              <span class=\"title\">Configurar</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"81\" id=\"subtab-ShopParameters\">
                  <a href=\"/multitienda/admin518ddw65d/index.php/configure/shop/preferences/preferences?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\">
                    <i class=\"material-icons mi-settings\">settings</i>
                    <span>
                    Parámetros de la tienda
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-81\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"82\" id=\"subtab-AdminParentPreferences\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/shop/preferences/preferences?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Configuración
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"85\" id=\"subtab-AdminParentOrderPreferences\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/shop/order-preferences/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Configuración de Pedidos
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"88\" id=\"subtab-AdminPPreferences\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/shop/product-preferences/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Configuración de Productos
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"89\" id=\"subtab-AdminParentCustomerPreferences\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/shop/customer-preferences/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Ajustes sobre clientes
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"93\" id=\"subtab-AdminParentStores\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/shop/contacts/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Contacto
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"96\" id=\"subtab-AdminParentMeta\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/shop/seo-urls/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Tráfico &amp; SEO
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"100\" id=\"subtab-AdminParentSearchConf\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminSearchConf&amp;token=2f26805a947f6bc0a6c4bad2a9e5c50a\" class=\"link\"> Buscar
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"173\" id=\"subtab-AdminGeorules\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminGeorules&amp;token=e6ccee71dfe19da4a19883234880ff5c\" class=\"link\"> Geolocation rules
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                                                    
                <li class=\"link-levelone has_submenu -active open ul-open\" data-submenu=\"103\" id=\"subtab-AdminAdvancedParameters\">
                  <a href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/system-information/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\">
                    <i class=\"material-icons mi-settings_applications\">settings_applications</i>
                    <span>
                    Parámetros Avanzados
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_up
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-103\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"104\" id=\"subtab-AdminInformation\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/system-information/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Información
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo -active\" data-submenu=\"105\" id=\"subtab-AdminPerformance\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/performance/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Rendimiento
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"106\" id=\"subtab-AdminAdminPreferences\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/administration/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Administración
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"107\" id=\"subtab-AdminEmails\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/emails/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Dirección de correo electrónico
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"108\" id=\"subtab-AdminImport\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/import/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Importar
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"109\" id=\"subtab-AdminParentEmployees\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/employees/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Equipo
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"113\" id=\"subtab-AdminParentRequestSql\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/sql-requests/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Base de datos
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"116\" id=\"subtab-AdminLogs\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/logs/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Registros/Logs
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"117\" id=\"subtab-AdminWebservice\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/webservice-keys/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Webservice
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"118\" id=\"subtab-AdminShopGroup\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminShopGroup&amp;token=11ffaac27b74f2a478d7b873426a70c8\" class=\"link\"> Multitienda
                              </a>
                            </li>

                                                                                                                              
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"151\" id=\"subtab-AdminExportProducts\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminExportProducts&amp;token=f4ebc1ade98b096fa74d847717548c47\" class=\"link\"> Export Products
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"191\" id=\"subtab-AdminKlaviyoPsConfig\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminKlaviyoPsConfig&amp;token=041a449b898191b1e0f5ab9a5b4cb05f\" class=\"link\">
                    <i class=\"material-icons mi-trending_up\">trending_up</i>
                    <span>
                    Klaviyo
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"130\" id=\"tab-AdminRevslider\">
              <span class=\"title\">Revolution Slider</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"131\" id=\"subtab-AdminRevsliderSliders\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminRevsliderSliders&amp;token=b8c88e1177a8d6ebf5ebd956b2a61efb\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Sliders
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"132\" id=\"subtab-AdminRevolutionsliderGlobalSettings\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminRevolutionsliderGlobalSettings&amp;token=5c8aee3dfd228162776324fd5429829d\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Global Settings
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"133\" id=\"subtab-AdminRevolutionsliderAddons\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminRevolutionsliderAddons&amp;token=6e380476e7a7489da8444b23afb6261a\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Addons
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"134\" id=\"subtab-AdminRevolutionsliderNavigation\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminRevolutionsliderNavigation&amp;token=fab4c1fa744cca7b873709330375e472\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Navigation
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"157\" id=\"tab-Management\">
              <span class=\"title\">CONTENT MANAGEMENT</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"158\" id=\"subtab-AdminPrestaBlog\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminPrestaBlog&amp;token=6ed9385adb994977eeee154d8d762727\" class=\"link\">
                    <i class=\"material-icons mi-library_books\">library_books</i>
                    <span>
                    PrestaBlog
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"171\" id=\"tab-AdminParentAddifyadvanceqty\">
              <span class=\"title\">Addify Advance Quantity</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"172\" id=\"subtab-AdminAddifyadvanceqty\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminAddifyadvanceqty&amp;token=eedf80f2becbec244946eea3255ec734\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Manage Advance Quantity
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"174\" id=\"tab-AdminContactFormUltimate\">
              <span class=\"title\">Contacto</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"175\" id=\"subtab-AdminContactFormUltimateDashboard\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminContactFormUltimateDashboard&amp;token=5fe0e6663c76873648af1bfbafa6e9b4\" class=\"link\">
                    <i class=\"material-icons mi-icon icon-home\">icon icon-home</i>
                    <span>
                    Contact dashboard
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"176\" id=\"subtab-AdminContactFormUltimateContactForm\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminContactFormUltimateContactForm&amp;token=1c54726964954dfc5f1debb8a65881c7\" class=\"link\">
                    <i class=\"material-icons mi-icon icon-envelope-o\">icon icon-envelope-o</i>
                    <span>
                    Contact forms
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"177\" id=\"subtab-AdminContactFormUltimateMessage\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminContactFormUltimateMessage&amp;token=e6be79383fb7523d4c9213ddf1c6847b\" class=\"link\">
                    <i class=\"material-icons mi-icon icon-comments\">icon icon-comments</i>
                    <span>
                    Messages
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"178\" id=\"subtab-AdminContactFormUltimateStatistics\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminContactFormUltimateStatistics&amp;token=ddea511612f1eedf5100fd88e08fd301\" class=\"link\">
                    <i class=\"material-icons mi-icon icon-line-chart\">icon icon-line-chart</i>
                    <span>
                    Statistics
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"179\" id=\"subtab-AdminContactFormUltimateIpBlacklist\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminContactFormUltimateIpBlacklist&amp;token=e8727394d34b9d42be8a7543ed5a1478\" class=\"link\">
                    <i class=\"material-icons mi-icon icon-user-times\">icon icon-user-times</i>
                    <span>
                    IP and Email blacklist
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"180\" id=\"subtab-AdminContactFormUltimateSetting\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminContactFormUltimateEmail&amp;token=eeee8b5e5a740958fe7b170dc97a0545\" class=\"link\">
                    <i class=\"material-icons mi-icon icon-cog\">icon icon-cog</i>
                    <span>
                    Setting
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-180\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"181\" id=\"subtab-AdminContactFormUltimateEmail\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminContactFormUltimateEmail&amp;token=eeee8b5e5a740958fe7b170dc97a0545\" class=\"link\"> Email templates
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"182\" id=\"subtab-AdminContactFormUltimateImportExport\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminContactFormUltimateImportExport&amp;token=90a02b5c34dea75f9c3dde377c0c045d\" class=\"link\"> Import/Export
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"183\" id=\"subtab-AdminContactFormUltimateIntegration\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminContactFormUltimateIntegration&amp;token=b66ce1b0d09401e9eb7d5b649b62acd9\" class=\"link\"> Integration
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"188\" id=\"tab-AdminCustomFields\">
              <span class=\"title\">Custom Fields</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"189\" id=\"subtab-AdminFields\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminFields&amp;token=d12760bba8e5ba6e9bdb510afa2cc3c0\" class=\"link\">
                    <i class=\"material-icons mi-content_paste\">content_paste</i>
                    <span>
                    Manage Custom Fields
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                          
        
            </ul>
  
</nav>

<div id=\"main-div\">
          
<div class=\"header-toolbar\">
  <div class=\"container-fluid\">

    
      <nav aria-label=\"Breadcrumb\">
        <ol class=\"breadcrumb\">
                      <li class=\"breadcrumb-item\">Parámetros Avanzados</li>
          
                      <li class=\"breadcrumb-item active\">
              <a href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/performance/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" aria-current=\"page\">Rendimiento</a>
            </li>
                  </ol>
      </nav>
    

    <div class=\"title-row\">
      
          <h1 class=\"title\">
            Rendimiento          </h1>
      

      
        <div class=\"toolbar-icons\">
          <div class=\"wrapper\">
            
                                                                                    <a
                  class=\"btn btn-primary  pointer\"                  id=\"page-header-desc-configuration-clear_cache\"
                  href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/performance/clear-cache?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\"                  title=\"Borrar la caché\"                >
                  <i class=\"material-icons\">delete</i>                  Borrar la caché
                </a>
                                                                  <a
                class=\"btn btn-outline-secondary \"
                id=\"page-header-desc-configuration-modules-list\"
                href=\"/multitienda/admin518ddw65d/index.php/improve/modules/catalog?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\"                title=\"Módulos recomendados\"
                              >
                Módulos recomendados
              </a>
            
            
                              <a class=\"btn btn-outline-secondary btn-help btn-sidebar\" href=\"#\"
                   title=\"Ayuda\"
                   data-toggle=\"sidebar\"
                   data-target=\"#right-sidebar\"
                   data-url=\"/multitienda/admin518ddw65d/index.php/common/sidebar/https%253A%252F%252Fhelp.prestashop.com%252Fes%252Fdoc%252FAdminPerformance%253Fversion%253D1.7.6.7%2526country%253Des/Ayuda?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\"
                   id=\"product_form_open_help\"
                >
                  Ayuda
                </a>
                                    </div>
        </div>
      
    </div>
  </div>

  
    
</div>
      
      <div class=\"content-div  \">

        

                                                        
        <div class=\"row \">
          <div class=\"col-sm-12\">
            <div id=\"ajax_confirmation\" class=\"alert alert-success\" style=\"display: none;\"></div>


  ";
        // line 1453
        $this->displayBlock('content_header', $context, $blocks);
        // line 1454
        echo "                 ";
        $this->displayBlock('content', $context, $blocks);
        // line 1455
        echo "                 ";
        $this->displayBlock('content_footer', $context, $blocks);
        // line 1456
        echo "                 ";
        $this->displayBlock('sidebar_right', $context, $blocks);
        // line 1457
        echo "
            
          </div>
        </div>

      </div>
    </div>

  <div id=\"non-responsive\" class=\"js-non-responsive\">
  <h1>¡Oh no!</h1>
  <p class=\"mt-3\">
    La versión para móviles de esta página no está disponible todavía.
  </p>
  <p class=\"mt-2\">
    Por favor, utiliza un ordenador de escritorio hasta que esta página sea adaptada para dispositivos móviles.
  </p>
  <p class=\"mt-2\">
    Gracias.
  </p>
  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminDashboard&amp;token=85fa14fe1cc9e34f50bf8fe2dbe383af\" class=\"btn btn-primary py-1 mt-3\">
    <i class=\"material-icons\">arrow_back</i>
    Atrás
  </a>
</div>
  <div class=\"mobile-layer\"></div>

      <div id=\"footer\" class=\"bootstrap\">
    <script type=\"text/javascript\">
    var link_ajax = 'https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminModules&amp;token=c254d09bde7a3c43ba55640a47808738&amp;configure=ets_cfultimate&amp;tab_module=front_office_features&amp;module_name=ets_cfultimate';
    \$(document).ready(function () {
        \$.ajax({
            url: link_ajax,
            data: 'action=etsCfuGetCountMessageContactForm',
            type: 'post',
            dataType: 'json',
            success: function (json) {
                if (parseInt(json.count) > 0) {
                    if (\$('#subtab-AdminContactFormUltimateMessage span').length)
                        \$('#subtab-AdminContactFormUltimateMessage span').append('<span class=\"count_messages \">' + json.count + '</span>');
                    else
                        \$('#subtab-AdminContactFormUltimateMessage a').append('<span class=\"count_messages \">' + json.count + '</span>');
                } else {
                    if (\$('#subtab-AdminContactFormUltimateMessage span').length)
                        \$('#subtab-AdminContactFormUltimateMessage span').append('<span class=\"count_messages hide\">' + json.count + '</span>');
                    else
                        \$('#subtab-AdminContactFormUltimateMessage a').append('<span class=\"count_messages hide\">' + json.count + '</span>');
                }

            },
        });
    });
</script>
</div>
  

      <div class=\"bootstrap\">
      <div class=\"modal fade\" id=\"modal_addons_connect\" tabindex=\"-1\">
\t<div class=\"modal-dialog modal-md\">
\t\t<div class=\"modal-content\">
\t\t\t\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>
\t\t\t\t<h4 class=\"modal-title\"><i class=\"icon-puzzle-piece\"></i> <a target=\"_blank\" href=\"https://addons.prestashop.com/?utm_source=back-office&utm_medium=modules&utm_campaign=back-office-ES&utm_content=download\">PrestaShop Addons</a></h4>
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"modal-body\">
\t\t\t\t\t\t<div class=\"alert alert-warning\">
\t\t\t\tSi quieres utilizar completamente el panel AdminModules para obtener módulos gratuitos, debes habilitar la siguiente configuración en tu servidor:
\t\t\t\t<br />
\t\t\t\t- Activar la configuración allow_url_fopen de PHP<br />\t\t\t\t\t\t\t</div>
\t\t\t</div>


\t\t\t\t\t</div>
\t</div>
</div>

    </div>
  
";
        // line 1535
        $this->displayBlock('javascripts', $context, $blocks);
        $this->displayBlock('extra_javascripts', $context, $blocks);
        $this->displayBlock('translate_javascripts', $context, $blocks);
        echo "</body>
</html>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 99
    public function block_stylesheets($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function block_extra_stylesheets($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "extra_stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "extra_stylesheets"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 1453
    public function block_content_header($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content_header"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content_header"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 1454
    public function block_content($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 1455
    public function block_content_footer($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content_footer"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content_footer"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 1456
    public function block_sidebar_right($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "sidebar_right"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "sidebar_right"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 1535
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function block_extra_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "extra_javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "extra_javascripts"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function block_translate_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "translate_javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "translate_javascripts"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "__string_template__2ee3ed67ad4686faa32c7ea66857e0794580e461ef44d17c1c51009c564f611a";
    }

    public function getDebugInfo()
    {
        return array (  1709 => 1535,  1692 => 1456,  1675 => 1455,  1658 => 1454,  1641 => 1453,  1608 => 99,  1594 => 1535,  1514 => 1457,  1511 => 1456,  1508 => 1455,  1505 => 1454,  1503 => 1453,  145 => 99,  45 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"es\">
<head>
  <meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
<meta name=\"apple-mobile-web-app-capable\" content=\"yes\">
<meta name=\"robots\" content=\"NOFOLLOW, NOINDEX\">

<link rel=\"icon\" type=\"image/x-icon\" href=\"/img/favicon.ico\" />
<link rel=\"apple-touch-icon\" href=\"/img/app_icon.png\" />

<title>Rendimiento • Amano Yarns</title>

  <script type=\"text/javascript\">
    var help_class_name = 'AdminPerformance';
    var iso_user = 'es';
    var lang_is_rtl = '0';
    var full_language_code = 'es';
    var full_cldr_language_code = 'es-ES';
    var country_iso_code = 'PE';
    var _PS_VERSION_ = '1.7.6.7';
    var roundMode = 2;
    var youEditFieldFor = '';
        var new_order_msg = 'Se ha recibido un nuevo pedido en tu tienda.';
    var order_number_msg = 'Número de pedido: ';
    var total_msg = 'Total: ';
    var from_msg = 'Desde: ';
    var see_order_msg = 'Ver este pedido';
    var new_customer_msg = 'Un nuevo cliente se ha registrado en tu tienda.';
    var customer_name_msg = 'Nombre del cliente: ';
    var new_msg = 'Un nuevo mensaje ha sido publicado en tu tienda.';
    var see_msg = 'Leer este mensaje';
    var token = 'c8bc945c236c4e69a99a1b206d3f43fc';
    var token_admin_orders = '337a752ec09bda44b82130d8be68f564';
    var token_admin_customers = '35478eae119ba8780bacfa853d357b43';
    var token_admin_customer_threads = '6970a63ff80a978d75df4fb59bb670ed';
    var currentIndex = 'index.php?controller=AdminPerformance';
    var employee_token = '78bd854d9d8d8d9008ab91be5d613ac1';
    var choose_language_translate = 'Selecciona el idioma';
    var default_language = '3';
    var admin_modules_link = '/multitienda/admin518ddw65d/index.php/improve/modules/catalog/recommended?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c';
    var admin_notification_get_link = '/multitienda/admin518ddw65d/index.php/common/notifications?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c';
    var admin_notification_push_link = '/multitienda/admin518ddw65d/index.php/common/notifications/ack?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c';
    var tab_modules_list = 'oneandonehosting,jmango360_api';
    var update_success_msg = 'Actualización correcta';
    var errorLogin = 'PrestaShop no pudo iniciar sesión en Addons. Por favor verifica tus datos de acceso y tu conexión de Internet.';
    var search_product_msg = 'Buscar un producto';
  </script>

      <link href=\"/modules/iqitelementor/views/css/backoffice.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/admin518ddw65d/themes/new-theme/public/theme.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/modules/prestablog/views/css/prestablog-back-office.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/modules/prestablog/views/css/admin.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/modules/ets_cfultimate/views/css/contact_form7_admin_all.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/modules/customfields/views/css/admin.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/js/jquery/plugins/chosen/jquery.chosen.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/admin518ddw65d/themes/default/css/vendor/nv.d3.css\" rel=\"stylesheet\" type=\"text/css\"/>
  
  <script type=\"text/javascript\">
var baseAdminDir = \"\\/admin518ddw65d\\/\";
var baseDir = \"\\/\";
var changeFormLanguageUrl = \"\\/multitienda\\/admin518ddw65d\\/index.php\\/configure\\/advanced\\/employees\\/change-form-language?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\";
var currency = {\"iso_code\":\"PEN\",\"sign\":\"PEN\",\"name\":\"Sol peruano\",\"format\":null};
var currency_specifications = {\"symbol\":[\",\",\".\",\";\",\"%\",\"-\",\"+\",\"E\",\"\\u00d7\",\"\\u2030\",\"\\u221e\",\"NaN\"],\"currencyCode\":\"PEN\",\"currencySymbol\":\"PEN\",\"positivePattern\":\"#,##0.00\\u00a0\\u00a4\",\"negativePattern\":\"-#,##0.00\\u00a0\\u00a4\",\"maxFractionDigits\":2,\"minFractionDigits\":2,\"groupingUsed\":true,\"primaryGroupSize\":3,\"secondaryGroupSize\":3};
var host_mode = false;
var iqitModulesAdditionalTabs = {\"ajaxUrl\":\"https:\\/\\/amanoyarns.com\\/admin518ddw65d\\/index.php?controller=AdminModules&token=c254d09bde7a3c43ba55640a47808738&ajax=1&configure=iqitadditionaltabs\"};
var iqitModulesSizeCharts = {\"ajaxUrl\":\"https:\\/\\/amanoyarns.com\\/admin518ddw65d\\/index.php?controller=AdminModules&token=c254d09bde7a3c43ba55640a47808738&ajax=1&configure=iqitsizecharts\"};
var number_specifications = {\"symbol\":[\",\",\".\",\";\",\"%\",\"-\",\"+\",\"E\",\"\\u00d7\",\"\\u2030\",\"\\u221e\",\"NaN\"],\"positivePattern\":\"#,##0.###\",\"negativePattern\":\"-#,##0.###\",\"maxFractionDigits\":3,\"minFractionDigits\":0,\"groupingUsed\":true,\"primaryGroupSize\":3,\"secondaryGroupSize\":3};
var show_new_customers = \"1\";
var show_new_messages = false;
var show_new_orders = \"1\";
</script>
<script type=\"text/javascript\" src=\"/modules/ps_faviconnotificationbo/views/js/favico.js\"></script>
<script type=\"text/javascript\" src=\"/modules/ps_faviconnotificationbo/views/js/ps_faviconnotificationbo.js\"></script>
<script type=\"text/javascript\" src=\"/admin518ddw65d/themes/new-theme/public/main.bundle.js\"></script>
<script type=\"text/javascript\" src=\"/js/jquery/plugins/jquery.chosen.js\"></script>
<script type=\"text/javascript\" src=\"/js/admin.js?v=1.7.6.7\"></script>
<script type=\"text/javascript\" src=\"/admin518ddw65d/themes/new-theme/public/cldr.bundle.js\"></script>
<script type=\"text/javascript\" src=\"/js/tools.js?v=1.7.6.7\"></script>
<script type=\"text/javascript\" src=\"/admin518ddw65d/public/bundle.js\"></script>
<script type=\"text/javascript\" src=\"/js/vendor/d3.v3.min.js\"></script>
<script type=\"text/javascript\" src=\"/admin518ddw65d/themes/default/js/vendor/nv.d3.min.js\"></script>

  <script>
  if (undefined !== ps_faviconnotificationbo) {
    ps_faviconnotificationbo.initialize({
      backgroundColor: '#DF0067',
      textColor: '#FFFFFF',
      notificationGetUrl: '/multitienda/admin518ddw65d/index.php/common/notifications?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c',
      CHECKBOX_ORDER: 1,
      CHECKBOX_CUSTOMER: 1,
      CHECKBOX_MESSAGE: 1,
      timer: 120000, // Refresh every 2 minutes
    });
  }
</script>


{% block stylesheets %}{% endblock %}{% block extra_stylesheets %}{% endblock %}</head>

<body class=\"lang-es adminperformance\">

  <header id=\"header\">

    <nav id=\"header_infos\" class=\"main-header\">
      <button class=\"btn btn-primary-reverse onclick btn-lg unbind ajax-spinner\"></button>

            <i class=\"material-icons js-mobile-menu\">menu</i>
      <a id=\"header_logo\" class=\"logo float-left\" href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminDashboard&amp;token=85fa14fe1cc9e34f50bf8fe2dbe383af\"></a>
      <span id=\"shop_version\">1.7.6.7</span>

      <div class=\"component\" id=\"quick-access-container\">
        <div class=\"dropdown quick-accesses\">
  <button class=\"btn btn-link btn-sm dropdown-toggle\" type=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" id=\"quick_select\">
    Acceso rápido
  </button>
  <div class=\"dropdown-menu\">
          <a class=\"dropdown-item\"
         href=\"https://amanoyarns.com/pe/admin518ddw65d/index.php?controller=AdminStats&amp;module=statscheckup&amp;token=253cda18de773939270c521081431117\"
                 data-item=\"Evaluación del catálogo\"
      >Evaluación del catálogo</a>
          <a class=\"dropdown-item\"
         href=\"https://amanoyarns.com/pe/admin518ddw65d/index.php/improve/modules/manage?token=8f7cbd628bc90d525d7e899bf7bfee1e\"
                 data-item=\"Módulos instalados\"
      >Módulos instalados</a>
          <a class=\"dropdown-item\"
         href=\"https://amanoyarns.com/pe/admin518ddw65d/index.php/sell/catalog/categories/new?token=8f7cbd628bc90d525d7e899bf7bfee1e\"
                 data-item=\"Nueva categoría\"
      >Nueva categoría</a>
          <a class=\"dropdown-item\"
         href=\"https://amanoyarns.com/pe/admin518ddw65d/index.php/sell/catalog/products/new?token=8f7cbd628bc90d525d7e899bf7bfee1e\"
                 data-item=\"Nuevo\"
      >Nuevo</a>
          <a class=\"dropdown-item\"
         href=\"https://amanoyarns.com/pe/admin518ddw65d/index.php?controller=AdminCartRules&amp;addcart_rule&amp;token=fb1ccab211a2c2db7969eeef885270b6\"
                 data-item=\"Nuevo cupón de descuento\"
      >Nuevo cupón de descuento</a>
          <a class=\"dropdown-item\"
         href=\"https://amanoyarns.com/pe/admin518ddw65d/index.php?controller=AdminOrders&amp;token=337a752ec09bda44b82130d8be68f564\"
                 data-item=\"Pedidos\"
      >Pedidos</a>
          <a class=\"dropdown-item\"
         href=\"https://amanoyarns.com/pe/admin518ddw65d/index.php?controller=AdminModules&amp;configure=prestablog&amp;module_name=prestablog&amp;token=c254d09bde7a3c43ba55640a47808738\"
                 data-item=\"PrestaBlog\"
      >PrestaBlog</a>
        <div class=\"dropdown-divider\"></div>
          <a
        class=\"dropdown-item js-quick-link\"
        href=\"#\"
        data-rand=\"190\"
        data-icon=\"icon-AdminAdvancedParameters\"
        data-method=\"add\"
        data-url=\"index.php/multitienda//configure/advanced/performance\"
        data-post-link=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminQuickAccesses&token=318d0b2b502e9217bcc4638d165f6395\"
        data-prompt-text=\"Por favor, renombre este acceso rápido:\"
        data-link=\"Rendimiento - Lista\"
      >
        <i class=\"material-icons\">add_circle</i>
        Añadir esta página a Acceso rápido
      </a>
        <a class=\"dropdown-item\" href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminQuickAccesses&token=318d0b2b502e9217bcc4638d165f6395\">
      <i class=\"material-icons\">settings</i>
      Administrar accesos rápidos
    </a>
  </div>
</div>
      </div>
      <div class=\"component\" id=\"header-search-container\">
        <form id=\"header_search\"
      class=\"bo_search_form dropdown-form js-dropdown-form collapsed\"
      method=\"post\"
      action=\"/admin518ddw65d/index.php?controller=AdminSearch&amp;token=5ac1c17ec53ded87186d84cc53ab63e0\"
      role=\"search\">
  <input type=\"hidden\" name=\"bo_search_type\" id=\"bo_search_type\" class=\"js-search-type\" />
    <div class=\"input-group\">
    <input type=\"text\" class=\"form-control js-form-search\" id=\"bo_query\" name=\"bo_query\" value=\"\" placeholder=\"Buscar (p. ej.: referencia de producto, nombre de cliente...)\">
    <div class=\"input-group-append\">
      <button type=\"button\" class=\"btn btn-outline-secondary dropdown-toggle js-dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
        toda la tienda
      </button>
      <div class=\"dropdown-menu js-items-list\">
        <a class=\"dropdown-item\" data-item=\"toda la tienda\" href=\"#\" data-value=\"0\" data-placeholder=\"¿Qué estás buscando?\" data-icon=\"icon-search\"><i class=\"material-icons\">search</i> toda la tienda</a>
        <div class=\"dropdown-divider\"></div>
        <a class=\"dropdown-item\" data-item=\"Catálogo\" href=\"#\" data-value=\"1\" data-placeholder=\"Nombre del producto, SKU, referencia...\" data-icon=\"icon-book\"><i class=\"material-icons\">store_mall_directory</i> Catálogo</a>
        <a class=\"dropdown-item\" data-item=\"Clientes por nombre\" href=\"#\" data-value=\"2\" data-placeholder=\"Email, nombre...\" data-icon=\"icon-group\"><i class=\"material-icons\">group</i> Clientes por nombre</a>
        <a class=\"dropdown-item\" data-item=\"Clientes por dirección IP\" href=\"#\" data-value=\"6\" data-placeholder=\"123.45.67.89\" data-icon=\"icon-desktop\"><i class=\"material-icons\">desktop_mac</i> Clientes por dirección IP</a>
        <a class=\"dropdown-item\" data-item=\"Pedidos\" href=\"#\" data-value=\"3\" data-placeholder=\"ID del pedido\" data-icon=\"icon-credit-card\"><i class=\"material-icons\">shopping_basket</i> Pedidos</a>
        <a class=\"dropdown-item\" data-item=\"Facturas\" href=\"#\" data-value=\"4\" data-placeholder=\"Número de factura\" data-icon=\"icon-book\"><i class=\"material-icons\">book</i> Facturas</a>
        <a class=\"dropdown-item\" data-item=\"Carritos\" href=\"#\" data-value=\"5\" data-placeholder=\"ID carrito\" data-icon=\"icon-shopping-cart\"><i class=\"material-icons\">shopping_cart</i> Carritos</a>
        <a class=\"dropdown-item\" data-item=\"Módulos\" href=\"#\" data-value=\"7\" data-placeholder=\"Nombre del módulo\" data-icon=\"icon-puzzle-piece\"><i class=\"material-icons\">extension</i> Módulos</a>
      </div>
      <button class=\"btn btn-primary\" type=\"submit\"><span class=\"d-none\">BÚSQUEDA</span><i class=\"material-icons\">search</i></button>
    </div>
  </div>
</form>

<script type=\"text/javascript\">
 \$(document).ready(function(){
    \$('#bo_query').one('click', function() {
    \$(this).closest('form').removeClass('collapsed');
  });
});
</script>
      </div>

              <div class=\"component hide-mobile-sm\" id=\"header-debug-mode-container\">
          <a class=\"link shop-state\"
             id=\"debug-mode\"
             data-toggle=\"pstooltip\"
             data-placement=\"bottom\"
             data-html=\"true\"
             title=\"<p class='text-left'><strong>Tu tienda está en modo debug.</strong></p><p class='text-left'>Se muestran todos los errores y mensajes PHP. Cuando ya no los necesites, <strong>desactiva</strong> este modo.</p>\"
             href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/performance/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\"
          >
            <i class=\"material-icons\">bug_report</i>
            <span>Modo depuración</span>
          </a>
        </div>
      
      
      <div class=\"component\" id=\"header-shop-list-container\">
          <div id=\"shop-list\" class=\"shop-list dropdown ps-dropdown stores\">
    <button class=\"btn btn-link\" type=\"button\" data-toggle=\"dropdown\">
      <span class=\"selected-item\">
        <i class=\"material-icons visibility\">visibility</i>
                  Amano Yarns Peru
                <i class=\"material-icons arrow-down\">arrow_drop_down</i>
      </span>
    </button>
    <div class=\"dropdown-menu dropdown-menu-right ps-dropdown-menu\">
      <ul class=\"items-list\"><li><a class=\"dropdown-item\" href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/performance/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c&amp;setShopContext=\">Todas las tiendas</a></li><li class=\"group\"><a class=\"dropdown-item\" href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/performance/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c&amp;setShopContext=g-1\">grupo Default</a></li><li class=\"shop\"><a class=\"dropdown-item \" href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/performance/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c&amp;setShopContext=s-1\">Amano Yarns</a><a class=\"link-shop\" href=\"https://amanoyarns.com/\" target=\"_blank\"><i class=\"material-icons\">&#xE8F4;</i></a></li><li class=\"shop active\"><a class=\"dropdown-item \" href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/performance/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c&amp;setShopContext=s-2\">Amano Yarns Peru</a><a class=\"link-shop\" href=\"https://amanoyarns.com/pe/\" target=\"_blank\"><i class=\"material-icons\">&#xE8F4;</i></a></li></ul>

    </div>
  </div>
      </div>

              <div class=\"component header-right-component\" id=\"header-notifications-container\">
          <div id=\"notif\" class=\"notification-center dropdown dropdown-clickable\">
  <button class=\"btn notification js-notification dropdown-toggle\" data-toggle=\"dropdown\">
    <i class=\"material-icons\">notifications_none</i>
    <span id=\"notifications-total\" class=\"count hide\">0</span>
  </button>
  <div class=\"dropdown-menu dropdown-menu-right js-notifs_dropdown\">
    <div class=\"notifications\">
      <ul class=\"nav nav-tabs\" role=\"tablist\">
                          <li class=\"nav-item\">
            <a
              class=\"nav-link active\"
              id=\"orders-tab\"
              data-toggle=\"tab\"
              data-type=\"order\"
              href=\"#orders-notifications\"
              role=\"tab\"
            >
              Pedidos<span id=\"_nb_new_orders_\"></span>
            </a>
          </li>
                                    <li class=\"nav-item\">
            <a
              class=\"nav-link \"
              id=\"customers-tab\"
              data-toggle=\"tab\"
              data-type=\"customer\"
              href=\"#customers-notifications\"
              role=\"tab\"
            >
              Clientes<span id=\"_nb_new_customers_\"></span>
            </a>
          </li>
                                    <li class=\"nav-item\">
            <a
              class=\"nav-link \"
              id=\"messages-tab\"
              data-toggle=\"tab\"
              data-type=\"customer_message\"
              href=\"#messages-notifications\"
              role=\"tab\"
            >
              Mensajes<span id=\"_nb_new_messages_\"></span>
            </a>
          </li>
                        </ul>

      <!-- Tab panes -->
      <div class=\"tab-content\">
                          <div class=\"tab-pane active empty\" id=\"orders-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              No hay pedidos nuevos por ahora :(<br>
              ¿Y si incluyes algunos descuentos de temporada?
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                                    <div class=\"tab-pane  empty\" id=\"customers-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              No hay clientes nuevos por ahora :(<br>
              ¿Ha enviado algún correo electrónico de adquisición últimamente?
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                                    <div class=\"tab-pane  empty\" id=\"messages-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              No hay mensajes nuevo por ahora.<br>
              Que no haya noticias, es de por sí una buena noticia, ¿verdad?
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                        </div>
    </div>
  </div>
</div>

  <script type=\"text/html\" id=\"order-notification-template\">
    <a class=\"notif\" href='order_url'>
      #_id_order_ -
      de <strong>_customer_name_</strong> (_iso_code_)_carrier_
      <strong class=\"float-sm-right\">_total_paid_</strong>
    </a>
  </script>

  <script type=\"text/html\" id=\"customer-notification-template\">
    <a class=\"notif\" href='customer_url'>
      #_id_customer_ - <strong>_customer_name_</strong>_company_ - registrado <strong>_date_add_</strong>
    </a>
  </script>

  <script type=\"text/html\" id=\"message-notification-template\">
    <a class=\"notif\" href='message_url'>
    <span class=\"message-notification-status _status_\">
      <i class=\"material-icons\">fiber_manual_record</i> _status_
    </span>
      - <strong>_customer_name_</strong> (_company_) - <i class=\"material-icons\">access_time</i> _date_add_
    </a>
  </script>
        </div>
      
      <div class=\"component\" id=\"header-employee-container\">
        <div class=\"dropdown employee-dropdown\">
  <div class=\"rounded-circle person\" data-toggle=\"dropdown\">
    <i class=\"material-icons\">account_circle</i>
  </div>
  <div class=\"dropdown-menu dropdown-menu-right\">
    <div class=\"employee-wrapper-avatar\">
      
      <span class=\"employee_avatar\"><img class=\"avatar rounded-circle\" src=\"https://profile.prestashop.com/ventasonline%40amanoyarns.com.jpg\" /></span>
      <span class=\"employee_profile\">Bienvenido de nuevo, Mia Bravo</span>
      <a class=\"dropdown-item employee-link profile-link\" href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/employees/3/edit?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\">
      <i class=\"material-icons\">settings</i>
      Tu perfil
    </a>
    </div>
    
    <p class=\"divider\"></p>
    <a class=\"dropdown-item\" href=\"https://www.prestashop.com/en/resources/documentations?utm_source=back-office&amp;utm_medium=profile&amp;utm_campaign=resources-en&amp;utm_content=download17\" target=\"_blank\"><i class=\"material-icons\">book</i> Recursos</a>
    <a class=\"dropdown-item\" href=\"https://www.prestashop.com/en/training?utm_source=back-office&amp;utm_medium=profile&amp;utm_campaign=training-en&amp;utm_content=download17\" target=\"_blank\"><i class=\"material-icons\">school</i> Formación</a>
    <a class=\"dropdown-item\" href=\"https://www.prestashop.com/en/experts?utm_source=back-office&amp;utm_medium=profile&amp;utm_campaign=expert-en&amp;utm_content=download17\" target=\"_blank\"><i class=\"material-icons\">person_pin_circle</i> Encontrar un Experto</a>
    <a class=\"dropdown-item\" href=\"https://addons.prestashop.com?utm_source=back-office&amp;utm_medium=profile&amp;utm_campaign=addons-en&amp;utm_content=download17\" target=\"_blank\"><i class=\"material-icons\">extension</i> Marketplace de PrestaShop</a>
    <a class=\"dropdown-item\" href=\"https://www.prestashop.com/en/contact?utm_source=back-office&amp;utm_medium=profile&amp;utm_campaign=help-center-en&amp;utm_content=download17\" target=\"_blank\"><i class=\"material-icons\">help</i> Centro de ayuda</a>
    <p class=\"divider\"></p>
    <a class=\"dropdown-item employee-link text-center\" id=\"header_logout\" href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminLogin&amp;logout=1&amp;token=74c6f35c05d4708e8f43c3bec6b521cc\">
      <i class=\"material-icons d-lg-none\">power_settings_new</i>
      <span>Cerrar sesión</span>
    </a>
  </div>
</div>
      </div>
    </nav>

      </header>

  <nav class=\"nav-bar d-none d-md-block\">
  <span class=\"menu-collapse\" data-toggle-url=\"/multitienda/admin518ddw65d/index.php/configure/advanced/employees/toggle-navigation?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\">
    <i class=\"material-icons\">chevron_left</i>
    <i class=\"material-icons\">chevron_left</i>
  </span>

  <ul class=\"main-menu\">

          
                
                
        
          <li class=\"link-levelone \" data-submenu=\"1\" id=\"tab-AdminDashboard\">
            <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminDashboard&amp;token=85fa14fe1cc9e34f50bf8fe2dbe383af\" class=\"link\" >
              <i class=\"material-icons\">trending_up</i> <span>Inicio</span>
            </a>
          </li>

        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"2\" id=\"tab-SELL\">
              <span class=\"title\">Vender</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"3\" id=\"subtab-AdminParentOrders\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminOrders&amp;token=337a752ec09bda44b82130d8be68f564\" class=\"link\">
                    <i class=\"material-icons mi-shopping_basket\">shopping_basket</i>
                    <span>
                    Pedidos
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-3\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"4\" id=\"subtab-AdminOrders\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminOrders&amp;token=337a752ec09bda44b82130d8be68f564\" class=\"link\"> Pedidos
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"5\" id=\"subtab-AdminInvoices\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/sell/orders/invoices/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Facturas
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"6\" id=\"subtab-AdminSlip\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminSlip&amp;token=32d2b953445e02b625a9414bf556b583\" class=\"link\"> Facturas por abono
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"7\" id=\"subtab-AdminDeliverySlip\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/sell/orders/delivery-slips/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Albaranes de entrega
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"8\" id=\"subtab-AdminCarts\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminCarts&amp;token=8d603d6b157b1dfdc05cec5c41df3fa1\" class=\"link\"> Carritos de compra
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"192\" id=\"subtab-deleteorderstab\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=deleteorderstab&amp;token=9236cf14ee4d38063d82fbf956537d25\" class=\"link\"> Delete Orders Free
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"9\" id=\"subtab-AdminCatalog\">
                  <a href=\"/multitienda/admin518ddw65d/index.php/sell/catalog/products?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\">
                    <i class=\"material-icons mi-store\">store</i>
                    <span>
                    Catálogo
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-9\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"10\" id=\"subtab-AdminProducts\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/sell/catalog/products?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Productos
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"11\" id=\"subtab-AdminCategories\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/sell/catalog/categories?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Categorías
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"12\" id=\"subtab-AdminTracking\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminTracking&amp;token=dc5f6ffdef00f749f9a7fde811d18969\" class=\"link\"> Monitoreo
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"13\" id=\"subtab-AdminParentAttributesGroups\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminAttributesGroups&amp;token=5e7d2cf12ed17474143674f72b829ce2\" class=\"link\"> Atributos y Características
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"16\" id=\"subtab-AdminParentManufacturers\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/sell/catalog/brands/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Marcas y Proveedores
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"19\" id=\"subtab-AdminAttachments\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminAttachments&amp;token=f12848fff910bbdb18358282e4532148\" class=\"link\"> Archivos
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"20\" id=\"subtab-AdminParentCartRules\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminCartRules&amp;token=fb1ccab211a2c2db7969eeef885270b6\" class=\"link\"> Descuentos
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"23\" id=\"subtab-AdminStockManagement\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/sell/stocks/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Stocks
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"24\" id=\"subtab-AdminParentCustomer\">
                  <a href=\"/multitienda/admin518ddw65d/index.php/sell/customers/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\">
                    <i class=\"material-icons mi-account_circle\">account_circle</i>
                    <span>
                    Clientes
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-24\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"25\" id=\"subtab-AdminCustomers\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/sell/customers/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Clientes
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"26\" id=\"subtab-AdminAddresses\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminAddresses&amp;token=3473c75244a1130ea0ce3e1b276eb578\" class=\"link\"> Direcciones
                              </a>
                            </li>

                                                                                                                          </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"28\" id=\"subtab-AdminParentCustomerThreads\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminCustomerThreads&amp;token=6970a63ff80a978d75df4fb59bb670ed\" class=\"link\">
                    <i class=\"material-icons mi-chat\">chat</i>
                    <span>
                    Servicio al Cliente
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-28\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"29\" id=\"subtab-AdminCustomerThreads\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminCustomerThreads&amp;token=6970a63ff80a978d75df4fb59bb670ed\" class=\"link\"> Servicio al Cliente
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"30\" id=\"subtab-AdminOrderMessage\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminOrderMessage&amp;token=4ea687869efb5b878d8a681627a2f8ea\" class=\"link\"> Mensajes de Pedidos
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"31\" id=\"subtab-AdminReturn\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminReturn&amp;token=24731f0726e68ee4238adac770908bca\" class=\"link\"> Devoluciones de mercancía
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"32\" id=\"subtab-AdminStats\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminStats&amp;token=253cda18de773939270c521081431117\" class=\"link\">
                    <i class=\"material-icons mi-assessment\">assessment</i>
                    <span>
                    Estadísticas
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"42\" id=\"tab-IMPROVE\">
              <span class=\"title\">Personalizar</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"43\" id=\"subtab-AdminParentModulesSf\">
                  <a href=\"/multitienda/admin518ddw65d/index.php/improve/modules/manage?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Módulos
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-43\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"44\" id=\"subtab-AdminModulesSf\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/improve/modules/manage?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Module Manager
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"48\" id=\"subtab-AdminParentModulesCatalog\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/improve/modules/catalog?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Catálogo de Módulos
                              </a>
                            </li>

                                                                                                                              
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"190\" id=\"subtab-AdminGeoIpRedirect\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminGeoIpRedirect&amp;token=b8bec064e350f0aa40ff2819b62a15f6\" class=\"link\"> Geo Ip Redirect
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"52\" id=\"subtab-AdminParentThemes\">
                  <a href=\"/multitienda/admin518ddw65d/index.php/improve/design/themes/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\">
                    <i class=\"material-icons mi-desktop_mac\">desktop_mac</i>
                    <span>
                    Diseño
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-52\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"126\" id=\"subtab-AdminThemesParent\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/improve/design/themes/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Tema y logotipo
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"54\" id=\"subtab-AdminThemesCatalog\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/improve/design/themes-catalog/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Catálogo de Temas
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"55\" id=\"subtab-AdminParentMailTheme\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/improve/design/mail_theme/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Tema Email
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"57\" id=\"subtab-AdminCmsContent\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/improve/design/cms-pages/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Páginas
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"58\" id=\"subtab-AdminModulesPositions\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/improve/design/modules/positions/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Posiciones
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"59\" id=\"subtab-AdminImages\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminImages&amp;token=d22032a75512a8bd57c66994fb1d7479\" class=\"link\"> Ajustes de imágenes
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"125\" id=\"subtab-AdminLinkWidget\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/modules/link-widget/list?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Link Widget
                              </a>
                            </li>

                                                                                                                              
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"139\" id=\"subtab-AdminIqitElementor\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminIqitElementor&amp;token=b14024e4f265a6366c791baeb751b9ef\" class=\"link\"> Iqit Elementor - Page builder
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"142\" id=\"subtab-IqitFrontThemeEditor\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=IqitFrontThemeEditor&amp;token=ff0f3a0545fe5244425c56689a4b9a5e\" class=\"link\"> IqitThemeEditor - Live
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"143\" id=\"subtab-AdminIqitThemeEditor\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminIqitThemeEditor&amp;token=17a7d2a70ea17a3e4d7325aa8f53ec35\" class=\"link\"> IqitThemeEditor - Backoffice
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"60\" id=\"subtab-AdminParentShipping\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminCarriers&amp;token=58052dbee7ce39b63b35875b9fa22e33\" class=\"link\">
                    <i class=\"material-icons mi-local_shipping\">local_shipping</i>
                    <span>
                    Transporte
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-60\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"61\" id=\"subtab-AdminCarriers\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminCarriers&amp;token=58052dbee7ce39b63b35875b9fa22e33\" class=\"link\"> Transportistas
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"62\" id=\"subtab-AdminShipping\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/improve/shipping/preferences?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Preferencias
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"63\" id=\"subtab-AdminParentPayment\">
                  <a href=\"/multitienda/admin518ddw65d/index.php/improve/payment/payment_methods?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\">
                    <i class=\"material-icons mi-payment\">payment</i>
                    <span>
                    Pago
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-63\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"64\" id=\"subtab-AdminPayment\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/improve/payment/payment_methods?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Métodos de pago
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"65\" id=\"subtab-AdminPaymentPreferences\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/improve/payment/preferences?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Preferencias
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"66\" id=\"subtab-AdminInternational\">
                  <a href=\"/multitienda/admin518ddw65d/index.php/improve/international/localization/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\">
                    <i class=\"material-icons mi-language\">language</i>
                    <span>
                    Internacional
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-66\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"67\" id=\"subtab-AdminParentLocalization\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/improve/international/localization/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Localización
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"72\" id=\"subtab-AdminParentCountries\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminZones&amp;token=9ab264a858ec3464bc7fc491f009b121\" class=\"link\"> Ubicaciones Geográficas
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"76\" id=\"subtab-AdminParentTaxes\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/improve/international/taxes/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Impuestos
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"79\" id=\"subtab-AdminTranslations\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/improve/international/translations/settings?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Traducciones
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title -active\" data-submenu=\"80\" id=\"tab-CONFIGURE\">
              <span class=\"title\">Configurar</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"81\" id=\"subtab-ShopParameters\">
                  <a href=\"/multitienda/admin518ddw65d/index.php/configure/shop/preferences/preferences?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\">
                    <i class=\"material-icons mi-settings\">settings</i>
                    <span>
                    Parámetros de la tienda
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-81\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"82\" id=\"subtab-AdminParentPreferences\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/shop/preferences/preferences?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Configuración
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"85\" id=\"subtab-AdminParentOrderPreferences\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/shop/order-preferences/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Configuración de Pedidos
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"88\" id=\"subtab-AdminPPreferences\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/shop/product-preferences/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Configuración de Productos
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"89\" id=\"subtab-AdminParentCustomerPreferences\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/shop/customer-preferences/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Ajustes sobre clientes
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"93\" id=\"subtab-AdminParentStores\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/shop/contacts/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Contacto
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"96\" id=\"subtab-AdminParentMeta\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/shop/seo-urls/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Tráfico &amp; SEO
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"100\" id=\"subtab-AdminParentSearchConf\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminSearchConf&amp;token=2f26805a947f6bc0a6c4bad2a9e5c50a\" class=\"link\"> Buscar
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"173\" id=\"subtab-AdminGeorules\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminGeorules&amp;token=e6ccee71dfe19da4a19883234880ff5c\" class=\"link\"> Geolocation rules
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                                                    
                <li class=\"link-levelone has_submenu -active open ul-open\" data-submenu=\"103\" id=\"subtab-AdminAdvancedParameters\">
                  <a href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/system-information/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\">
                    <i class=\"material-icons mi-settings_applications\">settings_applications</i>
                    <span>
                    Parámetros Avanzados
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_up
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-103\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"104\" id=\"subtab-AdminInformation\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/system-information/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Información
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo -active\" data-submenu=\"105\" id=\"subtab-AdminPerformance\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/performance/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Rendimiento
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"106\" id=\"subtab-AdminAdminPreferences\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/administration/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Administración
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"107\" id=\"subtab-AdminEmails\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/emails/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Dirección de correo electrónico
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"108\" id=\"subtab-AdminImport\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/import/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Importar
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"109\" id=\"subtab-AdminParentEmployees\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/employees/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Equipo
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"113\" id=\"subtab-AdminParentRequestSql\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/sql-requests/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Base de datos
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"116\" id=\"subtab-AdminLogs\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/logs/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Registros/Logs
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"117\" id=\"subtab-AdminWebservice\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/webservice-keys/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" class=\"link\"> Webservice
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"118\" id=\"subtab-AdminShopGroup\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminShopGroup&amp;token=11ffaac27b74f2a478d7b873426a70c8\" class=\"link\"> Multitienda
                              </a>
                            </li>

                                                                                                                              
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"151\" id=\"subtab-AdminExportProducts\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminExportProducts&amp;token=f4ebc1ade98b096fa74d847717548c47\" class=\"link\"> Export Products
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"191\" id=\"subtab-AdminKlaviyoPsConfig\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminKlaviyoPsConfig&amp;token=041a449b898191b1e0f5ab9a5b4cb05f\" class=\"link\">
                    <i class=\"material-icons mi-trending_up\">trending_up</i>
                    <span>
                    Klaviyo
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"130\" id=\"tab-AdminRevslider\">
              <span class=\"title\">Revolution Slider</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"131\" id=\"subtab-AdminRevsliderSliders\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminRevsliderSliders&amp;token=b8c88e1177a8d6ebf5ebd956b2a61efb\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Sliders
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"132\" id=\"subtab-AdminRevolutionsliderGlobalSettings\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminRevolutionsliderGlobalSettings&amp;token=5c8aee3dfd228162776324fd5429829d\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Global Settings
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"133\" id=\"subtab-AdminRevolutionsliderAddons\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminRevolutionsliderAddons&amp;token=6e380476e7a7489da8444b23afb6261a\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Addons
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"134\" id=\"subtab-AdminRevolutionsliderNavigation\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminRevolutionsliderNavigation&amp;token=fab4c1fa744cca7b873709330375e472\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Navigation
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"157\" id=\"tab-Management\">
              <span class=\"title\">CONTENT MANAGEMENT</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"158\" id=\"subtab-AdminPrestaBlog\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminPrestaBlog&amp;token=6ed9385adb994977eeee154d8d762727\" class=\"link\">
                    <i class=\"material-icons mi-library_books\">library_books</i>
                    <span>
                    PrestaBlog
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"171\" id=\"tab-AdminParentAddifyadvanceqty\">
              <span class=\"title\">Addify Advance Quantity</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"172\" id=\"subtab-AdminAddifyadvanceqty\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminAddifyadvanceqty&amp;token=eedf80f2becbec244946eea3255ec734\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Manage Advance Quantity
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"174\" id=\"tab-AdminContactFormUltimate\">
              <span class=\"title\">Contacto</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"175\" id=\"subtab-AdminContactFormUltimateDashboard\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminContactFormUltimateDashboard&amp;token=5fe0e6663c76873648af1bfbafa6e9b4\" class=\"link\">
                    <i class=\"material-icons mi-icon icon-home\">icon icon-home</i>
                    <span>
                    Contact dashboard
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"176\" id=\"subtab-AdminContactFormUltimateContactForm\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminContactFormUltimateContactForm&amp;token=1c54726964954dfc5f1debb8a65881c7\" class=\"link\">
                    <i class=\"material-icons mi-icon icon-envelope-o\">icon icon-envelope-o</i>
                    <span>
                    Contact forms
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"177\" id=\"subtab-AdminContactFormUltimateMessage\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminContactFormUltimateMessage&amp;token=e6be79383fb7523d4c9213ddf1c6847b\" class=\"link\">
                    <i class=\"material-icons mi-icon icon-comments\">icon icon-comments</i>
                    <span>
                    Messages
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"178\" id=\"subtab-AdminContactFormUltimateStatistics\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminContactFormUltimateStatistics&amp;token=ddea511612f1eedf5100fd88e08fd301\" class=\"link\">
                    <i class=\"material-icons mi-icon icon-line-chart\">icon icon-line-chart</i>
                    <span>
                    Statistics
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"179\" id=\"subtab-AdminContactFormUltimateIpBlacklist\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminContactFormUltimateIpBlacklist&amp;token=e8727394d34b9d42be8a7543ed5a1478\" class=\"link\">
                    <i class=\"material-icons mi-icon icon-user-times\">icon icon-user-times</i>
                    <span>
                    IP and Email blacklist
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"180\" id=\"subtab-AdminContactFormUltimateSetting\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminContactFormUltimateEmail&amp;token=eeee8b5e5a740958fe7b170dc97a0545\" class=\"link\">
                    <i class=\"material-icons mi-icon icon-cog\">icon icon-cog</i>
                    <span>
                    Setting
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-180\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"181\" id=\"subtab-AdminContactFormUltimateEmail\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminContactFormUltimateEmail&amp;token=eeee8b5e5a740958fe7b170dc97a0545\" class=\"link\"> Email templates
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"182\" id=\"subtab-AdminContactFormUltimateImportExport\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminContactFormUltimateImportExport&amp;token=90a02b5c34dea75f9c3dde377c0c045d\" class=\"link\"> Import/Export
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"183\" id=\"subtab-AdminContactFormUltimateIntegration\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminContactFormUltimateIntegration&amp;token=b66ce1b0d09401e9eb7d5b649b62acd9\" class=\"link\"> Integration
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"188\" id=\"tab-AdminCustomFields\">
              <span class=\"title\">Custom Fields</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"189\" id=\"subtab-AdminFields\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminFields&amp;token=d12760bba8e5ba6e9bdb510afa2cc3c0\" class=\"link\">
                    <i class=\"material-icons mi-content_paste\">content_paste</i>
                    <span>
                    Manage Custom Fields
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                          
        
            </ul>
  
</nav>

<div id=\"main-div\">
          
<div class=\"header-toolbar\">
  <div class=\"container-fluid\">

    
      <nav aria-label=\"Breadcrumb\">
        <ol class=\"breadcrumb\">
                      <li class=\"breadcrumb-item\">Parámetros Avanzados</li>
          
                      <li class=\"breadcrumb-item active\">
              <a href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/performance/?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\" aria-current=\"page\">Rendimiento</a>
            </li>
                  </ol>
      </nav>
    

    <div class=\"title-row\">
      
          <h1 class=\"title\">
            Rendimiento          </h1>
      

      
        <div class=\"toolbar-icons\">
          <div class=\"wrapper\">
            
                                                                                    <a
                  class=\"btn btn-primary  pointer\"                  id=\"page-header-desc-configuration-clear_cache\"
                  href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/performance/clear-cache?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\"                  title=\"Borrar la caché\"                >
                  <i class=\"material-icons\">delete</i>                  Borrar la caché
                </a>
                                                                  <a
                class=\"btn btn-outline-secondary \"
                id=\"page-header-desc-configuration-modules-list\"
                href=\"/multitienda/admin518ddw65d/index.php/improve/modules/catalog?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\"                title=\"Módulos recomendados\"
                              >
                Módulos recomendados
              </a>
            
            
                              <a class=\"btn btn-outline-secondary btn-help btn-sidebar\" href=\"#\"
                   title=\"Ayuda\"
                   data-toggle=\"sidebar\"
                   data-target=\"#right-sidebar\"
                   data-url=\"/multitienda/admin518ddw65d/index.php/common/sidebar/https%253A%252F%252Fhelp.prestashop.com%252Fes%252Fdoc%252FAdminPerformance%253Fversion%253D1.7.6.7%2526country%253Des/Ayuda?_token=aePcwNiCvhNbGCNDHyHQFFx0Nxj1137aYbSSp9xrD1c\"
                   id=\"product_form_open_help\"
                >
                  Ayuda
                </a>
                                    </div>
        </div>
      
    </div>
  </div>

  
    
</div>
      
      <div class=\"content-div  \">

        

                                                        
        <div class=\"row \">
          <div class=\"col-sm-12\">
            <div id=\"ajax_confirmation\" class=\"alert alert-success\" style=\"display: none;\"></div>


  {% block content_header %}{% endblock %}
                 {% block content %}{% endblock %}
                 {% block content_footer %}{% endblock %}
                 {% block sidebar_right %}{% endblock %}

            
          </div>
        </div>

      </div>
    </div>

  <div id=\"non-responsive\" class=\"js-non-responsive\">
  <h1>¡Oh no!</h1>
  <p class=\"mt-3\">
    La versión para móviles de esta página no está disponible todavía.
  </p>
  <p class=\"mt-2\">
    Por favor, utiliza un ordenador de escritorio hasta que esta página sea adaptada para dispositivos móviles.
  </p>
  <p class=\"mt-2\">
    Gracias.
  </p>
  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminDashboard&amp;token=85fa14fe1cc9e34f50bf8fe2dbe383af\" class=\"btn btn-primary py-1 mt-3\">
    <i class=\"material-icons\">arrow_back</i>
    Atrás
  </a>
</div>
  <div class=\"mobile-layer\"></div>

      <div id=\"footer\" class=\"bootstrap\">
    <script type=\"text/javascript\">
    var link_ajax = 'https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminModules&amp;token=c254d09bde7a3c43ba55640a47808738&amp;configure=ets_cfultimate&amp;tab_module=front_office_features&amp;module_name=ets_cfultimate';
    \$(document).ready(function () {
        \$.ajax({
            url: link_ajax,
            data: 'action=etsCfuGetCountMessageContactForm',
            type: 'post',
            dataType: 'json',
            success: function (json) {
                if (parseInt(json.count) > 0) {
                    if (\$('#subtab-AdminContactFormUltimateMessage span').length)
                        \$('#subtab-AdminContactFormUltimateMessage span').append('<span class=\"count_messages \">' + json.count + '</span>');
                    else
                        \$('#subtab-AdminContactFormUltimateMessage a').append('<span class=\"count_messages \">' + json.count + '</span>');
                } else {
                    if (\$('#subtab-AdminContactFormUltimateMessage span').length)
                        \$('#subtab-AdminContactFormUltimateMessage span').append('<span class=\"count_messages hide\">' + json.count + '</span>');
                    else
                        \$('#subtab-AdminContactFormUltimateMessage a').append('<span class=\"count_messages hide\">' + json.count + '</span>');
                }

            },
        });
    });
</script>
</div>
  

      <div class=\"bootstrap\">
      <div class=\"modal fade\" id=\"modal_addons_connect\" tabindex=\"-1\">
\t<div class=\"modal-dialog modal-md\">
\t\t<div class=\"modal-content\">
\t\t\t\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>
\t\t\t\t<h4 class=\"modal-title\"><i class=\"icon-puzzle-piece\"></i> <a target=\"_blank\" href=\"https://addons.prestashop.com/?utm_source=back-office&utm_medium=modules&utm_campaign=back-office-ES&utm_content=download\">PrestaShop Addons</a></h4>
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"modal-body\">
\t\t\t\t\t\t<div class=\"alert alert-warning\">
\t\t\t\tSi quieres utilizar completamente el panel AdminModules para obtener módulos gratuitos, debes habilitar la siguiente configuración en tu servidor:
\t\t\t\t<br />
\t\t\t\t- Activar la configuración allow_url_fopen de PHP<br />\t\t\t\t\t\t\t</div>
\t\t\t</div>


\t\t\t\t\t</div>
\t</div>
</div>

    </div>
  
{% block javascripts %}{% endblock %}{% block extra_javascripts %}{% endblock %}{% block translate_javascripts %}{% endblock %}</body>
</html>", "__string_template__2ee3ed67ad4686faa32c7ea66857e0794580e461ef44d17c1c51009c564f611a", "");
    }
}
