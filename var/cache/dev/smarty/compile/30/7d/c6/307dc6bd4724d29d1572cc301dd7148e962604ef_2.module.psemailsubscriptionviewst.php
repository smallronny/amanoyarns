<?php
/* Smarty version 3.1.33, created on 2021-03-08 20:47:53
  from 'module:psemailsubscriptionviewst' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6046d3c90bfe78_84184011',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '307dc6bd4724d29d1572cc301dd7148e962604ef' => 
    array (
      0 => 'module:psemailsubscriptionviewst',
      1 => 1611278172,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6046d3c90bfe78_84184011 (Smarty_Internal_Template $_smarty_tpl) {
?><!-- begin /home2/amanoyarns/public_html/multitienda/themes/warehouse/modules/ps_emailsubscription/views/templates/hook/ps_emailsubscription.tpl -->



<div class="col col-md pb-5 ps-emailsubscription-block">
 
     <h5 class="block-title"><span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Suscríbete','d'=>'Shop.Forms.Labels'),$_smarty_tpl ) );?>
</span></h5>
    <label class="col-auto "><span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Subscribe to our newsletter 
        for updates and special offers','d'=>'Shop.Forms.Labels'),$_smarty_tpl ) );?>
</span>
    </label>
    


    <form action="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0], array( array('entity'=>'index','params'=>array('fc'=>'module','module'=>'iqitemailsubscriptionconf','controller'=>'subscription')),$_smarty_tpl ) );?>
" method="post">

               <div class="input-group newsletter-input-group inpt_newsletter_amano">

                    <input

                        name="email"

                        type="email"

                        value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['value']->value, ENT_QUOTES, 'UTF-8');?>
"

                        class="form-control input-subscription"

                        placeholder="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'E-mail *','d'=>'Shop.Forms.Labels'),$_smarty_tpl ) );?>
"

                        aria-label="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Your email address','d'=>'Shop.Forms.Labels'),$_smarty_tpl ) );?>
"

                    >

                </div>
                <div class="pt-4">
                    <button class="btn btn-primary btn_newsletter text-white" name="submitNewsletter" type="submit" aria-label="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Subscribe','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>
"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'SUBSCRIBE','d'=>'Shop.Forms.Labels'),$_smarty_tpl ) );?>
 </button>
                </div>

        <?php if (isset($_smarty_tpl->tpl_vars['id_module']->value)) {?>

            <div class="mt-2 text-muted"> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayGDPRConsent','id_module'=>$_smarty_tpl->tpl_vars['id_module']->value),$_smarty_tpl ) );?>
</div>

        <?php }?>

                <input type="hidden" name="action" value="0">

    </form>

</div>









<!-- end /home2/amanoyarns/public_html/multitienda/themes/warehouse/modules/ps_emailsubscription/views/templates/hook/ps_emailsubscription.tpl --><?php }
}
