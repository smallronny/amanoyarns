<?php
/* Smarty version 3.1.33, created on 2021-03-08 20:47:53
  from 'module:iqitmegamenuviewstemplate' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6046d3c9037e05_18230494',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e077dd2170956816de1e46af35296e5cbbf8e702' => 
    array (
      0 => 'module:iqitmegamenuviewstemplate',
      1 => 1611278170,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6046d3c9037e05_18230494 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
  'mobile_links' => 
  array (
    'compiled_filepath' => '/home2/amanoyarns/public_html/multitienda/var/cache/dev/smarty/compile/e0/77/dd/e077dd2170956816de1e46af35296e5cbbf8e702_2.module.iqitmegamenuviewstemplate.cache.php',
    'uid' => 'e077dd2170956816de1e46af35296e5cbbf8e702',
    'call_name' => 'smarty_template_function_mobile_links_15774861546046d3c9031c77_55546561',
  ),
));
$_smarty_tpl->compiled->nocache_hash = '15774861546046d3c9031c77_55546561';
?>
<!-- begin /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/mobile_menu.tpl -->



<ul class="closeMenuMobile py-3" style="
    border-bottom: 1px solid;
">
<li class="menu-close row justify-content-center"> 
<div class="col-8 text-left pl-5">
    <img class="logo img-responsive" src="/img/logo-amano.png" alt="Amano" style="
    text-align: center;
    width: 50px;
">
    </div>
    <div class="col-4">
        <a class="close" data-toggle="dropdown" aria-label="Close">
          <span aria-hidden="true">×</span>
        </a>
    </div>
</li>
    </ul>








<?php if (isset($_smarty_tpl->tpl_vars['menu']->value)) {?>

	<?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'mobile_links', array('nodes'=>$_smarty_tpl->tpl_vars['menu']->value,'first'=>true), false);?>


<?php }?>

<!-- end /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/mobile_menu.tpl --><?php }
/* smarty_template_function_mobile_links_15774861546046d3c9031c77_55546561 */
if (!function_exists('smarty_template_function_mobile_links_15774861546046d3c9031c77_55546561')) {
function smarty_template_function_mobile_links_15774861546046d3c9031c77_55546561(Smarty_Internal_Template $_smarty_tpl,$params) {
$params = array_merge(array('nodes'=>array(),'first'=>false), $params);
foreach ($params as $key => $value) {
$_smarty_tpl->tpl_vars[$key] = new Smarty_Variable($value, $_smarty_tpl->isRenderingCache);
}
?>

 

 
	<?php if (count($_smarty_tpl->tpl_vars['nodes']->value)) {
if (!$_smarty_tpl->tpl_vars['first']->value) {?><ul><?php }
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['nodes']->value, 'node');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['node']->value) {
if (isset($_smarty_tpl->tpl_vars['node']->value['title'])) {?><li><?php if (isset($_smarty_tpl->tpl_vars['node']->value['children'])) {?><span class="mm-expand"><i class="fa fa-angle-down expand-icon" aria-hidden="true"></i><i class="fa fa-angle-up close-icon" aria-hidden="true"></i></span><?php }?><a <?php if (isset($_smarty_tpl->tpl_vars['node']->value['href'])) {?> href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['node']->value['href'], ENT_QUOTES, 'UTF-8');?>
" <?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['node']->value['title'], ENT_QUOTES, 'UTF-8');?>
</a><?php if (isset($_smarty_tpl->tpl_vars['node']->value['children'])) {
$_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'mobile_links', array('nodes'=>$_smarty_tpl->tpl_vars['node']->value['children'],'first'=>false), false);
}?></li><?php }
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
if (!$_smarty_tpl->tpl_vars['first']->value) {?></ul><?php }
}?>



<?php
}}
/*/ smarty_template_function_mobile_links_15774861546046d3c9031c77_55546561 */
}
