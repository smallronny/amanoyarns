<?php
/* Smarty version 3.1.33, created on 2021-03-08 20:47:53
  from 'module:iqitcookielawviewstemplat' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6046d3c9126ff8_26955393',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b7588a77287e3c02dfc5dfe6fd3a17abf03f7e2e' => 
    array (
      0 => 'module:iqitcookielawviewstemplat',
      1 => 1611278170,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6046d3c9126ff8_26955393 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->compiled->nocache_hash = '17236236406046d3c9125a41_04214584';
?>
<!-- begin /home2/amanoyarns/public_html/multitienda/modules/iqitcookielaw/views/templates/hook/iqitcookielaw.tpl -->


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_7610633146046d3c9126079_94026232', 'iqitcookielaw');
?>




 

<!-- end /home2/amanoyarns/public_html/multitienda/modules/iqitcookielaw/views/templates/hook/iqitcookielaw.tpl --><?php }
/* {block 'iqitcookielaw'} */
class Block_7610633146046d3c9126079_94026232 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'iqitcookielaw' => 
  array (
    0 => 'Block_7610633146046d3c9126079_94026232',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


<div id="iqitcookielaw" class="p-3">

    <div class="row justify-content-center p-4"> 

             <div class="col-md-8 col-12 align-self-center">

<?php echo $_smarty_tpl->tpl_vars['txt']->value;?>


 </div>

          <div class="col-md-2 col-6 align-self-center text-center">



<button class="btn btn-block btn-cok" id="iqitcookielaw-accept"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Accept','mod'=>'iqitcookielaw'),$_smarty_tpl ) );?>
</button>

  </div>

          <div class="col-md-2 col-6 align-self-center text-center">

<a href="/content/11-privacy-policy-and-cookies" id="iqitcookielaw-read" class="btn-cook btn btn-block btn-primary"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Read More','mod'=>'iqitcookielaw'),$_smarty_tpl ) );?>
</a>

  </div>

           </div>

</div>

<?php
}
}
/* {/block 'iqitcookielaw'} */
}
