<?php
/* Smarty version 3.1.33, created on 2021-03-08 20:49:04
  from '/home2/amanoyarns/public_html/multitienda/modules/lgcookieslaw/views/templates/hook/cookieslaw.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6046d410af3211_83390071',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f63f910f3889a627ff0085b1861396a15410139e' => 
    array (
      0 => '/home2/amanoyarns/public_html/multitienda/modules/lgcookieslaw/views/templates/hook/cookieslaw.tpl',
      1 => 1613758969,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6046d410af3211_83390071 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['lgcookieslaw_position']->value == 3) {?>
<div id="lgcookieslaw_banner" class="lgcookieslaw_banner  lgcookieslaw_message_floating">
    <div class="container">
        <div class="lgcookieslaw_message"><?php if (version_compare(@constant('_PS_VERSION_'),'1.7.0','>=')) {
ob_start();
echo $_smarty_tpl->tpl_vars['cookie_message']->value;
$_prefixVariable1 = ob_get_clean();
echo htmlspecialchars(strip_tags($_prefixVariable1), ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars(stripslashes(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['cookie_message']->value,'quotes','UTF-8' ))), ENT_QUOTES, 'UTF-8');
}?>
            <a id="lgcookieslaw_info" <?php if (isset($_smarty_tpl->tpl_vars['cms_target']->value) && $_smarty_tpl->tpl_vars['cms_target']->value) {?> target="_blank" <?php }?> href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['cms_link']->value,'quotes','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" >
                <?php echo htmlspecialchars(stripslashes(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['button2']->value,'quotes','UTF-8' ))), ENT_QUOTES, 'UTF-8');?>

            </a>            
            <?php if ($_smarty_tpl->tpl_vars['lgcookieslaw_setting_button']->value != 1) {?>
            <a onclick="customizeCookies()">
                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'customize cookies','mod'=>'lgcookieslaw'),$_smarty_tpl ) );?>

            </a>
            <?php }?>
        </div>
        <div class="lgcookieslaw_button_container">
            <?php if ($_smarty_tpl->tpl_vars['lgcookieslaw_setting_button']->value == 1) {?>
            <a class="lgcookieslaw_customize_cookies" onclick="customizeCookies()">
                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'customize cookies','mod'=>'lgcookieslaw'),$_smarty_tpl ) );?>

            </a>
            <?php }?>
            <button id="lgcookieslaw_accept" class="lgcookieslaw_btn<?php if ($_smarty_tpl->tpl_vars['lgcookieslaw_setting_button']->value != 1) {?> lgcookieslaw_btn_accept_big<?php }?>" onclick="closeinfo(true, true)"><?php echo htmlspecialchars(stripslashes(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['button1']->value,'quotes','UTF-8' ))), ENT_QUOTES, 'UTF-8');?>
</button>
        </div>
    </div>
</div>
<?php } else { ?>
<div id="lgcookieslaw_banner" class="lgcookieslaw_banner">
    <div class="container">
        <div class="lgcookieslaw_message"><?php if (version_compare(@constant('_PS_VERSION_'),'1.7.0','>=')) {
ob_start();
echo $_smarty_tpl->tpl_vars['cookie_message']->value;
$_prefixVariable2 = ob_get_clean();
echo htmlspecialchars(strip_tags($_prefixVariable2), ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars(stripslashes(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['cookie_message']->value,'quotes','UTF-8' ))), ENT_QUOTES, 'UTF-8');
}?>
            <a id="lgcookieslaw_info" <?php if (isset($_smarty_tpl->tpl_vars['cms_target']->value) && $_smarty_tpl->tpl_vars['cms_target']->value) {?> target="_blank" <?php }?> href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['cms_link']->value,'quotes','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" >
                <?php echo htmlspecialchars(stripslashes(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['button2']->value,'quotes','UTF-8' ))), ENT_QUOTES, 'UTF-8');?>

            </a>            
            <a class="lgcookieslaw_customize_cookies" onclick="customizeCookies()">
                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'customize cookies','mod'=>'lgcookieslaw'),$_smarty_tpl ) );?>

            </a>
        </div>
        <div class="lgcookieslaw_button_container">
            <button id="lgcookieslaw_accept" class="lgcookieslaw_btn lgcookieslaw_btn_accept" onclick="closeinfo(true, true)"><?php echo htmlspecialchars(stripslashes(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['button1']->value,'quotes','UTF-8' ))), ENT_QUOTES, 'UTF-8');?>
</button>
        </div>
    </div>
</div>
<?php }?>
<div style="display: none;" id="lgcookieslaw-modal">
    <div class="lgcookieslaw-modal-body">
        <h2><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Cookies configuration','mod'=>'lgcookieslaw'),$_smarty_tpl ) );?>
</h2>
        <div class="lgcookieslaw-section">
            <div class="lgcookieslaw-section-name">
                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Customization','mod'=>'lgcookieslaw'),$_smarty_tpl ) );?>

            </div>
            <div class="lgcookieslaw-section-checkbox">
                <label class="lgcookieslaw_switch">
                    <div class="lgcookieslaw_slider_option_left"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'No','mod'=>'lgcookieslaw'),$_smarty_tpl ) );?>
</div>
                    <input type="checkbox" id="lgcookieslaw-cutomization-enabled" <?php if ($_smarty_tpl->tpl_vars['third_paries']->value) {?>checked="checked"<?php }?>>
                    <span class="lgcookieslaw_slider<?php if ($_smarty_tpl->tpl_vars['third_paries']->value) {?> lgcookieslaw_slider_checked<?php }?>"></span>
                    <div class="lgcookieslaw_slider_option_right"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Yes','mod'=>'lgcookieslaw'),$_smarty_tpl ) );?>
</div>
                </label>
            </div>
            <div class="lgcookieslaw-section-description">
                <?php echo $_smarty_tpl->tpl_vars['cookie_additional']->value;?>
            </div>
        </div>
        <div class="lgcookieslaw-section">
            <div class="lgcookieslaw-section-name">
                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Functional (required)','mod'=>'lgcookieslaw'),$_smarty_tpl ) );?>

            </div>
            <div class="lgcookieslaw-section-checkbox">
                <label class="lgcookieslaw_switch">
                    <div class="lgcookieslaw_slider_option_left"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'No','mod'=>'lgcookieslaw'),$_smarty_tpl ) );?>
</div>
                    <input type="checkbox" checked="checked" disabled="disabled">
                    <span class="lgcookieslaw_slider lgcookieslaw_slider_checked"></span>
                    <div class="lgcookieslaw_slider_option_right"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Yes','mod'=>'lgcookieslaw'),$_smarty_tpl ) );?>
</div>
                </label>
            </div>
            <div class="lgcookieslaw-section-description">
                <?php echo $_smarty_tpl->tpl_vars['cookie_required']->value;?>
            </div>
        </div>
    </div>
    <div class="lgcookieslaw-modal-footer">
        <div class="lgcookieslaw-modal-footer-left">
            <button class="btn" id="lgcookieslaw-close"> > <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Cancel','mod'=>'lgcookieslaw'),$_smarty_tpl ) );?>
</button>
        </div>
        <div class="lgcookieslaw-modal-footer-right">
            <button class="btn" id="lgcookieslaw-save" onclick="closeinfo(true)"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Accept and continue','mod'=>'lgcookieslaw'),$_smarty_tpl ) );?>
</button>
        </div>
    </div>
</div>
<div class="lgcookieslaw_overlay"></div>
<?php }
}
