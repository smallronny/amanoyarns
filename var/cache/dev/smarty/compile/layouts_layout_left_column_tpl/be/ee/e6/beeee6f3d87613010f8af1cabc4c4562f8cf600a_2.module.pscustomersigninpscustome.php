<?php
/* Smarty version 3.1.33, created on 2021-03-08 20:48:05
  from 'module:pscustomersigninpscustome' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6046d3d58d7352_37257702',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'beeee6f3d87613010f8af1cabc4c4562f8cf600a' => 
    array (
      0 => 'module:pscustomersigninpscustome',
      1 => 1611278172,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6046d3d58d7352_37257702 (Smarty_Internal_Template $_smarty_tpl) {
?><!-- begin /home2/amanoyarns/public_html/multitienda/themes/warehouse/modules/ps_customersignin/ps_customersignin-mobile.tpl --><?php if ($_smarty_tpl->tpl_vars['logged']->value) {
if ($_smarty_tpl->tpl_vars['customer']->value instanceof customer) {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'truncate' ][ 0 ], array( $_smarty_tpl->tpl_vars['customer']->value->firstname,15,'...' )), ENT_QUOTES, 'UTF-8');?>

<?php } else {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'truncate' ][ 0 ], array( $_smarty_tpl->tpl_vars['customer']->value['firstname'],15,'...' )), ENT_QUOTES, 'UTF-8');?>

<?php }
} else { ?>
  <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Sign in','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>

<?php }?>
<!-- end /home2/amanoyarns/public_html/multitienda/themes/warehouse/modules/ps_customersignin/ps_customersignin-mobile.tpl --><?php }
}
