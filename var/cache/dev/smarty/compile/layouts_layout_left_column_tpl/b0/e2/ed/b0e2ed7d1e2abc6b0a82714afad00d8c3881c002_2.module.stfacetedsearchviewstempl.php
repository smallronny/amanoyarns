<?php
/* Smarty version 3.1.33, created on 2021-03-08 20:49:56
  from 'module:stfacetedsearchviewstempl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6046d4441aef00_34953899',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b0e2ed7d1e2abc6b0a82714afad00d8c3881c002' => 
    array (
      0 => 'module:stfacetedsearchviewstempl',
      1 => 1615220977,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
    'module:stfacetedsearch/views/templates/listing/center_column.tpl' => 1,
  ),
),false)) {
function content_6046d4441aef00_34953899 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>
<!-- begin /home2/amanoyarns/public_html/multitienda/modules/stfacetedsearch/views/templates/listing/category.tpl -->
<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12804552116046d4441ad429_63250388', 'product_list_top');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1914368246046d4441ae604_27653139', 'product_list_active_filters');
?>
<!-- end /home2/amanoyarns/public_html/multitienda/modules/stfacetedsearch/views/templates/listing/category.tpl --><?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, 'catalog/listing/category.tpl');
}
/* {block 'product_list_top'} */
class Block_12804552116046d4441ad429_63250388 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'product_list_top' => 
  array (
    0 => 'Block_12804552116046d4441ad429_63250388',
  ),
);
public $prepend = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<?php $_smarty_tpl->_subTemplateRender('module:stfacetedsearch/views/templates/listing/center_column.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
/* {/block 'product_list_top'} */
/* {block 'product_list_active_filters'} */
class Block_1914368246046d4441ae604_27653139 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'product_list_active_filters' => 
  array (
    0 => 'Block_1914368246046d4441ae604_27653139',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'product_list_active_filters'} */
}
