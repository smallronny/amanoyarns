<?php
/* Smarty version 3.1.33, created on 2021-03-08 20:48:17
  from '/home2/amanoyarns/public_html/multitienda/themes/warehousechild/templates/catalog/product.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6046d3e1079996_12858310',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4bd2ec53053d2c4e1bb46363406a89a99eacb3c2' => 
    array (
      0 => '/home2/amanoyarns/public_html/multitienda/themes/warehousechild/templates/catalog/product.tpl',
      1 => 1611278172,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:catalog/_partials/product-cover-thumbnails.tpl' => 1,
    'file:catalog/_partials/product-prices.tpl' => 2,
    'file:catalog/_partials/product-customization.tpl' => 1,
    'file:catalog/_partials/product-variants.tpl' => 1,
    'file:catalog/_partials/miniatures/pack-product.tpl' => 1,
    'file:catalog/_partials/product-add-to-cart.tpl' => 1,
    'file:catalog/_partials/product-discounts.tpl' => 1,
    'file:catalog/_partials/product-additional-info.tpl' => 1,
    'file:catalog/_partials/_product_partials/product-tabs-h.tpl' => 1,
    'file:catalog/_partials/_product_partials/product-tabs-sections.tpl' => 1,
    'file:catalog/_partials/miniatures/product-small.tpl' => 1,
    'file:catalog/_partials/miniatures/product.tpl' => 1,
    'file:catalog/_partials/product-images-modal.tpl' => 1,
  ),
),false)) {
function content_6046d3e1079996_12858310 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>





<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6729711336046d3e104dee3_42047774', 'head_seo');
?>






<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3185912086046d3e104eb26_42464648', 'head_og_tags');
?>






<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10204963746046d3e1051213_64577843', 'head');
?>






<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13372928906046d3e10576f9_71551028', 'content');
?>


<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['layout']->value);
}
/* {block 'head_seo'} */
class Block_6729711336046d3e104dee3_42047774 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'head_seo' => 
  array (
    0 => 'Block_6729711336046d3e104dee3_42047774',
  ),
);
public $prepend = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


    <link rel="canonical" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['canonical_url'], ENT_QUOTES, 'UTF-8');?>
">

<?php
}
}
/* {/block 'head_seo'} */
/* {block 'head_og_tags'} */
class Block_3185912086046d3e104eb26_42464648 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'head_og_tags' => 
  array (
    0 => 'Block_3185912086046d3e104eb26_42464648',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


    <meta property="og:type" content="product">

    <meta property="og:url" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['current_url'], ENT_QUOTES, 'UTF-8');?>
">

    <meta property="og:title" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['meta']['title'], ENT_QUOTES, 'UTF-8');?>
">

    <meta property="og:site_name" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['name'], ENT_QUOTES, 'UTF-8');?>
">

    <meta property="og:description" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['meta']['description'], ENT_QUOTES, 'UTF-8');?>
">

    <meta property="og:image" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['cover']['large']['url'], ENT_QUOTES, 'UTF-8');?>
">

    <meta property="og:image:width" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['cover']['large']['width'], ENT_QUOTES, 'UTF-8');?>
">

    <meta property="og:image:height" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['cover']['large']['height'], ENT_QUOTES, 'UTF-8');?>
">

<?php
}
}
/* {/block 'head_og_tags'} */
/* {block 'head'} */
class Block_10204963746046d3e1051213_64577843 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'head' => 
  array (
    0 => 'Block_10204963746046d3e1051213_64577843',
  ),
);
public $append = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


    <?php if ($_smarty_tpl->tpl_vars['product']->value['show_price']) {?>

        <meta property="product:pretax_price:amount" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['price_tax_exc'], ENT_QUOTES, 'UTF-8');?>
">

        <meta property="product:pretax_price:currency" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['currency']->value['iso_code'], ENT_QUOTES, 'UTF-8');?>
">

        <meta property="product:price:amount" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['price_amount'], ENT_QUOTES, 'UTF-8');?>
">

        <meta property="product:price:currency" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['currency']->value['iso_code'], ENT_QUOTES, 'UTF-8');?>
">

    <?php }?>

    <?php if (isset($_smarty_tpl->tpl_vars['product']->value['weight']) && ($_smarty_tpl->tpl_vars['product']->value['weight'] != 0)) {?>

        <meta property="product:weight:value" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['weight'], ENT_QUOTES, 'UTF-8');?>
">

        <meta property="product:weight:units" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['weight_unit'], ENT_QUOTES, 'UTF-8');?>
">

    <?php }?>



    <?php if ($_smarty_tpl->tpl_vars['iqitTheme']->value['bread_bg_category']) {?>

        <?php $_smarty_tpl->_assignInScope('categoryImage', "img/c/".((string)$_smarty_tpl->tpl_vars['product']->value['id_category_default'])."-category_default.jpg");?>

        <?php if (file_exists($_smarty_tpl->tpl_vars['categoryImage']->value)) {?>

            <style> #wrapper .breadcrumb{  background-image: url('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getCatImageLink($_smarty_tpl->tpl_vars['product']->value['category'],$_smarty_tpl->tpl_vars['product']->value['id_category_default'],'category_default'), ENT_QUOTES, 'UTF-8');?>
'); }</style>

        <?php }?>

    <?php }?>



<?php
}
}
/* {/block 'head'} */
/* {block 'product_cover_thumbnails'} */
class Block_2382211876046d3e105a062_10497925 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                                <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/product-cover-thumbnails.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                            <?php
}
}
/* {/block 'product_cover_thumbnails'} */
/* {block 'after_cover_thumbnails'} */
class Block_4462780276046d3e105c3a8_67583088 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                                <div class="after-cover-tumbnails text-center"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayAfterProductThumbs'),$_smarty_tpl ) );?>
</div>

                            <?php
}
}
/* {/block 'after_cover_thumbnails'} */
/* {block 'after_cover_thumbnails2'} */
class Block_19062038036046d3e105ce99_45883165 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                                <div class="after-cover-tumbnails2 mt-4"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayAfterProductThumbs2'),$_smarty_tpl ) );?>
</div>

                            <?php
}
}
/* {/block 'after_cover_thumbnails2'} */
/* {block 'page_content'} */
class Block_16644898446046d3e1059dc9_38043299 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>




                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2382211876046d3e105a062_10497925', 'product_cover_thumbnails', $this->tplIndex);
?>




                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_4462780276046d3e105c3a8_67583088', 'after_cover_thumbnails', $this->tplIndex);
?>




                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_19062038036046d3e105ce99_45883165', 'after_cover_thumbnails2', $this->tplIndex);
?>


                        <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_14516724056046d3e1059af4_27909293 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                    <section class="page-content" id="content">

                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_16644898446046d3e1059dc9_38043299', 'page_content', $this->tplIndex);
?>


                    </section>

                <?php
}
}
/* {/block 'page_content_container'} */
/* {block 'product_brand_below'} */
class Block_19445950966046d3e105dfc5_27303677 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                            <?php if ($_smarty_tpl->tpl_vars['iqitTheme']->value['pp_man_logo'] == 'next-title') {?>

                                <?php if (isset($_smarty_tpl->tpl_vars['product_manufacturer']->value->id)) {?>

                                    <?php if (isset($_smarty_tpl->tpl_vars['manufacturer_image_url']->value)) {?>

                                        <meta itemprop="brand" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_manufacturer']->value->name, ENT_QUOTES, 'UTF-8');?>
">

                                        <div class="product-manufacturer product-manufacturer-next float-right">

                                            <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_brand_url']->value, ENT_QUOTES, 'UTF-8');?>
">

                                                <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['manufacturer_image_url']->value, ENT_QUOTES, 'UTF-8');?>
"

                                                     class="img-fluid  manufacturer-logo" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_manufacturer']->value->name, ENT_QUOTES, 'UTF-8');?>
" />

                                            </a>

                                        </div>

                                    <?php }?>

                                <?php }?>

                            <?php }?>

                        <?php
}
}
/* {/block 'product_brand_below'} */
/* {block 'page_title'} */
class Block_3796832946046d3e10607f8_24435276 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8');
}
}
/* {/block 'page_title'} */
/* {block 'page_header'} */
class Block_6157630906046d3e1060524_04628572 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                        <h1 class="h1 page-title" itemprop="name"><span><?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3796832946046d3e10607f8_24435276', 'page_title', $this->tplIndex);
?>
</span></h1>

                    <?php
}
}
/* {/block 'page_header'} */
/* {block 'product_brand_below'} */
class Block_17292199566046d3e1061391_97875661 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                            <?php if ($_smarty_tpl->tpl_vars['iqitTheme']->value['pp_man_logo'] == 'title') {?>

                                <?php if (isset($_smarty_tpl->tpl_vars['product_manufacturer']->value->id)) {?>

                                    <meta itemprop="brand" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_manufacturer']->value->name, ENT_QUOTES, 'UTF-8');?>
">

                                        <?php if (isset($_smarty_tpl->tpl_vars['manufacturer_image_url']->value)) {?>

                                            <div class="product-manufacturer mb-3">

                                            <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_brand_url']->value, ENT_QUOTES, 'UTF-8');?>
">

                                                <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['manufacturer_image_url']->value, ENT_QUOTES, 'UTF-8');?>
"

                                                     class="img-fluid  manufacturer-logo" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_manufacturer']->value->name, ENT_QUOTES, 'UTF-8');?>
" />

                                            </a>

                                            </div>

                                        <?php } else { ?>

                                            <label class="label"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Brand','d'=>'Shop.Theme.Catalog'),$_smarty_tpl ) );?>
:</label>

                                            <span>

            <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_brand_url']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_manufacturer']->value->name, ENT_QUOTES, 'UTF-8');?>
</a>

          </span>

                                        <?php }?>



                                <?php }?>

                            <?php }?>

                            <?php if ($_smarty_tpl->tpl_vars['iqitTheme']->value['pp_man_logo'] == 'next-title') {?>

                                <?php if (isset($_smarty_tpl->tpl_vars['product_manufacturer']->value->id)) {?>

                                    <?php if (!isset($_smarty_tpl->tpl_vars['manufacturer_image_url']->value)) {?>

                                        <meta itemprop="brand" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_manufacturer']->value->name, ENT_QUOTES, 'UTF-8');?>
">

                                        <label class="label"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Brand','d'=>'Shop.Theme.Catalog'),$_smarty_tpl ) );?>
:</label>

                                        <span>

                                        <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_brand_url']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_manufacturer']->value->name, ENT_QUOTES, 'UTF-8');?>
</a>

                                        </span>

                                    <?php }?>

                                     <?php if ($_smarty_tpl->tpl_vars['product']->value['ean13']) {?>

        <meta itemprop="gtin13" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['ean13'], ENT_QUOTES, 'UTF-8');?>
">

        <?php }?>

                                <?php }?>

                            <?php }?>

                        <?php
}
}
/* {/block 'product_brand_below'} */
/* {block 'product_description_short'} */
class Block_2718806696046d3e1066cb0_71075517 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                        <div id="product-description-short-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['id'], ENT_QUOTES, 'UTF-8');?>
"

                             itemprop="description" class="rte-content"><?php echo $_smarty_tpl->tpl_vars['product']->value['description_short'];?>
</div>

                    <?php
}
}
/* {/block 'product_description_short'} */
/* {block 'hook_display_product_rating'} */
class Block_16637094706046d3e1067d23_08884586 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayProductRating','product'=>$_smarty_tpl->tpl_vars['product']->value),$_smarty_tpl ) );?>


                        <?php
}
}
/* {/block 'hook_display_product_rating'} */
/* {block 'product_prices'} */
class Block_17522989456046d3e1068b12_98615981 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                                <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/product-prices.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                            <?php
}
}
/* {/block 'product_prices'} */
/* {block 'page_header_container'} */
class Block_69712466046d3e105dd01_35187660 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                    <div class="product_header_container clearfix">



                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_19445950966046d3e105dfc5_27303677', 'product_brand_below', $this->tplIndex);
?>




                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6157630906046d3e1060524_04628572', 'page_header', $this->tplIndex);
?>


                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_17292199566046d3e1061391_97875661', 'product_brand_below', $this->tplIndex);
?>




                               <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2718806696046d3e1066cb0_71075517', 'product_description_short', $this->tplIndex);
?>



                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_16637094706046d3e1067d23_08884586', 'hook_display_product_rating', $this->tplIndex);
?>




                        <?php if ($_smarty_tpl->tpl_vars['iqitTheme']->value['pp_price_position'] == 'below-title') {?>

                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_17522989456046d3e1068b12_98615981', 'product_prices', $this->tplIndex);
?>


                        <?php }?>

                    </div>

                <?php
}
}
/* {/block 'page_header_container'} */
/* {block 'product_customization'} */
class Block_16257134846046d3e1069ed3_51125746 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                            <?php $_smarty_tpl->_subTemplateRender("file:catalog/_partials/product-customization.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('customizations'=>$_smarty_tpl->tpl_vars['product']->value['customizations']), 0, false);
?>

                        <?php
}
}
/* {/block 'product_customization'} */
/* {block 'product_variants'} */
class Block_15735234506046d3e106bb32_90179029 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayProductVariants','product'=>$_smarty_tpl->tpl_vars['product']->value),$_smarty_tpl ) );?>


                                    <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/product-variants.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                                <?php
}
}
/* {/block 'product_variants'} */
/* {block 'product_miniature'} */
class Block_13077723816046d3e106d643_17528598 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                                                    <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/miniatures/pack-product.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('product'=>$_smarty_tpl->tpl_vars['product_pack']->value), 0, true);
?>

                                                <?php
}
}
/* {/block 'product_miniature'} */
/* {block 'product_pack'} */
class Block_204383436046d3e106c602_89533422 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                                    <?php if ($_smarty_tpl->tpl_vars['packItems']->value) {?>

                                        <section class="product-pack">

                                            <p class="h4"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'This pack contains','d'=>'Shop.Theme.Catalog'),$_smarty_tpl ) );?>
</p>

                                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['packItems']->value, 'product_pack');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['product_pack']->value) {
?>

                                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13077723816046d3e106d643_17528598', 'product_miniature', $this->tplIndex);
?>


                                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

                                        </section>

                                    <?php }?>

                                <?php
}
}
/* {/block 'product_pack'} */
/* {block 'product_prices'} */
class Block_17985862006046d3e106e860_44011995 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                                        <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/product-prices.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

                                    <?php
}
}
/* {/block 'product_prices'} */
/* {block 'product_add_to_cart'} */
class Block_696913876046d3e106f0c4_82637219 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                                    <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/product-add-to-cart.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                                <?php
}
}
/* {/block 'product_add_to_cart'} */
/* {block 'product_discounts'} */
class Block_361392536046d3e106f7c2_76755336 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                                    <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/product-discounts.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                                <?php
}
}
/* {/block 'product_discounts'} */
/* {block 'product_additional_info'} */
class Block_1314726746046d3e106fec9_26993428 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                                    <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/product-additional-info.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                                <?php
}
}
/* {/block 'product_additional_info'} */
/* {block 'product_refresh'} */
class Block_19093376406046d3e10705c4_99438440 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'product_refresh'} */
/* {block 'product_buy'} */
class Block_17828857736046d3e106ac17_85080472 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                            <form action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['cart'], ENT_QUOTES, 'UTF-8');?>
" method="post" id="add-to-cart-or-refresh">

                                <input type="hidden" name="token" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['static_token']->value, ENT_QUOTES, 'UTF-8');?>
">

                                <input type="hidden" name="id_product" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['id'], ENT_QUOTES, 'UTF-8');?>
"

                                       id="product_page_product_id">

                                <input type="hidden" name="id_customization" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['id_customization'], ENT_QUOTES, 'UTF-8');?>
"

                                       id="product_customization_id">



                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15735234506046d3e106bb32_90179029', 'product_variants', $this->tplIndex);
?>




                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_204383436046d3e106c602_89533422', 'product_pack', $this->tplIndex);
?>




                                <?php if ($_smarty_tpl->tpl_vars['iqitTheme']->value['pp_price_position'] == 'above-button') {?>

                                    <div class="product_p_price_container">

                                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_17985862006046d3e106e860_44011995', 'product_prices', $this->tplIndex);
?>


                                    </div>

                                <?php }?>



                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_696913876046d3e106f0c4_82637219', 'product_add_to_cart', $this->tplIndex);
?>




                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_361392536046d3e106f7c2_76755336', 'product_discounts', $this->tplIndex);
?>




                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1314726746046d3e106fec9_26993428', 'product_additional_info', $this->tplIndex);
?>




                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_19093376406046d3e10705c4_99438440', 'product_refresh', $this->tplIndex);
?>


                            </form>

                        <?php
}
}
/* {/block 'product_buy'} */
/* {block 'hook_display_reassurance'} */
class Block_15757075676046d3e1070c13_94387899 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayReassurance'),$_smarty_tpl ) );?>


                        <?php
}
}
/* {/block 'hook_display_reassurance'} */
/* {block 'product_miniature'} */
class Block_14043099886046d3e1074507_10555154 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                                            <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/miniatures/product-small.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('product'=>$_smarty_tpl->tpl_vars['product_accessory']->value,'carousel'=>true,'elementor'=>true,'richData'=>true), 0, true);
?>

                                        <?php
}
}
/* {/block 'product_miniature'} */
/* {block 'product_accessories_sidebar'} */
class Block_16765544626046d3e1073502_84384029 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                        <?php if ($_smarty_tpl->tpl_vars['accessories']->value) {?>

                            <section class="product-accessories product-accessories-sidebar block">

                                <p class="block-title"><span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'You might also like','d'=>'Shop.Theme.Catalog'),$_smarty_tpl ) );?>
</span></p>

                                <div id="product-accessories-sidebar" class="block-content products products-grid">

                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['accessories']->value, 'product_accessory');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['product_accessory']->value) {
?>

                                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_14043099886046d3e1074507_10555154', 'product_miniature', $this->tplIndex);
?>


                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

                                </div>

                            </section>

                        <?php }?>

                    <?php
}
}
/* {/block 'product_accessories_sidebar'} */
/* {block 'product_miniature'} */
class Block_10949090816046d3e1076d92_43131484 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                                        <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/miniatures/product.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('product'=>$_smarty_tpl->tpl_vars['product_accessory']->value,'carousel'=>true,'richData'=>true), 0, true);
?>

                                    <?php
}
}
/* {/block 'product_miniature'} */
/* {block 'product_accessories_footer'} */
class Block_2101194036046d3e1075db4_72683746 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                <?php if ($_smarty_tpl->tpl_vars['accessories']->value) {?>

                    <section class="product-accessories block block-section">

                        <h3 class="section-title"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Related Products','d'=>'Shop.Theme.Catalog'),$_smarty_tpl ) );?>
</h3>

                        <div class="block-content">

                            <div class="products slick-products-carousel products-grid slick-default-carousel">

                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['accessories']->value, 'product_accessory');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['product_accessory']->value) {
?>

                                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10949090816046d3e1076d92_43131484', 'product_miniature', $this->tplIndex);
?>


                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

                            </div>

                        </div>

                    </section>

                <?php }?>

            <?php
}
}
/* {/block 'product_accessories_footer'} */
/* {block 'product_footer'} */
class Block_15827270096046d3e1077cd1_76507421 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayFooterProduct','product'=>$_smarty_tpl->tpl_vars['product']->value,'category'=>$_smarty_tpl->tpl_vars['category']->value),$_smarty_tpl ) );?>


        <?php
}
}
/* {/block 'product_footer'} */
/* {block 'product_images_modal'} */
class Block_2369833136046d3e10785c0_52055167 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


            <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/product-images-modal.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        <?php
}
}
/* {/block 'product_images_modal'} */
/* {block 'page_footer'} */
class Block_15332842096046d3e1078f74_94824904 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                    <!-- Footer content -->

                <?php
}
}
/* {/block 'page_footer'} */
/* {block 'page_footer_container'} */
class Block_7291684186046d3e1078cd4_89394562 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


            <footer class="page-footer">

                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15332842096046d3e1078f74_94824904', 'page_footer', $this->tplIndex);
?>


            </footer>

        <?php
}
}
/* {/block 'page_footer_container'} */
/* {block 'content'} */
class Block_13372928906046d3e10576f9_71551028 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_13372928906046d3e10576f9_71551028',
  ),
  'page_content_container' => 
  array (
    0 => 'Block_14516724056046d3e1059af4_27909293',
  ),
  'page_content' => 
  array (
    0 => 'Block_16644898446046d3e1059dc9_38043299',
  ),
  'product_cover_thumbnails' => 
  array (
    0 => 'Block_2382211876046d3e105a062_10497925',
  ),
  'after_cover_thumbnails' => 
  array (
    0 => 'Block_4462780276046d3e105c3a8_67583088',
  ),
  'after_cover_thumbnails2' => 
  array (
    0 => 'Block_19062038036046d3e105ce99_45883165',
  ),
  'page_header_container' => 
  array (
    0 => 'Block_69712466046d3e105dd01_35187660',
  ),
  'product_brand_below' => 
  array (
    0 => 'Block_19445950966046d3e105dfc5_27303677',
    1 => 'Block_17292199566046d3e1061391_97875661',
  ),
  'page_header' => 
  array (
    0 => 'Block_6157630906046d3e1060524_04628572',
  ),
  'page_title' => 
  array (
    0 => 'Block_3796832946046d3e10607f8_24435276',
  ),
  'product_description_short' => 
  array (
    0 => 'Block_2718806696046d3e1066cb0_71075517',
  ),
  'hook_display_product_rating' => 
  array (
    0 => 'Block_16637094706046d3e1067d23_08884586',
  ),
  'product_prices' => 
  array (
    0 => 'Block_17522989456046d3e1068b12_98615981',
    1 => 'Block_17985862006046d3e106e860_44011995',
  ),
  'product_customization' => 
  array (
    0 => 'Block_16257134846046d3e1069ed3_51125746',
  ),
  'product_buy' => 
  array (
    0 => 'Block_17828857736046d3e106ac17_85080472',
  ),
  'product_variants' => 
  array (
    0 => 'Block_15735234506046d3e106bb32_90179029',
  ),
  'product_pack' => 
  array (
    0 => 'Block_204383436046d3e106c602_89533422',
  ),
  'product_miniature' => 
  array (
    0 => 'Block_13077723816046d3e106d643_17528598',
    1 => 'Block_14043099886046d3e1074507_10555154',
    2 => 'Block_10949090816046d3e1076d92_43131484',
  ),
  'product_add_to_cart' => 
  array (
    0 => 'Block_696913876046d3e106f0c4_82637219',
  ),
  'product_discounts' => 
  array (
    0 => 'Block_361392536046d3e106f7c2_76755336',
  ),
  'product_additional_info' => 
  array (
    0 => 'Block_1314726746046d3e106fec9_26993428',
  ),
  'product_refresh' => 
  array (
    0 => 'Block_19093376406046d3e10705c4_99438440',
  ),
  'hook_display_reassurance' => 
  array (
    0 => 'Block_15757075676046d3e1070c13_94387899',
  ),
  'product_accessories_sidebar' => 
  array (
    0 => 'Block_16765544626046d3e1073502_84384029',
  ),
  'product_accessories_footer' => 
  array (
    0 => 'Block_2101194036046d3e1075db4_72683746',
  ),
  'product_footer' => 
  array (
    0 => 'Block_15827270096046d3e1077cd1_76507421',
  ),
  'product_images_modal' => 
  array (
    0 => 'Block_2369833136046d3e10785c0_52055167',
  ),
  'page_footer_container' => 
  array (
    0 => 'Block_7291684186046d3e1078cd4_89394562',
  ),
  'page_footer' => 
  array (
    0 => 'Block_15332842096046d3e1078f74_94824904',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


    <section id="main" itemscope itemtype="https://schema.org/Product">

        <div id="product-preloader"><i class="fa fa-circle-o-notch fa-spin"></i></div>

        <div id="main-product-wrapper" class="product-container">

        <meta itemprop="url" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['url'], ENT_QUOTES, 'UTF-8');?>
">



        <?php if ($_smarty_tpl->tpl_vars['product']->value['upc']) {?>

        <meta itemprop="gtin12" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['upc'], ENT_QUOTES, 'UTF-8');?>
">

        <?php }?>



        <?php if ($_smarty_tpl->tpl_vars['product']->value['ean13']) {?>

        <meta itemprop="gtin13" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['ean13'], ENT_QUOTES, 'UTF-8');?>
">

        <?php }?>



        <?php if (!$_smarty_tpl->tpl_vars['product']->value['upc'] && !$_smarty_tpl->tpl_vars['product']->value['ean13']) {?>

            <meta itemprop="identifier_exist" content="no">

        <?php }?>

 



        <div class="row product-info-row">

            <div class="col-md-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['iqitTheme']->value['pp_img_width'], ENT_QUOTES, 'UTF-8');?>
 col-product-image">

                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_14516724056046d3e1059af4_27909293', 'page_content_container', $this->tplIndex);
?>


            </div>



            <div class="col-md-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['iqitTheme']->value['pp_content_width'], ENT_QUOTES, 'UTF-8');?>
 col-product-info">

                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_69712466046d3e105dd01_35187660', 'page_header_container', $this->tplIndex);
?>




                <div class="product-information">

                 



                    <?php if ($_smarty_tpl->tpl_vars['product']->value['is_customizable'] && count($_smarty_tpl->tpl_vars['product']->value['customizations']['fields'])) {?>

                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_16257134846046d3e1069ed3_51125746', 'product_customization', $this->tplIndex);
?>


                    <?php }?>



                    <div class="product-actions">

                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_17828857736046d3e106ac17_85080472', 'product_buy', $this->tplIndex);
?>




                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15757075676046d3e1070c13_94387899', 'hook_display_reassurance', $this->tplIndex);
?>




                    </div>

                </div>


 <?php if ($_smarty_tpl->tpl_vars['iqitTheme']->value['pp_tabs'] == 'tabh' || $_smarty_tpl->tpl_vars['iqitTheme']->value['pp_tabs'] == 'tabha') {?>

            <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/_product_partials/product-tabs-h.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        <?php } elseif ($_smarty_tpl->tpl_vars['iqitTheme']->value['pp_tabs'] == 'section') {?>

            <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/_product_partials/product-tabs-sections.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }?>

            </div>



            <?php if ($_smarty_tpl->tpl_vars['iqitTheme']->value['pp_sidebar']) {?>

            <div class="col-md-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['iqitTheme']->value['pp_sidebar'], ENT_QUOTES, 'UTF-8');?>
 sidebar product-sidebar">



                <?php if ($_smarty_tpl->tpl_vars['iqitTheme']->value['pp_accesories'] == 'sidebar') {?>

                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_16765544626046d3e1073502_84384029', 'product_accessories_sidebar', $this->tplIndex);
?>


                <?php }?>



                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayRightColumnProduct'),$_smarty_tpl ) );?>




            </div>

            <?php }?>



        </div>



       


        </div>

        <?php if ($_smarty_tpl->tpl_vars['iqitTheme']->value['pp_accesories'] == 'footer') {?>

            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2101194036046d3e1075db4_72683746', 'product_accessories_footer', $this->tplIndex);
?>


        <?php }?>



        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15827270096046d3e1077cd1_76507421', 'product_footer', $this->tplIndex);
?>




        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2369833136046d3e10785c0_52055167', 'product_images_modal', $this->tplIndex);
?>




        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_7291684186046d3e1078cd4_89394562', 'page_footer_container', $this->tplIndex);
?>




    </section>

<?php
}
}
/* {/block 'content'} */
}
