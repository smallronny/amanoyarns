<?php
/* Smarty version 3.1.33, created on 2021-03-08 20:48:17
  from '/home2/amanoyarns/public_html/multitienda/themes/warehousechild/templates/catalog/_partials/product-flags.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6046d3e10e6644_60959423',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c944d777f020581130d3fa3e93c77287da091a9b' => 
    array (
      0 => '/home2/amanoyarns/public_html/multitienda/themes/warehousechild/templates/catalog/_partials/product-flags.tpl',
      1 => 1613004099,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6046d3e10e6644_60959423 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_4790616226046d3e10e49e0_17191409', 'product_flags');
?>
   

 <?php }
/* {block 'product_flags'} */
class Block_4790616226046d3e10e49e0_17191409 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'product_flags' => 
  array (
    0 => 'Block_4790616226046d3e10e49e0_17191409',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <ul class="product-flags">
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['product']->value['flags'], 'flag');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['flag']->value) {
?>
                        <li class="product-flag <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['flag']->value['type'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars(intval(($_smarty_tpl->tpl_vars['product']->value['specific_prices']['reduction']*100)), ENT_QUOTES, 'UTF-8');?>
%</li>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
    </ul>
<?php
}
}
/* {/block 'product_flags'} */
}
