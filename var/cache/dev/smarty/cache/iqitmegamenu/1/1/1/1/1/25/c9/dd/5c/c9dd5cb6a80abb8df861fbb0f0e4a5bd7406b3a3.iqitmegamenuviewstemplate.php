<?php
/* Smarty version 3.1.33, created on 2021-03-08 20:59:09
  from 'module:iqitmegamenuviewstemplate' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6046d66dd9a0e1_71890996',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '797404135c3d6163c184d5946c377ac2bc91c4d2' => 
    array (
      0 => 'module:iqitmegamenuviewstemplate',
      1 => 1611278170,
      2 => 'module',
    ),
    '470d5c96fd175e37e89afd5cc78d331c9756e29d' => 
    array (
      0 => 'module:iqitmegamenuviewstemplate',
      1 => 1611278170,
      2 => 'module',
    ),
    'e077dd2170956816de1e46af35296e5cbbf8e702' => 
    array (
      0 => 'module:iqitmegamenuviewstemplate',
      1 => 1611278170,
      2 => 'module',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_6046d66dd9a0e1_71890996 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
  'mobile_links' => 
  array (
    'compiled_filepath' => '/home2/amanoyarns/public_html/multitienda/var/cache/dev/smarty/compile/e0/77/dd/e077dd2170956816de1e46af35296e5cbbf8e702_2.module.iqitmegamenuviewstemplate.cache.php',
    'uid' => 'e077dd2170956816de1e46af35296e5cbbf8e702',
    'call_name' => 'smarty_template_function_mobile_links_15774861546046d3c9031c77_55546561',
  ),
  'categories_links' => 
  array (
    'compiled_filepath' => '/home2/amanoyarns/public_html/multitienda/var/cache/dev/smarty/compile/47/0d/5c/470d5c96fd175e37e89afd5cc78d331c9756e29d_2.module.iqitmegamenuviewstemplate.cache.php',
    'uid' => '470d5c96fd175e37e89afd5cc78d331c9756e29d',
    'call_name' => 'smarty_template_function_categories_links_14456976396046d3c8f34761_30162458',
  ),
));
?><!-- begin /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/horizontal.tpl -->	<div id="iqitmegamenu-wrapper" class="iqitmegamenu-wrapper iqitmegamenu-all">
		<div class="container container-iqitmegamenu">
		<div id="iqitmegamenu-horizontal" class="iqitmegamenu  clearfix" role="navigation">

								
				<nav id="cbp-hrmenu" class="cbp-hrmenu cbp-horizontal cbp-hrsub-narrow">
					<ul>
												<li id="cbp-hrmenu-tab-7" class="cbp-hrmenu-tab cbp-hrmenu-tab-7 ">
	<a href="/new-in" class="nav-link" >

								<span class="cbp-tab-title">
								New In</span>
														</a>
													</li>
												<li id="cbp-hrmenu-tab-5" class="cbp-hrmenu-tab cbp-hrmenu-tab-5  cbp-has-submeu">
	<a href="/2-yarns" class="nav-link" >

								<span class="cbp-tab-title">
								Yarns <i class="fa fa-angle-down cbp-submenu-aindicator"></i></span>
														</a>
														<div class="cbp-hrsub col-8">
								<div class="cbp-hrsub-inner">
									<div class="container iqitmegamenu-submenu-container">
									
																																	<!-- begin /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/submenu_content.tpl -->




<div class="row menu_row menu-element  first_rows menu-element-id-1">
                

                                                <!-- begin /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/submenu_content.tpl -->




    <div class="col-3 cbp-menu-column cbp-menu-element menu-element-id-2 ">
        <div class="cbp-menu-column-inner">
                        
                                                            <a href="/11-fiber"
                           class="cbp-column-title nav-link">Fiber </a>
                                    
                
                    
                                                    <ul class="cbp-links cbp-category-tree">
                                                                                                            <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/19-imperial-alpaca">Imperial Alpaca</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/20-royal-alpaca">Royal Alpaca</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/21-baby-alpaca">Baby Alpaca</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/22-merino-wool">Merino Wool</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/23-superwash-wool">Superwash Wool</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/163-peruvian-wool">Peruvian Wool</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/24-cashmere">Cashmere</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/25-mohair">Mohair</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/26-linen-">Linen</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/162-silk">Silk</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/173-cotton">Cotton</a>

                                                                                            </div>
                                        </li>
                                                                                                </ul>
                                            
                
            

            
            </div>    </div>
<!-- end /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/submenu_content.tpl -->                                    <!-- begin /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/submenu_content.tpl -->




    <div class="col-3 cbp-menu-column cbp-menu-element menu-element-id-3 ">
        <div class="cbp-menu-column-inner">
                        
                
                
                    
                                                    <div class="row cbp-categories-row">
                                                                                                            <div class="col-12">
                                            <div class="cbp-category-link-w"><a href="https://amanoyarns.com/16-weights"
                                                                                class="cbp-column-title nav-link cbp-category-title">Weights</a>
                                                                                                                                                    
    <ul class="cbp-links cbp-category-tree"><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.com/27-super-bulky">Super Bulky</a></div></li><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.com/28-chunky">Chunky</a></div></li><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.com/29-worsted">Worsted</a></div></li><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.com/31-sport">Sport</a></div></li><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.com/160-fingering">Fingering</a></div></li><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.com/161-lace">Lace</a></div></li></ul>

                                                                                            </div>
                                        </div>
                                                                                                </div>
                                            
                
            

            
            </div>    </div>
<!-- end /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/submenu_content.tpl -->                                    <!-- begin /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/submenu_content.tpl -->




    <div class="col-2 cbp-menu-column cbp-menu-element menu-element-id-4 ">
        <div class="cbp-menu-column-inner">
                        
                                                            <a href="/17-quality"
                           class="cbp-column-title nav-link">Product/Quality </a>
                                    
                
                    
                                                    <ul class="cbp-links cbp-category-tree">
                                                                                                            <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/34-apu">Apu</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/35-eco-puna-black">Eco Puna Black</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/158-eco-puna">Eco Puna</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/181-puna-traceable">Puna Traceable</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/37-warmi">Warmi</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/38-mamacha">Mamacha</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/39-yana-xl">Yana XL</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/40-yana">Yana</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/41-yana-journeys">Yana Journeys</a>

                                                                                            </div>
                                        </li>
                                                                                                </ul>
                                            
                
            

            
            </div>    </div>
<!-- end /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/submenu_content.tpl -->                                    <!-- begin /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/submenu_content.tpl -->




    <div class="col-1 cbp-menu-column cbp-menu-element menu-element-id-7 ">
        <div class="cbp-menu-column-inner">
                        
                
                
                    
                                                    <ul class="cbp-links cbp-category-tree">
                                                                                                            <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/42-skinny-yana">Skinny Yana</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/43-mayu">Mayu</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/44-mayu-lace">Mayu Lace</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/45-ayni">Ayni</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/46-puyu">Puyu</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/174-pacha">Pacha</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/175-awa">Awa</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/176-chaski">Chaski</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/177-sami">Sami</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/182-uma">Uma</a>

                                                                                            </div>
                                        </li>
                                                                                                </ul>
                                            
                
            

            
            </div>    </div>
<!-- end /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/submenu_content.tpl -->                                    <!-- begin /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/submenu_content.tpl -->




    <div class="col-3 cbp-menu-column cbp-menu-element menu-element-id-5 ">
        <div class="cbp-menu-column-inner">
                        
                
                
                                                                        <!-- begin /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/products_grid.tpl -->
<div class="cbp-products-big row ">
            <div class="product-grid-menu col-12">
            <div class="product-miniature-container">
                <div class="product-image-container">
                    <ul class="product-flags">
                                            </ul>
                    <a class="product_img_link" href="https://amanoyarns.com/yarns/53-208-warmi.html#/33-color-white_rose" title="Warmi">
                        <img class="img-fluid"
                             src="https://amanoyarns.com/443-home_default/warmi.jpg"
                             alt="Warmi"
                             width="315" height="535" />
                    </a>
                </div>
                <h6 class="product-title">
                    <a href="https://amanoyarns.com/yarns/53-208-warmi.html#/33-color-white_rose">Warmi</a>
                </h6>
                <div class="product-price-and-shipping" >
                    <span class="product-price">$19.00</span>
                                    </div>
            </div>
        </div>
    </div>
<!-- end /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/products_grid.tpl -->                                            
                
            

            
            </div>    </div>
<!-- end /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/submenu_content.tpl -->                            
                </div>
<!-- end /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/submenu_content.tpl -->																					
																			</div>
								</div>
							</div>
													</li>
												<li id="cbp-hrmenu-tab-6" class="cbp-hrmenu-tab cbp-hrmenu-tab-6  cbp-has-submeu">
	<a href="/content/15-patterns" class="nav-link" >

								<span class="cbp-tab-title">
								Patterns <i class="fa fa-angle-down cbp-submenu-aindicator"></i></span>
														</a>
														<div class="cbp-hrsub col-12">
								<div class="cbp-hrsub-inner">
									<div class="container iqitmegamenu-submenu-container">
									
																																	<!-- begin /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/submenu_content.tpl -->




<div class="row menu_row menu-element  first_rows menu-element-id-1">
                

                                                <!-- begin /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/submenu_content.tpl -->




    <div class="col-2 cbp-menu-column cbp-menu-element menu-element-id-4 ">
        <div class="cbp-menu-column-inner">
                        
                                                            <a href="/17-quality"
                           class="cbp-column-title nav-link">Product/Quality </a>
                                    
                
                    
                                                    <ul class="cbp-links cbp-category-tree">
                                                                                                            <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/34-apu">Apu</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/35-eco-puna-black">Eco Puna Black</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/158-eco-puna">Eco Puna</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/36-puna">Puna</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/37-warmi">Warmi</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/38-mamacha">Mamacha</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/39-yana-xl">Yana XL</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/40-yana">Yana</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/41-yana-journeys">Yana Journeys</a>

                                                                                            </div>
                                        </li>
                                                                                                </ul>
                                            
                
            

            
            </div>    </div>
<!-- end /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/submenu_content.tpl -->                                    <!-- begin /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/submenu_content.tpl -->




    <div class="col-1 cbp-menu-column cbp-menu-element menu-element-id-6 ">
        <div class="cbp-menu-column-inner">
                        
                
                
                    
                                                    <ul class="cbp-links cbp-category-tree">
                                                                                                            <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/42-skinny-yana">Skinny Yana</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/43-mayu">Mayu</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/44-mayu-lace">Mayu Lace</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/45-ayni">Ayni</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/46-puyu">Puyu</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/174-pacha">Pacha</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/175-awa">Awa</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/176-chaski">Chaski</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/177-sami">Sami</a>

                                                                                            </div>
                                        </li>
                                                                                                </ul>
                                            
                
            

            
            </div>    </div>
<!-- end /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/submenu_content.tpl -->                                    <!-- begin /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/submenu_content.tpl -->




    <div class="col-9 cbp-menu-column cbp-menu-element menu-element-id-10 ">
        <div class="cbp-menu-column-inner">
                        
                
                
                                             <div class="row">
<div class="col-sm-4 col-md-4 col-lg-4 patterns-menu"><img class="w-100 patterns-img" src="/img/cms/patterns/1-1-Sarita-Mittens-Apu.jpg" alt="Apu" /> <a class="Patterns-logo" href="/en/content/15-patterns"><img src="/img/cms/patterns/marca/apu-see.jpg" /></a></div>
<div class="col-sm-4 col-md-4 col-lg-4 patterns-menu"><img class="w-100" src="/img/cms/patterns/2-1-Aria-Hat-Eco-Puna-Black.jpg" alt="Eco Puna Black" /> <a class="Patterns-logo" href="/en/content/15-patterns"> <img src="/img/cms/patterns/marca/eco-puna-black-see.jpg" /> </a></div>
<div class="col-sm-4 col-md-4 col-lg-4 patterns-menu"><img class="w-100" src="/img/cms/patterns/3-1-Lorena-Sweater-Eco-Puna.jpg" alt="Eco Puna" /> <a class="Patterns-logo" href="/en/content/15-patterns"><img class="w-75" src="/img/cms/patterns/marca/eco-puna-see.jpg" alt="Apu" /> </a></div>
</div>
                    
                
            

            
            </div>    </div>
<!-- end /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/submenu_content.tpl -->                            
                </div>
<!-- end /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/submenu_content.tpl -->																					
																			</div>
								</div>
							</div>
													</li>
												<li id="cbp-hrmenu-tab-9" class="cbp-hrmenu-tab cbp-hrmenu-tab-9 ">
	<a href="https://amanoyarns.com/content/4-about-us" class="nav-link" >

								<span class="cbp-tab-title">
								About us</span>
														</a>
													</li>
												<li id="cbp-hrmenu-tab-10" class="cbp-hrmenu-tab cbp-hrmenu-tab-10 ">
	<a href="/blog" class="nav-link" >

								<span class="cbp-tab-title">
								Blog</span>
														</a>
													</li>
												<li id="cbp-hrmenu-tab-12" class="cbp-hrmenu-tab cbp-hrmenu-tab-12 ">
	<a href="https://amanoyarns.com/content/1-contact" class="nav-link" >

								<span class="cbp-tab-title">
								Contact</span>
														</a>
													</li>
											</ul>
				</nav>
		</div>
		</div>
		<div id="sticky-cart-wrapper"></div>
	</div>

<div id="_desktop_iqitmegamenu-mobile">
	<ul id="iqitmegamenu-mobile">
		<!-- begin /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/mobile_menu.tpl -->



<ul class="closeMenuMobile py-3" style="
    border-bottom: 1px solid;
">
<li class="menu-close row justify-content-center"> 
<div class="col-8 text-left pl-5">
    <img class="logo img-responsive" src="/img/logo-amano.png" alt="Amano" style="
    text-align: center;
    width: 50px;
">
    </div>
    <div class="col-4">
        <a class="close" data-toggle="dropdown" aria-label="Close">
          <span aria-hidden="true">×</span>
        </a>
    </div>
</li>
    </ul>









	
 

 
	<li><a  href="/new-in" >New In</a></li><li><a  href="/sale" >Sale</a></li><li><a  href="/2-yarns" >Yarns</a></li><li><a  href="/content/15-patterns" >Patterns</a></li><li><a  href="https://amanoyarns.com/content/4-about-us" >About us</a></li><li><a  href="/blog" >Blog</a></li><li><a  href="https://amanoyarns.com/content/1-contact" >Contact</a></li>





<!-- end /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/mobile_menu.tpl -->	</ul>
</div>
<!-- end /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/horizontal.tpl --><?php }
}
