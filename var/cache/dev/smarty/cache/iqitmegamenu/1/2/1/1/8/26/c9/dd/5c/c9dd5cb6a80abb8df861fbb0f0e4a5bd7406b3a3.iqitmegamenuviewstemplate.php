<?php
/* Smarty version 3.1.33, created on 2021-03-08 21:57:13
  from 'module:iqitmegamenuviewstemplate' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6046e409a063c3_93172140',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '797404135c3d6163c184d5946c377ac2bc91c4d2' => 
    array (
      0 => 'module:iqitmegamenuviewstemplate',
      1 => 1611278170,
      2 => 'module',
    ),
    '470d5c96fd175e37e89afd5cc78d331c9756e29d' => 
    array (
      0 => 'module:iqitmegamenuviewstemplate',
      1 => 1611278170,
      2 => 'module',
    ),
    'e077dd2170956816de1e46af35296e5cbbf8e702' => 
    array (
      0 => 'module:iqitmegamenuviewstemplate',
      1 => 1611278170,
      2 => 'module',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_6046e409a063c3_93172140 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
  'mobile_links' => 
  array (
    'compiled_filepath' => '/home2/amanoyarns/public_html/multitienda/var/cache/dev/smarty/compile/e0/77/dd/e077dd2170956816de1e46af35296e5cbbf8e702_2.module.iqitmegamenuviewstemplate.cache.php',
    'uid' => 'e077dd2170956816de1e46af35296e5cbbf8e702',
    'call_name' => 'smarty_template_function_mobile_links_15774861546046d3c9031c77_55546561',
  ),
  'categories_links' => 
  array (
    'compiled_filepath' => '/home2/amanoyarns/public_html/multitienda/var/cache/dev/smarty/compile/47/0d/5c/470d5c96fd175e37e89afd5cc78d331c9756e29d_2.module.iqitmegamenuviewstemplate.cache.php',
    'uid' => '470d5c96fd175e37e89afd5cc78d331c9756e29d',
    'call_name' => 'smarty_template_function_categories_links_14456976396046d3c8f34761_30162458',
  ),
));
?><!-- begin /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/horizontal.tpl -->	<div id="iqitmegamenu-wrapper" class="iqitmegamenu-wrapper iqitmegamenu-all">
		<div class="container container-iqitmegamenu">
		<div id="iqitmegamenu-horizontal" class="iqitmegamenu  clearfix" role="navigation">

								
				<nav id="cbp-hrmenu" class="cbp-hrmenu cbp-horizontal cbp-hrsub-narrow">
					<ul>
												<li id="cbp-hrmenu-tab-16" class="cbp-hrmenu-tab cbp-hrmenu-tab-16 ">
	<a href="/pe/new-in" class="nav-link" >

								<span class="cbp-tab-title">
								New In</span>
														</a>
													</li>
												<li id="cbp-hrmenu-tab-22" class="cbp-hrmenu-tab cbp-hrmenu-tab-22  cbp-has-submeu">
	<a href="/pe/2-madejas" class="nav-link" >

								<span class="cbp-tab-title">
								Madejas <i class="fa fa-angle-down cbp-submenu-aindicator"></i></span>
														</a>
														<div class="cbp-hrsub col-8">
								<div class="cbp-hrsub-inner">
									<div class="container iqitmegamenu-submenu-container">
									
																																	<!-- begin /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/submenu_content.tpl -->




<div class="row menu_row menu-element  first_rows menu-element-id-1">
                

                                                <!-- begin /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/submenu_content.tpl -->




    <div class="col-3 cbp-menu-column cbp-menu-element menu-element-id-2 ">
        <div class="cbp-menu-column-inner">
                        
                                                            <a href="/11-fibras"
                           class="cbp-column-title nav-link">Fibras </a>
                                    
                
                    
                                                    <ul class="cbp-links cbp-category-tree">
                                                                                                            <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/19-imperial-alpaca">Imperial Alpaca</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/20-royal-alpaca">Royal Alpaca</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/21-baby-alpaca">Baby Alpaca</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/22-lana-merino">Lana Merino</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/23-lana-superwash">Lana Superwash</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/163-lana-peruana">Lana Peruana</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/24-cashmere">Cashmere</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/25-mohair">Mohair</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/26-lino">Lino</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/162-seda">Seda</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/173-algodon">Algodón</a>

                                                                                            </div>
                                        </li>
                                                                                                </ul>
                                            
                
            

            
            </div>    </div>
<!-- end /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/submenu_content.tpl -->                                    <!-- begin /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/submenu_content.tpl -->




    <div class="col-3 cbp-menu-column cbp-menu-element menu-element-id-3 ">
        <div class="cbp-menu-column-inner">
                        
                
                
                    
                                                    <div class="row cbp-categories-row">
                                                                                                            <div class="col-12">
                                            <div class="cbp-category-link-w"><a href="https://amanoyarns.com/pe/16-grosor"
                                                                                class="cbp-column-title nav-link cbp-category-title">Grosor</a>
                                                                                                                                                    
    <ul class="cbp-links cbp-category-tree"><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.com/pe/27-super-bulky">Super Bulky</a></div></li><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.com/pe/28-chunky">Chunky</a></div></li><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.com/pe/29-worsted">Worsted</a></div></li><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.com/pe/31-sport">Sport</a></div></li><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.com/pe/160-fingering">Fingering</a></div></li><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.com/pe/161-lace">Lace</a></div></li></ul>

                                                                                            </div>
                                        </div>
                                                                                                </div>
                                            
                
            

            
            </div>    </div>
<!-- end /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/submenu_content.tpl -->                                    <!-- begin /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/submenu_content.tpl -->




    <div class="col-2 cbp-menu-column cbp-menu-element menu-element-id-4 ">
        <div class="cbp-menu-column-inner">
                        
                                                            <a href="/17-calidad"
                           class="cbp-column-title nav-link">Productos </a>
                                    
                
                    
                                                    <ul class="cbp-links cbp-category-tree">
                                                                                                            <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/183-inti">Inti</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/35-eco-puna-black">Eco Puna Black</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/158-eco-puna">Eco Puna</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/36-puna">Puna</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/37-warmi">Warmi</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/38-mamacha">Mamacha</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/39-yana-xl">Yana XL</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/40-yana">Yana</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/41-yana-journeys">Yana Journeys</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/34-apu">Apu</a>

                                                                                            </div>
                                        </li>
                                                                                                </ul>
                                            
                
            

            
            </div>    </div>
<!-- end /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/submenu_content.tpl -->                                    <!-- begin /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/submenu_content.tpl -->




    <div class="col-1 cbp-menu-column cbp-menu-element menu-element-id-7 ">
        <div class="cbp-menu-column-inner">
                        
                
                
                    
                                                    <ul class="cbp-links cbp-category-tree">
                                                                                                            <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/42-skinny-yana">Skinny Yana</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/43-mayu">Mayu</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/44-mayu-lace">Mayu Lace</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/45-ayni">Ayni</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/46-puyu">Puyu</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/174-pacha">Pacha</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/175-awa">Awa</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/176-chaski">Chaski</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/177-sami">Sami</a>

                                                                                            </div>
                                        </li>
                                                                                                </ul>
                                            
                
            

            
            </div>    </div>
<!-- end /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/submenu_content.tpl -->                                    <!-- begin /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/submenu_content.tpl -->




    <div class="col-3 cbp-menu-column cbp-menu-element menu-element-id-5 ">
        <div class="cbp-menu-column-inner">
                        
                
                
                                                                        <!-- begin /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/products_grid.tpl -->
<div class="cbp-products-big row ">
            <div class="product-grid-menu col-12">
            <div class="product-miniature-container">
                <div class="product-image-container">
                    <ul class="product-flags">
                                            </ul>
                    <a class="product_img_link" href="https://amanoyarns.com/pe/madejas/53-212-warmi.html#/39-color-beetroot" title="Warmi">
                        <img class="img-fluid"
                             src="https://amanoyarns.com/pe/410-home_default/warmi.jpg"
                             alt="Warmi"
                             width="315" height="535" />
                    </a>
                </div>
                <h6 class="product-title">
                    <a href="https://amanoyarns.com/pe/madejas/53-212-warmi.html#/39-color-beetroot">Warmi</a>
                </h6>
                <div class="product-price-and-shipping" >
                    <span class="product-price">37,00 PEN</span>
                                    </div>
            </div>
        </div>
    </div>
<!-- end /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/products_grid.tpl -->                                            
                
            

            
            </div>    </div>
<!-- end /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/submenu_content.tpl -->                            
                </div>
<!-- end /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/submenu_content.tpl -->																					
																			</div>
								</div>
							</div>
													</li>
												<li id="cbp-hrmenu-tab-15" class="cbp-hrmenu-tab cbp-hrmenu-tab-15  cbp-has-submeu">
	<a href=" /pe/content/15-patrones" class="nav-link" >

								<span class="cbp-tab-title">
								Patrones <i class="fa fa-angle-down cbp-submenu-aindicator"></i></span>
														</a>
														<div class="cbp-hrsub col-8">
								<div class="cbp-hrsub-inner">
									<div class="container iqitmegamenu-submenu-container">
									
																																	<!-- begin /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/submenu_content.tpl -->




<div class="row menu_row menu-element  first_rows menu-element-id-1">
                

                                                <!-- begin /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/submenu_content.tpl -->




    <div class="col-2 cbp-menu-column cbp-menu-element menu-element-id-4 ">
        <div class="cbp-menu-column-inner">
                        
                                                            <a href="/17-calidad"
                           class="cbp-column-title nav-link">Productos </a>
                                    
                
                    
                                                    <ul class="cbp-links cbp-category-tree">
                                                                                                            <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/37-warmi">Warmi</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/38-mamacha">Mamacha</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/39-yana-xl">Yana XL</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/40-yana">Yana</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/41-yana-journeys">Yana Journeys</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/158-eco-puna">Eco Puna</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/35-eco-puna-black">Eco Puna Black</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/36-puna">Puna</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/34-apu">Apu</a>

                                                                                            </div>
                                        </li>
                                                                                                </ul>
                                            
                
            

            
            </div>    </div>
<!-- end /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/submenu_content.tpl -->                                    <!-- begin /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/submenu_content.tpl -->




    <div class="col-1 cbp-menu-column cbp-menu-element menu-element-id-6 ">
        <div class="cbp-menu-column-inner">
                        
                
                
                    
                                                    <ul class="cbp-links cbp-category-tree">
                                                                                                            <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/42-skinny-yana">Skinny Yana</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/43-mayu">Mayu</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/44-mayu-lace">Mayu Lace</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/45-ayni">Ayni</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/46-puyu">Puyu</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/174-pacha">Pacha</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/175-awa">Awa</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/176-chaski">Chaski</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/177-sami">Sami</a>

                                                                                            </div>
                                        </li>
                                                                                                </ul>
                                            
                
            

            
            </div>    </div>
<!-- end /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/submenu_content.tpl -->                                    <!-- begin /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/submenu_content.tpl -->




    <div class="col-9 cbp-menu-column cbp-menu-element menu-element-id-10 ">
        <div class="cbp-menu-column-inner">
                        
                
                
                                             <div class="row">
<div class="col-sm-4 col-md-4 col-lg-4 patterns-menu"><img class="w-100 patterns-img" src="/img/cms/patterns/1-1-Sarita-Mittens-Apu.jpg" alt="Apu" /> <a class="Patterns-logo" href="/pe/content/15-patterns"><img src="/img/cms/patterns/marca/apu.jpg" /></a></div>
<div class="col-sm-4 col-md-4 col-lg-4 patterns-menu"><img class="w-100" src="/img/cms/patterns/2-1-Aria-Hat-Eco-Puna-Black.jpg" alt="Eco Puna Black" /> <a class="Patterns-logo" href="/pe/content/15-patterns"> <img src="/img/cms/patterns/marca/eco-puna-black.jpg" /> </a></div>
<div class="col-sm-4 col-md-4 col-lg-4 patterns-menu"><img class="w-100" src="/img/cms/patterns/3-1-Lorena-Sweater-Eco-Puna.jpg" alt="Eco Puna" /> <a class="Patterns-logo" href="/pe/content/15-patterns"><img class="w-75" src="/img/cms/patterns/marca/eco-puna.jpg" alt="Apu" /> </a></div>
</div>
                    
                
            

            
            </div>    </div>
<!-- end /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/submenu_content.tpl -->                            
                </div>
<!-- end /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/submenu_content.tpl -->																					
																			</div>
								</div>
							</div>
													</li>
												<li id="cbp-hrmenu-tab-18" class="cbp-hrmenu-tab cbp-hrmenu-tab-18 ">
	<a href="https://amanoyarns.com/pe/content/4-quienes-somos" class="nav-link" >

								<span class="cbp-tab-title">
								Quiénes somos</span>
														</a>
													</li>
												<li id="cbp-hrmenu-tab-19" class="cbp-hrmenu-tab cbp-hrmenu-tab-19 ">
	<a href="/pe/blog" class="nav-link" >

								<span class="cbp-tab-title">
								Blog</span>
														</a>
													</li>
												<li id="cbp-hrmenu-tab-20" class="cbp-hrmenu-tab cbp-hrmenu-tab-20 ">
	<a href="https://amanoyarns.com/pe/content/1-contacto" class="nav-link" >

								<span class="cbp-tab-title">
								Contacto</span>
														</a>
													</li>
											</ul>
				</nav>
		</div>
		</div>
		<div id="sticky-cart-wrapper"></div>
	</div>

<div id="_desktop_iqitmegamenu-mobile">
	<ul id="iqitmegamenu-mobile">
		<!-- begin /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/mobile_menu.tpl -->



<ul class="closeMenuMobile py-3" style="
    border-bottom: 1px solid;
">
<li class="menu-close row justify-content-center"> 
<div class="col-8 text-left pl-5">
    <img class="logo img-responsive" src="/img/logo-amano.png" alt="Amano" style="
    text-align: center;
    width: 50px;
">
    </div>
    <div class="col-4">
        <a class="close" data-toggle="dropdown" aria-label="Close">
          <span aria-hidden="true">×</span>
        </a>
    </div>
</li>
    </ul>









	
 

 
	<li><a  href="/pe/new-in" >New In</a></li><li><a  href="/pe/sale" >Sale</a></li><li><span class="mm-expand"><i class="fa fa-angle-down expand-icon" aria-hidden="true"></i><i class="fa fa-angle-up close-icon" aria-hidden="true"></i></span><a  href="https://amanoyarns.com/pe/" >Madejas</a>
 

 
	<ul><li><span class="mm-expand"><i class="fa fa-angle-down expand-icon" aria-hidden="true"></i><i class="fa fa-angle-up close-icon" aria-hidden="true"></i></span><a  href="https://amanoyarns.com/pe/11-fibras" >Fibras</a>
 

 
	<ul><li><a  href="https://amanoyarns.com/pe/20-royal-alpaca" >Royal Alpaca</a></li><li><a  href="https://amanoyarns.com/pe/21-baby-alpaca" >Baby Alpaca</a></li><li><a  href="https://amanoyarns.com/pe/22-lana-merino" >Lana Merino</a></li><li><a  href="https://amanoyarns.com/pe/23-lana-superwash" >Lana Superwash</a></li><li><a  href="https://amanoyarns.com/pe/24-cashmere" >Cashmere</a></li><li><a  href="https://amanoyarns.com/pe/25-mohair" >Mohair</a></li><li><a  href="https://amanoyarns.com/pe/26-lino" >Lino</a></li><li><a  href="https://amanoyarns.com/pe/162-seda" >Seda</a></li><li><a  href="https://amanoyarns.com/pe/163-lana-peruana" >Lana Peruana</a></li><li><a  href="https://amanoyarns.com/pe/173-algodon" >Algodón</a></li><li><a  href="https://amanoyarns.com/pe/12-vicuna" >Vicuña</a></li><li><a  href="https://amanoyarns.com/pe/179-baby-suri" >Baby Suri</a></li><li><a  href="https://amanoyarns.com/pe/19-imperial-alpaca" >Imperial Alpaca</a></li></ul>


</li><li><span class="mm-expand"><i class="fa fa-angle-down expand-icon" aria-hidden="true"></i><i class="fa fa-angle-up close-icon" aria-hidden="true"></i></span><a  href="https://amanoyarns.com/pe/16-grosor" >Grosor</a>
 

 
	<ul><li><a  href="https://amanoyarns.com/pe/27-super-bulky" >Super Bulky</a></li><li><a  href="https://amanoyarns.com/pe/28-chunky" >Chunky</a></li><li><a  href="https://amanoyarns.com/pe/29-worsted" >Worsted</a></li><li><a  href="https://amanoyarns.com/pe/31-sport" >Sport</a></li><li><a  href="https://amanoyarns.com/pe/160-fingering" >Fingering</a></li><li><a  href="https://amanoyarns.com/pe/161-lace" >Lace</a></li></ul>


</li><li><span class="mm-expand"><i class="fa fa-angle-down expand-icon" aria-hidden="true"></i><i class="fa fa-angle-up close-icon" aria-hidden="true"></i></span><a  href="https://amanoyarns.com/pe/17-productocalidad" >Producto/Calidad</a>
 

 
	<ul><li><a  href="https://amanoyarns.com/pe/37-warmi" >Warmi</a></li><li><a  href="https://amanoyarns.com/pe/38-mamacha" >Mamacha</a></li><li><a  href="https://amanoyarns.com/pe/39-yana-xl" >Yana XL</a></li><li><a  href="https://amanoyarns.com/pe/40-yana" >Yana</a></li><li><a  href="https://amanoyarns.com/pe/41-yana-journeys" >Yana Journeys</a></li><li><a  href="https://amanoyarns.com/pe/42-skinny-yana" >Skinny Yana</a></li><li><a  href="https://amanoyarns.com/pe/43-mayu" >Mayu</a></li><li><a  href="https://amanoyarns.com/pe/44-mayu-lace" >Mayu Lace</a></li><li><a  href="https://amanoyarns.com/pe/45-ayni" >Ayni</a></li><li><a  href="https://amanoyarns.com/pe/46-puyu" >Puyu</a></li><li><a  href="https://amanoyarns.com/pe/158-eco-puna" >Eco Puna</a></li><li><a  href="https://amanoyarns.com/pe/32-vicuna" >Vicuña</a></li><li><a  href="https://amanoyarns.com/pe/174-pacha" >Pacha</a></li><li><a  href="https://amanoyarns.com/pe/33-vicuna-negra-" >Vicuña Negra</a></li><li><a  href="https://amanoyarns.com/pe/175-awa" >Awa</a></li><li><a  href="https://amanoyarns.com/pe/176-chaski" >Chaski</a></li><li><a  href="https://amanoyarns.com/pe/35-eco-puna-black" >Eco Puna Black</a></li><li><a  href="https://amanoyarns.com/pe/177-sami" >Sami</a></li><li><a  href="https://amanoyarns.com/pe/36-puna" >Puna</a></li><li><a  href="https://amanoyarns.com/pe/180-puna-traceable" >Puna Traceable</a></li><li><a  href="https://amanoyarns.com/pe/181-puna-traceable" >Puna Traceable</a></li><li><a  href="https://amanoyarns.com/pe/182-uma" >Uma</a></li><li><a  href="https://amanoyarns.com/pe/183-inti" >Inti</a></li><li><a  href="https://amanoyarns.com/pe/34-apu" >Apu</a></li></ul>


</li><li><span class="mm-expand"><i class="fa fa-angle-down expand-icon" aria-hidden="true"></i><i class="fa fa-angle-up close-icon" aria-hidden="true"></i></span><a  href="https://amanoyarns.com/pe/165-family" >Family</a>
 

 
	<ul><li><a  href="https://amanoyarns.com/pe/167-origin" >Origin</a></li><li><a  href="https://amanoyarns.com/pe/168-our-wool" >Our Wool</a></li><li><a  href="https://amanoyarns.com/pe/169-special-blends" >Special Blends</a></li><li><a  href="https://amanoyarns.com/pe/170-spring-summer" >Spring Summer</a></li></ul>


</li></ul>


</li><li><a  href="/pe/content/15-patterns" >Patrones</a></li><li><a  href="https://amanoyarns.com/pe/content/4-quienes-somos" >Quiénes somos</a></li><li><a  href="/pe/blog" >Blog</a></li><li><a  href="https://amanoyarns.com/pe/content/1-contacto" >Contacto</a></li>





<!-- end /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/_partials/mobile_menu.tpl -->	</ul>
</div>
<!-- end /home2/amanoyarns/public_html/multitienda/modules/iqitmegamenu/views/templates/hook/horizontal.tpl --><?php }
}
