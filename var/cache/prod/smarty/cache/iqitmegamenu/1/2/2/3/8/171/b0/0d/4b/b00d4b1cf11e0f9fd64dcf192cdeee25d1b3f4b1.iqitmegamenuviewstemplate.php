<?php
/* Smarty version 3.1.33, created on 2021-01-18 20:05:54
  from 'module:iqitmegamenuviewstemplate' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6006307238b0e8_99579165',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '797404135c3d6163c184d5946c377ac2bc91c4d2' => 
    array (
      0 => 'module:iqitmegamenuviewstemplate',
      1 => 1597521698,
      2 => 'module',
    ),
    '470d5c96fd175e37e89afd5cc78d331c9756e29d' => 
    array (
      0 => 'module:iqitmegamenuviewstemplate',
      1 => 1597521698,
      2 => 'module',
    ),
    'e077dd2170956816de1e46af35296e5cbbf8e702' => 
    array (
      0 => 'module:iqitmegamenuviewstemplate',
      1 => 1598496449,
      2 => 'module',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_6006307238b0e8_99579165 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
  'mobile_links' => 
  array (
    'compiled_filepath' => '/home2/amanoyarns/public_html/store/var/cache/prod/smarty/compile/e0/77/dd/e077dd2170956816de1e46af35296e5cbbf8e702_2.module.iqitmegamenuviewstemplate.cache.php',
    'uid' => 'e077dd2170956816de1e46af35296e5cbbf8e702',
    'call_name' => 'smarty_template_function_mobile_links_14827494446005fd68dce763_76390308',
  ),
  'categories_links' => 
  array (
    'compiled_filepath' => '/home2/amanoyarns/public_html/store/var/cache/prod/smarty/compile/47/0d/5c/470d5c96fd175e37e89afd5cc78d331c9756e29d_2.module.iqitmegamenuviewstemplate.cache.php',
    'uid' => '470d5c96fd175e37e89afd5cc78d331c9756e29d',
    'call_name' => 'smarty_template_function_categories_links_5428304236005fd68d555a7_98103035',
  ),
));
?>	<div id="iqitmegamenu-wrapper" class="iqitmegamenu-wrapper iqitmegamenu-all">
		<div class="container container-iqitmegamenu">
		<div id="iqitmegamenu-horizontal" class="iqitmegamenu  clearfix" role="navigation">

								
				<nav id="cbp-hrmenu" class="cbp-hrmenu cbp-horizontal cbp-hrsub-narrow">
					<ul>
												<li id="cbp-hrmenu-tab-7" class="cbp-hrmenu-tab cbp-hrmenu-tab-7 ">
	<a href="/es/new-in" class="nav-link" >

								<span class="cbp-tab-title">
								New In</span>
														</a>
													</li>
												<li id="cbp-hrmenu-tab-5" class="cbp-hrmenu-tab cbp-hrmenu-tab-5  cbp-has-submeu">
	<a href="/es/2-madejas" class="nav-link" >

								<span class="cbp-tab-title">
								Madejas <i class="fa fa-angle-down cbp-submenu-aindicator"></i></span>
														</a>
														<div class="cbp-hrsub col-12">
								<div class="cbp-hrsub-inner">
									<div class="container iqitmegamenu-submenu-container">
									
																																	




<div class="row menu_row menu-element  first_rows menu-element-id-1">
                

                                                




    <div class="col-3 cbp-menu-column cbp-menu-element menu-element-id-2 ">
        <div class="cbp-menu-column-inner">
                        
                                                            <a href="/11-fibras"
                           class="cbp-column-title nav-link">Fibras </a>
                                    
                
                    
                                                    <ul class="cbp-links cbp-category-tree">
                                                                                                            <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/19-imperial-alpaca">Imperial Alpaca</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/20-royal-alpaca">Royal Alpaca</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/21-baby-alpaca">Baby Alpaca</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/22-lana-merino">Lana Merino</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/23-lana-superwash">Lana Superwash</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/163-lana-peruana">Lana Peruana</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/24-cashmere">Cashmere</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/25-mohair">Mohair</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/26-lino">Lino</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/162-seda">Seda</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/173-algodon">Algodón</a>

                                                                                            </div>
                                        </li>
                                                                                                </ul>
                                            
                
            

            
            </div>    </div>
                                    




    <div class="col-3 cbp-menu-column cbp-menu-element menu-element-id-3 ">
        <div class="cbp-menu-column-inner">
                        
                
                
                    
                                                    <div class="row cbp-categories-row">
                                                                                                            <div class="col-12">
                                            <div class="cbp-category-link-w"><a href="https://amanoyarns.store/es/16-grosor"
                                                                                class="cbp-column-title nav-link cbp-category-title">Grosor</a>
                                                                                                                                                    
    <ul class="cbp-links cbp-category-tree"><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.store/es/27-super-bulky">Super Bulky</a></div></li><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.store/es/28-chunky">Chunky</a></div></li><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.store/es/29-worsted">Worsted</a></div></li><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.store/es/31-sport">Sport</a></div></li><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.store/es/160-fingering">Fingering</a></div></li><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.store/es/161-lace">Lace</a></div></li></ul>

                                                                                            </div>
                                        </div>
                                                                                                </div>
                                            
                
            

            
            </div>    </div>
                                    




    <div class="col-2 cbp-menu-column cbp-menu-element menu-element-id-4 ">
        <div class="cbp-menu-column-inner">
                        
                                                            <a href="/17-calidad"
                           class="cbp-column-title nav-link">Producto/Calidad </a>
                                    
                
                    
                                                    <ul class="cbp-links cbp-category-tree">
                                                                                                            <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/34-apu">Apu</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/35-eco-puna-black">Eco Puna Black</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/158-eco-puna">Eco Puna</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/36-puna">Puna</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/37-warmi">Warmi</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/38-mamacha">Mamacha</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/39-yana-xl">Yana XL</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/40-yana">Yana</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/41-yana-journeys">Yana Journeys</a>

                                                                                            </div>
                                        </li>
                                                                                                </ul>
                                            
                
            

            
            </div>    </div>
                                    




    <div class="col-1 cbp-menu-column cbp-menu-element menu-element-id-7 ">
        <div class="cbp-menu-column-inner">
                        
                
                
                    
                                                    <ul class="cbp-links cbp-category-tree">
                                                                                                            <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/42-skinny-yana">Skinny Yana</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/43-mayu">Mayu</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/44-mayu-lace">Mayu Lace</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/45-ayni">Ayni</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/46-puyu">Puyu</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/174-pacha">Pacha</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/175-awa">Awa</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/176-chaski">Chaski</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/177-sami">Sami</a>

                                                                                            </div>
                                        </li>
                                                                                                </ul>
                                            
                
            

            
            </div>    </div>
                                    




    <div class="col-3 cbp-menu-column cbp-menu-element menu-element-id-5 ">
        <div class="cbp-menu-column-inner">
                        
                
                
                                                                        
<div class="cbp-products-big row ">
            <div class="product-grid-menu col-12">
            <div class="product-miniature-container">
                <div class="product-image-container">
                    <ul class="product-flags">
                                            </ul>
                    <a class="product_img_link" href="https://amanoyarns.store/es/madejas/53-212-warmi.html#/39-color-beetroot" title="Warmi">
                        <img class="img-fluid"
                             src="https://amanoyarns.store/410-home_default/warmi.jpg"
                             alt="Warmi"
                             width="315" height="535" />
                    </a>
                </div>
                <h6 class="product-title">
                    <a href="https://amanoyarns.store/es/madejas/53-212-warmi.html#/39-color-beetroot">Warmi</a>
                </h6>
                <div class="product-price-and-shipping" >
                    <span class="product-price">36,50 PEN</span>
                                    </div>
            </div>
        </div>
    </div>
                                            
                
            

            
            </div>    </div>
                            
                </div>
																					
																			</div>
								</div>
							</div>
													</li>
												<li id="cbp-hrmenu-tab-6" class="cbp-hrmenu-tab cbp-hrmenu-tab-6  cbp-has-submeu">
	<a href=" /es/content/15-patrones" class="nav-link" >

								<span class="cbp-tab-title">
								Patrones <i class="fa fa-angle-down cbp-submenu-aindicator"></i></span>
														</a>
														<div class="cbp-hrsub col-12">
								<div class="cbp-hrsub-inner">
									<div class="container iqitmegamenu-submenu-container">
									
																																	




<div class="row menu_row menu-element  first_rows menu-element-id-1">
                

                                                




    <div class="col-2 cbp-menu-column cbp-menu-element menu-element-id-4 ">
        <div class="cbp-menu-column-inner">
                        
                                                            <a href="/17-calidad"
                           class="cbp-column-title nav-link">Producto/Calidad </a>
                                    
                
                    
                                                    <ul class="cbp-links cbp-category-tree">
                                                                                                            <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/34-apu">Apu</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/35-eco-puna-black">Eco Puna Black</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/158-eco-puna">Eco Puna</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/36-puna">Puna</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/37-warmi">Warmi</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/38-mamacha">Mamacha</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/39-yana-xl">Yana XL</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/40-yana">Yana</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/41-yana-journeys">Yana Journeys</a>

                                                                                            </div>
                                        </li>
                                                                                                </ul>
                                            
                
            

            
            </div>    </div>
                                    




    <div class="col-1 cbp-menu-column cbp-menu-element menu-element-id-6 ">
        <div class="cbp-menu-column-inner">
                        
                
                
                    
                                                    <ul class="cbp-links cbp-category-tree">
                                                                                                            <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/42-skinny-yana">Skinny Yana</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/43-mayu">Mayu</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/44-mayu-lace">Mayu Lace</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/45-ayni">Ayni</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/46-puyu">Puyu</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/174-pacha">Pacha</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/175-awa">Awa</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/176-chaski">Chaski</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/es/177-sami">Sami</a>

                                                                                            </div>
                                        </li>
                                                                                                </ul>
                                            
                
            

            
            </div>    </div>
                                    




    <div class="col-9 cbp-menu-column cbp-menu-element menu-element-id-10 ">
        <div class="cbp-menu-column-inner">
                        
                
                
                                             <div class="row">
<div class="col-sm-4 col-md-4 col-lg-4 patterns-menu"><img class="w-100 patterns-img" src="/img/cms/patterns/1-1-Sarita-Mittens-Apu.jpg" alt="Apu" /> <a class="Patterns-logo" href="/es/content/15-patrones"><img src="/img/cms/patterns/marca/apu.jpg" /></a></div>
<div class="col-sm-4 col-md-4 col-lg-4 patterns-menu"><img class="w-100" src="/img/cms/patterns/2-1-Aria-Hat-Eco-Puna-Black.jpg" alt="Eco Puna Black" /> <a class="Patterns-logo" href="/es/content/15-patrones"> <img src="/img/cms/patterns/marca/eco-puna-black.jpg" /> </a></div>
<div class="col-sm-4 col-md-4 col-lg-4 patterns-menu"><img class="w-100" src="/img/cms/patterns/3-1-Lorena-Sweater-Eco-Puna.jpg" alt="Eco Puna" /> <a class="Patterns-logo" href="/es/content/15-patrones"><img class="w-75" src="/img/cms/patterns/marca/eco-puna.jpg" alt="Apu" /> </a></div>
</div>
                    
                
            

            
            </div>    </div>
                            
                </div>
																					
																			</div>
								</div>
							</div>
													</li>
												<li id="cbp-hrmenu-tab-9" class="cbp-hrmenu-tab cbp-hrmenu-tab-9 ">
	<a href="https://amanoyarns.store/es/content/4-quienes-somos" class="nav-link" >

								<span class="cbp-tab-title">
								Quiénes somos</span>
														</a>
													</li>
												<li id="cbp-hrmenu-tab-10" class="cbp-hrmenu-tab cbp-hrmenu-tab-10 ">
	<a href="/es/blog" class="nav-link" >

								<span class="cbp-tab-title">
								Blog</span>
														</a>
													</li>
												<li id="cbp-hrmenu-tab-12" class="cbp-hrmenu-tab cbp-hrmenu-tab-12 ">
	<a href="https://amanoyarns.store/es/content/1-contacto" class="nav-link" >

								<span class="cbp-tab-title">
								Contacto</span>
														</a>
													</li>
											</ul>
				</nav>
		</div>
		</div>
		<div id="sticky-cart-wrapper"></div>
	</div>

<div id="_desktop_iqitmegamenu-mobile">
	<ul id="iqitmegamenu-mobile">
		



<ul class="closeMenuMobile py-3" style="
    border-bottom: 1px solid;
">
<li class="menu-close row justify-content-center"> 
<div class="col-8 text-left pl-5">
    <img class="logo img-responsive" src="/img/logo-amano.png" alt="Amano" style="
    text-align: center;
    width: 50px;
">
    </div>
    <div class="col-4">
        <a class="close" data-toggle="dropdown" aria-label="Close">
          <span aria-hidden="true">×</span>
        </a>
    </div>
</li>
    </ul>









	
 

 
	<li><a  href="/es/new-in" >New In</a></li><li><a  href="/sale" >Sale</a></li><li><a  href="/es/2-madejas" >Madejas</a></li><li><a  href="/content/15-patrones" >Patrones</a></li><li><a  href="https://amanoyarns.store/es/content/4-quienes-somos" >Quiénes somos</a></li><li><a  href="/es/blog" >Blog</a></li><li><a  href="https://amanoyarns.store/es/content/1-contacto" >Contacto</a></li>





	</ul>
</div>
<?php }
}
