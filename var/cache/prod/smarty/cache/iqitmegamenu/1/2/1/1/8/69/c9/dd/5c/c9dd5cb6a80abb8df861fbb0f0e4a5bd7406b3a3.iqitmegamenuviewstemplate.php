<?php
/* Smarty version 3.1.33, created on 2021-03-19 17:35:02
  from 'module:iqitmegamenuviewstemplate' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_60552716bb2d33_06359699',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '797404135c3d6163c184d5946c377ac2bc91c4d2' => 
    array (
      0 => 'module:iqitmegamenuviewstemplate',
      1 => 1611278170,
      2 => 'module',
    ),
    '470d5c96fd175e37e89afd5cc78d331c9756e29d' => 
    array (
      0 => 'module:iqitmegamenuviewstemplate',
      1 => 1611278170,
      2 => 'module',
    ),
    'e077dd2170956816de1e46af35296e5cbbf8e702' => 
    array (
      0 => 'module:iqitmegamenuviewstemplate',
      1 => 1611278170,
      2 => 'module',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_60552716bb2d33_06359699 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
  'mobile_links' => 
  array (
    'compiled_filepath' => '/home2/amanoyarns/public_html/multitienda/var/cache/prod/smarty/compile/e0/77/dd/e077dd2170956816de1e46af35296e5cbbf8e702_2.module.iqitmegamenuviewstemplate.cache.php',
    'uid' => 'e077dd2170956816de1e46af35296e5cbbf8e702',
    'call_name' => 'smarty_template_function_mobile_links_1236060263605511540c1ac9_23941651',
  ),
  'categories_links' => 
  array (
    'compiled_filepath' => '/home2/amanoyarns/public_html/multitienda/var/cache/prod/smarty/compile/47/0d/5c/470d5c96fd175e37e89afd5cc78d331c9756e29d_2.module.iqitmegamenuviewstemplate.cache.php',
    'uid' => '470d5c96fd175e37e89afd5cc78d331c9756e29d',
    'call_name' => 'smarty_template_function_categories_links_20066765796055115408b872_10242995',
  ),
));
?>	<div id="iqitmegamenu-wrapper" class="iqitmegamenu-wrapper iqitmegamenu-all">
		<div class="container container-iqitmegamenu">
		<div id="iqitmegamenu-horizontal" class="iqitmegamenu  clearfix" role="navigation">

								
				<nav id="cbp-hrmenu" class="cbp-hrmenu cbp-horizontal cbp-hrsub-narrow">
					<ul>
												<li id="cbp-hrmenu-tab-16" class="cbp-hrmenu-tab cbp-hrmenu-tab-16 ">
	<a href="/pe/new-in" class="nav-link" >

								<span class="cbp-tab-title">
								New In</span>
														</a>
													</li>
												<li id="cbp-hrmenu-tab-5" class="cbp-hrmenu-tab cbp-hrmenu-tab-5  cbp-has-submeu">
	<a href="/pe/2-madejas" class="nav-link" >

								<span class="cbp-tab-title">
								Madejas <i class="fa fa-angle-down cbp-submenu-aindicator"></i></span>
														</a>
														<div class="cbp-hrsub col-8">
								<div class="cbp-hrsub-inner">
									<div class="container iqitmegamenu-submenu-container">
									
																																	




<div class="row menu_row menu-element  first_rows menu-element-id-1">
                

                                                




    <div class="col-3 cbp-menu-column cbp-menu-element menu-element-id-2 ">
        <div class="cbp-menu-column-inner">
                        
                                                            <a href="/11-fibras"
                           class="cbp-column-title nav-link">Fibras </a>
                                    
                
                    
                                                    <ul class="cbp-links cbp-category-tree">
                                                                                                            <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/19-imperial-alpaca">Imperial Alpaca</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/20-royal-alpaca">Royal Alpaca</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/21-baby-alpaca">Baby Alpaca</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/22-lana-merino">Lana Merino</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/23-lana-superwash">Lana Superwash</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/163-lana-peruana">Lana Peruana</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/24-cashmere">Cashmere</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/25-mohair">Mohair</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/26-lino">Lino</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/162-seda">Seda</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/pe/173-algodon">Algodón</a>

                                                                                            </div>
                                        </li>
                                                                                                </ul>
                                            
                
            

            
            </div>    </div>
                                    




    <div class="col-3 cbp-menu-column cbp-menu-element menu-element-id-3 ">
        <div class="cbp-menu-column-inner">
                        
                
                
                    
                                                    <div class="row cbp-categories-row">
                                                                                                            <div class="col-12">
                                            <div class="cbp-category-link-w"><a href="https://amanoyarns.com/pe/16-grosor"
                                                                                class="cbp-column-title nav-link cbp-category-title">Grosor</a>
                                                                                                                                                    
    <ul class="cbp-links cbp-category-tree"><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.com/pe/31-sport">Sport</a></div></li><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.com/pe/160-fingering">Fingering</a></div></li><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.com/pe/161-lace">Lace</a></div></li><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.com/pe/27-super-bulky">Super Bulky</a></div></li><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.com/pe/28-chunky">Chunky</a></div></li><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.com/pe/29-worsted">Worsted</a></div></li></ul>

                                                                                            </div>
                                        </div>
                                                                                                </div>
                                            
                
            

            
            </div>    </div>
                                    




    <div class="col-3 cbp-menu-column cbp-menu-element menu-element-id-4 ">
        <div class="cbp-menu-column-inner">
                        
                                                            <a href="/pe/17-productos"
                           class="cbp-column-title nav-link">Productos </a>
                                    
                
                                             <div class="row">
<div class="col-6">
<ul>
<li><a href="/pe/49-225-apu.html#/27-color-glacier">Apu</a></li>
<li><a href="/pe/50-229-eco-puna-black.html#/30-color-eco_puna_black">Eco Puna Black</a></li>
<li><a href="/pe/madejas/51-230-eco-puna.html#/31-color-nevado_white">Eco Puna</a></li>
<li><a href="/pe/madejas/52-401-puna.html#/97-color-blossom_field">Puna</a></li>
<li><a href="/pe/madejas/53-212-warmi.html#/39-color-beetroot">Warmi</a></li>
<li><a href="/pe/madejas/54-194-mamacha.html#/34-color-wheat">Mamacha</a></li>
<li><a href="/pe/madejas/62-300-yana-xl.html#/187-color-lilac_daisy">Yana XL</a></li>
<li><a href="/pe/madejas/63-443-yana.html#/178-color-scarlet_dream">Yana</a></li>
<li><a href="/pe/madejas/65-436-yana-journeys.html#/189-color-ayacucho">Yana Journeys</a></li>
</ul>
</div>
<div class="col-6">
<ul>
<li><a href="/pe/madejas/64-268-skinny-yana.html#/183-color-laguna">Skinny Yana</a></li>
<li><a href="/pe/madejas/55-173-mayu.html#/49-color-frost_white">Mayu</a></li>
<li><a href="/pe/madejas/56-384-mayu-lace.html#/69-color-french_rose">Mayu Lace</a></li>
<li><a href="/pe/madejas/58-363-ayni.html#/122-color-coral">Ayni</a></li>
<li><a href="/pe/madejas/57-365-puyu.html#/101-color-cloud">Puyu</a></li>
<li><a href="/pe/madejas/60-318-pacha.html#/141-color-muted_oyster_white">Pacha</a></li>
<li><a href="/pe/madejas/59-617-awa.html#/122-color-coral">Awa</a></li>
<li><a href="/pe/madejas/61-613-chaski.html#/253-color-tulip">Chaski</a></li>
<li><a href="/pe/madejas/66-242-sami.html#/199-color-seafoam">Sami</a></li>
<li><a href="/pe/madejas/82-653-inti.html#/267-color-orchid_oasis">Inti</a></li>
</ul>
</div>
<div class="col-12 menu-element-id-9 my-4"><a href="https://amanoyarns.com/pe/content/23-que-palitoscrochet-utilizar" class="btn-block"><span>¿Qué PALITOS</span><br /><span>/CROCHET utilizar?</span> </a></div>
</div>
                    
                
            

            
            </div>    </div>
                                    




    <div class="col-3 cbp-menu-column cbp-menu-element menu-element-id-5 ">
        <div class="cbp-menu-column-inner">
                        
                
                
                    
                                                    <div class="row cbp-categories-row">
                                                                                                            <div class="col-12">
                                            <div class="cbp-category-link-w"><a href="https://amanoyarns.com/pe/184-familia"
                                                                                class="cbp-column-title nav-link cbp-category-title">Familia</a>
                                                                                                                                                    
    <ul class="cbp-links cbp-category-tree"><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.com/pe/185-de-lujo">De lujo</a></div></li><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.com/pe/186-de-alpaca">De alpaca</a></div></li><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.com/pe/187-de-lana">De lana</a></div></li><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.com/pe/188-de-mezclas-finas">De mezclas finas</a></div></li><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.com/pe/189-de-verano">De verano</a></div></li></ul>

                                                                                            </div>
                                        </div>
                                                                                                </div>
                                            
                
            

            
            </div>    </div>
                            
                </div>
																					
																			</div>
								</div>
							</div>
													</li>
												<li id="cbp-hrmenu-tab-15" class="cbp-hrmenu-tab cbp-hrmenu-tab-15  cbp-has-submeu">
	<a href=" /pe/content/15-patrones" class="nav-link" >

								<span class="cbp-tab-title">
								Patrones <i class="fa fa-angle-down cbp-submenu-aindicator"></i></span>
														</a>
														<div class="cbp-hrsub col-8">
								<div class="cbp-hrsub-inner">
									<div class="container iqitmegamenu-submenu-container">
									
																																	




<div class="row menu_row menu-element  first_rows menu-element-id-1">
                

                                                




    <div class="col-3 cbp-menu-column cbp-menu-element menu-element-id-4 ">
        <div class="cbp-menu-column-inner">
                        
                                                            <a href="/pe/17-productos"
                           class="cbp-column-title nav-link">Productos </a>
                                    
                
                                             <div class="row">
<div class="col-6">
<ul>
<li><a href="/pe/49-225-apu.html#/27-color-glacier">Apu</a></li>
<li><a href="/pe/50-229-eco-puna-black.html#/30-color-eco_puna_black">Eco Puna Black</a></li>
<li><a href="/pe/madejas/51-230-eco-puna.html#/31-color-nevado_white">Eco Puna</a></li>
<li><a href="/pe/madejas/52-401-puna.html#/97-color-blossom_field">Puna</a></li>
<li><a href="/pe/madejas/53-212-warmi.html#/39-color-beetroot">Warmi</a></li>
<li><a href="/pe/madejas/54-194-mamacha.html#/34-color-wheat">Mamacha</a></li>
<li><a href="/pe/madejas/62-300-yana-xl.html#/187-color-lilac_daisy">Yana XL</a></li>
<li><a href="/pe/madejas/63-443-yana.html#/178-color-scarlet_dream">Yana</a></li>
<li><a href="/pe/madejas/65-436-yana-journeys.html#/189-color-ayacucho">Yana Journeys</a></li>
</ul>
</div>
<div class="col-6">
<ul>
<li><a href="/pe/madejas/64-268-skinny-yana.html#/183-color-laguna">Skinny Yana</a></li>
<li><a href="/pe/madejas/55-173-mayu.html#/49-color-frost_white">Mayu</a></li>
<li><a href="/pe/madejas/56-384-mayu-lace.html#/69-color-french_rose">Mayu Lace</a></li>
<li><a href="/pe/madejas/58-363-ayni.html#/122-color-coral">Ayni</a></li>
<li><a href="/pe/madejas/57-365-puyu.html#/101-color-cloud">Puyu</a></li>
<li><a href="/pe/madejas/60-318-pacha.html#/141-color-muted_oyster_white">Pacha</a></li>
<li><a href="/pe/madejas/59-617-awa.html#/122-color-coral">Awa</a></li>
<li><a href="/pe/madejas/61-613-chaski.html#/253-color-tulip">Chaski</a></li>
<li><a href="/pe/madejas/66-242-sami.html#/199-color-seafoam">Sami</a></li>
<li><a href="/pe/madejas/82-653-inti.html#/267-color-orchid_oasis">Inti</a></li>
</ul>
</div>
<div class="col-12 menu-element-id-9 my-4"><a href="https://amanoyarns.com/pe/content/23-que-palitoscrochet-utilizar" class="btn-block"><span>¿Qué PALITOS</span><br /><span>/CROCHET utilizar?</span> </a></div>
</div>
                    
                
            

            
            </div>    </div>
                                    




    <div class="col-9 cbp-menu-column cbp-menu-element menu-element-id-10 ">
        <div class="cbp-menu-column-inner">
                        
                
                
                                             <div class="row">
<div class="col-sm-4 col-md-4 col-lg-4 patterns-menu"><img class="w-100 patterns-img" src="/img/cms/patterns/1-1-Sarita-Mittens-Apu.jpg" alt="Apu" /> <a class="Patterns-logo" href="/pe/content/15-patterns"><img src="/img/cms/patterns/marca/apu.jpg" /></a></div>
<div class="col-sm-4 col-md-4 col-lg-4 patterns-menu"><img class="w-100" src="/img/cms/patterns/2-1-Aria-Hat-Eco-Puna-Black.jpg" alt="Eco Puna Black" /> <a class="Patterns-logo" href="/pe/content/15-patterns"> <img src="/img/cms/patterns/marca/eco-puna-black.jpg" /> </a></div>
<div class="col-sm-4 col-md-4 col-lg-4 patterns-menu"><img class="w-100" src="/img/cms/patterns/3-1-Lorena-Sweater-Eco-Puna.jpg" alt="Eco Puna" /> <a class="Patterns-logo" href="/pe/content/15-patterns"><img class="w-75" src="/img/cms/patterns/marca/eco-puna.jpg" alt="Apu" /> </a></div>
</div>
                    
                
            

            
            </div>    </div>
                            
                </div>
																					
																			</div>
								</div>
							</div>
													</li>
												<li id="cbp-hrmenu-tab-19" class="cbp-hrmenu-tab cbp-hrmenu-tab-19 ">
	<a href="/pe/blog" class="nav-link" >

								<span class="cbp-tab-title">
								Blog</span>
														</a>
													</li>
											</ul>
				</nav>
		</div>
		</div>
		<div id="sticky-cart-wrapper"></div>
	</div>

<div id="_desktop_iqitmegamenu-mobile">
	<ul id="iqitmegamenu-mobile">
		



<ul class="closeMenuMobile py-3" style="
    border-bottom: 1px solid;
">
<li class="menu-close row justify-content-center"> 
<div class="col-8 text-left pl-5">
    <img class="logo img-responsive" src="/img/logo-amano.png" alt="Amano" style="
    text-align: center;
    width: 50px;
">
    </div>
    <div class="col-4">
        <a class="close" data-toggle="dropdown" aria-label="Close">
          <span aria-hidden="true">×</span>
        </a>
    </div>
</li>
    </ul>









	
 

 
	<li><a  href="/pe/new-in" >New In</a></li><li><a  href="/pe/sale" >Sale</a></li><li><span class="mm-expand"><i class="fa fa-angle-down expand-icon" aria-hidden="true"></i><i class="fa fa-angle-up close-icon" aria-hidden="true"></i></span><a  href="https://amanoyarns.com/pe/" >Madejas</a>
 

 
	<ul><li><span class="mm-expand"><i class="fa fa-angle-down expand-icon" aria-hidden="true"></i><i class="fa fa-angle-up close-icon" aria-hidden="true"></i></span><a  href="https://amanoyarns.com/pe/11-fibras" >Fibras</a>
 

 
	<ul><li><a  href="https://amanoyarns.com/pe/20-royal-alpaca" >Royal Alpaca</a></li><li><a  href="https://amanoyarns.com/pe/21-baby-alpaca" >Baby Alpaca</a></li><li><a  href="https://amanoyarns.com/pe/22-lana-merino" >Lana Merino</a></li><li><a  href="https://amanoyarns.com/pe/23-lana-superwash" >Lana Superwash</a></li><li><a  href="https://amanoyarns.com/pe/24-cashmere" >Cashmere</a></li><li><a  href="https://amanoyarns.com/pe/25-mohair" >Mohair</a></li><li><a  href="https://amanoyarns.com/pe/26-lino" >Lino</a></li><li><a  href="https://amanoyarns.com/pe/162-seda" >Seda</a></li><li><a  href="https://amanoyarns.com/pe/163-lana-peruana" >Lana Peruana</a></li><li><a  href="https://amanoyarns.com/pe/173-algodon" >Algodón</a></li><li><a  href="https://amanoyarns.com/pe/12-vicuna" >Vicuña</a></li><li><a  href="https://amanoyarns.com/pe/179-baby-suri" >Baby Suri</a></li><li><a  href="https://amanoyarns.com/pe/19-imperial-alpaca" >Imperial Alpaca</a></li></ul>


</li><li><span class="mm-expand"><i class="fa fa-angle-down expand-icon" aria-hidden="true"></i><i class="fa fa-angle-up close-icon" aria-hidden="true"></i></span><a  href="https://amanoyarns.com/pe/16-grosor" >Grosor</a>
 

 
	<ul><li><a  href="https://amanoyarns.com/pe/31-sport" >Sport</a></li><li><a  href="https://amanoyarns.com/pe/160-fingering" >Fingering</a></li><li><a  href="https://amanoyarns.com/pe/161-lace" >Lace</a></li><li><a  href="https://amanoyarns.com/pe/27-super-bulky" >Super Bulky</a></li><li><a  href="https://amanoyarns.com/pe/28-chunky" >Chunky</a></li><li><a  href="https://amanoyarns.com/pe/29-worsted" >Worsted</a></li></ul>


</li><li><a  href="https://amanoyarns.com/pe/17-productos" >Productos</a></li><li><span class="mm-expand"><i class="fa fa-angle-down expand-icon" aria-hidden="true"></i><i class="fa fa-angle-up close-icon" aria-hidden="true"></i></span><a  href="https://amanoyarns.com/pe/184-familia" >Familia</a>
 

 
	<ul><li><a  href="https://amanoyarns.com/pe/185-de-lujo" >De lujo</a></li><li><a  href="https://amanoyarns.com/pe/186-de-alpaca" >De alpaca</a></li><li><a  href="https://amanoyarns.com/pe/187-de-lana" >De lana</a></li><li><a  href="https://amanoyarns.com/pe/188-de-mezclas-finas" >De mezclas finas</a></li><li><a  href="https://amanoyarns.com/pe/189-de-verano" >De verano</a></li></ul>


</li></ul>


</li><li><a  href="/pe/content/15-patterns" >Patrones</a></li><li><a  href="https://amanoyarns.com/pe/content/4-quienes-somos" >Quiénes somos</a></li><li><a  href="/pe/blog" >Blog</a></li><li><a  href="https://amanoyarns.com/pe/content/1-contacto" >Contacto</a></li>





	</ul>
</div>
<?php }
}
