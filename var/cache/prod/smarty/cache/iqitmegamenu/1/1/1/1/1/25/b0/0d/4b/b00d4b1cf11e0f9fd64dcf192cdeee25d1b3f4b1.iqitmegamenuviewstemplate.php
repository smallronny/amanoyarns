<?php
/* Smarty version 3.1.33, created on 2021-01-19 14:34:39
  from 'module:iqitmegamenuviewstemplate' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6007344fa70e91_80560008',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '797404135c3d6163c184d5946c377ac2bc91c4d2' => 
    array (
      0 => 'module:iqitmegamenuviewstemplate',
      1 => 1597521698,
      2 => 'module',
    ),
    '470d5c96fd175e37e89afd5cc78d331c9756e29d' => 
    array (
      0 => 'module:iqitmegamenuviewstemplate',
      1 => 1597521698,
      2 => 'module',
    ),
    'e077dd2170956816de1e46af35296e5cbbf8e702' => 
    array (
      0 => 'module:iqitmegamenuviewstemplate',
      1 => 1598496449,
      2 => 'module',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_6007344fa70e91_80560008 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
  'mobile_links' => 
  array (
    'compiled_filepath' => '/home2/amanoyarns/public_html/store/var/cache/prod/smarty/compile/e0/77/dd/e077dd2170956816de1e46af35296e5cbbf8e702_2.module.iqitmegamenuviewstemplate.cache.php',
    'uid' => 'e077dd2170956816de1e46af35296e5cbbf8e702',
    'call_name' => 'smarty_template_function_mobile_links_14827494446005fd68dce763_76390308',
  ),
  'categories_links' => 
  array (
    'compiled_filepath' => '/home2/amanoyarns/public_html/store/var/cache/prod/smarty/compile/47/0d/5c/470d5c96fd175e37e89afd5cc78d331c9756e29d_2.module.iqitmegamenuviewstemplate.cache.php',
    'uid' => '470d5c96fd175e37e89afd5cc78d331c9756e29d',
    'call_name' => 'smarty_template_function_categories_links_5428304236005fd68d555a7_98103035',
  ),
));
?>	<div id="iqitmegamenu-wrapper" class="iqitmegamenu-wrapper iqitmegamenu-all">
		<div class="container container-iqitmegamenu">
		<div id="iqitmegamenu-horizontal" class="iqitmegamenu  clearfix" role="navigation">

								
				<nav id="cbp-hrmenu" class="cbp-hrmenu cbp-horizontal cbp-hrsub-narrow">
					<ul>
												<li id="cbp-hrmenu-tab-7" class="cbp-hrmenu-tab cbp-hrmenu-tab-7 ">
	<a href="/en/new-in" class="nav-link" >

								<span class="cbp-tab-title">
								New In</span>
														</a>
													</li>
												<li id="cbp-hrmenu-tab-5" class="cbp-hrmenu-tab cbp-hrmenu-tab-5  cbp-has-submeu">
	<a href="/en/2-yarns" class="nav-link" >

								<span class="cbp-tab-title">
								Yarns <i class="fa fa-angle-down cbp-submenu-aindicator"></i></span>
														</a>
														<div class="cbp-hrsub col-12">
								<div class="cbp-hrsub-inner">
									<div class="container iqitmegamenu-submenu-container">
									
																																	




<div class="row menu_row menu-element  first_rows menu-element-id-1">
                

                                                




    <div class="col-3 cbp-menu-column cbp-menu-element menu-element-id-2 ">
        <div class="cbp-menu-column-inner">
                        
                                                            <a href="/11-fiber"
                           class="cbp-column-title nav-link">Fiber </a>
                                    
                
                    
                                                    <ul class="cbp-links cbp-category-tree">
                                                                                                            <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/19-imperial-alpaca">Imperial Alpaca</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/20-royal-alpaca">Royal Alpaca</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/21-baby-alpaca">Baby Alpaca</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/22-lana-merino">Lana Merino</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/23-lana-superwash">Lana Superwash</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/163-peruvian-wool">Peruvian Wool</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/24-cashmere">Cashmere</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/25-mohair">Mohair</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/26-lino">Lino</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/162-silk">Silk</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/173-cotton">Cotton</a>

                                                                                            </div>
                                        </li>
                                                                                                </ul>
                                            
                
            

            
            </div>    </div>
                                    




    <div class="col-3 cbp-menu-column cbp-menu-element menu-element-id-3 ">
        <div class="cbp-menu-column-inner">
                        
                
                
                    
                                                    <div class="row cbp-categories-row">
                                                                                                            <div class="col-12">
                                            <div class="cbp-category-link-w"><a href="https://amanoyarns.store/en/16-weights"
                                                                                class="cbp-column-title nav-link cbp-category-title">Weights</a>
                                                                                                                                                    
    <ul class="cbp-links cbp-category-tree"><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.store/en/27-super-bulky">Super Bulky</a></div></li><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.store/en/28-chunky">Chunky</a></div></li><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.store/en/29-worsted">Worsted</a></div></li><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.store/en/31-sport">Sport</a></div></li><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.store/en/160-fingering">Fingering</a></div></li><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.store/en/161-lace">Lace</a></div></li></ul>

                                                                                            </div>
                                        </div>
                                                                                                </div>
                                            
                
            

            
            </div>    </div>
                                    




    <div class="col-2 cbp-menu-column cbp-menu-element menu-element-id-4 ">
        <div class="cbp-menu-column-inner">
                        
                                                            <a href="/17-quality"
                           class="cbp-column-title nav-link">Product/Quality </a>
                                    
                
                    
                                                    <ul class="cbp-links cbp-category-tree">
                                                                                                            <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/34-apu">Apu</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/35-eco-puna-black">Eco Puna Black</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/158-eco-puna">Eco Puna</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/36-puna">Puna</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/37-warmi">Warmi</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/38-mamacha">Mamacha</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/39-yana-xl">Yana XL</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/40-yana">Yana</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/41-yana-journeys">Yana Journeys</a>

                                                                                            </div>
                                        </li>
                                                                                                </ul>
                                            
                
            

            
            </div>    </div>
                                    




    <div class="col-1 cbp-menu-column cbp-menu-element menu-element-id-7 ">
        <div class="cbp-menu-column-inner">
                        
                
                
                    
                                                    <ul class="cbp-links cbp-category-tree">
                                                                                                            <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/42-skinny-yana">Skinny Yana</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/43-mayu">Mayu</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/44-mayu-lace">Mayu Lace</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/45-ayni">Ayni</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/46-puyu">Puyu</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/174-pacha">Pacha</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/175-awa">Awa</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/176-chaski">Chaski</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/177-sami">Sami</a>

                                                                                            </div>
                                        </li>
                                                                                                </ul>
                                            
                
            

            
            </div>    </div>
                                    




    <div class="col-3 cbp-menu-column cbp-menu-element menu-element-id-5 ">
        <div class="cbp-menu-column-inner">
                        
                
                
                                                                        
<div class="cbp-products-big row ">
            <div class="product-grid-menu col-12">
            <div class="product-miniature-container">
                <div class="product-image-container">
                    <ul class="product-flags">
                                            </ul>
                    <a class="product_img_link" href="https://amanoyarns.store/en/yarns/53-212-warmi.html#/39-color-beetroot" title="Warmi">
                        <img class="img-fluid"
                             src="https://amanoyarns.store/410-home_default/warmi.jpg"
                             alt="Warmi"
                             width="315" height="535" />
                    </a>
                </div>
                <h6 class="product-title">
                    <a href="https://amanoyarns.store/en/yarns/53-212-warmi.html#/39-color-beetroot">Warmi</a>
                </h6>
                <div class="product-price-and-shipping" >
                    <span class="product-price">$19.00</span>
                                    </div>
            </div>
        </div>
    </div>
                                            
                
            

            
            </div>    </div>
                            
                </div>
																					
																			</div>
								</div>
							</div>
													</li>
												<li id="cbp-hrmenu-tab-6" class="cbp-hrmenu-tab cbp-hrmenu-tab-6  cbp-has-submeu">
	<a href="/en/content/15-patterns" class="nav-link" >

								<span class="cbp-tab-title">
								Patterns <i class="fa fa-angle-down cbp-submenu-aindicator"></i></span>
														</a>
														<div class="cbp-hrsub col-12">
								<div class="cbp-hrsub-inner">
									<div class="container iqitmegamenu-submenu-container">
									
																																	




<div class="row menu_row menu-element  first_rows menu-element-id-1">
                

                                                




    <div class="col-2 cbp-menu-column cbp-menu-element menu-element-id-4 ">
        <div class="cbp-menu-column-inner">
                        
                                                            <a href="/17-quality"
                           class="cbp-column-title nav-link">Product/Quality </a>
                                    
                
                    
                                                    <ul class="cbp-links cbp-category-tree">
                                                                                                            <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/34-apu">Apu</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/35-eco-puna-black">Eco Puna Black</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/158-eco-puna">Eco Puna</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/36-puna">Puna</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/37-warmi">Warmi</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/38-mamacha">Mamacha</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/39-yana-xl">Yana XL</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/40-yana">Yana</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/41-yana-journeys">Yana Journeys</a>

                                                                                            </div>
                                        </li>
                                                                                                </ul>
                                            
                
            

            
            </div>    </div>
                                    




    <div class="col-1 cbp-menu-column cbp-menu-element menu-element-id-6 ">
        <div class="cbp-menu-column-inner">
                        
                
                
                    
                                                    <ul class="cbp-links cbp-category-tree">
                                                                                                            <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/42-skinny-yana">Skinny Yana</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/43-mayu">Mayu</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/44-mayu-lace">Mayu Lace</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/45-ayni">Ayni</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/46-puyu">Puyu</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/174-pacha">Pacha</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/175-awa">Awa</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/176-chaski">Chaski</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.store/en/177-sami">Sami</a>

                                                                                            </div>
                                        </li>
                                                                                                </ul>
                                            
                
            

            
            </div>    </div>
                                    




    <div class="col-9 cbp-menu-column cbp-menu-element menu-element-id-10 ">
        <div class="cbp-menu-column-inner">
                        
                
                
                                             <div class="row">
<div class="col-sm-4 col-md-4 col-lg-4 patterns-menu"><img class="w-100 patterns-img" src="/img/cms/patterns/1-1-Sarita-Mittens-Apu.jpg" alt="Apu" /> <a class="Patterns-logo" href="/en/content/15-patterns"><img src="/img/cms/patterns/marca/apu.jpg" /></a></div>
<div class="col-sm-4 col-md-4 col-lg-4 patterns-menu"><img class="w-100" src="/img/cms/patterns/2-1-Aria-Hat-Eco-Puna-Black.jpg" alt="Eco Puna Black" /> <a class="Patterns-logo" href="/en/content/15-patterns"> <img src="/img/cms/patterns/marca/eco-puna-black.jpg" /> </a></div>
<div class="col-sm-4 col-md-4 col-lg-4 patterns-menu"><img class="w-100" src="/img/cms/patterns/3-1-Lorena-Sweater-Eco-Puna.jpg" alt="Eco Puna" /> <a class="Patterns-logo" href="/en/content/15-patterns"><img class="w-75" src="/img/cms/patterns/marca/eco-puna.jpg" alt="Apu" /> </a></div>
</div>
                    
                
            

            
            </div>    </div>
                            
                </div>
																					
																			</div>
								</div>
							</div>
													</li>
												<li id="cbp-hrmenu-tab-9" class="cbp-hrmenu-tab cbp-hrmenu-tab-9 ">
	<a href="https://amanoyarns.store/en/content/4-about-us" class="nav-link" >

								<span class="cbp-tab-title">
								About us</span>
														</a>
													</li>
												<li id="cbp-hrmenu-tab-10" class="cbp-hrmenu-tab cbp-hrmenu-tab-10 ">
	<a href="/en/blog" class="nav-link" >

								<span class="cbp-tab-title">
								Blog</span>
														</a>
													</li>
												<li id="cbp-hrmenu-tab-12" class="cbp-hrmenu-tab cbp-hrmenu-tab-12 ">
	<a href="https://amanoyarns.store/en/content/1-contact" class="nav-link" >

								<span class="cbp-tab-title">
								Contact</span>
														</a>
													</li>
											</ul>
				</nav>
		</div>
		</div>
		<div id="sticky-cart-wrapper"></div>
	</div>

<div id="_desktop_iqitmegamenu-mobile">
	<ul id="iqitmegamenu-mobile">
		



<ul class="closeMenuMobile py-3" style="
    border-bottom: 1px solid;
">
<li class="menu-close row justify-content-center"> 
<div class="col-8 text-left pl-5">
    <img class="logo img-responsive" src="/img/logo-amano.png" alt="Amano" style="
    text-align: center;
    width: 50px;
">
    </div>
    <div class="col-4">
        <a class="close" data-toggle="dropdown" aria-label="Close">
          <span aria-hidden="true">×</span>
        </a>
    </div>
</li>
    </ul>









	
 

 
	<li><a  href="/en/new-in" >New In</a></li><li><a  href="/sale" >Sale</a></li><li><a  href="/en/2-yarns" >Yarns</a></li><li><a  href="/content/15-patterns" >Patterns</a></li><li><a  href="https://amanoyarns.store/en/content/4-about-us" >About us</a></li><li><a  href="/en/blog" >Blog</a></li><li><a  href="https://amanoyarns.store/en/content/1-contact" >Contact</a></li>





	</ul>
</div>
<?php }
}
