<?php
/* Smarty version 3.1.33, created on 2021-03-19 18:33:30
  from 'module:iqitmegamenuviewstemplate' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_605526bad8e571_91426004',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '797404135c3d6163c184d5946c377ac2bc91c4d2' => 
    array (
      0 => 'module:iqitmegamenuviewstemplate',
      1 => 1611278170,
      2 => 'module',
    ),
    '470d5c96fd175e37e89afd5cc78d331c9756e29d' => 
    array (
      0 => 'module:iqitmegamenuviewstemplate',
      1 => 1611278170,
      2 => 'module',
    ),
    'e077dd2170956816de1e46af35296e5cbbf8e702' => 
    array (
      0 => 'module:iqitmegamenuviewstemplate',
      1 => 1611278170,
      2 => 'module',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_605526bad8e571_91426004 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
  'mobile_links' => 
  array (
    'compiled_filepath' => '/home2/amanoyarns/public_html/multitienda/var/cache/prod/smarty/compile/e0/77/dd/e077dd2170956816de1e46af35296e5cbbf8e702_2.module.iqitmegamenuviewstemplate.cache.php',
    'uid' => 'e077dd2170956816de1e46af35296e5cbbf8e702',
    'call_name' => 'smarty_template_function_mobile_links_1236060263605511540c1ac9_23941651',
  ),
  'categories_links' => 
  array (
    'compiled_filepath' => '/home2/amanoyarns/public_html/multitienda/var/cache/prod/smarty/compile/47/0d/5c/470d5c96fd175e37e89afd5cc78d331c9756e29d_2.module.iqitmegamenuviewstemplate.cache.php',
    'uid' => '470d5c96fd175e37e89afd5cc78d331c9756e29d',
    'call_name' => 'smarty_template_function_categories_links_20066765796055115408b872_10242995',
  ),
));
?>	<div id="iqitmegamenu-wrapper" class="iqitmegamenu-wrapper iqitmegamenu-all">
		<div class="container container-iqitmegamenu">
		<div id="iqitmegamenu-horizontal" class="iqitmegamenu  clearfix" role="navigation">

								
				<nav id="cbp-hrmenu" class="cbp-hrmenu cbp-horizontal cbp-hrsub-narrow">
					<ul>
												<li id="cbp-hrmenu-tab-7" class="cbp-hrmenu-tab cbp-hrmenu-tab-7 ">
	<a href="/new-in" class="nav-link" >

								<span class="cbp-tab-title">
								New In</span>
														</a>
													</li>
												<li id="cbp-hrmenu-tab-22" class="cbp-hrmenu-tab cbp-hrmenu-tab-22  cbp-has-submeu">
	<a href="/pe/2-yarns" class="nav-link" >

								<span class="cbp-tab-title">
								Yarns <i class="fa fa-angle-down cbp-submenu-aindicator"></i></span>
														</a>
														<div class="cbp-hrsub col-8">
								<div class="cbp-hrsub-inner">
									<div class="container iqitmegamenu-submenu-container">
									
																																	




<div class="row menu_row menu-element  first_rows menu-element-id-1">
                

                                                




    <div class="col-3 cbp-menu-column cbp-menu-element menu-element-id-2 ">
        <div class="cbp-menu-column-inner">
                        
                                                            <a href="/11-fiber"
                           class="cbp-column-title nav-link">Fiber </a>
                                    
                
                    
                                                    <ul class="cbp-links cbp-category-tree">
                                                                                                            <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/19-imperial-alpaca">Imperial Alpaca</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/20-royal-alpaca">Royal Alpaca</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/21-baby-alpaca">Baby Alpaca</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/22-merino-wool">Merino Wool</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/23-superwash-wool">Superwash Wool</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/163-peruvian-wool">Peruvian Wool</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/24-cashmere">Cashmere</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/25-mohair">Mohair</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/26-linen-">Linen</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/162-silk">Silk</a>

                                                                                            </div>
                                        </li>
                                                                                                                                                <li >
                                            <div class="cbp-category-link-w">
                                                <a href="https://amanoyarns.com/173-cotton">Cotton</a>

                                                                                            </div>
                                        </li>
                                                                                                </ul>
                                            
                
            

            
            </div>    </div>
                                    




    <div class="col-3 cbp-menu-column cbp-menu-element menu-element-id-3 ">
        <div class="cbp-menu-column-inner">
                        
                
                
                    
                                                    <div class="row cbp-categories-row">
                                                                                                            <div class="col-12">
                                            <div class="cbp-category-link-w"><a href="https://amanoyarns.com/16-weights"
                                                                                class="cbp-column-title nav-link cbp-category-title">Weights</a>
                                                                                                                                                    
    <ul class="cbp-links cbp-category-tree"><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.com/27-super-bulky">Super Bulky</a></div></li><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.com/28-chunky">Chunky</a></div></li><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.com/29-worsted">Worsted</a></div></li><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.com/31-sport">Sport</a></div></li><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.com/160-fingering">Fingering</a></div></li><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.com/161-lace">Lace</a></div></li></ul>

                                                                                            </div>
                                        </div>
                                                                                                </div>
                                            
                
            

            
            </div>    </div>
                                    




    <div class="col-3 cbp-menu-column cbp-menu-element menu-element-id-4 ">
        <div class="cbp-menu-column-inner">
                        
                                                            <a href="/17-products"
                           class="cbp-column-title nav-link">Products </a>
                                    
                
                                             <div class="row">
<div class="col-6">
<ul>
<li><a href="/49-225-apu.html#/27-color-glacier">Apu</a></li>
<li><a href="/50-229-eco-puna-black.html#/30-color-eco_puna_black">Eco Puna Black</a></li>
<li><a href="/yarns/51-230-eco-puna.html#/31-color-nevado_white">Eco Puna</a></li>
<li><a href="/yarns/52-401-puna.html#/97-color-blossom_field">Puna</a></li>
<li><a href="/yarns/53-212-warmi.html#/39-color-beetroot">Warmi</a></li>
<li><a href="/yarns/54-194-mamacha.html#/34-color-wheat">Mamacha</a></li>
<li><a href="/yarns/62-300-yana-xl.html#/187-color-lilac_daisy">Yana XL</a></li>
<li><a href="/yarns/63-443-yana.html#/178-color-scarlet_dream">Yana</a></li>
<li><a href="/yarns/65-436-yana-journeys.html#/189-color-ayacucho">Yana Journeys</a></li>
</ul>
</div>
<div class="col-6">
<ul>
<li><a href="/yarns/64-268-skinny-yana.html#/183-color-laguna">Skinny Yana</a></li>
<li><a href="/yarns/55-173-mayu.html#/49-color-frost_white">Mayu</a></li>
<li><a href="/yarns/56-384-mayu-lace.html#/69-color-french_rose">Mayu Lace</a></li>
<li><a href="/yarns/58-363-ayni.html#/122-color-coral">Ayni</a></li>
<li><a href="/yarns/57-365-puyu.html#/101-color-cloud">Puyu</a></li>
<li><a href="/yarns/60-318-pacha.html#/141-color-muted_oyster_white">Pacha</a></li>
<li><a href="/yarns/59-617-awa.html#/122-color-coral">Awa</a></li>
<li><a href="/yarns/61-613-chaski.html#/253-color-tulip">Chaski</a></li>
<li><a href="/yarns/66-242-sami.html#/199-color-seafoam">Sami</a></li>
<li><a href="/yarns/80-628-inti.html#/267-color-orchid_oasis">Inti</a></li>
</ul>
</div>
<div class="col-12 menu-element-id-9 my-4"><a href="/content/23-que-palitoscrochet-utilizar" class="btn-block"><span>¿Qué PALITOS</span><br /><span> /CROCHET utilizar? </span> </a></div>
</div>
                    
                
            

            
            </div>    </div>
                                    




    <div class="col-3 cbp-menu-column cbp-menu-element menu-element-id-5 ">
        <div class="cbp-menu-column-inner">
                        
                
                
                    
                                                    <div class="row cbp-categories-row">
                                                                                                            <div class="col-12">
                                            <div class="cbp-category-link-w"><a href="https://amanoyarns.com/184-family"
                                                                                class="cbp-column-title nav-link cbp-category-title">Family</a>
                                                                                                                                                    
    <ul class="cbp-links cbp-category-tree"><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.com/186-de-alpaca">De alpaca</a></div></li><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.com/185-de-lujo">De lujo</a></div></li><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.com/187-de-lana">De lana</a></div></li><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.com/188-de-mezclas-finas">De mezclas finas</a></div></li><li ><div class="cbp-category-link-w"><a href="https://amanoyarns.com/189-de-verano">De verano</a></div></li></ul>

                                                                                            </div>
                                        </div>
                                                                                                </div>
                                            
                
            

            
            </div>    </div>
                            
                </div>
																					
																			</div>
								</div>
							</div>
													</li>
												<li id="cbp-hrmenu-tab-6" class="cbp-hrmenu-tab cbp-hrmenu-tab-6  cbp-has-submeu">
	<a href="/content/15-patterns" class="nav-link" >

								<span class="cbp-tab-title">
								Patterns <i class="fa fa-angle-down cbp-submenu-aindicator"></i></span>
														</a>
														<div class="cbp-hrsub col-12">
								<div class="cbp-hrsub-inner">
									<div class="container iqitmegamenu-submenu-container">
									
																																	




<div class="row menu_row menu-element  first_rows menu-element-id-1">
                

                                                




    <div class="col-3 cbp-menu-column cbp-menu-element menu-element-id-4 ">
        <div class="cbp-menu-column-inner">
                        
                                                            <a href="/pe/17-products"
                           class="cbp-column-title nav-link">Products </a>
                                    
                
                    
                
            

            
            </div>    </div>
                                    




    <div class="col-9 cbp-menu-column cbp-menu-element menu-element-id-10 ">
        <div class="cbp-menu-column-inner">
                        
                
                
                                             <div class="row">
<div class="col-sm-4 col-md-4 col-lg-4 patterns-menu"><img class="w-100 patterns-img" src="/img/cms/patterns/1-1-Sarita-Mittens-Apu.jpg" alt="Apu" /> <a class="Patterns-logo" href="/en/content/15-patterns"><img src="/img/cms/patterns/marca/apu-see.jpg" /></a></div>
<div class="col-sm-4 col-md-4 col-lg-4 patterns-menu"><img class="w-100" src="/img/cms/patterns/2-1-Aria-Hat-Eco-Puna-Black.jpg" alt="Eco Puna Black" /> <a class="Patterns-logo" href="/en/content/15-patterns"> <img src="/img/cms/patterns/marca/eco-puna-black-see.jpg" /> </a></div>
<div class="col-sm-4 col-md-4 col-lg-4 patterns-menu"><img class="w-100" src="/img/cms/patterns/3-1-Lorena-Sweater-Eco-Puna.jpg" alt="Eco Puna" /> <a class="Patterns-logo" href="/en/content/15-patterns"><img class="w-75" src="/img/cms/patterns/marca/eco-puna-see.jpg" alt="Apu" /> </a></div>
</div>
                    
                
            

            
            </div>    </div>
                            
                </div>
																					
																			</div>
								</div>
							</div>
													</li>
												<li id="cbp-hrmenu-tab-10" class="cbp-hrmenu-tab cbp-hrmenu-tab-10 ">
	<a href="/blog" class="nav-link" >

								<span class="cbp-tab-title">
								Blog</span>
														</a>
													</li>
											</ul>
				</nav>
		</div>
		</div>
		<div id="sticky-cart-wrapper"></div>
	</div>

<div id="_desktop_iqitmegamenu-mobile">
	<ul id="iqitmegamenu-mobile">
		



<ul class="closeMenuMobile py-3" style="
    border-bottom: 1px solid;
">
<li class="menu-close row justify-content-center"> 
<div class="col-8 text-left pl-5">
    <img class="logo img-responsive" src="/img/logo-amano.png" alt="Amano" style="
    text-align: center;
    width: 50px;
">
    </div>
    <div class="col-4">
        <a class="close" data-toggle="dropdown" aria-label="Close">
          <span aria-hidden="true">×</span>
        </a>
    </div>
</li>
    </ul>









	
 

 
	<li><a  href="/new-in" >New In</a></li><li><a  href="/sale" >Sale</a></li><li><a  href="/2-yarns" >Yarns</a></li><li><a  href="/content/15-patterns" >Patterns</a></li><li><a  href="https://amanoyarns.com/content/4-about-us" >About us</a></li><li><a  href="/blog" >Blog</a></li><li><a  href="https://amanoyarns.com/content/1-contact" >Contact</a></li>





	</ul>
</div>
<?php }
}
