<?php
/* Smarty version 3.1.33, created on 2021-03-20 13:09:35
  from 'module:iqitelementorviewstemplat' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_60563a5fcde234_43581830',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2250361eb10369a5a6af91281303641da61fcf84' => 
    array (
      0 => 'module:iqitelementorviewstemplat',
      1 => 1611278170,
      2 => 'module',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_60563a5fcde234_43581830 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
));
?>

    		<style id="elementor-frontend-stylesheet">.elementor-element.elementor-element-0apkunh, .elementor-element.elementor-element-0apkunh > .elementor-background-overlay{border-radius:px px px px;}.elementor-element.elementor-element-0apkunh{margin-top:0px;margin-bottom:0px;padding:px px px px;}.elementor-element.elementor-element-gxf450h > .elementor-element-populated{border-radius:px px px px;margin:px px px px;padding:px px px px;}.elementor-element.elementor-element-an4x8n8 .elementor-iqit-banner-content{text-align:left;padding:0px 0px 0px 103px;}.elementor-element.elementor-element-an4x8n8 .elementor-iqit-banner .elementor-iqit-banner-title{margin:px 0;color:#ffffff;font-size:36px;line-height:em;letter-spacing:5.1px;}.elementor-element.elementor-element-an4x8n8 .elementor-iqit-banner .elementor-iqit-banner-description{color:#ffffff;font-size:px;line-height:em;letter-spacing:px;}.elementor-element.elementor-element-an4x8n8 .elementor-button{margin-top:px;}.elementor-element.elementor-element-an4x8n8 .elementor-widget-container{margin:px px px px;padding:px px px px;border-radius:px px px px;}.elementor-element.elementor-element-9k0rido .elementor-iqit-banner-content{text-align:left;padding:0px 0px 0px 103px;}.elementor-element.elementor-element-9k0rido .elementor-iqit-banner .elementor-iqit-banner-title{margin:px 0;color:#ffffff;font-size:36px;line-height:em;letter-spacing:5.1px;}.elementor-element.elementor-element-9k0rido .elementor-iqit-banner .elementor-iqit-banner-description{color:#ffffff;font-size:px;line-height:em;letter-spacing:px;}.elementor-element.elementor-element-9k0rido .elementor-button{margin-top:px;}.elementor-element.elementor-element-9k0rido .elementor-widget-container{margin:px px px px;padding:px px px px;border-radius:px px px px;}.elementor-element.elementor-element-mfbmp4s > .elementor-container{max-width:px;}.elementor-element.elementor-element-mfbmp4s, .elementor-element.elementor-element-mfbmp4s > .elementor-background-overlay{border-radius:px px px px;}.elementor-element.elementor-element-mfbmp4s{margin-top:50px;margin-bottom:50px;padding:px px px px;}.elementor-element.elementor-element-zige4iw > .elementor-element-populated{border-radius:px px px px;margin:px px px px;padding:0px 0px 0px 0px;}.elementor-element.elementor-element-8aagw14 .elementor-widget-container{margin:px px px px;padding:px px px px;border-radius:px px px px;}.elementor-element.elementor-element-i7i0sry > .elementor-element-populated{border-radius:px px px px;margin:px px px px;padding:10px 10px 10px 30px;}.elementor-element.elementor-element-bjshmhx .elementor-widget-container{margin:px px px px;padding:px px px px;border-radius:px px px px;}@media(max-width: 991px){.elementor-element.elementor-element-0apkunh{margin-top:px;margin-bottom:px;padding:px px px px;}.elementor-element.elementor-element-gxf450h > .elementor-element-populated{margin:px px px px;padding:px px px px;}.elementor-element.elementor-element-an4x8n8 .elementor-iqit-banner-content{text-align:center;padding:px px px px;}.elementor-element.elementor-element-an4x8n8 .elementor-iqit-banner .elementor-iqit-banner-title{margin:px 0;font-size:px;line-height:em;letter-spacing:px;}.elementor-element.elementor-element-an4x8n8 .elementor-iqit-banner .elementor-iqit-banner-description{font-size:px;line-height:em;letter-spacing:px;}.elementor-element.elementor-element-an4x8n8 .elementor-button{margin-top:px;}.elementor-element.elementor-element-an4x8n8 .elementor-widget-container{margin:px px px px;padding:px px px px;}.elementor-element.elementor-element-9k0rido .elementor-iqit-banner-content{text-align:center;padding:px px px px;}.elementor-element.elementor-element-9k0rido .elementor-iqit-banner .elementor-iqit-banner-title{margin:px 0;font-size:px;line-height:em;letter-spacing:px;}.elementor-element.elementor-element-9k0rido .elementor-iqit-banner .elementor-iqit-banner-description{font-size:px;line-height:em;letter-spacing:px;}.elementor-element.elementor-element-9k0rido .elementor-button{margin-top:px;}.elementor-element.elementor-element-9k0rido .elementor-widget-container{margin:px px px px;padding:px px px px;}.elementor-element.elementor-element-mfbmp4s{margin-top:px;margin-bottom:px;padding:px px px px;}.elementor-element.elementor-element-zige4iw > .elementor-element-populated{margin:px px px px;padding:px px px px;}.elementor-element.elementor-element-8aagw14 .elementor-widget-container{margin:px px px px;padding:px px px px;}.elementor-element.elementor-element-i7i0sry > .elementor-element-populated{margin:px px px px;padding:px px px px;}.elementor-element.elementor-element-bjshmhx .elementor-widget-container{margin:px px px px;padding:px px px px;}}@media(max-width: 767px){.elementor-element.elementor-element-0apkunh{margin-top:px;margin-bottom:px;padding:px px px px;}.elementor-element.elementor-element-gxf450h > .elementor-element-populated{margin:0px 0px 0px 0px;padding:0px 0px 0px 0px;}.elementor-element.elementor-element-an4x8n8 .elementor-iqit-banner-content{text-align:center;padding:px px px px;}.elementor-element.elementor-element-an4x8n8 .elementor-iqit-banner .elementor-iqit-banner-title{margin:px 0;font-size:px;line-height:em;letter-spacing:px;}.elementor-element.elementor-element-an4x8n8 .elementor-iqit-banner .elementor-iqit-banner-description{font-size:px;line-height:em;letter-spacing:px;}.elementor-element.elementor-element-an4x8n8 .elementor-button{margin-top:px;}.elementor-element.elementor-element-an4x8n8 .elementor-widget-container{margin:px px px px;padding:px px px px;}.elementor-element.elementor-element-9k0rido .elementor-iqit-banner-content{text-align:center;padding:80px 80px 80px 80px;}.elementor-element.elementor-element-9k0rido .elementor-iqit-banner .elementor-iqit-banner-title{margin:px 0;font-size:px;line-height:em;letter-spacing:px;}.elementor-element.elementor-element-9k0rido .elementor-iqit-banner .elementor-iqit-banner-description{font-size:px;line-height:em;letter-spacing:px;}.elementor-element.elementor-element-9k0rido .elementor-button{margin-top:px;}.elementor-element.elementor-element-9k0rido .elementor-widget-container{margin:0px 0px 0px 0px;padding:0px 0px 0px 0px;}.elementor-element.elementor-element-mfbmp4s{margin-top:px;margin-bottom:px;padding:px px px px;}.elementor-element.elementor-element-zige4iw > .elementor-element-populated{margin:px px px px;padding:px px px px;}.elementor-element.elementor-element-8aagw14 .elementor-widget-container{margin:px px px px;padding:px px px px;}.elementor-element.elementor-element-i7i0sry > .elementor-element-populated{margin:px px px px;padding:px px px px;}.elementor-element.elementor-element-bjshmhx .elementor-widget-container{margin:px px px px;padding:px px px px;}}@media (min-width: 768px) {.elementor-element.elementor-element-zige4iw{width:20.019%;}.elementor-element.elementor-element-i7i0sry{width:79.980%;}}</style>
				<div id="elementor" class="elementor">
			<div id="elementor-inner">
				<div id="elementor-section-wrap">
											        <div class="elementor-section elementor-element elementor-element-0apkunh elementor-top-section elementor-section-stretched elementor-section-full_width elementor-section-height-default elementor-section-height-default" data-element_type="section">
                        <div class="elementor-container elementor-column-gap-default">
                <div class="elementor-row">
        		<div class="elementor-column elementor-element elementor-element-gxf450h elementor-col-100 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
				<div class="elementor-widget-wrap">
		        <div class="elementor-widget elementor-element elementor-element-an4x8n8 elementor-widget-banner elementor-hidden-phone" data-element_type="banner">
                <div class="elementor-widget-container">
            <div class="elementor-iqit-banner"><figure class="elementor-iqit-banner-img"><span class="elementor-iqit-banner-overlay"></span><img src="https://amanoyarns.com/img/cms/patterns-amano-hero.jpg" alt=""></figure><div class="elementor-iqit-banner-content elementor-iqit-banner-content-on elementor-banner-align-middle-left"><h2 class="elementor-iqit-banner-title">Patrones de Tejido</h2><div class="elementor-iqit-banner-description">¡Inspírate del proyecto que más te guste y teje con nosotros!</div></div></div>        </div>
                </div>
                <div class="elementor-widget elementor-element elementor-element-9k0rido elementor-widget-banner elementor-hidden-desktop elementor-hidden-tablet" data-element_type="banner">
                <div class="elementor-widget-container">
            <div class="elementor-iqit-banner"><figure class="elementor-iqit-banner-img"><span class="elementor-iqit-banner-overlay"></span><img src="https://amanoyarns.com/img/cms/patterns/patterns-Amano-mobil.jpg" alt=""></figure><div class="elementor-iqit-banner-content elementor-iqit-banner-content-on elementor-banner-align-middle-center"><h2 class="elementor-iqit-banner-title">Patterns</h2></div></div>        </div>
                </div>
        				</div>
			</div>
		</div>
		                </div>
            </div>
        </div>
        											        <div class="elementor-section elementor-element elementor-element-mfbmp4s elementor-top-section elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-element_type="section">
                        <div class="elementor-container elementor-column-gap-default">
                <div class="elementor-row">
        		<div class="elementor-column elementor-element elementor-element-zige4iw elementor-col-50 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
				<div class="elementor-widget-wrap">
		        <div class="elementor-widget elementor-element elementor-element-8aagw14 elementor-widget-prestashop-widget-modules" data-element_type="prestashop-widget-modules">
                <div class="elementor-widget-container">
            

  <div class="block block-toggle block-categories block-links js-block-toggle">
    <h5 class="block-title"><span><a href="https://amanoyarns.com/pe/2-madejas">Madejas</a></span> </h5>
    <div class="category-top-menu block-content">
      
  <ul class="category-sub-menu"><li data-depth="0"><a href="https://amanoyarns.com/pe/11-fibras">Fibras</a><span class="collapse-icons" data-toggle="collapse" data-target="#exCollapsingNavbar11"><i class="fa fa-angle-down add" aria-hidden="true"></i><i class="fa fa-angle-up remove" aria-hidden="true"></i></span><div class="collapse" id="exCollapsingNavbar11">
  <ul class="category-sub-menu"><li data-depth="1"><a class="category-sub-link" href="https://amanoyarns.com/pe/20-royal-alpaca">Royal Alpaca</a></li><li data-depth="1"><a class="category-sub-link" href="https://amanoyarns.com/pe/21-baby-alpaca">Baby Alpaca</a></li><li data-depth="1"><a class="category-sub-link" href="https://amanoyarns.com/pe/22-lana-merino">Lana Merino</a></li><li data-depth="1"><a class="category-sub-link" href="https://amanoyarns.com/pe/23-lana-superwash">Lana Superwash</a></li><li data-depth="1"><a class="category-sub-link" href="https://amanoyarns.com/pe/24-cashmere">Cashmere</a></li><li data-depth="1"><a class="category-sub-link" href="https://amanoyarns.com/pe/25-mohair">Mohair</a></li><li data-depth="1"><a class="category-sub-link" href="https://amanoyarns.com/pe/26-lino">Lino</a></li><li data-depth="1"><a class="category-sub-link" href="https://amanoyarns.com/pe/162-seda">Seda</a></li><li data-depth="1"><a class="category-sub-link" href="https://amanoyarns.com/pe/163-lana-peruana">Lana Peruana</a></li><li data-depth="1"><a class="category-sub-link" href="https://amanoyarns.com/pe/173-algodon">Algodón</a></li><li data-depth="1"><a class="category-sub-link" href="https://amanoyarns.com/pe/12-vicuna">Vicuña</a></li><li data-depth="1"><a class="category-sub-link" href="https://amanoyarns.com/pe/179-baby-suri">Baby Suri</a></li><li data-depth="1"><a class="category-sub-link" href="https://amanoyarns.com/pe/19-imperial-alpaca">Imperial Alpaca</a></li></ul></div></li><li data-depth="0"><a href="https://amanoyarns.com/pe/16-grosor">Grosor</a><span class="collapse-icons" data-toggle="collapse" data-target="#exCollapsingNavbar16"><i class="fa fa-angle-down add" aria-hidden="true"></i><i class="fa fa-angle-up remove" aria-hidden="true"></i></span><div class="collapse" id="exCollapsingNavbar16">
  <ul class="category-sub-menu"><li data-depth="1"><a class="category-sub-link" href="https://amanoyarns.com/pe/27-super-bulky">Super Bulky</a></li><li data-depth="1"><a class="category-sub-link" href="https://amanoyarns.com/pe/28-chunky">Chunky</a></li><li data-depth="1"><a class="category-sub-link" href="https://amanoyarns.com/pe/29-worsted">Worsted</a></li><li data-depth="1"><a class="category-sub-link" href="https://amanoyarns.com/pe/31-sport">Sport</a></li><li data-depth="1"><a class="category-sub-link" href="https://amanoyarns.com/pe/160-fingering">Fingering</a></li><li data-depth="1"><a class="category-sub-link" href="https://amanoyarns.com/pe/161-lace">Lace</a></li></ul></div></li><li data-depth="0"><a href="https://amanoyarns.com/pe/17-productos">Productos</a></li><li data-depth="0"><a href="https://amanoyarns.com/pe/184-familia">Familia</a><span class="collapse-icons" data-toggle="collapse" data-target="#exCollapsingNavbar184"><i class="fa fa-angle-down add" aria-hidden="true"></i><i class="fa fa-angle-up remove" aria-hidden="true"></i></span><div class="collapse" id="exCollapsingNavbar184">
  <ul class="category-sub-menu"><li data-depth="1"><a class="category-sub-link" href="https://amanoyarns.com/pe/185-de-lujo">De lujo</a></li><li data-depth="1"><a class="category-sub-link" href="https://amanoyarns.com/pe/186-de-alpaca">De alpaca</a></li><li data-depth="1"><a class="category-sub-link" href="https://amanoyarns.com/pe/187-de-lana">De lana</a></li><li data-depth="1"><a class="category-sub-link" href="https://amanoyarns.com/pe/188-de-mezclas-finas">De mezclas finas</a></li><li data-depth="1"><a class="category-sub-link" href="https://amanoyarns.com/pe/189-de-verano">De verano</a></li></ul></div></li></ul>
    </div>
  </div>

        </div>
                </div>
        				</div>
			</div>
		</div>
				<div class="elementor-column elementor-element elementor-element-i7i0sry elementor-col-50 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
				<div class="elementor-widget-wrap">
		        <div class="elementor-widget elementor-element elementor-element-bjshmhx elementor-widget-html" data-element_type="html">
                <div class="elementor-widget-container">
                            <div class="row">
                  <!--  APU --->
                  <div class="col-6 col-sm-6 col-lg-3 gallery-patterns pb-3 pl-0" data-toggle="modal" data-target="#patterns1">
                     <img class="w-100 patterns-img"
                        src="/img/cms/patterns/1-1-Sarita-Mittens-Apu.jpg"
                        alt="Apu">
                     <a class="Patterns-logo-home" href="/34-apu"><img class=""
                        src="/img/cms/patterns/marca/apu.jpg"></a>
                  </div>
                  <!--  ECO PUNA BLACK --->
                  <div class="col-6 col-sm-6 col-lg-3 gallery-patterns pb-3 pl-0" data-toggle="modal" data-target="#patterns2">
                     <img class="w-100"
                        src="/img/cms/patterns/2-1-Aria-Hat-Eco-Puna-Black.jpg"
                        alt="Eco Puna Black">
                     <a class="Patterns-logo-home" href="/35-eco-puna-black"><img class=""
                        src="/img/cms/patterns/marca/eco-puna-black.jpg"> </a>
                  </div>
                  <!--  ECO PUNA --->
                  <div class="col-6 col-sm-6 col-lg-3 gallery-patterns pb-3 pl-0" data-toggle="modal" data-target="#patterns3">
                     <img class="w-100"
                        src="/img/cms/patterns/3-1-Lorena-Sweater-Eco-Puna.jpg"
                        alt="Eco Puna">
                     <a class="Patterns-logo-home" href="/158-eco-puna"><img class=""
                        src="/img/cms/patterns/marca/eco-puna.jpg" alt="Apu"> </a>
                  </div>
                  <!--  PUNA --->
                  <div class="col-6 col-sm-6 col-lg-3 gallery-patterns pb-3 pl-0" data-toggle="modal" data-target="#patterns4">
                     <img class="w-100"
                        src="/img/cms/patterns/4-1-Tamara-Cowl-Puna.jpg" alt="Puna">
                     <a class="Patterns-logo-home" href="/36-puna"><img class=""
                        src="/img/cms/patterns/marca/puna.jpg"></a>
                  </div>
               </div>
               <!-- End row -->
           
            
            
               <div class="row">
                  <!-- WARMI --->
                  <div class="col-6 col-sm-6 col-lg-3 gallery-patterns pb-3 pl-0" data-toggle="modal" data-target="#patterns5">
                     <img class="w-100"
                        src="/img/cms/patterns/5-1-Eva-Hat-Warmi.jpg" alt="Warmi">
                     <a class="Patterns-logo-home" href="/37-warmi"><img class=""
                        src="/img/cms/patterns/marca/warmi.jpg"></a>
                  </div>
                  <!-- MAMACHA --->
                  <div class="col-6 col-sm-6 col-lg-3 gallery-patterns pb-3 pl-0" data-toggle="modal" data-target="#patterns6">
                     <img class="w-100"
                        src="/img/cms/patterns/6-1-Silvia-Cowl-Mamacha.jpg"
                        alt="Mamacha">
                     <a class="Patterns-logo-home" href="/38-mamacha"><img class=""
                        src="/img/cms/patterns/marca/mamacha.jpg"></a>
                  </div>
                  <!-- YANA XL --->
                  <div class="col-6 col-sm-6 col-lg-3 gallery-patterns pb-3 pl-0" data-toggle="modal" data-target="#patterns7">
                     <img class="w-100"
                        src="/img/cms/patterns/7-1-Paola-Colorblock-Cowl-Yana-XL.jpg"
                        alt="Yana Xl">
                     <a class="Patterns-logo-home" href="/39-yana-xl"><img class=""
                        src="/img/cms/patterns/marca/yana-xl.jpg"></a>
                  </div>
                  <div class="col-6 col-sm-6 col-lg-3 gallery-patterns pb-3 pl-0" data-toggle="modal" data-target="#patterns8">
                     <img class="w-100"
                        src="/img/cms/patterns/8-1-Carmela-Hat-Yana.jpg" alt="Yana">
                     <a class="Patterns-logo-home" href="/40-yana"><img class=""
                        src="/img/cms/patterns/marca/yana.jpg"></a>
                  </div>
               </div>
               <!-- END ROW-->
         
         
            
               <div class="row">
                  <!-- YANA JOURNEYS --->
                  <div class="col-6 col-sm-6 col-lg-3 gallery-patterns pb-3 pl-0" data-toggle="modal" data-target="#patterns9">
                     <img class="w-100"
                        src="/img/cms/patterns/9-1-Antonella-Cowl-Yana-Journeys.jpg"
                        alt="Yana Journeys">
                     <a class="Patterns-logo-home" href="/41-yana-viajes"><img class=""
                        src="/img/cms/patterns/marca/yana-journeys.jpg"></a>
                  </div>
                  <!-- SKINNY YANA --->
                  <div class="col-6 col-sm-6 col-lg-3 gallery-patterns pb-3 pl-0" data-toggle="modal" data-target="#patterns10">
                     <img class="w-100"
                        src="/img/cms/patterns/10-1-Eva-Hat-Skinny-Yana.jpg"
                        alt="Skinny Yana">
                     <a class="Patterns-logo-home" href="/42-skinny-yana"><img class=""
                        src="/img/cms/patterns/marca/skinny-yana.jpg"></a>
                  </div>
                  <!-- MAYU --->
                  <div class="col-6 col-sm-6 col-lg-3 gallery-patterns pb-3 pl-0" data-toggle="modal" data-target="#patterns11">
                     <img class="w-100"
                        src="/img/cms/patterns/11-1-Gracia-Capelet-Mayu.jpg"
                        alt="Mayu">
                     <a class="Patterns-logo-home" href="/43-mayu"><img class=""
                        src="/img/cms/patterns/marca/mayu.jpg"></a>
                  </div>
                  <!-- MAYU LACE --->
                  <div class="col-6 col-sm-6 col-lg-3 gallery-patterns pb-3 pl-0" data-toggle="modal" data-target="#patterns12">
                     <img class="w-100"
                        src="/img/cms/patterns/12-1-Susana-Capelet-Mayu-Lace.jpg"
                        alt="Mayu Lace">
                     <a class="Patterns-logo-home" href="/44-mayu-lace"><img class=""
                        src="/img/cms/patterns/marca/mayu-lace.jpg"></a>
                  </div>
               </div>
         
           
           
               <div class="row">
                  <!-- PUYU --->
                  <div class="col-6 col-sm-6 col-lg-3 gallery-patterns pb-3 pl-0" data-toggle="modal" data-target="#patterns13">
                     <img class="w-100"
                        src="/img/cms/patterns/13-2-Maite-Cowl-Puyu.jpg" alt="Puyu">
                     <a class="Patterns-logo-home" href="/46-puyu"><img class=""
                        src="/img/cms/patterns/marca/puyu.jpg"></a>
                  </div>
                  <!-- PACHA --->
                  <div class="col-6 col-sm-6 col-lg-3 gallery-patterns pb-3 pl-0" data-toggle="modal" data-target="#patterns14">
                     <img class="w-100"
                        src="/img/cms/patterns/14-1-Nadia-Pullover-Pacha.jpg"
                        alt="Pacha">
                     <a class="Patterns-logo-home" href="/174-pacha"><img class=""
                        src="/img/cms/patterns/marca/pacha.jpg"></a>
                  </div>
                    <!-- WARMI --->
                  <div class="col-6 col-sm-6 col-lg-3 gallery-patterns pb-3 pl-0" data-toggle="modal" data-target="#patterns15">
                     <img class="w-100"
                        src="/img/cms/patterns/15-1-amano-dia-Warmi.jpg" alt="Warmi">
                     <a class="Patterns-logo-home" href="/37-warmi"><img class=""
                        src="/img/cms/patterns/marca/warmi.jpg"></a>
                  </div>
                  <!-- PACHA --->
                  <div class="col-6 col-sm-6 col-lg-3 gallery-patterns pb-3 pl-0" data-toggle="modal" data-target="#patterns16">
                     <img class="w-100"
                        src="/img/cms/patterns/16-1-amano-dia-Pacha.jpg"
                        alt="Pacha">
                     <a class="Patterns-logo-home" href="/174-pacha"><img class=""
                        src="/img/cms/patterns/marca/pacha.jpg"></a>
                  </div>
               </div>
           
            <!-- End row-->
    
      <!-- MODALES PATTERNS -->
      <!-- MODAL APU -->
      <div class="modal fade" id="patterns1" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="row align-items-center">
               <!-- INFO PATTERNS -->
            <div class="col-md-5 patterns-text">
               <div class="info-patterns text-center">
                  <img class="w-50" src="/img/cms/patterns/01-apu-icon.jpg"> 
                  <h3>Mitones Sarita</h3>
                  <p class="title-info-patherns"><strong>Diseñadora</strong></p>
                  <p class="title-info-patherns">Amy Gunderson</p>
                  <h4>Composición</h4>
                  <p>100% Imperial Alpaca</p>
                  <p><strong>Tamaño agujas:</strong> 3 (3.25 mm)  </p> 
                  <p><strong>Madejas:</strong>  2 ovillos de 25 gr</p>
                  <p><strong>Colores:</strong> #1000</p> 
               </div>
               <div class="text-center py-2">
                  <a href="/34-apu" class="btn btn-secondary btn-modal btn-modal float-none">Ver Productos</a>
               </div>
            </div>
             <!-- END INFO PATTERNS -->
               <!-- SLIDER PHOTOS PATTERNS -->
            <div class="col-md-7 patterns-photos">
               <div class="modal-body">
                  <div id="carruselApu" class="carousel slide" data-ride="carousel2">
                     <ol class="carousel-indicators">
                        <li data-target="#carruselApu" data-slide-to="0" class="active"></li>
                        <li data-target="#carruselApu" data-slide-to="1"></li>
                        <li data-target="#carruselApu" data-slide-to="2"></li>
                     </ol>
                     <div class="carousel-inner">
                        <div class="modal-close">
                           <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                           <span aria-hidden="true">&times;</span>
                           </button>
                        </div>
                        <div class="carousel-item active"> 
                            <img class="d-block w-100" src="/img/cms/patterns/1-1-Sarita-Mittens-Apu.jpg" alt="First slide">
                        </div>
                        <div class="carousel-item">
                             <img class="d-block w-100" src="/img/cms/patterns/1-4-Sarita-Mittens-Apu.jpg" alt="Third slide">
                        </div>
                        <div class="carousel-item">
                           <img class="d-block w-100" src="/img/cms/patterns/1-2-Sarita-Mittens-Apu.jpg" alt="Second slide">
                        </div>
                     </div>
                  </div>
               
                 <a class="carousel-control-prev" href="#carruselApu" role="button" data-slide="prev">
               <span class="carousel-control-prev-icon" aria-hidden="true"></span>
               <span class="sr-only">Previous</span>
               </a>
               <a class="carousel-control-next" href="#carruselApu" role="button" data-slide="next">
               <span class="carousel-control-next-icon" aria-hidden="true"></span>
               <span class="sr-only">Next</span>
             </a>
               </div>
            </div>
            <!-- END SLIDER PHOTOS PATTERNS -->
         </div>
      </div>
   </div>
</div>
      <!--  END APU --->
      <!-- MODAL ECO PUNA BLACK -->
      <div class="modal fade" id="patterns2" tabindex="-1" role="dialog" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="row align-items-center">
                   <!-- INFO PATTERNS -->
            <div class="col-md-5 patterns-text">
               <div class="info-patterns text-center">
                  <img class="w-50" src="/img/cms/patterns/02-ecopunablack-icon.jpg"> 
                  <h3>Gorro Aria</h3>
                  <p class="title-info-patherns"><strong>Diseñadora</strong></p>
                  <p class="title-info-patherns">Amy Gunderson</p>
                  <h4>Composición</h4>
                  <p>100% Baby Alpaca</p>
                  <p><strong>Tamaño agujas:</strong> Agujas circulares - 4 (3.5 mm)</p> 
                  <p><strong>Madejas:</strong>  3 ovillos de 25 gr</p>
                  <p><strong>Colores:</strong> #9005</p>
               </div>
               <div class="text-center py-2">
                  <a href="/35-eco-puna-black" class="btn btn-secondary btn-modal btn-modal float-none">Ver Productos</a>
               </div>
            </div>
             <!-- END INFO PATTERNS -->
                  <!-- SLIDER PHOTOS PATTERNS -->
                  <div class="col-md-7 patterns-photos">
                     <div class="modal-body">
                        <div id="carouselEcoPunaBlack" class="carousel slide">
                           <ol class="carousel-indicators">
                              <li data-target="#carouselEcoPunaBlack" data-slide-to="0" class="active"></li>
                              <li data-target="#carouselEcoPunaBlack" data-slide-to="1"></li>
                              <li data-target="#carouselEcoPunaBlack" data-slide-to="2"></li>
                           </ol>
                           <div class="carousel-inner">
                              <div class="modal-close">
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true">&times;</span>
                                 </button>
                              </div>
                              <div class="carousel-item active">
                                 <img class="d-block w-100"
                                    src="/img/cms/patterns/2-1-Aria-Hat-Eco-Puna-Black.jpg"
                                    alt="Aria Hat - Eco Puna Black">
                              </div>
                              <div class="carousel-item">
                                 <img class="d-block w-100"
                                    src="/img/cms/patterns/2-2-Aria-Hat-Eco-Puna-Black.jpg"
                                    alt="Aria Hat - Eco Puna Black">
                              </div>
                              <div class="carousel-item">
                                 <img class="d-block w-100"
                                    src="/img/cms/patterns/2-3-Aria-Hat-Eco-Puna-Black.jpg"
                                    alt="Aria Hat - Eco Puna Black">
                              </div>
                           </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselEcoPunaBlack" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselEcoPunaBlack" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                        </a>
                     </div>
                  </div>
                  <!-- END SLIDER PHOTOS PATTERNS -->
               </div>
            </div>
         </div>
      </div>
      <!--  END ECO PUNA BLACK --->
      <!-- MODAL ECO PUNA -->
      <div class="modal fade" id="patterns3" tabindex="-1" role="dialog" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="row align-items-center">
                 <!-- INFO PATTERNS -->
            <div class="col-md-5 patterns-text">
               <div class="info-patterns text-center">
                  <img class="w-50" src="/img/cms/patterns/03-ecopuna-icon.jpg"> 
                  <h3>Suéter Lorena </h3>
                  <p class="title-info-patherns"><strong>Diseñadora</strong></p>
                  <p class="title-info-patherns">Sandi Rosner</p>
                  <h4>Composición</h4>
                  <p>100% Baby Alpaca</p>
                  <p><strong>Tamaño agujas:</strong> Agujas circulares - 6 (4mm) </p>
                  <p><strong>Madejas:</strong>  5 madejas de 100 gr </p>
                  <p><strong>Colores:</strong>  #90008 - 1 madeja, #9004 - 2 madejas, #9007 -2 madejas</p>

               </div>
               <div class="text-center py-2">
                  <a href="/158-eco-puna" class="btn btn-secondary btn-modal btn-modal float-none">Ver Productos</a>
               </div>
            </div>
            <!-- END INFO PATTERNS -->
                  <!-- SLIDER PHOTOS PATTERNS -->
                  <div class="col-md-7 patterns-photos">
                     <div class="modal-body">
                        <div id="carouselEcoPuna" class="carousel slide">
                           <ol class="carousel-indicators">
                              <li data-target="#carouselEcoPuna" data-slide-to="0" class="active"></li>
                              <li data-target="#carouselEcoPuna" data-slide-to="1"></li>
                              <li data-target="#carouselEcoPuna" data-slide-to="2"></li>
                           </ol>
                           <div class="carousel-inner">
                              <div class="modal-close">
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true">&times;</span>
                                 </button>
                              </div>
                              <div class="carousel-item active">
                                 <img class="d-block w-100"
                                    src="/img/cms/patterns/3-1-Lorena-Sweater-Eco-Puna.jpg"
                                    alt="First slide">
                              </div>
                              <div class="carousel-item">
                                 <img class="d-block w-100"
                                    src="/img/cms/patterns/3-2-Lorena-Sweater-Eco-Puna.jpg"
                                    alt="Second slide">
                              </div>
                              <div class="carousel-item">
                                 <img class="d-block w-100"
                                    src="/img/cms/patterns/3-3-Lorena-Sweater-Eco-Puna.jpg"
                                    alt="Third slide">
                              </div>
                           </div>
                           <a class="carousel-control-prev" href="#carouselEcoPuna" role="button" data-slide="prev">
                           <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                           <span class="sr-only">Previous</span>
                           </a>
                           <a class="carousel-control-next" href="#carouselEcoPuna" role="button" data-slide="next">
                           <span class="carousel-control-next-icon" aria-hidden="true"></span>
                           <span class="sr-only">Next</span>
                           </a>
                        </div>
                     </div>
                     <!-- END SLIDER PHOTOS PATTERNS -->
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--  END ECO PUNA --->
      <!-- MODAL PUNA -->
      <div class="modal fade" id="patterns4" tabindex="-1" role="dialog" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="row align-items-center">
                  <!-- INFO PATTERNS -->
            <div class="col-md-5 patterns-text">
               <div class="info-patterns text-center">
                  <img class="w-50" src="/img/cms/patterns/04-puna-icon.jpg"> 
                  <h3>Cuello Tamara</h3>
                  <p class="title-info-patherns"><strong>Diseñadora</strong></p>
                  <p class="title-info-patherns">Natalya Berenzynska</p>
                  <h4>Composición</h4>
                  <p>100% Baby Alpaca</p>
                  <p><strong>Tamaño agujas:</strong> Agujas circulares - 7 (4.5mm)  </p> 
                  <p><strong>Madejas:</strong> 3 madejas de 100 gr</p>
                  <p><strong>Colores:</strong> #90008 - 1 madeja, #9004 - 2 madejas, #9007 -2 madejas</p>
               </div>
               <div class="text-center py-2">
                  <a href="/36-puna" class="btn btn-secondary btn-modal btn-modal float-none">Ver Productos</a>
               </div>
            </div>
            <!-- END INFO PATTERNS -->
                  <!-- SLIDER PHOTOS PATTERNS -->
                  <div class="col-md-7 patterns-photos">
                     <div class="modal-body">
                        <div id="carouselPuna" class="carousel slide">
                           <ol class="carousel-indicators">
                              <li data-target="#carouselPuna" data-slide-to="0" class="active"></li>
                              <li data-target="#carouselPuna" data-slide-to="1"></li>
                              <li data-target="#carouselPuna" data-slide-to="2"></li>
                           </ol>
                           <div class="carousel-inner">
                              <div class="modal-close">
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true">&times;</span>
                                 </button>
                              </div>
                              <div class="carousel-item active">
                                 <img class="d-block w-100"
                                    src="/img/cms/patterns/4-1-Tamara-Cowl-Puna.jpg"
                                    alt="First slide">
                              </div>
                              <div class="carousel-item">
                                 <img class="d-block w-100"
                                    src="/img/cms/patterns/4-2-Tamara-Cowl-Puna.jpg"
                                    alt="Second slide">
                              </div>
                              <div class="carousel-item">
                                 <img class="d-block w-100"
                                    src="/img/cms/patterns/4-3-Tamara-Cowl-Puna.jpg"
                                    alt="Third slide">
                              </div>
                           </div>
                           <a class="carousel-control-prev" href="#carouselPuna" role="button" data-slide="prev">
                           <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                           <span class="sr-only">Previous</span>
                           </a>
                           <a class="carousel-control-next" href="#carouselPuna" role="button" data-slide="next">
                           <span class="carousel-control-next-icon" aria-hidden="true"></span>
                           <span class="sr-only">Next</span>
                           </a>
                        </div>
                     </div>
                  </div>
                  <!-- END SLIDER PHOTOS PATTERNS -->
               </div>
            </div>
         </div>
      </div>
      <!--  END PUNA --->
      <!-- MODAL WARMI -->
      <div class="modal fade" id="patterns5" tabindex="-1" role="dialog" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="row align-items-center">
                   <!-- INFO PATTERNS -->
            <div class="col-md-5 patterns-text">
               <div class="info-patterns text-center">
                  <img class="w-50" src="/img/cms/patterns/05-warmi-icon.jpg">
                  <h3>Gorro Eva</h3>
                  <p class="title-info-patherns"><strong>Diseñadora</strong></p>
                  <p class="title-info-patherns">Sandi Rosner</p>
                  <h4>Composición</h4>
                  <p>70% Baby Alpaca, 30% Oveja Merino</p>
                  <p><strong>Tamaño agujas:</strong> 8 (5mm)  </p> 
                  <p><strong>Madejas:</strong> 2 Madejas de 100gr</p>
                  <p><strong>Colores:</strong> #6010 -1 madeja, #6015 1 madeja</p>
               </div>
               <div class="text-center py-2">
                  <a href="/37-warmi" class="btn btn-secondary btn-modal btn-modal float-none">Ver Productos</a>
               </div>
            </div>
            <!-- END INFO PATTERNS -->
                  <!-- SLIDER PHOTOS PATTERNS -->
                  <div class="col-md-7 patterns-photos">
                     <div class="modal-body">
                        <div id="carouselWarmi" class="carousel slide">
                           <ol class="carousel-indicators">
                              <li data-target="#carouselWarmi" data-slide-to="0" class="active"></li>
                              <li data-target="#carouselWarmi" data-slide-to="1"></li>
                              <li data-target="#carouselWarmi" data-slide-to="2"></li>
                           </ol>
                           <div class="carousel-inner">
                              <div class="modal-close">
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true">&times;</span>
                                 </button>
                              </div>
                              <div class="carousel-item active">
                                 <img class="d-block w-100"
                                    src="/img/cms/patterns/5-1-Eva-Hat-Warmi.jpg"
                                    alt="First slide">
                              </div>
                              <div class="carousel-item">
                                 <img class="d-block w-100"
                                    src="/img/cms/patterns/5-2-Eva-Hat-Warmi.jpg"
                                    alt="Second slide">
                              </div>
                              <div class="carousel-item">
                                 <img class="d-block w-100"
                                    src="/img/cms/patterns/5-3-Eva-Hat-Warmi.jpg"
                                    alt="Third slide">
                              </div>
                           </div>
                           <a class="carousel-control-prev" href="#carouselWarmi" role="button" data-slide="prev">
                           <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                           <span class="sr-only">Previous</span>
                           </a>
                           <a class="carousel-control-next" href="#carouselWarmi" role="button" data-slide="next">
                           <span class="carousel-control-next-icon" aria-hidden="true"></span>
                           <span class="sr-only">Next</span>
                           </a>
                        </div>
                     </div>
                  </div>
                  <!-- END SLIDER PHOTOS PATTERNS -->
               </div>
            </div>
         </div>
      </div>
      <!-- END WARMI --->
      <!-- MODAL MAMACHA -->
      <div class="modal fade" id="patterns6" tabindex="-1" role="dialog" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="row align-items-center">
                   <!-- INFO PATTERNS -->
            <div class="col-md-5 patterns-text">
               <div class="info-patterns text-center">
                  <img class="w-50" src="/img/cms/patterns/06-mamacha-icon.jpg">
                  <h3>Cuello Silvia</h3>
                  <p class="title-info-patherns"><strong>Diseñadora</strong></p>
                  <p class="title-info-patherns">Sandi Rosner</p>
                  <h4>Composición</h4>
                  <p>70% Baby Alpaca, 30% Oveja Merino</p>
                  <p><strong>Tamaño agujas:</strong> Agujas circulares - 15 (10mm)/ 13 (9mm)</p> 
                  <p><strong>Madejas:</strong> 4 Madejas de 100 gr</p>
                  <p><strong>Colores:</strong> #8014 - 2 madejas, #8011 - 2 madejas</p>
               </div>
               <div class="text-center py-2">
                  <a href="/38-mamacha" class="btn btn-secondary btn-modal btn-modal float-none">Ver Productos</a>
               </div>
            </div>
            <!-- END INFO PATTERNS -->
                  <!-- SLIDER PHOTOS PATTERNS -->
                  <div class="col-md-7 patterns-photos">
                     <div class="modal-body">
                        <div id="carouselMamacha" class="carousel slide">
                           <ol class="carousel-indicators">
                              <li data-target="#carouselMamacha" data-slide-to="0" class="active"></li>
                              <li data-target="#carouselMamacha" data-slide-to="1"></li>
                              <li data-target="#carouselMamacha" data-slide-to="2"></li>
                           </ol>
                           <div class="carousel-inner">
                              <div class="modal-close">
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true">&times;</span>
                                 </button>
                              </div>
                              <div class="carousel-item active">
                                 <img class="d-block w-100" src="/img/cms/patterns/6-1-Silvia-Cowl-Mamacha.jpg" alt="First slide">
                              </div>
                              <div class="carousel-item">
                                 <img class="d-block w-100" src="/img/cms/patterns/6-2-Silvia-Cowl-Mamacha.jpg" alt="Second slide">
                              </div>
                              <div class="carousel-item">
                                 <img class="d-block w-100" src="/img/cms/patterns/6-3-Silvia-Cowl-Mamacha.jpg" alt="Third slide">
                              </div>
                              <div class="carousel-item">
                                 <img class="d-block w-100" src="/img/cms/patterns/6-4-Silvia-Cowl-Mamacha.jpg" alt="Third slide">
                              </div>
                           </div>
                           <a class="carousel-control-prev" href="#carouselMamacha" role="button" data-slide="prev">
                           <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                           <span class="sr-only">Previous</span>
                           </a>
                           <a class="carousel-control-next" href="#carouselMamacha" role="button" data-slide="next">
                           <span class="carousel-control-next-icon" aria-hidden="true"></span>
                           <span class="sr-only">Next</span>
                           </a>
                        </div>
                     </div>
                  </div>
                  <!-- END SLIDER PHOTOS PATTERNS -->
               </div>
            </div>
         </div>
      </div>
      <!-- END MAMACHA --->
      <!-- MODAL YANA XL -->
      <div class="modal fade" id="patterns7" tabindex="-1" role="dialog" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="row align-items-center">
                    <!-- INFO PATTERNS -->
            <div class="col-md-5 patterns-text">
               <div class="info-patterns text-center">
                  <img class="w-50" src="/img/cms/patterns/07-yanaxl-icon.jpg">
                  <h3>Cuello Paola</h3>
                  <p class="title-info-patherns"><strong>Diseñadora</strong></p>
                  <p class="title-info-patherns">Kate Atherley</p>
                  <h4>Composición</h4>
                  <p>100% Oveja Fina Peruana</p>
                  <p><strong>Tamaño agujas:</strong> 17 (12 mm)</p> 
                  <p><strong>Madejas:</strong> 3 Madejas de 200 gr </p>
                  <p><strong>Colores:</strong> #1402 - 1 madeja, #1414 -1 madeja, #1408 - 1 madeja</p>
               </div>
               <div class="text-center py-2">
                  <a href="/39-yana-xl" class="btn btn-secondary btn-modal btn-modal float-none">Ver Productos</a>
               </div>
            </div>
            <!-- END INFO PATTERNS -->
                  <!-- SLIDER PHOTOS PATTERNS -->
                  <div class="col-md-7 patterns-photos">
                     <div class="modal-body">
                        <div id="carouselYanaXl" class="carousel slide">
                           <ol class="carousel-indicators">
                              <li data-target="#carouselYanaXl" data-slide-to="0" class="active"></li>
                              <li data-target="#carouselYanaXl" data-slide-to="1"></li>
                              <li data-target="#carouselYanaXl" data-slide-to="2"></li>
                           </ol>
                           <div class="carousel-inner">
                              <div class="modal-close">
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true">&times;</span>
                                 </button>
                              </div>
                              <div class="carousel-item active">
                                 <img class="d-block w-100" src="/img/cms/patterns/7-1-Paola-Colorblock-Cowl-Yana-XL.jpg"
                                    alt="First slide">
                              </div>
                              <div class="carousel-item">
                                 <img class="d-block w-100" src="/img/cms/patterns/7-2-Paola-Colorblock-Cowl-Yana-XL.jpg"
                                    alt="Second slide">
                              </div>
                              <div class="carousel-item">
                                 <img class="d-block w-100" src="/img/cms/patterns/7-3-Paola-Colorblock-Cowl-Yana-XL.jpg"
                                    alt="Third slide">
                              </div>
                           </div>
                           <a class="carousel-control-prev" href="#carouselYanaXl" role="button" data-slide="prev">
                           <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                           <span class="sr-only">Previous</span>
                           </a>
                           <a class="carousel-control-next" href="#carouselYanaXl" role="button" data-slide="next">
                           <span class="carousel-control-next-icon" aria-hidden="true"></span>
                           <span class="sr-only">Next</span>
                           </a>
                        </div>
                     </div>
                  </div>
                  <!-- END SLIDER PHOTOS PATTERNS -->
               </div>
            </div>
         </div>
      </div>
      <!-- END YANA XL --->
      <!-- MODAL YANA -->
      <div class="modal fade" id="patterns8" tabindex="-1" role="dialog" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="row align-items-center">
                  <!-- INFO PATTERNS -->
            <div class="col-md-5 patterns-text">
               <div class="info-patterns text-center">
                  <img class="w-50" src="/img/cms/patterns/08-yana-icon.jpg">
                  <h3>Gorro Carmela</h3>
                  <p class="title-info-patherns"><strong>Diseñadora</strong></p>
                  <p class="title-info-patherns">Kate Atherley</p>
                  <h4>Composición</h4>
                  <p>100% Oveja Fina Peruana</p>
                  <p><strong>Tamaño agujas:</strong> Agujas circulares - 10 (6 mm) </p> 
                  <p><strong>Madejas:</strong> 1 Madejas de 200 gr </p>
                  <p><strong>Colores:</strong> #1311</p>
               </div>
               <div class="text-center py-2">
                  <a href="/40-yana" class="btn btn-secondary btn-modal btn-modal float-none">Ver Productos</a>
               </div>
            </div>
            <!-- END INFO PATTERNS -->
                  <!-- SLIDER PHOTOS PATTERNS -->
                  <div class="col-md-7 patterns-photos">
                     <div class="modal-body">
                        <div id="carouselYana" class="carousel slide" data-ride="carousel">
                           <ol class="carousel-indicators">
                              <li data-target="#carouselYana" data-slide-to="0" class="active"></li>
                              <li data-target="#carouselYana" data-slide-to="1"></li>
                              <li data-target="#carouselYana" data-slide-to="2"></li>
                           </ol>
                           <div class="carousel-inner">
                              <div class="modal-close">
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true">&times;</span>
                                 </button>
                              </div>
                              <div class="carousel-item active">
                                 <img class="d-block w-100" src="/img/cms/patterns/8-1-Carmela-Hat-Yana.jpg" alt="First slide">
                              </div>
                              <div class="carousel-item">
                                 <img class="d-block w-100" src="/img/cms/patterns/8-2-Carmela-Hat-Yana.jpg" alt="Second slide">
                              </div>
                              <div class="carousel-item">
                                 <img class="d-block w-100" src="/img/cms/patterns/8-3-Carmela-Hat-Yana.jpg" alt="Third slide">
                              </div>
                           </div>
                           <a class="carousel-control-prev" href="#carouselYana" role="button" data-slide="prev">
                           <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                           <span class="sr-only">Previous</span>
                           </a>
                           <a class="carousel-control-next" href="#carouselYana" role="button" data-slide="next">
                           <span class="carousel-control-next-icon" aria-hidden="true"></span>
                           <span class="sr-only">Next</span>
                           </a>
                        </div>
                     </div>
                  </div>
                  <!-- END SLIDER PHOTOS PATTERNS -->
               </div>
            </div>
         </div>
      </div>
      <!-- END YANA -->
      <!-- MODAL YANA JOURNEYS -->
      <div class="modal fade" id="patterns9" tabindex="-1" role="dialog" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="row align-items-center">
                 <!-- INFO PATTERNS -->
            <div class="col-md-5 patterns-text">
               <div class="info-patterns text-center">
                  <img class="w-50" src="/img/cms/patterns/09-yanaviajes-icon.jpg">
                  <h3>Cuello Antonella</h3>
                  <p class="title-info-patherns"><strong>Diseñadora</strong></p>
                  <p class="title-info-patherns">Kate Atherley</p>
                  <h4>Composición</h4>
                  <p>100% Oveja Fina Peruana</p>
                  <p><strong>Tamaño agujas:</strong> Agujas circulares - 10½ (6.5 mm)</p> 
                  <p><strong>Madejas:</strong> 1 Madejas de 200 gr </p>
                  <p><strong>Colores:</strong> #1600</p>
               </div>
               <div class="text-center py-2">
                  <a href="/41-yana-viajes" class="btn btn-secondary btn-modal btn-modal float-none">Ver Productos</a>
               </div>
            </div>
            <!-- END INFO PATTERNS -->
                  <!-- SLIDER PHOTOS PATTERNS -->
                  <div class="col-md-7 patterns-photos">
                     <div class="modal-body">
                        <div id="carouselYanaViajes" class="carousel slide" data-ride="carousel">
                           <ol class="carousel-indicators">
                              <li data-target="#carouselYanaViajes" data-slide-to="0" class="active"></li>
                              <li data-target="#carouselYanaViajes" data-slide-to="1"></li>
                              <li data-target="#carouselYanaViajes" data-slide-to="2"></li>
                           </ol>
                           <div class="carousel-inner">
                              <div class="modal-close">
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true">&times;</span>
                                 </button>
                              </div>
                              <div class="carousel-item active">
                                 <img class="d-block w-100" src="/img/cms/patterns/9-1-Antonella-Cowl-Yana-Journeys.jpg"
                                    alt="First slide">
                              </div>
                              <div class="carousel-item">
                                 <img class="d-block w-100" src="/img/cms/patterns/9-2-Antonella-Cowl-Yana-Journeys.jpg"
                                    alt="Second slide">
                              </div>
                              <div class="carousel-item">
                                 <img class="d-block w-100" src="/img/cms/patterns/9-3-Antonella-Cowl-Yana-Journeys.jpg"
                                    alt="Third slide">
                              </div>
                           </div>
                           <a class="carousel-control-prev" href="#carouselYanaViajes" role="button" data-slide="prev">
                           <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                           <span class="sr-only">Previous</span>
                           </a>
                           <a class="carousel-control-next" href="#carouselYanaViajes" role="button" data-slide="next">
                           <span class="carousel-control-next-icon" aria-hidden="true"></span>
                           <span class="sr-only">Next</span>
                           </a>
                        </div>
                     </div>
                  </div>
                  <!-- END SLIDER PHOTOS PATTERNS -->
               </div>
            </div>
         </div>
      </div>
      <!-- END YANA JOURNEYS -->
 <!-- MODAL SKINNY YANA -->
      <div class="modal fade" id="patterns10" tabindex="-1" role="dialog" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="row align-items-center">
      <!-- INFO PATTERNS -->
            <div class="col-md-5 patterns-text">
               <div class="info-patterns text-center">
                  <img class="w-50" src="/img/cms/patterns/010-skinny-icon.jpg">
                  <h3>Gorro Evaluna</h3>
                  <p class="title-info-patherns"><strong>Diseñadora</strong></p>
                  <p class="title-info-patherns">Kate Atherley</p>
                  <h4>Composición</h4>
                  <p>100% Oveja Fina Peruana</p>
                  <p><strong>Tamaño agujas:</strong> Agujas circulares - 1½ (2.5 mm)/ 2 (2.75 mm)</p> 
                  <p><strong>Madejas:</strong> 2 Madejas de 100 gr </p>
                  <p><strong>Colores:</strong> #1502 - 1 madeja, #1508 - 1 madeja</p>
               </div>
               <div class="text-center py-2">
                  <a href="/42-skinny-yana" class="btn btn-secondary btn-modal btn-modal float-none">Ver Productos</a>
               </div>
            </div>
            <!-- END INFO PATTERNS -->
                  <!-- SLIDER PHOTOS PATTERNS -->
                  <div class="col-md-7 patterns-photos">
                     <div class="modal-body">
                        <div id="carouselSkinnyYana" class="carousel slide" data-ride="carousel">
                           <ol class="carousel-indicators">
                              <li data-target="#carouselSkinnyYana" data-slide-to="0" class="active"></li>
                              <li data-target="#carouselSkinnyYana" data-slide-to="1"></li>
                              <li data-target="#carouselSkinnyYana" data-slide-to="2"></li>
                           </ol>
                           <div class="carousel-inner">
                              <div class="modal-close">
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true">&times;</span>
                                 </button>
                              </div>
                              <div class="carousel-item active">
                                 <img class="d-block w-100" src="/img/cms/patterns/10-1-Eva-Hat-Skinny-Yana.jpg" alt="First slide">
                              </div>
                              <div class="carousel-item">
                                 <img class="d-block w-100" src="/img/cms/patterns/10-2-Eva-Hat-Skinny-Yana.jpg" alt="Second slide">
                              </div>
                              <div class="carousel-item">
                                 <img class="d-block w-100" src="/img/cms/patterns/10-3-Eva-Hat-Skinny-Yana.jpg" alt="Third slide">
                              </div>
                           </div>
                           <a class="carousel-control-prev" href="#carouselSkinnyYana" role="button" data-slide="prev">
                           <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                           <span class="sr-only">Previous</span>
                           </a>
                           <a class="carousel-control-next" href="#carouselSkinnyYana" role="button" data-slide="next">
                           <span class="carousel-control-next-icon" aria-hidden="true"></span>
                           <span class="sr-only">Next</span>
                           </a>
                        </div>
                     </div>
                  </div>
                  <!-- END SLIDER PHOTOS PATTERNS -->
               </div>
            </div>
         </div>
      </div>
      <!-- END SKYNNY YANA  -->
      <!-- MODAL MAYU -->
      <div class="modal fade" id="patterns11" tabindex="-1" role="dialog" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="row align-items-center">
                   <!-- INFO PATTERNS -->
            <div class="col-md-5 patterns-text">
               <div class="info-patterns text-center">
                  <img class="w-50" src="/img/cms/patterns/011-mayu-icon.jpg">
                  <h3>Poncho Gracia</h3>
                  <p class="title-info-patherns"><strong>Diseñadora</strong></p>
                  <p class="title-info-patherns">Kate Atherley</p>
                  <h4>Composición</h4>
                  <p>60% Royal Alpaca, 20% Cashmere, 20% Seda</p>
                  <p><strong>Tamaño agujas:</strong> Agujas circulares - 7 (4.5 mm)</p> 
                  <p><strong>Madejas:</strong> 4 madejas de 50 gr</p>
                  <p><strong>Colores:</strong> #2011</p>
               </div>
               <div class="text-center py-2">
                  <a href="/43-mayu" class="btn btn-secondary btn-modal btn-modal float-none">Ver Productos</a>
               </div>
            </div>
            <!-- END INFO PATTERNS -->
                  <!-- SLIDER PHOTOS PATTERNS -->
                  <div class="col-md-7 patterns-photos">
                     <div class="modal-body">
                        <div id="carouselMayu" class="carousel slide" data-ride="carousel">
                           <ol class="carousel-indicators">
                              <li data-target="#carouselMayu" data-slide-to="0" class="active"></li>
                              <li data-target="#carouselMayu" data-slide-to="1"></li>
                              <li data-target="#carouselMayu" data-slide-to="2"></li>
                           </ol>
                           <div class="carousel-inner">
                              <div class="modal-close">
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true">&times;</span>
                                 </button>
                              </div>
                              <div class="carousel-item active">
                                 <img class="d-block w-100" src="/img/cms/patterns/11-1-Gracia-Capelet-Mayu.jpg" alt="First slide">
                              </div>
                              <div class="carousel-item">
                                 <img class="d-block w-100" src="/img/cms/patterns/11-2-Gracia-Capelet-Mayu.jpg" alt="Second slide">
                              </div>
                              <div class="carousel-item">
                                 <img class="d-block w-100" src="/img/cms/patterns/11-3-Gracia-Capelet-Mayu.jpg" alt="Third slide">
                              </div>
                           </div>
                           <a class="carousel-control-prev" href="#carouselMayu" role="button" data-slide="prev">
                           <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                           <span class="sr-only">Previous</span>
                           </a>
                           <a class="carousel-control-next" href="#carouselMayu" role="button" data-slide="next">
                           <span class="carousel-control-next-icon" aria-hidden="true"></span>
                           <span class="sr-only">Next</span>
                           </a>
                        </div>
                     </div>
                  </div>
                  <!-- END SLIDER PHOTOS PATTERNS -->
               </div>
            </div>
         </div>
      </div>
      <!-- END MAYU  -->
      <!-- MADAL MAYU LACE -->
      <div class="modal fade" id="patterns12" tabindex="-1" role="dialog" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="row align-items-center">
                  <!-- INFO PATTERNS -->
            <div class="col-md-5 patterns-text">
               <div class="info-patterns text-center">
                  <img class="w-50" src="/img/cms/patterns/012-mayulace-icon.jpg">
                  <h3>Poncho Susana</h3>
                  <p class="title-info-patherns"><strong>Diseñadora</strong></p>
                  <p class="title-info-patherns">Kate Atherley</p>
                  <h4>Composición</h4>
                  <p>60% Royal Alpaca, 20% Cashmere, 20% Seda</p>
                  <p><strong>Tamaño agujas:</strong> Agujas circulares - 4 (3.5 mm)</p> 
                  <p><strong>Madejas:</strong> 2 madejas de 50 gr </p>
                  <p><strong>Colores:</strong> #2106</p>
               </div>
               <div class="text-center py-2">
                <a href="/44-mayu-lace" class="btn btn-secondary btn-modal btn-modal float-none">Ver Productos</a>
               </div>
            </div>
            <!-- END INFO PATTERNS -->
                  <!-- SLIDER PHOTOS PATTERNS -->
                  <div class="col-md-7 patterns-photos">
                     <div class="modal-body">
                        <div id="carouselMayuLace" class="carousel slide" data-ride="carousel">
                           <ol class="carousel-indicators">
                              <li data-target="#carouselMayuLace" data-slide-to="0" class="active"></li>
                              <li data-target="#carouselMayuLace" data-slide-to="1"></li>
                              <li data-target="#carouselMayuLace" data-slide-to="2"></li>
                           </ol>
                           <div class="carousel-inner">
                              <div class="modal-close">
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true">&times;</span>
                                 </button>
                              </div>
                              <div class="carousel-item active">
                                 <img class="d-block w-100" src="/img/cms/patterns/12-1-Susana-Capelet-Mayu-Lace.jpg"
                                    alt="First slide">
                              </div>
                              <div class="carousel-item">
                                 <img class="d-block w-100" src="/img/cms/patterns/12-2-Susana-Capelet-Mayu-Lace.jpg"
                                    alt="Second slide">
                              </div>
                              <div class="carousel-item">
                                 <img class="d-block w-100" src="/img/cms/patterns/12-3-Susana-Capelet-Mayu-Lace.jpg"
                                    alt="Third slide">
                              </div>
                           </div>
                           <a class="carousel-control-prev" href="#carouselMayuLace" role="button" data-slide="prev">
                           <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                           <span class="sr-only">Previous</span>
                           </a>
                           <a class="carousel-control-next" href="#carouselMayuLace" role="button" data-slide="next">
                           <span class="carousel-control-next-icon" aria-hidden="true"></span>
                           <span class="sr-only">Next</span>
                           </a>
                        </div>
                     </div>
                  </div>
                  <!-- END SLIDER PHOTOS PATTERNS -->
               </div>
            </div>
         </div>
      </div>
      <!-- END MAYU LACE  -->
      <!--  MODAL PUYU--->
      <div class="modal fade" id="patterns13" tabindex="-1" role="dialog" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="row align-items-center">
                  <!-- INFO PATTERNS -->
            <div class="col-md-5 patterns-text">
               <div class="info-patterns text-center">
                  <img class="w-50" src="/img/cms/patterns/013-puyu-icon.jpg">
                  <h3>Cuello Maite</h3>
                  <p class="title-info-patherns"><strong>Diseñadora</strong></p>
                  <p class="title-info-patherns">Mary Lou Egan</p>
                  <h4>Composición</h4>
                  <p>70% Baby Alpaca, 30% Seda</p>
                  <p><strong>Tamaño agujas:</strong> Agujas circulares - 13 (9 mm)</p> 
                  <p><strong>Madejas:</strong> 2 madejas de 50 gr </p>
                  <p><strong>Colores:</strong> #3008</p>
               </div> 
               <div class="text-center py-2">
                  <a href="/46-puyu" class="btn btn-secondary btn-modal btn-modal float-none">Ver Productos</a>
               </div>
            </div>
            <!-- END INFO PATTERNS -->
                  <!-- SLIDER PHOTOS PATTERNS -->
                  <div class="col-md-7 patterns-photos">
                     <div class="modal-body">
                        <div id="carouselPuyu" class="carousel slide" data-ride="carousel">
                           <ol class="carousel-indicators">
                              <li data-target="#carouselPuyu" data-slide-to="0" class="active"></li>
                              <li data-target="#carouselPuyu" data-slide-to="1"></li>
                              <li data-target="#carouselPuyu" data-slide-to="2"></li>
                           </ol>
                           <div class="carousel-inner">
                              <div class="modal-close">
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true">&times;</span>
                                 </button>
                              </div>
                              <div class="carousel-item active">
                                 <img class="d-block w-100" src="/img/cms/patterns/13-3-Maite-Cowl-Puyu.jpg" alt="Third slide">
                              </div>
                              <div class="carousel-item">
                                 <img class="d-block w-100" src="/img/cms/patterns/13-2-Maite-Cowl-Puyu.jpg" alt="Second slide">
                              </div>
                              <div class="carousel-item">
                                 <img class="d-block w-100" src="/img/cms/patterns/13-1-Maite-Cowl-Puyu.jpg" alt="First slide">
                              </div>
                           </div>
                           <a class="carousel-control-prev" href="#carouselPuyu" role="button" data-slide="prev">
                           <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                           <span class="sr-only">Previous</span>
                           </a>
                           <a class="carousel-control-next" href="#carouselPuyu" role="button" data-slide="next">
                           <span class="carousel-control-next-icon" aria-hidden="true"></span>
                           <span class="sr-only">Next</span>
                           </a>
                        </div>
                     </div>
                  </div>
                  <!-- END SLIDER PHOTOS PATTERNS -->
               </div>
            </div>
         </div>
      </div>
      <!-- END PUYU  -->
      <!-- MODAL PACHA -->
      <div class="modal fade" id="patterns14" tabindex="-1" role="dialog" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="row align-items-center">
                 <!-- INFO PATTERNS -->
            <div class="col-md-5 patterns-text">
               <div class="info-patterns text-center">
                  <img class="w-50" src="/img/cms/patterns/015-pacha-icon.jpg">
                  <h3>Suéter Nadia </h3>
                  <p class="title-info-patherns"><strong>Diseñadora</strong></p>
                  <p class="title-info-patherns">Mary Lou Egan</p>
                  <h4>Composición</h4>
                  <p>50 % Seda, 25 % Super Kid Mohair, 25 % Oveja Merino</p>
                  <p><strong>Tamaño agujas:</strong> Agujas circulares - 4 (3.5mm)/ 2 (2.75mm)</p> 
                  <p><strong>Madejas:</strong> 8 madejas de 50 gr </p>
                  <p><strong>Colores:</strong> #1201 - 3 madejas, #1202 - 1 madeja, #1210 - 2 madejas, #1212 - 2 madejas</p>
               </div>
               <div class="text-center py-2">
                  <a href="/174-pacha" class="btn btn-secondary btn-modal btn-modal float-none">Ver Productos</a>
               </div>
            </div>
            <!-- END INFO PATTERNS -->
                  <!-- SLIDER PHOTOS PATTERNS -->
                  <div class="col-md-7 patterns-photos">
                     <div class="modal-body">
                        <div id="carouselPacha" class="carousel slide" data-ride="carousel">
                           <ol class="carousel-indicators">
                              <li data-target="#carouselPacha" data-slide-to="0" class="active"></li>
                              <li data-target="#carouselPacha" data-slide-to="1"></li>
                              <li data-target="#carouselPacha" data-slide-to="2"></li>
                           </ol>
                           <div class="carousel-inner">
                              <div class="modal-close">
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true">&times;</span>
                                 </button>
                              </div>
                              <div class="carousel-item active">
                                 <img class="d-block w-100" src="/img/cms/patterns/14-4-Nadia-Pullover-Pacha.jpg" alt="Third slide">
                              </div>
                              <div class="carousel-item">
                                 <img class="d-block w-100" src="/img/cms/patterns/14-2-Nadia-Pullover-Pacha.jpg" alt="Second slide">
                              </div>
                              <div class="carousel-item">
                                 <img class="d-block w-100" src="/img/cms/patterns/14-3-Nadia-Pullover-Pacha.jpg" alt="Third slide">
                              </div>
                              <div class="carousel-item">
                                 <img class="d-block w-100" src="/img/cms/patterns/14-1-Nadia-Pullover-Pacha.jpg" alt="First slide">
                              </div>
                           </div>
                           <a class="carousel-control-prev" href="#carouselPacha" role="button" data-slide="prev">
                           <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                           <span class="sr-only">Previous</span>
                           </a>
                           <a class="carousel-control-next" href="#carouselPacha" role="button" data-slide="next">
                           <span class="carousel-control-next-icon" aria-hidden="true"></span>
                           <span class="sr-only">Next</span>
                           </a>
                        </div>
                     </div>
                  </div>
                  <!-- END SLIDER PHOTOS PATTERNS -->
               </div>
            </div>
         </div>
      </div>
      <!-- END PACHA  -->

        <!--  MODAL WARMI --->
      <div class="modal fade" id="patterns15" tabindex="-1" role="dialog" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="row align-items-center">
                  <!-- INFO PATTERNS -->
                  <div class="col-md-5 patterns-text">
                     <div class="info-patterns text-center">
                        <img class="w-50" src="/img/cms/patterns/05-warmi-icon.jpg">
                        <h3>Bufanda Daniela</h3>
                        <p class="title-info-patherns"><strong>Diseñadora</strong></p>
                        <p class="title-info-patherns">Natalya Berenzynska</p>
                        <h4>Composición</h4>
                        <p>70% Baby Alpaca, 30% Oveja Merino</p>
                        <p><strong>Tamaño agujas:</strong> Agujas circulares - 8 (5mm) </p>
                        <p><strong>Madejas:</strong> 4 madejas de 100 gr </p>
                        <p><strong>Colores:</strong> #6010 - 2 madejas, #6015 - 2 madejas</p>
                     </div>
                     <div class="text-center py-2">
                        <a href="/37-warmi" class="btn btn-secondary btn-modal btn-modal float-none">Ver Productos</a>
                     </div>
                  </div>
                  <!-- END INFO PATTERNS -->
                  <!-- SLIDER PHOTOS PATTERNS -->
                  <div class="col-md-7 patterns-photos">
                     <div class="modal-body">
                        <div id="carouselwarmi1" class="carousel slide" data-ride="carousel">
                           <ol class="carousel-indicators">
                              <li data-target="#carouselwarmi1" data-slide-to="0" class="active"></li>
                              <li data-target="#carouselwarmi1" data-slide-to="1"></li>
                              <li data-target="#carouselwarmi1" data-slide-to="2"></li>
                           </ol>
                           <div class="carousel-inner">
                              <div class="modal-close">
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true">&times;</span>
                                 </button>
                              </div>
                              <div class="carousel-item active">
                                 <img class="d-block w-100" src="/img/cms/patterns/15-1-amano-dia-Warmi.jpg" alt="Third slide">
                              </div>
                              <div class="carousel-item">
                                 <img class="d-block w-100" src="/img/cms/patterns/15-2-amano-dia-Warmi.jpg" alt="Second slide">
                              </div>
                              <div class="carousel-item">
                                 <img class="d-block w-100" src="/img/cms/patterns/15-3-amano-dia-Warmi.jpg" alt="First slide">
                              </div>
                           </div>
                           <a class="carousel-control-prev" href="#carouselwarmi1" role="button" data-slide="prev">
                           <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                           <span class="sr-only">Previous</span>
                           </a>
                           <a class="carousel-control-next" href="#carouselwarmi1" role="button" data-slide="next">
                           <span class="carousel-control-next-icon" aria-hidden="true"></span>
                           <span class="sr-only">Next</span>
                           </a>
                        </div>
                     </div>
                  </div>
                  <!-- END SLIDER PHOTOS PATTERNS -->
               </div>
            </div>
         </div>
      </div>
      <!-- END WARMI  -->
      <!-- MODAL PACHA-->
      <div class="modal fade" id="patterns16" tabindex="-1" role="dialog" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="row align-items-center">
                  <!-- INFO PATTERNS -->
                  <div class="col-md-5 patterns-text">
                     <div class="info-patterns text-center">
                        <img class="w-50" src="/img/cms/patterns/015-pacha-icon.jpg">
                        <h3>Estola Indira</h3>
                        <p class="title-info-patherns"><strong>Diseñadora</strong></p>
                        <p class="title-info-patherns">Mary Lou Egan</p>
                        <h4>Composición</h4>
                        <p>50 % Seda, 25 % Super Kid Mohair, 25 % Oveja Merino</p>
                        <p><strong>Tamaño agujas:</strong> 5 (3.75 mm) </p>
                        <p><strong>Madejas:</strong>  5 madejas de 50 gr</p>
                        <p><strong>Colores:</strong>  #1211 - 3 madejas, #1212 - 2 madejas </p>
                     </div>
                     <div class="text-center py-2">
                        <a href="/174-pacha" class="btn btn-secondary btn-modal btn-modal float-none">Ver Productos</a>
                     </div>
                  </div>
                  <!-- END INFO PATTERNS -->
                  <!-- SLIDER PHOTOS PATTERNS -->
                  <div class="col-md-7 patterns-photos">
                     <div class="modal-body">
                        <div id="carouselPacha1" class="carousel slide" data-ride="carousel">
                           <ol class="carousel-indicators">
                              <li data-target="#carouselPacha1" data-slide-to="0" class="active"></li>
                              <li data-target="#carouselPacha1" data-slide-to="1"></li>
                              <li data-target="#carouselPacha1" data-slide-to="2"></li>
                           </ol>
                           <div class="carousel-inner">
                              <div class="modal-close">
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true">&times;</span>
                                 </button>
                              </div>
                              <div class="carousel-item active">
                                 <img class="d-block w-100" src="/img/cms/patterns/16-1-amano-dia-Pacha.jpg" alt="Third slide">
                              </div>
                              <div class="carousel-item">
                                 <img class="d-block w-100" src="/img/cms/patterns/16-2-amano-dia-Pacha.jpg" alt="Second slide">
                              </div>
                              <div class="carousel-item">
                                 <img class="d-block w-100" src="/img/cms/patterns/16-3-amano-dia-Pacha.jpg" alt="Third slide">
                              </div>
                           </div>
                           <a class="carousel-control-prev" href="#carouselPacha1" role="button" data-slide="prev">
                           <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                           <span class="sr-only">Previous</span>
                           </a>
                           <a class="carousel-control-next" href="#carouselPacha1" role="button" data-slide="next">
                           <span class="carousel-control-next-icon" aria-hidden="true"></span>
                           <span class="sr-only">Next</span>
                           </a>
                        </div>
                     </div>
                  </div>
                  <!-- END SLIDER PHOTOS PATTERNS -->
               </div>
            </div>
         </div>
      </div>
      <!-- END PACHA  -->        </div>
                </div>
        				</div>
			</div>
		</div>
		                </div>
            </div>
        </div>
        									</div>
			</div>
		</div>
		

<?php }
}
