<?php
/* Smarty version 3.1.33, created on 2021-03-19 16:43:06
  from 'module:iqitlinksmanagerviewstemp' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_60551aea019cd6_91360049',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ef3dcd2ceee3dd6a458a9c29f5ad0be7ff371cd7' => 
    array (
      0 => 'module:iqitlinksmanagerviewstemp',
      1 => 1611278170,
      2 => 'module',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_60551aea019cd6_91360049 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
));
?>            <div class="col col-md block block-toggle block-iqitlinksmanager block-iqitlinksmanager-4 block-links js-block-toggle">
            <h5 class="block-title"><span>Información</span></h5>
            <div class="block-content">
                <ul>
                                                                        <li>
                                <a
                                        href="https://amanoyarns.com/pe/content/4-quienes-somos"
                                        title="Amano es lujo en su expresión más pura. Encuentra nuestra inspiración en cada hebra con Amano Yarns."                                >
                                    Quiénes somos
                                </a>
                            </li>
                                                                                                <li>
                                <a
                                        href="https://amanoyarns.com/pe/content/1-contacto"
                                        title="Contacto de Amano yarns Telf: +57 947717500 - Correo: ventasonline@amanoyarns.com"                                >
                                    Contacto
                                </a>
                            </li>
                                                                                                <li>
                                <a
                                        href="/pe/module/kbstorelocatorpickup/stores"
                                                                        >
                                    Buscar Tienda
                                </a>
                            </li>
                                                                                                <li>
                                <a
                                        href="/pe/blog"
                                                                        >
                                    Blog
                                </a>
                            </li>
                                                                                                <li>
                                <a
                                        href="https://amanoyarns.com/pe/content/6-preguntas-frecuentes"
                                        title="Estamos más que felices de responder cualquier pregunta que pueda tener"                                >
                                    Preguntas Frecuentes
                                </a>
                            </li>
                                                                                                <li>
                                <a
                                        href="https://amanoyarns.com/pe/content/5-pago-seguro"
                                        title="Nuestra forma de pago segura"                                >
                                    Pago seguro
                                </a>
                            </li>
                                                            </ul>
            </div>
        </div>
                <div class="col col-md block block-toggle block-iqitlinksmanager block-iqitlinksmanager-5 block-links js-block-toggle">
            <h5 class="block-title"><span>Servicio al Cliente</span></h5>
            <div class="block-content">
                <ul>
                                                                        <li>
                                <a
                                        href="https://amanoyarns.com/pe/content/9-envios"
                                        title=""                                >
                                    Envíos
                                </a>
                            </li>
                                                                                                <li>
                                <a
                                        href="https://amanoyarns.com/pe/content/10-devoluciones"
                                        title=""                                >
                                    Devoluciones
                                </a>
                            </li>
                                                                                                <li>
                                <a
                                        href="https://amanoyarns.com/pe/content/11-politica-de-privacidad-y-cookies"
                                        title=""                                >
                                    Política de Privacidad y Cookies
                                </a>
                            </li>
                                                                                                <li>
                                <a
                                        href="https://amanoyarns.com/pe/content/22-lista-de-proveedores"
                                        title=""                                >
                                    Lista de Proveedores
                                </a>
                            </li>
                                                                                                <li>
                                <a
                                        href="https://amanoyarns.com/pe/content/19-libro-de-reclamaciones"
                                        title=""                                >
                                    Libro de Reclamaciones
                                </a>
                            </li>
                                                            </ul>
            </div>
        </div>
    <?php }
}
