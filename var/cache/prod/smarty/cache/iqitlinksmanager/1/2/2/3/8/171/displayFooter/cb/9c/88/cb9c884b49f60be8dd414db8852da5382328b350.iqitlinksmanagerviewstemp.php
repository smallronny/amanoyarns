<?php
/* Smarty version 3.1.33, created on 2021-01-18 20:05:54
  from 'module:iqitlinksmanagerviewstemp' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_600630724e2338_92248155',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ef3dcd2ceee3dd6a458a9c29f5ad0be7ff371cd7' => 
    array (
      0 => 'module:iqitlinksmanagerviewstemp',
      1 => 1597521699,
      2 => 'module',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_600630724e2338_92248155 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
));
?>            <div class="col col-md block block-toggle block-iqitlinksmanager block-iqitlinksmanager-1 block-links js-block-toggle">
            <h5 class="block-title"><span>Servicio al Cliente</span></h5>
            <div class="block-content">
                <ul>
                                                                        <li>
                                <a
                                        href="https://amanoyarns.store/es/content/9-envios"
                                        title=""                                >
                                    Envíos
                                </a>
                            </li>
                                                                                                <li>
                                <a
                                        href="https://amanoyarns.store/es/content/10-devoluciones"
                                        title=""                                >
                                    Devoluciones
                                </a>
                            </li>
                                                                                                <li>
                                <a
                                        href="https://amanoyarns.store/es/content/11-politica-de-privacidad-y-cookies"
                                        title=""                                >
                                    Política de Privacidad y Cookies
                                </a>
                            </li>
                                                                                                <li>
                                <a
                                        href="https://amanoyarns.store/es/content/19-libro-de-reclamaciones"
                                        title=""                                >
                                    Libro de Reclamaciones
                                </a>
                            </li>
                                                            </ul>
            </div>
        </div>
                <div class="col col-md block block-toggle block-iqitlinksmanager block-iqitlinksmanager-2 block-links js-block-toggle">
            <h5 class="block-title"><span>Información</span></h5>
            <div class="block-content">
                <ul>
                                                                        <li>
                                <a
                                        href="/module/kbstorelocatorpickup/stores"
                                                                        >
                                    Buscar Tienda
                                </a>
                            </li>
                                                                                                <li>
                                <a
                                        href="/es/blog"
                                                                        >
                                    Blog
                                </a>
                            </li>
                                                                                                <li>
                                <a
                                        href="https://amanoyarns.store/es/content/6-preguntas-frecuentes"
                                        title="Estamos más que felices de responder cualquier pregunta que pueda tener"                                >
                                    Preguntas Frecuentes
                                </a>
                            </li>
                                                                                                <li>
                                <a
                                        href="https://amanoyarns.store/es/content/5-pago-seguro"
                                        title="Nuestra forma de pago segura"                                >
                                    Pago seguro
                                </a>
                            </li>
                                                            </ul>
            </div>
        </div>
    <?php }
}
