<?php
/* Smarty version 3.1.33, created on 2021-03-20 11:37:35
  from 'module:iqithtmlandbannersviewste' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_605616bf9ce958_80222001',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a1f82456bbbd860eb2f9337cfcd5bfd7ddc900f7' => 
    array (
      0 => 'module:iqithtmlandbannersviewste',
      1 => 1611278170,
      2 => 'module',
    ),
    'b74ca73a91234e272faf0555bce247fa03c55481' => 
    array (
      0 => 'module:iqithtmlandbannersviewste',
      1 => 1611278170,
      2 => 'module',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_605616bf9ce958_80222001 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
));
?>

      
    <div id="iqithtmlandbanners-block-1"  class="col col-md block block-toggle block-iqithtmlandbanners-html js-block-toggle">
        <h5 class="block-title"><span>Footer</span></h5>
        <div class="block-content rte-content">
            <div class="col col-md">
<div class="footer-info text-center"><img class="logo img-responsive" src="/img/logo-amano.png" alt="Amano" />
<p class="pt-3">Amano Phone</p>
<p><a href="tel:+51947717400" target="_blank">(+51)947717400</a></p>
<p><a href="mailto:info@amanoyarns.com" target="_blank">info@amanoyarns.com</a></p>
<div class="redes"><a href="https://www.facebook.com/AmanoYarns/?fref=ts" target="_blank"> <img loading="lazy" class="m-facebook" src="/img/cms/iconos/facebook.png" alt="Facebook" /> </a> <a href="https://www.youtube.com/channel/UC7N76h1SKsJYJJiijfrmjrg" target="_blank"> <img loading="lazy" class="px-4 m-youtube" src="/img/cms/iconos/youtube.png" alt="Youtube" /> </a> <a href="https://www.instagram.com/amanoyarns/" target="_blank"> <img loading="lazy" class="m-instagram" src="/img/cms/iconos/instagram.png" alt="Instagram" /> </a></div>
<div class="py-3"><img loading="lazy" style="width: auto;" src="/img/cms/Line_footer.png" alt="Line" /></div>
<p>© 2020 Amano yarns, Todos los derechos reservados. By <a href="https://webtilia.com/">Webtilia</a></p>
</div>
</div>
        </div>
    </div>


  
<?php }
}
