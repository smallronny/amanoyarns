<?php
/* Smarty version 3.1.33, created on 2021-01-18 20:08:11
  from 'module:iqitcookielawviewstemplat' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_600630fbacff81_65051657',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b7588a77287e3c02dfc5dfe6fd3a17abf03f7e2e' => 
    array (
      0 => 'module:iqitcookielawviewstemplat',
      1 => 1598462746,
      2 => 'module',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_600630fbacff81_65051657 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
));
?>




<div id="iqitcookielaw" class="p-3">

    <div class="row justify-content-center p-4"> 

             <div class="col-md-8 col-12 align-self-center">

<p>The cookies on this website are used by AMANO YARNS and third parties for different purposes, such as to personalize the content, to adapt the advertising to your interests and to measure the use of the website. To manage or disable these cookies, click on "Cookie Settings" or for more information, visit our Privacy and Cookies Policy.</p>

 </div>

          <div class="col-md-2 col-6 align-self-center text-center">



<button class="btn btn-block btn-cok" id="iqitcookielaw-accept">Accept</button>

  </div>

          <div class="col-md-2 col-6 align-self-center text-center">

<a href="/content/11-privacy-policy-and-cookies" id="iqitcookielaw-read" class="btn-cook btn btn-block btn-primary">Read More</a>

  </div>

           </div>

</div>





 

<?php }
}
