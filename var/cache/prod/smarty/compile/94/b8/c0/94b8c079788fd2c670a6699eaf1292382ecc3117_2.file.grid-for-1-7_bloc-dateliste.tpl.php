<?php
/* Smarty version 3.1.33, created on 2021-03-19 17:02:19
  from '/home2/amanoyarns/public_html/multitienda/modules/prestablog/views/templates/hook/grid-for-1-7_bloc-dateliste.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6055115b633f15_93134505',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '94b8c079788fd2c670a6699eaf1292382ecc3117' => 
    array (
      0 => '/home2/amanoyarns/public_html/multitienda/modules/prestablog/views/templates/hook/grid-for-1-7_bloc-dateliste.tpl',
      1 => 1611278170,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6055115b633f15_93134505 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- Module Presta Blog -->
<div class="block-categories">
	<h4 class="title_block"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Blog archives','mod'=>'prestablog'),$_smarty_tpl ) );?>
</h4>
	<div class="block_content" id="prestablog_dateliste">
		<?php if ($_smarty_tpl->tpl_vars['ResultDateListe']->value) {?>
			<ul>
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['ResultDateListe']->value, 'ValueAnnee', false, 'KeyAnnee', 'loopAnnee', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['KeyAnnee']->value => $_smarty_tpl->tpl_vars['ValueAnnee']->value) {
?>
				<li>
					<a href="#" class="prestablog_annee link_block" <?php if (count($_smarty_tpl->tpl_vars['ResultDateListe']->value) <= 1) {?>style="display:none;"<?php }?>><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['KeyAnnee']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
&nbsp;<span>(<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['ValueAnnee']->value['nombre_news']), ENT_QUOTES, 'UTF-8');?>
)</span></a>
					<ul class="prestablog_mois <?php if ((isset($_smarty_tpl->tpl_vars['prestablog_annee']->value) && $_smarty_tpl->tpl_vars['prestablog_annee']->value == $_smarty_tpl->tpl_vars['KeyAnnee']->value)) {?>prestablog_show<?php }?>">
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['ValueAnnee']->value['mois'], 'ValueMois', false, 'KeyMois', 'loopMois', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['KeyMois']->value => $_smarty_tpl->tpl_vars['ValueMois']->value) {
?>
						<li>
							<a href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogUrl'][0], array( array('y'=>$_smarty_tpl->tpl_vars['KeyAnnee']->value,'m'=>$_smarty_tpl->tpl_vars['KeyMois']->value),$_smarty_tpl ) );?>
" class="link_block"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['ValueMois']->value['mois_value'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
&nbsp;<span>(<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['ValueMois']->value['nombre_news']), ENT_QUOTES, 'UTF-8');?>
)</span></a>
						</li>
				<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
					</ul>
				</li>
			<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
			</ul>
		<?php } else { ?>
			<p><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'No news','mod'=>'prestablog'),$_smarty_tpl ) );?>
</p>
		<?php }?>
		<?php if ($_smarty_tpl->tpl_vars['prestablog_config']->value['prestablog_datenews_showall']) {?><a href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogUrl'][0], array( array(),$_smarty_tpl ) );?>
" class="btn-primary btn_link"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'See all','mod'=>'prestablog'),$_smarty_tpl ) );?>
</a><?php }?>
	</div>
</div>
<!-- /Module Presta Blog -->
<?php }
}
