<?php
/* Smarty version 3.1.33, created on 2021-03-19 16:03:49
  from '/home2/amanoyarns/public_html/multitienda/themes/warehouse/modules/ps_emailalerts/views/templates/hook/product.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_605511b539e7f3_24580484',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b659c3166c3f48abacb5b754b175f54a829449ea' => 
    array (
      0 => '/home2/amanoyarns/public_html/multitienda/themes/warehouse/modules/ps_emailalerts/views/templates/hook/product.tpl',
      1 => 1611278172,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605511b539e7f3_24580484 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="js-mailalert form-inline"
     data-url="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0], array( array('entity'=>'module','name'=>'ps_emailalerts','controller'=>'actions','params'=>array('process'=>'add')),$_smarty_tpl ) );?>
">
    <div class="input-group mr-2 mb-2">
        <?php if (isset($_smarty_tpl->tpl_vars['email']->value) && $_smarty_tpl->tpl_vars['email']->value) {?>
            <input type="email" placeholder="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'your@email.com','d'=>'Modules.Mailalerts.Shop'),$_smarty_tpl ) );?>
" class="form-control"/>
            <br/>
        <?php }?>
    </div>
    <div class="input-group mr-2 mb-2">
        <input type="hidden" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id_product']->value, ENT_QUOTES, 'UTF-8');?>
"/>
        <input type="hidden" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id_product_attribute']->value, ENT_QUOTES, 'UTF-8');?>
"/>
        <a href="#" class="btn btn-secondary" rel="nofollow"
           onclick="return addNotification();"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Notify me when available','d'=>'Modules.Mailalerts.Shop'),$_smarty_tpl ) );?>
</a>

    </div>
    <span class="alert alert-info" style="display:none;"></span>
</div>
<?php }
}
