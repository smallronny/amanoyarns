<?php
/* Smarty version 3.1.33, created on 2021-03-19 22:00:14
  from 'module:visanetperuviewstemplates' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6055653e5404f7_45746014',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '330f9bf936bb10607d4e20916b87e460eebd9fc2' => 
    array (
      0 => 'module:visanetperuviewstemplates',
      1 => 1611905120,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6055653e5404f7_45746014 (Smarty_Internal_Template $_smarty_tpl) {
?><section>
    <p><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Aceptamos todas las tarjetas asociadas a Visa, detalles de pago:'),$_smarty_tpl ) );?>

    <dl>
        <dt><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Importe'),$_smarty_tpl ) );?>
</dt>
        <dd><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['checkTotal']->value, ENT_QUOTES, 'UTF-8');?>
</dd>
    </dl>
    </p>
</section><?php }
}
