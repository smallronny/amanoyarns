<?php
/* Smarty version 3.1.33, created on 2021-01-18 20:31:01
  from '/home2/amanoyarns/public_html/store/modules/prestablog/views/templates/hook/grid-for-1-7_bloc-search.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_600636554e3731_75042116',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '33de22b8ff3b7fe3915e21c99944e82e5638d01c' => 
    array (
      0 => '/home2/amanoyarns/public_html/store/modules/prestablog/views/templates/hook/grid-for-1-7_bloc-search.tpl',
      1 => 1599067362,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_600636554e3731_75042116 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- Module Presta Blog -->
<div class="block-categories">
	<h4 class="title_block"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Search on blog','mod'=>'prestablog'),$_smarty_tpl ) );?>
</h4>
	<div class="block_content">
		<form action="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogUrl'][0], array( array(),$_smarty_tpl ) );?>
" method="post" id="prestablog_bloc_search">
			<input id="prestablog_search" class="search_query form-control ac_input" type="text" value="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['prestablog_search_query']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" placeholder="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Search on blog','mod'=>'prestablog'),$_smarty_tpl ) );?>
" name="prestablog_search" autocomplete="off">
			<button class="btn btn-default button-search" type="submit">
				<span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Search on blog','mod'=>'prestablog'),$_smarty_tpl ) );?>
</span>
			</button>
			<div class="clear"></div>
		</form>
	</div>
</div>
<!-- /Module Presta Blog -->
<?php }
}
