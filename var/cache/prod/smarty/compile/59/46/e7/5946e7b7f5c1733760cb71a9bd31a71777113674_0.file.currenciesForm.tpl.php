<?php
/* Smarty version 3.1.33, created on 2021-03-19 18:36:35
  from '/home2/amanoyarns/public_html/multitienda/modules/progeo/views/currenciesForm.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_60552773448969_05712857',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5946e7b7f5c1733760cb71a9bd31a71777113674' => 
    array (
      0 => '/home2/amanoyarns/public_html/multitienda/modules/progeo/views/currenciesForm.tpl',
      1 => 1611278170,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60552773448969_05712857 (Smarty_Internal_Template $_smarty_tpl) {
?><table class="table table-bordered table-striped">
    <tr>
        <th><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Currency','mod'=>'progeo'),$_smarty_tpl ) );?>
</th>
        <th><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Enable','mod'=>'progeo'),$_smarty_tpl ) );?>
</th>
        <th><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Default currency','mod'=>'progeo'),$_smarty_tpl ) );?>
</th>
    </tr>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['currencies']->value, 'currency');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['currency']->value) {
?>
        <tr>
            <td width="300"><?php echo $_smarty_tpl->tpl_vars['currency']->value['iso_code'];?>
 (<?php echo $_smarty_tpl->tpl_vars['currency']->value['sign'];?>
)</td>
            <td><input type="checkbox" class="selected_currency" id="selected_currency_<?php echo $_smarty_tpl->tpl_vars['currency']->value['id_currency'];?>
"
                       name="currencies[]" value="<?php echo $_smarty_tpl->tpl_vars['currency']->value['id_currency'];?>
"
                       <?php if (isset($_smarty_tpl->tpl_vars['selected_currencies']->value->currencies)) {
if (in_array($_smarty_tpl->tpl_vars['currency']->value['id_currency'],$_smarty_tpl->tpl_vars['selected_currencies']->value->currencies)) {?>checked<?php }
}?>/>
            </td>
            <td><input type="radio" class="default_currency" id="default_currency_<?php echo $_smarty_tpl->tpl_vars['currency']->value['id_currency'];?>
" name="id_currency"
                       value="<?php echo $_smarty_tpl->tpl_vars['currency']->value['id_currency'];?>
"
                       <?php if (isset($_smarty_tpl->tpl_vars['selected_currencies']->value->id_currency)) {?>
                       <?php if ($_smarty_tpl->tpl_vars['selected_currencies']->value->id_currency == $_smarty_tpl->tpl_vars['currency']->value['id_currency']) {?>checked<?php }?>/></td>
                       <?php }?>
        </tr>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
</table>

<?php echo '<script'; ?>
>
    function reloadCurrencyForm() {
        checked_default = false;
        last_checked = false;
        $('input.selected_currency:checked').each(function (i, d) {
            last_checked = d.value;
            if ($('#default_currency_' + d.value).is(':checked')) {
                checked_default = true
            }
        });
        if (checked_default == false && last_checked != false) {
            $('#default_currency_' + last_checked).click();
        }
    }

    $(document).ready(function () {
        $('.default_currency').change(function(){
            if ($('#selected_currency_'+$('.default_currency:checked').val()).is(':checked')){

            } else {
                $('#selected_currency_'+$('.default_currency:checked').val()).click();
            }
        });
        $('.selected_currency').change(function(){
            reloadCurrencyForm();
        });
        reloadCurrencyForm();
    });
<?php echo '</script'; ?>
><?php }
}
