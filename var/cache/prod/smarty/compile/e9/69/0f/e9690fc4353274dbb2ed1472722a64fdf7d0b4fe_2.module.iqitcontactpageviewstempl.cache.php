<?php
/* Smarty version 3.1.33, created on 2021-03-20 20:35:04
  from 'module:iqitcontactpageviewstempl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_605694b88844a1_58750381',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e9690fc4353274dbb2ed1472722a64fdf7d0b4fe' => 
    array (
      0 => 'module:iqitcontactpageviewstempl',
      1 => 1611278170,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605694b88844a1_58750381 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->compiled->nocache_hash = '680802330605694b887fb99_06819952';
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1099392105605694b8880381_11834438', 'iqitcontactpage-map');
}
/* {block 'iqitcontactpage-map'} */
class Block_1099392105605694b8880381_11834438 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'iqitcontactpage-map' => 
  array (
    0 => 'Block_1099392105605694b8880381_11834438',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<?php if ($_smarty_tpl->tpl_vars['show_map']->value) {?>
    <div id="iqitcontactpage-map">
        <iframe frameborder="0" marginheight="0" style="border:0" allowfullscreen marginwidth="0" src="https://maps.google.com/maps?q=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['point']->value['latitude'], ENT_QUOTES, 'UTF-8');?>
,<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['point']->value['longitude'], ENT_QUOTES, 'UTF-8');?>
&t=m&z=10&output=embed&iwloc=near"></iframe>
    </div>
<?php }
}
}
/* {/block 'iqitcontactpage-map'} */
}
