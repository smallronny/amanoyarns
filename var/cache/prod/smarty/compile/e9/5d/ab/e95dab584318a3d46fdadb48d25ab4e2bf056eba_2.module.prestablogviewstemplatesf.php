<?php
/* Smarty version 3.1.33, created on 2021-03-19 16:04:14
  from 'module:prestablogviewstemplatesf' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_605511ce9944b7_74007903',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e95dab584318a3d46fdadb48d25ab4e2bf056eba' => 
    array (
      0 => 'module:prestablogviewstemplatesf',
      1 => 1611278170,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605511ce9944b7_74007903 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- Module Presta Blog -->
<div id="prestablog-comments">
<h3><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Add a comment','mod'=>'prestablog'),$_smarty_tpl ) );?>
</h3>
<?php if (($_smarty_tpl->tpl_vars['prestablog_config']->value['prestablog_comment_only_login'] && $_smarty_tpl->tpl_vars['isLogged']->value) || !$_smarty_tpl->tpl_vars['prestablog_config']->value['prestablog_comment_only_login']) {?>
	<?php if (!$_smarty_tpl->tpl_vars['isSubmit']->value) {?>
		<form action="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['LinkReal']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
&id=<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['news']->value->id), ENT_QUOTES, 'UTF-8');?>
" method="post" class="std">
			<fieldset id="prestablog-comment">
				<?php if (sizeof($_smarty_tpl->tpl_vars['errors']->value)) {?>
				<p id="errors"><?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['errors']->value, 'Ierror', false, NULL, 'errors', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['Ierror']->value) {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['Ierror']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
<br /><?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?></p>
				<?php }?>
				<p class="text">
					<input type="text" class="text<?php if (sizeof($_smarty_tpl->tpl_vars['errors']->value) && array_key_exists('name',$_smarty_tpl->tpl_vars['errors']->value)) {?> errors<?php }?>" name="name" id="name" value="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['content_form']->value['name'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" placeholder="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Name','mod'=>'prestablog'),$_smarty_tpl ) );?>
:" />
				</p>
				<p class="textarea">
					<textarea name="comment" id="comment" cols="26" rows="2" <?php if (sizeof($_smarty_tpl->tpl_vars['errors']->value) && array_key_exists('comment',$_smarty_tpl->tpl_vars['errors']->value)) {?>class="errors"<?php }?> placeholder="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Comment','mod'=>'prestablog'),$_smarty_tpl ) );?>
:"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['content_form']->value['comment'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</textarea>
				</p>
				<?php if (isset($_smarty_tpl->tpl_vars['AntiSpam']->value)) {?>
					<p class="text">
						<label for="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['AntiSpam']->value['checksum'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Antispam protection','mod'=>'prestablog'),$_smarty_tpl ) );?>
 : <strong><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['AntiSpam']->value['question'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</strong></label>
						<input type="text" class="text<?php if (sizeof($_smarty_tpl->tpl_vars['errors']->value) && array_key_exists($_smarty_tpl->tpl_vars['AntiSpam']->value['checksum'],$_smarty_tpl->tpl_vars['errors']->value)) {?> errors<?php }?>" name="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['AntiSpam']->value['checksum'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" id="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['AntiSpam']->value['checksum'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" value="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['content_form']->value['antispam_checksum'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" />
					</p>
				<?php }?>
				 <?php if ($_smarty_tpl->tpl_vars['prestablog_config']->value['prestablog_captcha_actif'] == 1) {?>
				 <div class="g-recaptcha" data-sitekey="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prestablog_config']->value['prestablog_captcha_public_key'], ENT_QUOTES, 'UTF-8');?>
"></div>
				<?php }?>
				<p class="submit">
					<input type="submit" class="btn-primary" name="submitComment" id="submitComment" value="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Submit comment','mod'=>'prestablog'),$_smarty_tpl ) );?>
" />
				</p>
			</fieldset>
		</form>
	<?php } else { ?>
		<form id="submitOk" class="std">
			<fieldset>
				<h3><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Your comment has been successfully sent','mod'=>'prestablog'),$_smarty_tpl ) );?>
</h3>
				<?php if ($_smarty_tpl->tpl_vars['prestablog_config']->value['prestablog_comment_auto_actif']) {?>
				<p><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'This comment is automatically published.','mod'=>'prestablog'),$_smarty_tpl ) );?>
</p>
				<?php } else { ?>
				<p><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Before published, your comment must be approve by an administrator.','mod'=>'prestablog'),$_smarty_tpl ) );?>
</p>
				<?php }?>
			</fieldset>
		</form>
	<?php }
} else { ?>
	<form class="std">
		<fieldset id="prestablog-comment-register">
			<p style="text-align:center;">
				<a href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getPageLink('authentication',true),'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'You must be register','mod'=>'prestablog'),$_smarty_tpl ) );?>
<br /><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Clic here to registered','mod'=>'prestablog'),$_smarty_tpl ) );?>
</a>
			</p>
		</fieldset>
	</form>
<?php }
if (sizeof($_smarty_tpl->tpl_vars['comments']->value)) {?>

		<?php echo htmlspecialchars(intval(count($_smarty_tpl->tpl_vars['comments']->value)), ENT_QUOTES, 'UTF-8');?>
 <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'comments','mod'=>'prestablog'),$_smarty_tpl ) );?>

		<?php if ($_smarty_tpl->tpl_vars['prestablog_config']->value['prestablog_comment_subscription']) {?>
			<div id="abo">
			<?php if ($_smarty_tpl->tpl_vars['Is_Subscribe']->value) {?>
				<a href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['LinkReal']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
&d=<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['news']->value->id), ENT_QUOTES, 'UTF-8');?>
"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Stop my subscription to comments','mod'=>'prestablog'),$_smarty_tpl ) );?>
</a>
			<?php } else { ?>
				<a href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['LinkReal']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
&a=<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['news']->value->id), ENT_QUOTES, 'UTF-8');?>
"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Subscribe to comments','mod'=>'prestablog'),$_smarty_tpl ) );?>
</a>
			<?php }?>
			</div>
<?php }?>

<div id="comments">
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['comments']->value, 'comment', false, NULL, 'Comment', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['comment']->value) {
?>
	<div class="comment">
		<p><strong><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['comment']->value['name'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
 - <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['dateFormat'][0], array( array('date'=>$_smarty_tpl->tpl_vars['comment']->value['date'],'full'=>1),$_smarty_tpl ) );?>
</strong></p>
		<hr />
		<p><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['comment']->value['comment'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</p>
	</div>
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
</div>

<?php }?>
</div>
<!-- /Module Presta Blog -->
<?php }
}
