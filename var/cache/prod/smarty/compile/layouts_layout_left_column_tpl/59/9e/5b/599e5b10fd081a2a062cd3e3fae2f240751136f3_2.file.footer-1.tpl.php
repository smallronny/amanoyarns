<?php
/* Smarty version 3.1.33, created on 2021-03-19 16:03:46
  from '/home2/amanoyarns/public_html/multitienda/themes/warehousechild/templates/_partials/_variants/footer-1.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_605511b2465953_23767838',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '599e5b10fd081a2a062cd3e3fae2f240751136f3' => 
    array (
      0 => '/home2/amanoyarns/public_html/multitienda/themes/warehousechild/templates/_partials/_variants/footer-1.tpl',
      1 => 1615169520,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_elements/social-links.tpl' => 1,
    'file:_partials/_variants/footer-copyrights-1.tpl' => 1,
  ),
),false)) {
function content_605511b2465953_23767838 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>






<div id="footer-container-main" class="footer-container footer-style-1">

    <div class="container-fluid">

        <div class="row">

            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_446058667605511b2460a50_66614640', 'hook_footer_before');
?>


        </div>

        <div class="row">

            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2060550490605511b2461347_11461694', 'hook_footer');
?>


            <?php if ($_smarty_tpl->tpl_vars['iqitTheme']->value['f_newsletter_status'] == 1 || $_smarty_tpl->tpl_vars['iqitTheme']->value['f_social_status'] == 1) {?>

            <div class="col-12  col-md-auto">



                <?php if ($_smarty_tpl->tpl_vars['iqitTheme']->value['f_social_status'] == 1) {?>

                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1679552751605511b2462945_55726215', 'socials_footer');
?>


                <?php }?>

                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_459169783605511b2463994_47801024', 'newsletter_footer');
?>



            </div>

            <?php }?>

        </div>

        <div class="row">

            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_729775397605511b2464f34_96427863', 'hook_footer_after');
?>


        </div>

    </div>

</div>

<?php $_smarty_tpl->_subTemplateRender('file:_partials/_variants/footer-copyrights-1.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>



<?php }
/* {block 'hook_footer_before'} */
class Block_446058667605511b2460a50_66614640 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'hook_footer_before' => 
  array (
    0 => 'Block_446058667605511b2460a50_66614640',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayFooterBefore'),$_smarty_tpl ) );?>


            <?php
}
}
/* {/block 'hook_footer_before'} */
/* {block 'hook_footer'} */
class Block_2060550490605511b2461347_11461694 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'hook_footer' => 
  array (
    0 => 'Block_2060550490605511b2461347_11461694',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayFooter'),$_smarty_tpl ) );?>


            <?php
}
}
/* {/block 'hook_footer'} */
/* {block 'socials_footer'} */
class Block_1679552751605511b2462945_55726215 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'socials_footer' => 
  array (
    0 => 'Block_1679552751605511b2462945_55726215',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                <div class="block block-footer block-toggle block-social-links js-block-toggle">

                    <h5 class="block-title"><span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Follow us','d'=>'Shop.Warehousetheme'),$_smarty_tpl ) );?>
</span></h5>

                    <div class="block-content">

                        <?php $_smarty_tpl->_subTemplateRender('file:_elements/social-links.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('class'=>'_footer'), 0, false);
?>

                    </div>

                </div>

                <?php
}
}
/* {/block 'socials_footer'} */
/* {block 'newsletter_footer'} */
class Block_459169783605511b2463994_47801024 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'newsletter_footer' => 
  array (
    0 => 'Block_459169783605511b2463994_47801024',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                <?php if ($_smarty_tpl->tpl_vars['iqitTheme']->value['f_newsletter_status'] == 1) {?>
<div class="klaviyo-form-WLN6T8"></div>
                <div class="block block-footer block-toggle block-newsletter js-block-toggle t_footer">

                    <h3 class="text-white"><span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Suscribete','d'=>'Shop.Warehousetheme'),$_smarty_tpl ) );?>
</span></h3>
                    <p class="t_newsle  pt-3"> Donec neec justo eget felis facilisis fermentum aliquam portiton<p>

                    <div class="block-content pt-4">

                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['widget'][0], array( array('name'=>"ps_emailsubscription",'hook'=>'displayFooter'),$_smarty_tpl ) );?>


                    </div>

                </div>

                <?php }?>

<?php
}
}
/* {/block 'newsletter_footer'} */
/* {block 'hook_footer_after'} */
class Block_729775397605511b2464f34_96427863 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'hook_footer_after' => 
  array (
    0 => 'Block_729775397605511b2464f34_96427863',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayFooterAfter'),$_smarty_tpl ) );?>


            <?php
}
}
/* {/block 'hook_footer_after'} */
}
