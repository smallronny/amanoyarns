<?php
/* Smarty version 3.1.33, created on 2021-03-19 16:03:46
  from 'module:stfacetedsearchviewstempl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_605511b21703b7_83229191',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ff147c4473337796e3344a079bf5fade260db729' => 
    array (
      0 => 'module:stfacetedsearchviewstempl',
      1 => 1615220977,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
    'module:stfacetedsearch/views/templates/listing/center_column.tpl' => 1,
  ),
),false)) {
function content_605511b21703b7_83229191 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_917664693605511b216d495_59745883', 'product_list_top');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1948865473605511b216fdb7_66908541', 'product_list_active_filters');
$_smarty_tpl->inheritance->endChild($_smarty_tpl, 'catalog/listing/new-products.tpl');
}
/* {block 'product_list_top'} */
class Block_917664693605511b216d495_59745883 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'product_list_top' => 
  array (
    0 => 'Block_917664693605511b216d495_59745883',
  ),
);
public $prepend = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<?php $_smarty_tpl->_subTemplateRender('module:stfacetedsearch/views/templates/listing/center_column.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
/* {/block 'product_list_top'} */
/* {block 'product_list_active_filters'} */
class Block_1948865473605511b216fdb7_66908541 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'product_list_active_filters' => 
  array (
    0 => 'Block_1948865473605511b216fdb7_66908541',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'product_list_active_filters'} */
}
