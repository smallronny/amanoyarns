<?php
/* Smarty version 3.1.33, created on 2021-03-19 16:05:15
  from 'module:stfacetedsearchviewstempl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6055120b296247_58937230',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '941bc2aba7f723a453b838b7a36aa003bf626d42' => 
    array (
      0 => 'module:stfacetedsearchviewstempl',
      1 => 1615220977,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
    'module:stfacetedsearch/views/templates/listing/center_column.tpl' => 1,
  ),
),false)) {
function content_6055120b296247_58937230 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_17707175916055120b293582_10275451', 'product_list_top');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10686660436055120b295ca7_43786526', 'product_list_active_filters');
$_smarty_tpl->inheritance->endChild($_smarty_tpl, 'catalog/listing/prices-drop.tpl');
}
/* {block 'product_list_top'} */
class Block_17707175916055120b293582_10275451 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'product_list_top' => 
  array (
    0 => 'Block_17707175916055120b293582_10275451',
  ),
);
public $prepend = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<?php $_smarty_tpl->_subTemplateRender('module:stfacetedsearch/views/templates/listing/center_column.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
/* {/block 'product_list_top'} */
/* {block 'product_list_active_filters'} */
class Block_10686660436055120b295ca7_43786526 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'product_list_active_filters' => 
  array (
    0 => 'Block_10686660436055120b295ca7_43786526',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'product_list_active_filters'} */
}
