<?php
/* Smarty version 3.1.33, created on 2021-03-19 18:36:35
  from '/home2/amanoyarns/public_html/multitienda/modules/progeo/views/languagesForm.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_60552773451784_36024387',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '35fc76d16326cad10bd0cecc602b5b7112d1c253' => 
    array (
      0 => '/home2/amanoyarns/public_html/multitienda/modules/progeo/views/languagesForm.tpl',
      1 => 1611278170,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60552773451784_36024387 (Smarty_Internal_Template $_smarty_tpl) {
?><table class="table table-bordered table-striped">
    <tr>
        <th><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Language','mod'=>'progeo'),$_smarty_tpl ) );?>
</th>
        <th><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Enable','mod'=>'progeo'),$_smarty_tpl ) );?>
</th>
        <th><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Default language','mod'=>'progeo'),$_smarty_tpl ) );?>
</th>
    </tr>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['languages']->value, 'language');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['language']->value) {
?>
        <tr>
            <td width="300"><?php echo $_smarty_tpl->tpl_vars['language']->value['name'];?>
 (<?php echo $_smarty_tpl->tpl_vars['language']->value['iso_code'];?>
)</td>
            <td><input type="checkbox" class="selected_language" id="selected_language_<?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
"
                       name="languages[]" value="<?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
"
                       <?php if (isset($_smarty_tpl->tpl_vars['selected_languages']->value->languages)) {
if (in_array($_smarty_tpl->tpl_vars['language']->value['id_lang'],$_smarty_tpl->tpl_vars['selected_languages']->value->languages)) {?>checked<?php }
}?>/>
            </td>
            <td><input type="radio" class="default_language" id="default_language_<?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
" name="id_language"
                       value="<?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
"
                       <?php if (isset($_smarty_tpl->tpl_vars['selected_languages']->value->id_language)) {?>
                       <?php if ($_smarty_tpl->tpl_vars['selected_languages']->value->id_language == $_smarty_tpl->tpl_vars['language']->value['id_lang']) {?>checked<?php }?>
                       <?php }?>
                       /></td>
        </tr>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
</table>

<?php echo '<script'; ?>
>
    function reloadlanguageForm() {
        checked_default_lang = false;
        last_checked = false;
        $('input.selected_language:checked').each(function (i, d) {
            last_checked = d.value;
            if ($('#default_language_' + d.value).is(':checked')) {
                checked_default_lang = true
            }
        });
        if (checked_default_lang == false && last_checked != false) {
            $('#default_language_' + last_checked).click();
        }
    }

    $(document).ready(function () {
        $('.default_language').change(function () {
            if ($('#selected_language_' + $('.default_language:checked').val()).is(':checked')) {

            } else {
                $('#selected_language_' + $('.default_language:checked').val()).click();
            }
        });
        $('.selected_language').change(function () {
            reloadlanguageForm();
        });
        reloadlanguageForm();
    });
<?php echo '</script'; ?>
><?php }
}
