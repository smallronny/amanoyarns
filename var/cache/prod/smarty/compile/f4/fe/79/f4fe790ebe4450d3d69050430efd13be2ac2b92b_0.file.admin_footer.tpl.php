<?php
/* Smarty version 3.1.33, created on 2021-01-18 17:54:23
  from '/home2/amanoyarns/public_html/store/modules/ets_cfultimate/views/templates/hook/admin_footer.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6006119f7fdb49_20320080',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f4fe790ebe4450d3d69050430efd13be2ac2b92b' => 
    array (
      0 => '/home2/amanoyarns/public_html/store/modules/ets_cfultimate/views/templates/hook/admin_footer.tpl',
      1 => 1600833812,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6006119f7fdb49_20320080 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
 type="text/javascript">
    var link_ajax = '<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link_ajax']->value,'html','UTF-8' ));?>
';
    $(document).ready(function () {
        $.ajax({
            url: link_ajax,
            data: 'action=etsCfuGetCountMessageContactForm',
            type: 'post',
            dataType: 'json',
            success: function (json) {
                if (parseInt(json.count) > 0) {
                    if ($('#subtab-AdminContactFormUltimateMessage span').length)
                        $('#subtab-AdminContactFormUltimateMessage span').append('<span class="count_messages ">' + json.count + '</span>');
                    else
                        $('#subtab-AdminContactFormUltimateMessage a').append('<span class="count_messages ">' + json.count + '</span>');
                } else {
                    if ($('#subtab-AdminContactFormUltimateMessage span').length)
                        $('#subtab-AdminContactFormUltimateMessage span').append('<span class="count_messages hide">' + json.count + '</span>');
                    else
                        $('#subtab-AdminContactFormUltimateMessage a').append('<span class="count_messages hide">' + json.count + '</span>');
                }

            },
        });
    });
<?php echo '</script'; ?>
><?php }
}
