<?php
/* Smarty version 3.1.33, created on 2021-01-18 16:28:07
  from '/home2/amanoyarns/public_html/store/modules/ets_cfultimate/views/templates/hook/header.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6005fd67b30e24_54578919',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3e97d8a06dccc82fadace1203fcf13d263ce3006' => 
    array (
      0 => '/home2/amanoyarns/public_html/store/modules/ets_cfultimate/views/templates/hook/header.tpl',
      1 => 1600833812,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6005fd67b30e24_54578919 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
 type="text/javascript">
    var url_basic_ets = '<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['url_basic']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
';
    var link_contact_ets = '<?php echo $_smarty_tpl->tpl_vars['link_contact_ets']->value;?>
';
    var ets_cfu_recaptcha_enabled = <?php if (isset($_smarty_tpl->tpl_vars['rc_enabled']->value) && $_smarty_tpl->tpl_vars['rc_enabled']->value) {?>1<?php } else { ?>0<?php }?>;
    var iso_code = '<?php if (isset($_smarty_tpl->tpl_vars['iso_code']->value) && $_smarty_tpl->tpl_vars['iso_code']->value) {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['iso_code']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');
}?>';
    <?php if (isset($_smarty_tpl->tpl_vars['rc_enabled']->value) && $_smarty_tpl->tpl_vars['rc_enabled']->value) {?>
        var ets_cfu_recaptcha_v3 = <?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['rc_v3']->value), ENT_QUOTES, 'UTF-8');?>
;
        var ets_cfu_recaptcha_key = "<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['rc_key']->value,'html','utf-8' )), ENT_QUOTES, 'UTF-8');?>
";
    <?php }?>
    <?php if (isset($_smarty_tpl->tpl_vars['hidden_referrence']->value) && $_smarty_tpl->tpl_vars['hidden_referrence']->value) {?>
        var hidden_referrence = <?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['hidden_referrence']->value), ENT_QUOTES, 'UTF-8');?>
;
    <?php }
echo '</script'; ?>
>
<?php if (isset($_smarty_tpl->tpl_vars['rc_enabled']->value) && $_smarty_tpl->tpl_vars['rc_enabled']->value) {?>
    <?php echo '<script'; ?>
 src="https://www.google.com/recaptcha/api.js?hl=<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['iso_code']->value,'html','utf-8' )), ENT_QUOTES, 'UTF-8');
if ($_smarty_tpl->tpl_vars['rc_v3']->value) {?>&render=<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['rc_key']->value,'html','utf-8' )), ENT_QUOTES, 'UTF-8');
}?>"><?php echo '</script'; ?>
>
<?php }
}
}
