<?php
/* Smarty version 3.1.33, created on 2021-01-18 16:28:08
  from '/home2/amanoyarns/public_html/store/modules/addifyadvanceqty/views/templates/hook/advanceqty.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6005fd68eeab99_97073907',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7d31f96ddd0a46cfa9ac6f4120161a0e97f31b13' => 
    array (
      0 => '/home2/amanoyarns/public_html/store/modules/addifyadvanceqty/views/templates/hook/advanceqty.tpl',
      1 => 1601166677,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6005fd68eeab99_97073907 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="addifyadvanceqty" id="addifyadvanceqty-<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['product_id']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"  changeqtybox="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['changeqtybox']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" qtytype="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['quantity_type']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" 
     currentproid="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['product_id']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" 
      >
<div class="addifyadvancereplacebox"style="display: none !important;" >
    <?php if (isset($_smarty_tpl->tpl_vars['quantity_type']->value) && $_smarty_tpl->tpl_vars['quantity_type']->value == 1) {?>
    <input type="number" name="qty" readonly="" id="quantity_wanted_addify" value="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['fixqty']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" class="input-group form-control" minlength="5" maxlength="5" aria-label="Quantity" style="display: block !important;">
    
   <?php } elseif (isset($_smarty_tpl->tpl_vars['quantity_type']->value) && $_smarty_tpl->tpl_vars['quantity_type']->value == 3) {?>
    <div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input  readonly="" type="number" name="qty" id="quantity_wanted_addify" value="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['min']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" class="input-group form-control" min="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['min']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" max="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['max']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" interval="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['interval']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" aria-label="Quantity" style="display: block !important;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"><button class="btn btn-touchspin addify_button_up bootstrap-touchspin-up" onclick="increase()" type="button"><i class="material-icons touchspin-up"></i></button><button class="btn btn-touchspin addify_button_down  bootstrap-touchspin-down" onclick="decrease()" type="button"><i class="material-icons  touchspin-down"></i></button></span></div>
    <?php } else { ?>
    <select type="number" name="qty" readonly="" class="form-control form-control-select" id="quantity_wanted_addify" style="display: block !important;">
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['dropdowns']->value, 'dropdown');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['dropdown']->value) {
?>
        <option value="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['dropdown']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['dropdown']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</option>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
    </select>
    <?php }?>
</div>
    <ul class="ps-alert-error  addifyadvanceqty_error_msg_max" style="display: none;"><li class="item">
        <i>
            <svg viewBox="0 0 24 24">
                <path fill="#fff" d="M11,15H13V17H11V15M11,7H13V13H11V7M12,2C6.47,2 2,6.5 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2M12,20A8,8 0 0,1 4,12A8,8 0 0,1 12,4A8,8 0 0,1 20,12A8,8 0 0,1 12,20Z"></path>
            </svg>
        </i>
        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'You reached Maximun quantity','mod'=>'addifyadvanceqty'),$_smarty_tpl ) );?>

    </li></ul>
     <ul class="ps-alert-error  addifyadvanceqty_error_msg_min" style="display: none;"><li class="item">
        <i>
            <svg viewBox="0 0 24 24">
                <path fill="#fff" d="M11,15H13V17H11V15M11,7H13V13H11V7M12,2C6.47,2 2,6.5 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2M12,20A8,8 0 0,1 4,12A8,8 0 0,1 12,4A8,8 0 0,1 20,12A8,8 0 0,1 12,20Z"></path>
            </svg>
        </i>
        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'You reached Minimum quantity','mod'=>'addifyadvanceqty'),$_smarty_tpl ) );?>

    </li></ul>
</div>


<?php }
}
