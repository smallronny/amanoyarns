<?php
/* Smarty version 3.1.33, created on 2021-03-20 20:44:32
  from 'module:iqitsocialloginviewstempl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_605696f0bf8d68_52383696',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2d9a48a422d63c62948f5a3ef74ef3df94dbf28e' => 
    array (
      0 => 'module:iqitsocialloginviewstempl',
      1 => 1611278170,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605696f0bf8d68_52383696 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_854276384605696f0bf2b41_06137459', 'page_content');
?>


<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, 'page.tpl');
}
/* {block 'page_content'} */
class Block_854276384605696f0bf2b41_06137459 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'page_content' => 
  array (
    0 => 'Block_854276384605696f0bf2b41_06137459',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php if (isset($_smarty_tpl->tpl_vars['message']->value)) {?>
    <h1 class="h1 page-title"><span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Problem with login','mod'=>'iqitsociallogin'),$_smarty_tpl ) );?>
</span></h1>
    <p class="alert alert-warning"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'There is some problem with login. Please use traditional login/registration','mod'=>'iqitsociallogin'),$_smarty_tpl ) );?>


    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value, ENT_QUOTES, 'UTF-8');?>

    </p>
    <?php } else { ?>
        <?php if ($_smarty_tpl->tpl_vars['popup']->value) {?>
            <?php echo '<script'; ?>
 type="text/javascript">
                
                    window.opener.location.reload();
                    self.close();
                
            <?php echo '</script'; ?>
>
        <?php }?>
    <?php }
}
}
/* {/block 'page_content'} */
}
