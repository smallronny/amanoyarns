<?php
/* Smarty version 3.1.33, created on 2021-01-19 12:56:12
  from '/home2/amanoyarns/public_html/store/themes/warehousechild/templates/customer/page.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_60071d3c6a4266_08653518',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '191f6682348b0a43671109a01b795015529b5c58' => 
    array (
      0 => '/home2/amanoyarns/public_html/store/themes/warehousechild/templates/customer/page.tpl',
      1 => 1609275031,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_partials/notifications.tpl' => 1,
    'file:customer/_partials/my-account-links.tpl' => 1,
  ),
),false)) {
function content_60071d3c6a4266_08653518 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_214334738260071d3c68fb46_61778173', 'page_header_container');
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_5283852960071d3c6905d4_28796007', 'notifications');
?>



<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_101196599360071d3c690b15_13258268', 'page_content_container');
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_110317597160071d3c6a3430_06154361', 'page_footer');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, 'page.tpl');
}
/* {block 'page_header_container'} */
class Block_214334738260071d3c68fb46_61778173 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'page_header_container' => 
  array (
    0 => 'Block_214334738260071d3c68fb46_61778173',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <header class="page-header">
      <h1 class="h1 page-title"><span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Your account','d'=>'Shop.Theme.Customeraccount'),$_smarty_tpl ) );?>
</span></h1>
    </header>
<?php
}
}
/* {/block 'page_header_container'} */
/* {block 'notifications'} */
class Block_5283852960071d3c6905d4_28796007 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'notifications' => 
  array (
    0 => 'Block_5283852960071d3c6905d4_28796007',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'notifications'} */
/* {block 'display_customer_account'} */
class Block_12332097160071d3c69c4a0_94791075 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayCustomerAccount'),$_smarty_tpl ) );?>

          <?php
}
}
/* {/block 'display_customer_account'} */
/* {block 'my_account_side_links'} */
class Block_149059353160071d3c690e73_02377390 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php if ($_smarty_tpl->tpl_vars['customer']->value['is_logged']) {?>
      <div class="my-account-side-links col-sm-3">
          <a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="identity-link" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['identity'], ENT_QUOTES, 'UTF-8');?>
">
        <span class="link-item">
          <i class="fa fa-user fa-fw" aria-hidden="true"></i>
          <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Information','d'=>'Shop.Theme.Customeraccount'),$_smarty_tpl ) );?>

        </span>
          </a>

          <?php if (count($_smarty_tpl->tpl_vars['customer']->value['addresses'])) {?>
            <a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="addresses-link" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['addresses'], ENT_QUOTES, 'UTF-8');?>
">
          <span class="link-item">
            <i class="fa fa-map-marker fa-fw" aria-hidden="true"></i>
            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Addresses','d'=>'Shop.Theme.Customeraccount'),$_smarty_tpl ) );?>

          </span>
            </a>
          <?php } else { ?>
            <a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="address-link" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['address'], ENT_QUOTES, 'UTF-8');?>
">
          <span class="link-item">
            <i class="fa fa-map-marker fa-fw" aria-hidden="true"></i>
            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Add first address','d'=>'Shop.Theme.Customeraccount'),$_smarty_tpl ) );?>

          </span>
            </a>
          <?php }?>

          <?php if (!$_smarty_tpl->tpl_vars['configuration']->value['is_catalog']) {?>
            <a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="history-link" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['history'], ENT_QUOTES, 'UTF-8');?>
">
          <span class="link-item">
            <i class="fa fa-history fa-fw" aria-hidden="true"></i>
            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Order history and details','d'=>'Shop.Theme.Customeraccount'),$_smarty_tpl ) );?>

          </span>
            </a>
          <?php }?>
 
          <?php if (!$_smarty_tpl->tpl_vars['configuration']->value['is_catalog']) {?>
            <a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="order-slips-link" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['order_slip'], ENT_QUOTES, 'UTF-8');?>
">
          <span class="link-item">
            <i class="fa fa-file-o fa-fw" aria-hidden="true"></i>
            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Credit slips','d'=>'Shop.Theme.Customeraccount'),$_smarty_tpl ) );?>

          </span>
            </a>
          <?php }?>
 
          <?php if ($_smarty_tpl->tpl_vars['configuration']->value['voucher_enabled'] && !$_smarty_tpl->tpl_vars['configuration']->value['is_catalog']) {?>
            <a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="discounts-link" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['discount'], ENT_QUOTES, 'UTF-8');?>
">
          <span class="link-item">
            <i class="fa fa-tags fa-fw" aria-hidden="true"></i>
            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Vouchers','d'=>'Shop.Theme.Customeraccount'),$_smarty_tpl ) );?>

          </span>
            </a>
          <?php }?>

          <?php if ($_smarty_tpl->tpl_vars['configuration']->value['return_enabled'] && !$_smarty_tpl->tpl_vars['configuration']->value['is_catalog']) {?>
            <a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="returns-link" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['order_follow'], ENT_QUOTES, 'UTF-8');?>
">
          <span class="link-item">
            <i class="fa fa-undo fa-fw"" aria-hidden="true"></i>
            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Merchandise returns','d'=>'Shop.Theme.Customeraccount'),$_smarty_tpl ) );?>

          </span>
            </a>
          <?php }?>

          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12332097160071d3c69c4a0_94791075', 'display_customer_account', $this->tplIndex);
?>


          <a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0], array( array('entity'=>'index','params'=>array('mylogout'=>'')),$_smarty_tpl ) );?>
">
          <span class="link-item">
            <i class="fa fa-sign-out fa-fw" aria-hidden="true"></i>
            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Sign out','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>

          </span>
          </a>
      </div>
      <?php }?>
    <?php
}
}
/* {/block 'my_account_side_links'} */
/* {block 'page_title'} */
class Block_18034586160071d3c69e827_23108026 extends Smarty_Internal_Block
{
public $callsChild = 'true';
public $hide = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <h2><?php 
$_smarty_tpl->inheritance->callChild($_smarty_tpl, $this);
?>
</h2>
      <?php
}
}
/* {/block 'page_title'} */
/* {block 'customer_notifications'} */
class Block_39051251060071d3c6a0387_07603755 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <?php $_smarty_tpl->_subTemplateRender('file:_partials/notifications.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php
}
}
/* {/block 'customer_notifications'} */
/* {block 'page_content_top'} */
class Block_92889935360071d3c6a0066_31124868 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_39051251060071d3c6a0387_07603755', 'customer_notifications', $this->tplIndex);
?>

      <?php
}
}
/* {/block 'page_content_top'} */
/* {block 'page_content'} */
class Block_82910167960071d3c6a2c56_11944428 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

       <!-- Page content -->
      <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_101196599360071d3c690b15_13258268 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'page_content_container' => 
  array (
    0 => 'Block_101196599360071d3c690b15_13258268',
  ),
  'my_account_side_links' => 
  array (
    0 => 'Block_149059353160071d3c690e73_02377390',
  ),
  'display_customer_account' => 
  array (
    0 => 'Block_12332097160071d3c69c4a0_94791075',
  ),
  'page_title' => 
  array (
    0 => 'Block_18034586160071d3c69e827_23108026',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_92889935360071d3c6a0066_31124868',
  ),
  'customer_notifications' => 
  array (
    0 => 'Block_39051251060071d3c6a0387_07603755',
  ),
  'page_content' => 
  array (
    0 => 'Block_82910167960071d3c6a2c56_11944428',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <section id="content" class="page-content my-account-page-content-wrapper">
    <div class="row">

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_149059353160071d3c690e73_02377390', 'my_account_side_links', $this->tplIndex);
?>



    <div class="my-account-page-content <?php if ($_smarty_tpl->tpl_vars['customer']->value['is_logged']) {?>col-sm-9<?php } else { ?>col<?php }?>">

      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18034586160071d3c69e827_23108026', 'page_title', $this->tplIndex);
?>


      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_92889935360071d3c6a0066_31124868', 'page_content_top', $this->tplIndex);
?>


      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_82910167960071d3c6a2c56_11944428', 'page_content', $this->tplIndex);
?>

    </div>

    </div>
  </section>

<?php
}
}
/* {/block 'page_content_container'} */
/* {block 'my_account_links'} */
class Block_159187999160071d3c6a3745_73204191 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php $_smarty_tpl->_subTemplateRender('file:customer/_partials/my-account-links.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
  <?php
}
}
/* {/block 'my_account_links'} */
/* {block 'page_footer'} */
class Block_110317597160071d3c6a3430_06154361 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'page_footer' => 
  array (
    0 => 'Block_110317597160071d3c6a3430_06154361',
  ),
  'my_account_links' => 
  array (
    0 => 'Block_159187999160071d3c6a3745_73204191',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_159187999160071d3c6a3745_73204191', 'my_account_links', $this->tplIndex);
?>

<?php
}
}
/* {/block 'page_footer'} */
}
