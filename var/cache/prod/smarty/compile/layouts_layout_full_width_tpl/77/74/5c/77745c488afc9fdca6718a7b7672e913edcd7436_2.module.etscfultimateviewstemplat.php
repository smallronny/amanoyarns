<?php
/* Smarty version 3.1.33, created on 2021-03-21 04:29:51
  from 'module:etscfultimateviewstemplat' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_605703ff3b9af6_65685402',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '77745c488afc9fdca6718a7b7672e913edcd7436' => 
    array (
      0 => 'module:etscfultimateviewstemplat',
      1 => 1611278170,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605703ff3b9af6_65685402 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<section id="content" class="page-content page-not-found">
    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1563912888605703ff3b70a9_53489520', 'page_content');
?>

</section><?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, 'page.tpl');
}
/* {block 'page_content'} */
class Block_1563912888605703ff3b70a9_53489520 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'page_content' => 
  array (
    0 => 'Block_1563912888605703ff3b70a9_53489520',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <h4><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Contact form is not available','mod'=>'ets_cfultimate'),$_smarty_tpl ) );?>
</h4>
    <?php
}
}
/* {/block 'page_content'} */
}
