<?php
/* Smarty version 3.1.33, created on 2021-01-18 16:28:08
  from '/home2/amanoyarns/public_html/store/themes/warehousechild/templates/catalog/product.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6005fd6881f596_42404493',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ba35d06c048d432a78e1561904a7b8d4efe7a1ad' => 
    array (
      0 => '/home2/amanoyarns/public_html/store/themes/warehousechild/templates/catalog/product.tpl',
      1 => 1603932969,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:catalog/_partials/product-cover-thumbnails.tpl' => 1,
    'file:catalog/_partials/product-prices.tpl' => 2,
    'file:catalog/_partials/product-customization.tpl' => 1,
    'file:catalog/_partials/product-variants.tpl' => 1,
    'file:catalog/_partials/miniatures/pack-product.tpl' => 1,
    'file:catalog/_partials/product-add-to-cart.tpl' => 1,
    'file:catalog/_partials/product-discounts.tpl' => 1,
    'file:catalog/_partials/product-additional-info.tpl' => 1,
    'file:catalog/_partials/_product_partials/product-tabs-h.tpl' => 1,
    'file:catalog/_partials/_product_partials/product-tabs-sections.tpl' => 1,
    'file:catalog/_partials/miniatures/product-small.tpl' => 1,
    'file:catalog/_partials/miniatures/product.tpl' => 1,
    'file:catalog/_partials/product-images-modal.tpl' => 1,
  ),
),false)) {
function content_6005fd6881f596_42404493 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>





<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_14843711976005fd687e9a05_49311720', 'head_seo');
?>






<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12905065376005fd687ea889_84894405', 'head_og_tags');
?>






<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_91921436005fd687ed262_91882432', 'head');
?>






<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10709317166005fd687f4074_08077063', 'content');
?>


<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['layout']->value);
}
/* {block 'head_seo'} */
class Block_14843711976005fd687e9a05_49311720 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'head_seo' => 
  array (
    0 => 'Block_14843711976005fd687e9a05_49311720',
  ),
);
public $prepend = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


    <link rel="canonical" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['canonical_url'], ENT_QUOTES, 'UTF-8');?>
">

<?php
}
}
/* {/block 'head_seo'} */
/* {block 'head_og_tags'} */
class Block_12905065376005fd687ea889_84894405 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'head_og_tags' => 
  array (
    0 => 'Block_12905065376005fd687ea889_84894405',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


    <meta property="og:type" content="product">

    <meta property="og:url" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['current_url'], ENT_QUOTES, 'UTF-8');?>
">

    <meta property="og:title" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['meta']['title'], ENT_QUOTES, 'UTF-8');?>
">

    <meta property="og:site_name" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['name'], ENT_QUOTES, 'UTF-8');?>
">

    <meta property="og:description" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['meta']['description'], ENT_QUOTES, 'UTF-8');?>
">

    <meta property="og:image" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['cover']['large']['url'], ENT_QUOTES, 'UTF-8');?>
">

    <meta property="og:image:width" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['cover']['large']['width'], ENT_QUOTES, 'UTF-8');?>
">

    <meta property="og:image:height" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['cover']['large']['height'], ENT_QUOTES, 'UTF-8');?>
">

<?php
}
}
/* {/block 'head_og_tags'} */
/* {block 'head'} */
class Block_91921436005fd687ed262_91882432 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'head' => 
  array (
    0 => 'Block_91921436005fd687ed262_91882432',
  ),
);
public $append = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


    <?php if ($_smarty_tpl->tpl_vars['product']->value['show_price']) {?>

        <meta property="product:pretax_price:amount" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['price_tax_exc'], ENT_QUOTES, 'UTF-8');?>
">

        <meta property="product:pretax_price:currency" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['currency']->value['iso_code'], ENT_QUOTES, 'UTF-8');?>
">

        <meta property="product:price:amount" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['price_amount'], ENT_QUOTES, 'UTF-8');?>
">

        <meta property="product:price:currency" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['currency']->value['iso_code'], ENT_QUOTES, 'UTF-8');?>
">

    <?php }?>

    <?php if (isset($_smarty_tpl->tpl_vars['product']->value['weight']) && ($_smarty_tpl->tpl_vars['product']->value['weight'] != 0)) {?>

        <meta property="product:weight:value" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['weight'], ENT_QUOTES, 'UTF-8');?>
">

        <meta property="product:weight:units" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['weight_unit'], ENT_QUOTES, 'UTF-8');?>
">

    <?php }?>



    <?php if ($_smarty_tpl->tpl_vars['iqitTheme']->value['bread_bg_category']) {?>

        <?php $_smarty_tpl->_assignInScope('categoryImage', "img/c/".((string)$_smarty_tpl->tpl_vars['product']->value['id_category_default'])."-category_default.jpg");?>

        <?php if (file_exists($_smarty_tpl->tpl_vars['categoryImage']->value)) {?>

            <style> #wrapper .breadcrumb{  background-image: url('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getCatImageLink($_smarty_tpl->tpl_vars['product']->value['category'],$_smarty_tpl->tpl_vars['product']->value['id_category_default'],'category_default'), ENT_QUOTES, 'UTF-8');?>
'); }</style>

        <?php }?>

    <?php }?>



<?php
}
}
/* {/block 'head'} */
/* {block 'product_cover_thumbnails'} */
class Block_21428337696005fd687f8025_67999224 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                                <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/product-cover-thumbnails.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                            <?php
}
}
/* {/block 'product_cover_thumbnails'} */
/* {block 'after_cover_thumbnails'} */
class Block_1280795376005fd687fa7d5_93746580 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                                <div class="after-cover-tumbnails text-center"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayAfterProductThumbs'),$_smarty_tpl ) );?>
</div>

                            <?php
}
}
/* {/block 'after_cover_thumbnails'} */
/* {block 'after_cover_thumbnails2'} */
class Block_4756211956005fd687fb470_00666642 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                                <div class="after-cover-tumbnails2 mt-4"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayAfterProductThumbs2'),$_smarty_tpl ) );?>
</div>

                            <?php
}
}
/* {/block 'after_cover_thumbnails2'} */
/* {block 'page_content'} */
class Block_15870344926005fd687f7c60_86032794 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>




                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_21428337696005fd687f8025_67999224', 'product_cover_thumbnails', $this->tplIndex);
?>




                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1280795376005fd687fa7d5_93746580', 'after_cover_thumbnails', $this->tplIndex);
?>




                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_4756211956005fd687fb470_00666642', 'after_cover_thumbnails2', $this->tplIndex);
?>


                        <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_18159503716005fd687f7930_47173289 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                    <section class="page-content" id="content">

                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15870344926005fd687f7c60_86032794', 'page_content', $this->tplIndex);
?>


                    </section>

                <?php
}
}
/* {/block 'page_content_container'} */
/* {block 'product_brand_below'} */
class Block_12665407616005fd687fcc50_91651909 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                            <?php if ($_smarty_tpl->tpl_vars['iqitTheme']->value['pp_man_logo'] == 'next-title') {?>

                                <?php if (isset($_smarty_tpl->tpl_vars['product_manufacturer']->value->id)) {?>

                                    <?php if (isset($_smarty_tpl->tpl_vars['manufacturer_image_url']->value)) {?>

                                        <meta itemprop="brand" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_manufacturer']->value->name, ENT_QUOTES, 'UTF-8');?>
">

                                        <div class="product-manufacturer product-manufacturer-next float-right">

                                            <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_brand_url']->value, ENT_QUOTES, 'UTF-8');?>
">

                                                <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['manufacturer_image_url']->value, ENT_QUOTES, 'UTF-8');?>
"

                                                     class="img-fluid  manufacturer-logo" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_manufacturer']->value->name, ENT_QUOTES, 'UTF-8');?>
" />

                                            </a>

                                        </div>

                                    <?php }?>

                                <?php }?>

                            <?php }?>

                        <?php
}
}
/* {/block 'product_brand_below'} */
/* {block 'page_title'} */
class Block_2465980306005fd68800202_06736702 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8');
}
}
/* {/block 'page_title'} */
/* {block 'page_header'} */
class Block_12180395886005fd687ffe05_95166916 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                        <h1 class="h1 page-title" itemprop="name"><span><?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2465980306005fd68800202_06736702', 'page_title', $this->tplIndex);
?>
</span></h1>

                    <?php
}
}
/* {/block 'page_header'} */
/* {block 'product_brand_below'} */
class Block_10301369836005fd68800e03_40367239 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                            <?php if ($_smarty_tpl->tpl_vars['iqitTheme']->value['pp_man_logo'] == 'title') {?>

                                <?php if (isset($_smarty_tpl->tpl_vars['product_manufacturer']->value->id)) {?>

                                    <meta itemprop="brand" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_manufacturer']->value->name, ENT_QUOTES, 'UTF-8');?>
">

                                        <?php if (isset($_smarty_tpl->tpl_vars['manufacturer_image_url']->value)) {?>

                                            <div class="product-manufacturer mb-3">

                                            <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_brand_url']->value, ENT_QUOTES, 'UTF-8');?>
">

                                                <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['manufacturer_image_url']->value, ENT_QUOTES, 'UTF-8');?>
"

                                                     class="img-fluid  manufacturer-logo" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_manufacturer']->value->name, ENT_QUOTES, 'UTF-8');?>
" />

                                            </a>

                                            </div>

                                        <?php } else { ?>

                                            <label class="label"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Brand','d'=>'Shop.Theme.Catalog'),$_smarty_tpl ) );?>
:</label>

                                            <span>

            <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_brand_url']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_manufacturer']->value->name, ENT_QUOTES, 'UTF-8');?>
</a>

          </span>

                                        <?php }?>



                                <?php }?>

                            <?php }?>

                            <?php if ($_smarty_tpl->tpl_vars['iqitTheme']->value['pp_man_logo'] == 'next-title') {?>

                                <?php if (isset($_smarty_tpl->tpl_vars['product_manufacturer']->value->id)) {?>

                                    <?php if (!isset($_smarty_tpl->tpl_vars['manufacturer_image_url']->value)) {?>

                                        <meta itemprop="brand" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_manufacturer']->value->name, ENT_QUOTES, 'UTF-8');?>
">

                                        <label class="label"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Brand','d'=>'Shop.Theme.Catalog'),$_smarty_tpl ) );?>
:</label>

                                        <span>

                                        <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_brand_url']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_manufacturer']->value->name, ENT_QUOTES, 'UTF-8');?>
</a>

                                        </span>

                                    <?php }?>

                                     <?php if ($_smarty_tpl->tpl_vars['product']->value['ean13']) {?>

        <meta itemprop="gtin13" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['ean13'], ENT_QUOTES, 'UTF-8');?>
">

        <?php }?>

                                <?php }?>

                            <?php }?>

                        <?php
}
}
/* {/block 'product_brand_below'} */
/* {block 'product_description_short'} */
class Block_5752502106005fd68807b67_54273842 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                        <div id="product-description-short-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['id'], ENT_QUOTES, 'UTF-8');?>
"

                             itemprop="description" class="rte-content"><?php echo $_smarty_tpl->tpl_vars['product']->value['description_short'];?>
</div>

                    <?php
}
}
/* {/block 'product_description_short'} */
/* {block 'hook_display_product_rating'} */
class Block_13096645816005fd68808ba6_47575653 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayProductRating','product'=>$_smarty_tpl->tpl_vars['product']->value),$_smarty_tpl ) );?>


                        <?php
}
}
/* {/block 'hook_display_product_rating'} */
/* {block 'product_prices'} */
class Block_10875223736005fd6880a300_02100189 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                                <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/product-prices.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                            <?php
}
}
/* {/block 'product_prices'} */
/* {block 'page_header_container'} */
class Block_9189688206005fd687fc856_40753678 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                    <div class="product_header_container clearfix">



                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12665407616005fd687fcc50_91651909', 'product_brand_below', $this->tplIndex);
?>




                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12180395886005fd687ffe05_95166916', 'page_header', $this->tplIndex);
?>


                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10301369836005fd68800e03_40367239', 'product_brand_below', $this->tplIndex);
?>




                               <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_5752502106005fd68807b67_54273842', 'product_description_short', $this->tplIndex);
?>



                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13096645816005fd68808ba6_47575653', 'hook_display_product_rating', $this->tplIndex);
?>




                        <?php if ($_smarty_tpl->tpl_vars['iqitTheme']->value['pp_price_position'] == 'below-title') {?>

                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10875223736005fd6880a300_02100189', 'product_prices', $this->tplIndex);
?>


                        <?php }?>

                    </div>

                <?php
}
}
/* {/block 'page_header_container'} */
/* {block 'product_customization'} */
class Block_1019824326005fd6880c051_36711128 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                            <?php $_smarty_tpl->_subTemplateRender("file:catalog/_partials/product-customization.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('customizations'=>$_smarty_tpl->tpl_vars['product']->value['customizations']), 0, false);
?>

                        <?php
}
}
/* {/block 'product_customization'} */
/* {block 'product_variants'} */
class Block_16817189106005fd6880e6b3_57058922 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayProductVariants','product'=>$_smarty_tpl->tpl_vars['product']->value),$_smarty_tpl ) );?>


                                    <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/product-variants.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                                <?php
}
}
/* {/block 'product_variants'} */
/* {block 'product_miniature'} */
class Block_7942679226005fd68810c22_13722819 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                                                    <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/miniatures/pack-product.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('product'=>$_smarty_tpl->tpl_vars['product_pack']->value), 0, true);
?>

                                                <?php
}
}
/* {/block 'product_miniature'} */
/* {block 'product_pack'} */
class Block_5249101246005fd6880f671_46523005 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                                    <?php if ($_smarty_tpl->tpl_vars['packItems']->value) {?>

                                        <section class="product-pack">

                                            <p class="h4"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'This pack contains','d'=>'Shop.Theme.Catalog'),$_smarty_tpl ) );?>
</p>

                                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['packItems']->value, 'product_pack');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['product_pack']->value) {
?>

                                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_7942679226005fd68810c22_13722819', 'product_miniature', $this->tplIndex);
?>


                                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

                                        </section>

                                    <?php }?>

                                <?php
}
}
/* {/block 'product_pack'} */
/* {block 'product_prices'} */
class Block_18586099216005fd68812450_03464221 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                                        <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/product-prices.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

                                    <?php
}
}
/* {/block 'product_prices'} */
/* {block 'product_add_to_cart'} */
class Block_9490124646005fd68812ea6_28611710 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                                    <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/product-add-to-cart.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                                <?php
}
}
/* {/block 'product_add_to_cart'} */
/* {block 'product_discounts'} */
class Block_21076399376005fd68813780_05773365 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                                    <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/product-discounts.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                                <?php
}
}
/* {/block 'product_discounts'} */
/* {block 'product_additional_info'} */
class Block_10667852796005fd68814116_45880594 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                                    <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/product-additional-info.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                                <?php
}
}
/* {/block 'product_additional_info'} */
/* {block 'product_refresh'} */
class Block_12458831936005fd68814ad2_70681962 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'product_refresh'} */
/* {block 'product_buy'} */
class Block_15829548816005fd6880d0c8_93923682 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                            <form action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['cart'], ENT_QUOTES, 'UTF-8');?>
" method="post" id="add-to-cart-or-refresh">

                                <input type="hidden" name="token" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['static_token']->value, ENT_QUOTES, 'UTF-8');?>
">

                                <input type="hidden" name="id_product" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['id'], ENT_QUOTES, 'UTF-8');?>
"

                                       id="product_page_product_id">

                                <input type="hidden" name="id_customization" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['id_customization'], ENT_QUOTES, 'UTF-8');?>
"

                                       id="product_customization_id">



                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_16817189106005fd6880e6b3_57058922', 'product_variants', $this->tplIndex);
?>




                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_5249101246005fd6880f671_46523005', 'product_pack', $this->tplIndex);
?>




                                <?php if ($_smarty_tpl->tpl_vars['iqitTheme']->value['pp_price_position'] == 'above-button') {?>

                                    <div class="product_p_price_container">

                                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18586099216005fd68812450_03464221', 'product_prices', $this->tplIndex);
?>


                                    </div>

                                <?php }?>



                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9490124646005fd68812ea6_28611710', 'product_add_to_cart', $this->tplIndex);
?>




                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_21076399376005fd68813780_05773365', 'product_discounts', $this->tplIndex);
?>




                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10667852796005fd68814116_45880594', 'product_additional_info', $this->tplIndex);
?>




                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12458831936005fd68814ad2_70681962', 'product_refresh', $this->tplIndex);
?>


                            </form>

                        <?php
}
}
/* {/block 'product_buy'} */
/* {block 'hook_display_reassurance'} */
class Block_10115725966005fd688151f9_74236198 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayReassurance'),$_smarty_tpl ) );?>


                        <?php
}
}
/* {/block 'hook_display_reassurance'} */
/* {block 'product_miniature'} */
class Block_11670660846005fd68818f92_43164593 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                                            <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/miniatures/product-small.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('product'=>$_smarty_tpl->tpl_vars['product_accessory']->value,'carousel'=>true,'elementor'=>true,'richData'=>true), 0, true);
?>

                                        <?php
}
}
/* {/block 'product_miniature'} */
/* {block 'product_accessories_sidebar'} */
class Block_12513278386005fd68817cc9_76524435 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                        <?php if ($_smarty_tpl->tpl_vars['accessories']->value) {?>

                            <section class="product-accessories product-accessories-sidebar block">

                                <p class="block-title"><span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'You might also like','d'=>'Shop.Theme.Catalog'),$_smarty_tpl ) );?>
</span></p>

                                <div id="product-accessories-sidebar" class="block-content products products-grid">

                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['accessories']->value, 'product_accessory');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['product_accessory']->value) {
?>

                                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11670660846005fd68818f92_43164593', 'product_miniature', $this->tplIndex);
?>


                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

                                </div>

                            </section>

                        <?php }?>

                    <?php
}
}
/* {/block 'product_accessories_sidebar'} */
/* {block 'product_miniature'} */
class Block_6611170016005fd6881bff4_40016884 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                                        <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/miniatures/product.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('product'=>$_smarty_tpl->tpl_vars['product_accessory']->value,'carousel'=>true,'richData'=>true), 0, true);
?>

                                    <?php
}
}
/* {/block 'product_miniature'} */
/* {block 'product_accessories_footer'} */
class Block_9032634966005fd6881ac22_66725730 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                <?php if ($_smarty_tpl->tpl_vars['accessories']->value) {?>

                    <section class="product-accessories block block-section">

                        <h3 class="section-title"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Related Products','d'=>'Shop.Theme.Catalog'),$_smarty_tpl ) );?>
</h3>

                        <div class="block-content">

                            <div class="products slick-products-carousel products-grid slick-default-carousel">

                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['accessories']->value, 'product_accessory');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['product_accessory']->value) {
?>

                                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6611170016005fd6881bff4_40016884', 'product_miniature', $this->tplIndex);
?>


                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

                            </div>

                        </div>

                    </section>

                <?php }?>

            <?php
}
}
/* {/block 'product_accessories_footer'} */
/* {block 'product_footer'} */
class Block_19312305866005fd6881d525_42001038 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayFooterProduct','product'=>$_smarty_tpl->tpl_vars['product']->value,'category'=>$_smarty_tpl->tpl_vars['category']->value),$_smarty_tpl ) );?>


        <?php
}
}
/* {/block 'product_footer'} */
/* {block 'product_images_modal'} */
class Block_12587464986005fd6881df45_93028804 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


            <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/product-images-modal.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        <?php
}
}
/* {/block 'product_images_modal'} */
/* {block 'page_footer'} */
class Block_10224908846005fd6881ea47_25452899 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                    <!-- Footer content -->

                <?php
}
}
/* {/block 'page_footer'} */
/* {block 'page_footer_container'} */
class Block_18000264956005fd6881e740_67294152 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


            <footer class="page-footer">

                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10224908846005fd6881ea47_25452899', 'page_footer', $this->tplIndex);
?>


            </footer>

        <?php
}
}
/* {/block 'page_footer_container'} */
/* {block 'content'} */
class Block_10709317166005fd687f4074_08077063 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_10709317166005fd687f4074_08077063',
  ),
  'page_content_container' => 
  array (
    0 => 'Block_18159503716005fd687f7930_47173289',
  ),
  'page_content' => 
  array (
    0 => 'Block_15870344926005fd687f7c60_86032794',
  ),
  'product_cover_thumbnails' => 
  array (
    0 => 'Block_21428337696005fd687f8025_67999224',
  ),
  'after_cover_thumbnails' => 
  array (
    0 => 'Block_1280795376005fd687fa7d5_93746580',
  ),
  'after_cover_thumbnails2' => 
  array (
    0 => 'Block_4756211956005fd687fb470_00666642',
  ),
  'page_header_container' => 
  array (
    0 => 'Block_9189688206005fd687fc856_40753678',
  ),
  'product_brand_below' => 
  array (
    0 => 'Block_12665407616005fd687fcc50_91651909',
    1 => 'Block_10301369836005fd68800e03_40367239',
  ),
  'page_header' => 
  array (
    0 => 'Block_12180395886005fd687ffe05_95166916',
  ),
  'page_title' => 
  array (
    0 => 'Block_2465980306005fd68800202_06736702',
  ),
  'product_description_short' => 
  array (
    0 => 'Block_5752502106005fd68807b67_54273842',
  ),
  'hook_display_product_rating' => 
  array (
    0 => 'Block_13096645816005fd68808ba6_47575653',
  ),
  'product_prices' => 
  array (
    0 => 'Block_10875223736005fd6880a300_02100189',
    1 => 'Block_18586099216005fd68812450_03464221',
  ),
  'product_customization' => 
  array (
    0 => 'Block_1019824326005fd6880c051_36711128',
  ),
  'product_buy' => 
  array (
    0 => 'Block_15829548816005fd6880d0c8_93923682',
  ),
  'product_variants' => 
  array (
    0 => 'Block_16817189106005fd6880e6b3_57058922',
  ),
  'product_pack' => 
  array (
    0 => 'Block_5249101246005fd6880f671_46523005',
  ),
  'product_miniature' => 
  array (
    0 => 'Block_7942679226005fd68810c22_13722819',
    1 => 'Block_11670660846005fd68818f92_43164593',
    2 => 'Block_6611170016005fd6881bff4_40016884',
  ),
  'product_add_to_cart' => 
  array (
    0 => 'Block_9490124646005fd68812ea6_28611710',
  ),
  'product_discounts' => 
  array (
    0 => 'Block_21076399376005fd68813780_05773365',
  ),
  'product_additional_info' => 
  array (
    0 => 'Block_10667852796005fd68814116_45880594',
  ),
  'product_refresh' => 
  array (
    0 => 'Block_12458831936005fd68814ad2_70681962',
  ),
  'hook_display_reassurance' => 
  array (
    0 => 'Block_10115725966005fd688151f9_74236198',
  ),
  'product_accessories_sidebar' => 
  array (
    0 => 'Block_12513278386005fd68817cc9_76524435',
  ),
  'product_accessories_footer' => 
  array (
    0 => 'Block_9032634966005fd6881ac22_66725730',
  ),
  'product_footer' => 
  array (
    0 => 'Block_19312305866005fd6881d525_42001038',
  ),
  'product_images_modal' => 
  array (
    0 => 'Block_12587464986005fd6881df45_93028804',
  ),
  'page_footer_container' => 
  array (
    0 => 'Block_18000264956005fd6881e740_67294152',
  ),
  'page_footer' => 
  array (
    0 => 'Block_10224908846005fd6881ea47_25452899',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


    <section id="main" itemscope itemtype="https://schema.org/Product">

        <div id="product-preloader"><i class="fa fa-circle-o-notch fa-spin"></i></div>

        <div id="main-product-wrapper" class="product-container">

        <meta itemprop="url" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['url'], ENT_QUOTES, 'UTF-8');?>
">



        <?php if ($_smarty_tpl->tpl_vars['product']->value['upc']) {?>

        <meta itemprop="gtin12" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['upc'], ENT_QUOTES, 'UTF-8');?>
">

        <?php }?>



        <?php if ($_smarty_tpl->tpl_vars['product']->value['ean13']) {?>

        <meta itemprop="gtin13" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['ean13'], ENT_QUOTES, 'UTF-8');?>
">

        <?php }?>



        <?php if (!$_smarty_tpl->tpl_vars['product']->value['upc'] && !$_smarty_tpl->tpl_vars['product']->value['ean13']) {?>

            <meta itemprop="identifier_exist" content="no">

        <?php }?>

 



        <div class="row product-info-row">

            <div class="col-md-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['iqitTheme']->value['pp_img_width'], ENT_QUOTES, 'UTF-8');?>
 col-product-image">

                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18159503716005fd687f7930_47173289', 'page_content_container', $this->tplIndex);
?>


            </div>



            <div class="col-md-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['iqitTheme']->value['pp_content_width'], ENT_QUOTES, 'UTF-8');?>
 col-product-info">

                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9189688206005fd687fc856_40753678', 'page_header_container', $this->tplIndex);
?>




                <div class="product-information">

                 



                    <?php if ($_smarty_tpl->tpl_vars['product']->value['is_customizable'] && count($_smarty_tpl->tpl_vars['product']->value['customizations']['fields'])) {?>

                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1019824326005fd6880c051_36711128', 'product_customization', $this->tplIndex);
?>


                    <?php }?>



                    <div class="product-actions">

                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15829548816005fd6880d0c8_93923682', 'product_buy', $this->tplIndex);
?>




                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10115725966005fd688151f9_74236198', 'hook_display_reassurance', $this->tplIndex);
?>




                    </div>

                </div>


 <?php if ($_smarty_tpl->tpl_vars['iqitTheme']->value['pp_tabs'] == 'tabh' || $_smarty_tpl->tpl_vars['iqitTheme']->value['pp_tabs'] == 'tabha') {?>

            <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/_product_partials/product-tabs-h.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        <?php } elseif ($_smarty_tpl->tpl_vars['iqitTheme']->value['pp_tabs'] == 'section') {?>

            <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/_product_partials/product-tabs-sections.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }?>

            </div>



            <?php if ($_smarty_tpl->tpl_vars['iqitTheme']->value['pp_sidebar']) {?>

            <div class="col-md-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['iqitTheme']->value['pp_sidebar'], ENT_QUOTES, 'UTF-8');?>
 sidebar product-sidebar">



                <?php if ($_smarty_tpl->tpl_vars['iqitTheme']->value['pp_accesories'] == 'sidebar') {?>

                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12513278386005fd68817cc9_76524435', 'product_accessories_sidebar', $this->tplIndex);
?>


                <?php }?>



                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayRightColumnProduct'),$_smarty_tpl ) );?>




            </div>

            <?php }?>



        </div>



       


        </div>

        <?php if ($_smarty_tpl->tpl_vars['iqitTheme']->value['pp_accesories'] == 'footer') {?>

            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9032634966005fd6881ac22_66725730', 'product_accessories_footer', $this->tplIndex);
?>


        <?php }?>



        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_19312305866005fd6881d525_42001038', 'product_footer', $this->tplIndex);
?>




        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12587464986005fd6881df45_93028804', 'product_images_modal', $this->tplIndex);
?>




        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18000264956005fd6881e740_67294152', 'page_footer_container', $this->tplIndex);
?>




    </section>

<?php
}
}
/* {/block 'content'} */
}
