<?php
/* Smarty version 3.1.33, created on 2021-01-19 12:56:12
  from '/home2/amanoyarns/public_html/store/themes/warehousechild/templates/customer/my-account.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_60071d3c68a6d1_69192405',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5af6f78482fbf9712bdca49c4bc47ec23f72f6ab' => 
    array (
      0 => '/home2/amanoyarns/public_html/store/themes/warehousechild/templates/customer/my-account.tpl',
      1 => 1597518116,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60071d3c68a6d1_69192405 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_150562738060071d3c688952_58722008', 'page_content');
?>



<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, 'customer/page.tpl');
}
/* {block 'page_content'} */
class Block_150562738060071d3c688952_58722008 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'page_content' => 
  array (
    0 => 'Block_150562738060071d3c688952_58722008',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

 <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'It is your account dashboard. Choose your section. ','d'=>'Shop.Theme.Customeraccount'),$_smarty_tpl ) );?>

 <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayMyAccountDashboard'),$_smarty_tpl ) );?>

<?php
}
}
/* {/block 'page_content'} */
}
