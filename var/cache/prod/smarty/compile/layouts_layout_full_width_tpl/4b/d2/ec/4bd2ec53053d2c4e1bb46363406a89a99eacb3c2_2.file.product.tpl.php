<?php
/* Smarty version 3.1.33, created on 2021-03-19 17:02:11
  from '/home2/amanoyarns/public_html/multitienda/themes/warehousechild/templates/catalog/product.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_60551153eb9fd6_06457247',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4bd2ec53053d2c4e1bb46363406a89a99eacb3c2' => 
    array (
      0 => '/home2/amanoyarns/public_html/multitienda/themes/warehousechild/templates/catalog/product.tpl',
      1 => 1611278172,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:catalog/_partials/product-cover-thumbnails.tpl' => 1,
    'file:catalog/_partials/product-prices.tpl' => 2,
    'file:catalog/_partials/product-customization.tpl' => 1,
    'file:catalog/_partials/product-variants.tpl' => 1,
    'file:catalog/_partials/miniatures/pack-product.tpl' => 1,
    'file:catalog/_partials/product-add-to-cart.tpl' => 1,
    'file:catalog/_partials/product-discounts.tpl' => 1,
    'file:catalog/_partials/product-additional-info.tpl' => 1,
    'file:catalog/_partials/_product_partials/product-tabs-h.tpl' => 1,
    'file:catalog/_partials/_product_partials/product-tabs-sections.tpl' => 1,
    'file:catalog/_partials/miniatures/product-small.tpl' => 1,
    'file:catalog/_partials/miniatures/product.tpl' => 1,
    'file:catalog/_partials/product-images-modal.tpl' => 1,
  ),
),false)) {
function content_60551153eb9fd6_06457247 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>





<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_206554441260551153e8ceb8_07139465', 'head_seo');
?>






<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_160869644660551153e8db92_44079626', 'head_og_tags');
?>






<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_49627049660551153e90077_34350432', 'head');
?>






<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_151696000660551153e967f2_72099087', 'content');
?>


<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['layout']->value);
}
/* {block 'head_seo'} */
class Block_206554441260551153e8ceb8_07139465 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'head_seo' => 
  array (
    0 => 'Block_206554441260551153e8ceb8_07139465',
  ),
);
public $prepend = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


    <link rel="canonical" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['canonical_url'], ENT_QUOTES, 'UTF-8');?>
">

<?php
}
}
/* {/block 'head_seo'} */
/* {block 'head_og_tags'} */
class Block_160869644660551153e8db92_44079626 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'head_og_tags' => 
  array (
    0 => 'Block_160869644660551153e8db92_44079626',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


    <meta property="og:type" content="product">

    <meta property="og:url" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['current_url'], ENT_QUOTES, 'UTF-8');?>
">

    <meta property="og:title" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['meta']['title'], ENT_QUOTES, 'UTF-8');?>
">

    <meta property="og:site_name" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['name'], ENT_QUOTES, 'UTF-8');?>
">

    <meta property="og:description" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['meta']['description'], ENT_QUOTES, 'UTF-8');?>
">

    <meta property="og:image" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['cover']['large']['url'], ENT_QUOTES, 'UTF-8');?>
">

    <meta property="og:image:width" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['cover']['large']['width'], ENT_QUOTES, 'UTF-8');?>
">

    <meta property="og:image:height" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['cover']['large']['height'], ENT_QUOTES, 'UTF-8');?>
">

<?php
}
}
/* {/block 'head_og_tags'} */
/* {block 'head'} */
class Block_49627049660551153e90077_34350432 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'head' => 
  array (
    0 => 'Block_49627049660551153e90077_34350432',
  ),
);
public $append = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


    <?php if ($_smarty_tpl->tpl_vars['product']->value['show_price']) {?>

        <meta property="product:pretax_price:amount" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['price_tax_exc'], ENT_QUOTES, 'UTF-8');?>
">

        <meta property="product:pretax_price:currency" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['currency']->value['iso_code'], ENT_QUOTES, 'UTF-8');?>
">

        <meta property="product:price:amount" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['price_amount'], ENT_QUOTES, 'UTF-8');?>
">

        <meta property="product:price:currency" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['currency']->value['iso_code'], ENT_QUOTES, 'UTF-8');?>
">

    <?php }?>

    <?php if (isset($_smarty_tpl->tpl_vars['product']->value['weight']) && ($_smarty_tpl->tpl_vars['product']->value['weight'] != 0)) {?>

        <meta property="product:weight:value" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['weight'], ENT_QUOTES, 'UTF-8');?>
">

        <meta property="product:weight:units" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['weight_unit'], ENT_QUOTES, 'UTF-8');?>
">

    <?php }?>



    <?php if ($_smarty_tpl->tpl_vars['iqitTheme']->value['bread_bg_category']) {?>

        <?php $_smarty_tpl->_assignInScope('categoryImage', "img/c/".((string)$_smarty_tpl->tpl_vars['product']->value['id_category_default'])."-category_default.jpg");?>

        <?php if (file_exists($_smarty_tpl->tpl_vars['categoryImage']->value)) {?>

            <style> #wrapper .breadcrumb{  background-image: url('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getCatImageLink($_smarty_tpl->tpl_vars['product']->value['category'],$_smarty_tpl->tpl_vars['product']->value['id_category_default'],'category_default'), ENT_QUOTES, 'UTF-8');?>
'); }</style>

        <?php }?>

    <?php }?>



<?php
}
}
/* {/block 'head'} */
/* {block 'product_cover_thumbnails'} */
class Block_66072993060551153e995b5_20907237 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                                <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/product-cover-thumbnails.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                            <?php
}
}
/* {/block 'product_cover_thumbnails'} */
/* {block 'after_cover_thumbnails'} */
class Block_127613644660551153e9bc96_48225994 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                                <div class="after-cover-tumbnails text-center"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayAfterProductThumbs'),$_smarty_tpl ) );?>
</div>

                            <?php
}
}
/* {/block 'after_cover_thumbnails'} */
/* {block 'after_cover_thumbnails2'} */
class Block_380463260551153e9c810_06981157 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                                <div class="after-cover-tumbnails2 mt-4"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayAfterProductThumbs2'),$_smarty_tpl ) );?>
</div>

                            <?php
}
}
/* {/block 'after_cover_thumbnails2'} */
/* {block 'page_content'} */
class Block_151094401060551153e992f3_25426390 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>




                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_66072993060551153e995b5_20907237', 'product_cover_thumbnails', $this->tplIndex);
?>




                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_127613644660551153e9bc96_48225994', 'after_cover_thumbnails', $this->tplIndex);
?>




                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_380463260551153e9c810_06981157', 'after_cover_thumbnails2', $this->tplIndex);
?>


                        <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_83154880560551153e98fe8_30641218 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                    <section class="page-content" id="content">

                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_151094401060551153e992f3_25426390', 'page_content', $this->tplIndex);
?>


                    </section>

                <?php
}
}
/* {/block 'page_content_container'} */
/* {block 'product_brand_below'} */
class Block_126375053460551153e9db68_96163067 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                            <?php if ($_smarty_tpl->tpl_vars['iqitTheme']->value['pp_man_logo'] == 'next-title') {?>

                                <?php if (isset($_smarty_tpl->tpl_vars['product_manufacturer']->value->id)) {?>

                                    <?php if (isset($_smarty_tpl->tpl_vars['manufacturer_image_url']->value)) {?>

                                        <meta itemprop="brand" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_manufacturer']->value->name, ENT_QUOTES, 'UTF-8');?>
">

                                        <div class="product-manufacturer product-manufacturer-next float-right">

                                            <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_brand_url']->value, ENT_QUOTES, 'UTF-8');?>
">

                                                <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['manufacturer_image_url']->value, ENT_QUOTES, 'UTF-8');?>
"

                                                     class="img-fluid  manufacturer-logo" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_manufacturer']->value->name, ENT_QUOTES, 'UTF-8');?>
" />

                                            </a>

                                        </div>

                                    <?php }?>

                                <?php }?>

                            <?php }?>

                        <?php
}
}
/* {/block 'product_brand_below'} */
/* {block 'page_title'} */
class Block_132954350860551153ea02a7_59049212 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8');
}
}
/* {/block 'page_title'} */
/* {block 'page_header'} */
class Block_81196678460551153e9ffa3_52929810 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                        <h1 class="h1 page-title" itemprop="name"><span><?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_132954350860551153ea02a7_59049212', 'page_title', $this->tplIndex);
?>
</span></h1>

                    <?php
}
}
/* {/block 'page_header'} */
/* {block 'product_brand_below'} */
class Block_143278750760551153ea0d20_78249784 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                            <?php if ($_smarty_tpl->tpl_vars['iqitTheme']->value['pp_man_logo'] == 'title') {?>

                                <?php if (isset($_smarty_tpl->tpl_vars['product_manufacturer']->value->id)) {?>

                                    <meta itemprop="brand" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_manufacturer']->value->name, ENT_QUOTES, 'UTF-8');?>
">

                                        <?php if (isset($_smarty_tpl->tpl_vars['manufacturer_image_url']->value)) {?>

                                            <div class="product-manufacturer mb-3">

                                            <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_brand_url']->value, ENT_QUOTES, 'UTF-8');?>
">

                                                <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['manufacturer_image_url']->value, ENT_QUOTES, 'UTF-8');?>
"

                                                     class="img-fluid  manufacturer-logo" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_manufacturer']->value->name, ENT_QUOTES, 'UTF-8');?>
" />

                                            </a>

                                            </div>

                                        <?php } else { ?>

                                            <label class="label"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Brand','d'=>'Shop.Theme.Catalog'),$_smarty_tpl ) );?>
:</label>

                                            <span>

            <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_brand_url']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_manufacturer']->value->name, ENT_QUOTES, 'UTF-8');?>
</a>

          </span>

                                        <?php }?>



                                <?php }?>

                            <?php }?>

                            <?php if ($_smarty_tpl->tpl_vars['iqitTheme']->value['pp_man_logo'] == 'next-title') {?>

                                <?php if (isset($_smarty_tpl->tpl_vars['product_manufacturer']->value->id)) {?>

                                    <?php if (!isset($_smarty_tpl->tpl_vars['manufacturer_image_url']->value)) {?>

                                        <meta itemprop="brand" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_manufacturer']->value->name, ENT_QUOTES, 'UTF-8');?>
">

                                        <label class="label"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Brand','d'=>'Shop.Theme.Catalog'),$_smarty_tpl ) );?>
:</label>

                                        <span>

                                        <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_brand_url']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_manufacturer']->value->name, ENT_QUOTES, 'UTF-8');?>
</a>

                                        </span>

                                    <?php }?>

                                     <?php if ($_smarty_tpl->tpl_vars['product']->value['ean13']) {?>

        <meta itemprop="gtin13" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['ean13'], ENT_QUOTES, 'UTF-8');?>
">

        <?php }?>

                                <?php }?>

                            <?php }?>

                        <?php
}
}
/* {/block 'product_brand_below'} */
/* {block 'product_description_short'} */
class Block_197305426060551153ea6343_17646418 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                        <div id="product-description-short-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['id'], ENT_QUOTES, 'UTF-8');?>
"

                             itemprop="description" class="rte-content"><?php echo $_smarty_tpl->tpl_vars['product']->value['description_short'];?>
</div>

                    <?php
}
}
/* {/block 'product_description_short'} */
/* {block 'hook_display_product_rating'} */
class Block_112720502160551153ea7121_31931305 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayProductRating','product'=>$_smarty_tpl->tpl_vars['product']->value),$_smarty_tpl ) );?>


                        <?php
}
}
/* {/block 'hook_display_product_rating'} */
/* {block 'product_prices'} */
class Block_62481395960551153ea7eb5_99067187 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                                <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/product-prices.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                            <?php
}
}
/* {/block 'product_prices'} */
/* {block 'page_header_container'} */
class Block_65930335560551153e9d874_25508255 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                    <div class="product_header_container clearfix">



                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_126375053460551153e9db68_96163067', 'product_brand_below', $this->tplIndex);
?>




                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_81196678460551153e9ffa3_52929810', 'page_header', $this->tplIndex);
?>


                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_143278750760551153ea0d20_78249784', 'product_brand_below', $this->tplIndex);
?>




                               <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_197305426060551153ea6343_17646418', 'product_description_short', $this->tplIndex);
?>



                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_112720502160551153ea7121_31931305', 'hook_display_product_rating', $this->tplIndex);
?>




                        <?php if ($_smarty_tpl->tpl_vars['iqitTheme']->value['pp_price_position'] == 'below-title') {?>

                            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_62481395960551153ea7eb5_99067187', 'product_prices', $this->tplIndex);
?>


                        <?php }?>

                    </div>

                <?php
}
}
/* {/block 'page_header_container'} */
/* {block 'product_customization'} */
class Block_37200965760551153ea9447_43732376 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                            <?php $_smarty_tpl->_subTemplateRender("file:catalog/_partials/product-customization.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('customizations'=>$_smarty_tpl->tpl_vars['product']->value['customizations']), 0, false);
?>

                        <?php
}
}
/* {/block 'product_customization'} */
/* {block 'product_variants'} */
class Block_10572475360551153eab156_78143534 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayProductVariants','product'=>$_smarty_tpl->tpl_vars['product']->value),$_smarty_tpl ) );?>


                                    <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/product-variants.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                                <?php
}
}
/* {/block 'product_variants'} */
/* {block 'product_miniature'} */
class Block_63394042460551153ead0e4_81684211 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                                                    <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/miniatures/pack-product.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('product'=>$_smarty_tpl->tpl_vars['product_pack']->value), 0, true);
?>

                                                <?php
}
}
/* {/block 'product_miniature'} */
/* {block 'product_pack'} */
class Block_156059974560551153eabcd1_90501053 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                                    <?php if ($_smarty_tpl->tpl_vars['packItems']->value) {?>

                                        <section class="product-pack">

                                            <p class="h4"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'This pack contains','d'=>'Shop.Theme.Catalog'),$_smarty_tpl ) );?>
</p>

                                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['packItems']->value, 'product_pack');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['product_pack']->value) {
?>

                                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_63394042460551153ead0e4_81684211', 'product_miniature', $this->tplIndex);
?>


                                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

                                        </section>

                                    <?php }?>

                                <?php
}
}
/* {/block 'product_pack'} */
/* {block 'product_prices'} */
class Block_10400346660551153eae463_25739252 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                                        <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/product-prices.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

                                    <?php
}
}
/* {/block 'product_prices'} */
/* {block 'product_add_to_cart'} */
class Block_125503084260551153eaed60_97715513 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                                    <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/product-add-to-cart.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                                <?php
}
}
/* {/block 'product_add_to_cart'} */
/* {block 'product_discounts'} */
class Block_61337156860551153eaf4e2_06369346 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                                    <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/product-discounts.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                                <?php
}
}
/* {/block 'product_discounts'} */
/* {block 'product_additional_info'} */
class Block_69758010060551153eafc52_82128137 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                                    <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/product-additional-info.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                                <?php
}
}
/* {/block 'product_additional_info'} */
/* {block 'product_refresh'} */
class Block_106480047060551153eb0471_13404308 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'product_refresh'} */
/* {block 'product_buy'} */
class Block_88498862360551153eaa148_90802993 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                            <form action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['cart'], ENT_QUOTES, 'UTF-8');?>
" method="post" id="add-to-cart-or-refresh">

                                <input type="hidden" name="token" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['static_token']->value, ENT_QUOTES, 'UTF-8');?>
">

                                <input type="hidden" name="id_product" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['id'], ENT_QUOTES, 'UTF-8');?>
"

                                       id="product_page_product_id">

                                <input type="hidden" name="id_customization" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['id_customization'], ENT_QUOTES, 'UTF-8');?>
"

                                       id="product_customization_id">



                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10572475360551153eab156_78143534', 'product_variants', $this->tplIndex);
?>




                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_156059974560551153eabcd1_90501053', 'product_pack', $this->tplIndex);
?>




                                <?php if ($_smarty_tpl->tpl_vars['iqitTheme']->value['pp_price_position'] == 'above-button') {?>

                                    <div class="product_p_price_container">

                                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10400346660551153eae463_25739252', 'product_prices', $this->tplIndex);
?>


                                    </div>

                                <?php }?>



                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_125503084260551153eaed60_97715513', 'product_add_to_cart', $this->tplIndex);
?>




                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_61337156860551153eaf4e2_06369346', 'product_discounts', $this->tplIndex);
?>




                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_69758010060551153eafc52_82128137', 'product_additional_info', $this->tplIndex);
?>




                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_106480047060551153eb0471_13404308', 'product_refresh', $this->tplIndex);
?>


                            </form>

                        <?php
}
}
/* {/block 'product_buy'} */
/* {block 'hook_display_reassurance'} */
class Block_192875595460551153eb0b47_21901147 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayReassurance'),$_smarty_tpl ) );?>


                        <?php
}
}
/* {/block 'hook_display_reassurance'} */
/* {block 'product_miniature'} */
class Block_126284317660551153eb4617_57835602 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                                            <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/miniatures/product-small.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('product'=>$_smarty_tpl->tpl_vars['product_accessory']->value,'carousel'=>true,'elementor'=>true,'richData'=>true), 0, true);
?>

                                        <?php
}
}
/* {/block 'product_miniature'} */
/* {block 'product_accessories_sidebar'} */
class Block_103568423360551153eb33c7_42273834 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                        <?php if ($_smarty_tpl->tpl_vars['accessories']->value) {?>

                            <section class="product-accessories product-accessories-sidebar block">

                                <p class="block-title"><span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'You might also like','d'=>'Shop.Theme.Catalog'),$_smarty_tpl ) );?>
</span></p>

                                <div id="product-accessories-sidebar" class="block-content products products-grid">

                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['accessories']->value, 'product_accessory');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['product_accessory']->value) {
?>

                                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_126284317660551153eb4617_57835602', 'product_miniature', $this->tplIndex);
?>


                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

                                </div>

                            </section>

                        <?php }?>

                    <?php
}
}
/* {/block 'product_accessories_sidebar'} */
/* {block 'product_miniature'} */
class Block_36556082960551153eb6fa8_36257272 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                                        <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/miniatures/product.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('product'=>$_smarty_tpl->tpl_vars['product_accessory']->value,'carousel'=>true,'richData'=>true), 0, true);
?>

                                    <?php
}
}
/* {/block 'product_miniature'} */
/* {block 'product_accessories_footer'} */
class Block_108368235260551153eb60d5_21348948 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                <?php if ($_smarty_tpl->tpl_vars['accessories']->value) {?>

                    <section class="product-accessories block block-section">

                        <h3 class="section-title"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Related Products','d'=>'Shop.Theme.Catalog'),$_smarty_tpl ) );?>
</h3>

                        <div class="block-content">

                            <div class="products slick-products-carousel products-grid slick-default-carousel">

                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['accessories']->value, 'product_accessory');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['product_accessory']->value) {
?>

                                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_36556082960551153eb6fa8_36257272', 'product_miniature', $this->tplIndex);
?>


                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

                            </div>

                        </div>

                    </section>

                <?php }?>

            <?php
}
}
/* {/block 'product_accessories_footer'} */
/* {block 'product_footer'} */
class Block_94132952660551153eb80c8_97373970 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayFooterProduct','product'=>$_smarty_tpl->tpl_vars['product']->value,'category'=>$_smarty_tpl->tpl_vars['category']->value),$_smarty_tpl ) );?>


        <?php
}
}
/* {/block 'product_footer'} */
/* {block 'product_images_modal'} */
class Block_164737090360551153eb8a51_76701401 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


            <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/product-images-modal.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        <?php
}
}
/* {/block 'product_images_modal'} */
/* {block 'page_footer'} */
class Block_159152969060551153eb94a2_04314832 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                    <!-- Footer content -->

                <?php
}
}
/* {/block 'page_footer'} */
/* {block 'page_footer_container'} */
class Block_179478018360551153eb91e4_48690687 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


            <footer class="page-footer">

                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_159152969060551153eb94a2_04314832', 'page_footer', $this->tplIndex);
?>


            </footer>

        <?php
}
}
/* {/block 'page_footer_container'} */
/* {block 'content'} */
class Block_151696000660551153e967f2_72099087 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_151696000660551153e967f2_72099087',
  ),
  'page_content_container' => 
  array (
    0 => 'Block_83154880560551153e98fe8_30641218',
  ),
  'page_content' => 
  array (
    0 => 'Block_151094401060551153e992f3_25426390',
  ),
  'product_cover_thumbnails' => 
  array (
    0 => 'Block_66072993060551153e995b5_20907237',
  ),
  'after_cover_thumbnails' => 
  array (
    0 => 'Block_127613644660551153e9bc96_48225994',
  ),
  'after_cover_thumbnails2' => 
  array (
    0 => 'Block_380463260551153e9c810_06981157',
  ),
  'page_header_container' => 
  array (
    0 => 'Block_65930335560551153e9d874_25508255',
  ),
  'product_brand_below' => 
  array (
    0 => 'Block_126375053460551153e9db68_96163067',
    1 => 'Block_143278750760551153ea0d20_78249784',
  ),
  'page_header' => 
  array (
    0 => 'Block_81196678460551153e9ffa3_52929810',
  ),
  'page_title' => 
  array (
    0 => 'Block_132954350860551153ea02a7_59049212',
  ),
  'product_description_short' => 
  array (
    0 => 'Block_197305426060551153ea6343_17646418',
  ),
  'hook_display_product_rating' => 
  array (
    0 => 'Block_112720502160551153ea7121_31931305',
  ),
  'product_prices' => 
  array (
    0 => 'Block_62481395960551153ea7eb5_99067187',
    1 => 'Block_10400346660551153eae463_25739252',
  ),
  'product_customization' => 
  array (
    0 => 'Block_37200965760551153ea9447_43732376',
  ),
  'product_buy' => 
  array (
    0 => 'Block_88498862360551153eaa148_90802993',
  ),
  'product_variants' => 
  array (
    0 => 'Block_10572475360551153eab156_78143534',
  ),
  'product_pack' => 
  array (
    0 => 'Block_156059974560551153eabcd1_90501053',
  ),
  'product_miniature' => 
  array (
    0 => 'Block_63394042460551153ead0e4_81684211',
    1 => 'Block_126284317660551153eb4617_57835602',
    2 => 'Block_36556082960551153eb6fa8_36257272',
  ),
  'product_add_to_cart' => 
  array (
    0 => 'Block_125503084260551153eaed60_97715513',
  ),
  'product_discounts' => 
  array (
    0 => 'Block_61337156860551153eaf4e2_06369346',
  ),
  'product_additional_info' => 
  array (
    0 => 'Block_69758010060551153eafc52_82128137',
  ),
  'product_refresh' => 
  array (
    0 => 'Block_106480047060551153eb0471_13404308',
  ),
  'hook_display_reassurance' => 
  array (
    0 => 'Block_192875595460551153eb0b47_21901147',
  ),
  'product_accessories_sidebar' => 
  array (
    0 => 'Block_103568423360551153eb33c7_42273834',
  ),
  'product_accessories_footer' => 
  array (
    0 => 'Block_108368235260551153eb60d5_21348948',
  ),
  'product_footer' => 
  array (
    0 => 'Block_94132952660551153eb80c8_97373970',
  ),
  'product_images_modal' => 
  array (
    0 => 'Block_164737090360551153eb8a51_76701401',
  ),
  'page_footer_container' => 
  array (
    0 => 'Block_179478018360551153eb91e4_48690687',
  ),
  'page_footer' => 
  array (
    0 => 'Block_159152969060551153eb94a2_04314832',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


    <section id="main" itemscope itemtype="https://schema.org/Product">

        <div id="product-preloader"><i class="fa fa-circle-o-notch fa-spin"></i></div>

        <div id="main-product-wrapper" class="product-container">

        <meta itemprop="url" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['url'], ENT_QUOTES, 'UTF-8');?>
">



        <?php if ($_smarty_tpl->tpl_vars['product']->value['upc']) {?>

        <meta itemprop="gtin12" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['upc'], ENT_QUOTES, 'UTF-8');?>
">

        <?php }?>



        <?php if ($_smarty_tpl->tpl_vars['product']->value['ean13']) {?>

        <meta itemprop="gtin13" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['ean13'], ENT_QUOTES, 'UTF-8');?>
">

        <?php }?>



        <?php if (!$_smarty_tpl->tpl_vars['product']->value['upc'] && !$_smarty_tpl->tpl_vars['product']->value['ean13']) {?>

            <meta itemprop="identifier_exist" content="no">

        <?php }?>

 



        <div class="row product-info-row">

            <div class="col-md-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['iqitTheme']->value['pp_img_width'], ENT_QUOTES, 'UTF-8');?>
 col-product-image">

                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_83154880560551153e98fe8_30641218', 'page_content_container', $this->tplIndex);
?>


            </div>



            <div class="col-md-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['iqitTheme']->value['pp_content_width'], ENT_QUOTES, 'UTF-8');?>
 col-product-info">

                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_65930335560551153e9d874_25508255', 'page_header_container', $this->tplIndex);
?>




                <div class="product-information">

                 



                    <?php if ($_smarty_tpl->tpl_vars['product']->value['is_customizable'] && count($_smarty_tpl->tpl_vars['product']->value['customizations']['fields'])) {?>

                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_37200965760551153ea9447_43732376', 'product_customization', $this->tplIndex);
?>


                    <?php }?>



                    <div class="product-actions">

                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_88498862360551153eaa148_90802993', 'product_buy', $this->tplIndex);
?>




                        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_192875595460551153eb0b47_21901147', 'hook_display_reassurance', $this->tplIndex);
?>




                    </div>

                </div>


 <?php if ($_smarty_tpl->tpl_vars['iqitTheme']->value['pp_tabs'] == 'tabh' || $_smarty_tpl->tpl_vars['iqitTheme']->value['pp_tabs'] == 'tabha') {?>

            <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/_product_partials/product-tabs-h.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        <?php } elseif ($_smarty_tpl->tpl_vars['iqitTheme']->value['pp_tabs'] == 'section') {?>

            <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/_product_partials/product-tabs-sections.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }?>

            </div>



            <?php if ($_smarty_tpl->tpl_vars['iqitTheme']->value['pp_sidebar']) {?>

            <div class="col-md-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['iqitTheme']->value['pp_sidebar'], ENT_QUOTES, 'UTF-8');?>
 sidebar product-sidebar">



                <?php if ($_smarty_tpl->tpl_vars['iqitTheme']->value['pp_accesories'] == 'sidebar') {?>

                    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_103568423360551153eb33c7_42273834', 'product_accessories_sidebar', $this->tplIndex);
?>


                <?php }?>



                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayRightColumnProduct'),$_smarty_tpl ) );?>




            </div>

            <?php }?>



        </div>



       


        </div>

        <?php if ($_smarty_tpl->tpl_vars['iqitTheme']->value['pp_accesories'] == 'footer') {?>

            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_108368235260551153eb60d5_21348948', 'product_accessories_footer', $this->tplIndex);
?>


        <?php }?>



        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_94132952660551153eb80c8_97373970', 'product_footer', $this->tplIndex);
?>




        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_164737090360551153eb8a51_76701401', 'product_images_modal', $this->tplIndex);
?>




        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_179478018360551153eb91e4_48690687', 'page_footer_container', $this->tplIndex);
?>




    </section>

<?php
}
}
/* {/block 'content'} */
}
