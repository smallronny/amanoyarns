<?php
/* Smarty version 3.1.33, created on 2021-01-18 16:36:33
  from '/home2/amanoyarns/public_html/store/themes/warehousechild/templates/page.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6005ff610ddd91_23408540',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e91807830c9e7250bd16d3fc81179d31281a4f72' => 
    array (
      0 => '/home2/amanoyarns/public_html/store/themes/warehousechild/templates/page.tpl',
      1 => 1597518116,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6005ff610ddd91_23408540 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6695233156005ff610dab19_90485714', 'content');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['layout']->value);
}
/* {block 'page_title'} */
class Block_4628181446005ff610db283_15188154 extends Smarty_Internal_Block
{
public $callsChild = 'true';
public $hide = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <header class="page-header">
            <h1 class="h1 page-title"><span><?php 
$_smarty_tpl->inheritance->callChild($_smarty_tpl, $this);
?>
</span></h1>
        </header>
      <?php
}
}
/* {/block 'page_title'} */
/* {block 'page_header_container'} */
class Block_16295301256005ff610dae84_66111458 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_4628181446005ff610db283_15188154', 'page_title', $this->tplIndex);
?>

    <?php
}
}
/* {/block 'page_header_container'} */
/* {block 'page_content_top'} */
class Block_12371087596005ff610dc6f2_35867406 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'page_content_top'} */
/* {block 'page_content'} */
class Block_2301227116005ff610dcb77_75036678 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <!-- Page content -->
        <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_12196732096005ff610dc3b4_00430286 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <section id="content" class="page-content">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12371087596005ff610dc6f2_35867406', 'page_content_top', $this->tplIndex);
?>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2301227116005ff610dcb77_75036678', 'page_content', $this->tplIndex);
?>

      </section>
    <?php
}
}
/* {/block 'page_content_container'} */
/* {block 'page_footer'} */
class Block_11296246836005ff610dd5a1_45243877 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <!-- Footer content -->
        <?php
}
}
/* {/block 'page_footer'} */
/* {block 'page_footer_container'} */
class Block_9092280006005ff610dd2a2_93165363 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <footer class="page-footer">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11296246836005ff610dd5a1_45243877', 'page_footer', $this->tplIndex);
?>

      </footer>
    <?php
}
}
/* {/block 'page_footer_container'} */
/* {block 'content'} */
class Block_6695233156005ff610dab19_90485714 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_6695233156005ff610dab19_90485714',
  ),
  'page_header_container' => 
  array (
    0 => 'Block_16295301256005ff610dae84_66111458',
  ),
  'page_title' => 
  array (
    0 => 'Block_4628181446005ff610db283_15188154',
  ),
  'page_content_container' => 
  array (
    0 => 'Block_12196732096005ff610dc3b4_00430286',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_12371087596005ff610dc6f2_35867406',
  ),
  'page_content' => 
  array (
    0 => 'Block_2301227116005ff610dcb77_75036678',
  ),
  'page_footer_container' => 
  array (
    0 => 'Block_9092280006005ff610dd2a2_93165363',
  ),
  'page_footer' => 
  array (
    0 => 'Block_11296246836005ff610dd5a1_45243877',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


  <section id="main">

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_16295301256005ff610dae84_66111458', 'page_header_container', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12196732096005ff610dc3b4_00430286', 'page_content_container', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9092280006005ff610dd2a2_93165363', 'page_footer_container', $this->tplIndex);
?>


  </section>

<?php
}
}
/* {/block 'content'} */
}
