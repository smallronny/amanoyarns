<?php
/* Smarty version 3.1.33, created on 2021-03-20 20:44:35
  from '/home2/amanoyarns/public_html/multitienda/themes/warehousechild/templates/customer/my-account.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_605696f3018086_30038144',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '285e035d4efd42453034b834878ee66d045dae2a' => 
    array (
      0 => '/home2/amanoyarns/public_html/multitienda/themes/warehousechild/templates/customer/my-account.tpl',
      1 => 1611278172,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605696f3018086_30038144 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1219205039605696f3016485_24391987', 'page_content');
?>



<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, 'customer/page.tpl');
}
/* {block 'page_content'} */
class Block_1219205039605696f3016485_24391987 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'page_content' => 
  array (
    0 => 'Block_1219205039605696f3016485_24391987',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

 <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'It is your account dashboard. Choose your section. ','d'=>'Shop.Theme.Customeraccount'),$_smarty_tpl ) );?>

 <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayMyAccountDashboard'),$_smarty_tpl ) );?>

<?php
}
}
/* {/block 'page_content'} */
}
