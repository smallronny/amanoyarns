<?php
/* Smarty version 3.1.33, created on 2021-03-20 20:44:35
  from '/home2/amanoyarns/public_html/multitienda/themes/warehousechild/templates/customer/page.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_605696f3033ec5_52203116',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ffc87b488d965168ecb39d0ef9405f463f90859d' => 
    array (
      0 => '/home2/amanoyarns/public_html/multitienda/themes/warehousechild/templates/customer/page.tpl',
      1 => 1611278172,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_partials/notifications.tpl' => 1,
    'file:customer/_partials/my-account-links.tpl' => 1,
  ),
),false)) {
function content_605696f3033ec5_52203116 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2125484095605696f301dff9_76380142', 'page_header_container');
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2104782929605696f301e907_88856358', 'notifications');
?>



<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1985180256605696f301ee39_34659211', 'page_content_container');
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_376139567605696f3033341_37892886', 'page_footer');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, 'page.tpl');
}
/* {block 'page_header_container'} */
class Block_2125484095605696f301dff9_76380142 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'page_header_container' => 
  array (
    0 => 'Block_2125484095605696f301dff9_76380142',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <header class="page-header">
      <h1 class="h1 page-title"><span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Your account','d'=>'Shop.Theme.Customeraccount'),$_smarty_tpl ) );?>
</span></h1>
    </header>
<?php
}
}
/* {/block 'page_header_container'} */
/* {block 'notifications'} */
class Block_2104782929605696f301e907_88856358 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'notifications' => 
  array (
    0 => 'Block_2104782929605696f301e907_88856358',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'notifications'} */
/* {block 'display_customer_account'} */
class Block_1591421548605696f3029462_37144816 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayCustomerAccount'),$_smarty_tpl ) );?>

          <?php
}
}
/* {/block 'display_customer_account'} */
/* {block 'my_account_side_links'} */
class Block_448791230605696f301f0f4_97095313 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php if ($_smarty_tpl->tpl_vars['customer']->value['is_logged']) {?>
      <div class="my-account-side-links col-sm-3">
          <a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="identity-link" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['identity'], ENT_QUOTES, 'UTF-8');?>
">
        <span class="link-item">
          <i class="fa fa-user fa-fw" aria-hidden="true"></i>
          <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Information','d'=>'Shop.Theme.Customeraccount'),$_smarty_tpl ) );?>

        </span>
          </a>

          <?php if (count($_smarty_tpl->tpl_vars['customer']->value['addresses'])) {?>
            <a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="addresses-link" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['addresses'], ENT_QUOTES, 'UTF-8');?>
">
          <span class="link-item">
            <i class="fa fa-map-marker fa-fw" aria-hidden="true"></i>
            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Addresses','d'=>'Shop.Theme.Customeraccount'),$_smarty_tpl ) );?>

          </span>
            </a>
          <?php } else { ?>
            <a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="address-link" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['address'], ENT_QUOTES, 'UTF-8');?>
">
          <span class="link-item">
            <i class="fa fa-map-marker fa-fw" aria-hidden="true"></i>
            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Add first address','d'=>'Shop.Theme.Customeraccount'),$_smarty_tpl ) );?>

          </span>
            </a>
          <?php }?>

          <?php if (!$_smarty_tpl->tpl_vars['configuration']->value['is_catalog']) {?>
            <a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="history-link" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['history'], ENT_QUOTES, 'UTF-8');?>
">
          <span class="link-item">
            <i class="fa fa-history fa-fw" aria-hidden="true"></i>
            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Order history and details','d'=>'Shop.Theme.Customeraccount'),$_smarty_tpl ) );?>

          </span>
            </a>
          <?php }?>
 
          <?php if (!$_smarty_tpl->tpl_vars['configuration']->value['is_catalog']) {?>
            <a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="order-slips-link" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['order_slip'], ENT_QUOTES, 'UTF-8');?>
">
          <span class="link-item">
            <i class="fa fa-file-o fa-fw" aria-hidden="true"></i>
            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Credit slips','d'=>'Shop.Theme.Customeraccount'),$_smarty_tpl ) );?>

          </span>
            </a>
          <?php }?>
 
          <?php if ($_smarty_tpl->tpl_vars['configuration']->value['voucher_enabled'] && !$_smarty_tpl->tpl_vars['configuration']->value['is_catalog']) {?>
            <a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="discounts-link" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['discount'], ENT_QUOTES, 'UTF-8');?>
">
          <span class="link-item">
            <i class="fa fa-tags fa-fw" aria-hidden="true"></i>
            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Vouchers','d'=>'Shop.Theme.Customeraccount'),$_smarty_tpl ) );?>

          </span>
            </a>
          <?php }?>

          <?php if ($_smarty_tpl->tpl_vars['configuration']->value['return_enabled'] && !$_smarty_tpl->tpl_vars['configuration']->value['is_catalog']) {?>
            <a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="returns-link" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['order_follow'], ENT_QUOTES, 'UTF-8');?>
">
          <span class="link-item">
            <i class="fa fa-undo fa-fw"" aria-hidden="true"></i>
            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Merchandise returns','d'=>'Shop.Theme.Customeraccount'),$_smarty_tpl ) );?>

          </span>
            </a>
          <?php }?>

          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1591421548605696f3029462_37144816', 'display_customer_account', $this->tplIndex);
?>


          <a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0], array( array('entity'=>'index','params'=>array('mylogout'=>'')),$_smarty_tpl ) );?>
">
          <span class="link-item">
            <i class="fa fa-sign-out fa-fw" aria-hidden="true"></i>
            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Sign out','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>

          </span>
          </a>
      </div>
      <?php }?>
    <?php
}
}
/* {/block 'my_account_side_links'} */
/* {block 'page_title'} */
class Block_408176357605696f302b315_37573290 extends Smarty_Internal_Block
{
public $callsChild = 'true';
public $hide = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <h2><?php 
$_smarty_tpl->inheritance->callChild($_smarty_tpl, $this);
?>
</h2>
      <?php
}
}
/* {/block 'page_title'} */
/* {block 'customer_notifications'} */
class Block_1986565653605696f3030154_44791492 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <?php $_smarty_tpl->_subTemplateRender('file:_partials/notifications.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php
}
}
/* {/block 'customer_notifications'} */
/* {block 'page_content_top'} */
class Block_866587793605696f302fe38_25439870 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1986565653605696f3030154_44791492', 'customer_notifications', $this->tplIndex);
?>

      <?php
}
}
/* {/block 'page_content_top'} */
/* {block 'page_content'} */
class Block_1408581877605696f3032be4_83672141 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

       <!-- Page content -->
      <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_1985180256605696f301ee39_34659211 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'page_content_container' => 
  array (
    0 => 'Block_1985180256605696f301ee39_34659211',
  ),
  'my_account_side_links' => 
  array (
    0 => 'Block_448791230605696f301f0f4_97095313',
  ),
  'display_customer_account' => 
  array (
    0 => 'Block_1591421548605696f3029462_37144816',
  ),
  'page_title' => 
  array (
    0 => 'Block_408176357605696f302b315_37573290',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_866587793605696f302fe38_25439870',
  ),
  'customer_notifications' => 
  array (
    0 => 'Block_1986565653605696f3030154_44791492',
  ),
  'page_content' => 
  array (
    0 => 'Block_1408581877605696f3032be4_83672141',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <section id="content" class="page-content my-account-page-content-wrapper">
    <div class="row">

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_448791230605696f301f0f4_97095313', 'my_account_side_links', $this->tplIndex);
?>



    <div class="my-account-page-content <?php if ($_smarty_tpl->tpl_vars['customer']->value['is_logged']) {?>col-sm-9<?php } else { ?>col<?php }?>">

      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_408176357605696f302b315_37573290', 'page_title', $this->tplIndex);
?>


      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_866587793605696f302fe38_25439870', 'page_content_top', $this->tplIndex);
?>


      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1408581877605696f3032be4_83672141', 'page_content', $this->tplIndex);
?>

    </div>

    </div>
  </section>

<?php
}
}
/* {/block 'page_content_container'} */
/* {block 'my_account_links'} */
class Block_1180246859605696f3033628_62108964 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php $_smarty_tpl->_subTemplateRender('file:customer/_partials/my-account-links.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
  <?php
}
}
/* {/block 'my_account_links'} */
/* {block 'page_footer'} */
class Block_376139567605696f3033341_37892886 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'page_footer' => 
  array (
    0 => 'Block_376139567605696f3033341_37892886',
  ),
  'my_account_links' => 
  array (
    0 => 'Block_1180246859605696f3033628_62108964',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1180246859605696f3033628_62108964', 'my_account_links', $this->tplIndex);
?>

<?php
}
}
/* {/block 'page_footer'} */
}
