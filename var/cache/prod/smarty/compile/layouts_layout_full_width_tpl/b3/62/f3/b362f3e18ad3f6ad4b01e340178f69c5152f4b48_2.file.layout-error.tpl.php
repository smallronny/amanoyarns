<?php
/* Smarty version 3.1.33, created on 2021-03-20 09:34:22
  from '/home2/amanoyarns/public_html/multitienda/themes/warehousechild/templates/layouts/layout-error.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6055f9dec84aa8_71266512',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b362f3e18ad3f6ad4b01e340178f69c5152f4b48' => 
    array (
      0 => '/home2/amanoyarns/public_html/multitienda/themes/warehousechild/templates/layouts/layout-error.tpl',
      1 => 1611278172,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_partials/stylesheets.tpl' => 1,
  ),
),false)) {
function content_6055f9dec84aa8_71266512 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>
<!doctype html>
<html lang="">

  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15475596556055f9dec78549_47028370', 'head_seo');
?>

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_14497066946055f9dec7b691_08544760', 'stylesheets');
?>


  </head>

  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_5145949836055f9dec837c4_70930086', 'body_tag');
?>


  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12472706586055f9dec83d02_11955804', 'layout_error_tag');
?>

      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_4639092926055f9dec841f6_67091869', 'content');
?>

    </div>

  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3039436106055f9dec846a2_59399805', 'maintenance_js');
?>


  </body>

</html>
<?php }
/* {block 'head_seo_title'} */
class Block_3203855406055f9dec78cf2_42181910 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'head_seo_title'} */
/* {block 'head_seo_description'} */
class Block_9299659126055f9dec79975_48821218 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'head_seo_description'} */
/* {block 'head_seo_keywords'} */
class Block_11297591076055f9dec7a449_74875693 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'head_seo_keywords'} */
/* {block 'head_seo'} */
class Block_15475596556055f9dec78549_47028370 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'head_seo' => 
  array (
    0 => 'Block_15475596556055f9dec78549_47028370',
  ),
  'head_seo_title' => 
  array (
    0 => 'Block_3203855406055f9dec78cf2_42181910',
  ),
  'head_seo_description' => 
  array (
    0 => 'Block_9299659126055f9dec79975_48821218',
  ),
  'head_seo_keywords' => 
  array (
    0 => 'Block_11297591076055f9dec7a449_74875693',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <title><?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3203855406055f9dec78cf2_42181910', 'head_seo_title', $this->tplIndex);
?>
</title>
      <meta name="description" content="<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9299659126055f9dec79975_48821218', 'head_seo_description', $this->tplIndex);
?>
">
      <meta name="keywords" content="<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11297591076055f9dec7a449_74875693', 'head_seo_keywords', $this->tplIndex);
?>
">
    <?php
}
}
/* {/block 'head_seo'} */
/* {block 'stylesheets'} */
class Block_14497066946055f9dec7b691_08544760 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'stylesheets' => 
  array (
    0 => 'Block_14497066946055f9dec7b691_08544760',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php $_smarty_tpl->_subTemplateRender("file:_partials/stylesheets.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('stylesheets'=>$_smarty_tpl->tpl_vars['stylesheets']->value), 0, false);
?>
    <?php
}
}
/* {/block 'stylesheets'} */
/* {block 'body_tag'} */
class Block_5145949836055f9dec837c4_70930086 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'body_tag' => 
  array (
    0 => 'Block_5145949836055f9dec837c4_70930086',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <body>
  <?php
}
}
/* {/block 'body_tag'} */
/* {block 'layout_error_tag'} */
class Block_12472706586055f9dec83d02_11955804 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'layout_error_tag' => 
  array (
    0 => 'Block_12472706586055f9dec83d02_11955804',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <div id="layout-error">
  <?php
}
}
/* {/block 'layout_error_tag'} */
/* {block 'content'} */
class Block_4639092926055f9dec841f6_67091869 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_4639092926055f9dec841f6_67091869',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <p>Hello world! This is HTML5 Boilerplate.</p>
      <?php
}
}
/* {/block 'content'} */
/* {block 'maintenance_js'} */
class Block_3039436106055f9dec846a2_59399805 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'maintenance_js' => 
  array (
    0 => 'Block_3039436106055f9dec846a2_59399805',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <?php
}
}
/* {/block 'maintenance_js'} */
}
