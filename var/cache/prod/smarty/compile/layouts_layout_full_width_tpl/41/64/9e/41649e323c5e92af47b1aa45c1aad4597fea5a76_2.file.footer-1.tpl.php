<?php
/* Smarty version 3.1.33, created on 2021-01-18 16:28:09
  from '/home2/amanoyarns/public_html/store/themes/warehousechild/templates/_partials/_variants/footer-1.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6005fd69038858_83082726',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '41649e323c5e92af47b1aa45c1aad4597fea5a76' => 
    array (
      0 => '/home2/amanoyarns/public_html/store/themes/warehousechild/templates/_partials/_variants/footer-1.tpl',
      1 => 1597518116,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6005fd69038858_83082726 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>




                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_616635786005fd69037149_82291504', 'newsletter_footer');
?>


<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, 'parent:_partials/_variants/footer-1.tpl');
}
/* {block 'newsletter_footer'} */
class Block_616635786005fd69037149_82291504 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'newsletter_footer' => 
  array (
    0 => 'Block_616635786005fd69037149_82291504',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


                <?php if ($_smarty_tpl->tpl_vars['iqitTheme']->value['f_newsletter_status'] == 1) {?>

                <div class="block block-footer block-toggle block-newsletter js-block-toggle t_footer">

                    <h3 class="text-white"><span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Suscribete','d'=>'Shop.Warehousetheme'),$_smarty_tpl ) );?>
</span></h3>
                    <p class="t_newsle  pt-3"> Donec neec justo eget felis facilisis fermentum aliquam portiton<p>

                    <div class="block-content pt-4">

                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['widget'][0], array( array('name'=>"ps_emailsubscription",'hook'=>'displayFooter'),$_smarty_tpl ) );?>


                    </div>

                </div>

                <?php }?>

<?php
}
}
/* {/block 'newsletter_footer'} */
}
