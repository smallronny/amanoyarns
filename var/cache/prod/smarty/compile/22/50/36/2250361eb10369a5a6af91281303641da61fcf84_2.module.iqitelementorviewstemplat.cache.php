<?php
/* Smarty version 3.1.33, created on 2021-03-19 17:37:37
  from 'module:iqitelementorviewstemplat' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_605519a12c8306_27097603',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2250361eb10369a5a6af91281303641da61fcf84' => 
    array (
      0 => 'module:iqitelementorviewstemplat',
      1 => 1611278170,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605519a12c8306_27097603 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->compiled->nocache_hash = '1805427839605519a12c58d5_66642632';
?>


<?php if ($_smarty_tpl->tpl_vars['options']->value['elementor']) {?>
    <?php echo $_smarty_tpl->tpl_vars['content']->value;?>

<?php } else { ?>
    <div class="rte-content"><?php echo $_smarty_tpl->tpl_vars['content']->value;?>
</div>
<?php }?>

<?php }
}
