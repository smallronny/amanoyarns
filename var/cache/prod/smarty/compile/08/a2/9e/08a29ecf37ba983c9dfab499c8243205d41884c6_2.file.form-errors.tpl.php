<?php
/* Smarty version 3.1.33, created on 2021-01-18 16:32:13
  from '/home2/amanoyarns/public_html/store/themes/warehousechild/templates/_partials/form-errors.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6005fe5dd2e8a8_10750662',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '08a29ecf37ba983c9dfab499c8243205d41884c6' => 
    array (
      0 => '/home2/amanoyarns/public_html/store/themes/warehousechild/templates/_partials/form-errors.tpl',
      1 => 1597518116,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6005fe5dd2e8a8_10750662 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
if (count($_smarty_tpl->tpl_vars['errors']->value)) {?>
  <div class="help-block">
    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_8256648096005fe5dd2cfc4_58941239', 'form_errors');
?>

  </div>
<?php }
}
/* {block 'form_errors'} */
class Block_8256648096005fe5dd2cfc4_58941239 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'form_errors' => 
  array (
    0 => 'Block_8256648096005fe5dd2cfc4_58941239',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <ul>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['errors']->value, 'error');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['error']->value) {
?>
          <li class="alert alert-danger"><?php echo nl2br($_smarty_tpl->tpl_vars['error']->value);?>
</li>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
      </ul>
    <?php
}
}
/* {/block 'form_errors'} */
}
