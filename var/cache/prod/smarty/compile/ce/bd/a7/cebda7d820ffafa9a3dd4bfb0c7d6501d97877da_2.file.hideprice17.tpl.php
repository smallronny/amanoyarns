<?php
/* Smarty version 3.1.33, created on 2021-01-18 16:32:13
  from '/home2/amanoyarns/public_html/store/modules/hideprice/views/templates/front/hideprice17.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6005fe5da64a01_65393688',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'cebda7d820ffafa9a3dd4bfb0c7d6501d97877da' => 
    array (
      0 => '/home2/amanoyarns/public_html/store/modules/hideprice/views/templates/front/hideprice17.tpl',
      1 => 1603902690,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6005fe5da64a01_65393688 (Smarty_Internal_Template $_smarty_tpl) {
?>

<?php if (isset($_smarty_tpl->tpl_vars['text']->value)) {?>
<div class="hidePriceText"><?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['text']->value,'quotes','UTF-8' ));?>
</div>
<?php }?>

<?php echo '<script'; ?>
 type="text/javascript">
<?php if (($_smarty_tpl->tpl_vars['remove_button']->value)) {?>
	remove_button = "<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['remove_button']->value,'quotes','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
";
<?php }?>

var waitForJQuery = setInterval(function () {
    if (typeof $ != 'undefined') {
		if ($('.product-information').length > 0) {
			if ($('.product-information .hidePriceText').length == 1) {
				$('.product-additional-info .hidePriceText').prependTo(".product-information");
			} else if ($('.product-information .hidePriceText').length > 1) {
				$('.product-additional-info .hidePriceText').hide();
			}
		}
		if (remove_button == 1) {
			$('.product-actions .product-add-to-cart').html('');
		}

        clearInterval(waitForJQuery);
    }
}, 10);


<?php echo '</script'; ?>
><?php }
}
