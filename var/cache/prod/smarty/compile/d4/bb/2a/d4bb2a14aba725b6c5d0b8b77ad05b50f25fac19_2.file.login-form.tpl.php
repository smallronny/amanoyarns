<?php
/* Smarty version 3.1.33, created on 2021-03-19 22:00:14
  from '/home2/amanoyarns/public_html/multitienda/themes/warehousechild/templates/checkout/_partials/login-form.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6055653e1cc766_98045900',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd4bb2a14aba725b6c5d0b8b77ad05b50f25fac19' => 
    array (
      0 => '/home2/amanoyarns/public_html/multitienda/themes/warehousechild/templates/checkout/_partials/login-form.tpl',
      1 => 1611278172,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6055653e1cc766_98045900 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13913827396055653e1c64d9_01002911', 'login_form_start');
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_5165737606055653e1ca6d4_83663942', 'form_buttons');
?>


  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1614241766055653e1cbe18_29827136', 'login_form_end');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, 'customer/_partials/login-form.tpl');
}
/* {block 'login_form_start'} */
class Block_13913827396055653e1c64d9_01002911 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'login_form_start' => 
  array (
    0 => 'Block_13913827396055653e1c64d9_01002911',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="collapse <?php if (count($_smarty_tpl->tpl_vars['errors']->value[''])) {?>show<?php }?>" id="personal-information-step-login">
<?php
}
}
/* {/block 'login_form_start'} */
/* {block 'form_buttons'} */
class Block_5165737606055653e1ca6d4_83663942 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'form_buttons' => 
  array (
    0 => 'Block_5165737606055653e1ca6d4_83663942',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <button
    class="continue btn btn-primary btn-block btn-lg"
    name="continue"
    data-link-action="sign-in"
    type="submit"
    value="1"
  >
    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Sign in','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>

  </button>
<?php
}
}
/* {/block 'form_buttons'} */
/* {block 'login_form_end'} */
class Block_1614241766055653e1cbe18_29827136 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'login_form_end' => 
  array (
    0 => 'Block_1614241766055653e1cbe18_29827136',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

</div>
    <?php
}
}
/* {/block 'login_form_end'} */
}
