<?php
/* Smarty version 3.1.33, created on 2021-03-19 22:21:24
  from 'module:prestablogviewstemplatesf' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_60555c240af028_61863640',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '90e7d97ecd24623b373b667b8cd587831f18fe32' => 
    array (
      0 => 'module:prestablogviewstemplatesf',
      1 => 1611278170,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60555c240af028_61863640 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- Module Presta Blog -->
<?php if ($_smarty_tpl->tpl_vars['prestablog_categorie_obj']->value->image_presente && $_smarty_tpl->tpl_vars['prestablog_config']->value['prestablog_view_cat_img']) {?>
<img class="prestablog_cat_img" src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['prestablog_theme_upimg']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
c/full_<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['prestablog_categorie_obj']->value->id), ENT_QUOTES, 'UTF-8');?>
.jpg?<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['md5pic']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['prestablog_categorie_obj']->value->title,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" />
<?php }
if ($_smarty_tpl->tpl_vars['prestablog_categorie_obj']->value->image_presente && $_smarty_tpl->tpl_vars['prestablog_config']->value['prestablog_view_cat_thumb']) {?>
<img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['prestablog_theme_upimg']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
c/thumb_<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['prestablog_categorie_obj']->value->id), ENT_QUOTES, 'UTF-8');?>
.jpg?<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['md5pic']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['prestablog_categorie_obj']->value->title,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" class="prestablog_thumb_cat"/>
<?php }
if (isset($_smarty_tpl->tpl_vars['prestablog_categorie_obj']->value->description) && $_smarty_tpl->tpl_vars['prestablog_config']->value['prestablog_view_cat_desc']) {?>
<p class="cat_desc_blog" itemprop="description" ><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogContent'][0], array( array('return'=>$_smarty_tpl->tpl_vars['prestablog_categorie_obj']->value->descri),$_smarty_tpl ) );?>
</p>
<?php }?>
<div class="clearfix"></div>
<!-- /Module Presta Blog -->
<?php }
}
