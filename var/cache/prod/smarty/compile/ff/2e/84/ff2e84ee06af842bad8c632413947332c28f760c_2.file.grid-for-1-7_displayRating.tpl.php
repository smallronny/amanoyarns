<?php
/* Smarty version 3.1.33, created on 2021-03-19 16:04:14
  from '/home2/amanoyarns/public_html/multitienda/modules/prestablog/views/templates/hook/grid-for-1-7_displayRating.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_605511ce97a7b4_49276840',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ff2e84ee06af842bad8c632413947332c28f760c' => 
    array (
      0 => '/home2/amanoyarns/public_html/multitienda/modules/prestablog/views/templates/hook/grid-for-1-7_displayRating.tpl',
      1 => 1611278170,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605511ce97a7b4_49276840 (Smarty_Internal_Template $_smarty_tpl) {
?><!-- Module Presta Blog -->
<div id="prestablog-rating">
<?php if (($_smarty_tpl->tpl_vars['isLogged']->value)) {?>
	<?php ob_start();
echo htmlspecialchars($_smarty_tpl->tpl_vars['validate']->value, ENT_QUOTES, 'UTF-8');
$_prefixVariable2 = ob_get_clean();
ob_start();
echo htmlspecialchars($_smarty_tpl->tpl_vars['validate']->value, ENT_QUOTES, 'UTF-8');
$_prefixVariable3 = ob_get_clean();
if (($_prefixVariable2) && ($_prefixVariable3 == 'true')) {?>
		<form action="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['LinkReal']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
&id=<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['news']->value->id), ENT_QUOTES, 'UTF-8');?>
" method="post" class="rating">
 	<input type="radio" id="star5" name="rate" value="5" /><label class = "material-icons" for="star5" title="5 stars">star</label>
    <input type="radio" id="star4" name="rate" value="4" /><label class = "material-icons" for="star4" title="4 stars">star</label>
    <input type="radio" id="star3" name="rate" value="3" /><label class = "material-icons" for="star3" title="3 stars">star</label>
    <input type="radio" id="star2" name="rate" value="2" /><label class = "material-icons" for="star2" title="2 stars">star</label>
    <input type="radio" id="star1" name="rate" value="1" /><label class = "material-icons" for="star1" title="1 star">star</label>
				<p class="submit">
					<input type="submit" class="btn-primary" name="submitRating" id="submitRating" value="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Rate the article','mod'=>'prestablog'),$_smarty_tpl ) );?>
" />
				</p>
</form>
<?php } else { ?>
<div id="prestablogfont">
  <p>
<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'You have already rated this article','mod'=>'prestablog'),$_smarty_tpl ) );?>

</p>
</div>
<?php }
} else { ?>
<div id="prestablogfont">
  <p>
<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Please log in to rate this article','mod'=>'prestablog'),$_smarty_tpl ) );?>

</p>
</div>
<?php }?>
</div>
  <div class="clearfix" style="margin-bottom:30px;"></div>

<!-- /Module Presta Blog -->
<?php }
}
