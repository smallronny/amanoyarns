<?php
/* Smarty version 3.1.33, created on 2021-03-19 16:02:29
  from 'module:iqitfreedeliverycountview' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_605511658948f9_43266016',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6eace4bf156fb3b043cf7ba194e6e011e54e9e3e' => 
    array (
      0 => 'module:iqitfreedeliverycountview',
      1 => 1611278170,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605511658948f9_43266016 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_874401030605511658927f3_05711019', 'iqitfreedeliverycount_product');
?>


<?php }
/* {block 'iqitfreedeliverycount_product'} */
class Block_874401030605511658927f3_05711019 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'iqitfreedeliverycount_product' => 
  array (
    0 => 'Block_874401030605511658927f3_05711019',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="iqitfreedeliverycount iqitfreedeliverycount-product clearfix <?php if ($_smarty_tpl->tpl_vars['hide']->value) {?>iqitfreedeliverycount-hidden<?php }?>">
<div class="fd-table">
<div class="ifdc-icon fd-table-cell"><i class="icon icon-truck"></i></div>

<div class="ifdc-remaining  fd-table-cell"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Spend','mod'=>'iqitfreedeliverycount'),$_smarty_tpl ) );?>
 <span class="ifdc-remaining-price"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['free_ship_remaining']->value, ENT_QUOTES, 'UTF-8');?>
</span> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'more and get Free Shipping!','mod'=>'iqitfreedeliverycount'),$_smarty_tpl ) );?>
</div></div>
<?php if (isset($_smarty_tpl->tpl_vars['txt']->value) && $_smarty_tpl->tpl_vars['txt']->value != '') {?><div class="ifdc-txt"><div class="ifdc-txt-content"><?php echo $_smarty_tpl->tpl_vars['txt']->value;?>
</div></div><?php }?> </div>
<?php
}
}
/* {/block 'iqitfreedeliverycount_product'} */
}
