<?php
/* Smarty version 3.1.33, created on 2021-01-18 16:32:13
  from 'module:iqitcookielawviewstemplat' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6005fe5dd77516_56975043',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b7588a77287e3c02dfc5dfe6fd3a17abf03f7e2e' => 
    array (
      0 => 'module:iqitcookielawviewstemplat',
      1 => 1598462746,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6005fe5dd77516_56975043 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->compiled->nocache_hash = '11314709186005fe5dd75de1_66589392';
?>



<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9276314176005fe5dd76439_20903540', 'iqitcookielaw');
?>




 

<?php }
/* {block 'iqitcookielaw'} */
class Block_9276314176005fe5dd76439_20903540 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'iqitcookielaw' => 
  array (
    0 => 'Block_9276314176005fe5dd76439_20903540',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


<div id="iqitcookielaw" class="p-3">

    <div class="row justify-content-center p-4"> 

             <div class="col-md-8 col-12 align-self-center">

<?php echo $_smarty_tpl->tpl_vars['txt']->value;?>


 </div>

          <div class="col-md-2 col-6 align-self-center text-center">



<button class="btn btn-block btn-cok" id="iqitcookielaw-accept"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Accept','mod'=>'iqitcookielaw'),$_smarty_tpl ) );?>
</button>

  </div>

          <div class="col-md-2 col-6 align-self-center text-center">

<a href="/content/11-privacy-policy-and-cookies" id="iqitcookielaw-read" class="btn-cook btn btn-block btn-primary"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Read More','mod'=>'iqitcookielaw'),$_smarty_tpl ) );?>
</a>

  </div>

           </div>

</div>

<?php
}
}
/* {/block 'iqitcookielaw'} */
}
