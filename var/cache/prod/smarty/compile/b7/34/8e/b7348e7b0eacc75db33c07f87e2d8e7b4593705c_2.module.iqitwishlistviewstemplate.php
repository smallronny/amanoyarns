<?php
/* Smarty version 3.1.33, created on 2021-03-20 20:44:35
  from 'module:iqitwishlistviewstemplate' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_605696f3081b01_81595254',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b7348e7b0eacc75db33c07f87e2d8e7b4593705c' => 
    array (
      0 => 'module:iqitwishlistviewstemplate',
      1 => 1611278170,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605696f3081b01_81595254 (Smarty_Internal_Template $_smarty_tpl) {
?>
<a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="iqitwishlist"
   href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0], array( array('entity'=>'module','name'=>'iqitwishlist','controller'=>'view'),$_smarty_tpl ) );?>
" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'My wishlist','mod'=>'iqitwishlist'),$_smarty_tpl ) );?>
">
  <span class="link-item">
  <i class="fa fa-heart-o fa-fw" aria-hidden="true"></i>
    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'My wishlist','mod'=>'iqitwishlist'),$_smarty_tpl ) );?>

  </span>
</a><?php }
}
