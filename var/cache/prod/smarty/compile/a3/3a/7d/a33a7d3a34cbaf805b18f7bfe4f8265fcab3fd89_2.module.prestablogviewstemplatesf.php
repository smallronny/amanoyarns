<?php
/* Smarty version 3.1.33, created on 2021-03-19 17:24:30
  from 'module:prestablogviewstemplatesf' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6055249ecc6da7_36370963',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a33a7d3a34cbaf805b18f7bfe4f8265fcab3fd89' => 
    array (
      0 => 'module:prestablogviewstemplatesf',
      1 => 1611278170,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6055249ecc6da7_36370963 (Smarty_Internal_Template $_smarty_tpl) {
?>   <!-- Module Presta Blog -->
   <div id="prestablogauthor">
    <img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['prestablog_author_upimg']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');
echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['author_id']->value), ENT_QUOTES, 'UTF-8');?>
.jpg" class="author" alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['firstname']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"/>

    <h1 id="prestablog_pseudo" data-referenceid="<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['author_id']->value), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['pseudo']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</h1>
<div id="prestablogfont">
  <p itemprop="text"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['PrestaBlogContent'][0], array( array('return'=>$_smarty_tpl->tpl_vars['biography']->value),$_smarty_tpl ) );?>
</p>
</div>

</div>

<div id="blog_article_linked">
    <h2><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'From the same author','mod'=>'prestablog'),$_smarty_tpl ) );?>
</h2>
    <?php if ((sizeof($_smarty_tpl->tpl_vars['articles_author']->value))) {?>
    <ul id="blog_list_1-7">
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['articles_author']->value, 'article', false, 'key', 'current', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['article']->value) {
?>
        <?php if ($_smarty_tpl->tpl_vars['article']->value['title']) {?>
        <li class="blog-grid">
            <div class="block_cont">
                <div class="block_top">
                    <?php if ($_smarty_tpl->tpl_vars['article']->value['image_presente']) {?>
                    <a href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['article']->value['link'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['article']->value['title'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
                        <img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['prestablog_theme_upimg']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
thumb_<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['key']->value), ENT_QUOTES, 'UTF-8');?>
.jpg?<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['md5pic']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['article']->value['title'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['article']->value['title'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"/>
                        <?php }?>
                    </a>
                </div>
                <div class="block_bas">

                    <h3>
                        <?php if (isset($_smarty_tpl->tpl_vars['article']->value['link'])) {?><a href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['article']->value['link'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['article']->value['title'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"><?php }
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['article']->value['title'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
if (isset($_smarty_tpl->tpl_vars['article']->value['link'])) {?></a><?php }?>
                        <br /><span class="date_blog-cat"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Published :','mod'=>'prestablog'),$_smarty_tpl ) );?>

                            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['dateFormat'][0], array( array('date'=>$_smarty_tpl->tpl_vars['article']->value['date'],'full'=>0),$_smarty_tpl ) );?>


                        </span>
                    </h3>
                </div>
                <?php if (isset($_smarty_tpl->tpl_vars['article']->value['link'])) {?>
                <div class="prestablog_more">
                    <a href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['article']->value['link'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" class="blog_link"><i class="material-icons">search</i> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Read more','mod'=>'prestablog'),$_smarty_tpl ) );?>
</a>
                </div><?php }?>
            </div>
        </li>
        <?php }?>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
    </ul>
    <?php }?>
</div>
<?php }
}
