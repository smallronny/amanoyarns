<?php
/* Smarty version 3.1.33, created on 2021-03-21 10:42:29
  from '/home2/amanoyarns/public_html/multitienda/modules/lgcookieslaw/views/templates/front/account_button_17.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_605769658b85e3_05290691',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '57c43a10b184f5a208666f7bb0efc1157262f3a3' => 
    array (
      0 => '/home2/amanoyarns/public_html/multitienda/modules/lgcookieslaw/views/templates/front/account_button_17.tpl',
      1 => 1613758969,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_605769658b85e3_05290691 (Smarty_Internal_Template $_smarty_tpl) {
?><a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="lgcookieslaw-link" href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['lgcookieslaw_disallow_url']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Revoke my consent to cookies','mod'=>'lgcookieslaw'),$_smarty_tpl ) );?>
">
    <span class="link-item">
        <img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['lgcookieslaw_image_path']->value,'quotes','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" style="padding: 10px; float: left;">
        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Revoke my consent to cookies','mod'=>'lgcookieslaw'),$_smarty_tpl ) );?>

    </span>
</a>
<?php }
}
