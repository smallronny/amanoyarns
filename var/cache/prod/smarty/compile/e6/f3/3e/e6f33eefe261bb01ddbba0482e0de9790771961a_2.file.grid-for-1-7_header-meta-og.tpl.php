<?php
/* Smarty version 3.1.33, created on 2021-01-18 20:31:00
  from '/home2/amanoyarns/public_html/store/modules/prestablog/views/templates/hook/grid-for-1-7_header-meta-og.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_600636541c3193_14198075',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e6f33eefe261bb01ddbba0482e0de9790771961a' => 
    array (
      0 => '/home2/amanoyarns/public_html/store/modules/prestablog/views/templates/hook/grid-for-1-7_header-meta-og.tpl',
      1 => 1599067362,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_600636541c3193_14198075 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- Module Presta Blog -->
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['prestablog_fb_admins']->value, 'fbmoderator');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['fbmoderator']->value) {
?>
<meta property="fb:admins"       content="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['fbmoderator']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" />
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
if ($_smarty_tpl->tpl_vars['prestablog_fb_appid']->value) {?>
<meta property="fb:app_id"       content="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['prestablog_fb_appid']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" />
<?php }?>
<meta property="og:url"          content="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['prestablog_news_meta_url']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" />
<meta property="og:image"        content="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['prestablog_news_meta_img']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" />
<meta property="og:title"        content="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['prestablog_news_meta']->value->title,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" />
<meta property="og:description"  content="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['prestablog_news_meta']->value->paragraph,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" />
<?php if ($_smarty_tpl->tpl_vars['prestablog_config']->value['prestablog_captcha_actif'] == 1) {
echo '<script'; ?>
 src='https://www.google.com/recaptcha/api.js'><?php echo '</script'; ?>
>
<?php }?>
<!-- Module Presta Blog -->

<?php }
}
