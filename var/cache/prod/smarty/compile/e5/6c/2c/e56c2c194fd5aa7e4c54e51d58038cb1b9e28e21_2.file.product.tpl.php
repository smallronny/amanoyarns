<?php
/* Smarty version 3.1.33, created on 2021-01-19 15:14:18
  from '/home2/amanoyarns/public_html/store/themes/warehouse/modules/ps_emailalerts/views/templates/hook/product.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_60073d9aaddfe4_14084203',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e56c2c194fd5aa7e4c54e51d58038cb1b9e28e21' => 
    array (
      0 => '/home2/amanoyarns/public_html/store/themes/warehouse/modules/ps_emailalerts/views/templates/hook/product.tpl',
      1 => 1597521714,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60073d9aaddfe4_14084203 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="js-mailalert form-inline"
     data-url="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0], array( array('entity'=>'module','name'=>'ps_emailalerts','controller'=>'actions','params'=>array('process'=>'add')),$_smarty_tpl ) );?>
">
    <div class="input-group mr-2 mb-2">
        <?php if (isset($_smarty_tpl->tpl_vars['email']->value) && $_smarty_tpl->tpl_vars['email']->value) {?>
            <input type="email" placeholder="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'your@email.com','d'=>'Modules.Mailalerts.Shop'),$_smarty_tpl ) );?>
" class="form-control"/>
            <br/>
        <?php }?>
    </div>
    <div class="input-group mr-2 mb-2">
        <input type="hidden" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id_product']->value, ENT_QUOTES, 'UTF-8');?>
"/>
        <input type="hidden" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id_product_attribute']->value, ENT_QUOTES, 'UTF-8');?>
"/>
        <a href="#" class="btn btn-secondary" rel="nofollow"
           onclick="return addNotification();"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Notify me when available','d'=>'Modules.Mailalerts.Shop'),$_smarty_tpl ) );?>
</a>

    </div>
    <span class="alert alert-info" style="display:none;"></span>
</div>
<?php }
}
