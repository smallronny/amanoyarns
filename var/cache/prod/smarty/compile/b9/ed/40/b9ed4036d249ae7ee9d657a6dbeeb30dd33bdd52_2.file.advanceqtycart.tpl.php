<?php
/* Smarty version 3.1.33, created on 2021-01-18 20:08:30
  from '/home2/amanoyarns/public_html/store/modules/addifyadvanceqty/views/templates/hook/advanceqtycart.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6006310e6d1067_70702873',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b9ed4036d249ae7ee9d657a6dbeeb30dd33bdd52' => 
    array (
      0 => '/home2/amanoyarns/public_html/store/modules/addifyadvanceqty/views/templates/hook/advanceqtycart.tpl',
      1 => 1601166677,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6006310e6d1067_70702873 (Smarty_Internal_Template $_smarty_tpl) {
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['products']->value, 'product');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['product']->value) {
?>

<div class="addifyadvancereplaceboxcart " id="addifyadvancereplacebox-<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['product']->value['id_product'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" numbers="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['product']->value['numbers'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" changeqtybox="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['product']->value['changeqtybox'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" style="display: none; !important;" >
<div class="addifyadvancereplacebox">
    <?php if (isset($_smarty_tpl->tpl_vars['product']->value['quantity_type'])) {?>
     <div type="number" name="qty" readonly="" id="quantity_wanted_addify" value="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['product']->value['cart_quantity'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" class="input-group form-control js-cart-line-product-quantity-addify" minlength="5" maxlength="5" aria-label="Quantity"   style="display: block !important;"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['product']->value['cart_quantity'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</div>
    
    <?php }?>
</div>
</div>
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
  
    


<?php }
}
