<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__f82d5a15938ae66613644f18b8eaf1c67a9968b927f2ab5e06d2ab6d9309f86d */
class __TwigTemplate_aafc3321f7f1d4be7c210b9eac095412d9ce79195f42ea4af020bab2829db640 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'stylesheets' => [$this, 'block_stylesheets'],
            'extra_stylesheets' => [$this, 'block_extra_stylesheets'],
            'content_header' => [$this, 'block_content_header'],
            'content' => [$this, 'block_content'],
            'content_footer' => [$this, 'block_content_footer'],
            'sidebar_right' => [$this, 'block_sidebar_right'],
            'javascripts' => [$this, 'block_javascripts'],
            'extra_javascripts' => [$this, 'block_extra_javascripts'],
            'translate_javascripts' => [$this, 'block_translate_javascripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"es\">
<head>
  <meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
<meta name=\"apple-mobile-web-app-capable\" content=\"yes\">
<meta name=\"robots\" content=\"NOFOLLOW, NOINDEX\">

<link rel=\"icon\" type=\"image/x-icon\" href=\"/img/favicon.ico\" />
<link rel=\"apple-touch-icon\" href=\"/img/app_icon.png\" />

<title>Idiomas • Amano Yarns</title>

  <script type=\"text/javascript\">
    var help_class_name = 'AdminLanguages';
    var iso_user = 'es';
    var lang_is_rtl = '0';
    var full_language_code = 'es';
    var full_cldr_language_code = 'es-ES';
    var country_iso_code = 'PE';
    var _PS_VERSION_ = '1.7.6.7';
    var roundMode = 2;
    var youEditFieldFor = '';
        var new_order_msg = 'Se ha recibido un nuevo pedido en tu tienda.';
    var order_number_msg = 'Número de pedido: ';
    var total_msg = 'Total: ';
    var from_msg = 'Desde: ';
    var see_order_msg = 'Ver este pedido';
    var new_customer_msg = 'Un nuevo cliente se ha registrado en tu tienda.';
    var customer_name_msg = 'Nombre del cliente: ';
    var new_msg = 'Un nuevo mensaje ha sido publicado en tu tienda.';
    var see_msg = 'Leer este mensaje';
    var token = '2f3be254e8dd09c749af687ae53cdf3d';
    var token_admin_orders = '84f375eb87814384d5310a0f443fc5db';
    var token_admin_customers = 'b0b4ecbcfc46a4c25442154db31f8581';
    var token_admin_customer_threads = 'c45c59e27c586cd7ed863edd985df8b1';
    var currentIndex = 'index.php?controller=AdminLanguages';
    var employee_token = '59abe705e0f30fa61fa616525a1eab09';
    var choose_language_translate = 'Selecciona el idioma';
    var default_language = '3';
    var admin_modules_link = '/multitienda/admin518ddw65d/index.php/improve/modules/catalog/recommended?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU';
    var admin_notification_get_link = '/multitienda/admin518ddw65d/index.php/common/notifications?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU';
    var admin_notification_push_link = '/multitienda/admin518ddw65d/index.php/common/notifications/ack?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU';
    var tab_modules_list = '';
    var update_success_msg = 'Actualización correcta';
    var errorLogin = 'PrestaShop no pudo iniciar sesión en Addons. Por favor verifica tus datos de acceso y tu conexión de Internet.';
    var search_product_msg = 'Buscar un producto';
  </script>

      <link href=\"/modules/iqitelementor/views/css/backoffice.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/admin518ddw65d/themes/new-theme/public/theme.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/modules/prestablog/views/css/prestablog-back-office.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/modules/prestablog/views/css/admin.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/modules/ets_cfultimate/views/css/contact_form7_admin_all.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/modules/customfields/views/css/admin.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/js/jquery/plugins/chosen/jquery.chosen.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/admin518ddw65d/themes/default/css/vendor/nv.d3.css\" rel=\"stylesheet\" type=\"text/css\"/>
  
  <script type=\"text/javascript\">
var baseAdminDir = \"\\/admin518ddw65d\\/\";
var baseDir = \"\\/\";
var changeFormLanguageUrl = \"\\/multitienda\\/admin518ddw65d\\/index.php\\/configure\\/advanced\\/employees\\/change-form-language?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\";
var currency = {\"iso_code\":\"PEN\",\"sign\":\"PEN\",\"name\":\"Sol peruano\",\"format\":null};
var currency_specifications = {\"symbol\":[\",\",\".\",\";\",\"%\",\"-\",\"+\",\"E\",\"\\u00d7\",\"\\u2030\",\"\\u221e\",\"NaN\"],\"currencyCode\":\"PEN\",\"currencySymbol\":\"PEN\",\"positivePattern\":\"#,##0.00\\u00a0\\u00a4\",\"negativePattern\":\"-#,##0.00\\u00a0\\u00a4\",\"maxFractionDigits\":2,\"minFractionDigits\":2,\"groupingUsed\":true,\"primaryGroupSize\":3,\"secondaryGroupSize\":3};
var host_mode = false;
var iqitModulesAdditionalTabs = {\"ajaxUrl\":\"https:\\/\\/amanoyarns.com\\/admin518ddw65d\\/index.php?controller=AdminModules&token=e4c9d346c43bf3e98747485f72dc6dec&ajax=1&configure=iqitadditionaltabs\"};
var iqitModulesSizeCharts = {\"ajaxUrl\":\"https:\\/\\/amanoyarns.com\\/admin518ddw65d\\/index.php?controller=AdminModules&token=e4c9d346c43bf3e98747485f72dc6dec&ajax=1&configure=iqitsizecharts\"};
var number_specifications = {\"symbol\":[\",\",\".\",\";\",\"%\",\"-\",\"+\",\"E\",\"\\u00d7\",\"\\u2030\",\"\\u221e\",\"NaN\"],\"positivePattern\":\"#,##0.###\",\"negativePattern\":\"-#,##0.###\",\"maxFractionDigits\":3,\"minFractionDigits\":0,\"groupingUsed\":true,\"primaryGroupSize\":3,\"secondaryGroupSize\":3};
var show_new_customers = \"1\";
var show_new_messages = false;
var show_new_orders = \"1\";
</script>
<script type=\"text/javascript\" src=\"/modules/ps_faviconnotificationbo/views/js/favico.js\"></script>
<script type=\"text/javascript\" src=\"/modules/ps_faviconnotificationbo/views/js/ps_faviconnotificationbo.js\"></script>
<script type=\"text/javascript\" src=\"/admin518ddw65d/themes/new-theme/public/main.bundle.js\"></script>
<script type=\"text/javascript\" src=\"/js/jquery/plugins/jquery.chosen.js\"></script>
<script type=\"text/javascript\" src=\"/js/admin.js?v=1.7.6.7\"></script>
<script type=\"text/javascript\" src=\"/admin518ddw65d/themes/new-theme/public/cldr.bundle.js\"></script>
<script type=\"text/javascript\" src=\"/js/tools.js?v=1.7.6.7\"></script>
<script type=\"text/javascript\" src=\"/admin518ddw65d/public/bundle.js\"></script>
<script type=\"text/javascript\" src=\"/js/vendor/d3.v3.min.js\"></script>
<script type=\"text/javascript\" src=\"/admin518ddw65d/themes/default/js/vendor/nv.d3.min.js\"></script>

  <script>
  if (undefined !== ps_faviconnotificationbo) {
    ps_faviconnotificationbo.initialize({
      backgroundColor: '#DF0067',
      textColor: '#FFFFFF',
      notificationGetUrl: '/multitienda/admin518ddw65d/index.php/common/notifications?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU',
      CHECKBOX_ORDER: 1,
      CHECKBOX_CUSTOMER: 1,
      CHECKBOX_MESSAGE: 1,
      timer: 120000, // Refresh every 2 minutes
    });
  }
</script>


";
        // line 99
        $this->displayBlock('stylesheets', $context, $blocks);
        $this->displayBlock('extra_stylesheets', $context, $blocks);
        echo "</head>

<body class=\"lang-es adminlanguages\">

  <header id=\"header\">

    <nav id=\"header_infos\" class=\"main-header\">
      <button class=\"btn btn-primary-reverse onclick btn-lg unbind ajax-spinner\"></button>

            <i class=\"material-icons js-mobile-menu\">menu</i>
      <a id=\"header_logo\" class=\"logo float-left\" href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminDashboard&amp;token=432bf165c629bbbe0e5039375072e220\"></a>
      <span id=\"shop_version\">1.7.6.7</span>

      <div class=\"component\" id=\"quick-access-container\">
        <div class=\"dropdown quick-accesses\">
  <button class=\"btn btn-link btn-sm dropdown-toggle\" type=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" id=\"quick_select\">
    Acceso rápido
  </button>
  <div class=\"dropdown-menu\">
          <a class=\"dropdown-item\"
         href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminStats&amp;module=statscheckup&amp;token=0f42ec7a35bd55d130a718143cd9b99a\"
                 data-item=\"Evaluación del catálogo\"
      >Evaluación del catálogo</a>
          <a class=\"dropdown-item\"
         href=\"https://amanoyarns.com/admin518ddw65d/index.php/improve/modules/manage?token=f20c6b9d85e35de7f3d0cafe82acc83b\"
                 data-item=\"Módulos instalados\"
      >Módulos instalados</a>
          <a class=\"dropdown-item\"
         href=\"https://amanoyarns.com/admin518ddw65d/index.php/sell/catalog/categories/new?token=f20c6b9d85e35de7f3d0cafe82acc83b\"
                 data-item=\"Nueva categoría\"
      >Nueva categoría</a>
          <a class=\"dropdown-item\"
         href=\"https://amanoyarns.com/admin518ddw65d/index.php/sell/catalog/products/new?token=f20c6b9d85e35de7f3d0cafe82acc83b\"
                 data-item=\"Nuevo\"
      >Nuevo</a>
          <a class=\"dropdown-item\"
         href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminCartRules&amp;addcart_rule&amp;token=c6be553dbb26c192a8e062f4ba74a954\"
                 data-item=\"Nuevo cupón de descuento\"
      >Nuevo cupón de descuento</a>
          <a class=\"dropdown-item\"
         href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminOrders&amp;token=84f375eb87814384d5310a0f443fc5db\"
                 data-item=\"Pedidos\"
      >Pedidos</a>
          <a class=\"dropdown-item\"
         href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminModules&amp;configure=prestablog&amp;module_name=prestablog&amp;token=e4c9d346c43bf3e98747485f72dc6dec\"
                 data-item=\"PrestaBlog\"
      >PrestaBlog</a>
        <div class=\"dropdown-divider\"></div>
          <a
        class=\"dropdown-item js-quick-link\"
        href=\"#\"
        data-rand=\"193\"
        data-icon=\"icon-AdminParentLocalization\"
        data-method=\"add\"
        data-url=\"index.php/multitienda//improve/international/languages/?-HHgbAIAf_0v_YCBJHBU\"
        data-post-link=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminQuickAccesses&token=3032522f4efff97e8048c0280d131535\"
        data-prompt-text=\"Por favor, renombre este acceso rápido:\"
        data-link=\"Idiomas - Lista\"
      >
        <i class=\"material-icons\">add_circle</i>
        Añadir esta página a Acceso rápido
      </a>
        <a class=\"dropdown-item\" href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminQuickAccesses&token=3032522f4efff97e8048c0280d131535\">
      <i class=\"material-icons\">settings</i>
      Administrar accesos rápidos
    </a>
  </div>
</div>
      </div>
      <div class=\"component\" id=\"header-search-container\">
        <form id=\"header_search\"
      class=\"bo_search_form dropdown-form js-dropdown-form collapsed\"
      method=\"post\"
      action=\"/admin518ddw65d/index.php?controller=AdminSearch&amp;token=6bcc0c8886762e54e7b3276a1131569b\"
      role=\"search\">
  <input type=\"hidden\" name=\"bo_search_type\" id=\"bo_search_type\" class=\"js-search-type\" />
    <div class=\"input-group\">
    <input type=\"text\" class=\"form-control js-form-search\" id=\"bo_query\" name=\"bo_query\" value=\"\" placeholder=\"Buscar (p. ej.: referencia de producto, nombre de cliente...)\">
    <div class=\"input-group-append\">
      <button type=\"button\" class=\"btn btn-outline-secondary dropdown-toggle js-dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
        toda la tienda
      </button>
      <div class=\"dropdown-menu js-items-list\">
        <a class=\"dropdown-item\" data-item=\"toda la tienda\" href=\"#\" data-value=\"0\" data-placeholder=\"¿Qué estás buscando?\" data-icon=\"icon-search\"><i class=\"material-icons\">search</i> toda la tienda</a>
        <div class=\"dropdown-divider\"></div>
        <a class=\"dropdown-item\" data-item=\"Catálogo\" href=\"#\" data-value=\"1\" data-placeholder=\"Nombre del producto, SKU, referencia...\" data-icon=\"icon-book\"><i class=\"material-icons\">store_mall_directory</i> Catálogo</a>
        <a class=\"dropdown-item\" data-item=\"Clientes por nombre\" href=\"#\" data-value=\"2\" data-placeholder=\"Email, nombre...\" data-icon=\"icon-group\"><i class=\"material-icons\">group</i> Clientes por nombre</a>
        <a class=\"dropdown-item\" data-item=\"Clientes por dirección IP\" href=\"#\" data-value=\"6\" data-placeholder=\"123.45.67.89\" data-icon=\"icon-desktop\"><i class=\"material-icons\">desktop_mac</i> Clientes por dirección IP</a>
        <a class=\"dropdown-item\" data-item=\"Pedidos\" href=\"#\" data-value=\"3\" data-placeholder=\"ID del pedido\" data-icon=\"icon-credit-card\"><i class=\"material-icons\">shopping_basket</i> Pedidos</a>
        <a class=\"dropdown-item\" data-item=\"Facturas\" href=\"#\" data-value=\"4\" data-placeholder=\"Número de factura\" data-icon=\"icon-book\"><i class=\"material-icons\">book</i> Facturas</a>
        <a class=\"dropdown-item\" data-item=\"Carritos\" href=\"#\" data-value=\"5\" data-placeholder=\"ID carrito\" data-icon=\"icon-shopping-cart\"><i class=\"material-icons\">shopping_cart</i> Carritos</a>
        <a class=\"dropdown-item\" data-item=\"Módulos\" href=\"#\" data-value=\"7\" data-placeholder=\"Nombre del módulo\" data-icon=\"icon-puzzle-piece\"><i class=\"material-icons\">extension</i> Módulos</a>
      </div>
      <button class=\"btn btn-primary\" type=\"submit\"><span class=\"d-none\">BÚSQUEDA</span><i class=\"material-icons\">search</i></button>
    </div>
  </div>
</form>

<script type=\"text/javascript\">
 \$(document).ready(function(){
    \$('#bo_query').one('click', function() {
    \$(this).closest('form').removeClass('collapsed');
  });
});
</script>
      </div>

      
      
      <div class=\"component\" id=\"header-shop-list-container\">
          <div id=\"shop-list\" class=\"shop-list dropdown ps-dropdown stores\">
    <button class=\"btn btn-link\" type=\"button\" data-toggle=\"dropdown\">
      <span class=\"selected-item\">
        <i class=\"material-icons visibility\">visibility</i>
                  All shops
                <i class=\"material-icons arrow-down\">arrow_drop_down</i>
      </span>
    </button>
    <div class=\"dropdown-menu dropdown-menu-right ps-dropdown-menu\">
      <ul class=\"items-list\"><li class=\"active\"><a class=\"dropdown-item\" href=\"/multitienda/admin518ddw65d/index.php/improve/international/languages/?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU&amp;setShopContext=\">Todas las tiendas</a></li></ul></ul>

    </div>
  </div>
      </div>

              <div class=\"component header-right-component\" id=\"header-notifications-container\">
          <div id=\"notif\" class=\"notification-center dropdown dropdown-clickable\">
  <button class=\"btn notification js-notification dropdown-toggle\" data-toggle=\"dropdown\">
    <i class=\"material-icons\">notifications_none</i>
    <span id=\"notifications-total\" class=\"count hide\">0</span>
  </button>
  <div class=\"dropdown-menu dropdown-menu-right js-notifs_dropdown\">
    <div class=\"notifications\">
      <ul class=\"nav nav-tabs\" role=\"tablist\">
                          <li class=\"nav-item\">
            <a
              class=\"nav-link active\"
              id=\"orders-tab\"
              data-toggle=\"tab\"
              data-type=\"order\"
              href=\"#orders-notifications\"
              role=\"tab\"
            >
              Pedidos<span id=\"_nb_new_orders_\"></span>
            </a>
          </li>
                                    <li class=\"nav-item\">
            <a
              class=\"nav-link \"
              id=\"customers-tab\"
              data-toggle=\"tab\"
              data-type=\"customer\"
              href=\"#customers-notifications\"
              role=\"tab\"
            >
              Clientes<span id=\"_nb_new_customers_\"></span>
            </a>
          </li>
                                    <li class=\"nav-item\">
            <a
              class=\"nav-link \"
              id=\"messages-tab\"
              data-toggle=\"tab\"
              data-type=\"customer_message\"
              href=\"#messages-notifications\"
              role=\"tab\"
            >
              Mensajes<span id=\"_nb_new_messages_\"></span>
            </a>
          </li>
                        </ul>

      <!-- Tab panes -->
      <div class=\"tab-content\">
                          <div class=\"tab-pane active empty\" id=\"orders-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              No hay pedidos nuevos por ahora :(<br>
              ¿Has revisado tus <strong><a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminCarts&action=filterOnlyAbandonedCarts&token=36ab3fdb83e759f2ff9dbc1158642b18\">carritos abandonados</a></strong>?<br>?. ¡Tu próximo pedido podría estar ocultándose allí!
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                                    <div class=\"tab-pane  empty\" id=\"customers-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              No hay clientes nuevos por ahora :(<br>
              ¿Ha enviado algún correo electrónico de adquisición últimamente?
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                                    <div class=\"tab-pane  empty\" id=\"messages-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              No hay mensajes nuevo por ahora.<br>
              Que no haya noticias, es de por sí una buena noticia, ¿verdad?
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                        </div>
    </div>
  </div>
</div>

  <script type=\"text/html\" id=\"order-notification-template\">
    <a class=\"notif\" href='order_url'>
      #_id_order_ -
      de <strong>_customer_name_</strong> (_iso_code_)_carrier_
      <strong class=\"float-sm-right\">_total_paid_</strong>
    </a>
  </script>

  <script type=\"text/html\" id=\"customer-notification-template\">
    <a class=\"notif\" href='customer_url'>
      #_id_customer_ - <strong>_customer_name_</strong>_company_ - registrado <strong>_date_add_</strong>
    </a>
  </script>

  <script type=\"text/html\" id=\"message-notification-template\">
    <a class=\"notif\" href='message_url'>
    <span class=\"message-notification-status _status_\">
      <i class=\"material-icons\">fiber_manual_record</i> _status_
    </span>
      - <strong>_customer_name_</strong> (_company_) - <i class=\"material-icons\">access_time</i> _date_add_
    </a>
  </script>
        </div>
      
      <div class=\"component\" id=\"header-employee-container\">
        <div class=\"dropdown employee-dropdown\">
  <div class=\"rounded-circle person\" data-toggle=\"dropdown\">
    <i class=\"material-icons\">account_circle</i>
  </div>
  <div class=\"dropdown-menu dropdown-menu-right\">
    <div class=\"employee-wrapper-avatar\">
      
      <span class=\"employee_avatar\"><img class=\"avatar rounded-circle\" src=\"https://profile.prestashop.com/desarrollador1%40turnoverweb.com.jpg\" /></span>
      <span class=\"employee_profile\">Bienvenido de nuevo, Developer</span>
      <a class=\"dropdown-item employee-link profile-link\" href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/employees/2/edit?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\">
      <i class=\"material-icons\">settings</i>
      Tu perfil
    </a>
    </div>
    
    <p class=\"divider\"></p>
    <a class=\"dropdown-item\" href=\"https://www.prestashop.com/en/resources/documentations?utm_source=back-office&amp;utm_medium=profile&amp;utm_campaign=resources-en&amp;utm_content=download17\" target=\"_blank\"><i class=\"material-icons\">book</i> Recursos</a>
    <a class=\"dropdown-item\" href=\"https://www.prestashop.com/en/training?utm_source=back-office&amp;utm_medium=profile&amp;utm_campaign=training-en&amp;utm_content=download17\" target=\"_blank\"><i class=\"material-icons\">school</i> Formación</a>
    <a class=\"dropdown-item\" href=\"https://www.prestashop.com/en/experts?utm_source=back-office&amp;utm_medium=profile&amp;utm_campaign=expert-en&amp;utm_content=download17\" target=\"_blank\"><i class=\"material-icons\">person_pin_circle</i> Encontrar un Experto</a>
    <a class=\"dropdown-item\" href=\"https://addons.prestashop.com?utm_source=back-office&amp;utm_medium=profile&amp;utm_campaign=addons-en&amp;utm_content=download17\" target=\"_blank\"><i class=\"material-icons\">extension</i> Marketplace de PrestaShop</a>
    <a class=\"dropdown-item\" href=\"https://www.prestashop.com/en/contact?utm_source=back-office&amp;utm_medium=profile&amp;utm_campaign=help-center-en&amp;utm_content=download17\" target=\"_blank\"><i class=\"material-icons\">help</i> Centro de ayuda</a>
    <p class=\"divider\"></p>
    <a class=\"dropdown-item employee-link text-center\" id=\"header_logout\" href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminLogin&amp;logout=1&amp;token=8ff17ee4fba7abf58503a95c4e820ea7\">
      <i class=\"material-icons d-lg-none\">power_settings_new</i>
      <span>Cerrar sesión</span>
    </a>
  </div>
</div>
      </div>
    </nav>

      </header>

  <nav class=\"nav-bar d-none d-md-block\">
  <span class=\"menu-collapse\" data-toggle-url=\"/multitienda/admin518ddw65d/index.php/configure/advanced/employees/toggle-navigation?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\">
    <i class=\"material-icons\">chevron_left</i>
    <i class=\"material-icons\">chevron_left</i>
  </span>

  <ul class=\"main-menu\">

          
                
                
        
          <li class=\"link-levelone \" data-submenu=\"1\" id=\"tab-AdminDashboard\">
            <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminDashboard&amp;token=432bf165c629bbbe0e5039375072e220\" class=\"link\" >
              <i class=\"material-icons\">trending_up</i> <span>Inicio</span>
            </a>
          </li>

        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"2\" id=\"tab-SELL\">
              <span class=\"title\">Vender</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"3\" id=\"subtab-AdminParentOrders\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminOrders&amp;token=84f375eb87814384d5310a0f443fc5db\" class=\"link\">
                    <i class=\"material-icons mi-shopping_basket\">shopping_basket</i>
                    <span>
                    Pedidos
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-3\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"4\" id=\"subtab-AdminOrders\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminOrders&amp;token=84f375eb87814384d5310a0f443fc5db\" class=\"link\"> Pedidos
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"5\" id=\"subtab-AdminInvoices\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/sell/orders/invoices/?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\"> Facturas
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"6\" id=\"subtab-AdminSlip\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminSlip&amp;token=d54acc94e2fe31867629beddc2eb206b\" class=\"link\"> Facturas por abono
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"7\" id=\"subtab-AdminDeliverySlip\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/sell/orders/delivery-slips/?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\"> Albaranes de entrega
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"8\" id=\"subtab-AdminCarts\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminCarts&amp;token=36ab3fdb83e759f2ff9dbc1158642b18\" class=\"link\"> Carritos de compra
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"192\" id=\"subtab-deleteorderstab\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=deleteorderstab&amp;token=1bf7af182e9ea1deaac6fa4b505341bf\" class=\"link\"> Delete Orders Free
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"9\" id=\"subtab-AdminCatalog\">
                  <a href=\"/multitienda/admin518ddw65d/index.php/sell/catalog/products?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\">
                    <i class=\"material-icons mi-store\">store</i>
                    <span>
                    Catálogo
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-9\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"10\" id=\"subtab-AdminProducts\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/sell/catalog/products?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\"> Productos
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"11\" id=\"subtab-AdminCategories\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/sell/catalog/categories?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\"> Categorías
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"12\" id=\"subtab-AdminTracking\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminTracking&amp;token=70019ee7e97da38c9df98bc4f7b95a1f\" class=\"link\"> Monitoreo
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"13\" id=\"subtab-AdminParentAttributesGroups\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminAttributesGroups&amp;token=29fd84808e942aaeff6ac95bef3ee36d\" class=\"link\"> Atributos y Características
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"16\" id=\"subtab-AdminParentManufacturers\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/sell/catalog/brands/?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\"> Marcas y Proveedores
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"19\" id=\"subtab-AdminAttachments\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminAttachments&amp;token=a5d4ddd81cecf63e10f11afb64a143a0\" class=\"link\"> Archivos
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"20\" id=\"subtab-AdminParentCartRules\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminCartRules&amp;token=c6be553dbb26c192a8e062f4ba74a954\" class=\"link\"> Descuentos
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"23\" id=\"subtab-AdminStockManagement\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/sell/stocks/?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\"> Stocks
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"24\" id=\"subtab-AdminParentCustomer\">
                  <a href=\"/multitienda/admin518ddw65d/index.php/sell/customers/?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\">
                    <i class=\"material-icons mi-account_circle\">account_circle</i>
                    <span>
                    Clientes
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-24\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"25\" id=\"subtab-AdminCustomers\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/sell/customers/?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\"> Clientes
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"26\" id=\"subtab-AdminAddresses\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminAddresses&amp;token=c2bb2ab3696985b2ca56b7a84f0b85bb\" class=\"link\"> Direcciones
                              </a>
                            </li>

                                                                                                                          </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"28\" id=\"subtab-AdminParentCustomerThreads\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminCustomerThreads&amp;token=c45c59e27c586cd7ed863edd985df8b1\" class=\"link\">
                    <i class=\"material-icons mi-chat\">chat</i>
                    <span>
                    Servicio al Cliente
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-28\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"29\" id=\"subtab-AdminCustomerThreads\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminCustomerThreads&amp;token=c45c59e27c586cd7ed863edd985df8b1\" class=\"link\"> Servicio al Cliente
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"30\" id=\"subtab-AdminOrderMessage\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminOrderMessage&amp;token=8bacce8aa470dfb02300c7d57dccd8d2\" class=\"link\"> Mensajes de Pedidos
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"31\" id=\"subtab-AdminReturn\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminReturn&amp;token=78fb9afa3e48b093060a8f8feed86c46\" class=\"link\"> Devoluciones de mercancía
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"32\" id=\"subtab-AdminStats\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminStats&amp;token=0f42ec7a35bd55d130a718143cd9b99a\" class=\"link\">
                    <i class=\"material-icons mi-assessment\">assessment</i>
                    <span>
                    Estadísticas
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title -active\" data-submenu=\"42\" id=\"tab-IMPROVE\">
              <span class=\"title\">Personalizar</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"43\" id=\"subtab-AdminParentModulesSf\">
                  <a href=\"/multitienda/admin518ddw65d/index.php/improve/modules/manage?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Módulos
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-43\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"44\" id=\"subtab-AdminModulesSf\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/improve/modules/manage?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\"> Module Manager
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"48\" id=\"subtab-AdminParentModulesCatalog\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/improve/modules/catalog?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\"> Catálogo de Módulos
                              </a>
                            </li>

                                                                                                                              
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"190\" id=\"subtab-AdminGeoIpRedirect\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminGeoIpRedirect&amp;token=2c0ecda5f7d69393d5a64731b5a7eb95\" class=\"link\"> Geo Ip Redirect
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"52\" id=\"subtab-AdminParentThemes\">
                  <a href=\"/multitienda/admin518ddw65d/index.php/improve/design/themes/?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\">
                    <i class=\"material-icons mi-desktop_mac\">desktop_mac</i>
                    <span>
                    Diseño
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-52\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"126\" id=\"subtab-AdminThemesParent\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/improve/design/themes/?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\"> Tema y logotipo
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"54\" id=\"subtab-AdminThemesCatalog\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/improve/design/themes-catalog/?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\"> Catálogo de Temas
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"55\" id=\"subtab-AdminParentMailTheme\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/improve/design/mail_theme/?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\"> Tema Email
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"57\" id=\"subtab-AdminCmsContent\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/improve/design/cms-pages/?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\"> Páginas
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"58\" id=\"subtab-AdminModulesPositions\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/improve/design/modules/positions/?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\"> Posiciones
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"59\" id=\"subtab-AdminImages\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminImages&amp;token=ee7cf1d2a18be609b54560e57762e51d\" class=\"link\"> Ajustes de imágenes
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"125\" id=\"subtab-AdminLinkWidget\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/modules/link-widget/list?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\"> Link Widget
                              </a>
                            </li>

                                                                                                                              
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"139\" id=\"subtab-AdminIqitElementor\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminIqitElementor&amp;token=92bd49f01a73c60ffbe897b9014d3c69\" class=\"link\"> Iqit Elementor - Page builder
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"142\" id=\"subtab-IqitFrontThemeEditor\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=IqitFrontThemeEditor&amp;token=1d65d9475c478392a8cba5e6f09889df\" class=\"link\"> IqitThemeEditor - Live
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"143\" id=\"subtab-AdminIqitThemeEditor\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminIqitThemeEditor&amp;token=d09386c4dc2348b42c949d8749d3698e\" class=\"link\"> IqitThemeEditor - Backoffice
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"60\" id=\"subtab-AdminParentShipping\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminCarriers&amp;token=56f2300adaa08eb6338ff1b0a732061f\" class=\"link\">
                    <i class=\"material-icons mi-local_shipping\">local_shipping</i>
                    <span>
                    Transporte
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-60\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"61\" id=\"subtab-AdminCarriers\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminCarriers&amp;token=56f2300adaa08eb6338ff1b0a732061f\" class=\"link\"> Transportistas
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"62\" id=\"subtab-AdminShipping\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/improve/shipping/preferences?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\"> Preferencias
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"63\" id=\"subtab-AdminParentPayment\">
                  <a href=\"/multitienda/admin518ddw65d/index.php/improve/payment/payment_methods?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\">
                    <i class=\"material-icons mi-payment\">payment</i>
                    <span>
                    Pago
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-63\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"64\" id=\"subtab-AdminPayment\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/improve/payment/payment_methods?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\"> Métodos de pago
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"65\" id=\"subtab-AdminPaymentPreferences\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/improve/payment/preferences?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\"> Preferencias
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                                                    
                <li class=\"link-levelone has_submenu -active open ul-open\" data-submenu=\"66\" id=\"subtab-AdminInternational\">
                  <a href=\"/multitienda/admin518ddw65d/index.php/improve/international/localization/?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\">
                    <i class=\"material-icons mi-language\">language</i>
                    <span>
                    Internacional
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_up
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-66\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo -active\" data-submenu=\"67\" id=\"subtab-AdminParentLocalization\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/improve/international/localization/?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\"> Localización
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"72\" id=\"subtab-AdminParentCountries\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminZones&amp;token=1eacfaf2204db80f7362897edd80e574\" class=\"link\"> Ubicaciones Geográficas
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"76\" id=\"subtab-AdminParentTaxes\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/improve/international/taxes/?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\"> Impuestos
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"79\" id=\"subtab-AdminTranslations\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/improve/international/translations/settings?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\"> Traducciones
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"80\" id=\"tab-CONFIGURE\">
              <span class=\"title\">Configurar</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"81\" id=\"subtab-ShopParameters\">
                  <a href=\"/multitienda/admin518ddw65d/index.php/configure/shop/preferences/preferences?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\">
                    <i class=\"material-icons mi-settings\">settings</i>
                    <span>
                    Parámetros de la tienda
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-81\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"82\" id=\"subtab-AdminParentPreferences\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/shop/preferences/preferences?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\"> Configuración
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"85\" id=\"subtab-AdminParentOrderPreferences\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/shop/order-preferences/?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\"> Configuración de Pedidos
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"88\" id=\"subtab-AdminPPreferences\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/shop/product-preferences/?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\"> Configuración de Productos
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"89\" id=\"subtab-AdminParentCustomerPreferences\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/shop/customer-preferences/?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\"> Ajustes sobre clientes
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"93\" id=\"subtab-AdminParentStores\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/shop/contacts/?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\"> Contacto
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"96\" id=\"subtab-AdminParentMeta\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/shop/seo-urls/?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\"> Tráfico &amp; SEO
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"100\" id=\"subtab-AdminParentSearchConf\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminSearchConf&amp;token=054b1b636cef719fbd9e15b1b1400267\" class=\"link\"> Buscar
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"173\" id=\"subtab-AdminGeorules\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminGeorules&amp;token=04097df4dfe53256aaccde144f37ec0b\" class=\"link\"> Geolocation rules
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"103\" id=\"subtab-AdminAdvancedParameters\">
                  <a href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/system-information/?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\">
                    <i class=\"material-icons mi-settings_applications\">settings_applications</i>
                    <span>
                    Parámetros Avanzados
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-103\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"104\" id=\"subtab-AdminInformation\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/system-information/?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\"> Información
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"105\" id=\"subtab-AdminPerformance\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/performance/?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\"> Rendimiento
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"106\" id=\"subtab-AdminAdminPreferences\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/administration/?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\"> Administración
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"107\" id=\"subtab-AdminEmails\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/emails/?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\"> Dirección de correo electrónico
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"108\" id=\"subtab-AdminImport\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/import/?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\"> Importar
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"109\" id=\"subtab-AdminParentEmployees\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/employees/?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\"> Equipo
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"113\" id=\"subtab-AdminParentRequestSql\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/sql-requests/?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\"> Base de datos
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"116\" id=\"subtab-AdminLogs\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/logs/?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\"> Registros/Logs
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"117\" id=\"subtab-AdminWebservice\">
                              <a href=\"/multitienda/admin518ddw65d/index.php/configure/advanced/webservice-keys/?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" class=\"link\"> Webservice
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"118\" id=\"subtab-AdminShopGroup\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminShopGroup&amp;token=eab2f4d1f6924064ac49f803144d42ce\" class=\"link\"> Multitienda
                              </a>
                            </li>

                                                                                                                              
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"151\" id=\"subtab-AdminExportProducts\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminExportProducts&amp;token=dc077c75720a8928f270755950a957f7\" class=\"link\"> Export Products
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"191\" id=\"subtab-AdminKlaviyoPsConfig\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminKlaviyoPsConfig&amp;token=8e537cf7f306eacdb141ef3b816a4b6e\" class=\"link\">
                    <i class=\"material-icons mi-trending_up\">trending_up</i>
                    <span>
                    Klaviyo
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"130\" id=\"tab-AdminRevslider\">
              <span class=\"title\">Revolution Slider</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"131\" id=\"subtab-AdminRevsliderSliders\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminRevsliderSliders&amp;token=37fddf2f7547eb2e17c0d8269f26389d\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Sliders
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"132\" id=\"subtab-AdminRevolutionsliderGlobalSettings\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminRevolutionsliderGlobalSettings&amp;token=477800944ee7a1741940e7193bf94859\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Global Settings
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"133\" id=\"subtab-AdminRevolutionsliderAddons\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminRevolutionsliderAddons&amp;token=239e082745112777515de40c699f8fe8\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Addons
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"134\" id=\"subtab-AdminRevolutionsliderNavigation\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminRevolutionsliderNavigation&amp;token=ca8c342ea8b241e978ab07b74524e0bd\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Navigation
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"157\" id=\"tab-Management\">
              <span class=\"title\">CONTENT MANAGEMENT</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"158\" id=\"subtab-AdminPrestaBlog\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminPrestaBlog&amp;token=55dcebbf705ff42bf5aeb0bd028970af\" class=\"link\">
                    <i class=\"material-icons mi-library_books\">library_books</i>
                    <span>
                    PrestaBlog
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"171\" id=\"tab-AdminParentAddifyadvanceqty\">
              <span class=\"title\">Addify Advance Quantity</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"172\" id=\"subtab-AdminAddifyadvanceqty\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminAddifyadvanceqty&amp;token=1a107828e6f4ede357537ee0b8484bef\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Manage Advance Quantity
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"174\" id=\"tab-AdminContactFormUltimate\">
              <span class=\"title\">Contacto</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"175\" id=\"subtab-AdminContactFormUltimateDashboard\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminContactFormUltimateDashboard&amp;token=a76ca4d7e0e8ccb109ebddce4e49a389\" class=\"link\">
                    <i class=\"material-icons mi-icon icon-home\">icon icon-home</i>
                    <span>
                    Contact dashboard
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"176\" id=\"subtab-AdminContactFormUltimateContactForm\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminContactFormUltimateContactForm&amp;token=e1e4f9cd627361f16acfa60027e8cefa\" class=\"link\">
                    <i class=\"material-icons mi-icon icon-envelope-o\">icon icon-envelope-o</i>
                    <span>
                    Contact forms
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"177\" id=\"subtab-AdminContactFormUltimateMessage\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminContactFormUltimateMessage&amp;token=ae93d01fa7dac27b95ec28799cd400aa\" class=\"link\">
                    <i class=\"material-icons mi-icon icon-comments\">icon icon-comments</i>
                    <span>
                    Messages
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"178\" id=\"subtab-AdminContactFormUltimateStatistics\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminContactFormUltimateStatistics&amp;token=c2985476de36ffb97603cb8079e9ab72\" class=\"link\">
                    <i class=\"material-icons mi-icon icon-line-chart\">icon icon-line-chart</i>
                    <span>
                    Statistics
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"179\" id=\"subtab-AdminContactFormUltimateIpBlacklist\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminContactFormUltimateIpBlacklist&amp;token=65d1fd16f541b6a9a8ae22a3f2d480d3\" class=\"link\">
                    <i class=\"material-icons mi-icon icon-user-times\">icon icon-user-times</i>
                    <span>
                    IP and Email blacklist
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"180\" id=\"subtab-AdminContactFormUltimateSetting\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminContactFormUltimateEmail&amp;token=65a32454857e18e50486f17c1776855c\" class=\"link\">
                    <i class=\"material-icons mi-icon icon-cog\">icon icon-cog</i>
                    <span>
                    Setting
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-180\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"181\" id=\"subtab-AdminContactFormUltimateEmail\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminContactFormUltimateEmail&amp;token=65a32454857e18e50486f17c1776855c\" class=\"link\"> Email templates
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"182\" id=\"subtab-AdminContactFormUltimateImportExport\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminContactFormUltimateImportExport&amp;token=233a68dfb76e1ac375fcb0558f8d48eb\" class=\"link\"> Import/Export
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"183\" id=\"subtab-AdminContactFormUltimateIntegration\">
                              <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminContactFormUltimateIntegration&amp;token=5968493ec9e8b34973c8a2fba3d23009\" class=\"link\"> Integration
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"188\" id=\"tab-AdminCustomFields\">
              <span class=\"title\">Custom Fields</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"189\" id=\"subtab-AdminFields\">
                  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminFields&amp;token=3feecfbb8a90038dda4c60e45abccbd7\" class=\"link\">
                    <i class=\"material-icons mi-content_paste\">content_paste</i>
                    <span>
                    Manage Custom Fields
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                          
        
            </ul>
  
</nav>

<div id=\"main-div\">
          
<div class=\"header-toolbar\">
  <div class=\"container-fluid\">

    
      <nav aria-label=\"Breadcrumb\">
        <ol class=\"breadcrumb\">
                      <li class=\"breadcrumb-item\">Localización</li>
          
                      <li class=\"breadcrumb-item active\">
              <a href=\"/multitienda/admin518ddw65d/index.php/improve/international/languages/?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" aria-current=\"page\">Idiomas</a>
            </li>
                  </ol>
      </nav>
    

    <div class=\"title-row\">
      
          <h1 class=\"title\">
            Idiomas          </h1>
      

      
        <div class=\"toolbar-icons\">
          <div class=\"wrapper\">
            
                                                          <a
                  class=\"btn btn-primary  pointer\"                  id=\"page-header-desc-configuration-add\"
                  href=\"/multitienda/admin518ddw65d/index.php/improve/international/languages/new?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\"                  title=\"Añadir nuevo idioma\"                >
                  <i class=\"material-icons\">add_circle_outline</i>                  Añadir nuevo idioma
                </a>
                                      
            
                              <a class=\"btn btn-outline-secondary btn-help btn-sidebar\" href=\"#\"
                   title=\"Ayuda\"
                   data-toggle=\"sidebar\"
                   data-target=\"#right-sidebar\"
                   data-url=\"/multitienda/admin518ddw65d/index.php/common/sidebar/https%253A%252F%252Fhelp.prestashop.com%252Fes%252Fdoc%252FAdminLanguages%253Fversion%253D1.7.6.7%2526country%253Des/Ayuda?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\"
                   id=\"product_form_open_help\"
                >
                  Ayuda
                </a>
                                    </div>
        </div>
      
    </div>
  </div>

  
      <div class=\"page-head-tabs\" id=\"head_tabs\">
      <ul class=\"nav nav-pills\">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <li class=\"nav-item\">
                    <a href=\"/multitienda/admin518ddw65d/index.php/improve/international/localization/?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" id=\"subtab-AdminLocalization\" class=\"nav-link tab \" data-submenu=\"68\">
                      Localización
                      <span class=\"notification-container\">
                        <span class=\"notification-counter\"></span>
                      </span>
                    </a>
                  </li>
                                                                <li class=\"nav-item\">
                    <a href=\"/multitienda/admin518ddw65d/index.php/improve/international/languages/?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" id=\"subtab-AdminLanguages\" class=\"nav-link tab active current\" data-submenu=\"69\">
                      Idiomas
                      <span class=\"notification-container\">
                        <span class=\"notification-counter\"></span>
                      </span>
                    </a>
                  </li>
                                                                <li class=\"nav-item\">
                    <a href=\"/multitienda/admin518ddw65d/index.php/improve/international/currencies/?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" id=\"subtab-AdminCurrencies\" class=\"nav-link tab \" data-submenu=\"70\">
                      Monedas
                      <span class=\"notification-container\">
                        <span class=\"notification-counter\"></span>
                      </span>
                    </a>
                  </li>
                                                                <li class=\"nav-item\">
                    <a href=\"/multitienda/admin518ddw65d/index.php/improve/international/geolocation/?_token=v4hfrmi5ZG2LCyTllSnPisP-HHgbAIAf_0v_YCBJHBU\" id=\"subtab-AdminGeolocation\" class=\"nav-link tab \" data-submenu=\"71\">
                      Geolocalización
                      <span class=\"notification-container\">
                        <span class=\"notification-counter\"></span>
                      </span>
                    </a>
                  </li>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        </ul>
    </div>
    
</div>
      
      <div class=\"content-div  with-tabs\">

        

                                                        
        <div class=\"row \">
          <div class=\"col-sm-12\">
            <div id=\"ajax_confirmation\" class=\"alert alert-success\" style=\"display: none;\"></div>


  ";
        // line 1469
        $this->displayBlock('content_header', $context, $blocks);
        // line 1470
        echo "                 ";
        $this->displayBlock('content', $context, $blocks);
        // line 1471
        echo "                 ";
        $this->displayBlock('content_footer', $context, $blocks);
        // line 1472
        echo "                 ";
        $this->displayBlock('sidebar_right', $context, $blocks);
        // line 1473
        echo "
            
          </div>
        </div>

      </div>
    </div>

  <div id=\"non-responsive\" class=\"js-non-responsive\">
  <h1>¡Oh no!</h1>
  <p class=\"mt-3\">
    La versión para móviles de esta página no está disponible todavía.
  </p>
  <p class=\"mt-2\">
    Por favor, utiliza un ordenador de escritorio hasta que esta página sea adaptada para dispositivos móviles.
  </p>
  <p class=\"mt-2\">
    Gracias.
  </p>
  <a href=\"https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminDashboard&amp;token=432bf165c629bbbe0e5039375072e220\" class=\"btn btn-primary py-1 mt-3\">
    <i class=\"material-icons\">arrow_back</i>
    Atrás
  </a>
</div>
  <div class=\"mobile-layer\"></div>

      <div id=\"footer\" class=\"bootstrap\">
    <script type=\"text/javascript\">
    var link_ajax = 'https://amanoyarns.com/admin518ddw65d/index.php?controller=AdminModules&amp;token=e4c9d346c43bf3e98747485f72dc6dec&amp;configure=ets_cfultimate&amp;tab_module=front_office_features&amp;module_name=ets_cfultimate';
    \$(document).ready(function () {
        \$.ajax({
            url: link_ajax,
            data: 'action=etsCfuGetCountMessageContactForm',
            type: 'post',
            dataType: 'json',
            success: function (json) {
                if (parseInt(json.count) > 0) {
                    if (\$('#subtab-AdminContactFormUltimateMessage span').length)
                        \$('#subtab-AdminContactFormUltimateMessage span').append('<span class=\"count_messages \">' + json.count + '</span>');
                    else
                        \$('#subtab-AdminContactFormUltimateMessage a').append('<span class=\"count_messages \">' + json.count + '</span>');
                } else {
                    if (\$('#subtab-AdminContactFormUltimateMessage span').length)
                        \$('#subtab-AdminContactFormUltimateMessage span').append('<span class=\"count_messages hide\">' + json.count + '</span>');
                    else
                        \$('#subtab-AdminContactFormUltimateMessage a').append('<span class=\"count_messages hide\">' + json.count + '</span>');
                }

            },
        });
    });
</script>
</div>
  

      <div class=\"bootstrap\">
      <div class=\"modal fade\" id=\"modal_addons_connect\" tabindex=\"-1\">
\t<div class=\"modal-dialog modal-md\">
\t\t<div class=\"modal-content\">
\t\t\t\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>
\t\t\t\t<h4 class=\"modal-title\"><i class=\"icon-puzzle-piece\"></i> <a target=\"_blank\" href=\"https://addons.prestashop.com/?utm_source=back-office&utm_medium=modules&utm_campaign=back-office-ES&utm_content=download\">PrestaShop Addons</a></h4>
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"modal-body\">
\t\t\t\t\t\t<div class=\"alert alert-warning\">
\t\t\t\tSi quieres utilizar completamente el panel AdminModules para obtener módulos gratuitos, debes habilitar la siguiente configuración en tu servidor:
\t\t\t\t<br />
\t\t\t\t- Activar la configuración allow_url_fopen de PHP<br />\t\t\t\t\t\t\t</div>
\t\t\t</div>


\t\t\t\t\t</div>
\t</div>
</div>

    </div>
  
";
        // line 1551
        $this->displayBlock('javascripts', $context, $blocks);
        $this->displayBlock('extra_javascripts', $context, $blocks);
        $this->displayBlock('translate_javascripts', $context, $blocks);
        echo "</body>
</html>";
    }

    // line 99
    public function block_stylesheets($context, array $blocks = [])
    {
    }

    public function block_extra_stylesheets($context, array $blocks = [])
    {
    }

    // line 1469
    public function block_content_header($context, array $blocks = [])
    {
    }

    // line 1470
    public function block_content($context, array $blocks = [])
    {
    }

    // line 1471
    public function block_content_footer($context, array $blocks = [])
    {
    }

    // line 1472
    public function block_sidebar_right($context, array $blocks = [])
    {
    }

    // line 1551
    public function block_javascripts($context, array $blocks = [])
    {
    }

    public function block_extra_javascripts($context, array $blocks = [])
    {
    }

    public function block_translate_javascripts($context, array $blocks = [])
    {
    }

    public function getTemplateName()
    {
        return "__string_template__f82d5a15938ae66613644f18b8eaf1c67a9968b927f2ab5e06d2ab6d9309f86d";
    }

    public function getDebugInfo()
    {
        return array (  1641 => 1551,  1636 => 1472,  1631 => 1471,  1626 => 1470,  1621 => 1469,  1612 => 99,  1604 => 1551,  1524 => 1473,  1521 => 1472,  1518 => 1471,  1515 => 1470,  1513 => 1469,  139 => 99,  39 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__f82d5a15938ae66613644f18b8eaf1c67a9968b927f2ab5e06d2ab6d9309f86d", "");
    }
}
