<?php 
namespace PrestaShop\PrestaShop\Adapter\Entity;

class WarehouseAddress extends \WarehouseAddress {};
class RequestSql extends \RequestSql {};
class ProductDownload extends \ProductDownload {};
class TaxManagerFactory extends \TaxManagerFactory {};
class TaxRulesGroup extends \TaxRulesGroup {};
class Tax extends \Tax {};
abstract class TaxManagerModule extends \TaxManagerModule {};
class TaxConfiguration extends \TaxConfiguration {};
class TaxRule extends \TaxRule {};
class TaxCalculator extends \TaxCalculator {};
class TaxRulesTaxManager extends \TaxRulesTaxManager {};
class Access extends \Access {};
class Gender extends \Gender {};
class Mail extends \Mail {};
abstract class PaymentModule extends \PaymentModule {};
class FormField extends \FormField {};
class CustomerAddressFormatter extends \CustomerAddressFormatter {};
class CustomerAddressPersister extends \CustomerAddressPersister {};
class CustomerLoginFormatter extends \CustomerLoginFormatter {};
class CustomerFormatter extends \CustomerFormatter {};
abstract class AbstractForm extends \AbstractForm {};
class CustomerForm extends \CustomerForm {};
class CustomerLoginForm extends \CustomerLoginForm {};
class CustomerPersister extends \CustomerPersister {};
class CustomerAddressForm extends \CustomerAddressForm {};
class Guest extends \Guest {};
class SearchEngine extends \SearchEngine {};
class DateRange extends \DateRange {};
class PrestaShopBackup extends \PrestaShopBackup {};
class Carrier extends \Carrier {};
class ProductSale extends \ProductSale {};
class TranslatedConfiguration extends \TranslatedConfiguration {};
class Media extends \Media {};
class Delivery extends \Delivery {};
abstract class TreeToolbarButton extends \TreeToolbarButton {};
class TreeToolbarSearchCategories extends \TreeToolbarSearchCategories {};
class Tree extends \Tree {};
class TreeToolbarSearch extends \TreeToolbarSearch {};
class TreeToolbar extends \TreeToolbar {};
class TreeToolbarLink extends \TreeToolbarLink {};
class ValidateConstraintTranslator extends \ValidateConstraintTranslator {};
class PrestaShopCollection extends \PrestaShopCollection {};
class Manufacturer extends \Manufacturer {};
class Connection extends \Connection {};
class Page extends \Page {};
class ImageManager extends \ImageManager {};
class Shop extends \Shop {};
class ShopGroup extends \ShopGroup {};
class ShopUrl extends \ShopUrl {};
class Feature extends \Feature {};
abstract class ModuleAdminController extends \ModuleAdminController {};
abstract class ProductPresentingFrontController extends \ProductPresentingFrontController {};
class FrontController extends \FrontController {};
abstract class ProductListingFrontController extends \ProductListingFrontController {};
class ModuleFrontController extends \ModuleFrontController {};
class AdminController extends \AdminController {};
abstract class Controller extends \Controller {};
class PrestaShopPaymentException extends \PrestaShopPaymentException {};
class PrestaShopException extends \PrestaShopException {};
class PrestaShopDatabaseException extends \PrestaShopDatabaseException {};
class PrestaShopModuleException extends \PrestaShopModuleException {};
class PrestaShopObjectNotFoundException extends \PrestaShopObjectNotFoundException {};
class PrestaShopLogger extends \PrestaShopLogger {};
abstract class ObjectModel extends \ObjectModel {};
class Meta extends \Meta {};
class State extends \State {};
class ProductAssembler extends \ProductAssembler {};
class CustomerAddress extends \CustomerAddress {};
class ConnectionsSource extends \ConnectionsSource {};
class GroupReduction extends \GroupReduction {};
class Upgrader extends \Upgrader {};
class PhpEncryptionLegacyEngine extends \PhpEncryptionLegacyEngine {};
class CMSRole extends \CMSRole {};
class ProductPresenterFactory extends \ProductPresenterFactory {};
class CustomerSession extends \CustomerSession {};
class Supplier extends \Supplier {};
class Tools extends \Tools {};
class SupplyOrder extends \SupplyOrder {};
class StockAvailable extends \StockAvailable {};
class Stock extends \Stock {};
class WarehouseProductLocation extends \WarehouseProductLocation {};
class StockMvtWS extends \StockMvtWS {};
class SupplyOrderReceiptHistory extends \SupplyOrderReceiptHistory {};
class StockMvtReason extends \StockMvtReason {};
class SupplyOrderHistory extends \SupplyOrderHistory {};
class StockManager extends \StockManager {};
class Warehouse extends \Warehouse {};
class StockMvt extends \StockMvt {};
class StockManagerFactory extends \StockManagerFactory {};
class SupplyOrderState extends \SupplyOrderState {};
abstract class StockManagerModule extends \StockManagerModule {};
class SupplyOrderDetail extends \SupplyOrderDetail {};
class Referrer extends \Referrer {};
class Dispatcher extends \Dispatcher {};
class Language extends \Language {};
class PhpEncryption extends \PhpEncryption {};
class Configuration extends \Configuration {};
class AddressFormat extends \AddressFormat {};
class PhpEncryptionEngine extends \PhpEncryptionEngine {};
class LocalizationPack extends \LocalizationPack {};
class MetaLang extends \MetaLang {};
class OrderMessageLang extends \OrderMessageLang {};
class TabLang extends \TabLang {};
class StockMvtReasonLang extends \StockMvtReasonLang {};
class GroupLang extends \GroupLang {};
class ProfileLang extends \ProfileLang {};
class ContactLang extends \ContactLang {};
class DataLang extends \DataLang {};
class ConfigurationLang extends \ConfigurationLang {};
class ThemeLang extends \ThemeLang {};
class SupplyOrderStateLang extends \SupplyOrderStateLang {};
class AttributeGroupLang extends \AttributeGroupLang {};
class RiskLang extends \RiskLang {};
class OrderReturnStateLang extends \OrderReturnStateLang {};
class FeatureValueLang extends \FeatureValueLang {};
class CarrierLang extends \CarrierLang {};
class QuickAccessLang extends \QuickAccessLang {};
class AttributeLang extends \AttributeLang {};
class GenderLang extends \GenderLang {};
class CategoryLang extends \CategoryLang {};
class OrderStateLang extends \OrderStateLang {};
class FeatureLang extends \FeatureLang {};
class CmsCategoryLang extends \CmsCategoryLang {};
class Store extends \Store {};
class JavascriptManager extends \JavascriptManager {};
class StylesheetManager extends \StylesheetManager {};
class JsMinifier extends \JsMinifier {};
class CccReducer extends \CccReducer {};
abstract class AbstractAssetManager extends \AbstractAssetManager {};
class CssMinifier extends \CssMinifier {};
class OrderDiscount extends \OrderDiscount {};
class OrderReturnState extends \OrderReturnState {};
class OrderInvoice extends \OrderInvoice {};
class OrderPayment extends \OrderPayment {};
class Order extends \Order {};
class OrderMessage extends \OrderMessage {};
class OrderHistory extends \OrderHistory {};
class OrderCartRule extends \OrderCartRule {};
class OrderReturn extends \OrderReturn {};
class OrderCarrier extends \OrderCarrier {};
class OrderDetail extends \OrderDetail {};
class OrderState extends \OrderState {};
class OrderSlip extends \OrderSlip {};
class CheckoutAddressesStep extends \CheckoutAddressesStep {};
class CheckoutSession extends \CheckoutSession {};
class DeliveryOptionsFinder extends \DeliveryOptionsFinder {};
class AddressValidator extends \AddressValidator {};
class PaymentOptionsFinder extends \PaymentOptionsFinder {};
class ConditionsToApproveFinder extends \ConditionsToApproveFinder {};
class CheckoutProcess extends \CheckoutProcess {};
abstract class AbstractCheckoutStep extends \AbstractCheckoutStep {};
class CheckoutPaymentStep extends \CheckoutPaymentStep {};
class CartChecksum extends \CartChecksum {};
class CheckoutDeliveryStep extends \CheckoutDeliveryStep {};
class CheckoutPersonalInformationStep extends \CheckoutPersonalInformationStep {};
class Translate extends \Translate {};
class Category extends \Category {};
class CMSCategory extends \CMSCategory {};
class Attribute extends \Attribute {};
class Context extends \Context {};
class Message extends \Message {};
class SupplierAddress extends \SupplierAddress {};
class ConfigurationKPI extends \ConfigurationKPI {};
class Address extends \Address {};
class EmployeeSession extends \EmployeeSession {};
class Zone extends \Zone {};
class QqUploadedFileForm extends \QqUploadedFileForm {};
class Link extends \Link {};
class Notification extends \Notification {};
class Customer extends \Customer {};
class Image extends \Image {};
class Contact extends \Contact {};
class CustomerThread extends \CustomerThread {};
class ConfigurationTest extends \ConfigurationTest {};
class SpecificPriceRule extends \SpecificPriceRule {};
class Employee extends \Employee {};
class TemplateFinder extends \TemplateFinder {};
class SmartyResourceParent extends \SmartyResourceParent {};
class SmartyCustom extends \SmartyCustom {};
class SmartyResourceModule extends \SmartyResourceModule {};
class SmartyDevTemplate extends \SmartyDevTemplate {};
class SmartyCustomTemplate extends \SmartyCustomTemplate {};
class Attachment extends \Attachment {};
abstract class Cache extends \Cache {};
class CacheMemcached extends \CacheMemcached {};
class CacheXcache extends \CacheXcache {};
class CacheMemcache extends \CacheMemcache {};
class CacheApc extends \CacheApc {};
class Country extends \Country {};
class WebserviceOutputJSON extends \WebserviceOutputJSON {};
class WebserviceRequest extends \WebserviceRequest {};
class WebserviceException extends \WebserviceException {};
class WebserviceKey extends \WebserviceKey {};
class WebserviceSpecificManagementSearch extends \WebserviceSpecificManagementSearch {};
class WebserviceSpecificManagementImages extends \WebserviceSpecificManagementImages {};
class WebserviceOutputBuilder extends \WebserviceOutputBuilder {};
class WebserviceOutputXML extends \WebserviceOutputXML {};
class ImageType extends \ImageType {};
class Tag extends \Tag {};
class Profile extends \Profile {};
class SpecificPrice extends \SpecificPrice {};
class LinkProxy extends \LinkProxy {};
class Product extends \Product {};
class CartRule extends \CartRule {};
class Alias extends \Alias {};
class Group extends \Group {};
class Hook extends \Hook {};
class FileUploader extends \FileUploader {};
class Search extends \Search {};
class Windows extends \Windows {};
abstract class Module extends \Module {};
abstract class ModuleGridEngine extends \ModuleGridEngine {};
abstract class ModuleGrid extends \ModuleGrid {};
abstract class ModuleGraph extends \ModuleGraph {};
abstract class CarrierModule extends \CarrierModule {};
abstract class ModuleGraphEngine extends \ModuleGraphEngine {};
class Uploader extends \Uploader {};
class AttributeGroup extends \AttributeGroup {};
class Customization extends \Customization {};
class Risk extends \Risk {};
class ManufacturerAddress extends \ManufacturerAddress {};
class Tab extends \Tab {};
class QqUploadedFileXhr extends \QqUploadedFileXhr {};
class Curve extends \Curve {};
class Cookie extends \Cookie {};
class Validate extends \Validate {};
class HTMLTemplateOrderReturn extends \HTMLTemplateOrderReturn {};
class HTMLTemplateDeliverySlip extends \HTMLTemplateDeliverySlip {};
class PDF extends \PDF {};
class HTMLTemplateInvoice extends \HTMLTemplateInvoice {};
class HTMLTemplateOrderSlip extends \HTMLTemplateOrderSlip {};
abstract class HTMLTemplate extends \HTMLTemplate {};
class PDFGenerator extends \PDFGenerator {};
class HTMLTemplateSupplyOrderForm extends \HTMLTemplateSupplyOrderForm {};
class CMS extends \CMS {};
class CustomerMessage extends \CustomerMessage {};
class HelperOptions extends \HelperOptions {};
class HelperKpiRow extends \HelperKpiRow {};
class HelperUploader extends \HelperUploader {};
class HelperTreeShops extends \HelperTreeShops {};
class HelperView extends \HelperView {};
class Helper extends \Helper {};
class HelperKpi extends \HelperKpi {};
class HelperList extends \HelperList {};
class HelperShop extends \HelperShop {};
class HelperImageUploader extends \HelperImageUploader {};
class HelperCalendar extends \HelperCalendar {};
class HelperTreeCategories extends \HelperTreeCategories {};
class HelperForm extends \HelperForm {};
class ProductSupplier extends \ProductSupplier {};
class Currency extends \Currency {};
class DbPDO extends \DbPDO {};
class DbMySQLi extends \DbMySQLi {};
abstract class Db extends \Db {};
class DbQuery extends \DbQuery {};
class Pack extends \Pack {};
class Cart extends \Cart {};
class Chart extends \Chart {};
class Combination extends \Combination {};
class CustomizationField extends \CustomizationField {};
class QuickAccess extends \QuickAccess {};
class RangePrice extends \RangePrice {};
class RangeWeight extends \RangeWeight {};
class FeatureValue extends \FeatureValue {};
class CSV extends \CSV {};
class AddressChecksum extends \AddressChecksum {};
class FileLogger extends \FileLogger {};
abstract class AbstractLogger extends \AbstractLogger {};
